/*
 *	Version : @(#)end.c	1.5	11/03/93
 */

/**************************************************************************
**                                                                        *
**  FILE        :  end.c   (might be needed when CrossView-166 is used)   *
**                                                                        *
**  DESCRIPTION :  This buffer is used to hold string constant given      *
**		   by the user. For example when specifying the CPU to    *
**		   be used by CrossView via the $CPU variable or when	  *
**		   passing strings as parameter to command line procedure *
**		   calls. If you need more space for string constants     *
**		   increase the size of _buffer[].                        *
**                                                                        *
**	NOTE that this module must be compiled with the -g option,        *
**	so HLL debugging information is passed to CrossView-166.          *
**	It should be linked with the reset-task only (like cstart).       *
**                                                                        *
**  COPYRIGHT   :  1993 Tasking Software B.V., Amersfoort                 *
**                                                                        *
**************************************************************************/

char _buffer[100];

void
_end_( void )
{
	int x = 1;	/* This line is used as the return address */
			/* command line procedure calls of xvw166  */
}
