/* @(#)AC.C */
/* ASEA SOURCE CODE */
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	FILE:			ac.c
//	PURPOSE:		Source Module for the ASEA CONTROLLER, 3-phase models.
//
//	Proprietary Software:	ASEA POWER SYSTEMS
//							5151 Oceanus Drive, Unit 105
//							Huntington Beach, CA  92649
//							714.899.5559
//
//				COPYRIGHT ASEA POWER SYSTEMS JANUARY 1999
//				COPYRIGHT ASEA POWER SYSTEMS APRIL 2000
//				COPYRIGHT ASEA POWER SYSTEMS JANUARY 2001
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ALL FIRMWARE MODULES HEREWITHIN ARE SPECIFIC TO ASEA POWER SYSTEMS' MARINE CONVERTER PRODUCT LINES
// DESIGNED & DEVELOPED by:  DAVID E. NEWBERRY (Embedded Systems Engineering) for ASEA POWER SYSTEMS.
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/* ======================================================================================= INCLUDE FILES */
#include "ac.h"			//Asea Controller main header file.
//#define AUTO_CLEAR_HW_TRAPS		//v2.29 resets on hardware traps incl. stack over/underflow.

//==========================================================================
// Typedefs
//==========================================================================
typedef unsigned long	timeType;	//This type exists in log.h
typedef unsigned		timerId;	//Timer Id/Handle is an index

#ifdef MM3
	#define VOUT_OK (gp_state.inpstat1)				//need to pass test if input on for DMC.
#else
	#define VOUT_OK ((gp_status_word2&0x0008)>>3)	//bit#3 in status_word_2 masked & shifted right 3.
#endif //~MM3
/* ========================================================================================================= */
/* ======================================================================================= PERSISTENT MEMORY */
#pragma noclear
/* ========================================================================================================= */
#pragma align hb=c 								// ALIGNMENT: PEC ADDRESSABLE
/* ========================================================================================================= */
/* ========================================================================================================= */
#ifdef MM3
	char *IDN="ASEA POWER 1.02\0";			// Always keep version# updated!
	char *VER=           "1.02\0";
#else
	#ifdef TEST_FW
		char *IDN="ASEA POWER TEST\0";		// Always keep version# updated!
		char *VER=           "TEST\0";
	#else
		#ifdef LIQUID_COOLED_MODEL
				char *IDN="ASEA POWER 3.08\0";		// LC Models, std.product
				char *VER=           "3.08\0";		//next version: v3.08
		#else
				char *IDN="ASEA POWER 9.10\0";		//XMFR TAP Display for M/Y Constellation, GEN START CMD, Input XMFR TAP Select Display
				char *VER=           "9.10\0";		//next version: v9.11

//				char *IDN="ASEA POWER 8.00\0";		// WESTPORT50 compile flag
//				char *VER=           "8.00\0";		//next version: v8.01

//				char *IDN="ASEA POWER 5.00\0";		// DUAL-SHORE CORD compile flag
//				char *VER=           "5.00\0";		//next version: v5.00
		#endif //~LIQUID_COOLED
	#endif //~TEST_FW
#endif //~MM3

/*
VERSION		DATE			DESCRIPTION */
/*************************************************************************************************************************
v3.00		8/25/08		Load Management 3.0
						"Oil Level Low" statusWord3_bit#15 added to Event Log, WARNING issued when LOW
						ModbusRTU f(2)/Read DI, f(3)/Read HoldingReg, f(6)/Write HoldingReg completed.

v2.99		2/28/08		Lurrsen gen_mux_id initialization.

v2.91	  	07/17/2007	Westport 5003/5004 development, includes:
						-604277/278 TIP command output
v2.88	  	05/01/2007	EPROM CHECK-SUM: LOW=, HIGH= 
						Lurssen M/Y Bounty Hunter development (MULTIGEN_BESECKE), includes
						-Modbus RTU Server fully featured & functional
						-Special Bus Modes & Transfers (incl Tie-Breaker) using 604278 assy
						-Note: EPROM available program memory space is now < 6KBytes (swap@BE65Ah), so,
								must refactor source to reduce footprint!
****************************************************************************************************************************
***************************************************************************************************************************/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#pragma default_attributes
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
//
// VCO PROTOTYPES
//
//#define VCO_MOD
#ifdef VCO_MOD
void testVCO(void);
void doVCO(void);
unsigned get_adc_count(unsigned);
float get_average_frequency(void);

unsigned frequency_mode;		//frequency control by external Vdc (OFF/ON)
unsigned VCO_MAX;
unsigned VCO_MIN;
#endif //VCO_MOD
////

void processHybridSTO(void);

int process_REM_XFR_REQ(void);
int process_MULTIGEN_REM_XFR_REQ(void);	//v2.71

void genSpeedTester(void);
void gen2genActuatorResponseEdit(void); //NEW_G2G_OPTION. (not used for std. TECHNEL_OPTION).

unsigned char last_transfer_status;
int active_status_code;
int last_failure_code;
int last_failure_code_logged;
unsigned maintenanceMode; //1=No transfers, CP_ON ok.  0=Transfers ok if bus well-defined.

char bat_ok[40];					/* battery backed up ram test string */
int RAM_reset;           				//flag to indicate RAM has been reset.

volatile unsigned long eventTime;	//unsigned long eventTime in 10ms units.
volatile unsigned long lastWattTime;	//unsigned long wattTime in 10ms units.
volatile unsigned long wattTime;	//unsigned long wattTime in 10ms units.
float wattHours;
unsigned wattHourMeterON;

volatile unsigned milliseconds;
volatile unsigned seconds;
volatile unsigned minutes;
volatile unsigned long hours;

int INTERFACE;						/* 0=IEEE-488, 1=RS-232, 2=none */
unsigned char IE_dev_address;				/* 0-30, device address in BB RAM. */
long int ser_baud_rate;					/* Baud rate */
int ser_parity;						/* Parity */
int eos_mode;						/* XMTD eos: 1=LF, 0=CR/LF, 2=CR. */

//char *strOwner="OWNER \0";
//char *strOwner="LIGHTS\0";
//char *strOwner="CLEAN \0";
char *strOwner="MIXED \0";

char *copyright="  (c)COPYRIGHT 2019 ASEA POWER SYSTEMS  \0";

char *key_name[]={	"NONE\0",
					"CP_ON\0",
					"TS_GENERATOR\0",
					"TS_OFF\0",
					"TS_CONVERTER\0",
					"CP_OFF\0",
					"F1\0",
					"F2\0",
					"F3\0",
					"HELP\0",
					"SHORE POWER\0",
					"GENERATOR POWER\0",
					"CONVERTER POWER\0",
					"SYSTEM STATUS\0"};

//char *sumstat[5]={	"OFFLINE       \0",
//			   		"ONLINE        \0",
//			   		"FAULT         \0",
//			   		"UNDER-VOLTAGE \0",
//			   		"OVER-VOLTAGE  \0"};

char *sumstat[5]={	"OFFLINE\0",
			   		"ONLINE \0",
			   		"FAULT  \0",
			   		"UNDER-V\0",
			   		"OVER-V \0"};

char *system_status[3]={	"   OK  \0","WARNING\0","FAILURE\0"};

char *master_slave[2]={	"SLAVE \0",
						"MASTER\0"};

char *shutoffStr[15]={"INPUT is ONLINE\0",					//0
		   			"SHORE POWER OFF KEY PRESSED\0",		//1
		   			"REMOTE SHORE POWER OFF\0",			//2
		   			"REMOTE EMERGENCY POWER OFF\0",		//3
		   			"BLACKOUT OR RED-EMERGENCY SWITCH\0",	//4
		   			"DOCK POWER BROWNOUT SHUTDOWN\0",		//5
		   			"OVERTEMPERATURE SHUTDOWN\0",			//6
		   			"OVERLOAD SHUTDOWN\0",				//7
		   			"INPUT POWER FORM CHANGE SHUTDOWN\0",	//8
		   			"HVDC > 210 SHUTDOWN\0",				//9
		   			"PFC FAULT SHUTDOWN\0",				//10, not used.
		   			"LOW-VOLTAGE DC SHUTDOWN\0",			//11
		   			"GROUND FAULT SHUTDOWN\0",			//12
		   			"INVERTER FAULT SHUTDOWN\0",			//13, not used.
		   			"EMERGENCY POWER OFF\0"};			//14, EPO
//		   			"Vout OK LOW, OUTPUT SHUTDOWN\0"};	//14, not used.

char vessel[40];
char *ship_name=vessel;

unsigned autorestart;
unsigned last_autorestart;
volatile unsigned shore_power_off_key; //v1.84

//TECHNEL_OPTION
unsigned generator_on_delay;
unsigned gen_start_cmd_enable;
unsigned inhibit_transfer;
//END TECHNEL_OPTION

//NEW_G2G_OPTION
unsigned generator_off_delay;
unsigned gen_stop_cmd_enable;
//END NEW_G2G_OPTION

//TIP_OPTION
unsigned tip_delay;
unsigned tip_cmd_enable;
//END TIP_OPTION

unsigned generator_transfer_delay;		//crossover
unsigned generator_transfer_rolloff;	//rolloff
unsigned transfer_timeout;				//generator conf timeout

unsigned resetDelay;
unsigned resetCbOn;
unsigned external_cb_delay;
unsigned external_cb_Open_delay;
unsigned monitorExtCb;

struct mach_struct mach;

float newSlewRate,newFrequency,newVoltage;	//used with slew_output() for load management.
float tempSlewRate,tempFrequency,tempVoltage;	//used with slew_output() for load management.

int shoreCordRating;						// {30 to 250 amps}.
int alarmLevel;								// {50to100% of dsc_cordRating}.
unsigned alarmState;						// {DISABLED,ENABLED}.
int droop;
int droopEnable;

unsigned long droopDampTime;
unsigned autoTransferOnOverload;
unsigned autoTransferGenset;

unsigned LAST_configuration_word;		// GP CONFIGURATION WORD--LAST.
unsigned LAST_configuration_word2;	//CS3B(low) GP CONFIGURATION WORD--LAST.

float lastMasterCapacity;

int slaveLoadShare;
//							dutyCycle,percentOfLoad(LOW_RANGE),percentOfLoad(HIGH_RANGE)
int loadShareTable[100][3]={1,2438,1128,
							2,2474,1072,
							3,2452,1049,
							4,2406,1033,
							5,2348,1004,
							6,2286,982,
							7,2218,964,
							8,2144,947,
							9,2055,926,
							10,1980,909,
							11,1908,896,
							12,1830,883,
							13,1762,870,
							14,1696,857,
							15,1634,845,
							16,1576,832,
							17,1498,820,
							18,1455,808,
							19,1405,802,
							20,1342,793,
							21,1300,778,
							22,1258,773,
							23,1218,765,
							24,1184,756,
							25,1142,744,
							26,1110,739,
							27,1078,733,
							28,1052,722,
							29,1025,717,
							30,1000,712,
							31,975,706,
							32,953,701,
							33,932,690,
							34,906,685,
							35,888,680,
							36,870,675,
							37,852,670,
							38,836,665,
							39,821,660,
							40,806,655,
							41,791,650,
							42,775,645,
							43,761,641,
							44,750,638,
							45,739,635,
							46,726,630,
							47,716,625,
							48,705,620,
							49,695,617,
							50,685,612,
							51,676,610,
							52,666,606,
							53,658,604,
							54,649,600,
							55,641,596,
							56,634,592,
							57,628,590,
							58,619,587,
							59,611,584,
							60,604,582,
							61,598,579,
							62,592,577,
							63,587,573,
							64,581,569,
							65,575,568,
							66,570,566,
							67,563,564,
							68,557,562,
							69,553,560,
							70,548,558,
							71,544,556,
							72,538,554,
							73,536,552,
							74,530,550,
							75,525,545,
							76,522,544,
							77,517,542,
							78,514,540,
							79,508,538,
							80,498,536,
							81,493,535,
							82,491,533,
							83,486,531,
							84,483,529,
							85,479,527,
							86,478,525,
							87,476,523,
							88,471,522,
							89,467,521,
							90,464,519,
							91,462,518,
							92,458,516,
							93,456,514,
							94,454,513,
							95,451,512,
							96,449,511,
							97,446,510,
							98,442,509,
							99,440,508,
							100,438,507};

float maxSystemLevel;
float maxSystemPower;

float maxGenFreq;
float minGenFreq;

float maxLevelIn1;
float maxPowerIn1;

//CONVERTER INPUT
float minShorVolts;
float maxShorVolts;
float minShorAmps;
float maxShorAmps;
float minShorFreq;
float maxShorFreq;
//CONVERTER OUTPUT
float minConvVolts;
float maxConvVolts;
float minConvAmps;
float maxConvAmps;
float minConvFreq;
float maxConvFreq;
float maxConvLevel;
float maxConvPower;
//GENERATOR #1
float minGen1Volts;
float maxGen1Volts;
float minGen1Freq;
float maxGen1Freq;
//GENERATOR #2
float minGen2Volts;
float maxGen2Volts;
float minGen2Freq;
float maxGen2Freq;

volatile unsigned retryCount;
unsigned actuatorResponseDelay;			//500ms between pulses or delay for response in gen freq.

unsigned gen1DutyCycle;
unsigned gen2DutyCycle;

//unsigned ssTimer;			//declare ssTimer.
//unsigned inhibitOnTimer;		//declare inhibitOnTimer.
timerId ssTimer;			//declare ssTimer.
timerId inhibitOnTimer;		//declare inhibitOnTimer.

unsigned autoShutdownEnable;		//M/Y BOUNTY HUNTER feature
unsigned negCrossover;

float LmShedOffLevel;
float LmShedOnLevel;
float LmAddOnLevel;
float LmAddOffLevel;

unsigned LmShedOnDelay;
unsigned LmShedOffDelay;
unsigned LmAddOnDelay;
unsigned LmAddOffDelay;

timerId LmAddOffTimer;		//declare & allocate timer
timerId LmShedOffTimer;	//declare & allocate timer
timerId LmShedOnTimer;		//declare & allocate timer
timerId LmAddOnTimer;		//declare & allocate timer

/* ======================================================================================= VOLITILE MEMORY */
/* ======================================================================================= VOLITILE MEMORY */
#pragma clear	// fill iram with cleared data so noclear data is put in batt ram.
/* ======================================================================================= VOLITILE MEMORY */

volatile unsigned ts;

unsigned long droopOnTime;
unsigned long overloadAlarmTime;

float system_level;						//highest level of input/output A/B/C.
float system_kva;						//highest load of input/output A/B/C.
float system_kw;								//highest power of input/output A/B/C.

float averSystemLevel;
float averSystemPower;

iram float Van_DC, Vbn_DC, Vcn_DC;
iram float Van, Vbn, Vcn, Vab, Vbc, Vca;		/* RMS metered output value */
iram float Ia_rms, Ib_rms, Ic_rms;					/* RMS metered output value */
iram float Ia_pk, Ib_pk, Ic_pk;						/* peak currents */
iram float KWa, KWb, KWc;							/* metered output value */
iram float KVAa, KVAb, KVAc;							/* calculated output value */
float PFa, PFb, PFc;							/* calculated output value */
float ICFa, ICFb, ICFc;							/* calculated output value */

float gp_Van_DC[8], gp_Vbn_DC[8], gp_Vcn_DC[8];	/* RMS metered output value */
float gp_Van[8], gp_Vbn[8], gp_Vcn[8];			/* RMS metered output value */
float gp_Vab[8], gp_Vbc[8], gp_Vca[8];			/* RMS metered output value */
float gp_Ia_rms[8], gp_Ib_rms[8], gp_Ic_rms[8];	/* RMS metered output value */
float gp_Ia_pk[8], gp_Ib_pk[8], gp_Ic_pk[8]; 	/* peak currents */
float gp_KWa[8], gp_KWb[8], gp_KWc[8];			/* metered output value */
float gp_KVAa[8], gp_KVAb[8], gp_KVAc[8];	   	/* calculated output value */
float gp_PFa[8], gp_PFb[8], gp_PFc[8];			/* calculated output value */
float gp_ICFa[8], gp_ICFb[8], gp_ICFc[8];	  	/* calculated output value */
float gp_level_a[8], gp_level_b[8], gp_level_c[8];	/* calculated output Load Level (% rating) */
float gp_Freq[8];							  	/* calculated frequencye */

int huge *a_volts;	// meter_data pointer.
int huge *a_amps;		// meter_data pointer.
int huge *b_volts;	// meter_data pointer.
int huge *b_amps;		// meter_data pointer.
int huge *c_volts;	// meter_data pointer.
int huge *c_amps;		// meter_data pointer.

int sp1a_raw[8];	//first n samples of raw shore power voltage.
int sp1b_raw[8];
int sp1c_raw[8];
int sp2a_raw[8];
int sp2b_raw[8];
int sp2c_raw[8];
int sp2ba_raw[8];
int sp2cb_raw[8];
int sp2ac_raw[8];

volatile unsigned abort_power_on;
volatile unsigned Lock;

volatile int gp_diagnostic_code;
unsigned sync_mux_id;
unsigned daq_mux_id;
int undetected_sync_signal_source;

unsigned int moisture_oil_init_delay = 100;					//1 second init delay before start checking moisture in oil

struct gp_state_struct gp_state;

#ifdef TRACE_ON
struct queueType trace,*trace_queue;
#endif //TRACE_ON

volatile unsigned update_display;

volatile unsigned special_function;
volatile int gp_mode;
volatile int monitor_started;
unsigned KB_LED;
volatile int combo_key;
volatile int display_type;
volatile int display_function;
volatile int first_time_through;

iram volatile int meter_calc;			/* flag == 1 allows metering calculation */

iram char cvtbufr[161];			/* buffer for sprintf conversions (== 1 line) */

volatile iram unsigned long wait_10ms;		/* 10ms interrupt increments */

int x, y;				/* X and Y axis of cursor */
int start_x, start_y;			/* starting axis of cursor */

iram char strng[200];			//RESERVE 160+40 CHARS
char *str=strng;			//point to RESERVEd 160 char string + null
char character;				/* string to be output */

volatile unsigned wdt;		// External UPC Watch Dog pulse flip-flop flag.
volatile unsigned flash;		// used by display_metering()

volatile unsigned input_dropped;

volatile unsigned transfer_in_progress;
unsigned g2c;
unsigned c2g;
volatile unsigned remote_off_flag;
unsigned remoteActive;

unsigned int debug = 0, debug2 = 0, debug3 = 0;

unsigned int optcon_status_rd, optcon_status_save;


unsigned lastDuty;
volatile unsigned monitorTime;

Xfer_Imped_Ramp_Stat_Type Xfer_Imped_Ramp_Status;
unsigned int Impedance_Rate_Q8;
unsigned int Impedance_Ramp_Cnt;
unsigned int New_Imped_Duty;

unsigned int Active_DutyCycle;

unsigned char ten_ms_IRQ = 0;

int Gen_Sel;

unsigned int Remote_Panel_Xfr_Req_Stat;

float Debug_Var[10];
//unsigned int Debug_Int[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};



void show_test_generator_screen( int );		//prototype

unsigned virtualTransferRequest;

/* ======================================================================================= EXTERNAL REFERENCES */
/* ======================================================================================= EXTERNAL REFERENCES */
//from upc_dm.c 	DISPLAY_MANAGER.
extern void display_metering(int, int); 			/* Show meter & ENTRY values & update */
extern void show_signon_screen(void);

extern unsigned int XFMR_Tap_Delay_Rd;

//from upc_key.c	KEYPAD
extern void strobe_kybd_for_slew(void);
extern int wait_for_key(void);

//from upc_lcd.c	LCD
extern void init_lcd(void);
extern void lcd_display(char *, int, int);
extern float lcd_display_value(float, int, int, char *);
extern void clear_screen(void);
extern void put_cursor(int,int);

//from upc_meas.c	METER
extern float amps2volts;					// sets ammeter scaling.
extern float volts2volts;					// sets voltmeter scaling.
extern volatile unsigned sync_count;		// global temp.
extern volatile unsigned sync_1_count;		// global temp.
extern unsigned deltaT5gen1;	// Relative Tick count for one cycle, used for frequency calc.
extern unsigned deltaT5gen2;	// Relative Tick count for one cycle, used for frequency calc.
extern void meas_all(void);			/* calc new meter values. */
extern float get_positive_hvdc(void);
extern float get_negative_hvdc(void);
extern void meas_avg_system_temperature(void);
extern void initialize_metering_ram(void);			// initializes battery-backed-up RAM variables for osc calib.,etc..
//extern void dummy_meter_data(unsigned);

//from upc_sid.c	SYSTEM INTERFACE DRIVER
extern void setGenSpeedMux(int);	//NEW_G2G_OPTION speed-trim MUX.
//#ifdef TEST_FIXTURE
//extern volatile unsigned far int gp_configuration_word;	//CS3B(low) GP CONFIGURATION WORD.
//#endif //~TEST_FIXTURE
extern unsigned VIRTUALconfiguration_word;	// VIRTUAL CONFIGURATION WORD.
extern unsigned VIRTUALconfiguration_word2;	// VIRTUAL CONFIGURATION WORD.
extern volatile unsigned far int gp_status_word;		// GP STATUS WORD--.
extern volatile unsigned far int gp_status_word2;		// GP STATUS WORD2--.
extern volatile unsigned far int gp_status_word3;		// GP STATUS WORD3--.

#ifdef NO_OPTCON_PCB
extern volatile unsigned far int optcon_control;		// OPCON CONTROL WORD--DUMMY.
extern volatile unsigned far int optcon_control_2;		// OPCON CONTROL WORD 2.
#else												//CS3B(low)
extern volatile system unsigned int optcon_control;		//CS2*Y7*(low) OPCON CONTROL WORD.
extern volatile system unsigned int optcon_control_2;		//CS2*Y7*(low) OPCON CONTROL WORD 2.
#endif

extern volatile unsigned far int gp_configuration_word;	//CS3B(low) GP CONFIGURATION WORD.
extern volatile unsigned far int gp_configuration_word2;	//CS3B(low) GP CONFIGURATION WORD.
extern unsigned OPTCON_CONTROL_2;	//image in RAM
extern volatile unsigned last_sw1;
extern volatile unsigned last_sw2;
extern volatile unsigned last_sw3;
extern int set_meter_mux(unsigned, unsigned);
extern int check_gp_status(void);
extern int set_sysstat(unsigned);
extern int sys_config(unsigned);
extern float GP_OUTPUT_VOLTAGE;
extern float GP_OUTPUT_FREQUENCY;
extern float RATED_POWER;
extern float IN_RATED_AMPS_LO_ONE;
extern unsigned AC75_CONTROL_PCB;	//S4-7	Used in Model 'L' Series & AC75, or bigger, units.
extern unsigned GP_OUTPUT_FORM;
extern unsigned DUAL_INPUT;
extern unsigned PARALLELING;
extern unsigned SEAMLESS_TRANSFER;
extern unsigned TIP_OPTION;
extern unsigned TECHNEL_OPTION;
extern unsigned AEM_OPTION;
extern unsigned DUAL_GENSET;
extern unsigned EXT_CB_OPTION;
extern unsigned EXT_XFMR_OPTION;	//S3-3
extern unsigned AEM2_OPTION;		//S4-2
extern unsigned MODEL_LC;			//S4-8

extern void ledLightShow(void);

extern unsigned GEN_START_OPTION;
extern unsigned GEN2GEN_OPTION;
extern unsigned MULTIGEN_OPTION;	//S3-5
extern unsigned MULTIGEN_INHIBIT;	//S3-6
extern unsigned config_no_crossover_delay(void);	//S2-8
extern unsigned AUTO_TRANSFER;		
extern unsigned auto_transfer_enable;		
extern unsigned transferMode;		
extern unsigned NEW_G2G_OPTION;	//S3-8
extern unsigned SIX_CONTACTOR_CONFIG;	//S4-4

extern unsigned GEN_AMPS_PRESENT;	//S4-6  v2.32, or better
extern unsigned REMOTE_PANEL;		//S4-3
extern unsigned REMOTE_RUN;			//S2-4 RUN(low) input M1041
extern unsigned SWITCHGEAR_INTERFACE_OPTION;	//S2-8
extern unsigned OPTCON_PCB;		//opcon_status bit#15

extern void set_REM_IN_ON(void);		//toggle "REM_IN_ON" signal high then low.
extern unsigned get_REM_XFR_REQ(void);	//REMOTE_PANEL
extern unsigned get_REM_RUN_REQ(void);	//REMOTE_RUN.  Returns 1 if REM_RUN_REQ received.

extern unsigned EXT_OSC_OPTION;			//S3-6
extern unsigned HIGH_Z;					//S2-3
extern unsigned TIE_CONF_PRESENT;		//S3-7 Master only
extern unsigned MULTIGEN_BESECKE;		//gp_status_word3 & 0x0004 (high) to configure.
extern unsigned AUTO_SHUTDOWN;			//M/Y BOUNTY HUNTER feature
extern unsigned HYBRID_STO;				//S2-3 and S2-7

extern unsigned int Oil_Level_Timer, Moisture_Oil_Timer;	//sid.c

extern char *gp_cid;


//from upc_osc.c	OSCILLATOR & TRANSIENT
extern void init_transient_ram(void);
extern int slew_output(float, float, float);		//create and execute transient for output transistion.
extern void init_outputs(void); 			// set outputs to nominal.
extern void init_waveforms(void);
extern void exec_osc_pgm(void);
extern void set_Vr_ILMr(void);
extern void initialize_waveform_ram(void);
extern void initialize_program_ram(void);

extern volatile system unsigned int pha_wf_dac;		// phase A waveform DAC.
extern volatile system unsigned int phb_wf_dac;		// phase B waveform DAC.
extern volatile system unsigned int phc_wf_dac;		// phase C waveform DAC.
extern unsigned huge osc_wf[WF_ARRAY];		// executed waveforms, #pragma noclear or CSTART will blow up.
extern int ALC_mode;						/* 0=disabled, 1=Enabled */
extern int AGC_state;						/* 0=disabled, 1=Enabled */
extern volatile unsigned new_key;
extern volatile unsigned last_key;
extern volatile unsigned key_hold_time;
extern struct user_pgm_structure far pgm_edit;			/* make array for editing programs */
extern struct user_pgm_structure far def_pgm;			/* make array for factory defaults */
extern volatile unsigned osc_gen_sync;
extern float positive_hvdc;
extern float negative_hvdc;
extern iram unsigned int wfp_a, wfp_b, wfp_c;	/* waveform pointers for PECs */
extern iram unsigned int pwm_f, pwm_s;		/* loads pwm frequency fast & slow from PECs*/

extern volatile unsigned far int gp_control_word;			//CS3B(low) GP CONTROL WORD.
extern unsigned volatile GP_CONTROL_WORD;							//image in RAM

//from comm.c	REMOTE COMMUNICATIONS
extern unsigned remote_genset;
extern unsigned remoteEvLogMode;

extern void clear_meter_data(unsigned);  // set measured meter data values to zero given daq channel.

//EVENT LOG extern
#include "event.h"
extern int LogEvent( unsigned char, unsigned char );
extern void initializeEventLog( void );
extern unsigned eventLogCount( void );
extern void eventLogViewer( void );
extern void eventLogTracker( void );
extern void displayEventRegistrationInformation( void );
extern void editEventRegistrationScreen( void );
extern void startEventLog( void );
extern void stopEventLog( void );
extern void initializeEvMaskAll( void );

extern volatile unsigned shutoffId;

//#ifdef MODBUS						//v3.00
extern void remoteManager( void );
extern unsigned modbusActive;
extern void initializeModbusRAM( void );
extern unsigned char modbusNodeId;
extern unsigned mbCommand;
//#endif //MODBUS

extern void initialize_sid(void);

extern void softStartGuard( void );	//v3.00
extern void	initializeTimers( void );	//initialize all timers.
extern timerId getTimer( void );		//allocate a new timer.
extern void	freeTimer( timerId );		//deallocate this timer.
extern unsigned timerRunning( timerId );	//Boolean indicating if timer started.
extern void inhibitInputOn( void );			//REM_IN_OFF asserted HIGH.
extern void allowInputOn( void );		//REM_IN_OFF deasserted LOW.

extern void auto_shutdown_control_screen(void);
extern unsigned u_TIP_signal;

extern unsigned int Oil_Fault;

/* ======================================================================================= FUNCTION DEFINITIONS */
/*---------------------------------------------------------------------------*/
_inline unsigned tieBreaker_CLOSED()
{
#ifndef WESTPORT50
	if (TIE_CONF_PRESENT)
		return (_getbit(P5,9));					//TIE AUX Confirmation signal.
	else
#endif //WESTPORT50
		return  ((optcon_status_2 & 4) == 4);	//CB3 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
_inline unsigned cb6_CLOSED()
{
	return  ((optcon_status_2 & 0x0010) == 0x0010);	//CB6 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned cb4_CLOSED()
{
	if (NEW_G2G_OPTION)
		return  ((optcon_status_2 & 0x1000) == 0x1000);	//EXTERNAL_CB Confirmation signal.
	else
		return  ((optcon_status_2 & 1) == 1);	//CB4 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned cb5_CLOSED()
{
	return  ((optcon_status_2 & 2) == 2);	//CB5 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned eGen1_CLOSED()
{
	return  ((optcon_status_2 & 0x0100) == 0x0100);	//GENERATOR N1 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned eGen2_CLOSED()
{
	return  ((optcon_status_2 & 0x0200) == 0x0200);	//GENERATOR N2 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned eGen3_CLOSED()
{
#ifdef TEST_FIXTURE
	return  ((gp_configuration_word & 0x4000)!=0);	//FOR DEBUG ONLY!!
#else
	return  ((optcon_status_2 & 0x0400) == 0x0400);	//GENERATOR FORE Confirmation signal.
#endif //TEST_FIXTURE
}
/*---------------------------------------------------------------------------*/
_inline unsigned eGen4_CLOSED()
{
	return  ((optcon_status_2 & 0x0800) == 0x0800);	//GENERATOR SPARE Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned EXTERNAL_CB_CLOSED()
{
	if (SIX_CONTACTOR_CONFIG)
	{
		return  (_getbit(P7,4)||_getbit(P5,9));	//CRN EXTERNAL_CB Confirmation signal.
	}
	else
	{
//		return  ((optcon_status_2 & 0x1000) == 0x1000);	//EXTERNAL_CB Confirmation signal.
		if (OPTCON_PCB)
			return  ((optcon_status_2 & 0x1000) == 0x1000);	//EXTERNAL_CB Confirmation signal.
		else
			return (_getbit (P5, 10));						//Converter Output C/B Confirmation
	}
}
/*---------------------------------------------------------------------------*/
unsigned slaveK2_ON()
{
#ifdef GEN2GEN_CONTROL_OPTION_TEST
	return  ((gp_configuration_word & 8) == 8);	//Slave K2 Confirmation signal.	//FOR DEBUG ONLY!!
#else
	return  ((optcon_status_2 & 8) == 8);	//Slave K2 Confirmation signal.
#endif //GEN2GEN_CONTROL_OPTION_TEST
}
/*---------------------------------------------------------------------------*/
unsigned optconPcbPresent()
{
	return ((optcon_status & 0x8000)==0x8000);	//R30-d15
}
/*---------------------------------------------------------------------------*/
unsigned multipleGensetsOnline()
{
  unsigned moreThanOne=TRUE;
  unsigned genConSigs=(optcon_status_2 & 0x0F00);

	if (MULTIGEN_OPTION)
	{
		if (genConSigs == 0x0100) moreThanOne = FALSE;
		else if (genConSigs == 0x0200) moreThanOne = FALSE;
		else if (genConSigs == 0x0400) moreThanOne = FALSE;
		else if (genConSigs == 0x0800) moreThanOne = FALSE;
		else if (genConSigs == 0) moreThanOne = FALSE;
	}
	else if (HYBRID_STO)
	{
		moreThanOne=FALSE;
	}
	else	//STANDARD 2 GEN SYSTEM
	{
		if (!(gp_state.generator1_contactor_state && gp_state.generator2_contactor_state)) moreThanOne = FALSE;
	}

	return moreThanOne;
}
/*---------------------------------------------------------------------------*/
_inline unsigned gensetOnline()
{
	return  ((optcon_status_2 & 0x0F00) != 0);//at least one GENERATOR Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned GENERATOR_CB_CLOSED()
{
	if (DUAL_GENSET)
	{
		return  (Gen1conf || Gen2conf);	//DUAL GENERATOR_CB Confirmation signals.
	}
	else
	{
		return  (Gen1conf);	//Gen#1, Single GENERATOR_CB Confirmation signal.
	}
}
/*---------------------------------------------------------------------------*/
/* ======================================================== INTERRUPTS/TASKS */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
interrupt(0x02) void		// Non-Maskable Interrupt, power failure.
pwr_fail()
{
	TFR = TFR & 0x7FFF;		//clear TFR bit#15

	LogEvent(Ev_CPU_POWER_CORRUPT,0);

	if (!PARALLELING)
	{

//	LogEvent(Ev_CPU_POWER_CORRUPT,0);
		clear_screen();
		lcd_display("INPUT POWER CORRUPT\nSYSTEM HALTED\n\nPress a key to reset\0",1,1);
		
		if (autorestart == DISABLED){
			while((P8&0xF)==0xF)
			{
				_srvwdt();		   		// wait for any key press
			}
		}
	
#ifndef DEBUG
		init_outputs();	/* set all outputs to nominal */
		_int166(0);			 		// re-boot.
#endif //~DEBUG
	}
}
/*---------------------------------------------------------------------------*/
interrupt(0x04) void		//Stack-Overflow Hardware Trap.
stack_overflow()
{
	init_outputs();				// set all outputs to nominal
	LogEvent(Ev_STACK_OVERFLOW,0);
	TFR = TFR & 0xBFFF;			//clear TFR bit#14 to prevent retriggering on exit.
#ifndef DEBUG
#ifdef AUTO_CLEAR_HW_TRAPS
	_int166(0);			 		// re-boot.
#endif //AUTO_CLEAR_HW_TRAPS
#endif //~DEBUG
}
/*---------------------------------------------------------------------------*/
interrupt(0x06) void		//Stack-Underflow Hardware Trap.
stack_underflow()
{
	init_outputs();				// set all outputs to nominal
//	LogEvent(Ev_STACK_UNDERFLOW,0);
	TFR = TFR & 0xDFFF;			//clear TFR bit#13 to prevent retriggering on exit.
#ifndef DEBUG
#ifdef AUTO_CLEAR_HW_TRAPS
	_int166(0);			 		// re-boot.
#endif //AUTO_CLEAR_HW_TRAPS
#endif //~DEBUG
}
/*---------------------------------------------------------------------------*/
interrupt(0x0A) void		//class_B_trap Hardware Trap.
class_B_trap()
{
  unsigned char trapArg=0;
  unsigned tfr;

	init_outputs();				// set all outputs to nominal
	trapArg = (unsigned char) TFR & 0x000F;			//bits 0-3, stays in place.
//	tfr = TFR & 0x0010;
	tfr = TFR & 0x0080;
	tfr = tfr >> 3;									//bit 7
	trapArg = (unsigned char) tfr | trapArg;		//moved to bit 4.
	tfr = TFR & 0xE000;
	tfr = tfr >> 8;									//bits 13-15
	trapArg = (unsigned char) tfr | trapArg;		//moved to bits 5-7.

	LogEvent(Ev_HARDWARE_TRAP,trapArg);
	TFR = 0;						//clear TFR to prevent retriggering on exit.
#ifndef DEBUG
#ifdef AUTO_CLEAR_HW_TRAPS
	_int166(0);			 		// re-boot.
#endif //AUTO_CLEAR_HW_TRAPS
#endif //~DEBUG
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void Xfer_Impedance_Reset_Ramp (void) {
	unsigned long temp_ul;
	
	if (!Xfer_Imped_Ramp_Status.bits.Init_Calc) {
		if (mach.xfr_dutyCycle > mach.pwm2_dutyCycle)
			Impedance_Rate_Q8 = (mach.xfr_dutyCycle - mach.pwm2_dutyCycle) << 8;
		else {
			Impedance_Rate_Q8 = (mach.pwm2_dutyCycle - mach.xfr_dutyCycle) << 8;
			Xfer_Imped_Ramp_Status.bits.Neg_Slope = 1;
		}
		
		Impedance_Rate_Q8 /= 50;
		
		Impedance_Ramp_Cnt = 0;
		
		_putbit(0,PWMCON0,2);					//stop PWM2 counter.
		
		_putbit(0,PWMCON1,6);		//align edge.
		PP2 = 511;
		
		Xfer_Imped_Ramp_Status.bits.Init_Calc = 1;
//		Debug_Int[2] = 0;
	}
	
	if (!Xfer_Imped_Ramp_Status.bits.Ramp_Comp) {
//		Debug_Int[2]++;
		if (Impedance_Ramp_Cnt < 50) {
			Impedance_Ramp_Cnt++;
			
			if (Xfer_Imped_Ramp_Status.bits.Neg_Slope)
				New_Imped_Duty = mach.xfr_dutyCycle + ((Impedance_Rate_Q8 * Impedance_Ramp_Cnt) >> 8);
			else
				New_Imped_Duty = mach.xfr_dutyCycle - ((Impedance_Rate_Q8 * Impedance_Ramp_Cnt) >> 8);
		}
		else {
			New_Imped_Duty = mach.pwm2_dutyCycle;
			Xfer_Imped_Ramp_Status.bits.Ramp_Comp = 1;
		}
		
		Active_DutyCycle = New_Imped_Duty;
		
		temp_ul = _mulu32(New_Imped_Duty, 512);
			
		_putbit(0,PWMCON0,2);					//stop PWM2 counter.
		
		PW2 = _divu32 (temp_ul, 100);
		if (PW2 >= 512)
			PW2 = 511;

		_putbit(1,PWMCON0,2);					//start PWM2 counter.
	}
}
/*---------------------------------------------------------------------------*/

interrupt(0x3e) using (TIME_RB) void	// T8INT, 10millisecond interrupt.
int10ms()
{
#ifndef MM3
  register unsigned keyin_enable=0x66;	// to avoid EXTR instruction.
  register int i,x;
#endif //~MM3

	_putbit(1,P3,3);	//P3.3 => output high.		//10ms

	ten_ms_IRQ = 1;		//10msec IRQ is active

// ===========================ALARM FLASHER==================================================
	if (!(milliseconds%1000))		//FLASH ALARM indicator ON/OFF @ 1sec PERIODS. 
	{
		flash = flash^1;			//toggle flash msg flag.  Let it free run.
	}

#ifndef MM3
// ==========================LOG STATUS BIT CHANGES=========================================
	for(i=0;i<16;i++)
	{
		if ((gp_status_word & (1<<i)) ^ (last_sw1 & (1<<i)))
		{
			if (gp_status_word & (1<<i)) x = 1;
			else x = 2 ;
			LogEvent(i+1,x);
		}
		if ((gp_status_word2 & (1<<i)) ^ (last_sw2 & (1<<i)))
		{
			if (gp_status_word2 & (1<<i)) x = 1;
			else x = 2 ;
			LogEvent(i+17,x);
		}
		if ((gp_status_word3 & (1<<i)) ^ (last_sw3 & (1<<i)))
		{
			if (gp_status_word3 & (1<<i))
			{
				x = 1;
				if (!i && (gp_status_word & 0x8000)) 	//INPUT_OVLD && Rev.D PCB v2.22
				{
					gp_diagnostic_code=-30;		//INPUT_OVLD
					last_failure_code = 30;
					shutoffId = 7;				//reason input is off, overload.
				}
			}
			else
			{
				x = 2 ;
				if (!i && ((gp_status_word & 0x8000)==0)) //INPUT_OVLD && Rev.C PCB v2.22
				{
					gp_diagnostic_code=-30;		//INPUT_OVLD
					last_failure_code = 30;
					shutoffId = 7;				//reason input is off, overload.
				}
			}
			LogEvent(i+33,x);
		}
	}
	last_sw1 = gp_status_word;
	last_sw2 = gp_status_word2;
	last_sw3 = gp_status_word3;
#endif //~MM3

// ===========================WATCHDOG TIMER UPDATE=========================================
	_srvwdt();					// service C167's internal watchdog timer.

// ===========================ASEA TIME SERVICE=============================================
	wait_10ms++;				// update system clock.
	eventTime++;				//unsigned long eventTime in 10ms units.
	wattTime++;
	milliseconds=milliseconds+10;		// CLOCK -- contiguous hours of operation.

	if (milliseconds==1000)
	{
		milliseconds=0;
		seconds++;
		if (seconds==60)
		{
			minutes++;
			if (minutes==60)
			{
				minutes=0;
				hours++;
				seconds=1;		//compensate clock for loss of 1 second/hour.
				eventTime=eventTime+100;	//compensate eventTime to match clock.
				wattTime=wattTime+100;	//compensate eventTime to match clock.
			}
			else
			{
				seconds=0;
			}
		}
	}
	
	if (MODEL_LC) {
		if ((!(gp_status_word2 & 0x8000)) && (Oil_Level_Timer))					//08082012 DHK
			Oil_Level_Timer--;
			
		if ((gp_status_word2 & 0x0100) && (Moisture_Oil_Timer))
			Moisture_Oil_Timer--;
			
		if (moisture_oil_init_delay)
			moisture_oil_init_delay--;
	}

#ifndef MM3
// ============================KEYBOARD POLL================================================
	if (!(milliseconds%100)) 
	{
		if (new_key && (new_key==last_key))
		{
			if (monitor_started && key_hold_time++>20)
			{
				special_function=YES;

//				if (!MODEL_LC)	//Model 'L' sees 2 keys only.
//				{
					gp_mode=CALIBRATION_MODE;			   //note: MAKE THESE "VOLATILE" !!
					display_function=F3;
					first_time_through=YES;
//				}
				new_key=0;
			}
		}
		else
		{
			last_key = new_key;
			key_hold_time = 0;
		}
	}
// ===========================HIGH-Z & HYBRID_STO===========================================

	if (!SEAMLESS_TRANSFER || HYBRID_STO)
	{
		if (!g2c && (HIGH_Z || EXT_OSC_OPTION))
		{
			if (gp_status_word3 & 0x0010)	// K4_CLOSE_CMD
			{
				if (Gen1conf || Gen2conf)	//if GEN#1 or GEN#2 online.
				{
					if (!transfer_in_progress && (PW2==0x01FF))
					{
						PW2 = 0x01FF;	//100% DUTY
						LogEvent(Ev_HIGH_Z,1);	//HIGH
						
//						debug = 1;
						
						Xfer_Imped_Ramp_Status.bits.Imped_Comp = 0;
						Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 0;

						if (HYBRID_STO)
						{
							u_TIP_signal=transfer_in_progress=TRUE;
						}
					}
				}
				else
				{
					if (transfer_in_progress && (PW2==0x00FF))
					{
//						PW2 = 0x00FF;	//50% DUTY
//						LogEvent(Ev_HIGH_Z,2);	//LOW
						if (HYBRID_STO)
						{
							u_TIP_signal=transfer_in_progress=FALSE;
						}
					}
				}
			}
			else
			{
				if (transfer_in_progress && (PW2==0x00FF))
				{
//					PW2 = 0x00FF;	//50% DUTY
//					LogEvent(Ev_HIGH_Z,2);	//LOW
					if (HYBRID_STO)
					{
						u_TIP_signal=transfer_in_progress=FALSE;
					}
				}
			}
		}
	}
	
// ==============================LCD UPDATE PERIOD==========================================
	if (!(milliseconds%100))
  	{
		update_display = 1;

//		if (!MODEL_LC)	//Model 'L' sees 2 keys only.
//		{
			CC16IC = keyin_enable; 			//enable kybd interrupt.
			CC17IC = keyin_enable; 			//enable kybd interrupt.
			CC18IC = keyin_enable; 			//enable kybd interrupt.
//		}

		CC19IC = keyin_enable; 			//enable kybd interrupt.
		CC20IC = keyin_enable; 			//enable kybd interrupt.
	}//endif (!(milliseconds%100))	//moved from end of interrupt v4.01 during Hybrid STO development

// ===============================UNEXPECTED CB CLOSURE--detection==========================
		if (SEAMLESS_TRANSFER && !transfer_in_progress)
		{
			if (GENERATOR_CB_CLOSED())	//if GEN#1 or GEN#2 online.
			{
				if (GEN2GEN_OPTION)
				{
					if (Gen1conf && cb4_CLOSED())
					{													// CB1+4 closed.
						remote_off_flag=YES;
					}
					else if (Gen1conf && tieBreaker_CLOSED() && cb5_CLOSED())
					{													// CB1+3+5 closed.
						remote_off_flag=YES;
					}
					else if (Gen2conf && cb5_CLOSED())
					{													// CB2+5 closed.
						remote_off_flag=YES;
					}
					else if (Gen2conf && tieBreaker_CLOSED() && cb4_CLOSED())
					{													// CB2+3+4 closed.
						remote_off_flag=YES;
					}
				}
				else if (TIE_CONF_PRESENT)		//v2.83, or better
				{
					if (Gen1conf || tieBreaker_CLOSED())
					{
						remote_off_flag=YES;	//allow gen#2 online if tie open.
					}
				}
				else if (!MULTIGEN_OPTION)
				{
//					if (monitorExtCb && (EXTERNAL_CB_CLOSED()))			//11252014 DHK
						remote_off_flag=YES;
				}
				else if (MULTIGEN_OPTION && gensetOnline())
				{
					if (MULTIGEN_BESECKE)  //M/Y Bounty Hunter  //gp_status_word3 & 0x0004 (high) to configure.
					{
						if (tieBreaker_CLOSED())  //M/Y Bounty Hunter  //gp_status_word3 & 0x0004 (high) to configure.
						{
							if (EXTERNAL_CB_CLOSED())
								remote_off_flag=YES;
						}
						else
						{
							remote_off_flag=NO;
						}
					}
					else
					{
#ifdef WESTPORT50
///v1.92 SIO/WESTPORT50 start
						if (!tieBreaker_CLOSED())	//Tie Open, so, allow Gen#3 or Gen#4
						{
							if (eGen4_CLOSED()) remote_off_flag=NO;
							if (eGen3_CLOSED()) remote_off_flag=NO;
							if (eGen2_CLOSED()) remote_off_flag=NO;
							if (eGen1_CLOSED()) remote_off_flag=YES;
						}
						else
///v1.92 SIO/WESTPORT50 end
#endif //WESTPORT50
							remote_off_flag=YES;
					}
				}
			}//endif (Gen1conf || Gen2conf)	//if GEN#1 or GEN#2 online.
//			else if (!MULTIGEN_OPTION)			//11252014 DHK
//			{
//				if (EXTERNAL_CB_CLOSED())
//					remote_off_flag=YES;
//			}
			else if (MULTIGEN_OPTION && gensetOnline())
			{
				if (MULTIGEN_BESECKE)  //M/Y Bounty Hunter  //gp_status_word3 & 0x0004 (high) to configure.
				{
					if (tieBreaker_CLOSED())  //M/Y Bounty Hunter  //gp_status_word3 & 0x0004 (high) to configure.
					{
						if (EXTERNAL_CB_CLOSED())
							remote_off_flag=YES;
					}
					else
					{
						remote_off_flag=NO;
					}
				}
				else
				{
#ifdef WESTPORT50
///v1.92 SIO/WESTPORT50 start
					if (!tieBreaker_CLOSED())	//Tie Open, so, allow Gen#3 or Gen#4
					{
						if (eGen4_CLOSED()) remote_off_flag=NO;
						if (eGen3_CLOSED()) remote_off_flag=NO;
						if (eGen2_CLOSED()) remote_off_flag=NO;
						if (eGen1_CLOSED()) remote_off_flag=YES;
					}
					else
///v1.92 SIO/WESTPORT50 end
#endif //WESTPORT50
						remote_off_flag=YES;
				}
			}//endelse if (MULTIGEN_OPTION && gensetOnline())

// ===============================UNEXPECTED CB CLOSURE--handler============================
			if (remote_off_flag)
			{			
				if ((GP_CONTROL_WORD & 2) == 2)
				{
					LogEvent(Ev_REM_OUT_OFF,2);
				}

				GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFFD;		//clear REM_OUT_OFF in image.
				gp_control_word = GP_CONTROL_WORD;	   			//write GP_CONTROL_WORD.

				if (AEM_OPTION)
				{
					if (gp_status_word & 0x0040) 	// OUTPUT_ENABLE.
					{
						GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0080; //set GEN_1_CB_CLOSE in image.
						gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
					}
				}

				if (EXT_CB_OPTION)
				{
					gp_state.invstat=OFFLINE;
					gp_state.output_contactor_state=OFF;
					gp_state.converter_power_state=OFF;
				}
				else								//07312012 DHK Added from v2.98
				{
					remote_off_flag=NO;
				}

			}//endif (remote_off_flag)
		}//endif (SEAMLESS_TRANSFER && !transfer_in_progress)
		else if (!SEAMLESS_TRANSFER) {			//Added 06062017 for V9.06
			if (remote_off_flag) {
				if ((GP_CONTROL_WORD & 2) == 2)
				{
					LogEvent(Ev_REM_OUT_OFF,2);
				}

				GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFFD;		//clear REM_OUT_OFF in image.
				gp_control_word = GP_CONTROL_WORD;	   			//write GP_CONTROL_WORD.

				if (EXT_CB_OPTION)
				{			
					gp_state.invstat=OFFLINE;
					gp_state.output_contactor_state=OFF;
					gp_state.converter_power_state=OFF;
				}
				else {
					remote_off_flag=NO;
				}
			}
		}					
		
//	}//endif (!(milliseconds%100))
#endif //~MM3

	if ((Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down) && (!Xfer_Imped_Ramp_Status.bits.Imped_Comp)) {
		Xfer_Impedance_Reset_Ramp();
		
		if (Xfer_Imped_Ramp_Status.bits.Ramp_Comp) {
			LogEvent(Ev_HIGH_Z, 2);	//LOW
//			debug2 = 1;
			Xfer_Imped_Ramp_Status.all = 0;	
			Xfer_Imped_Ramp_Status.bits.Imped_Comp = 1;
		}
	}
	
	if (XFMR_Tap_Delay_Rd)
		XFMR_Tap_Delay_Rd--;
		
	ten_ms_IRQ = 0;


	_putbit(0,P3,3);	//P3.3 => Port P3A-3 => output low.		//10ms
}
/* ======================================================================================= FUNCTION DEFINITIONS */
/* ======================================================================================= FUNCTION DEFINITIONS */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void initialize_gp_state()	// initialize gp_state structure in RAM.
{
	monitor_started=NO;
	input_dropped=NO;

	u_TIP_signal=transfer_in_progress=NO;
	
	virtualTransferRequest=NO;	//Modbus Virtual REM_XFR_REQ

	remoteActive=NO;

	c2g=NO;
	g2c=NO;
	remote_off_flag=NO;

	first_time_through=NO;
	wait_10ms=0;
	sync_mux_id=1;
	daq_mux_id=2;
	special_function=NO;
	gp_mode=1;
	combo_key=86;
	new_key=NO;	
	last_key==18; //dummy SYSTEM_STATUS keypress.
	key_hold_time=0;
	display_type=4;
	display_function=0;
	abort_power_on=NO;
	Lock=NO;
	undetected_sync_signal_source=-1;

	ship_name=SHIP_NAME;

	gp_state.cord_fault					=0;		// {OK,NOT_OK}.	//NOT_OK==CORD_FAULT.

	gp_state.sysstat					=0;		// {OK,WARNING,FAILURE}.
	gp_state.inpstat1					=0;		// {OFFLINE,ONLINE,FAULT,UNDER_VOLTAGE,OVER_VOLTAGE}.
	gp_state.inpstat2					=0;		// {OFFLINE,ONLINE,FAULT,UNDER_VOLTAGE,OVER_VOLTAGE}.
	gp_state.invstat					=0;		// {OFFLINE,ONLINE,FAULT,UNDER_VOLTAGE,OVER_VOLTAGE}.
	gp_state.inv2stat					=0;		// {OFFLINE,ONLINE}.
	gp_state.genstat1					=1;		// {OFFLINE,ONLINE,FAULT,UNDER_VOLTAGE,OVER_VOLTAGE}.
	gp_state.genstat2					=1;		// {OFFLINE,ONLINE,FAULT,UNDER_VOLTAGE,OVER_VOLTAGE}.
	gp_state.gp_diagnostic_code			=0;		// -n < gp_diagnostic_code < -m
	gp_state.input_sync_mux_id			=1;		// {NONE,SP1A_sync,SP1B_sync,SP2A_sync,SP2B_sync}.
	gp_state.input1_form				=3;		// {INVALID,ONE_PHASE,TWO_PHASE,THREE_PHASE}.    
	gp_state.input2_form				=3;		// {INVALID,ONE_PHASE,TWO_PHASE,THREE_PHASE}.    
	gp_state.scr_mode_id				=0;		// {NONE,ABC,BCA,CAB,ACB,BAC,CBA}.
	gp_state.range_mode_id				=0;		// {NONE,LOW_RANGE,HIGH_RANGE}.
	gp_state.pfc_state					=0;		// {DISABLED,ENABLED}.
	gp_state.converter_power_state		=0;		// {OFF,ON}.
	gp_state.output_contactor_state		=0;		// {DISABLED,ENABLED}.
	gp_state.generator1_contactor_state	=1;		// {OFF,ON}.
	gp_state.generator2_contactor_state	=1;		// {OFF,ON}.
	gp_state.aemGenMatchCmd				=0;		// {OFF,ON}.
	gp_state.input_overcurrent			=0;		// {OK,NOT_OK}.	//NOT_OK==INPUT_OVERCURRENT.
	gp_state.hardware_current_limit		=0;		// {OK,NOT_OK}.	//NOT_OK==CURRENT_LIMITING.
	gp_state.pfc_fault					=0;		// {OK,NOT_OK}.	//NOT_OK==PFC_FAULT.
	gp_state.lvdc_fault					=0;		// {OK,NOT_OK}.	//NOT_OK==LVDC_FAULT.
	gp_state.hvdc_fault					=0;		// {OK,NOT_OK}.	//NOT_OK==HVDC>210.
	gp_state.inverter_fault				=0;		// {OK,NOT_OK}.	//NOT_OK==INVERTER_FAULT.
	gp_state.neg_hvdc_overvoltage		=0;		// {OK,NOT_OK}.	//NOT_OK==NEGATIVE_HVDC_OVERVOLTAGE.
	gp_state.pos_hvdc_overvoltage		=0;		// {OK,NOT_OK}.	//NOT_OK==POSITIVE_HVDC_OVERVOLTAGE.
	gp_state.system_overtemperature		=0;		// {OK,NOT_OK}.	//NOT_OK==SYSTEM_OVERTEMPERATURE.
	gp_state.shutdown					=0;		// {OK,NOT_OK}.	//NOT_OK==SHUTDOWN.
	gp_state.gp_mode					=1;		// {undefined,NORMAL_MODE,TEST_MODE,CALIBRATION_MODE}.
	gp_state.display_type				=4;		// {SHORE_POWER_1,SHORE_POWER_2,GENERATOR_POWER,CONVERTER_POWER,SYSTEM_STATUS,HELP}
	gp_state.display_function			=0;		// {F1,F2,F3,HELP}.
	gp_state.active_display				=0;		// {OTHER,GEN1,GEN2}
	gp_state.shore_power_viewed			=0;		// {FALSE,TRUE}.
	gp_state.default_program			=def_pgm;	// default osc. output configuration.
	gp_state.tieBreaker_CB_state		=1;		// {OFF,ON}.
	gp_state.oil_level					=1;		// {LOW,NORMAL}.	//LOW==OIL_LEVEL_LOW.
}
/*---------------------------------------------------------------------------*/
void init_ram()	/* initialize RAM if BATT failed */
{
  int i=0;

	lastDuty = 202;	//used to calibrate NEW_G2G_OPTION speed-trim Null.

	amps2volts = AMPS_VOLTS;
	volts2volts = VINT_VOLTS;

	sys_config(GP_SERIES);
	
//	strcpy(gp_state.gp_cid, gp_cid);


	ts = 0;	//transfer via serial flag.

	if (PARALLELING) 
	{
//		if (gp_state.local_master==1)	//force ram reset if changed!
//		{
//			bat_ok[0] = ' ';
//		}
		gp_state.local_master = 0;
	}
	else
	{
//		if (gp_state.local_master==0)	//force ram reset if changed!
//		{
//			bat_ok[0] = ' ';
//		}
		gp_state.local_master = 1;
	}

#ifndef MM3
	if (LAST_configuration_word != gp_configuration_word)
	{
		newSlewRate = 0.5;
		newFrequency = GP_OUTPUT_FREQUENCY;
		newVoltage = GP_OUTPUT_VOLTAGE;

		initialize_program_ram();
	 
		tempSlewRate = newSlewRate;
		tempFrequency = newFrequency;
		tempVoltage = newVoltage;

		set_Vr_ILMr();

		init_transient_ram();
	}
#endif //MM3

	LAST_configuration_word = gp_configuration_word;
	LAST_configuration_word2 = gp_configuration_word2;


	i=0;
	while(bat_ok[i] == IDN[i])
		if(IDN[i++] == '\0')
			return;			/* battery back up ram test */
	i=0;
	while(bat_ok[i] = IDN[i])
		i++;

// 
// THE FOLLOWING VARIABLES ARE IN BATTERY-BACKED SRAM.  Initial values assigned on REBOOT REBOOT only.
// 
	LmShedOnLevel	=95.0;	//Load Mgmt defaults
	LmShedOffLevel	=85.0;
	LmAddOffLevel	=80.0;
	LmAddOnLevel	=60.0;
	LmShedOnDelay	=200;
	LmShedOffDelay	=200;
	LmAddOnDelay	=200;
	LmAddOffDelay	=200;

	
	initializeTimers();						//clear all timers.

	LmAddOffTimer = getTimer();		//declare & allocate timer
	LmShedOffTimer = getTimer();	//declare & allocate timer
	LmShedOnTimer = getTimer();		//declare & allocate timer
	LmAddOnTimer = getTimer();		//declare & allocate timer

	negCrossover=FALSE;	//Default for early GenCB open MULTIGEN_BESECKE

//used for softStartGuard()
	ssTimer = getTimer();			//declare ssTimer.
	inhibitOnTimer = getTimer();	//declare inhibitOnTimer.

//#ifdef MODBUS
	initializeModbusRAM();
	modbusNodeId = modbusNodeId+PARALLELING;	//Master=3, Slave=4
//#endif //MODBUS

	autoShutdownEnable=DISABLED;	//M/Y Bounty Hunter feature

//speed-trim programmatic tweak-terms
	actuatorResponseDelay=100;			//100ms between pulses or delay for response in gen freq.
//	actuatorResponseDelay=20;			//20ms between pulses or delay for response in gen freq.

	retryCount=0;			//autorestart retry count
	shutoffId = 4;			//reason input is off, initialize to Red-Switch

	maintenanceMode = NO;
	
	maxSystemLevel = 0.0;
	maxSystemPower = 0.0;
	maxGenFreq = 0.0;
	minGenFreq = 60.0;

	maxLevelIn1 = 0.0;
	maxPowerIn1 = 0.0;

	eventTime = 0;				//unsigned long eventTime in 10ms units.
	wattTime = 0;
	lastWattTime = 1000;	//unsigned long wattTime+10sec.s in 10ms units.
	wattHours = 0;
	wattHourMeterON = ON;

	milliseconds = 0;
	seconds = 0;
	minutes = 0;
	hours = 0;

	initializeEventLog();	//Event Log initialization--call once each full memory reset.
	initializeEvMaskAll();
	startEventLog();

	remoteEvLogMode = 0;	//Event Log remote MODE default is TEXT=0.

	newSlewRate = 0.5;
	newFrequency = GP_OUTPUT_FREQUENCY;
	newVoltage = GP_OUTPUT_VOLTAGE;
	
	minGenFreq = GP_OUTPUT_FREQUENCY;

///new logic for setting reset value of shoreCordRating (v2.36)
	shoreCordRating	=250;  	// {30 to 250 amps}. //set during sysConfig() on each IPL

	if ((RATED_POWER >= 75.0) || AC75_CONTROL_PCB)
	{
		if (IN_RATED_AMPS_LO_ONE < shoreCordRating)
		{
			shoreCordRating = IN_RATED_AMPS_LO_ONE;			//worst case: 3-phase, Low Range.
		}
	}
	else
	{
		if ((IN_RATED_AMPS_LO_ONE * 1.5) < shoreCordRating)
		{
			shoreCordRating = IN_RATED_AMPS_LO_ONE * 1.5;	//worst case: 1-phase, Low Range.
		}
	}

	alarmLevel	   	=100;	// {50 to 100% of cordRating}.
	alarmState	   	=0;		// {DISABLED,ENABLED}.
	droop			=5;
	droopEnable		=DISABLED;
	droopDampTime	=180000;	//30 minutes in eventTime units (10ms).

	autoTransferOnOverload =DISABLED;
	autoTransferGenset =GEN1;

///
	mach.pwm1_state = ENABLED;		//50% duty=DISABLED.
	mach.pwm1_bits = 9;				//9bit (9bit,edge=>39.065KHz).
	mach.pwm1_align = 0;			//EDGE aligned == 0.
	mach.pwm1_dutyCycle = 50;		//100% initial duty cycle.

	mach.xfr_dutyCycle = 100;		//100% duty cycle during transfers.
	mach.pwm2_state = ENABLED;		//50% duty=DISABLED.
	mach.pwm2_bits = 9;				//9bit (9bit,edge=>39.065KHz).
	mach.pwm2_align = 0;			//EDGE aligned == 0.
	mach.pwm2_dutyCycle = 50;		//100% initial duty cycle.

	mach.pwm3_state = ENABLED;		//50% duty=DISABLED.
	mach.pwm3_bits = 9;				//9bit (9bit,edge=>39.065KHz).
	mach.pwm3_align = 0;			//EDGE aligned == 0.
	mach.pwm3_dutyCycle = 50;		//100% initial duty cycle.

///
//	gen1DutyCycle = 39;		//3.2Vdc?
//	gen2DutyCycle = 39;		//3.2Vdc?
//	lastDuty = 39;	//used to calibrate NEW_G2G_OPTION speed-trim Null.

	gen1DutyCycle = 202;		//3.2Vdc?
	gen2DutyCycle = 202;		//3.2Vdc?
	lastDuty = 202;	//used to calibrate NEW_G2G_OPTION speed-trim Null.
///
	if (gp_state.local_master==SLAVE)
	{
		if (gp_state.vout_range == LOW_RANGE)
		{
			mach.pwm2_dutyCycle = 30;		//50% initial duty cycle. Assume Low Range input.
		}
		else
		{
			mach.pwm2_dutyCycle = 5;		//50% initial duty cycle. Assume Low Range input.
		}
	}
	else
	{
		mach.pwm2_dutyCycle = 50;		//100% initial duty cycle.
	}

	lastMasterCapacity = RATED_POWER;
	remote_genset = GEN1;

//TECHNEL_OPTION or GEN_START_OPTION
	generator_on_delay = 60;		//default to 60 second delay after GEN_ON_CMD issued. v2.00

//#ifdef DHK_12012014
	if (GEN_START_OPTION)
	{
		gen_start_cmd_enable = ENABLED;	//default to enabled.
	}
	else //	if (NEW_G2G_OPTION) .OR. not configured.
	{
		gen_start_cmd_enable = DISABLED;	//default to disabled.
	}
//#endif

	inhibit_transfer = ENABLED;		//default to enabled.
//END TECHNEL_OPTION

//NEW_G2G_OPTION
	generator_off_delay = 60;		//default to 60 second delay after GENx_OFF_CMD issued. v2.10
	gen_stop_cmd_enable = DISABLED;	//default to disabled.
//END NEW_G2G_OPTION

#ifdef GEN2GEN_CONTROL_OPTION_TEST	//GEN2GEN_OPTION = TRUE
	generator_on_delay = 10;		//default to 60 second delay after GEN_ON_CMD issued. v2.00
	generator_off_delay = 10;		//default to 60 second delay after GENx_OFF_CMD issued. v2.10
#endif //GEN2GEN_CONTROL_OPTION_TEST

//TIP_OPTION
	tip_delay = 1;
	tip_cmd_enable = ENABLED;
	u_TIP_signal = -1;
//END TIP_OPTION

	if (config_no_crossover_delay())
	{
		generator_transfer_delay = 600;
	}
	else
	{
//		generator_transfer_delay = 1600;		//default to 1 second delay after GEN_CONF received.
		generator_transfer_delay = 200;		//default to 200ms delay after GEN_CONF received.
	}

	transfer_timeout = 2000;

	generator_transfer_rolloff = 100;	//default to 100 T5 counts
//	generator_transfer_rolloff = 200;	//default to 200 T5 counts T029 proven, v2.29

	resetDelay = 200;			//default to 600 milliseconds.
	resetCbOn = DISABLED;
	
	external_cb_delay = 700;			//12112014 DHK: changed to 700ms;		200;	//default was:600 ms; is:200ms, v2.90.
	external_cb_Open_delay = 700;		//12112014 DHK: changed to 700ms;		3000;	//default to 3seconds as per PK, v2.90.

#ifdef WESTPORT50
	monitorExtCb = ENABLED;
#else
	if ((EXT_CB_OPTION) && (gp_state.local_master == MASTER))
		monitorExtCb = ENABLED;					
	else
		monitorExtCb = DISABLED;
#endif //WESTPORT50

	autorestart = DISABLED;
	last_autorestart = DISABLED;

	AUTO_TRANSFER = DISABLED;
	auto_transfer_enable = DISABLED;		

	last_transfer_status = (unsigned char) 86;
	active_status_code = OK;
	last_failure_code = OK;

	initialize_waveform_ram();

	initialize_metering_ram();

	initialize_program_ram();
 
	tempSlewRate = newSlewRate;
	tempFrequency = newFrequency;
	tempVoltage = newVoltage;


	if (EXT_OSC_OPTION)
	{
		AGC_state=OFF;	//v1.99
	}
	else
	{
		AGC_state=ON;	//v1.99
	}
	ALC_mode=AGC_state;	//used for 3-phase systems as of v1.99

	set_Vr_ILMr();

	init_transient_ram();

///////////////////////////////////////////////////////////////////////////
//	initialize_remote_interface_ram();
	INTERFACE=1;					/* default to RS-232 */
#ifdef GPIB
	INTERFACE=0;					/* select GPIB bus */
#endif
	IE_dev_address=1;				/* default Device address */
	ser_parity=0;					/* default no parity */
	eos_mode=0;						/* default xmtd eos CR/LF */
	ser_baud_rate=19200;			/* default baud rate for rs-232. */
///////////////////////////////////////////////////////////////////////////

    RAM_reset = 1;           //flag to indicate RAM has been reset.
}
/*---------------------------------------------------------------------------*/
void init_PECCs()
{
	PECC0 = 0x4ff;		/* move data from waveform table to pha wf dac */
	SRCP0 = wfp_a;
	DSTP0 = (int)&pha_wf_dac;

	PECC1 = 0x4ff;		/* move data from waveform table to phb wf dac */
	SRCP1 = wfp_b;
	DSTP1 = (int)&phb_wf_dac;

	PECC2 = 0x4ff;		/* move data from waveform table to phc wf dac */
	SRCP2 = wfp_c;
	DSTP2 = (int)&phc_wf_dac;

	PECC3 = 0xff;		/* pec3 reloads pec0 source pointer */
	SRCP3 = (int)&wfp_a;		/* to begining of pha waveform */
	DSTP3 = (int)&SRCP0;		/* at pha cycle reset */

	PECC4 = 0xff;		/* pec4 reloads pec1 source pointer */
	SRCP4 = (int)&wfp_b;		/* to begining of phb waveform */
	DSTP4 = (int)&SRCP1;		/* at phb cycle reset */

	PECC5 = 0xff;		/* pec5 reloads pec2 source pointer */
	SRCP5 = (int)&wfp_c;		/* to begining of phc waveform */
	DSTP5 = (int)&SRCP2;		/* at phc cycle reset */

	PECC6 = 0x0ff;		/* pec6 reloads pp0 count */
	SRCP6 = (int)&pwm_s;
	DSTP6 = (int)&PP0;

	PECC7 = 0x0ff;		/* pec7 reloads pp0 count */
	SRCP7 = (int)&pwm_f;
	DSTP7 = (int)&PP0;
}
/*---------------------------------------------------------------------------*/
void init_hw()	/* initialization of hardware at every reset */
{
// ################## BUSCON ########################################################
//BUSCON's  already set in cstart.asm, so, this is redundant.
//	BUSCON2 = 0xC4AE;       //RD for 16-bit ADC and WR for 12-bit DAC's
//	BUSCON2 = 0xC4AC;       //RD for 16-bit ADC and WR for 12-bit DAC's		//06172015 DHK changed to 2 waits from 1 wait
//	BUSCON3 = 0x04AE;		//1 wait state.

//	BUSCON2 = 0x84AE;       //WR for 12-bit DAC's, 1 wait state.
//    BUSCON3 = 0xC4AE;		//RD/WR '244/'374 MEMORY MAPPED DEVICES, 1 wait state.
//    BUSCON3 = 0x068C;		//RD/WR '244/'374 MEMORY MAPPED DEVICES, 2 wait state.
// ################## BUSCON ########################################################

	pwm_f = PWM_C_MIN;	/* internal ram init at every reset */
	pwm_s = PWM_C_MIN;
	PP0 = PWM_C_MIN;
	PW0 = PWM_C_MIN>>1;	/* required width */

	PP1 = 511;		//PWM2 mode 0=edge align, 39.065KHz
	PW1 = 256;		//PWM2 50% duty cycle
	PT1 = 0;

	PP2 = 511;		//PWM2 mode 0=edge align, 39.065KHz
	PW2 = 256;		//PWM2 50% duty cycle
	PT2 = 0;


	PP3 = 511;		//PWM3 mode 1=center align, 39.065KHz
	PW3 = 256;		//PWM3 50% duty cycle
	PT3 = 0;

//	PP3 = 255;		//PWM3 mode 1=center align, 39.065KHz
//	PW3 = 128;		//PWM3 50% duty cycle
//	PT3 = 0;

	wfp_a = (int) &osc_wf;	/* internal ram init at every reset */
	wfp_b = (int) &osc_wf;
	wfp_c = (int) &osc_wf;

	init_PECCs();

	_putbit(0,DP3,6);		//P3.6 input

//	DP2 = 0x1dff;		// output p2.0...12 LCD, input p2.9,p2.13...15 interrupts.
//	DP2 = 0x00FF;		//P2.0-7 LCD output, P2.8-15 input, esp. P2-11 AUX_INT input. //v1.84

//	if (MODEL_LC)	//Model 'L' has 12 LEDs w/No LCD.
//	{
//#ifdef MM3
//		DP2 = 0x3300;		//P2.0-7 Inputs for DMC Config., P2-11 AUX_INT input.
//		DP4 = 0x000E;		//P4.4-7 Inputs for DMC Config.
//#else
////		DP2 = 0x15FF;		//P2.0-7 LED output, P2-11 AUX_INT input.
//		DP2 = 0x11FF;		//P2.0-7 LED output, P2-11 AUX_INT input.
//		DP4 = 0x00FE;		//Enable LED control lines as outputs.
//#endif //~MM3
//	}
//	else
//	{
//		DP2 = 0x15FF;		//P2.0-7 LCD output, P2.8-15 input, esp. P2-11 AUX_INT input. //v1.84
		DP2 = 0x11FF;		//P2.0-7 LCD output, P2.8-15 input, esp. P2-11 AUX_INT input. //v1.84
		DP4 = 0x00F0;		//Enable LCD control lines
//	}

#ifdef MM3
		DP2 = 0x3300;		//P2.0-7 Inputs for DMC Config., P2-11 AUX_INT input.
		DP4 = 0x000E;		//P4.4-7 Inputs for DMC Config.
#endif

	DP6 = 0x80;			/* enable transient pedstal output */
	DP7 = 0x0F;			/* PWM and keyboard outputs */

	DP8=0x00;			//ALL INPUTS

	_putbit(1, P6, 7);	/* tran ped low */

// ################## P3 ######################################################################

	_putbit(0,P3,1);	//P3.2 => Port P3A-1 =>  ALARM low
	_nop();

	_putbit(1,P3,2);	//KEYPAD ROW #1 HIGH, allow (not enable) interrupt
	_nop();

	_putbit(0,P3,3);	//P3.3 => Port P3A-3 => output low.
	_nop();

	_putbit(0,P3,5);	//P3.5 => Port P3A-5 => output low.		//out_sync
	_nop();

	_putbit(1,P3,7);	//KEYPAD ROW #2 HIGH, allow (not enable) interrupt
	_nop();

	_putbit(0,P3,8);	//P3.8 => Port P3A-8 => output low.		//adc_read
//	_putbit(1,P3,8);	//P3.8 => Port P3A-8 => output high.	//adc_read
	_nop();

	_putbit(0,P3,9);	//P3.9 => Port P3A-5 => output low.		//display_manager
//	_putbit(1,P3,9);	//P3.9 => Port P3A-5 => output high.	//display_manager
	_nop();

	_putbit(0,P3,13);	//P3.13 => Port P3A-9 => output low.	//meas_all
//	_putbit(1,P3,13);	//P3.13 => Port P3A-9 => output high.	//meas_all
	_nop();

	_putbit(0,P3,15);	//P3.15 => Port P3A-7 => output low.	//SPARE					//sp1_sync
//	_putbit(1,P3,15);	//P3.15 => Port P3A-7 => output high.	//sp1_sync
// ################## P3 ######################################################################

	T6CON =	0x000A;				//GPT2 TIMER 6 CONTROL REGISTER, COUNTER MODE.
	T5CON =	0x0003;				//GPT2 TIMER 5 CONTROL REGISTER, TIMER MODE.
	T5 =	0;					//CLEAR TIMER. 	//GPT2 TIMER 5 REGISTER.
//	T6=0xffff;        			// RELOAD T6
//	T6IC = 0x37;				//line sync interrupt priority
//	T6IC = 0x36;				//line sync interrupt priority
	_putbit(1, T5CON, 6);		// START T5.
	wdt=0;
	flash=0;

	PWMCON0 = 0x010D;	/* run pwm0 & pwm2 & pwm3 and enable interrupt pwm0 */
	PWMCON1 = 0x008D;	/* enable output for pwm0 mode0 & pwm2 mode0 & pwm3 mode1*/
//	PWMCON0 = 0x101;	/* run and enable interrupt pwm0 */
//	PWMCON1 = 0x1;		/* enable pwm0 out, mode0 */

	T0 = 0x0fff0;		/* start near max count */
	T0REL = (65536 - WF_STEPS);	/* reload reg with number of steps in waveform */

	T1REL = 65286;		/* T1 100microsecond interrupt */
	T01CON = 0x4049;	/* T0 counter pos edge T0in=P3.0, T1 move tran step, 100uS */

//	T8REL = 3036;		/* 100millisecond interrupt */
	T7REL = (65536-(WF_STEPS/ADC_SAMPLES));	/* metering samples */
	T78CON = 0x420a;	/* T8 int10ms timer, T7 counter neg edge T7in=P2.15 */
	T8REL = 59286;		/* 10millisecond interrupt */

//	T7 = 0x0fff0;		/* start near max count */
//	T7REL = (65536 - WF_STEPS);	/* reload reg with number of steps in waveform */
//	T78CON = 0x400a;	/* T8 int10ms timer, T7 counter neg edge T7in=P2.15 */
//	_putbit(1, T78CON, 6);			/* start T7 */

	CC2 = 0xffff;		/* to make pha cycle reset at 0 deg */
	CC3 = (65535-((WF_STEPS*2)/3));	/* to make phb cycle reset at 120 deg */
	CC4 = (65535-(WF_STEPS/3));	/* to make phc cycle reset at 240 deg */
	CC5 = 0xffff;		/* start metering at pha 0 deg */
	CC6 = 0xffff;		/* cycle_based transient at 0 deg */
	CC12 = (65536-(WF_STEPS/2));	/* reset sync out */

	CCM0 = 0x6666;		/* cc0-3, int only */
	CCM1 = 0x6666;		/* cc4-7, int only */

//	CCM2 = 0x0010;		/* cc8-11, int only */	//cc9 +edge capture, used for sync interrupt.
//	CCM2 = 0x0020;		/* cc8-11, int only */	//cc9 -edge capture, used for sync interrupt.
	CCM2 = 0x1010;		/* cc8-11, int only */	//cc9 +edge capture, CC11 +edge capture. //v1.84

	CCM3 = 0x2227;		/* cc15 & cc14 int - edge, cc13 int IEEE, cc12 sync out */
	CCM4 = 0x2222;		/* cc16 to cc19 int - edge kybd */
//	CCM5 = 0x2222;		/* cc20 to cc23 int - edge kybd */
	CCM5 = 0x1112;		/* cc20 to cc23 int - edge kybd */ //cc21, cc22, & cc23 +edge capture, used for sync interrupt.
	CCM7 = 0x1111;		/* cc28 to cc31 int - edge kybd */ //cc29 +edge capture, used for sync interrupt.

	CC2IC =		0x7b;	/* reload pec 0 */
	T8IC =		0x6f;	/* 10mS interrupt, watchdog timer */

	T0IC =		0x7f;	/* pwm count fast */
	CC12IC =	0x7f;	/* pwm count fast */
	CC0IC =		0x7e;	/* pwm count slow */
	CC1IC =		0x7e;	/* pwm count slow */
	PWMIC =		0x78;	/* move pha osc */
//	T7IC =		0x77;	/* move metering data */

	CC4IC =		0x7d;	/* reload pec 2 */
	CC15IC =	0x7a;	/* move phc osc */
#ifndef SPLIT_PHASE
	CC3IC =		0x7c;	/* reload pec 1 */
	CC14IC =	0x79;	/* move phb osc */
#endif // SPLIT_PHASE

	if (shore_power_off_key)
	{
		CC11IC = 0x0067;	//enable shore_power_off() interrupt, CC11. //v1.98
	}
#ifndef MM3
	init_PWM1(mach.pwm1_bits,mach.pwm1_align,mach.pwm1_dutyCycle);		//unsigned(bits,align,duty)
#endif
	init_PWM2(mach.pwm2_bits,mach.pwm2_align,mach.pwm2_dutyCycle);		//unsigned(bits,align,duty)
	LogEvent(Ev_HIGH_Z,2);	//LOW
	init_PWM3(mach.pwm3_bits,mach.pwm3_align,mach.pwm3_dutyCycle);		//unsigned(bits,align,duty)
}
/*---------------------------------------------------------------------------*/
void gp_reset()
{
		clear_screen();
		lcd_display("REBOOT ... REBOOT ... REBOOT ... REBOOT\0",2,1);
#ifdef DEBUG
		init_outputs();			// set all outputs to nominal.
		init_ram();				// do ram test and initalize if needed.
		initialize_gp_state();	// initialize gp_state structure in RAM.
		init_hw();				// initialize hardware.
		init_lcd();				// initialize display.
		special_function=0;
		combo_key=86;
		pgm_edit=def_pgm;

		if (gp_state.vout_range==HIGH_RANGE) 
		{
			pgm_edit.Va = newVoltage * 0.5;
			pgm_edit.Vb = newVoltage * 0.5;
			pgm_edit.Vc = newVoltage * 0.5;
		}
		else
		{
			pgm_edit.Va = newVoltage;
			pgm_edit.Vb = newVoltage;
			pgm_edit.Vc = newVoltage;
		}

		exec_osc_pgm();
		_putbit(1, PSW, 11);	// enable all interrupts.
#else
		init_outputs();	/* set all outputs to nominal */
		_int166(0);			 		// re-boot.
#endif //~DEBUG
}
/*--------------------------------------------------------------------------*/
int meter_mux_on_display(int display_meter)
{
#ifdef TRACE_ON
	int trace_code=5;
	enqueue(trace_queue, &trace_code); 		//circular trace queue.
#endif //TRACE_ON

	switch (display_meter)
	{
		case SHORE_POWER_1://SP1
							set_meter_mux(SP1, SP1A_sync);
							break;

		case SHORE_POWER_2://SP2
							set_meter_mux(SP1, SP1A_sync);
							break;

		case GENERATOR_POWER://GENERATOR
							set_meter_mux(GEN, GEN_sync);
							break;

		case CONVERTER_POWER://CONVERTER
							set_meter_mux(OSC, OSC_sync);
							break;

		case SYSTEM_POWER://GEN2
							set_meter_mux(SYS, SYS_sync);
							break;

		case SYSTEM_STATUS://SYSTEM STATUS

		default://system status display (or other), meter all.
							meter_mux_iterator();
							break;
	}
	return 0;
}
/*--------------------------------------------------------------------------*/
int meter_mux_iterator()
{
  unsigned daq_id =	get_next_daq_mux_id(daq_mux_id);
  unsigned sync_id=	get_sync_mux_id(daq_id);

#ifdef TRACE_ON
	int trace_code=4;
	enqueue(trace_queue, &trace_code); 		//circular trace queue.
#endif //TRACE_ON

	set_meter_mux(daq_id, sync_id);

	return 0;
}
/*--------------------------------------------------------------------------*/
unsigned get_next_daq_mux_id(unsigned active_daq_mux_id)	//iterate metering selection.
{
  unsigned next_daq_mux_id=active_daq_mux_id;

#ifdef TRACE_ON
	int trace_code=6;
	enqueue(trace_queue, &trace_code); 		//circular trace queue.
#endif //TRACE_ON

	switch (active_daq_mux_id)	//SP1->[SP2]->GEN->[OSC]->[SYS]->SP1
	{
		case OSC://internal
				if (DUAL_GENSET==NO)
				{
					next_daq_mux_id = SP1; 
				}
				else
				{
					next_daq_mux_id = SYS; 
				}
				break;

		case EXT://external
				next_daq_mux_id = SP1;
				break;

		case SP1://shore power #1
				if (!SEAMLESS_TRANSFER || EXT_OSC_OPTION)	//v2.56, S3-6 configuration
				{
					next_daq_mux_id = OSC;
				}
				else
				{
					next_daq_mux_id = GEN;
				}
				break;

		case SP2://shore power #2
//				next_daq_mux_id = OSC;
				next_daq_mux_id = GEN;
				break;

		case GEN://generator power
//			   	if (gp_state.invstat==ONLINE)
//				{
					next_daq_mux_id = OSC;
//				}
//				else
//				{
//					next_daq_mux_id = SP1;
//				}
				break;

		case SYS://system power (parallel)
				next_daq_mux_id = SP1;
				break;

		case AUX://aux in
				next_daq_mux_id = SP1;
				break;

		case XXX://spare
				next_daq_mux_id = SP1;
				break;

		default://error condition
				next_daq_mux_id = SP1;
//				gp_diagnostic_code = -30;
				break;
	}
	return next_daq_mux_id;
}
/*--------------------------------------------------------------------------*/
unsigned get_sync_mux_id(unsigned daq_id)	//match sync signal to active metering selection.
{
  unsigned new_sync_mux_id=sync_mux_id;		//default to same sync signal

#ifdef TRACE_ON
	int trace_code=7;
	enqueue(trace_queue, &trace_code); 		//circular trace queue.
#endif //TRACE_ON

	switch (daq_id)
	{
		case OSC://internal
				new_sync_mux_id = OSC_sync;
				break;

		case EXT://external
				new_sync_mux_id = OSC_sync;
				break;

		case SP1://shore power #1
//				new_sync_mux_id = gp_state.input_sync_mux_id;
				new_sync_mux_id = SP1A_sync;
				break;

		case SP2://shore power #2
				new_sync_mux_id = SP1A_sync;
				break;

		case GEN://generator power
//				new_sync_mux_id = OSC_sync;
				new_sync_mux_id = GEN_sync;
				break;

		case SYS://system power (parallel)
				new_sync_mux_id = SYS_sync;
				break;

		case AUX://aux in
				new_sync_mux_id = OSC_sync;
				break;

		case XXX://spare
				new_sync_mux_id = OSC_sync;
				break;

		default://error condition
//				gp_diagnostic_code = -31;
				break;
	}
	return new_sync_mux_id;
}
/*---------------------------------------------------------------------------*/
/*
unsigned determine_power_form(unsigned daq_id)	//returns power form for a given daq_mux_id. 
{
  int L1,L2,L3,L1_equal_L2,L2_equal_L3,L3_equal_L1=0;
  unsigned power_form=INVALID;

	L1=gp_Van[daq_id];
  	L2=gp_Vbn[daq_id];
  	L3=gp_Vcn[daq_id];
  	L1_equal_L2=EQUAL_RMS(L1,L2);
  	L2_equal_L3=EQUAL_RMS(L2,L3);
  	L3_equal_L1=EQUAL_RMS(L3,L1);
	
	
	if (L1_equal_L2 && EQUAL_RMS(L3,2*L1))		// L3==gp_Vcn[]==gp_Vca[]==VOLTAGE LEG.
	{
		power_form = ONE_PHASE;
	}
	if (L2_equal_L3 && EQUAL_RMS(L1,2*L2))		// L1==gp_Van[]==gp_Vab[]==VOLTAGE LEG.
	{
		power_form = ONE_PHASE;
	}
	if (L3_equal_L1 && EQUAL_RMS(L2,2*L3))		// L2==gp_Vbn[]==gp_Vbc[]==VOLTAGE LEG.
	{
		power_form = ONE_PHASE;
	}
		
	
	if (L1_equal_L2 && EQUAL_RMS(L3,SQRT_OF_3*L1))		// L3==gp_Vcn[]==gp_Vca[]==VOLTAGE LEG.
	{
		power_form = TWO_PHASE;
	}
	if (L2_equal_L3 && EQUAL_RMS(L1,SQRT_OF_3*L2))		// L1==gp_Van[]==gp_Vab[]==VOLTAGE LEG.
	{
		power_form = TWO_PHASE;
	}
	if (L3_equal_L1 && EQUAL_RMS(L2,SQRT_OF_3*L3))		// L2==gp_Vbn[]==gp_Vbc[]==VOLTAGE LEG.
	{
		power_form = TWO_PHASE;
	}


//	if (L1_equal_L2 && L2_equal_L3)				// L1==L2==L3==BALANCED THREE-PHASE POWER.
	if (EQUAL_RMS_40(L1,L2) && EQUAL_RMS_40(L2,L3))			// L1==L2==L3==BALANCED THREE-PHASE POWER.
	{
		power_form = THREE_PHASE;
	}

	
	if (L1_equal_L2 && EQUAL_RMS(L3,0.0))		// L3==gp_Vcn[]==gp_Vca[]==VOLTAGE LEG.
	{
		power_form = ONE_PHASE;
	}
	if (L2_equal_L3 && EQUAL_RMS(L1,0.0))		// L1==gp_Van[]==gp_Vab[]==VOLTAGE LEG.
	{
		power_form = ONE_PHASE;
	}
	if (L3_equal_L1 && EQUAL_RMS(L2,0.0))		// L2==gp_Vbn[]==gp_Vbc[]==VOLTAGE LEG.
	{
		power_form = ONE_PHASE;
	}
	return power_form;
}
/*---------------------------------------------------------------------------*/
unsigned frequency_in_range(unsigned daq_id, unsigned sync_id, float fmin, float fmax)
{
  unsigned status=1;
  unsigned temp_daq_mux_id=daq_mux_id;
  unsigned temp_sync_mux_id=sync_mux_id;

	set_meter_mux(daq_id,sync_id);
	meas_all();

	if (fmax<=gp_Freq[daq_id] || gp_Freq[daq_id]<=fmin)
	{
		status=0;
	}

	set_meter_mux(temp_daq_mux_id, temp_sync_mux_id);	//restore daq & sync mux settings.
	
	return status;
}
/*---------------------------------------------------------------------------*/
unsigned voltage_in_range(unsigned daq_id, unsigned sync_id, float vmin, float vmax)
{
  unsigned status=1;
  unsigned temp_daq_mux_id=daq_mux_id;
  unsigned temp_sync_mux_id=sync_mux_id;
  float Van,Vbn,Vcn;

	set_meter_mux(daq_id,sync_id);
	meas_all();

	Van = gp_Van[daq_id];
	Vbn = gp_Vbn[daq_id];
	Vcn = gp_Vcn[daq_id];

	if (daq_id==GEN && GP_OUTPUT_FORM==ONE_PHASE)	//v1.57 compensate for line-line gen volts.
	{
		Van = Van * 0.5;
	}
	if (daq_id==SYS && GP_OUTPUT_FORM==ONE_PHASE)
	{
		Van = Van * 0.5;
	}

	if (vmax<=Van || Van<=vmin)
	{
		status=0;
	}

//	if (GP_OUTPUT_FORM != ONE_PHASE)	//OLD LOGIC DIDN'T CONSIDER TWO-PHASE OUTPUTS.
	if ((daq_id==OSC || daq_id==GEN || daq_id==SYS) && GP_OUTPUT_FORM == THREE_PHASE)	//v2.11
	{
		if (vmax<=Vbn || Vbn<=vmin)
		{
			status=0;
		}

		if (vmax<=Vcn || Vcn<=vmin)
		{
			status=0;
		}
	}

	set_meter_mux(temp_daq_mux_id, temp_sync_mux_id);	//restore daq & sync mux settings.

	return status;
}
/*---------------------------------------------------------------------------*/
void main()
{
	_srvwdt();

//	init_hw();				// initialize hardware.
//	_srvwdt();
	
	init_outputs();			// set all outputs to nominal.
	_srvwdt();

	initialize_gp_state();	// initialize gp_state structure in RAM.
	_srvwdt();

	init_ram();				// do ram test and initalize if needed.
	_srvwdt();

//	initialize_sid();		//initialize SYSTEM INTERFACE DRIVER.

	inhibitInputOn();			//INHIBIT INPUT ON.

	LogEvent(Ev_RESET,0);
	_srvwdt();

	if (MODEL_LC) {
		if (gp_status_word2 & 0x0100) {
			shutoffId = 12;				//Moisture in Coolant Oil detected
			gp_diagnostic_code=-249;	//Moisture in Coolant Oil.
			set_sysstat(FAILURE);
			
//			inhibitInputOn();			//INHIBIT INPUT ON.
//				set_REM_IN_OFF();

			Oil_Fault |= 0x02;
		}
	}

	init_hw();				// initialize hardware.
	_srvwdt();

	init_lcd();				// initialize display.
	_srvwdt();

	init_waveforms();		// initialize waveforms if needed.
	_srvwdt();

	show_signon_screen();
	_srvwdt();

	IE_init();				// initialize external communications port.
	_srvwdt();

	_putbit(1, PSW, 11);	// enable all interrupts.

	pgm_edit=def_pgm;

	if (gp_state.vout_range==HIGH_RANGE) 
	{
		pgm_edit.Va = newVoltage * 0.5;
		pgm_edit.Vb = newVoltage * 0.5;
		pgm_edit.Vc = newVoltage * 0.5;
	}
	else
	{
		pgm_edit.Va = newVoltage;
		pgm_edit.Vb = newVoltage;
		pgm_edit.Vc = newVoltage;
	}
	exec_osc_pgm();

	clear_TIP_CMD();		//set "TIP_CMD" signal LOW.
	delay(2000);			//pause 3 seconds for lvps to stablize.


	if ((!MODEL_LC) || ((MODEL_LC) && (!(gp_status_word2 & 0x0100))))
		allowInputOn();
	
	clear_screen();
	display_metering(display_type, display_function);
	
	

	pgm_edit=def_pgm;

	if (gp_state.vout_range==HIGH_RANGE) 
	{
		pgm_edit.Va = newVoltage * 0.5;
		pgm_edit.Vb = newVoltage * 0.5;
		pgm_edit.Vc = newVoltage * 0.5;
	}
	else
	{
		pgm_edit.Va = newVoltage;
		pgm_edit.Vb = newVoltage;
		pgm_edit.Vc = newVoltage;
	}
	exec_osc_pgm();

#ifdef VCO_MOD
	VCO_MIN=pgm_edit.Freq-3;
	VCO_MAX=pgm_edit.Freq+3;
#endif //VCO_MOD

//	delay(3000);			//pause 3 seconds.
//	delay(2000);			//pause 2 more seconds for a total of 3+2= 5 seconds.

//	if (RAM_reset) LogEvent(Ev_ERR_UNCALIBRATED,0);

#ifndef MM3
	
	Xfer_Imped_Ramp_Status.all = 0;

	if (!SWITCHGEAR_INTERFACE_OPTION && !HIGH_Z && !EXT_OSC_OPTION && !MULTIGEN_BESECKE)
	{
		gp_state.generator1_contactor_state = Gen1conf;	// GEN #1 CONTACTOR CONFIRM
		gp_state.generator2_contactor_state = Gen2conf;   	// GEN #2 CONTACTOR CONFIRM
	}
	if (HYBRID_STO)
	{
		gp_state.generator1_contactor_state = Gen1conf;	// GEN CONTACTOR CONFIRM
		gp_state.generator2_contactor_state = FALSE;		   	// GEN #2 CONTACTOR CONFIRM
	}

	initialize_generator_status();

	if (SEAMLESS_TRANSFER && !MULTIGEN_BESECKE)
	{
		if (gp_state.generator1_contactor_state || gp_state.generator2_contactor_state)
		{
			if (TIE_CONF_PRESENT)
			{
				if (gp_state.generator1_contactor_state)
				{
					disable_output();	//level control "REM_OUT_OFF" high=enable output low=disable output.
				}
				else if (tieBreaker_CLOSED())
				{
					disable_output();	//level control "REM_OUT_OFF" high=enable output low=disable output.
				}
			}
			else
			{
				disable_output();	//level control "REM_OUT_OFF" high=enable output low=disable output.
			}
		}
		else
		{
			if (!EXT_CB_OPTION)  {
				enable_output();	//level control "REM_OUT_OFF" high=enable output low=disable output.
//				debug = 3;
			}
		}
	}
	else
	{
		if (!EXT_CB_OPTION) {
			enable_output();	//level control "REM_OUT_OFF" high=enable output low=disable output.
//			debug = 4;
		}
	}

//	if (MODEL_LC) ledLightShow();	//blink LED's in sequence during self-test.

	check_gp_status();

	if (autorestart==ENABLED && (!REMOTE_RUN))
	{
		do_autorestart();
	}
	else	//ISSUE WARNING(S) ONLY IF (SYSSTAT==BAD) (A) BAD SYSSTAT ON INITIALIZATION,
			//		ALSO; IF AUTORESTART WAS ON (B) AUTORESTART HAS BEEN DISABLED.
	{
		autorestart=DISABLED;
	}
#endif //~MM3
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
//	CC22IC = 0x0073;					// enable gen#2 sync interrupt for synchroscope.

	monitor();				// loop forever, it all happens from here.

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
	freeTimer(ssTimer);							//free ssTimer.
	freeTimer(inhibitOnTimer);					//free inhibitOnTimer.

	lcd_display("ASEA MONITOR EXITED . . .\n . . . RECOVERY REQUIRES A COMPLETE SYSTEM RESTART.\nPRESS ANY KEY . . .\0",1,1);
	wait_for_key();
	gp_reset();
	 
 }
/*--------------------------------------------------------------------------*/
void do_autorestart()		// Autorestart sequence.
{
  volatile unsigned t0=seconds;
  unsigned i=0;

	LogEvent(Ev_AUTO_ON,0);

	check_gp_status();

	if (gp_state.sysstat!=OK)
	{
		autorestart=DISABLED;
		LogEvent(Ev_ERR_AUTO_ON,0);
		return;
	}

	if (shutoffId>=6)
	{
		retryCount++;
	}
	else
	{
		retryCount=0;
	}

	if (retryCount>=3)
	{
		autorestart=DISABLED;
		LogEvent(Ev_ERR_AUTO_ON,0);
		return;
	}

	if (gp_state.invstat==OFFLINE)
	{
		if (TIE_CONF_PRESENT)			//this is a work-around to accomidate Open Tie Restarts.
		{
			if (!tieBreaker_CLOSED()) 						//TB CB Open
				gp_state.generator2_contactor_state = OFF;  	// GEN #2 CONTACTOR CONFIRM
		}

 		if (gp_state.generator1_contactor_state==OFF && gp_state.generator2_contactor_state==OFF)
		{
start_ac:
			gp_state.generator2_contactor_state = Gen2conf;   	// GEN #2 CONTACTOR CONFIRM restore

			if (gp_state.local_master==SLAVE)
			{
				delay(3000);	// 6/14/2001 timeline from MAW.
				delay(2000);	// 6/14/2001 timeline from MAW.
			}

			set_REM_IN_ON();

//			for (i=0;i<10;i++)		//delay 5 seconds 
			for (i=0;i<12;i++)		//delay 6 seconds 
			{
				softStartGuard();
				delay(500);			//update display every 500ms.
				check_gp_status();
				meter_mux_on_display(CONVERTER_POWER);
				meas_all();				// calc metering data.
	 			display_metering(CONVERTER_POWER, display_function);		// Show updated meter.
			}

			i=0;
			while(gp_state.inpstat1==OFFLINE && i<10)	//wait for sp1 to come online.
			{											// 10 second timeout.
				softStartGuard();
				check_gp_status();
				delay(200);			//delay 200 milliseconds.
				meter_mux_on_display(CONVERTER_POWER);
				meas_all();				// calc metering data.
	 			display_metering(CONVERTER_POWER, display_function);		// Show updated meter.

				if (t0 != seconds)
				{
					t0=seconds;
					i++;			//increment local seconds counter.
				}
			}
 
			i=0;
//			while(gp_state.inpstat1==ONLINE && i<10)	//sp1 must stay online for 20sec's.
			while(gp_state.inpstat1==ONLINE && i<7)		//sp1 must stay online for 16sec's.
			{
				check_gp_status();
				delay(200);			//delay 200 milliseconds.
				meter_mux_on_display(CONVERTER_POWER);
				meas_all();				// calc metering data.
	 			display_metering(CONVERTER_POWER, display_function);		// Show updated meter.

				if (t0 != seconds)
				{
					t0=seconds;
					i++;			//increment local seconds counter.
				}
			}

			if((gp_state.sysstat==OK) && gp_state.inpstat1==ONLINE)
			{
				if(g2c!=TRUE)
				{
					set_GEN_CBs_OPEN();	//disable GENERATORS, open both generator contactors.
					gp_state.genstat2=OFFLINE;
					delay(40);
					gp_state.genstat1=OFFLINE;
					delay(40);
				}

#ifdef DHK_09172015
				if (gp_state.local_master==MASTER)
				{
					delay(3000);	// let slave catch-up, 6/14/2001 timeline from MAW.
					delay(2000);	// let slave catch-up, 6/14/2001 timeline from MAW.
//					delay(3000);	// wait for PFC OK 5/15/2001 memo from MAW.
				}
#endif

				if (EXT_CB_OPTION) {		//DHK 10142015: V9.03
					set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
					enable_output();
				}
				else {
					enable_output();
					set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
				}
//				debug = 5;
			}
			else	//DISABLE AND ISSUE 'WARNING' (D) AUTORESTART HAS BEEN DISABLED.
			{
				autorestart=DISABLED;
				LogEvent(Ev_ERR_AUTO_ON,0);
			}
		}
		else
		{
			gp_state.generator2_contactor_state = Gen2conf;   	// GEN #2 CONTACTOR CONFIRM restore

			if (MULTIGEN_OPTION)
			{
				if (multipleGensetsOnline())
				{
					if (MULTIGEN_INHIBIT)
					{
						auto_transfer_enable=DISABLED;
						abort_transfer(9);
						return;
					}
					else 
					{
						remote_genset = 1;
					}
				}
				else	//only one genset online
				{
					if (eGen4_CLOSED()) remote_genset = 4;
					else if (eGen3_CLOSED()) remote_genset = 3;
					else if (eGen2_CLOSED()) remote_genset = 2;
					else remote_genset = 1;
				}
			}
			else
			{
				if (gp_state.generator2_contactor_state==ON) remote_genset = GEN2;
				else remote_genset = GEN1;
			}

			if (auto_transfer_enable)
			{
				if (AUTO_TRANSFER || ((gp_status_word3 & 0x8000)==0))	//slave's status (LOW==OK).
				{
					g2c = TRUE;
					goto start_ac;
				}
			}

			LogEvent(Ev_ERR_AUTO_ON,0);
		}
	}
	else	//DISABLE AND ISSUE 'WARNING' (C) AUTORESTART HAS BEEN DISABLED.
	{
		autorestart=DISABLED;
		LogEvent(Ev_ERR_AUTO_ON,0);
	}//ENDelseif (gp_state.invstat==OFFLINE)
}
/*--------------------------------------------------------------------------*/
void monitor()
{
  int remoteRunCounter=0;
  int k;
  int transferRequest=0; //0=black-out;1=genset#1(A+B);2=genset#(A+B)2;3=converter(A+B);4=split(g1/A+g2/B).
  int busState;
  int ship_gen_sw;
  int ship_conv_sw;
  volatile unsigned long tempTime = eventTime;				//start MONITOR TIMER

	initialize_generator_status();

	key_hold_time=0;

	gp_mode = NORMAL_MODE;
	monitor_started = 1;

	if (timerRunning(inhibitOnTimer))
	{
		inhibitInputOn();			//INHIBIT INPUT ON.
	}
	

	while(1)		   					//do forever.
	{
		softStartGuard();

		if (gp_state.gp_mode != gp_mode)
		{
			gp_state.gp_mode=gp_mode;		// {undefined,NORMAL_MODE,TEST_MODE,CALIBRATION_MODE}.

			LogEvent(Ev_RUN_MODE,(unsigned char)gp_mode);
		}

		switch (gp_mode)
		{
			case NORMAL_MODE:		
#ifdef VCO_MOD
								doVCO();	//VCO frequency control by external Vdc.
#endif //VCO_MOD
								if (!MULTIGEN_OPTION && display_type==GENERATOR_POWER && display_function==F4)
								{ 												//synchroscope
									if (SEAMLESS_TRANSFER)
									{
										meter_mux_on_display(GENERATOR_POWER);

										CC29IC = 0x0077;	// enable gen#1 generator sync interrupt.
										CC22IC = 0x0073;	// enable gen#2 sync interrupt.
									}
									else
									{
										meter_mux_iterator(); 	// iterate meter & sync mux controls.
										CC29IC = 0;	// disable gen#1 generator sync interrupt.
										CC22IC = 0;	// disable gen#2 sync interrupt.
									}
								}
								else
								{
									meter_mux_iterator(); 	// iterate meter & sync mux controls.
									CC29IC = 0;	// disable gen#1 generator sync interrupt.
									CC22IC = 0;	// disable gen#2 sync interrupt.
								}
//
//Atlas Emulation Mode TRANSFER VIA SWITCHGEAR GEN_MATCH_CMD
//
								if (AEM_OPTION)
								{
									if (gp_state.aemGenMatchCmd)
									{
										if (gp_state.sysstat==OK)
										{
											abort_power_on=0;
											new_key=0;
											initialize_generator_status();
											set_TIP_CMD();
											transfer_in_progress=YES;

//											if (AEM2_OPTION) aem2_sync2gen(GEN1);
//											else 
											aem_sync2gen(GEN1);

											restore_output();	//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!
											clear_TIP_CMD();
											transfer_in_progress=NO;
										}
									}
								}
//HYBRID_STO==============================================================================================
								processHybridSTO();
//HYBRID_STO==============================================================================================
//								if (MODEL_LC) ship_gen_sw = 0;
//								else 
									ship_gen_sw = _getbit(P5,11);
#ifdef MM3
								c2g = 0;
#endif //MM3
								if (c2g || ship_gen_sw) // SP_GEN_SW  P3A-11 // TRANSFER TO GENERATOR
								{
									if (_getbit(P5,11))
									{
										LogEvent(Ev_KEY_PRESS,254);
										c2g=0;
									}

									if (gp_state.sysstat==OK || gp_state.sysstat==WARNING)
									{
	  									abort_power_on=0;
	  									initialize_generator_status();

										meas_all();

										if (GEN2GEN_OPTION)
										{
											if (c2g)
											{
												transferRequest = c2g;
												clear_screen();
											}
											else
											{
												transferRequest = gen2gen_transfer_control();
											}
											
											transferPower(transferRequest);
										}
/*
										else if (MULTIGEN_BESECKE)
										{
											if (c2g)
											{
												transferRequest = c2g;
												clear_screen();
											}
											else
											{
												transferRequest = besecke_transfer_control(GENERATOR_POWER);
											}
											
											transferPower(transferRequest);
										}
*/
										else
										{
											if (HYBRID_STO)
											{
												clear_screen();
												lcd_display("HYBRID TRANSFER SYSTEM\0",1,10);
												lcd_display("TRANSFERS TO GENERATOR NOT ALLOWED\0",2,4);
												delay(2000);
//												abort_transfer(19);  //Bad Transfer Request
											}
											else
											{
												meter_mux_on_display(GENERATOR_POWER);
												transfer2generator();
											}
										}

										gp_mode=NORMAL_MODE;
										c2g=NO;
										
										if (transfer_in_progress != NO) {
											restore_output();	//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!
											clear_TIP_CMD();
											transfer_in_progress=NO;
										}

										if (AUTO_SHUTDOWN && autoShutdownEnable)
										{
											check_gp_status();
//											if(gp_state.invstat==OFFLINE && !abort_power_on)
											if(gp_state.invstat==OFFLINE)
											{
												LogEvent(Ev_AUTO_SHUTDOWN,0);
												set_REM_IN_OFF();
											}
										}
									}
									else
									{
										abort_transfer(2);
										c2g=NO;
										clear_TIP_CMD();
										transfer_in_progress=NO;
									}
								}

//								if (MODEL_LC) ship_conv_sw = 0;
//								else 
									ship_conv_sw = _getbit(P5,12);
#ifdef MM3
								g2c = 0;
#endif //MM3
								if (g2c || ship_conv_sw) // SP_CON_SW  P3A-12 // TRANSFER TO CONVERTER
								{
									if (_getbit(P5,12)) LogEvent(Ev_KEY_PRESS,255);

									if (gp_state.sysstat==OK)
									{
										abort_power_on=0;
										initialize_generator_status();

										meas_all();

										if (GEN2GEN_OPTION)
										{
											if (NEW_G2G_OPTION)

												busState = get_busState2();
											else
												busState = get_busState();

											switch (busState)
											{
												case BsDEAD: transferPower(BsCONV); break;
												case BsCONV: abort_power_on=20;		break;
												default: 	 abort_power_on=50;		break;
											}
										}
/*
										else if (MULTIGEN_BESECKE)
										{
											if (g2c)
											{
												transferRequest = g2c;
												clear_screen();
											}
											else
											{
												transferRequest = besecke_transfer_control(CONVERTER_POWER);
											}
											
											transferPower(transferRequest);
										}
*/
										else
										{
											meter_mux_on_display(GENERATOR_POWER);
											transfer2converter();
										}
										gp_mode=NORMAL_MODE;
										g2c=NO;
//										clear_TIP_CMD();
//										transfer_in_progress=NO;
										restore_output();	//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!
										clear_TIP_CMD();	//moved to follow Z-recovery
										transfer_in_progress=NO;
									}
									else
									{
										abort_transfer(2);
										g2c=NO;
										clear_TIP_CMD();
										transfer_in_progress=NO;
									}
								}
//create:	int convertAbortCode( int );
//usage: 	abort_transfer(convertAbortCode(abort_power_on))

								if(abort_power_on==37)
								{
									if (get_busState2()==BsG1AG2B)
										abort_power_on=0;
									else
										abort_transfer(22);
									abort_power_on=0;
								}

								if(abort_power_on==20)
								{
									abort_transfer(10);
								}

								if(abort_power_on==30)
								{
									abort_transfer(11);
								}

								if(abort_power_on==40)
								{
									abort_transfer(12);
								}

								if(abort_power_on==50)
								{
									abort_transfer(20);
								}

								if(abort_power_on==51)
								{
									abort_transfer(13);
								}

								if(abort_power_on==55)
								{
									abort_transfer(14);
								}

								if(abort_power_on==56)
								{
									abort_transfer(23);
								}

								if(abort_power_on==60)
								{
									abort_transfer(15);
								}

								if(abort_power_on==61)
								{
									abort_transfer(16);
								}

								if(abort_power_on==2)
								{
									abort_transfer(3);
								}

								if(abort_power_on==33)
								{
									abort_transfer(3);
								}

								if(abort_power_on==44)
								{
									abort_transfer(4);
								}

								if(abort_power_on==4)
								{
									abort_transfer(7);
								}

								if(abort_power_on==23)
								{
									abort_transfer(1);
								}

								if(abort_power_on==24)
								{
									abort_transfer(24);
								}

								if(abort_power_on==25)
								{
									abort_transfer(25);
								}

								if(abort_power_on==26)
								{
									abort_transfer(26);
								}

								if(abort_power_on==27)
								{
									abort_transfer(27);
								}

								if(abort_power_on==28)
								{
									abort_transfer(28);
								}

								if(abort_power_on==29)
								{
									abort_transfer(29);
								}

								if (GEN2GEN_OPTION && !NEW_G2G_OPTION)
								{
									if (cb6_CLOSED())	//Generator Power on Input to Converter.
									{
										busState = get_busState();
									//
									// FORCE TRANSFER FROM CONVERTER IF NOT DOCK POWER INPUT.
									//
										if ((busState==3) || (busState>6))
										{
	  										abort_power_on=0;
											initialize_generator_status();
				
											transferRequest = gen2gen_transfer_control();
											transferPower(transferRequest);

											gp_mode=NORMAL_MODE;
//											clear_TIP_CMD();
//											transfer_in_progress=NO;
											restore_output();	//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!
											c2g=NO;
											clear_TIP_CMD();		//moved to follow Z-recovery
											transfer_in_progress=NO;
										}
									}
								}
								
								//select a screen or display.
//								if (TECHNEL_OPTION && new_key==100)
//								if (gen_start_cmd_enable && new_key==100)
//								{
//									transfer_inhibit_control_screen();
//									display_function = F1; 
//								}
//								else
//								if (TECHNEL_OPTION && new_key==100)	//gp+f4
								if (new_key==100)	//gp+f4
								{
 									generator_start_control_screen();
									display_function = F1; 
								}
//								else 
//								if (TECHNEL_OPTION && new_key==101)
//								if (gen_start_cmd_enable && new_key==101)
//								{
// 									generator_start_control_screen();
//									display_function = F1; 
//								}
//								else 
//								if (GEN_START_OPTION && new_key==100)
//								{
// 									generator_start_control_screen();
//									display_function = F1; 
//								}
								else 
								if (GEN_START_OPTION && new_key==100)
								{
 									generator_start_control_screen();
									display_function = F1; 
								}
								else 
								if (NEW_G2G_OPTION && new_key==101)
								{
									generator_control_screen();
									display_function = F1; 
								}
								else 
								if (TIP_OPTION && new_key==101)
								{
									tip_delay_control_screen();
									display_function = F1; 
								}
								else 
								if ((EXT_CB_OPTION && new_key==304) && (gp_state.local_master == MASTER))
								{
									external_cb_control_screen();
									display_function = F1; 
								}
								else 
								if (AUTO_SHUTDOWN && new_key==205)
								{
 									auto_shutdown_control_screen();
									display_function = F1; 
								}
								else 
								if (new_key==305)
								{
									vout_setup_screen();
									display_function = F1; 
								}
								else 
								if (new_key==306)
								{
									reset_cb_control_screen();
									display_function = F1; 
								}
								else 
								if (new_key==204)
								{
									AGC_control_screen();
									display_function = F1; 
								}
								else 
								if (new_key==103)	//gp+f3
								{
									if (config_no_crossover_delay()==FALSE)
									{
										generator_transfer_control_screen();
										display_function = F1; 
									}
								}
								else 
								if (new_key==104)
								{
									transfer_guard_control_screen();
									display_function = F1; 
								}
								else 
								if (new_key==105)
								{
									transfer_timeout_control_screen();
									display_function = F1; 
								}
								else 
								if (new_key==201)
								{
//									shorepoweralarm_setup_screen();
									energy_management_setup_screen();
									display_function = F1; 
								}
								else 
								if (new_key==202)
								{
									auto_transfer_control_screen();
									display_function = F1; 
								}
								else 
								if (new_key==203)
								{
									pwm2_setup_screen();
									display_function = F1; 
								}
								else 
								if (new_key==400 && MULTIGEN_OPTION)
								{
									meter_mux_on_display(GENERATOR_POWER);
									set_gen_mux(1);
									clear_meter_data(GEN);
									new_key=0;
									meas_all();
									abort_power_on=0;
									initialize_generator_status();
									test_generator_interface(GEN1);
									display_function = F1; 
									display_type = SYSTEM_STATUS; 
									new_key=0;
											gp_mode=NORMAL_MODE;
											u_TIP_signal=transfer_in_progress=NO;
											restore_output();	//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!
								}
								else 
								if (new_key==99)
								{
									EventLogSelectControl();
						 			display_metering(display_type, display_function);		// Show updated meter.
									new_key=0;
								}
#ifdef GNOP_ON
								else 
 								if (new_key==206)
  								{
									pong();
									display_type = SYSTEM_STATUS; 
									display_function = F1; 
								}
#endif // GNOP_ON
								else
								{
//									if (new_key) LogEvent(Ev_KEY_PRESS,(unsigned char)new_key);
#ifndef MM3
						 			display_metering(display_type, display_function);		// Show updated meter.
#else	//12122016 DHK: Added, S2-4 (REMOTE_RUN), can not calibrate
									new_key=0;
#endif //~MM3
								}

//								IE_main();
								remoteManager();
//
//COMMAND INTERPRETER
//
								command_interpreter(mbCommand);
//
//REMOTE TRANSFER REQUEST
//
								if (REMOTE_PANEL)
								{
									if (get_REM_XFR_REQ())	//Returns 1 if REM_XFR_REQ received.
									{
										if (MULTIGEN_OPTION)
											remote_genset = process_MULTIGEN_REM_XFR_REQ();
										else 
											remote_genset = process_REM_XFR_REQ();
									}
									else if (Remote_Panel_Xfr_Req_Stat)
										Remote_Panel_Xfr_Req_Stat = 0;
								}
//
//VIRTUAL TRANSFER REQUEST
//
								if (virtualTransferRequest)	//virtural REM_XFR_REQ received via Modbus.
								{
									if (MULTIGEN_OPTION)
										remote_genset = process_MULTIGEN_REM_XFR_REQ();
									else
										remote_genset = process_REM_XFR_REQ();

									virtualTransferRequest=NO;
								}
								else if (Remote_Panel_Xfr_Req_Stat)
									Remote_Panel_Xfr_Req_Stat = 0;

//
//REMOTE RUN(low) REQUEST
//
								if (REMOTE_RUN)	//S2-4 RUN(low) input M1041
								{
									if (get_REM_RUN_REQ() && (remoteRunCounter<3))	//Returns 1 if REM_XFR_REQ received.
									{
										if (gp_state.sysstat!=OK)
											set_sysstat(OK);	//clear alarm, forces SUMMARY DISPLAY

///									do_autorestart();
										check_gp_status();	//check for active alarms.
										if (gp_state.sysstat==OK)
										{
											if (gp_state.invstat==OFFLINE)
											{
												LogEvent(Ev_AUTO_ON,0);
												clear_screen();
												lcd_display("AUTOMATIC ON IN PROGRESS...\0",1,1);	//display what's happening.

												if (gp_state.inpstat1==OFFLINE)
												{
													set_REM_IN_ON();
													softStartGuard();

													for (k=0;k<6;k++)
													{
														delay(1000);
														softStartGuard();
													}
													check_gp_status();	//check for active alarms.
												}

												if (gp_state.sysstat==OK)
												{
													if (gp_state.inpstat1==ONLINE)
													{
														if (EXT_CB_OPTION) {		//DHK 10142015: V9.03
															set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
															enable_output();
														}
														else {
															enable_output();
															set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
														}
													}
													else	//input didn't come on.
													{
														LogEvent(Ev_ERR_AUTO_ON,0);
														remoteRunCounter++;
													}
												}
												else		//bad system status
												{
													LogEvent(Ev_ERR_AUTO_ON,0);
													remoteRunCounter++;
												}

												delay(2000);			//give time to deassert REMOTE RUN signal
									 			display_metering(display_type, display_function);	//restore display.

											}//if (gp_state.invstat==OFFLINE)
										}//if (gp_state.sysstat==OK)
										else						//bad system status
										{
											LogEvent(Ev_ERR_AUTO_ON,0);
											remoteRunCounter++;
										}
									}//if (get_REM_RUN_REQ())
								}//if (REMOTE_RUN)

								break;

			case TEST_MODE:			
							if (EXT_OSC_OPTION)	//v2.56, S3-6 configuration
							{
								gp_mode=NORMAL_MODE;	//allow use of HELP key.
								special_function=0;
							}
							else
							{
								meter_mux_on_display(GENERATOR_POWER);

								abort_power_on=0;
								initialize_generator_status();

								test_generator_interface(GEN1);

								restore_output();	//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!
							}
							break;

			case CALIBRATION_MODE:	
								if(display_type==GENERATOR_POWER && gp_state.active_display==GEN2)
								{
									meter_mux_on_display(SYSTEM_POWER);
								}
								else
								{
									meter_mux_on_display(display_type);
								}

								if (gp_state.sysstat)
								{
									new_key=18;	//BAIL-OUT, NO CALIB ALLOWED 'CAUSE BAD STATUS!
								}
								
								if (new_key==18)	// if SYSTEM_STATUS key pressed then return to NORMAL_MODE.
								{
									gp_mode=NORMAL_MODE;	//allow use of HELP key.
									special_function=0;

									if(gp_state.active_display==GEN2)
									{
										display_function=F2;
									}
									else	//GENSET #1
									{
										display_function=F1;
									}

									new_key=0;
									ALC_mode=AGC_state;	//v1.99
//									ALC_mode=ON;	//v1.29
								}
								else	// else CALIBRATION_MODE requires special keypad handler.
								{
									strobe_kybd_for_slew();

									if (new_key)
									{
										new_key=0;
										meas_all();		//GET FRESH MEASUREMENTS TO DISPLAY.
//							 			display_metering(display_type, display_function);
									}
								}

					 			display_metering(display_type, display_function);		// Show updated meter.		
								break;
							
			default://invalid gp_mode
								gp_mode=NORMAL_MODE;	//allow use of HELP key.
								special_function=0;
								meter_mux_iterator();
								break;
		}

#ifdef MM3
		meter_mux_on_display(CONVERTER_POWER);
		gp_mode=NORMAL_MODE;	//allow use of HELP key.
		special_function=0;
		last_key = new_key;
		key_hold_time = 0;

		if (_getbit(P7,6))	//AGC switch in at P78-3,-4
		{
			AGC_state=ON;	//v1.99
		}
		else
		{
			AGC_state=OFF;	//v1.99
		}
		ALC_mode=AGC_state;	//used for 3-phase systems as of v1.99

//		IE_main();
		remoteManager();

#else
		strobe_kybd_for_slew();	//sets 'new_key=0' and polls kybd for active key-down event.
#endif //~MM3

		meas_all();				// calc metering data and do CSC.
		check_gp_status();
		
#ifndef MM3
//		if ((!AEM_OPTION) && (!HIGH_Z)) {			//060315 DHK: commented out; 11092012 DHK: Added condition to call initialize_generator_status() due to filling Event Log with GEN2 ONLINE in AEM_OPTION and HIGH_Z
			initialize_generator_status();
//		}
#endif //~MM3

		monitorTime = (unsigned) (eventTime-tempTime);	//stop MONITOR TIMER
		tempTime = eventTime;							//start MONITOR TIMER

//		if(update_display)			// wait 200 milliseconds.
//		{
//			update_display=0;
// 			display_metering(display_type, display_function);		// Show updated meter.
// 		}

	}
}
/*---------------------------------------------------------------------------*/
char *utoX(unsigned number)
{
	cvtbufr[0]=NULL;
	sprintf(cvtbufr,"%4X\0",number);	//convert number to ascii(HEX) string.
	return cvtbufr;
}
/*---------------------------------------------------------------------------*/
char *itoa(int number)
{
	cvtbufr[0]=NULL;
	sprintf(cvtbufr,"%d\0",number);	//convert number to ascii string.
	return cvtbufr;
}
/*---------------------------------------------------------------------------*/
char *utoa(unsigned number)
{
	cvtbufr[0]=NULL;
	sprintf(cvtbufr,"%u\0",number);	//convert number to ascii string.
	return cvtbufr;
}
/*-------------------------------------------------------------------------- */
char *ltoa(long number)
{
	cvtbufr[0]=NULL;
	sprintf(cvtbufr,"%ld\0",number);	//convert number to ascii string.
	return cvtbufr;
}
/*-------------------------------------------------------------------------- */
char *ltoX(long number)
{
union	{ 						//used to convert a long int to a string of eight hex ascii char.s
	long number;					//32-bit number input
	unsigned buf[2];      				//2-by-16 bit unsigned buf
	} hex;

	hex.number=number;
	cvtbufr[0]=NULL;
	sprintf(cvtbufr,"%X%X\0",hex.buf[1],hex.buf[0]);	//convert number to ascii string.
	return cvtbufr;
}
/*---------------------------------------------------------------------------*/
///////////////////////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*----------------------------- END -----------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
int initialize_generator_status()
{
  unsigned temp_daq_mux_id=daq_mux_id;
  unsigned temp_sync_mux_id=sync_mux_id;
  float min_gen_voltage=(GP_OUTPUT_VOLTAGE-(0.2*GP_OUTPUT_VOLTAGE));	//20% low.
  float max_gen_voltage=(GP_OUTPUT_VOLTAGE+(0.2*GP_OUTPUT_VOLTAGE));	//20% high.

	if (HYBRID_STO)
	{
		gp_state.genstat2 = OFFLINE;
		gp_state.generator2_contactor_state = OFF;

		if (Gen1conf)		// GEN #1 CONTACTOR CONFIRM
		{
			gp_state.generator1_contactor_state = ON;
			gp_state.genstat1 = ONLINE;
		}
		else													//06032015 DHK Added
		{
			gp_state.genstat1 = OFFLINE;
			gp_state.generator1_contactor_state = OFF;
		}
		
		return gp_state.genstat1;								//06032015 DHK Added			
	}
	else if (HIGH_Z || EXT_OSC_OPTION)
	{
		gp_state.genstat1 = OFFLINE;
		gp_state.generator1_contactor_state = OFF;
		gp_state.genstat2 = OFFLINE;
		gp_state.generator2_contactor_state = OFF;
		return gp_state.genstat1;
	}

	if (GP_OUTPUT_FORM==ONE_PHASE)	//v1.58 compensate for line-line gen volts.
	{
		min_gen_voltage = min_gen_voltage * 2.0;
		max_gen_voltage = max_gen_voltage * 2.0;
	}

	if (DUAL_GENSET==NO)
	{
		gp_state.active_display=GEN1;
	}

	if ((!SEAMLESS_TRANSFER) || SWITCHGEAR_INTERFACE_OPTION || EXT_OSC_OPTION || MULTIGEN_BESECKE)
	{
		gp_state.genstat1 = OFFLINE;
		gp_state.generator1_contactor_state = OFF;
		gp_state.genstat2 = OFFLINE;
		gp_state.generator2_contactor_state = OFF;

		if (!SEAMLESS_TRANSFER)
		{
			if (Gen1conf)		// GEN #1 CONTACTOR CONFIRM
			{
				gp_state.generator1_contactor_state = ON;
				gp_state.genstat1 = ONLINE;
			}
			if (Gen2conf)	// GEN #2 CONTACTOR CONFIRM
			{
				gp_state.generator2_contactor_state = ON;
				gp_state.genstat2=ONLINE;
			}
			return 0;
		}
	}

  {//do GENSET #1
	gp_state.genstat1=OFFLINE;

	if (MULTIGEN_OPTION)
	{
		if (gensetOnline())
		{
			if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
			{
				set_GEN_ON_LED();

				if (eGen1_CLOSED())
				{
					gp_state.genstat1 = ONLINE;
					gp_state.generator1_contactor_state = ON;
					return ON;
				}
				if (eGen2_CLOSED())
				{
					gp_state.genstat2 = ONLINE;
					gp_state.generator2_contactor_state = ON;
					return ON;
				}
				if (eGen3_CLOSED())
				{
					gp_state.genstat1 = ONLINE;
					gp_state.generator1_contactor_state = ON;
					return ON;
				}
				if (eGen4_CLOSED())
				{
					gp_state.genstat2 = ONLINE;
					gp_state.generator2_contactor_state = ON;
					return ON;
				}
			}
			else
			{
				gp_state.genstat1 = ONLINE;
				gp_state.generator1_contactor_state = ON;
			}
			set_GEN_ON_LED();
		}
		else	// !gensetOnline()
		{
			clear_GEN_ON_LED();
			if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
			{
				return OFF;
			}
		}//endif ~(gensetOnline())
	}//endif (MULTIGEN_OPTION)
	else	// !MULTIGEN_OPTION
	{
		if (Gen1conf)		// GEN #1 CONTACTOR CONFIRM
		{
			gp_state.genstat1 = ONLINE;
		}
	}

	if (SWITCHGEAR_INTERFACE_OPTION || EXT_OSC_OPTION || MULTIGEN_BESECKE)
	{
		gp_state.genstat1 = OFFLINE;
		gp_state.generator1_contactor_state = OFF;
		gp_state.genstat2 = OFFLINE;
		gp_state.generator2_contactor_state = OFF;
	}

	if (gp_state.genstat1 == ONLINE)		// GEN #1 CONTACTOR CONFIRM
	{
		gp_state.generator1_contactor_state = ON;
		gp_state.genstat1=ONLINE;
		set_meter_mux(GEN, GEN_sync);
		meas_all();

		if (gp_Van[GEN] > min_gen_voltage)
		{
			if (gp_Van[GEN] < max_gen_voltage)
			{
				gp_state.genstat1=ONLINE;
			}
			else
			{
				gp_state.genstat1=OVER_VOLTAGE;
			}
		}
		else
		{
			gp_state.genstat1=UNDER_VOLTAGE;
		}

		if ((gp_Freq[GEN] < 45.0) || (gp_Freq[GEN] > 66.0))
		{
			gp_state.genstat1=FAULT;
		}
	}
	else
	{
		gp_state.generator1_contactor_state = OFF;
		gp_state.genstat1=OFFLINE;
	}
  }//enddo GENSET #1

  if (DUAL_GENSET && !(MULTIGEN_OPTION))
  {//do GENSET #2
	gp_state.genstat2=OFFLINE;

	if (Gen2conf)	// GEN #2 CONTACTOR CONFIRM
	{
		gp_state.generator2_contactor_state = ON;
		gp_state.genstat2=ONLINE;

		set_meter_mux(SYS, SYS_sync);
		meas_all();

		if (gp_Van[SYS] > min_gen_voltage)
		{
			if (gp_Van[SYS] < max_gen_voltage)
			{
				gp_state.genstat2=ONLINE;
			}
			else
			{
				gp_state.genstat2=OVER_VOLTAGE;
			}
		}
		else
		{
			gp_state.genstat2=UNDER_VOLTAGE;
		}

		if ((gp_Freq[SYS] < 45.0) || (gp_Freq[SYS] > 66.0))
		{
			gp_state.genstat2=FAULT;
		}
	}
	else
	{
		gp_state.generator2_contactor_state = OFF;
		gp_state.genstat2=OFFLINE;
	}
  }//enddo GENSET #2
  else
  {
		gp_state.generator2_contactor_state = OFF;
		gp_state.genstat2=OFFLINE;
		
  }

	set_meter_mux(temp_daq_mux_id, temp_sync_mux_id);	//restore daq & sync mux settings.

	return gp_state.genstat1;
}
/*---------------------------------------------------------------------------*/
int sync_osc_with_gen(unsigned gen_id)		//(gp_state.active_display==gen_id).
{
//SYNC. OSC w/GENERATOR ZERO-CROSSING, VERIFY FREQ, VOLTAGE, PHASE MATCHING.
  int sync_status=OK;
  float vmin=GP_OUTPUT_VOLTAGE - 0.2 * GP_OUTPUT_VOLTAGE;
  float vmax=GP_OUTPUT_VOLTAGE + 0.2 * GP_OUTPUT_VOLTAGE;
  float fmin=GP_OUTPUT_FREQUENCY - 0.1 * GP_OUTPUT_FREQUENCY;
  float fmax=GP_OUTPUT_FREQUENCY + 0.1 * GP_OUTPUT_FREQUENCY;
  unsigned long my_time=eventTime;

	LogEvent(Ev_SEEKING,0);

	osc_gen_sync=0;

//	if (gen_id==GEN2 && !MULTIGEN_OPTION)
 	if (gen_id==GEN2 && (!MULTIGEN_OPTION || SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE))
	{
		if(!voltage_in_range(SYS,SYS_sync,vmin,vmax))
		{
			sync_status = NOT_OK;
			abort_power_on=33;
		}

		if(!frequency_in_range(SYS,SYS_sync,fmin,fmax))
		{
			sync_status = NOT_OK;
			abort_power_on=44;
		}

		if(sync_status == OK)
		{
			gp_mode=TEST_MODE;
			set_meter_mux(SYS,SYS_sync);
			meas_all();
			Lock=YES;
			LogEvent(Ev_LOCK,0);
			meter_calc = 1; 	// DISABLE DAQ of ADC data/enable meas_freq() meter calculations.
		}
	}
	else	//GEN1
	{
		if(!voltage_in_range(GEN,GEN_sync,vmin,vmax))
		{
			sync_status = NOT_OK;
			abort_power_on=33;
		}

		if(!frequency_in_range(GEN,GEN_sync,fmin,fmax))
		{
			sync_status = NOT_OK;
			abort_power_on=44;
		}

		if(sync_status == OK)
		{
			gp_mode=TEST_MODE;
			set_meter_mux(GEN,GEN_sync);
			meas_all();

			Lock=YES;
			LogEvent(Ev_LOCK,0);

			meter_calc = 1; 	// DISABLE DAQ of ADC data/but allow meas_freq() meter calculations.
		}
	}

	new_key = 0;

	if(sync_status==OK)
	{
		while (!osc_gen_sync && !abort_power_on)		//look for lock or abort flag.
		{
			check_gp_status();
	
			if (new_key==18)
			{
				sync_status=NOT_OK;
				abort_power_on=4;		//operator abort during phase-lock seek.
				Lock=NO;
				restore_output();	//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!
			}

			if (eventTime >= (my_time+3000))	//30seconds in event time.
			{
				sync_status=NOT_OK;
				abort_power_on=44;		//operator abort during phase-lock seek.
				Lock=NO;
			}
		}
	}
	else	//NOT_OK
	{
		restore_output();	//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!
 	}
 
	return sync_status;
}
/*---------------------------------------------------------------------------*/
int transfer2converter()
{
  unsigned k=0;
  unsigned temp_daq_mux_id=daq_mux_id;
  unsigned temp_sync_mux_id=sync_mux_id;
  unsigned sync_id=GEN_sync;
  int genset=gp_state.active_display;
  int multiGen=0;
  float gp_van_gen=GP_OUTPUT_VOLTAGE;
  float gp_freq_gen=GP_OUTPUT_FREQUENCY;
  int sync_status=OK;
  float vmin=GP_OUTPUT_VOLTAGE - 0.2 * GP_OUTPUT_VOLTAGE;
  float vmax=GP_OUTPUT_VOLTAGE + 0.2 * GP_OUTPUT_VOLTAGE;
  float fmin=GP_OUTPUT_FREQUENCY - 0.1 * GP_OUTPUT_FREQUENCY;
  float fmax=GP_OUTPUT_FREQUENCY + 0.1 * GP_OUTPUT_FREQUENCY;

//
//SHORE POWER ON test?
//
	if (gp_state.inpstat1!=ONLINE)
	{
		abort_transfer(14);
		return 0;
	}

//
//check transfer Mode.
//
	if (manual_mode())			//check if manual_mode().
	{
		abort_transfer(25);
		return 0;
	}

	if (MULTIGEN_BESECKE && !EXTERNAL_CB_CLOSED())
	{
//abort transfer w/msg that 2Q1 is open
		abort_transfer(23);			//NO CONVERTER POWER, converters output power is not ready
		return 0;
	}

	if (gp_state.invstat==ONLINE)
	{
		if (MULTIGEN_BESECKE)
		{
			if (!gensetOnline() && !tieBreaker_CLOSED())
			{
				set_TIE_BREAKER_CB_CLOSE();	//CB3 //toggle "TIE_BREAKER_CB_CLOSE" signal high then low.
				return 0;
			}

			if (get_busState2()==3) return (abort_power_on=20);		//conv already online...
			if (get_busState2()==10) return (abort_power_on=20);	//as per PK conv already online...
		}
		else
		{
			return (abort_power_on=20);
		}
	}

	if (AEM_OPTION)	
	{
		abort_transfer(6);
		return 0;
	}

	if (SEAMLESS_TRANSFER==NO)
	{
		abort_transfer(5);
		return 0;
	}
	else
	{
//		if (TECHNEL_OPTION) 	//Note: FOR TESTING
		if (TECHNEL_OPTION && _getbit(P7,4)) 	//Note: conflict with GEN_1_MATCH_CMD !!
		{										//Issued when GEN LOAD > CONVERTER CAPACITY.
			if (inhibit_transfer==ENABLED)
			{
				abort_transfer(8);
				return 0;
			}
		}

		if (MULTIGEN_INHIBIT)
		{
			if (multipleGensetsOnline())
			{
				abort_transfer(9);
				return 0;
			}
		}

		if (TIE_CONF_PRESENT && !tieBreaker_CLOSED())	//TIE OPEN
		{
			if (gp_state.genstat1==OFFLINE)
			{
				genset=1;
				goto justTurnOn;
			}
			else
			{
				goto select_Gen1;
			}
		}
//wip//////////////////////////////////////////////////wip////////////////////////////////wip//
		if (MULTIGEN_BESECKE)
		{
			if (tieBreaker_CLOSED())	//Bs (2)	(GEN BOTH BUSES)
			{
				genset=process_MULTIGEN_REM_XFR_REQ();	//find running gen to sync to.;
//				goto select_Gen1;
			}
			else						//Bs (6,[10])	(GEN 1/2-BUS, [MIXED-BUS])
			{
				genset=1;
				goto justTurnOn;
			}
		}
//wip//////////////////////////////////////////////////wip////////////////////////////////wip//

		if (DUAL_GENSET==NO)
		{
			genset=GEN1;
			goto select_Gen1;		//added for HYBRID_STO 8/10
		}
		else
		{
			if (g2c)
			{
				genset = remote_genset;

				if (MULTIGEN_OPTION)	//confirm selected genset matches genset presently online.
				{
					multiGen = genset;	//genset is 1, 2, 3, or 4.
//wip v2.47
					if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
					{
						if (multiGen==1 || multiGen==3)	//SWITCHGEAR_INTERFACE_OPTION
						{
							genset = GEN1;
						}
						else	//multiGen==2 || multiGen==4
						{
							genset = GEN2;
						}
					}
					else
					{
						genset = GEN1;
					}

					set_gen_mux(multiGen);
					goto setMultiGen;
				}

				if (gp_state.genstat1==ONLINE || gp_state.genstat2==ONLINE)	//confirm selected genset matches genset presently online.
				{
					  if (genset==GEN1 && gp_state.genstat1!=ONLINE)
					  {
						return abort_power_on=40;
					  }
					  if ((genset==GEN2 && gp_state.genstat2!=ONLINE))
					  {
						return abort_power_on=40;
					  }
				}
			}
			else	//not g2c, so, transfer from local interface
			if (MULTIGEN_OPTION || gp_state.genstat1==ONLINE || gp_state.genstat2==ONLINE)
			{
				if (MULTIGEN_OPTION && !gensetOnline())
				{
					goto justTurnOn;
				}
				clear_screen();

				if (MULTIGEN_OPTION)
				{
					multiGen = process_MULTIGEN_REM_XFR_REQ();	//find running gen to sync to.

					switch (multiGen)	//find running gen to sync to.
					{
						case 1:
							    lcd_display("GENSET #1 AUTO-SELECTED\0",1,1);
								  delay(1000);		//wait a second.
								new_key=19;
								goto select_Gen1;
						case 2:
							    lcd_display("GENSET #2 AUTO-SELECTED\0",1,1);
								  delay(1000);		//wait a second.
								new_key=20;
								goto select_Gen2;
						case 3:
							    lcd_display("GENSET #3 AUTO-SELECTED\0",1,1);
								  delay(1000);		//wait a second.
								new_key=8;
								goto select_Gen3;
						case 4:
							    lcd_display("GENSET #4 AUTO-SELECTED\0",1,1);
					if (MULTIGEN_BESECKE)  //M/Y Bounty Hunter  //gp_status_word3 & 0x0004 (high) to configure.
						lcd_display(strOwner,1,1);
								  delay(1000);		//wait a second.
								new_key=9;
								goto select_Gen4;
						default:
								break;
					}

 					lcd_display("SELECT GENERATOR FOR TRANSFER: \0",1,1);
				  	lcd_display("Press: 'F1' for GEN #1,  'F3' for GEN #3\0",2,1);
				  	lcd_display("       'F2' for GEN #2,  'F4' for GEN #4\0",3,1);
					  lcd_display("       'SYSTEM_STATUS' to ABORT TRANSFER\0",4,1);

					if (MULTIGEN_BESECKE)  //M/Y Bounty Hunter  //gp_status_word3 & 0x0004 (high) to configure.
						lcd_display(strOwner,3,35);
//wip//////////////////////////////////////////////////wip////////////////////////////////wip//
				}
				else
				{
				//autodetect logic
					if (gp_state.genstat1==ONLINE) // GEN #1 CONTACTOR CONFIRM
					{
						if (gp_state.invstat==OFFLINE)
						{
							if (gp_state.genstat2==OFFLINE)
							{	
							    lcd_display("GENSET #1 AUTO-SELECTED\0",1,1);
								  delay(1000);		//wait a second.
								new_key=19;
								goto select_Gen1;
							}
//else multiple-gensets online
							else goto selectGen;
						}
						else goto selectGen;
					}
					else if (gp_state.genstat2==ONLINE)
					{
						if (gp_state.invstat==OFFLINE)
						{
							if (gp_state.genstat1==OFFLINE)
							{
							    lcd_display("GENSET #2 AUTO-SELECTED\0",1,1);
								  delay(1000);		//wait a second.
								new_key=20;
								goto select_Gen2;
							}
//else multiple-gensets online
							else goto selectGen;
						}
						else goto selectGen;
					}
					else
					{
selectGen:
					  lcd_display("SELECT GENERATOR FOR TRANSFER: \0",1,1);
					  lcd_display("Press: 'F1' for GENSET #1,\0",2,1);
					  lcd_display("       'F2' for GENSET #2,\0",3,1);
					  lcd_display("       'SYSTEM_STATUS' to ABORT TRANSFER\0",4,1);
					}
				}//endelseif (MULTIGEN_OPTION)

guess_again:
				  delay(1000);		//wait a second.
				  wait_for_key();

				  if (new_key==19)			//F1 has been pressed.
				  {
select_Gen1:
						g2c=YES;	//added for HYBRID_STO

						genset = GEN1;
						if (MULTIGEN_OPTION)
						{
							multiGen = 1;
							set_gen_mux(1);
						}
						LogEvent(Ev_G1_MAST,0);
				  }
				  else if (new_key==20)		//F2 has been pressed.
				  {
select_Gen2:
					if (MULTIGEN_OPTION)
					{
						multiGen = 2;
						if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
							genset = GEN2;
						else
							genset = GEN1;
						set_gen_mux(2);
					}
					else
					{
						genset = GEN2;
					}
					LogEvent(Ev_G2_MAST,0);
				  }

				  else if (new_key==8 && MULTIGEN_OPTION)		//F3 has been pressed.
				  {
select_Gen3:
						multiGen = 3;
						genset = GEN1;
						set_gen_mux(3);					
				  }
				  else if (new_key==9 && MULTIGEN_OPTION)		//F4 has been pressed.
				  {
select_Gen4:
						multiGen = 4;
						if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
							genset = GEN2;
						else
							genset = GEN1;

	  					set_gen_mux(4);

				  }
				  else if (new_key==18)		//SYSTEM STATUS has been pressed.
				  {
					return abort_power_on=0;
				  }
				  else
				  {
					goto guess_again;	//loop for valid key.
				  }

setMultiGen:
				if (MULTIGEN_OPTION)	//confirm selected genset matches genset presently online.
				{
					if (multiGen==1)
					{
						if ((optcon_status_2 & 0x0F00) != 0x0100) return abort_power_on=40;
					}
					if (multiGen==2)
					{
						if ((optcon_status_2 & 0x0F00) != 0x0200) return abort_power_on=40;
					}
					if (multiGen==3)
					{
						if ((optcon_status_2 & 0x0F00) != 0x0400) return abort_power_on=40;
					}
					if (multiGen==4)
					{
						if ((optcon_status_2 & 0x0F00) != 0x0800) return abort_power_on=40;
					}
				}
				else
				{
				  if ((genset==GEN1 && gp_state.genstat1!=ONLINE) || (genset==GEN2 && gp_state.genstat2!=ONLINE))
				  {
					return abort_power_on=40;
				  }
				}
			}
			else
			if (genset==0)
			{
				genset = gp_state.active_display = 1;
			}
		}

//		if (MULTIGEN_OPTION)
//		{
			switch (multiGen)
			{
				case 1:
						if (EXT_CB_OPTION)
						{
							do_transfer(genset,CLOSE_K4,0,OPEN_EG1);
//							do_transfer(genset,CLOSE_K4,CLOSE_EXT_CB,OPEN_EG1);
						}
						else
						{
							if (negCrossover)
								do_transfer(genset,OPEN_EG1,CLOSE_CB3,CLOSE_K4);
							else
								do_transfer(genset,CLOSE_K4,CLOSE_CB3,OPEN_EG1);
						}
						break;

				case 2:
						if (EXT_CB_OPTION)
						{
							do_transfer(genset,CLOSE_K4,0,OPEN_EG2);
//							do_transfer(genset,CLOSE_K4,CLOSE_EXT_CB,OPEN_EG2);
						}
						else
						{
							if (negCrossover)
								do_transfer(genset,OPEN_EG2,CLOSE_CB3,CLOSE_K4);
							else
								do_transfer(genset,CLOSE_K4,CLOSE_CB3,OPEN_EG2);
						}
						break;

				case 3:
						if (EXT_CB_OPTION)
						{
							do_transfer(genset,CLOSE_K4,0,OPEN_EG3);
//							do_transfer(genset,CLOSE_K4,CLOSE_EXT_CB,OPEN_EG3);
						}
						else
						{
							if (negCrossover)
								do_transfer(genset,OPEN_EG3,CLOSE_CB3,CLOSE_K4);
							else
								do_transfer(genset,CLOSE_K4,CLOSE_CB3,OPEN_EG3);
						}
						break;

				case 4:
						if (EXT_CB_OPTION)
						{
							do_transfer(genset,CLOSE_K4,0,OPEN_EG4);
//							do_transfer(genset,CLOSE_K4,CLOSE_EXT_CB,OPEN_EG4);
						}
						else
						{
//wip//////////////////////////////////////////////////wip////////////////////////////////wip//
//#define CLOSE_EG1		31
//#define CLOSE_EG2		32
//#define CLOSE_EG3		33
							if (MULTIGEN_BESECKE)	//XFR to MIXED MODE
							{
								if (gensetOnline())
								{
									genset = process_MULTIGEN_REM_XFR_REQ();	//used to find running gen to sync to.

				  					set_gen_mux(genset);

									if ((0<genset) && (genset<4))	//qualify GEN_ID
										do_transfer(genset,(genset+30),CLOSE_K4,OPEN_CB3);
									else
										do_transfer(autoTransferGenset,(autoTransferGenset+30),CLOSE_K4,OPEN_CB3);
								}
								else
								{
									do_transfer(autoTransferGenset,(autoTransferGenset+30),CLOSE_K4,OPEN_CB3);
//									goto justTurnOn;
								}
							}
							else
							{
								do_transfer(genset,CLOSE_K4,0,OPEN_EG4);
							}
						}
						break;

				case 0:
				default:
						break;
			}

			if (MULTIGEN_OPTION)
				return abort_power_on;						//RETURN HERE if MULTIGEN.
//		}

		if ((gp_state.inpstat1==ONLINE) && ((gp_state.genstat1==ONLINE && genset==GEN1) || (gp_state.genstat2==ONLINE && genset==GEN2)))	//do we have a generator online?
		{//SYNC. OSC w/GENERATOR ZERO-CROSSING, VERIFY FREQ, VOLTAGE, PHASE MATCHING.
			if (!abort_power_on)
			{
				set_TIP_CMD();
				transfer_in_progress=YES;

				clear_screen();
				lcd_display("GENSET #   TO CONVERTER TRANSFER\0",2,5);
				lcd_display(". . . POWER TRANSFER IN PROGRESS . . .\0",3,2);

				if (genset==GEN2)
				{
					lcd_display("2\0",2,13);

					if(!voltage_in_range(SYS,SYS_sync,vmin,vmax))
					{
						sync_status = NOT_OK;
						abort_power_on=33;
					}

					if(!frequency_in_range(SYS,SYS_sync,fmin,fmax))
					{
						sync_status = NOT_OK;
						abort_power_on=44;
					}
				}
				else
				{
					if (multiGen)
					{
						if (MULTIGEN_BESECKE && multiGen==4)  //M/Y Bounty Hunter  //gp_status_word3 & 0x0004 (high) to configure.
							lcd_display(strOwner,2,4);
						else
							lcd_display(itoa(multiGen),2,13);
					}
					else
					{
						lcd_display("1\0",2,13);
					}

					if(!voltage_in_range(GEN,GEN_sync,vmin,vmax))
					{
						sync_status = NOT_OK;
						abort_power_on=33;
					}

					if(!frequency_in_range(GEN,GEN_sync,fmin,fmax))
					{
						sync_status = NOT_OK;
						abort_power_on=44;
					}
				}

				if(sync_status == NOT_OK)
				{
					if (!abort_power_on) abort_power_on=44;
					return abort_power_on;
				}

				if(genset==GEN2)
				{
					sync_id=SYS_sync;
					gp_van_gen=gp_Van[SYS];
					gp_freq_gen=gp_Freq[SYS];
				}
				else
				{
					genset=GEN1;
					gp_van_gen=gp_Van[GEN];
					gp_freq_gen=gp_Freq[GEN];
				}
#ifdef TEST_FIXTURE
lcd_display_value(gp_freq_gen,4,16,"%4.1f\0");
#endif //TEST_FIXTURE

				if (GP_OUTPUT_FORM==ONE_PHASE)	//v1.57 compensate for line-line gen volts.
				{
					gp_van_gen = gp_van_gen * 0.5;
				}

				gp_van_gen = gp_van_gen+(0.02*gp_van_gen);


				LogEvent(Ev_XFR_TO_CONV,0);

				set_TIP_CMD();
				transfer_in_progress=YES;

				slew_output(0.5, gp_freq_gen+0.1, gp_van_gen);	// Fout 1/2Hz above Fgen.

//
//SYNC CONVERTER:GENERATOR SYNC.
//
				if(sync_osc_with_gen(genset)==NOT_OK)	//SYNC CONVERTER:GENERATOR SYNC.
				{
					if (!abort_power_on) abort_power_on=2;
					return abort_power_on;
				}

				slew_output(0.002, gp_freq_gen, gp_van_gen);	// Vout 2% ABOVE Vgen, Fout=Fgen.

///////////////////////////////////////////////////////////////////
				init_PWM2(9,0,mach.xfr_dutyCycle);				//unsigned(bits,align,duty)
				LogEvent(Ev_HIGH_Z,1);	//HIGH
//				debug = 2;
				Xfer_Imped_Ramp_Status.bits.Imped_Comp = 0;
				Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 0;
//////////////////////////////////////////////////////////////////

				if (SIX_CONTACTOR_CONFIG)
				{
					if(genset==GEN2)
					{
						GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0020; 	//set GEN_2_CB_CLOSE in image.
						GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0010; 	//set GEN_2_CB_OPEN in image.
						gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
					}
					else //genset==GEN1
					{
						GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0080; 	//set GEN_1_CB_CLOSE in image.
						GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0040; 	//set GEN_1_CB_OPEN in image.
						gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
					}
		
					delay(50);
					remote_off_flag=NO;
					set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.

					enable_output();	//deasserts EXT_CB CLOSE (strobe high-low)
//					debug = 7;
					GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0002; 	//set REM_OUT_OFF in image.
					gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.

//hold EXT_CB's CLOSE HIGH
					delay(50);
					GP_CONTROL_WORD = GP_CONTROL_WORD | 0x4000; //set EXT_CB_CLOSE in image.
					GP_CONTROL_WORD = GP_CONTROL_WORD | 0x2000; //set EXT_CB_OPEN in image.
					gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
//					debug = 1;

				}
				else
				{
					remote_off_flag=NO;
					
					if (EXT_CB_OPTION) {		//DHK 10142015: V9.03
						set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
						enable_output();
					}
					else {
						enable_output();
						set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
					}
				}

///////////////////////////////////////////////////////////////////
//TRANSFER LOAD BY REDUCING PROG.Zo WHILE MONITORING Current & HVDC ...(WOULD HAPPEN HERE).
///////////////////////////////////////////////////////////////////

				lcd_display("CONVERTER CONTACTOR CLOSED.             \0",3,1);
				lcd_display("CONVERTER AND GENERATOR BOTH ONLINE . . \0",4,1);
				
				delayWithFreqLock(generator_transfer_delay,0); 

				Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 1;
						
				if (TIE_CONF_PRESENT)
				{
					if (tieBreaker_CLOSED())
					{
						set_GEN_CBs_OPEN();	//disable GENERATORS, open both generator contactors.
						gp_state.genstat2=OFFLINE;
						gp_state.genstat1=OFFLINE;
					}
					else	//TB CB Open
					{
						if (genset==2)
						{
							set_GEN_2_CB_OPEN();	//toggle "GEN_2_CB_OPEN" signal high then low.
							gp_state.genstat2=OFFLINE;
						}
						else	//genset==1
						{
							set_GEN_1_CB_OPEN();	//toggle "GEN_1_CB_OPEN" signal high then low.
							gp_state.genstat1=OFFLINE;
						}
					}
				}
				else	//(not)TIE_CONF_PRESENT
				{
					set_GEN_CBs_OPEN();	//disable GENERATORS, open both generator contactors.
					gp_state.genstat2=OFFLINE;
					gp_state.genstat1=OFFLINE;
				}

				if (SIX_CONTACTOR_CONFIG)
				{
//	delayWithFreqLock(external_cb_delay+50 in enable_output());
					enable_output();	//deasserts EXT_CB CLOSE (strobe high-low)
//					debug = 9;
				}

				lcd_display("WAITING FOR GENERATOR CONTACTOR TO OPEN\0",3,1);  

				k=0;
				if(genset==GEN2)
				{
					while (Lock && (Gen2conf) && !abort_power_on && (k<transfer_timeout))
					{
						if (deltaT5gen2 > 0)				
						{					 
							gp_freq_gen = TICK_COUNT/(float)deltaT5gen2;	//calculate frequency from tick count.
						}

						if (gp_freq_gen>maxGenFreq) maxGenFreq=gp_freq_gen;
						if (gp_freq_gen<minGenFreq) minGenFreq=gp_freq_gen;

						if (!VOUT_OK)	//V_OUT_OK went south!
						{
							Lock=0;
							abort_power_on=24;
						}

						if(45<=gp_freq_gen && gp_freq_gen<=65)
						{
							pgm_edit.Freq = gp_freq_gen;
							exec_osc_pgm();
						}
						else
						{
							Lock=0;
							abort_power_on=44;
						}

						k++;
					}
				}
				else	//GENSET #1
				{
					while (Lock && (Gen1conf) && !abort_power_on && (k<transfer_timeout))
					{
						if (deltaT5gen1 > 0)				
						{					 
							gp_freq_gen = TICK_COUNT/(float)deltaT5gen1;	//calculate frequency from tick count.
						}

						if (gp_freq_gen>maxGenFreq) maxGenFreq=gp_freq_gen;
						if (gp_freq_gen<minGenFreq) minGenFreq=gp_freq_gen;

						if (!VOUT_OK)	//V_OUT_OK went south!
						{
							Lock=0;
							abort_power_on=24;
						}

						if(45<=gp_freq_gen && gp_freq_gen<=65)
						{
							pgm_edit.Freq = gp_freq_gen;
							exec_osc_pgm();
						}
						else
						{
							Lock=0;
							abort_power_on=44;
						}

						k++;

#ifdef TEST_FIXTURE
lcd_display_value(gp_freq_gen,4,16,"%4.1f\0");
#endif //TEST_FIXTURE
					}
				}

				if((genset==GEN1 && (!Gen1conf)) || (genset==GEN2 && (!Gen2conf)))
				{
					lcd_display("GENERATOR CONTACTOR OPEN.               \0",3,1);
					lcd_display("TRANSFER TO CONVERTER COMPLETE.         \0",4,1);
					last_transfer_status = (unsigned char) OK;
//					Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;			//Reset the Impedance, it is in 10msec loop	
//					pause(2000);				//Commented out DHK 10032014
				}
				else
				{
					abort_transfer(17);
				}
			} //END IF NOT ABORT

			Lock=NO;
			gp_mode=NORMAL_MODE;
			special_function=NO;
		}
		else
		{	//just do it. v1.33
justTurnOn:
				g2c=YES;				//added for HYBRID_STO
				set_TIP_CMD();
				transfer_in_progress=YES;

			if (gp_state.inpstat1==ONLINE && gp_state.sysstat==OK)
			{
				Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 1;
						

				if (TIE_CONF_PRESENT)
				{
					if (tieBreaker_CLOSED())
					{
						set_GEN_CBs_OPEN();	//disable GENERATORS, open both generator contactors.
						gp_state.genstat2=OFFLINE;
						gp_state.genstat1=OFFLINE;
					}
					else	//TB CB Open
					{
						if (genset==2)
						{
							set_GEN_2_CB_OPEN();	//toggle "GEN_2_CB_OPEN" signal high then low.
							gp_state.genstat2=OFFLINE;
						}
						else	//genset==1
						{
#ifndef WESTPORT50
							set_GEN_1_CB_OPEN();	//toggle "GEN_1_CB_OPEN" signal high then low.
							gp_state.genstat1=OFFLINE;
#endif //WESTPORT50
						}
					}
				}
				else	//(not)TIE_CONF_PRESENT
				{
					if (!SWITCHGEAR_INTERFACE_OPTION && !MULTIGEN_BESECKE)
					{
						set_GEN_CBs_OPEN();	//disable GENERATORS, open both generator contactors.
						gp_state.genstat2=OFFLINE;
						gp_state.genstat1=OFFLINE;
					}
				}

				remote_off_flag=NO;
				
				if (EXT_CB_OPTION) {		//DHK 10142015: V9.03
					set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
					enable_output();
				}
				else {
					enable_output();
					set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
				}

				last_transfer_status = (unsigned char) OK;
			}
			else
			{
			//SOMETHING IS NOT RIGHT FOR SEAMLESS TRANSFERS TO CONVERTER.
				if (gp_state.sysstat)
				{
					abort_transfer(2);
				}
				else
				{
					abort_transfer(14);
				}
			}
		}

		pgm_edit=def_pgm;

		if (gp_state.vout_range==HIGH_RANGE) 
		{
			pgm_edit.Va = newVoltage * 0.5;
			pgm_edit.Vb = newVoltage * 0.5;
			pgm_edit.Vc = newVoltage * 0.5;
		}
		else
		{
			pgm_edit.Va = newVoltage;
			pgm_edit.Vb = newVoltage;
			pgm_edit.Vc = newVoltage;
		}
		Lock=NO;
		gp_mode=NORMAL_MODE;
		special_function=NO;

//		delay(generator_transfer_delay);	//12112014 DHK: Commented out; GENERATOR_CONTACTOR_CONFIRMATION signal received normally.v1_26

	}//endelse SEAMLESS_TRANSFER==YES

	set_meter_mux(temp_daq_mux_id, temp_sync_mux_id);	//restore daq & sync mux settings.

	return abort_power_on;
}
/*---------------------------------------------------------------------------*/
int transfer2generator()
{
  unsigned k=0;
  unsigned temp_daq_mux_id=daq_mux_id;
  unsigned temp_sync_mux_id=sync_mux_id;
  unsigned sync_id=GEN_sync;
  int genset=gp_state.active_display;
  int multiGen=0;
  float gp_van_gen=GP_OUTPUT_VOLTAGE;
  float gp_freq_gen=GP_OUTPUT_FREQUENCY;
  int bus=get_busState();
  int sync_status=OK;
  float vmin=GP_OUTPUT_VOLTAGE - 0.2 * GP_OUTPUT_VOLTAGE;
  float vmax=GP_OUTPUT_VOLTAGE + 0.2 * GP_OUTPUT_VOLTAGE;
  float fmin=GP_OUTPUT_FREQUENCY - 0.1 * GP_OUTPUT_FREQUENCY;
  float fmax=GP_OUTPUT_FREQUENCY + 0.1 * GP_OUTPUT_FREQUENCY;

	if (AEM_OPTION)	
	{
		abort_transfer(6);
		return 0;
	}

	if (SEAMLESS_TRANSFER==NO)
	{
		abort_transfer(5);
		return 0;
	}

	if (HYBRID_STO)							//No Transfer To Generator, handled via switchgear
	{
		abort_transfer(19);
		return 0;
	}

	if (TECHNEL_OPTION && _getbit(P7,4)) 	//Note: conflict with GEN_1_MATCH_CMD !!
	{										//Issued when GEN LOAD > CONVERTER CAPACITY.
		if (inhibit_transfer==ENABLED)
		{
			abort_transfer(8);
			return 0;
		}
	}
	
//
//check transfer Mode.
//
	if (manual_mode())			//check if manual_mode().
	{
		abort_transfer(25);
		return 0;
	}

	if (!MULTIGEN_BESECKE && gp_state.genstat1==ONLINE) return abort_power_on=30;
//	if (gensetOnline())
//	{
//		if (MULTIGEN_BESECKE)
//		{
//			if ((gp_state.invstat!=ONLINE) && !tieBreaker_CLOSED())
//			{
//				set_TIE_BREAKER_CB_CLOSE();	//CB3 //toggle "TIE_BREAKER_CB_CLOSE" signal high then low.
//			}
//		}
//		else
//		{
////			if (get_busState2()!=10)
//				return (abort_power_on=30);
//		}
//	}

	if (!MULTIGEN_BESECKE && gp_state.genstat2==ONLINE)
	{
		if (TIE_CONF_PRESENT)
		{
			if (!tieBreaker_CLOSED())
			{
				genset = remote_genset = GEN1;
				goto MultiGenContinue;
			}
			else
			{
				return (abort_power_on=30);
			}
		}
		else
		{
			return (abort_power_on=30);
		}
	}


	if (MULTIGEN_OPTION && gensetOnline())
	{
//wip//////////////////////////////////////////////////wip////////////////////////////////wip//
		if (MULTIGEN_BESECKE)	//Bs is: 2, 6,or 10
		{
			if (tieBreaker_CLOSED())	//Bs (2)	(GEN BOTH BUSES)
			{
			//
			//SHORE POWER ON test?
			//
				if (gp_state.inpstat1!=ONLINE)
				{
					abort_transfer(14);
					return 0;
				}
				genset = remote_genset = GEN4;
				new_key=9;
				goto MultiGen4Continue;
			}
			else						//Bs (6,10)	(GEN 1/2-BUS, MIXED-BUS)
			{
				if ((gp_state.invstat!=ONLINE))
				{
					set_TIE_BREAKER_CB_CLOSE();	//CB3 //toggle "TIE_BREAKER_CB_CLOSE" signal high then low.
					return 0;
				}
				else
				{ 		//Mixed Mode to Generator Only transfer (10 to 2)
//					genset = remote_genset = process_MULTIGEN_REM_XFR_REQ();	//used to find running gen to sync to.
					if (gp_state.busState==10) goto MixedContinue;
					else 
					{
						abort_transfer(19);	//bad transfer request?
						return 0;
					}
				}
			}
		}
//wip//////////////////////////////////////////////////wip////////////////////////////////wip//
		else
		{
			return abort_power_on=30;
		}
	}

	if (DUAL_GENSET==NO)
	{
		genset=GEN1;
	}
	else
	{
		if (c2g)
		{
			genset = remote_genset;

			if (MULTIGEN_OPTION)	//confirm selected genset matches genset presently online.
			{
				multiGen = genset;
//wip v2.47
				if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
				{
					if (multiGen==1 || multiGen==3)	//SWITCHGEAR_INTERFACE_OPTION
					{
						genset = GEN1;
					}
					else	//multiGen==2 || multiGen==4
					{
						genset = GEN2;
					}
				}
				else
  				{
					genset = GEN1;
				}
				set_gen_mux(multiGen);
				goto MultiGenContinue;
			}
		}
		else
		{
			  clear_screen();

			  lcd_display("SELECT GENERATOR FOR TRANSFER: \0",1,1);
			  lcd_display("Press: 'F1' for GENSET #1,\0",2,1);
			  lcd_display("       'F2' for GENSET #2,\0",3,1);
			  lcd_display("       'SYSTEM_STATUS' to ABORT TRANSFER\0",4,1);

			if (MULTIGEN_OPTION)
			{
				if (gensetOnline())
				{	
					if(bus==10)
					{
MixedContinue:
						multiGen = process_MULTIGEN_REM_XFR_REQ();	//used to find running gen to sync to.

						if (multiGen==0) multiGen = autoTransferGenset;

	  					set_gen_mux(multiGen);

						switch (multiGen)
						{
							case 2:
							case 4:
									genset = GEN2;	break;
							case 1:
							case 3:
							default:
									genset = GEN1;	break;
						}

						goto MultiGenContinue;
					}
					else if (bus==2)
					{
						goto MultiGen4Continue;
					}
					else
					{
						return 0;
					}
				}
				else
				{
				  	lcd_display("Press: 'F1' for GEN #1,  'F3' for GEN #3\0",2,1);
				  	lcd_display("       'F2' for GEN #2,  'F4' for GEN #4\0",3,1);

					if (MULTIGEN_BESECKE)  //M/Y Bounty Hunter  //gp_status_word3 & 0x0004 (high) to configure.
						lcd_display(strOwner,3,35);
				}
			}

guess_again2:
			  pause(1000);		//wait a second.
			  wait_for_key();

			  if (new_key==19)			//F1 has been pressed.
			  {
					genset = GEN1;
					if (MULTIGEN_OPTION)
					{
						multiGen = 1;
						set_gen_mux(1);
					}
					LogEvent(Ev_G1_MAST,0);
			  }
			  else if (new_key==20)		//F2 has been pressed.
			  {
				if (MULTIGEN_OPTION)
				{
					multiGen = 2;
					if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
						genset = GEN2;
					else
						genset = GEN1;
					set_gen_mux(2);
				}
				else
				{
					genset = GEN2;
				}
				LogEvent(Ev_G2_MAST,0);
			  }
			  else if (MULTIGEN_OPTION && (new_key==8))		//F3 has been pressed.
			  {
					multiGen = 3;
					genset = GEN1;
					set_gen_mux(3);
			  }
			  else if (MULTIGEN_OPTION && (new_key==9))		//F4 has been pressed.
			  {
//wip//////////////////////////////////////////////////wip////////////////////////////////wip//
				if (MULTIGEN_BESECKE)
				{
MultiGen4Continue:
					multiGen = 4;
					genset = process_MULTIGEN_REM_XFR_REQ();	//used to find running gen to sync to.

					if (genset==0) genset = autoTransferGenset;

  					set_gen_mux(genset);

					switch (genset)
					{
						case 2:
						case 4:
								genset = GEN2;	break;
						case 1:
						case 3:
						default:
								genset = GEN1;	break;
					}
				}
				else
//wip//////////////////////////////////////////////////wip////////////////////////////////wip//
				{
					multiGen = 4;
					if (SWITCHGEAR_INTERFACE_OPTION)
						genset = GEN2;
					else
						genset = GEN1;
  					set_gen_mux(4);
				}
			  }
			  else if (new_key==18)		//SYSTEM STATUS has been pressed.
			  {
				return abort_power_on=0;
			  }
			  else
			  {
				goto guess_again2;	//loop for valid key.
			  }
		}

		if (genset==0)
		{
			genset = gp_state.active_display = 1;
		}
	}

MultiGenContinue:

	if (SEAMLESS_TRANSFER==NO)
	{
		abort_transfer(5);
		return 0;
	}
	else	//DO SEAMLESS TRANSFER.
	{
#ifdef TEST_FIXTURE
gp_state.invstat=ONLINE;
gp_state.genstat1=OFFLINE;
#endif //TEST_FIXTURE

		if (MULTIGEN_BESECKE && multiGen!=4)
		{
			if (bus==0 || bus==11)	//just close selected gen cb
			{
				//JUST CLOSE_GEN CB
				if (multiGen==2)
				{
					set_GEN_2_CB_CLOSE();	//enable GENERATOR, close generator contactor.
				}
				else
				{
					set_GEN_1_CB_CLOSE();	//enable GENERATOR, close generator contactor.
				}
				return 0;
			}
		}
//v3.06 wip begin
//			transferPower(transferRequest);
//v3.06 wip end

		clear_screen();
		lcd_display("CONVERTER TO  GENSET #   TRANSFER\0",2,5);

		if(genset==GEN1)
		{
			lcd_display("1\0",2,27);
		}
		if(genset==GEN2)
		{
			lcd_display("2\0",2,27);
		}
		if (multiGen)
		{
			if (MULTIGEN_BESECKE)  //M/Y Bounty Hunter  //gp_status_word3 & 0x0004 (high) to configure.
				lcd_display(strOwner,2,19);
			else
				lcd_display(itoa(multiGen),2,27);
		}

		if (MULTIGEN_OPTION)
			startGenset(multiGen);		//fully handles qualified genstart and warm-up.
		else
			startGenset(genset);		//fully handles qualified genstart and warm-up.

		if (MULTIGEN_OPTION && gp_state.invstat==ONLINE || MULTIGEN_BESECKE) //conv may be offline if MULTIGEN_BESECKE
		{
			switch (multiGen)
			{
				case 1:
						if (EXT_CB_OPTION)
						{
							do_transfer(genset,CLOSE_EG1,0,OPEN_K4);
//							do_transfer(genset,CLOSE_EG1,OPEN_EXT_CB,OPEN_K4);
						}
						else
						{
							if (get_busState2()==10)	//Mixed Mode to Gen transfer
								do_transfer(genset,CLOSE_CB3,0,OPEN_K4);
							else
								do_transfer(genset,CLOSE_EG1,CLOSE_CB3,OPEN_K4);
						}
						break;

				case 2:
						if (EXT_CB_OPTION)
						{
							do_transfer(genset,CLOSE_EG2,0,OPEN_K4);
//							do_transfer(genset,CLOSE_EG2,OPEN_EXT_CB,OPEN_K4);
						}
						else
						{
							if (get_busState2()==10)	//Mixed Mode to Gen transfer
								do_transfer(genset,CLOSE_CB3,0,OPEN_K4);
							else
								do_transfer(genset,CLOSE_EG2,CLOSE_CB3,OPEN_K4);
						}
						break;

				case 3:
						if (EXT_CB_OPTION)
						{
							do_transfer(genset,CLOSE_EG3,0,OPEN_K4);
//							do_transfer(genset,CLOSE_EG3,OPEN_EXT_CB,OPEN_K4);
						}
						else
						{
							if (get_busState2()==10)	//Mixed Mode to Gen transfer
								do_transfer(genset,CLOSE_CB3,0,OPEN_K4);
							else
								do_transfer(genset,CLOSE_EG3,CLOSE_CB3,OPEN_K4);
						}
						break;

				case 4:
						if (EXT_CB_OPTION)
						{
							do_transfer(genset,CLOSE_EG4,0,OPEN_K4);
//							do_transfer(genset,CLOSE_EG4,OPEN_EXT_CB,OPEN_K4);
						}
						else
						{
							if (MULTIGEN_BESECKE)	//XFR to MIXED MODE
							{
//wip//////////////////////////////////////////////////wip////////////////////////////////wip//
//#define CLOSE_EG1		31
//#define CLOSE_EG2		32
//#define CLOSE_EG3		33
//								if (gensetOnline())
								{
									if (!EXTERNAL_CB_CLOSED())
									{
								//abort transfer w/msg that 2Q1 is open
										abort_transfer(23);			//NO CONVERTER POWER, converters output power is not ready
										return 0;
									}

									genset = process_MULTIGEN_REM_XFR_REQ();	//used to find running gen to sync to.
				  					set_gen_mux(genset);

									if ((0<genset) && (genset<4))	//qualify GEN_ID
										do_transfer(genset,(genset+30),CLOSE_K4,OPEN_CB3);
									else
										do_transfer(autoTransferGenset,(autoTransferGenset+30),CLOSE_K4,OPEN_CB3);
									//GENSET MUST BE RUNNING FOR TRANSFER TO MIXED MODE.
								}

//								else
//								{
//									do_transfer(autoTransferGenset,CLOSE_K4,(autoTransferGenset+30),OPEN_CB3);
//								}

							}
							else
							{
								do_transfer(genset,CLOSE_EG4,0,OPEN_K4);
							}
						}
						break;

				case 0:
				default:
						break;
			}

			return abort_power_on;
		}


		if (gp_state.invstat==ONLINE && ((gp_state.genstat1==OFFLINE && genset==GEN1) || (gp_state.genstat2==OFFLINE && genset==GEN2)))
		{
			if (!abort_power_on)
			{
				clear_screen();
				lcd_display("CONVERTER TO  GENSET #   TRANSFER\0",2,5);
				lcd_display(". . . POWER TRANSFER IN PROGRESS . . .\0",3,2);

				if(genset==GEN1)
				{
					lcd_display("1\0",2,27);
				}

				if(genset==GEN2)
				{
					lcd_display("2\0",2,27);
				}

				if (multiGen)
				{
					if (MULTIGEN_BESECKE)  //M/Y Bounty Hunter  //gp_status_word3 & 0x0004 (high) to configure.
						lcd_display(strOwner,2,19);
					else
						lcd_display(itoa(multiGen),2,27);
//					lcd_display(itoa(multiGen),2,27);
				}

				if (genset==GEN2)
				{
					if(!voltage_in_range(SYS,SYS_sync,vmin,vmax))
					{
						sync_status = NOT_OK;
					}

					if(!frequency_in_range(SYS,SYS_sync,fmin,fmax))
					{
						sync_status = NOT_OK;
					}
				}
				else
				{
					if(!voltage_in_range(GEN,GEN_sync,vmin,vmax))
					{
						sync_status = NOT_OK;
					}

					if(!frequency_in_range(GEN,GEN_sync,fmin,fmax))
					{
						sync_status = NOT_OK;
					}
				}

				if(sync_status == NOT_OK)
				{
					abort_power_on=2;
					return abort_power_on;
				}
				else //ELSEif(sync_status == OK)
			  	{

					if(genset==GEN2)
					{
						sync_id=SYS_sync;
						gp_van_gen=gp_Van[SYS];
						gp_freq_gen=gp_Freq[SYS];
					}
					else
					{
						genset=GEN1;
						gp_van_gen=gp_Van[GEN];
						gp_freq_gen=gp_Freq[GEN];
					}
#ifdef TEST_FIXTURE
lcd_display_value(gp_freq_gen,4,16,"%4.1f\0");
#endif //TEST_FIXTURE

					if (GP_OUTPUT_FORM==ONE_PHASE)	//v1.57 compensate for line-line gen volts.
					{
						gp_van_gen = gp_van_gen * 0.5;
					}

					gp_van_gen = gp_van_gen+(0.02*gp_van_gen);

					LogEvent(Ev_XFR_TO_GEN,0);

					set_TIP_CMD();
					transfer_in_progress=YES;

					slew_output(0.5, gp_freq_gen+0.1, gp_van_gen);	//slew Fout=Fgen+0.5Hz.

					if(sync_osc_with_gen(genset)==NOT_OK)
					{
						if (!abort_power_on) abort_power_on=2;
						return abort_power_on;
					}

					slew_output(0.002, gp_freq_gen, gp_van_gen);	//slew Vout=Vgen+2%.

///////////////////////////////////////////////////////////////////
					init_PWM2(9,0,mach.xfr_dutyCycle);				//unsigned(bits,align,duty)
					LogEvent(Ev_HIGH_Z,1);	//HIGH
//					debug = 3;
					Xfer_Imped_Ramp_Status.bits.Imped_Comp = 0;
					Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 0;
//////////////////////////////////////////////////////////////////

					if (SIX_CONTACTOR_CONFIG)
					{
						remote_off_flag=NO;
						set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
	//hold EXT_CB's CLOSE HIGH
						GP_CONTROL_WORD = GP_CONTROL_WORD | 0x4000; //set EXT_CB_CLOSE in image.
						GP_CONTROL_WORD = GP_CONTROL_WORD | 0x2000; //set EXT_CB_OPEN in image.
						gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
						
//						debug = 2;

					}

					if(genset==GEN2)
					{
						if (SIX_CONTACTOR_CONFIG)
						{
							LogEvent(Ev_GEN2_CB_CLOSE,0);
							GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0020; 	//set GEN_2_CB_CLOSE in image.
							GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0010; 	//set GEN_2_CB_OPEN in image.
							gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
						}
						else
						{
							set_GEN_2_CB_CLOSE();	//enable GENERATOR, close generator contactor.
							gp_state.genstat2=ONLINE;	
						}
					}
					else	//GENSET #1
					{
						if (SIX_CONTACTOR_CONFIG)
						{
							LogEvent(Ev_GEN1_CB_CLOSE,0);
							GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0080; 	//set GEN_1_CB_CLOSE in image.
							GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0040; 	//set GEN_1_CB_OPEN in image.
							gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
						}
						else
						{
							set_GEN_1_CB_CLOSE();	//enable GENERATOR, close generator contactor.
							gp_state.genstat1=ONLINE;	
						}
					}

					lcd_display("WAITING FOR GENERATOR CONTACTOR TO CLOSE\0",3,1);  

					k=0;
					if(genset==GEN2)
					{
						while (Lock && (!Gen2conf) && (!abort_power_on) && (k<transfer_timeout))	//wait for GENSET #2 CONF
						{
							if (deltaT5gen2 > 0)				
							{					 
								gp_freq_gen = TICK_COUNT/(float)deltaT5gen2;	//calculate frequency from tick count.
							}

							if (gp_freq_gen>maxGenFreq) maxGenFreq=gp_freq_gen;
							if (gp_freq_gen<minGenFreq) minGenFreq=gp_freq_gen;

							if (!VOUT_OK)	//V_OUT_OK went south!
							{
								Lock=0;
								abort_power_on=24;
							}

							if(45<=gp_freq_gen && gp_freq_gen<=65)
							{
								pgm_edit.Freq = gp_freq_gen;
								exec_osc_pgm();
							}
							else
							{
								Lock=0;
								abort_power_on=44;
							}

							k++;
						}
					}
					else	//GENSET #1
					{
						while (Lock && (!Gen1conf) && (!abort_power_on) && (k<transfer_timeout))	//wait for GENSET #1 CONF
						{
							if (deltaT5gen1 > 0)				
							{					 
							   gp_freq_gen = TICK_COUNT/(float)deltaT5gen1;	//calculate frequency from tick count.
							}

							if (gp_freq_gen>maxGenFreq) maxGenFreq=gp_freq_gen;
							if (gp_freq_gen<minGenFreq) minGenFreq=gp_freq_gen;

							if (!VOUT_OK)	//V_OUT_OK went south!
							{
								Lock=0;
								abort_power_on=24;
							}

							if(45<=gp_freq_gen && gp_freq_gen<=65)
							{
								pgm_edit.Freq = gp_freq_gen;
								exec_osc_pgm();
							}
							else
							{
								Lock=0;
								abort_power_on=44;
							}
							k++;
						} 										//END WHILE
					}										//END ELSE GENSET2

					if((genset==GEN1 && Gen1conf) || (genset==GEN2 && Gen2conf))
					{
						lcd_display("GENERATOR CONTACTOR CLOSED.             \0",3,1);
						lcd_display("GENERATOR AND CONVERTER BOTH ONLINE . . \0",4,1);
						delayWithFreqLock(generator_transfer_delay,0); 
						lcd_display("TRANSFER TO GENERATOR COMPLETE.         \0",4,1);
						last_transfer_status = (unsigned char) OK;
					}
					else
					{
				 		output_OFF();			//v1.93e
						remote_off_flag=YES;	//v1.93e

						abort_transfer(18);
					}

					if (SIX_CONTACTOR_CONFIG)
					{
//output_off() call above deasserted EXT_CB CLOSE so output trips off.
						GP_CONTROL_WORD = GP_CONTROL_WORD & 0xBFFF;		//clear EXT_CB_CLOSE in image.
						GP_CONTROL_WORD = GP_CONTROL_WORD & 0xDFFF;	//clear EXT_CB_OPEN in image.
						gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.

				 		output_OFF();

//deassert GEN_CLOSE gen should stay on.
						delayWithFreqLock(50,0);										//delay 600ms.
						set_GEN_CBs_OPEN();
					}
					else
					{
				 		output_OFF();
					}


					clear_TIP_CMD();
					transfer_in_progress=NO;	//was after output_OFF() till v1.93e

					lcd_display("CONVERTER CONTACTOR OPEN.               \0",3,1);
			
				}	//END IF NOT ABORT
			}
			pgm_edit=def_pgm;

			if (gp_state.vout_range==HIGH_RANGE) 
			{
				pgm_edit.Va = newVoltage * 0.5;
				pgm_edit.Vb = newVoltage * 0.5;
				pgm_edit.Vc = newVoltage * 0.5;
			}
			else
			{
				pgm_edit.Va = newVoltage;
				pgm_edit.Vb = newVoltage;
				pgm_edit.Vc = newVoltage;
			}

			Lock = NO;
			gp_mode = NORMAL_MODE;
			special_function=NO;
		}
		else	//BAD STATUS.
		{
			last_transfer_status = 21;
			abort_transfer(21);
//			abort_power_on=0;
		} //END BAD STATUS

//		restore_output();	//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!

	}	//END DO SEAMLESS TRANSFER

	initialize_generator_status();
	set_meter_mux(temp_daq_mux_id, temp_sync_mux_id);	//restore daq & sync mux settings.

	return abort_power_on;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void output_OFF()	 
{
	disable_output();	// open output contactor.
	delayWithFreqLock(50,0);		//give K4 time to settle.
	restore_output();	//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!
}
/*---------------------------------------------------------------------------*/
///////////////////////////////////////////////////////////////////
//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!
///////////////////////////////////////////////////////////////////
void restore_output()	//bring output to nominal state.
{
	Lock=NO;

	if (EXT_OSC_OPTION)	return;

	pgm_edit=def_pgm;

	if (MULTIGEN_OPTION)	//then no Synchroscope, so, don't confuse meas.c
	{
		CC22IC = 0;			// disable GEN2_sync interrupt.
		CC29IC = 0;			// disable GEN1_sync interrupt.
	}
	if (!(display_type==GENERATOR_POWER && display_function==F4))
	{
		CC22IC = 0;			// disable GEN2_sync interrupt.
		CC29IC = 0;			// disable GEN1_sync interrupt.
	}

	if (gp_state.vout_range==HIGH_RANGE) 
	{
		pgm_edit.Va = newVoltage * 0.5;
		pgm_edit.Vb = newVoltage * 0.5;
		pgm_edit.Vc = newVoltage * 0.5;
	}
	else
	{
		pgm_edit.Va = newVoltage;
		pgm_edit.Vb = newVoltage;
		pgm_edit.Vc = newVoltage;
	}
	gp_mode=NORMAL_MODE;

	LogEvent(Ev_FREE,0);
	
	if (!Xfer_Imped_Ramp_Status.bits.Imped_Comp)
		Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;			//Reset the Impedance, it is in 10msec loop	

	slew_output(0.5, GP_OUTPUT_FREQUENCY, newVoltage);

#ifdef DHK_04242014
	init_PWM2(mach.pwm2_bits,mach.pwm2_align,mach.pwm2_dutyCycle);		//unsigned(bits,align,duty)
	LogEvent(Ev_HIGH_Z,2);	//LOW
#endif

	if (AEM_OPTION)
	{
			clear_aem_GEN_MATCH_CONF();		//deassert(low) GEN_MATCH_CONF.
	}

	ALC_mode=AGC_state;	//v1.99
//	ALC_mode=ON;	//v1.29

	special_function=NO;
	initialize_generator_status();

	if (MULTIGEN_OPTION)
	{
		if (display_type==GENERATOR_POWER) {
			switch (get_gen_id())
			{
				case 4:	display_function=F4;	break;
				case 3:	display_function=F7;	break;
				case 2:	display_function=F2;	break;
				case 1:	
				default:	display_function=F1;	break;
			}
		}
		else
			display_function=F1;
	}
	else
	{
		if(gp_state.active_display==GEN2)
		{
			display_function=F2;
		}
		else	//GENSET #1
		{
//			if (!MULTIGEN_OPTION)
				display_function=F1;
		}
	}
///////////////////////////////////////////////////////////////////
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
//Note: due to legacy interface circuits, duty is inverted at CPU outputs.
//		i.e. 100% duty results in a LOW (0Vdc) output & 0% duty is HIGH (5Vdc).
/*---------------------------------------------------------------------------*/
void init_PWM1(unsigned bits, unsigned align, unsigned duty)	//initialize PWM1 to given duty & resolution.
{
	//// 8<bits<16, align=0=edge, align=1=center, 0<duty<100 ////

	unsigned period = 1 << (bits-align);	//
	float fperiod = (float) period;
	float fduty = (float) duty;
	float fpw2 = (fperiod*fduty)/100.0;

		_putbit(0,PWMCON0,1);					//stop PWM1 counter.
		_nop();
		
		if(align) 	_putbit(1,PWMCON1,5);		//align center.
		else		_putbit(0,PWMCON1,5);		//align edge.
		_nop();

		PP1	=	period - 1;						//set PWM period.
		_nop();

		PW1	=	(unsigned) fpw2;				//set PWM pulse width.
		_nop();

		if(duty==100)		PT1 = 0;			//LOW output, PT1 < PW1.
		else if(duty==0)	PT1 = PW1;			//HIGH output, PT1 >= PW1.
		else
		{
			_putbit(1,PWMCON0,1);				//run PWM1 counter.
			PT1 = 0;							//set initial timer value.
		}
}
/*---------------------------------------------------------------------------*/
//#define PTR2 2
//#define PEN2 4
//#define PM2  6
/*---------------------------------------------------------------------------*/
void init_PWM2(unsigned bits, unsigned align, unsigned duty)	//initialize PWM2 to given duty & resolution.
{
	//// 8<bits<16, align=0=edge, align=1=center, 0<duty<100 ////

	unsigned period = 1 << (bits-align);	//
	float fperiod = (float) period;
	float fduty = (float) duty;
	float fpw2 = (fperiod*fduty)/100.0;

	Active_DutyCycle = duty;

		_putbit(0,PWMCON0,2);					//stop PWM2 counter.
		_nop();
		
		if(align) 	_putbit(1,PWMCON1,6);		//align center.
		else		_putbit(0,PWMCON1,6);		//align edge.
		_nop();

		PP2	=	period - 1;						//set PWM period.
		_nop();

		PW2	=	(unsigned) fpw2;				//set PWM pulse width.
		_nop();

		if(duty==100)		PT2 = 0;			//LOW output, PT2 < PW2.
		else if(duty==0)	PT2 = PW2;			//HIGH output, PT2 >= PW2.
		else
		{
			_putbit(1,PWMCON0,2);				//run PWM2 counter.
			PT2 = 0;							//set initial timer value.
		}
}
/*---------------------------------------------------------------------------*/
void init_PWM3(unsigned bits, unsigned align, unsigned duty)	//initialize PWM3 to given duty & resolution.
{
	//// 8<bits<16, align=0=edge, align=1=center, 0<duty<100 ////

	unsigned period = 1 << (bits-align);	//
	float fperiod = (float) period;
	float fduty = (float) duty;
	float fpw3 = (fperiod*fduty)/100.0;

		_putbit(0,PWMCON0,3);					//stop PWM counter.
		_nop();
		
		if(align) 	_putbit(1,PWMCON1,7);		//align center.
		else		_putbit(0,PWMCON1,7);		//align edge.
		_nop();

		PP3	=	(unsigned) period - 1;			//set PWM period.
		_nop();

		PW3	=	(unsigned) fpw3;				//set PWM pulse width.
		_nop();

		if(duty==100)		PT3 = 0;			//LOW output, PT < PW.
		else if(duty==0)	PT3 = PW3;			//HIGH output, PT >= PW.
		else
		{
			_putbit(1,PWMCON0,3);				//run PWM3 counter.
			PT3 = 0;							//set initial timer value.
		}
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
int test_generator_interface(int genset)	//manage gen interface test.
{
  int genOnline=FALSE;

	if (AEM_OPTION)	
	{
		abort_transfer(6);
		return 0;
	}

  abort_power_on=0;

  clear_screen();

  lcd_display("GENERATOR TRANSFER TESTS       \0",1,1);
  lcd_display("Press: 'F1' for GENSET #1 TEST,\0",2,1);
  lcd_display("       'F2' for GENSET #2 TEST,\0",3,1);

  if (GEN2GEN_OPTION)
  {
	  lcd_display("       'F3' for GEN-TO-GEN TEST\0",4,1);
  }
  else if (MULTIGEN_OPTION)
  {
	lcd_display("Press: 'F1' for GEN #1,  'F3' for GEN #3\0",2,1);
	lcd_display("       'F2' for GEN #2,  'F4' for GEN #4\0",3,1);
	lcd_display("       'F5' for Control Option PCB TEST \0",4,1);
	if (MULTIGEN_BESECKE)  //M/Y Bounty Hunter  //gp_status_word3 & 0x0004 (high) to configure.
		lcd_display(strOwner,3,35);
  }

guess_again3:

  delay(1000);		//wait a second.
  wait_for_key();

  if (new_key==19)			//F1 has been pressed.
  {
	genset = GEN1;
	LogEvent(Ev_G1_MAST,0);
  }
  else if (new_key==20)		//F2 has been pressed.
  {
	genset = GEN2;
	LogEvent(Ev_G2_MAST,0);
  }
  else if (new_key==8 && GEN2GEN_OPTION)
  {
  	control_option_pcb_test_screen();	//test CONTROL and STATUS on CONTROL_OPTION_PCB.
	setGenSpeedMux(NONE);	//NEW_G2G_OPTION speed-trim MUX.
	return abort_power_on=0;
  }
  else if (new_key==8 && MULTIGEN_OPTION)
  {
	genset = 3;
  }
  else if (new_key==9 && MULTIGEN_OPTION)
  {
	genset = 4;
  }
  else if (new_key==10 && MULTIGEN_OPTION)	//F5
  {
  	control_option_pcb_test_screen();	//test CONTROL and STATUS on CONTROL_OPTION_PCB.
//	setGenSpeedMux(NONE);	//NEW_G2G_OPTION speed-trim MUX.
	return abort_power_on=0;
  }
  else if (new_key==18)		//SYSTEM_STATUS has been pressed.
  {
	return abort_power_on=0;
  }
  else
  {
	goto guess_again3;	//loop for valid key.
  }

	clear_screen();
  lcd_display("\
GENSET #1 TRANSFER TESTS CONFIRMATION  \n\
Press 'GENERATOR POWER' to start test. \n\
Press any other key to abort test.     \0",1,1);

	if(genset==4)
	{
		lcd_display("4\0",1,9);

		if (MULTIGEN_OPTION)
			set_gen_mux(4);
	}
	else if(genset==3)
	{
	  	lcd_display("3\0",1,9);

		if (MULTIGEN_OPTION)
			set_gen_mux(3);
	}
	else if(genset==GEN2)
	{
	  	lcd_display("2\0",1,9);

		if (MULTIGEN_OPTION)
			set_gen_mux(2);
	}
	else
	{
		if (MULTIGEN_OPTION)
			set_gen_mux(1);
	}

  pause(500);		//wait a 1/2second.
  wait_for_key();

  if (new_key==11)	//GENERATOR_POWER has been pressed.
  {
	lcd_display("Please wait a few seconds ...\0",4,1);

	if (MULTIGEN_OPTION)
	{
		pause(2000);	//delay one second (generator_mux_delay) times.
	}

	show_test_generator_screen(genset);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		abort_power_on=0;

		strobe_kybd_for_slew();	//sets 'new_key=0' and polls kybd for active key-down event.

		initialize_generator_status();
		check_gp_status();

		if(genset==4)
		{
			if(eGen4_CLOSED())
			{
				genOnline = TRUE;
			}
			else
			{
				genOnline = FALSE;
			}
		}
		else if(genset==3)
		{
			if(eGen3_CLOSED())
			{
				genOnline = TRUE;
			}
			else
			{
				genOnline = FALSE;
			}
		}
		else if(genset==2)
		{
			if (MULTIGEN_OPTION)
			{
				if(eGen2_CLOSED())
				{
					genOnline = TRUE;
				}
				else
				{
					genOnline = FALSE;
				}
			}
			else
			{
				if(Gen2conf)
				{
					genOnline = TRUE;
				}
				else
				{
					genOnline = FALSE;
				}
			}
		}
		else	//genset==GEN1
		{
			if (MULTIGEN_OPTION)
			{
				if(eGen1_CLOSED())
				{
					genOnline = TRUE;
				}
				else
				{
					genOnline = FALSE;
				}
			}
			else
			{
				if(Gen1conf)
				{
					genOnline = TRUE;
				}
				else
				{
					genOnline = FALSE;
				}
			}
		}

		if(genOnline)
		{
			lcd_display("Online  \0",4,13);
		}
		else
		{
			lcd_display("Offline \0",4,13);
		}

		switch (new_key)
		{
			case 19:	//F1:
					if (gp_state.invstat==OFFLINE)
					{
						if (genset==GEN2)
						{
							if(MULTIGEN_OPTION)
							{
								set_GEN_1_CB_OPEN();	//disable GENERATOR, open generator contactor.
								gp_state.genstat1=OFFLINE;
							}
							else
							{
								set_GEN_2_CB_OPEN();	//disable GENERATOR, open generator contactor.
								gp_state.genstat2=OFFLINE;
							}
						}
						else
						{
							set_GEN_1_CB_OPEN();	//disable GENERATOR, open generator contactor.
							gp_state.genstat1=OFFLINE;
						}
						pause(200);
					}
					else
					{
						lcd_display("CONVERTER IS NOT OFFLINE--INVALID TEST.\0",4,1);
						pause(3000);
						show_test_generator_screen(genset);
					}
					break;

			case 20:	//F2:
					if (gp_state.invstat==OFFLINE)
					{
						if (genset==GEN2)
						{
							if(MULTIGEN_OPTION)
							{
								if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
								{
									set_GEN_2_CB_CLOSE();	//enable GENERATOR, close generator contactor.
								}
								else
								{
									set_GEN_1_CB_CLOSE();	//enable GENERATOR, close generator contactor.
									gp_state.genstat1=ONLINE;
								}
							}
							else
							{
								set_GEN_2_CB_CLOSE();	//enable GENERATOR, close generator contactor.
								gp_state.genstat2=ONLINE;
							}
						}
						else if (genset==GEN3 && (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE))
						{
							if(MULTIGEN_OPTION)
							{
								set_GEN_1_CB_CLOSE();	//enable GENERATOR, close generator contactor.
							}
						}
						else if (genset==GEN4 && (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE))
						{
							if(MULTIGEN_OPTION)
							{
								set_GEN_2_CB_CLOSE();	//enable GENERATOR, close generator contactor.
							}
						}
						else
						{
							set_GEN_1_CB_CLOSE();	//enable GENERATOR, close generator contactor.
							gp_state.genstat1=ONLINE;
						}
						pause(200);
					}
					else
					{
						lcd_display("CONVERTER IS NOT OFFLINE--INVALID TEST.\0",4,1);
						pause(3000);
						show_test_generator_screen(genset);
					}
					break;

			case 9:		//F4:
#ifdef TEST_FIXTURE
					if (abort_power_on!=2)
#endif //TEST_FIXTURE
					{
						set_TIP_CMD();
						transfer_in_progress=YES;
						sync2gen(genset);
						clear_TIP_CMD();
						transfer_in_progress=NO;
					}
					show_test_generator_screen(genset);
					break;

			case 10:	//F5
					restore_output();	//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!
					break;

			default:
					break;
		}//endswitch
	}//endwhile

	set_GEN_CBs_OPEN();	//disable GENERATORS, open both generator contactors.
	gp_state.genstat2=OFFLINE;
	gp_state.genstat1=OFFLINE;
  }//endif

  new_key=0;
  display_type = SYSTEM_STATUS; 
  display_function = F1;
  gp_mode=NORMAL_MODE;

  return abort_power_on;
}
/*---------------------------------------------------------------------------*/
void show_test_generator_screen(int genId)
{
	lcd_display("\
GENSET #1 TRANSFER TESTS               \n\
Press F1 to open G1 CB, F2 to close.   \n\
Press F4 to match G1, F5 to release.   \n\
Status : G1         &  Unmatched       \0",1,1);

	if(genId==2)
	{
		lcd_display("2\0",1,9);
		lcd_display("2\0",2,19);
		lcd_display("2\0",3,20);
		lcd_display("2\0",4,11);
	}
	else if(genId==3)
	{
		lcd_display("3\0",1,9);
		lcd_display("3\0",2,19);
		lcd_display("3\0",3,20);
		lcd_display("3\0",4,11);
	}
	else if(genId==4)
	{
		lcd_display("4\0",1,9);
		lcd_display("4\0",2,19);
		lcd_display("4\0",3,20);
		lcd_display("4\0",4,11);
	}
}
/*---------------------------------------------------------------------------*/
void transfer_inhibit_control_screen()	//TECHNEL_OPTION.
{
	clear_screen();
	lcd_display("SEAMLESS TRANSFER INHIBIT\0",1,8);
	lcd_display(" INHIBIT \0",2,1);
//	lcd_display("SYSTEM STATUS to exit screen.\0",4,1);
//	lcd_display("Enable  Disable                    Exit \0",4,1);
	lcd_display("Enable  Disable          PrevScr   Exit \0",4,1);
	
	new_key = 0;

	while ((new_key!=18) && (new_key != 9))	//exit test when SYSTEM_STATUS is pressed.
	{
		if(inhibit_transfer==ENABLED)
		{
			lcd_display(" ENABLED \0",2,10);
		}
		else
		{
			lcd_display("DISABLED \0",2,10);
		}

		put_cursor(2,10);
		delay(250);
		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
					inhibit_transfer = ENABLED;		//inhibit if overload.
					break;

			case 20:	//F2:
					inhibit_transfer = DISABLED;	//do not inhibit.
					break;

			case 9:	new_key = 9;
					break;

					
			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(500);	
						break;

			default:
					break;
		}
	}
//	new_key = 0;
}
/*---------------------------------------------------------------------------*/
void generator_transfer_control_screen()	//ASEA3 v1.27.
{
	clear_screen();
	lcd_display("GENERATOR/CONVERTER CROSSOVER TIME\0",1,4);
	lcd_display(" TRANSFER DELAY      MILLISECONDS\0",2,1);
//	lcd_display("F1 to increase, F2 to decrease,",3,1);
//	lcd_display("SYSTEM STATUS to exit screen.",4,1);
//	lcd_display(" More    Less                      Exit \0",4,1);
//	lcd_display(" More    Less  GenCB               Exit \0",4,1);

	if (MULTIGEN_BESECKE)
		lcd_display(" More    Less      +/-             Exit \0",4,1);
	else
		lcd_display(" More    Less                      Exit \0",4,1);
		
	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display("    \0",2,17);	//clear field

		if (generator_transfer_delay < 1000)
		{
			lcd_display(itoa(generator_transfer_delay),2,18);
		}
		else
		if (generator_transfer_delay < 100)
		{
			lcd_display(itoa(generator_transfer_delay),2,19);
		}
		else
		if (generator_transfer_delay < 10)
		{
			lcd_display(itoa(generator_transfer_delay),2,20);
		}
		else
		{
			lcd_display(itoa(generator_transfer_delay),2,17);
		}
		
		if (MULTIGEN_BESECKE) {
			if (negCrossover)
				lcd_display(" Negative Crossover Active\0",3,1);
			else
				lcd_display(" Positive Crossover Active\0",3,1);
		}
		else
			lcd_display("                                        \0",3,1);
			

		put_cursor(2,20);
		delay(500);

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:

					if (generator_transfer_delay<3000)
					{
						generator_transfer_delay = generator_transfer_delay + 100;
					}
					break;

			case 20:	//F2:

					if (generator_transfer_delay>0)
					{
						generator_transfer_delay = generator_transfer_delay - 100;
					}
					else
					{
						generator_transfer_delay = 0;
					}
					break;

			case 8:	//F3:
					if (MULTIGEN_BESECKE) {										
						if (negCrossover)
						{
							negCrossover=FALSE;
						}
						else
						{
							negCrossover=TRUE;
						//	if (generator_transfer_delay<250) generator_transfer_delay=250;
						}
					}
					else
						negCrossover=FALSE;
					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(500);	
						break;

			default:
					break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
void Gen_Start_Control_Screen_Redisplay()
{
	clear_screen();
	lcd_display("GENERATOR AUTOSTART CONTROL\0",1,7);
	lcd_display(" TRANSFER DELAY     SECONDS    \0",2,1);
	lcd_display(" AUTOSTART           \0",3,1);

}

void generator_start_control_screen()	//TECHNEL_OPTION or GEN_START_OPTION.
{
  int edit_field=0;

	Gen_Start_Control_Screen_Redisplay();

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display("   \0",2,17);	//clear field

		if (generator_on_delay < 10)
		{
			lcd_display(itoa(generator_on_delay),2,18);
		}
		else
		{
			lcd_display(itoa(generator_on_delay),2,17);
		}
	
		if(gen_start_cmd_enable==ENABLED)
		{
			lcd_display(" ENABLED \0",3,12);
		}
		else
		{
			lcd_display("DISABLED \0",3,12);
		}

		if (edit_field==0)
		{
//			if(gen_start_cmd_enable==ENABLED)
				lcd_display(" More    Less    Cursor  NextScr   Exit \0",4,1);
//			else
//				lcd_display(" More    Less    Cursor            Exit \0",4,1);
				
////////////////////////////////////////////////////////////////
		//				 1234567 1234567 12345678 1234567 1234567
////////////////////////////////////////////////////////////////
			put_cursor(2,17);
			pause(500);
		}
		else
		{
//			if(gen_start_cmd_enable==ENABLED)
				lcd_display("Enable  Disable  Cursor  NextScr   Exit \0",4,1);
//			else
//				lcd_display("Enable  Disable  Cursor            Exit \0",4,1);
				
			put_cursor(3,12);
			pause(500);
		}

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
					if (edit_field==0)
					{
//						if (generator_on_delay<180) generator_on_delay++;
						if (generator_on_delay<300) generator_on_delay++;
					}
					else
					{
						gen_start_cmd_enable = ENABLED;		//inhibit if overload.
					}
					break;

			case 20:	//F2:
					if (edit_field==0)
					{
						if (generator_on_delay>5) generator_on_delay--;
					}
					else
					{
						gen_start_cmd_enable = DISABLED;		//inhibit if overload.
					}
					break;

			case 8:		//F3:
					if (edit_field==0)
					{
						edit_field = 1;	//toggle edit fieldt.
					}
					else
					{
						edit_field = 0;	//toggle edit fieldt.
					}
					break;

			case 9:		//F4:
//					if(gen_start_cmd_enable==ENABLED) {
						transfer_inhibit_control_screen();
						
						if (new_key == 9)
							Gen_Start_Control_Screen_Redisplay();
//					}
					break;
					
			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						break;

			default:
					break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
int	sync2gen(int gensetX)	//sync until F5 key pressed.
{
//unsigned my_minutes=minutes;
  unsigned k=0;
  unsigned sync_id=GEN_sync;
  volatile float gp_van_gen=GP_OUTPUT_VOLTAGE;
  volatile float gp_freq_gen=GP_OUTPUT_FREQUENCY;
  float vmin=GP_OUTPUT_VOLTAGE - 0.2 * GP_OUTPUT_VOLTAGE;
  float vmax=GP_OUTPUT_VOLTAGE + 0.2 * GP_OUTPUT_VOLTAGE;
  float fmin=GP_OUTPUT_FREQUENCY - 0.1 * GP_OUTPUT_FREQUENCY;
  float fmax=GP_OUTPUT_FREQUENCY + 0.1 * GP_OUTPUT_FREQUENCY;
  volatile int genset=1;

	if (!SEAMLESS_TRANSFER)
	{
		abort_transfer(5);
		return 0;
	}

	if (MULTIGEN_OPTION)
	{
		if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
		{
			set_gen_mux(gensetX);

			if (gensetX==2 || gensetX==4)
			{
				genset = 2;
//				set_meter_mux(SYS,SYS_sync);	//done in set_gen_mux(gensetX) when SWITCHGEAR_INTERFACE_OPTION.
			}
			else
			{
				genset = 1;
//				set_meter_mux(GEN,GEN_sync);	//done in set_gen_mux(gensetX) when SWITCHGEAR_INTERFACE_OPTION.
			}
		}
		else
		{
			genset = 1;
			set_meter_mux(GEN,GEN_sync);
		}
	}
	else
	{
		genset = gensetX;
	}

	initialize_generator_status();

	if (genset==GEN2)
	{
		if(!voltage_in_range(SYS,SYS_sync,vmin,vmax))
		{
			abort_power_on=2;
		}

		if(!frequency_in_range(SYS,SYS_sync,fmin,fmax))
		{
			abort_power_on=44;
		}
	}
	else	//genset==GEN1.
	{
		if(!voltage_in_range(GEN,GEN_sync,vmin,vmax))
		{
			abort_power_on=2;
		}

		if(!frequency_in_range(GEN,GEN_sync,fmin,fmax))
		{
			abort_power_on=44;
		}
	}

	if(!abort_power_on)
  	{
		if(genset==GEN2)
		{
			sync_id=SYS_sync;
			gp_van_gen=gp_Van[SYS];
			gp_freq_gen=gp_Freq[SYS];
		}
		else
		{
			gp_van_gen=gp_Van[GEN];
			gp_freq_gen=gp_Freq[GEN];
		}

		if (GP_OUTPUT_FORM==ONE_PHASE)	//v1.57 compensate for line-line gen volts.
		{
			gp_van_gen = gp_van_gen * 0.5;
		}

//
//debug
		lcd_display_value(gp_freq_gen,4,34,"%4.1fHz\0");
//

		lcd_display("SEEKING\0",1,32);

		gp_van_gen = gp_van_gen+(0.01*gp_van_gen);

		slew_output(0.5, gp_freq_gen+0.1, gp_van_gen);	//slew Fout=Fgen+0.1Hz.

		if(sync_osc_with_gen(genset)==OK)
		{
			slew_output(0.002, gp_freq_gen, gp_van_gen);	//slew Vout=Vgen+2%.

			lcd_display("Matched   \0",4,24);
			lcd_display("       \0",1,32);

///////////////////////////////////////////////////////////////////
//	if (gp_state.vout_range==HIGH_RANGE)
//	{
//		init_PWM2(9,0,75);				//unsigned(bits,align,duty)
//	}
//	else
//	{
		init_PWM2(9,0,mach.xfr_dutyCycle);				//unsigned(bits,align,duty)
		LogEvent(Ev_HIGH_Z,1);	//HIGH
//		debug = 4;
		Xfer_Imped_Ramp_Status.bits.Imped_Comp = 0;
		Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 0;
//	}
//////////////////////////////////////////////////////////////////

			k=0;
			while ((new_key!=18) && Lock && (!abort_power_on) && (new_key!=10))	//wait for SYSTEM_STATUS key press.
			{
				if (genset==GEN2)
				{
					if (deltaT5gen2 > 0)				
					{					 
						gp_freq_gen = TICK_COUNT/(float)deltaT5gen2;	//calculate frequency from tick count.
					}
				}
				else
				{
					if (deltaT5gen1 > 0)				
					{					 
						gp_freq_gen = TICK_COUNT/(float)deltaT5gen1;	//calculate frequency from tick count.
					}
				}

				if (gp_freq_gen>maxGenFreq) maxGenFreq=gp_freq_gen;
				if (gp_freq_gen<minGenFreq) minGenFreq=gp_freq_gen;

				if (!VOUT_OK)	//V_OUT_OK went south!
				{
					Lock=0;
					abort_power_on=24;
				}
//
//debug
		lcd_display_value(gp_freq_gen,4,34,"%4.1f\0");
//

				if((fmin<=gp_freq_gen) && (gp_freq_gen<=fmax))
				{
//					slew_output(0.002, gp_freq_gen, gp_van_gen);	// Vout 2% ABOVE Vgen, Fout=Fgen.
					pgm_edit.Freq = gp_freq_gen;
					exec_osc_pgm();
				}
				else
				{
					restore_output();
					
					return (abort_power_on=44);
				}

				strobe_kybd_for_slew();	//sets 'new_key=0' and polls kybd for active key-down event.

	//			if(k++>n) Lock=0;	//timeout after n counts.
	//			if(minutes>my_minutes+2) Lock=0;	//timeout after 2-3 minutes.
			}


		}
		else
		{
			if (!abort_power_on) abort_power_on=2;
		}
	}

	lcd_display("Unmatched \0",4,24);

	restore_output();	//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!
						
//	Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;			//Reset the Impedance, it is in 10msec loop	

//	set_meter_mux(temp_daq_mux_id, temp_sync_mux_id);	//restore daq & sync mux settings.

	if(abort_power_on==2)
	{
		clear_screen();
		lcd_display("TRANSFER ABORTED, UNABLE TO SYNC.\0",1,1);
		lcd_display("GENSET  VOLTAGE OR FREQUENCY BAD.\0",2,1);
		lcd_display("DISPLAY  SELECTED  GENSET  METER.\0",3,1);
		lcd_display("CHECK CONVERTER/GENERATOR WIRING.\0",4,1);
		pause(3000);
		if(new_key!=18) pause(3000);
		abort_power_on=0;
	}

	if(abort_power_on==4)
	{
		abort_transfer(1);
	}

	return abort_power_on;
}
/*---------------------------------------------------------------------------*/
void transfer_guard_control_screen()	//ASEA3 v1.27.
{
	clear_screen();
	lcd_display("TRANSFER FREQUENCY ROLLOFF CONTROL\0",1,4);
	lcd_display(" GUARD WINDOW =       T5 counts.\0",2,1);
//	lcd_display("F1 to increase, F2 to decrease,",3,1);
//	lcd_display("SYSTEM STATUS to exit screen.",4,1);
	lcd_display(" More    Less                      Exit \0",4,1);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display("    \0",2,17);	//clear field

		if (generator_transfer_rolloff < 10000)
		{
			lcd_display(itoa(generator_transfer_rolloff),2,18);
		}
		else
		if (generator_transfer_rolloff < 100)
		{
			lcd_display(itoa(generator_transfer_rolloff),2,19);
		}
		else
		if (generator_transfer_rolloff < 10)
		{
			lcd_display(itoa(generator_transfer_rolloff),2,20);
		}
		else
		{
			lcd_display(itoa(generator_transfer_rolloff),2,17);
		}

		put_cursor(2,20);
		pause(500);

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:

					if (generator_transfer_rolloff<30000)
//					if (generator_transfer_rolloff<410)
					{
						generator_transfer_rolloff = generator_transfer_rolloff + 10;
					}
					break;

			case 20:	//F2:

					if (generator_transfer_rolloff>10)
					{
						generator_transfer_rolloff = generator_transfer_rolloff - 10;
					}
					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(500);	
						break;

			default:
					break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
int	aem_sync2gen(int genset)	//sync untill GEN_MATCH_COMMAND goes low.
{
  unsigned verbose=TRUE;
  unsigned k=0;
  unsigned temp_daq_mux_id=daq_mux_id;
  unsigned temp_sync_mux_id=sync_mux_id;
  unsigned sync_id=GEN_sync;
  float gp_van_gen=GP_OUTPUT_VOLTAGE;
  float gp_freq_gen=GP_OUTPUT_FREQUENCY;
  float vmin=GP_OUTPUT_VOLTAGE - 0.2 * GP_OUTPUT_VOLTAGE;
  float vmax=GP_OUTPUT_VOLTAGE + 0.2 * GP_OUTPUT_VOLTAGE;
  float fmin=GP_OUTPUT_FREQUENCY - 0.1 * GP_OUTPUT_FREQUENCY;
  float fmax=GP_OUTPUT_FREQUENCY + 0.1 * GP_OUTPUT_FREQUENCY;

	if (verbose)
	{
		clear_screen();
		lcd_display(". . . POWER TRANSFER IN PROGRESS . . .\0",3,2);
		lcd_display("Generator:          &  Unmatched       \0",4,1);

		if(genset==GEN2) lcd_display("GENSET #2\0",1,1);

		if (SEAMLESS_TRANSFER==NO)
		{
			abort_transfer(5);
			return 0;
		}
	}

	if (AEM2_OPTION)
	{
//	Power Panels version of switchgear transfers.
		if (gp_state.inpstat1!=ONLINE) return (abort_power_on=55);	//AEM2
	}
	else 
	{
		if (gp_state.invstat!=ONLINE) return (abort_power_on=56);	//AEM
	}

	check_gp_status();
	initialize_generator_status();

	if (verbose)
	{
		if(genset==GEN2)
		{
			if(Gen2conf)
			{
				lcd_display("Online  \0",4,13);
			}
			else
			{
				lcd_display("Offline \0",4,13);
			}
		}
		else	//genset==GEN1
		{
			if(Gen1conf)
			{
				lcd_display("Online  \0",4,13);
			}
			else
			{
				lcd_display("Offline \0",4,13);
			}
		}
	}

	if (!gp_state.aemGenMatchCmd) abort_power_on=5;

	if (genset==GEN2)
	{
		if(!voltage_in_range(SYS,SYS_sync,vmin,vmax))
		{
			abort_power_on=6;
		}

		if(!frequency_in_range(SYS,SYS_sync,fmin,fmax))
		{
			abort_power_on=7;
		}
	}
	else	//genset==GEN1.
	{
		if(!voltage_in_range(GEN,GEN_sync,vmin,vmax))
		{
			abort_power_on=6;			
		}

		if(!frequency_in_range(GEN,GEN_sync,fmin,fmax))
		{
			abort_power_on=7;
		}
	}

	if(!abort_power_on)
  	{

		if(genset==GEN2)
		{
			sync_id=SYS_sync;
			gp_van_gen=gp_Van[SYS];
			gp_freq_gen=gp_Freq[SYS];
		}
		else
		{
			genset=GEN1;
			gp_van_gen=gp_Van[GEN];
			gp_freq_gen=gp_Freq[GEN];
		}

		if (GP_OUTPUT_FORM==ONE_PHASE)	//v1.57 compensate for line-line gen volts.
		{
			gp_van_gen = gp_van_gen * 0.5;
		}

		if (verbose)
		{
			lcd_display("SEEKING\0",1,32);
		}

		gp_van_gen = gp_van_gen+(0.01*gp_van_gen);

		slew_output(0.5, gp_freq_gen+0.1, gp_van_gen);	//slew Fout=Fgen+0.5Hz.

		if(sync_osc_with_gen(genset)==OK)
		{
			slew_output(0.002, gp_freq_gen, gp_van_gen);	//slew Vout=Vgen+2%.

///////////////////////////////////////////////////////////////////
			init_PWM2(9,0,mach.xfr_dutyCycle);				//unsigned(bits,align,duty)
			LogEvent(Ev_HIGH_Z,1);	//HIGH
//			debug = 5;
			Xfer_Imped_Ramp_Status.bits.Imped_Comp = 0;
			Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 0;
//////////////////////////////////////////////////////////////////

			set_aem_GEN_MATCH_CONF();						//assert GEN_MATCH_CONF.
///////////////////////////////////////////////////////////////////

			if (verbose)
			{
				lcd_display("Matched   \0",4,24);
				lcd_display("       \0",1,32);
			}

			k=0;
			while (gp_state.aemGenMatchCmd && Lock && (!abort_power_on))	//wait for SYSTEM_STATUS key press.
			{
				if (genset==GEN2)
				{
					if (deltaT5gen2 > 0)				
					{					 
						gp_freq_gen = TICK_COUNT/(float)deltaT5gen2;	//calculate frequency from tick count.
					}
				}
				else
				{
					if (deltaT5gen1 > 0)				
					{					 
						gp_freq_gen = TICK_COUNT/(float)deltaT5gen1;	//calculate frequency from tick count.
					}
				}

				if (gp_freq_gen>maxGenFreq) maxGenFreq=gp_freq_gen;
				if (gp_freq_gen<minGenFreq) minGenFreq=gp_freq_gen;

				if (!VOUT_OK)	//V_OUT_OK went south!
				{
					Lock=0;
					abort_power_on=24;
				}

				if((45.0<=gp_freq_gen) && (gp_freq_gen<=66.0))
				{
					pgm_edit.Freq = gp_freq_gen;
					exec_osc_pgm();

				}
				else
				{
					pgm_edit=def_pgm;

					if (gp_state.vout_range==HIGH_RANGE) 
					{
						pgm_edit.Va = newVoltage * 0.5;
						pgm_edit.Vb = newVoltage * 0.5;
						pgm_edit.Vc = newVoltage * 0.5;
					}
					else
					{
						pgm_edit.Va = newVoltage;
						pgm_edit.Vb = newVoltage;
						pgm_edit.Vc = newVoltage;
					}
					Lock=0;
					abort_power_on=44;
				}

#ifdef TEST_FIXTURE
				gp_state.aemGenMatchCmd =  ((gp_configuration_word & 0x4000)!=0);	// FOR DEBUG ONLY!! GENERATOR_MATCH_COMMAND.
#else
//v2.29				gp_state.aemGenMatchCmd =  _getbit(P5,9);	// GENERATOR_MATCH_COMMAND.
#endif //~TEST_FIXTURE

	//			if(k++>n) Lock=0;	//timeout after n counts.
	//			if(minutes>my_minutes+2) Lock=0;	//timeout after 2-3 minutes.
		
//v2.29			if (EXT_CB_OPTION)
				{
					check_gp_status();	//respond to CONVERTER POWER button events.
				}

				if (verbose)
				{
					if(genset==GEN2)
					{
						if(Gen2conf)
						{
							lcd_display("Online  \0",4,13);
						}
						else
						{
							lcd_display("Offline \0",4,13);
						}
					}
					else	//genset==GEN1
					{
						if(Gen1conf)
						{
							lcd_display("Online  \0",4,13);
						}
						else
						{
							lcd_display("Offline \0",4,13);
						}
					}
				}//endif verbose
			}//endwhile
		}//endif(sync_osc_with_gen(genset)==OK)
		else
		{
			if(verbose)
			{
				clear_screen();
				lcd_display("TRANSFER ABORTED, UNABLE TO SYNC.\0",1,1);
				lcd_display("SYNC STATUS IS  ** NOT OK **     \0",2,1);
				lcd_display("DISPLAY  SELECTED  GENSET  METER.\0",3,1);
				lcd_display("CHECK CONVERTER/GENERATOR WIRING.\0",4,1);
				pause(3000);
				if(new_key!=18) pause(3000);
				abort_power_on=0;
			}
		}
	}

	if (verbose && !abort_power_on) lcd_display("Unmatched \0",4,24);

	restore_output();	//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!

	set_meter_mux(temp_daq_mux_id, temp_sync_mux_id);	//restore daq & sync mux settings.

	if(verbose && abort_power_on==6)
	{
		clear_screen();
		lcd_display("TRANSFER ABORTED, UNABLE TO SYNC.\0",1,1);
		lcd_display("GENSET  VOLTAGE  BAD.\0",2,1);
		lcd_display("DISPLAY  SELECTED  GENSET  METER.\0",3,1);
		lcd_display("CHECK CONVERTER/GENERATOR WIRING.\0",4,1);
		pause(3000);
		if(new_key!=18) pause(3000);
		abort_power_on=0;
	}

	if(verbose && abort_power_on==7)
	{
		clear_screen();
		lcd_display("TRANSFER ABORTED, UNABLE TO SYNC.\0",1,1);
		lcd_display("GENSET  FREQUENCY  BAD.\0",2,1);
		lcd_display("DISPLAY  SELECTED  GENSET  METER.\0",3,1);
		lcd_display("CHECK CONVERTER/GENERATOR WIRING.\0",4,1);
		pause(3000);
		if(new_key!=18) pause(3000);
		abort_power_on=0;
	}

	if(verbose && abort_power_on==5)
	{
		clear_screen();
		lcd_display("TRANSFER ABORTED.                \0",1,1);
		lcd_display("GEN_MATCH_CMD deasserted before  \0",2,1);
		lcd_display("converter had a chance to sync.  \0",3,1);
		lcd_display("-check for noise on GEN_MATCH_CMD\0",4,1);
		pause(3000);
		if(new_key!=18) pause(3000);
		abort_power_on=0;
	}

	if(verbose && abort_power_on==4)
	{
		abort_transfer(1);
	}

	return abort_power_on;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
int do_transfer( int genset, int cbIn, int cbMid, int cbOut)
{
  int cbConfReceived=FALSE;
  unsigned temp_daq_mux_id=daq_mux_id;
  unsigned temp_sync_mux_id=sync_mux_id;
  unsigned sync_id=GEN_sync;
  float gp_van_gen=GP_OUTPUT_VOLTAGE;
  float gp_freq_gen=GP_OUTPUT_FREQUENCY;
  int sync_status=OK;
  float vmin=GP_OUTPUT_VOLTAGE - 0.2 * GP_OUTPUT_VOLTAGE;
  float vmax=GP_OUTPUT_VOLTAGE + 0.2 * GP_OUTPUT_VOLTAGE;
  float fmin=GP_OUTPUT_FREQUENCY - 0.1 * GP_OUTPUT_FREQUENCY;
  float fmax=GP_OUTPUT_FREQUENCY + 0.1 * GP_OUTPUT_FREQUENCY;

	clear_screen();
	lcd_display("CAUTION:   POWER TRANSFER IN PROGRESS   \0",1,1);

	if (MULTIGEN_OPTION)	//confirm selected genset matches genset presently online.
	{
		pause(3000);	//delay one second (generator_mux_delay) times.
		pause(2000);	//delay one second (generator_mux_delay) times.
	}

	lcd_display("GENSET #  SYNC IN PROGRESS:             \0",2,5);

	if (genset==GEN2)
	{
		if (MULTIGEN_OPTION)
		{
			switch (get_gen_id())
			{
				case 1:
						lcd_display("1\0",2,13);
						break;
				case 2:
						lcd_display("2\0",2,13);
						break;
				case 3:
						lcd_display("3\0",2,13);
						break;
				case 4:
						if (MULTIGEN_BESECKE)  //M/Y Bounty Hunter  //gp_status_word3 & 0x0004 (high) to configure.
							lcd_display(strOwner,2,5);
						else
							lcd_display("4\0",2,13);
						break;
				default:
						break;
			}
		}
		else
		{
			lcd_display("2\0",2,13);
		}

		if(!voltage_in_range(SYS,SYS_sync,vmin,vmax))
		{
			sync_status = NOT_OK;
			abort_power_on = 33;
		}

		if(!frequency_in_range(SYS,SYS_sync,fmin,fmax))
		{
			sync_status = NOT_OK;
			abort_power_on = 44;
		}
	}
	else
	{
		if (MULTIGEN_OPTION)
		{
			switch (get_gen_id())
			{
				case 1:
						lcd_display("1\0",2,13);
						break;
				case 2:
						lcd_display("2\0",2,13);
						break;
				case 3:
						lcd_display("3\0",2,13);
						break;
				case 4:
//		if (MULTIGEN_BESECKE)  //M/Y Bounty Hunter  //gp_status_word3 & 0x0004 (high) to configure.
//		else
						lcd_display("4\0",2,13);
						break;
				default:
						break;
			}
		}
		else
		{
			lcd_display("1\0",2,13);
		}

		if(!voltage_in_range(GEN,GEN_sync,vmin,vmax))
		{
			sync_status = NOT_OK;
			abort_power_on = 33;
		}

		if(!frequency_in_range(GEN,GEN_sync,fmin,fmax))
		{
			sync_status = NOT_OK;
			abort_power_on = 44;
		}
	}

	if(sync_status == NOT_OK)
	{
		if (!abort_power_on) abort_power_on=2;
		return abort_power_on;
	}

	if(genset==GEN2)
	{
		sync_id=SYS_sync;
		gp_van_gen=gp_Van[SYS];
		gp_freq_gen=gp_Freq[SYS];
	}
	else
	{
		genset=GEN1;
		gp_van_gen=gp_Van[GEN];
		gp_freq_gen=gp_Freq[GEN];
	}

	if (GP_OUTPUT_FORM==ONE_PHASE)	//v1.57 compensate for line-line gen volts.
	{
		gp_van_gen = gp_van_gen * 0.5;
	}

	gp_van_gen = gp_van_gen+(0.02*gp_van_gen);

	set_TIP_CMD();
	transfer_in_progress=YES;

	lcd_display("SEEKING\0",2,34);

	slew_output(0.5, gp_freq_gen+0.1, gp_van_gen);	// Fout 1/2Hz above Fgen.

	if(sync_osc_with_gen(genset)==NOT_OK)	//SYNC CONVERTER:GENERATOR SYNC.
	{
		if (!abort_power_on) abort_power_on=2;
		return abort_power_on;
	}

	lcd_display("LOCKED \0",2,34);

	slew_output(0.002, gp_freq_gen, gp_van_gen);	// Vout 2% ABOVE Vgen, Fout=Fgen.

	init_PWM2(9,0,mach.xfr_dutyCycle);				//unsigned(bits,align,duty)
	LogEvent(Ev_HIGH_Z,1);	//HIGH
//	debug = 6;
	Xfer_Imped_Ramp_Status.bits.Imped_Comp = 0;
	Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 0;

	lcd_display("         CIRCUIT BREAKER CLOSE COMMAND\0",3,1);	//40chars

	if (negCrossover)
		lcd_display("OPEN \0",3,26);

  	cbConfReceived = FALSE;

	switch (cbIn)
	{
		case CLOSE_CB1:
						lcd_display("GEN #1 \0",3,1);
						
						set_GEN_1_CB_CLOSE();	//enable GENERATOR, close generator contactor.

						delayWithFreqLock(transfer_timeout,CLOSE_CB1);

						if (Gen1conf)	cbConfReceived = TRUE;

						break;
		case CLOSE_CB2:
						lcd_display("GEN #2 \0",3,1);

						set_GEN_2_CB_CLOSE();	//enable GENERATOR, close generator contactor.

						delayWithFreqLock(transfer_timeout,CLOSE_CB2);

						if (Gen2conf)	cbConfReceived = TRUE;

						break;
		case CLOSE_CB3:
						lcd_display("CB3 TIE\0",3,1);

						set_TIE_BREAKER_CB_CLOSE();	//toggle "TIE_BREAKER_CB_CLOSE" signal high then low.

						delayWithFreqLock(transfer_timeout,CLOSE_CB3);

						if (tieBreaker_CLOSED())	cbConfReceived = TRUE;

						break;
		case CLOSE_K4:
						lcd_display("OUTPUT \0",3,1);
						remote_off_flag=NO;
						
						if (EXT_CB_OPTION) {		//DHK 10142015: V9.03
							set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
							enable_output();
						}
						else {
							enable_output();
							set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
						}
						
						break;

// multiGen stuff
		case CLOSE_EG1:
						lcd_display("GEN #1 \0",3,1);

						set_GEN_1_CB_CLOSE();	//enable GENERATOR, close generator contactor.

						delayWithFreqLock(transfer_timeout,CLOSE_EG1);

						if (eGen1_CLOSED())	cbConfReceived = TRUE;

						break;
		case CLOSE_EG2:
						lcd_display("GEN #2 \0",3,1);

						if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
						{
							set_GEN_2_CB_CLOSE();	//enable GENERATOR, close generator contactor.
						}
						else
						{
							set_GEN_1_CB_CLOSE();	//enable GENERATOR, close generator contactor.
						}

						delayWithFreqLock(transfer_timeout,CLOSE_EG2);

						if (eGen2_CLOSED())	cbConfReceived = TRUE;

						break;
		case CLOSE_EG3:
						lcd_display("GEN #3 \0",3,1);
						
						set_GEN_1_CB_CLOSE();	//enable GENERATOR, close generator contactor.

						delayWithFreqLock(transfer_timeout,CLOSE_EG3);

						if (eGen3_CLOSED())	cbConfReceived = TRUE;

						break;
		case CLOSE_EG4:
						lcd_display("GEN #4 \0",3,1);
						
						if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
						{
							set_GEN_2_CB_CLOSE();	//enable GENERATOR, close generator contactor.
						}
						else
						{
							set_GEN_1_CB_CLOSE();	//enable GENERATOR, close generator contactor.
						}

						delayWithFreqLock(transfer_timeout,CLOSE_EG4);

						if (eGen4_CLOSED())	cbConfReceived = TRUE;

						break;

// multiGen stuff for Lurssen Early GenCB Open (long ass delays for open CB response, so, issue it before closing K4 on xfer to converter)
		case OPEN_EG1:
						lcd_display("GEN #1 \0",3,1);

						set_GEN_1_CB_OPEN();	//disable GENERATOR, open generator contactor.

//						delayWithFreqLock(transfer_timeout,OPEN_EG1);
//						if (!eGen1_CLOSED())	cbConfReceived = TRUE;

						break;

		case OPEN_EG2:
						lcd_display("GEN #2 \0",3,1);

						set_GEN_1_CB_OPEN();	//disable GENERATOR, open generator contactor.

//						delayWithFreqLock(transfer_timeout,OPEN_EG2);
//						if (!eGen2_CLOSED())	cbConfReceived = TRUE;

						break;

		case OPEN_EG3:
						lcd_display("GEN #3 \0",3,1);

						set_GEN_1_CB_OPEN();	//disable GENERATOR, open generator contactor.

//						delayWithFreqLock(transfer_timeout,OPEN_EG3);
//						if (!eGen3_CLOSED())	cbConfReceived = TRUE;

						break;


		default:
						break;
	}

	lcd_display("                               \0",3,10);	//30chars

	if (!negCrossover)
	if (cbIn == CLOSE_K4)
	{
		lcd_display("CONVERTER OUTPUT CONTACTOR CLOSED.      \0",3,1);
	}
	else
	{
		if (cbConfReceived)
		{
			lcd_display("CIRCUIT BREAKER CLOSED.        \0",3,10);
		}
		else
		{
		//if cbIn is Close Tie then
		//if no confirm, bail on entire transfer.

			lcd_display("TIME-OUT.  NO CONFIRMATION.    \0",3,10);
			output_OFF();
			remote_off_flag=YES;	//v1.95
			return (abort_power_on = 60);	//v1.95
		}
	}

	if (MULTIGEN_OPTION)
	{
		if (gensetOnline())
		{
			if ((MULTIGEN_BESECKE || SWITCHGEAR_INTERFACE_OPTION) && genset==2)
				gp_state.genstat2 = ONLINE;
			else
				gp_state.genstat1 = ONLINE;

			set_GEN_ON_LED();
		}
		else
		{
			clear_GEN_ON_LED();
		}
	}

	lcd_display("POWER IS NOW HEAD-TO-HEAD . . .         \0",4,1);

  	cbConfReceived = FALSE;

	switch (cbMid)
	{
		case CLOSE_K4:
						lcd_display("CONVERTER OUTPUT CONTACTOR, K4, CLOSED  \0",3,1);
						remote_off_flag=NO;
						
						if (EXT_CB_OPTION) {		//DHK 10142015: V9.03
							set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
							enable_output();
						}
						else {
							enable_output();
							set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
						}
						break;
		case CLOSE_CB3:
						lcd_display("2Q2 TIE CIRCUIT BREAKER CLOSED          \0",3,1);
						if (!tieBreaker_CLOSED()) 						//TB CB Open
						{
							set_TIE_BREAKER_CB_CLOSE();	//toggle "TIE_BREAKER_CB_CLOSE" signal high then low.
						}
						break;
		case OPEN_CB4:
						lcd_display("CB4 CONV/BUS-A CIRCUIT BREAKER OPENED   \0",3,1);
						set_CONV_BUS_A_CB_OPEN();
						break;
		case CLOSE_CB4:
						lcd_display("CB4 CONV/BUS-A CIRCUIT BREAKER CLOSED   \0",3,1);
						set_CONV_BUS_A_CB_CLOSE();
						break;
		case OPEN_CB5:
						lcd_display("CB5 CONV/BUS-B CIRCUIT BREAKER OPENED   \0",3,1);
						set_CONV_BUS_B_CB_OPEN();
						break;
		case CLOSE_CB5:
						lcd_display("CB5 CONV/BUS-B CIRCUIT BREAKER CLOSED   \0",3,1);
						set_CONV_BUS_B_CB_CLOSE();
						break;
		case OPEN_EXT_CB:
						lcd_display("EXTERNAL OUTPUT CIRCUIT BREAKER OPENED  \0",3,1);
						set_EXT_CB_OPEN();
						break;
		case CLOSE_EXT_CB:
						lcd_display("EXTERNAL OUTPUT CIRCUIT BREAKER CLOSED  \0",3,1);

						resetExtCB();		//toggle "EXT_CB_OPEN" signal high then low.

						set_EXT_CB_CLOSE();
						break;
		default:
						break;
	}
///////////////////////////////////////////////////////////////////
//TRANSFER LOAD BY REDUCING PROG.Zo WHILE MONITORING Current & HVDC ...(WOULD HAPPEN HERE).
///////////////////////////////////////////////////////////////////

	delayWithFreqLock(generator_transfer_delay,0);	//Programmable delay controls 'hang' time after GEN CONF received added.v1_26

	lcd_display("         CIRCUIT BREAKER OPEN COMMANDED \0",3,1);

  	cbConfReceived = FALSE;

	switch (cbOut)
	{
		case CLOSE_K4:
						lcd_display("CONVERTER OUTPUT CONTACTOR, K4, CLOSED  \0",3,1);
						remote_off_flag=NO;
						
						if (EXT_CB_OPTION) {		//DHK 10142015: V9.03
							set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
							enable_output();
						}
						else {
							enable_output();
							set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
						}
						break;

		case OPEN_CB1:
						lcd_display("GEN #1 \0",3,1);

						set_GEN_1_CB_OPEN();	//disable GENERATOR, open generator contactor.

						delayWithFreqLock(transfer_timeout,OPEN_CB1);

						if (!Gen1conf)
							cbConfReceived = TRUE;
							

						break;
		case OPEN_CB2:
						lcd_display("GEN #2 \0",3,1);

						set_GEN_2_CB_OPEN();	//disable GENERATOR, open generator contactor.

						delayWithFreqLock(transfer_timeout,OPEN_CB2);

						if (!Gen2conf)
							cbConfReceived = TRUE;

						break;
		case OPEN_CB3:
						lcd_display("CB3 TIE\0",3,1);

						set_TIE_BREAKER_CB_OPEN();	//toggle "TIE_BREAKER_CB_OPEN" signal high then low.

						delayWithFreqLock(transfer_timeout,OPEN_CB3);

						if (!tieBreaker_CLOSED()) cbConfReceived = TRUE;

						break;
		case OPEN_K4:
						lcd_display("OUTPUT \0",3,1);
				 		output_OFF();
						break;

// multiGen stuff
		case OPEN_EG1:
						lcd_display("GEN #1 \0",3,1);

						Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 1;
						Gen_Sel = 1;
						
						set_GEN_1_CB_OPEN();	//disable GENERATOR, open generator contactor.

						delayWithFreqLock(transfer_timeout,OPEN_EG1);

						if (!eGen1_CLOSED()) {
							cbConfReceived = TRUE;
							
							if (Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait)
								Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;
						}

						break;

		case OPEN_EG2:
						lcd_display("GEN #2 \0",3,1);

						Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 1;
						Gen_Sel = 2;

						set_GEN_1_CB_OPEN();	//disable GENERATOR, open generator contactor.

						delayWithFreqLock(transfer_timeout,OPEN_EG2);

						if (!eGen2_CLOSED()) {
							cbConfReceived = TRUE;
							
							if (Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait)
								Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;
						}

						break;

		case OPEN_EG3:
						lcd_display("GEN #3 \0",3,1);

						set_GEN_1_CB_OPEN();	//disable GENERATOR, open generator contactor.

						Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 1;
						Gen_Sel = 3;
						
						delayWithFreqLock(transfer_timeout,OPEN_EG3);

						if (!eGen3_CLOSED()) {
							cbConfReceived = TRUE;
							
							if (Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait)
								Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;
						}

						break;

		case OPEN_EG4:
						lcd_display("GEN #4 \0",3,1);

						Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 1;
						Gen_Sel = 4;
						
						set_GEN_1_CB_OPEN();	//disable GENERATOR, open generator contactor.

						delayWithFreqLock(transfer_timeout,OPEN_EG4);

						if (!eGen4_CLOSED()){
							cbConfReceived = TRUE;
							
							if (Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait)
								Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;
						}

						break;

		default:
						break;
	}


	if (cbOut == OPEN_K4)
	{
   		lcd_display("CONVERTER OUTPUT CONTACTOR OPENED.      \0",3,1);
		lcd_display("                                        \0",4,1);
	}
	else if (cbOut == CLOSE_K4)
	{
		lcd_display("CONVERTER OUTPUT CONTACTOR CLOSED.      \0",3,1);
	}
	else
	{
				   //1234567890123456789012345678901234567890
		lcd_display("OUTPUT TO NOMINAL VOLTAGE AND FREQUENCY \0",4,1);

		if (cbConfReceived)
		{
			lcd_display("CIRCUIT BREAKER OPENED.        \0",3,10);
		}
		else
		{
			lcd_display("TIME-OUT.  NO CONFIRMATION.    \0",3,10);
			abort_power_on = 61;
		}
	}

	if (MULTIGEN_OPTION)
	{
		if (gensetOnline())
		{
			gp_state.genstat1 = ONLINE;
			set_GEN_ON_LED();
		}
		else
		{
			clear_GEN_ON_LED();
		}
	}

	delayWithFreqLock(generator_transfer_delay,0);	//GENERATOR_CONTACTOR_CONFIRMATION signal received normally.v1_26

// multiGen stuff for Lurssen Early GenCB Open (long ass delays for open CB response, so, issue it before closing K4 on xfer to converter)
	if (negCrossover)
	{
		switch (cbIn)
		{
			case OPEN_EG1:	Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 1;
							Gen_Sel = 1;
							
							delayWithFreqLock(transfer_timeout,OPEN_EG1);
							break;
							
			case OPEN_EG2:	Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 1;
							Gen_Sel = 2;
							
							delayWithFreqLock(transfer_timeout,OPEN_EG2);
							break;
							
			case OPEN_EG3:	Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 1;
							Gen_Sel = 3;
							
							delayWithFreqLock(transfer_timeout,OPEN_EG3);
							break;
							
			default:		break;
		}
	}

	if (MULTIGEN_OPTION)
	{
		if (gensetOnline())
		{
			gp_state.genstat1 = ONLINE;
			set_GEN_ON_LED();
		}
		else
		{
			clear_GEN_ON_LED();
		}
	}

	lcd_display("POWER TRANSFER COMPLETED        ",1,9);

	last_transfer_status = (unsigned char) OK;

	restore_output();	//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!

	set_meter_mux(temp_daq_mux_id, temp_sync_mux_id);	//restore daq & sync mux settings.

	pause(500);

	return abort_power_on;
}
/*---------------------------------------------------------------------------*/
int transferPower(int transferID)	//given transferId, will determine how to do it and do it.
{
  int busStatus;
  int k=0;
  int shutdown_flag=0;
  int genstartMode;
  float genLoad=0.0;
  float convCapacity=getCapacity();	//get converter capacity.

	if (transferID)
	{
		abort_power_on=0;
		initialize_generator_status();
		check_gp_status();		

		if (NEW_G2G_OPTION)
		{
			busStatus = get_busState2();
			genstartMode = (gen_start_cmd_enable==ENABLED);
		}
		else
		{
			busStatus = get_busState();
			genstartMode = (GEN_START_OPTION && (gen_start_cmd_enable==ENABLED));
		}
			
//if(!optconConnected()) abort;
//if(!GEN2GEN_OPTION) abort
//if(systat bad) abort

		LogEvent(Ev_BUSSTATE,(unsigned char)busStatus);
		LogEvent(Ev_XFR_REQUEST,(unsigned char)transferID);

//
//check transfer Mode.
//
		if (manual_mode())			//check if manual_mode().
		{
			return (abort_power_on=25);
		}
		else if (maintenanceMode)	//check if maintenanceMode.
		{
			return (abort_power_on=26);
		}
		else if (NEW_G2G_OPTION)
		{
			if (gp_state.inpstat1!=ONLINE && transferMode==AUTOMATIC)	//bail with msg if sp1 not online.
			{
				return (abort_power_on=55);
			}
		}
		else if (GEN2GEN_OPTION)
		{
//
//if(MASTER and SLAVE shorepower not online) abort
//
			if (gp_state.inpstat1!=ONLINE)	//bail with msg if sp1 not online.
			{
				return (abort_power_on=55);
			}

			if ((slaveK2_ON()==FALSE) && !GEN_AMPS_PRESENT)	//bail with msg if sp1 not online.
			{
				return (abort_power_on=55);
			}
		}
		else
		{
			if (gp_state.inpstat1!=ONLINE)	//bail with msg if sp1 not online.
			{
				return (abort_power_on=55);
			}
		}

//
//v2.32 wip
//
		if (GEN_AMPS_PRESENT)
		{
			if(optconPcbPresent())
			{
				if (slaveK2_ON())
				{
					convCapacity = convCapacity * 2.0;
				}
			}
		}

//
//if(busstat combination is NOT WELL-DEFINED) message-abort
//

		if (transferID==BsCONV) //GENERATOR AUTOSTART W/PROGRAMMED DELAY.
		{
			genstartMode = FALSE;
		}
//
////////////////////////////////////////////////////////
//
//  BUS STATES & transferRequest DEFINED CONSTANTS:
//
//#define BsDEAD		0	// A=off  + B=off
//#define BsGEN1		1	// A=gen1 + B=gen1
//#define BsGEN2		2 	// A=gen2 + B=gen2
//#define BsCONV		3 	// A=conv + B=conv
//#define BsG1AG2B		4 	// A=gen1 + B=gen2
//#define BsG1A			5 	// A=gen1 + B=OFF
//#define BsG2B			6 	// A=OFF  + B=gen2
//#define BsCA			7 	// A=conv + B=OFF
//#define BsCB			8 	// A=OFF  + B=conv
//#define BsG1ACB		9 	// A=gen1 + B=conv
//#define BsCAG2B		10	// A=conv + B=gen2
//
////////////////////////////////////////////////////////

		u_TIP_signal=transfer_in_progress=YES;

		switch (transferID)
		{
			case BsDEAD:	//(0}	// A=off  + B=off
						lcd_display("CAUTION:   FULL POWER BLACK-OUT PENDING \0",1,1);
						lcd_display("DO YOU REALLY REALLY WANT TO DO THIS?   \0",4,1);

						pause(2000);
//						wait_for_key();

						set_CONV_BUS_B_CB_OPEN();
						set_CONV_BUS_A_CB_OPEN();
				 		output_OFF();
						set_GEN_1_CB_OPEN();
						set_GEN_2_CB_OPEN();
						break;

			case BsGEN1:	//(1)	// A=gen1 + B=gen1
						if (genstartMode)
						{
							startGenset(GEN1);
						}

						switch (busStatus)
						{
							case BsDEAD:	//(0}	// A=off  + B=off
								set_GEN_1_CB_CLOSE();
								set_TIE_BREAKER_CB_CLOSE();
// FORCE BUS CONFIGURATION...
								set_CONV_BUS_B_CB_OPEN();
								set_CONV_BUS_A_CB_OPEN();
								break;

							case BsGEN2:
										if (NEW_G2G_OPTION)
										{
											do_G2G_transfer(GEN1,CLOSE_CB1,OPEN_CB2);
											shutdown_flag=shutdown_flag+GEN2;
										}
										else
										{
///
//CHECK CAPACITY OF CONV WILL NOT BE EXCEEDED
//
											if (GEN_AMPS_PRESENT)
											{
												set_Igen_mux(GEN2);
												set_meter_mux(SYS, SYS_sync);
												meas_all();
												genLoad = (gp_KVAa[SYS]+gp_KVAb[SYS]+gp_KVAc[SYS]);
												//compare 
												if (convCapacity < genLoad)
												{
													//abort LOAD EXCEEDS CONV. CAPACITY
													//
													return abort_power_on=29;
												}
											}

											if (do_transfer(GEN2,CLOSE_K4,CLOSE_CB5,OPEN_CB2) == OK)
												do_transfer(GEN1,CLOSE_CB1,OPEN_CB5,OPEN_K4);
										}
										break;

							case BsCONV:
// FORCE BUS CONFIGURATION...
								set_TIE_BREAKER_CB_CLOSE();
								set_CONV_BUS_A_CB_CLOSE();
								set_CONV_BUS_B_CB_OPEN();
								do_transfer(GEN1,CLOSE_CB1,OPEN_CB4,OPEN_K4);
								break;

							case BsG1AG2B:
										if (NEW_G2G_OPTION)
										{
											do_G2G_transfer(GEN2,CLOSE_CB3,OPEN_CB2);
											shutdown_flag=shutdown_flag+GEN2;
										}
										else
										{
///
//CHECK CAPACITY OF CONV WILL NOT BE EXCEEDED
//
											if (GEN_AMPS_PRESENT)
											{
												set_Igen_mux(GEN1);
												set_meter_mux(GEN, GEN_sync);
												meas_all();
												genLoad = (gp_KVAa[GEN]+gp_KVAb[GEN]+gp_KVAc[GEN]);

												set_Igen_mux(GEN2);
												set_meter_mux(SYS, SYS_sync);
												meas_all();
												genLoad = genLoad + (gp_KVAa[SYS]+gp_KVAb[SYS]+gp_KVAc[SYS]);

												//compare 
												if (convCapacity < genLoad)
												{
													//abort LOAD EXCEEDS CONV. CAPACITY
													//
													return abort_power_on=29;
												}
											}

											if (do_transfer(GEN2,CLOSE_K4,CLOSE_CB5,OPEN_CB2) == OK)
												do_transfer(GEN1,CLOSE_CB3,OPEN_CB5,OPEN_K4);
										}
										break;

//=============================================================================
							case BsG1A:		//(5) 	// A=gen1 + B=OFF
								set_TIE_BREAKER_CB_CLOSE();
								break;

							case BsG2B:		//(6) 	// A=OFF  + B=gen2
///
//CHECK CAPACITY OF CONV WILL NOT BE EXCEEDED
//
								if (GEN_AMPS_PRESENT)
								{
									set_Igen_mux(GEN1);
									set_meter_mux(GEN, GEN_sync);
									meas_all();
									genLoad = (gp_KVAa[GEN]+gp_KVAb[GEN]+gp_KVAc[GEN]);

									set_Igen_mux(GEN2);
									set_meter_mux(SYS, SYS_sync);
									meas_all();
									genLoad = genLoad + (gp_KVAa[SYS]+gp_KVAb[SYS]+gp_KVAc[SYS]);

									//compare 
									if (convCapacity < genLoad)
									{
										//abort LOAD EXCEEDS CONV. CAPACITY
										//
										return abort_power_on=29;
									}
								}
								set_TIE_BREAKER_CB_CLOSE();
								if (do_transfer(GEN2,CLOSE_K4,CLOSE_CB5,OPEN_CB2) == OK)
									do_transfer(GEN1,CLOSE_CB1,OPEN_CB5,OPEN_K4);
								break;

							case BsCA:		//(7) 	// A=conv + B=OFF
// FORCE BUS CONFIGURATION...
								set_GEN_1_CB_OPEN();
								set_TIE_BREAKER_CB_CLOSE();
								do_transfer(GEN1,CLOSE_CB1,OPEN_CB4,OPEN_K4);
								break;

							case BsCB:		//(8) 	// A=OFF  + B=conv
// FORCE BUS CONFIGURATION...
								set_GEN_2_CB_OPEN();
								set_TIE_BREAKER_CB_CLOSE();
								do_transfer(GEN1,CLOSE_CB1,OPEN_CB5,OPEN_K4);
								break;

							case BsG1ACB:	//(9) 	// A=gen1 + B=conv
								set_GEN_2_CB_OPEN();
								set_CONV_BUS_A_CB_OPEN();
								do_transfer(GEN1,CLOSE_CB3,OPEN_CB5,OPEN_K4);
								break;

							case BsCAG2B:	//(10)	// A=conv + B=gen2
///
//CHECK CAPACITY OF CONV WILL NOT BE EXCEEDED
//
								if (GEN_AMPS_PRESENT)
								{
									set_Igen_mux(GEN1);
									set_meter_mux(GEN, GEN_sync);
									meas_all();
									genLoad = (gp_KVAa[GEN]+gp_KVAb[GEN]+gp_KVAc[GEN]);

									set_Igen_mux(GEN2);
									set_meter_mux(SYS, SYS_sync);
									meas_all();
									genLoad = genLoad + (gp_KVAa[SYS]+gp_KVAb[SYS]+gp_KVAc[SYS]);

									//compare 
									if (convCapacity < genLoad)
									{
										//abort LOAD EXCEEDS CONV. CAPACITY
										//
										return abort_power_on=29;
									}
								}
///
// FORCE BUS CONFIGURATION...
								set_GEN_1_CB_OPEN();
								set_CONV_BUS_B_CB_OPEN();

								if (do_transfer(GEN2,CLOSE_K4,CLOSE_CB3,OPEN_CB2) == OK)
								{
									do_transfer(GEN1,CLOSE_CB1,OPEN_CB4,OPEN_K4);
								}
								break;

//=============================================================================

							default:
								abort_transfer(19);
								break;
						}
						break;

			case BsGEN2:	//(2) 	// A=gen2 + B=gen2
						if (genstartMode)
						{
							startGenset(GEN2);
						}

						switch (busStatus)
						{
							case BsDEAD:	//(0}	// A=off  + B=off
								set_GEN_2_CB_CLOSE();
								set_TIE_BREAKER_CB_CLOSE();
// FORCE BUS CONFIGURATION...
								set_CONV_BUS_A_CB_OPEN();
								set_CONV_BUS_B_CB_OPEN();
								break;

							case BsGEN1:
										if (NEW_G2G_OPTION)
										{
											do_G2G_transfer(GEN2,CLOSE_CB2,OPEN_CB1);
											shutdown_flag=shutdown_flag+GEN1;
										}
										else
										{
///
//CHECK CAPACITY OF CONV WILL NOT BE EXCEEDED
//
											if (GEN_AMPS_PRESENT)
											{
												set_Igen_mux(GEN1);
												set_meter_mux(GEN, GEN_sync);
												meas_all();
												genLoad = (gp_KVAa[GEN]+gp_KVAb[GEN]+gp_KVAc[GEN]);
												//compare 
												if (convCapacity < genLoad)
												{
													//abort LOAD EXCEEDS CONV. CAPACITY
													//
													return abort_power_on=29;
												}

											}
///
											if (do_transfer(GEN1,CLOSE_K4,CLOSE_CB4,OPEN_CB1) == OK)
												do_transfer(GEN2,CLOSE_CB2,OPEN_CB4,OPEN_K4);
										}
										break;

							case BsCONV:
// FORCE BUS CONFIGURATION...
								set_TIE_BREAKER_CB_CLOSE();
								set_CONV_BUS_A_CB_CLOSE();
								set_CONV_BUS_B_CB_OPEN();
								do_transfer(GEN2,CLOSE_CB2,OPEN_CB4,OPEN_K4);
								break;

							case BsG1AG2B:
										if (NEW_G2G_OPTION)
										{
											do_G2G_transfer(GEN1,CLOSE_CB3,OPEN_CB1);
											shutdown_flag=shutdown_flag+GEN1;
										}
										else
										{
///
//CHECK CAPACITY OF CONV WILL NOT BE EXCEEDED
//
											if (GEN_AMPS_PRESENT)
											{
												set_Igen_mux(GEN1);
												set_meter_mux(GEN, GEN_sync);
												meas_all();
												genLoad = (gp_KVAa[GEN]+gp_KVAb[GEN]+gp_KVAc[GEN]);

												set_Igen_mux(GEN2);
												set_meter_mux(SYS, SYS_sync);
												meas_all();
												genLoad = genLoad + (gp_KVAa[SYS]+gp_KVAb[SYS]+gp_KVAc[SYS]);

												//compare 
												if (convCapacity < genLoad)
												{
													//abort LOAD EXCEEDS CONV. CAPACITY
													//
													return abort_power_on=29;
												}
											}
///
											if (do_transfer(GEN1,CLOSE_K4,CLOSE_CB4,OPEN_CB1) == OK)
												do_transfer(GEN2,CLOSE_CB3,OPEN_CB4,OPEN_K4);
										}
										break;
//=============================================================================
							case BsG1A:		//(5) 	// A=gen1 + B=OFF
///
//CHECK CAPACITY OF CONV WILL NOT BE EXCEEDED
//
								if (GEN_AMPS_PRESENT)
								{
									set_Igen_mux(GEN1);
									set_meter_mux(GEN, GEN_sync);
									meas_all();
									genLoad = (gp_KVAa[GEN]+gp_KVAb[GEN]+gp_KVAc[GEN]);

									set_Igen_mux(GEN2);
									set_meter_mux(SYS, SYS_sync);
									meas_all();
									genLoad = genLoad + (gp_KVAa[SYS]+gp_KVAb[SYS]+gp_KVAc[SYS]);

									//compare 
									if (convCapacity < genLoad)
									{
										//abort LOAD EXCEEDS CONV. CAPACITY
										//
										return abort_power_on=29;
									}
								}
///
								set_TIE_BREAKER_CB_CLOSE();
								if (do_transfer(GEN1,CLOSE_K4,CLOSE_CB4,OPEN_CB1) == OK)
									do_transfer(GEN2,CLOSE_CB2,OPEN_CB4,OPEN_K4);
								break;

							case BsG2B:		//(6) 	// A=OFF  + B=gen2
								set_TIE_BREAKER_CB_CLOSE();
								break;

							case BsCA:		//(7) 	// A=conv + B=OFF
// FORCE BUS CONFIGURATION...
								set_GEN_1_CB_OPEN();
								set_TIE_BREAKER_CB_CLOSE();
								do_transfer(GEN2,CLOSE_CB2,OPEN_CB4,OPEN_K4);
								break;

							case BsCB:		//(8) 	// A=OFF  + B=conv
// FORCE BUS CONFIGURATION...
								set_GEN_2_CB_OPEN();
								set_TIE_BREAKER_CB_CLOSE();
								do_transfer(GEN2,CLOSE_CB2,OPEN_CB5,OPEN_K4);
								break;

							case BsG1ACB:	//(9) 	// A=gen1 + B=conv
								set_GEN_2_CB_OPEN();
								do_transfer(GEN1,CLOSE_CB3,CLOSE_CB4,OPEN_CB1);
								set_CONV_BUS_B_CB_OPEN();
								do_transfer(GEN2,CLOSE_CB2,OPEN_CB4,OPEN_K4);
								break;

							case BsCAG2B:	//(10)	// A=conv + B=gen2
								set_GEN_1_CB_OPEN();
								set_CONV_BUS_B_CB_OPEN();
								do_transfer(GEN2,CLOSE_CB3,OPEN_CB4,OPEN_K4);
								break;

//=============================================================================

							default:
								abort_transfer(19);
								break;
						}
						break;


			case BsCONV:	//(3) 	// A=conv + B=conv
//
// ABORT TRANSFER TO CONVERTER IF NOT DOCK POWER INPUT.
//
						if (GEN2GEN_OPTION && !NEW_G2G_OPTION && cb6_CLOSED())
						{
							return (abort_power_on = 51);
						}

						switch (busStatus)
						{
							case BsDEAD:	//(0}	// A=off  + B=off
									remote_off_flag=NO;
									
									if (EXT_CB_OPTION) {		//DHK 10142015: V9.03
										set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
										enable_output();
									}
									else {
										enable_output();
										set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
									}
										
									set_CONV_BUS_A_CB_CLOSE();
									set_CONV_BUS_B_CB_OPEN();
									set_TIE_BREAKER_CB_CLOSE();
									break;

							case BsGEN1:
///
//CHECK CAPACITY OF CONV WILL NOT BE EXCEEDED
//
								if (GEN_AMPS_PRESENT)
								{
									set_Igen_mux(GEN1);
									set_meter_mux(GEN, GEN_sync);
									meas_all();
									genLoad = (gp_KVAa[GEN]+gp_KVAb[GEN]+gp_KVAc[GEN]);
									//compare 
									if (convCapacity < genLoad)
									{
										//abort LOAD EXCEEDS CONV. CAPACITY
										//
										return abort_power_on=29;
									}

								}
///
								if (do_transfer(GEN1,CLOSE_K4,CLOSE_CB4,OPEN_CB1) == OK)
								{
									set_CONV_BUS_A_CB_CLOSE();
									set_CONV_BUS_B_CB_OPEN();
									set_TIE_BREAKER_CB_CLOSE();
								}
								break;

							case BsGEN2:
///
//CHECK CAPACITY OF CONV WILL NOT BE EXCEEDED
//
								if (GEN_AMPS_PRESENT)
								{
									set_Igen_mux(GEN2);
									set_meter_mux(SYS, SYS_sync);
									meas_all();
									genLoad = (gp_KVAa[SYS]+gp_KVAb[SYS]+gp_KVAc[SYS]);
									//compare 
									if (convCapacity < genLoad)
									{
										//abort LOAD EXCEEDS CONV. CAPACITY
										//
										return abort_power_on=29;
									}
								}
///
								if (do_transfer(GEN2,CLOSE_K4,CLOSE_CB4,OPEN_CB2) == OK)
								{
									set_CONV_BUS_A_CB_CLOSE();
									set_TIE_BREAKER_CB_CLOSE();
									set_CONV_BUS_B_CB_OPEN();
								}
								break;

							case BsG1AG2B:
///
//CHECK CAPACITY OF CONV WILL NOT BE EXCEEDED
//
								if (GEN_AMPS_PRESENT)
								{
									set_Igen_mux(GEN1);
									set_meter_mux(GEN, GEN_sync);
									meas_all();
									genLoad = (gp_KVAa[GEN]+gp_KVAb[GEN]+gp_KVAc[GEN]);

									set_Igen_mux(GEN2);
									set_meter_mux(SYS, SYS_sync);
									meas_all();
									genLoad = genLoad + (gp_KVAa[SYS]+gp_KVAb[SYS]+gp_KVAc[SYS]);

									//compare 
									if (convCapacity < genLoad)
									{
										//abort LOAD EXCEEDS CONV. CAPACITY
										//
										return abort_power_on=29;
									}
								}
//
								if (NEW_G2G_OPTION)
								{
									if (do_G2G_transfer(GEN2,CLOSE_CB3,OPEN_CB2) == OK)
									{
										shutdown_flag=shutdown_flag+GEN2;
										do_transfer(GEN1,CLOSE_K4,CLOSE_CB4,OPEN_CB1); //xfr2 conv
									}
								}
								else
								{
									if (do_transfer(GEN1,CLOSE_K4,CLOSE_CB4,OPEN_CB1) == OK)
									{
										if (do_transfer(GEN2,CLOSE_CB3,NONE,OPEN_CB2) == OK)
										{
											set_CONV_BUS_A_CB_CLOSE();
											set_CONV_BUS_B_CB_OPEN();
											set_TIE_BREAKER_CB_CLOSE();

											if (NEW_G2G_OPTION)
											{
//												stopGenset((int)GEN1);
//												stopGenset((int)GEN2);
												shutdown_flag=3;
											}
										}
									}
								}
								break;

							default:
								abort_transfer(19);
							break;
						}
						break;

			case BsG1AG2B:	//(4) 	// A=gen1 + B=gen2
						if (genstartMode)
						{
							startGenset(GEN1+GEN2);
						}

						switch (busStatus)
						{
							case BsDEAD:	//(0}	// A=off  + B=off
								set_GEN_2_CB_OPEN();
								set_GEN_1_CB_CLOSE();
								set_TIE_BREAKER_CB_OPEN();
								set_GEN_2_CB_CLOSE();
								break;

							case BsGEN1:
										if (NEW_G2G_OPTION)
										{
											do_G2G_transfer(GEN2,CLOSE_CB2,OPEN_CB3);
										}
										else
										{
///
//CHECK CAPACITY OF CONV WILL NOT BE EXCEEDED
//
											if (GEN_AMPS_PRESENT)
											{
												set_Igen_mux(GEN1);
												set_meter_mux(GEN, GEN_sync);
												meas_all();
												genLoad = (gp_KVAa[GEN]+gp_KVAb[GEN]+gp_KVAc[GEN]);

												set_Igen_mux(GEN2);
												set_meter_mux(SYS, SYS_sync);
												meas_all();
												genLoad = genLoad + (gp_KVAa[SYS]+gp_KVAb[SYS]+gp_KVAc[SYS]);

												//compare 
												if (convCapacity < genLoad)
												{
													//abort LOAD EXCEEDS CONV. CAPACITY
													//
													return abort_power_on=29;
												}
											}
///
											if (do_transfer(GEN1,CLOSE_K4,CLOSE_CB5,OPEN_CB3) == OK)
											{
												set_CONV_BUS_A_CB_OPEN();
												do_transfer(GEN2,CLOSE_CB2,OPEN_CB5,OPEN_K4);
											}
										}
										break;

							case BsGEN2:
										if (NEW_G2G_OPTION)
										{
											do_G2G_transfer(GEN1,CLOSE_CB1,OPEN_CB3);
										}
										else
										{
///
//CHECK CAPACITY OF CONV WILL NOT BE EXCEEDED
//
											if (GEN_AMPS_PRESENT)
											{
												set_Igen_mux(GEN1);
												set_meter_mux(GEN, GEN_sync);
												meas_all();
												genLoad = (gp_KVAa[GEN]+gp_KVAb[GEN]+gp_KVAc[GEN]);

												set_Igen_mux(GEN2);
												set_meter_mux(SYS, SYS_sync);
												meas_all();
												genLoad = genLoad + (gp_KVAa[SYS]+gp_KVAb[SYS]+gp_KVAc[SYS]);

												//compare 
												if (convCapacity < genLoad)
												{
													//abort LOAD EXCEEDS CONV. CAPACITY
													//
													return abort_power_on=29;
												}
											}
///
											if (do_transfer(GEN2,CLOSE_K4,CLOSE_CB4,OPEN_CB3) == OK)
											{
												set_CONV_BUS_B_CB_OPEN();
												do_transfer(GEN1,CLOSE_CB1,OPEN_CB4,OPEN_K4);
											}
										}
										break;

							case BsCONV:
								if (NEW_G2G_OPTION)
								{
// FORCE BUS CONFIGURATION...
									set_TIE_BREAKER_CB_CLOSE();
									if (do_transfer(GEN1,CLOSE_CB1,OPEN_CB4,OPEN_K4) == OK) //xfr2 gen#1
									{
										do_G2G_transfer(GEN2,CLOSE_CB2,OPEN_CB3); //xfr2 split
									}
								}
								else
								{
// FORCE BUS CONFIGURATION...
									set_TIE_BREAKER_CB_CLOSE();
									set_CONV_BUS_A_CB_CLOSE();
									set_CONV_BUS_B_CB_OPEN();
									if (do_transfer(GEN2,CLOSE_CB2,NONE,OPEN_CB3) == OK)
										do_transfer(GEN1,CLOSE_CB1,OPEN_CB4,OPEN_K4);
								}
								break;

//=============================================================================
							case BsG1A:		//(5) 	// A=gen1 + B=OFF
								set_CONV_BUS_B_CB_OPEN();
								set_TIE_BREAKER_CB_OPEN();
								set_CONV_BUS_A_CB_OPEN();
								set_GEN_2_CB_CLOSE();
								break;

							case BsG2B:		//(6) 	// A=OFF  + B=gen2
								set_CONV_BUS_A_CB_OPEN();
								set_TIE_BREAKER_CB_OPEN();
								set_CONV_BUS_B_CB_OPEN();
								set_GEN_1_CB_CLOSE();
								break;

							case BsCA:		//(7) 	// A=conv + B=OFF
// FORCE BUS CONFIGURATION...
								set_GEN_1_CB_OPEN();
								set_TIE_BREAKER_CB_CLOSE();
								set_CONV_BUS_A_CB_CLOSE();
								set_CONV_BUS_B_CB_OPEN();
								if (do_transfer(GEN2,CLOSE_CB2,NONE,OPEN_CB3) == OK)
									do_transfer(GEN1,CLOSE_CB1,OPEN_CB4,OPEN_K4);
								break;

							case BsCB:		//(8) 	// A=OFF  + B=conv
// FORCE BUS CONFIGURATION...
								set_GEN_2_CB_OPEN();
								set_TIE_BREAKER_CB_CLOSE();
								set_CONV_BUS_A_CB_CLOSE();
								set_CONV_BUS_B_CB_OPEN();
								if (do_transfer(GEN2,CLOSE_CB2,NONE,OPEN_CB3) == OK)
									do_transfer(GEN1,CLOSE_CB1,OPEN_CB4,OPEN_K4);
								break;

							case BsG1ACB:	//(9) 	// A=gen1 + B=conv
								set_GEN_2_CB_OPEN();
								set_TIE_BREAKER_CB_OPEN();
								do_transfer(GEN2,CLOSE_CB2,OPEN_CB5,OPEN_K4);
								break;

							case BsCAG2B:	//(10)	// A=conv + B=gen2
								set_GEN_1_CB_OPEN();
								set_TIE_BREAKER_CB_OPEN();
								do_transfer(GEN1,CLOSE_CB1,OPEN_CB4,OPEN_K4);
								break;

//=============================================================================


							default:
								abort_transfer(19);
								break;
					}
					break;

			case BsG1A:		//(5) 	// A=gen1 + B=OFF

					//break;
			case BsG2B:		//(6) 	// A=OFF  + B=gen2

					//break;
			case BsCA:		//(7) 	// A=conv + B=OFF

					//break;
			case BsCB:		//(8) 	// A=OFF  + B=conv

					//break;
			case BsG1ACB:	//(9) 	// A=gen1 + B=conv

					//break;
			case BsCAG2B:	//(10)	// A=conv + B=gen2

					//break;
			default:
					abort_transfer(19);
					break;
		}

		clear_screen();

		if (abort_power_on)
		{
			lcd_display("POWER TRANSFER FAILED\0",1,10);
		}
		else
		{
			lcd_display("POWER TRANSFER COMPLETED\0",1,9);

			if (NEW_G2G_OPTION && shutdown_flag)
			{
				stopGenset(shutdown_flag);
			}
		}

		if (NEW_G2G_OPTION)
		{
			busStatus = get_busState2();
		}
		else
		{
			busStatus = get_busState();
		}

		switch (busStatus)	//assign transferRequest function keys for standard requests.
		{
			case 0:					//NO POWER:
					lcd_display("NO POWER on BUS-A      NO POWER on BUS-B\0",3,1);
					break;
			case 1:					//GENSET1:
					lcd_display("GEN #1 ONLINE  Powering BUS-A and BUS-B\0",3,1);
					break;
			case 2:					//GENSET2:
					lcd_display("GEN #2 ONLINE  Powering BUS-A and BUS-B\0",3,1);
					break;
			case 3:					//CONVERTER:
					lcd_display("CONVERTER ONLINE  Powering BUS-A & BUS-B\0",3,1);
					break;
			case 4:				//SPLIT-BUS, G1onA G2onB.
					lcd_display("GEN #1 ONLINE BUS-A, GEN #2 ONLINE BUS-B\0",3,1);
					break;
			case 5:
					lcd_display("GEN #1 ONLINE BUS-A,       OFFLINE BUS-B\0",3,1);
					break;
			case 6:
					lcd_display("OFFLINE BUS-A,       GEN #2 ONLINE BUS-B\0",3,1);
					break;
			case 7:
					lcd_display("CONVERTER ONLINE BUS-A,    OFFLINE BUS-B\0",3,1);
					break;
			case 8:
					lcd_display("OFFLINE BUS-A,    CONVERTER ONLINE BUS-B\0",3,1);
					break;
			case 9:
					lcd_display("GEN #1 ONLINE BUS-A,  CONV. ONLINE BUS-B\0",3,1);
					break;
			case 10:
					lcd_display("CONV. ONLINE BUS-A,  GEN #2 ONLINE BUS-B\0",3,1);
					break;
			case -1://BsERR1
					lcd_display("UNRECOGNIZED BUS STATE\0",3,1);
					break;
			case -2://BsERR2
					lcd_display("ILLEGAL HEAD-TO-HEAD GENERATOR OPERATION\0",3,1);
					break;
			default:				//any other, for now.
					lcd_display("BOZO on BUS-A,             BOZO on BUS-B\0",3,1);
					break;
		}
		pause(2000);
	}
	else
	{
		abort_power_on = NO;	//msg. already shown.  Got here 'cause transferRequest==0.
	} 												//end if(transferID)ELSE
	return abort_power_on;
}
/*---------------------------------------------------------------------------*/
void control_option_pcb_test_screen()	//test CONTROL and STATUS on CONTROL_OPTION_PCB.
{
  int edit_field=0;
  int row[7]=	{ 1, 1, 2, 2, 3, 3, 4};
  int column[7]={26,35,26,35,26,35,33};

  volatile unsigned STATUS_WORD_1=optcon_status;		// STATUS WORD1--IMAGE.
  volatile unsigned STATUS_WORD_2=optcon_status_2;	// STATUS WORD2--IMAGE.
  unsigned i;

redraw:
	clear_screen();

	if (NEW_G2G_OPTION)
	{
		lcd_display("BIT: FEDCBA9876543210 TIE_OPEN TIE_CLOSE\0",1,1);
		lcd_display("R30: ---------------- EXT_OPEN EXT_CLOSE\0",2,1);
		lcd_display("R31: ---------------- SPD_MUX1 SPD_MUX2 \0",3,1);
		lcd_display("Execute               SPEED-TRIM TESTER \0",4,1);
////////////////////////////////////////////////////////////////
	//				 1234567 1234567 12345678 1234567 1234567
////////////////////////////////////////////////////////////////
	}
	else
	{
		lcd_display("BIT: FEDCBA9876543210 CB3_OPEN CB3_CLOSE\0",1,1);
		lcd_display("R30: ---------------- CB4_OPEN CB4_CLOSE\0",2,1);
		lcd_display("R31: ---------------- CB5_OPEN CB5_CLOSE\0",3,1);
		lcd_display("Control Option Tester GEN_MUX:          \0",4,1);
		set_gen_mux(0);
		lcd_display("0\0",4,32);
	}
	delay(500); //wait a 1/2 sec. for operator to see screen.

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		STATUS_WORD_1=optcon_status;	// STATUS WORD1--IMAGE.
		_nop();
		STATUS_WORD_2=optcon_status_2;	// STATUS WORD2--IMAGE.

#ifdef GEN2GEN_CONTROL_OPTION_TEST
	if (GEN2GEN_OPTION)
	{
		STATUS_WORD_2 = (gp_configuration_word);					// FOR DEBUG ONLY!!
	}
//FOR DEBUG ONLY!!
#endif //GEN2GEN_CONTROL_OPTION_TEST

		for (i=0;i<16;i++)
		{
			if (STATUS_WORD_1 & (0x8000>>i))
			{
				lcd_display("1\0",2,6+i);
			}
			else
			{
				lcd_display("0\0",2,6+i);
			}

			if (STATUS_WORD_2 & (0x8000>>i))
			{
				lcd_display("1\0",3,6+i);
			}
			else
			{
				lcd_display("0\0",3,6+i);
			}
		}

		put_cursor(row[edit_field],column[edit_field]);

		delay(500);

		strobe_kybd_for_slew();

		switch (new_key)
		{
			case 19:	//F1:
					switch (edit_field)
					{
						case 0:	//CB3 OPEN
								set_TIE_BREAKER_CB_OPEN();
								break;
						case 1:	//CB3
								set_TIE_BREAKER_CB_CLOSE();
								break;
						case 2:	//CB4 OPEN
								if (NEW_G2G_OPTION)
								{
									set_EXT_CB_OPEN();
								}
								else
								{
									set_CONV_BUS_A_CB_OPEN();
								}
								break;
						case 3:	//CB4
								if (NEW_G2G_OPTION)
								{
									set_EXT_CB_CLOSE();
								}
								else
								{
									set_CONV_BUS_A_CB_CLOSE();
								}
								break;
						case 4:	//SPD_MUX1/CB5 OPEN
								if (NEW_G2G_OPTION)
								{
									setGenSpeedMux(1);	//NEW_G2G_OPTION speed-trim MUX.
								}
								else
								{
									set_CONV_BUS_B_CB_OPEN();
								}
								break;
						case 5:	//SPD_MUX2/CB5_CLOSE
								if (NEW_G2G_OPTION)
								{
									setGenSpeedMux(2);	//NEW_G2G_OPTION speed-trim MUX.
								}
								else
								{
									set_CONV_BUS_B_CB_CLOSE();
								}
								break;
						case 6:	//SPEED-TRIM TESTER/GEN_MUX
								if (NEW_G2G_OPTION)
								{
									genSpeedTester();	//NEW_G2G_OPTION speed-trim MUX & Test.
									goto redraw;
								}
								else
								{
									set_gen_mux(1);
									lcd_display("1\0",4,32);
								}
								break;
						default: 
								break;
					}
					break;

			case 20:	//F2:
					switch (edit_field)
					{
						case 6:	//gen MUX
								set_gen_mux(2);
								lcd_display("2\0",4,32);
								break;
						default: 
								break;
					}
					break;

			case 8:	//F3:
					switch (edit_field)
					{
						case 6:	//gen MUX
								set_gen_mux(3);
								lcd_display("3\0",4,32);
								break;
						default: 
								break;
					}
					break;

			case 9:	//F4:
					switch (edit_field)
					{
						case 6:	//gen MUX
								set_gen_mux(4);
								lcd_display("4\0",4,32);
								break;
						default: 
								break;
					}
					break;

			case 10:	//F5:
					switch (edit_field)
					{
						case 6:	//gen MUX
								set_gen_mux(0);
								lcd_display("0\0",4,32);
								break;
						default: 
								break;
					}
					break;

			case 17:		//CONVERTER POWER	//advance cursor.
					if (edit_field<=0)
					{
						edit_field=6;
					}
					else
					{
						edit_field--;	
					}
					put_cursor(row[edit_field],column[edit_field]);
					delay(500);	
					break;

			case 16:		//SHORE POWER		//backup cursor.
					if (edit_field>=6)
					{
						edit_field=0;
					}
					else
					{
						edit_field++;	
					}
					put_cursor(row[edit_field],column[edit_field]);
					delay(500);	
					break;

			default:
					break;
		}
	}

	new_key = 0;
}
/*---------------------------------------------------------------------------*/
void genSpeedTester()	//NEW_G2G_OPTION Speed-Trim Test and Calibrate.
{
  unsigned genSelected=1;
  unsigned temp1Duty=gen1DutyCycle;
  unsigned temp2Duty=gen2DutyCycle;
  float gen1Freq;
  float gen2Freq;
  char *syncScreen="GENSET #1                      GENSET #2\0";

	setGenSpeedMux(1);		//NEW_G2G_OPTION speed-trim MUX.

	display_type=GENERATOR_POWER;
	display_function=F4;
	CC29IC = 0x0077;	// enable gen#1 generator sync interrupt.
	CC22IC = 0x0073;	// enable gen#2 sync interrupt.

redraw:
	clear_screen();
	lcd_display("GENERATOR SPEED-TRIM TEST SCREEN        \0",1,1);
	lcd_display(" Gen #1 Frequency =   .   Hz            \0",2,1);
	lcd_display(" Gen #2 Frequency =   .   Hz            \0",3,1);
	lcd_display(" F_Up   F_Down  SelectGen    Hz   Calib.\0",4,1);
////////////////////////////////////////////////////////////////
//				 1234567 1234567 12345678 1234567 1234567
//				   F 1     F 2     F 3      F 4     F 5  
//				 1234567890123456789012345678901234567890
//				          1         2         3         4
////////////////////////////////////////////////////////////////

	lcd_display_value(GP_OUTPUT_FREQUENCY,4,27,"%3.0f\0");

	delay(500);	

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		if ((genSelected!=1) && (genSelected!=2))	//force valid selection 
		{
			genSelected = 1;
			setGenSpeedMux(1);		//NEW_G2G_OPTION speed-trim MUX.
		}

		if (genSelected==1)
		{
			lcd_display("#1\0",1,38);
		}
		else
		{
			lcd_display("#2\0",1,38);
		}

		if (deltaT5gen1 > 0)
		{					 
			gen1Freq 	= 	TICK_COUNT/(float)deltaT5gen1;	//calculate frequency from tick count.
		}
		else
		{
			gen1Freq=0.0;

		}

		if (deltaT5gen2 > 0)
		{					 
			gen2Freq 	= 	TICK_COUNT/(float)deltaT5gen2;	//calculate frequency from tick count.
		}
		else
		{
			gen2Freq=0.0;

		}

		lcd_display_value(gen1Freq,2,21,"%5.2f\0");
		lcd_display_value(gen2Freq,3,21,"%5.2f\0");

		lcd_display("       \0",2,34);
		lcd_display(utoa(temp1Duty),2,34);
		lcd_display("       \0",3,34);
		lcd_display(utoa(temp2Duty),3,34);

		if (genSelected==1)
		{
			put_cursor(2,7);
		}
		else
		{
			put_cursor(3,7);
		}

		delay(200);
		strobe_kybd_for_slew();

		switch (new_key)
		{
			case 19:	//F1:
					if (genSelected==1)
					{
						if (temp1Duty>0) temp1Duty--;	//limit duty slew
						set_PWM3( temp1Duty );			//sets PWM3 duty cycle to given binary count, 0<=count<=PP3.
					}
					else
					{
						if (temp2Duty>0) temp2Duty--;	//limit duty slew
						set_PWM3( temp2Duty );			//sets PWM3 duty cycle to given binary count, 0<=count<=PP3.
					}
					new_key = 0;
					break;

			case 20:	//F2:
					if (genSelected==1)
					{
						if (temp1Duty<PP3) temp1Duty++;	//limit duty slew
						set_PWM3( temp1Duty );			//sets PWM3 duty cycle to given binary count, 0<=count<=PP3.
					}
					else
					{
						if (temp2Duty<PP3) temp2Duty++;	//limit duty slew
						set_PWM3( temp2Duty );			//sets PWM3 duty cycle to given binary count, 0<=count<=PP3.
					}
					new_key = 0;
					break;

			case 8:		//F3:
					if (genSelected==1)
					{
						setGenSpeedMux(2);	//NEW_G2G_OPTION speed-trim MUX.
						genSelected = 2;
						temp2Duty=gen2DutyCycle;
					}
					else
					{
						setGenSpeedMux(1);	//NEW_G2G_OPTION speed-trim MUX.
						genSelected = 1;
						temp1Duty=gen1DutyCycle;
					}
					new_key = 0;
					break;

			case 9:		//F4:
					lcd_display(syncScreen,2,1);
					adjustGenSpeed(genSelected,GP_OUTPUT_FREQUENCY);

					if (genSelected==1)
					{
						temp1Duty = lastDuty;
					}
					else
					{
						temp2Duty = lastDuty;
					}
					new_key = 0;
					goto redraw;
//					break;

			case 10:	//F5:
					if (genSelected==1)
					{
						lcd_display("CALIBRATING\0",2,30);
						gen1DutyCycle = temp1Duty;
						setGenSpeedMux(1);	//NEW_G2G_OPTION speed-trim MUX.
					}
					else
					{
						lcd_display("CALIBRATING\0",3,30);
						gen2DutyCycle = temp2Duty;
						setGenSpeedMux(2);	//NEW_G2G_OPTION speed-trim MUX.
					}
					delay(2000);
					new_key = 0;
					goto redraw;
//					break;

			default:
					break;

		}//endswitch (new_key)
	}//endwhile (new_key!=18)

	new_key = 0;
	setGenSpeedMux(NONE);	//NEW_G2G_OPTION speed-trim MUX.
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
//
//  BUS STATES & transferRequest DEFINED CONSTANTS:
//
//#define BsDEAD		0	// A=off  + B=off
//#define BsGEN1		1	// A=gen1 + B=gen1
//#define BsGEN2		2 	// A=gen2 + B=gen2
//#define BsCONV		3 	// A=conv + B=conv
//#define BsG1AG2B		4 	// A=gen1 + B=gen2
//#define BsG1A			5 	// A=gen1 + B=OFF
//#define BsG2B			6 	// A=OFF  + B=gen2
//#define BsCA			7 	// A=conv + B=OFF
//#define BsCB			8 	// A=OFF  + B=conv
//#define BsG1ACB		9 	// A=gen1 + B=conv
//#define BsCAG2B		10	// A=conv + B=gen2
//
/*---------------------------------------------------------------------------*/
int get_busState()	//read status from CONTROL_OPTION_PCB and return bus state [0-10].
{
	int busState = -1;
	unsigned cops = optcon_status_2;	// Control OPtion Status (cops) vector.
	unsigned tiedBus = ((cops & 4) || ((cops & 3) == 3));	//CB3 CLOSED .OR. CB4,5 CLOSED.
	unsigned splitBus;

	if (MULTIGEN_BESECKE)
	{
		return get_busState2();
	}

///WIP
if (!GEN2GEN_OPTION)
{
	tiedBus=TRUE;

	if (TIE_CONF_PRESENT)
	{
		if (tieBreaker_CLOSED())
		{
			cops =0x0004;	//need to show TB Closed & initialize cops vector!
//	tiedBus=TRUE;
		}
		else
		{
			cops =0x0000;	//need to show TB Open & initialize cops vector!
			tiedBus=FALSE;	//NEW v8.00
		}
	}
	else
	{
		cops =0x0004;	//need to show TB Closed & initialize cops vector!
	}

	if (gp_state.invstat) cops = (cops | 0x0003);
}
///WIP

	if (Gen1conf) cops = (cops | 0x0100);
	else cops = (cops & 0xFEFF);

	if (Gen2conf) cops = (cops | 0x0200);
	else cops = (cops & 0xFDFF);

#ifdef GEN2GEN_CONTROL_OPTION_TEST
	if (GEN2GEN_OPTION)
	{
		cops = (gp_configuration_word);					// FOR DEBUG ONLY!!
		tiedBus = ((cops & 4) || ((cops & 3) == 3));	//CB3 CLOSED .OR. CB4,5 CLOSED.
	}
//FOR DEBUG ONLY!!
#endif //GEN2GEN_CONTROL_OPTION_TEST

	if (tiedBus) splitBus = FALSE;
	else splitBus = TRUE;

	if (GEN2GEN_OPTION)
	{
		if ( tiedBus && !tieBreaker_CLOSED() )
		{
			set_TIE_BREAKER_CB_CLOSE();

			if (tieBreaker_CLOSED())	//if(cb3 close confirmed)
			{
				set_CONV_BUS_B_CB_OPEN();
			
				if (gp_state.invstat!=ONLINE)
				{
					set_CONV_BUS_A_CB_OPEN();
				}
			}
		}
	}
//
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//  Control OPtion Status (cops) vector == CONTROL OPTION PCB (RCS31*)
//
// 	15  14  13  12   11  10   9   8    7   6   5   4    3   2   1   0
// 	xxx xxx xxx CBC  G4C G3C CB2 CB1  CB9 CB8 CB7 CB6  SK2 CB3 CB5 CB4
//	 0   0   0   0    0   0   1   1    0   0   0   0    0	1   1   1
// 
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//
//test for dead bus.
//
	if ((cops & 0x0303) == 0)
	{
		busState = BsDEAD;				//cb1,2,4,5 Open.
	}
	else if (((cops & 0x0300) == 0) && (gp_state.invstat==OFFLINE))	
	{
		busState = BsDEAD;				//cb1,2 Open and Converter offline.
	}
//
//test for bus error states.
//
	else if (((cops & 0x0300) == 0x0300) && tiedBus)
	{					//BsERR: ILLEGAL HEAD-TO-HEAD GENERATOR OPERATION
						//open Tie-Breaker CB or Gen CB immediately!.
		busState = -2;	// A=gen1 + B=gen2	//cb1,2 Closed and tied bus. 
	}
//
//test for one-of-ten remaining valid states.
//
	else if ((cops & 0x0100) && tiedBus)
	{
		busState = BsGEN1;	// A=gen1 + B=gen1	//cb1 Closed and tied bus.
	}
	else if ((cops & 0x0200) && tiedBus)
	{
		busState = BsGEN2;	// A=gen2 + B=gen2	//cb2 Closed and tied bus.
	}
	else if ((cops & 0x0003) && tiedBus && (gp_state.invstat==ONLINE))
	{
		busState = BsCONV;	// A=conv + B=conv	//cb3 and (4 and/or 5) Closed and Converter online.
	}
	else if (((cops & 0x0300) == 0x0300) && splitBus)
	{
		busState = BsG1AG2B;// A=gen1 + B=gen2	//cb1,2 Closed and split bus.
	}
	else if (((cops & 0x0300) == 0x0100) && splitBus && (gp_state.invstat==OFFLINE))
	{
		busState = BsG1A;	// A=gen1 + B=OFF	//cb1 Closed, cb2 Open, split bus and Converter offline.
	}
	else if (((cops & 0x0300) == 0x0200) && splitBus && (gp_state.invstat==OFFLINE))
	{
		busState = BsG2B;	// A=OFF  + B=gen2	//cb2 Closed, cb1 Open, split bus and Converter offline.
	}
	else if (((cops & 0x0307) == 0x0001) && (gp_state.invstat==ONLINE))
	{
		busState = BsCA;	// A=conv + B=OFF	//cb1,2,3,5 Open, cb4 Closed, split bus and Converter online.
	}
	else if (((cops & 0x0307) == 0x0002) && (gp_state.invstat==ONLINE))
	{
		busState = BsCB;	// A=OFF  + B=conv	//cb1,2,3,4 Open, cb5 Closed, split bus and Converter online.
	}
	else if (((cops & 0x0307) == 0x0102) && (gp_state.invstat==ONLINE))
	{
		busState = BsG1ACB;	// A=gen1 + B=conv	//cb2,3,4 Open, cb1,5 Closed, split bus and Converter online.
	}
	else if (((cops & 0x0307) == 0x0201) && (gp_state.invstat==ONLINE))
	{
		busState = BsCAG2B;	// A=conv + B=gen2	//cb1,3,5 Open, cb2,4 Closed, split bus and Converter online.
	}
	else
	{						//BsERR: UNRECOGNIZED BUS STATE
		busState = -1;
	}

	return busState;
}
//
// Control OPtion Status (cops) vector:
//
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// 	15  14  13  12   11  10   9   8    7   6   5   4    3   2   1   0
// 	xxx xxx xxx CBC  G4C G3C CB2 CB1  CB9 CB8 CB7 CB6  SK2 CB3 CB5 CB4
//	 0   0   0   0    0   0   1   1    0   0   0   0    0	1   1   1
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
int get_busState2()	//read status from CONTROL_OPTION_PCB and return bus state [0-10].
{
	int busState = -1;
	unsigned cops = optcon_status_2;		// Control OPtion Status (cops) vector.
	unsigned tiedBus = ((cops & 4) == 4);	// CB3 CLOSED.
	unsigned splitBus;

	if (MULTIGEN_BESECKE)
	{
		if ((cops&0x0400) || (cops&0x0100))
		{
			cops = cops|0x0200;		//if GEN#1 OR #3 CB CONF then set GEN#2 CB CONF
			cops = cops&0xFAFF;		//CLEAR GEN#1 & GEN#3 CB CONF'S
		}

		if ((cops&0x1000)==0)
		{
			gp_state.invstat=OFFLINE;
		}
	}
	else
	{
		if (Gen1conf) cops = (cops | 0x0100);	//gen #1 online bit in cops vector.
		else cops = (cops & 0xFEFF);

		if (Gen2conf) cops = (cops | 0x0200);	//gen #2 online bit in cops vector.
		else cops = (cops & 0xFDFF);
	}

#ifdef GEN2GEN_CONTROL_OPTION_TEST
	if (GEN2GEN_OPTION)
	{
		cops = (gp_configuration_word);					// FOR DEBUG ONLY!!
		tiedBus = (cops & 4);							//CB3 CLOSED.
	}
#endif //GEN2GEN_CONTROL_OPTION_TEST

	if (tiedBus) splitBus = FALSE;
	else splitBus = TRUE;

//	if ( tiedBus && !tieBreaker_CLOSED() )
//	{
//		set_TIE_BREAKER_CB_CLOSE();
//
//		if (tieBreaker_CLOSED())	//if(cb3 close confirmed)
//		{
//			set_CONV_BUS_B_CB_OPEN();
//		
//			if (gp_state.invstat!=ONLINE)
//			{
//				set_CONV_BUS_A_CB_OPEN();
//			}
//		}
//	}

//
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//  Control OPtion Status (cops) vector == CONTROL OPTION PCB (RCS31*)
//
// 	15  14  13  12   11  10   9   8    7   6   5   4    3   2   1   0
// 	xxx xxx xxx CBC  G4C G3C CB2 CB1  CB9 CB8 CB7 CB6  SK2 CB3 CB5 CB4
//	 0   0   0   0    0   0   1   1    0   0   0   0    0	1   1   1
// 
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//
//test for dead bus.
//
	if ((cops & 0x1300) == 0)
	{
		if (splitBus)
			busState = BsOPEN;				//cb1,2, and EXT_CB Open AND TIE OPEN.
		else
			busState = BsDEAD;				//cb1,2, and EXT_CB Open.
	}
	else if (((cops & 0x0300) == 0) && (gp_state.invstat==OFFLINE))	
	{
		if (splitBus)
			busState = BsOPEN;				//cb1,2, and EXT_CB Open AND TIE OPEN.
		else
			busState = BsDEAD;				//cb1,2, and EXT_CB Open.
	}
//
//test for bus error states.
//
	else if (((cops & 0x0300) == 0x0300) && tiedBus && !MULTIGEN_BESECKE)	//allow for MIXED MODE
	{					//BsERR: ILLEGAL HEAD-TO-HEAD GENERATOR OPERATION
						//open Tie-Breaker CB or Gen CB immediately!.
		busState = -2;	// A=gen1 + B=gen2	//cb1,2 Closed and tied bus. 
	}
//
//test for one-of-ten remaining valid states.
//
	else if (((cops & 0x0100) == 0x0100) && tiedBus && (gp_state.invstat==OFFLINE))
	{
		busState = BsGEN1;	// A=gen1 + B=gen1	//cb1 Closed and tied bus.
	}
	else if (((cops & 0x0200) == 0x0200) && tiedBus && (gp_state.invstat==OFFLINE))
	{
		busState = BsGEN2;	// A=gen2 + B=gen2	//cb2 Closed and tied bus.
	}
	else if (((cops & 0x1304) == 0x1200) && (gp_state.invstat==ONLINE))
	{
		busState = BsCAG2B;	// A=conv + B=gen2	//cb1,3,5 Open, cb2,4 Closed, split bus and Converter online.
	}
	else if (((cops & 0x1304) == 0x0200) && (gp_state.invstat==OFFLINE))
	{
		busState = BsG2B;	// A=conv + B=gen2	//cb1,3,5 Open, cb2,4 Closed, split bus and Converter online.
	}
	else if (((cops & 0x1304) == 0x1000) && (gp_state.invstat==ONLINE))	//open tie
	{
		busState = BsCA;	// A=conv + B=OFF	//cb1,2,3,5 Open, cb4 Closed, split bus and Converter online.
	}
	else if (((cops & 0x1000) == 0x1000) && tiedBus && (gp_state.invstat==ONLINE))
	{
		busState = BsCONV;	// A=conv + B=conv	//cb3 and (4 and/or 5) Closed and Converter online.
	}
	else if (((cops & 0x0300) == 0x0300) && splitBus && (gp_state.invstat==OFFLINE))
	{
		busState = BsG1AG2B;// A=gen1 + B=gen2	//cb1,2 Closed and split bus.
	}
	else if (((cops & 0x0300) == 0x0100) && splitBus && (gp_state.invstat==OFFLINE))
	{
		busState = BsG1A;	// A=gen1 + B=OFF	//cb1 Closed, cb2 Open, split bus and Converter offline.
	}
	else if (((cops & 0x0300) == 0x0200) && splitBus && (gp_state.invstat==OFFLINE))
	{
		busState = BsG2B;	// A=OFF  + B=gen2	//cb2 Closed, cb1 Open, split bus and Converter offline.
	}
////
//	else if (((cops & 0x0307) == 0x0002) && (gp_state.invstat==ONLINE))
//	{
//		busState = BsCB;	// A=OFF  + B=conv	//cb1,2,3,4 Open, cb5 Closed, split bus and Converter online.
//	}
//	else if (((cops & 0x0307) == 0x0102) && (gp_state.invstat==ONLINE))
//	{
//		busState = BsG1ACB;	// A=gen1 + B=conv	//cb2,3,4 Open, cb1,5 Closed, split bus and Converter online.
//	}
////
	else
	{						//BsERR: UNRECOGNIZED BUS STATE
		busState = -1;
	}

	return busState;
}
//
// Control OPtion Status (cops) vector:
//
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// 	15  14  13  12   11  10   9   8    7   6   5   4    3   2   1   0
// 	xxx xxx xxx CBC  G4C G3C CB2 CB1  CB9 CB8 CB7 CB6  SK2 CB3 CB5 CB4
//	 0   0   0   0    0   0   1   1    0   0   0   0    0	1   1   1
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
/*---------------------------------------------------------------------------*/
/*
void delayWithFreqLock( unsigned  ms, unsigned cbid)		// 60000ms = 60s max.
{
	delay(ms);
	cbid=ms;
}
/*---------------------------------------------------------------------------*/

void delayWithFreqLock( unsigned  ms, unsigned cbId)		// 60000ms = 60s max.
{
  float ftemp;
  unsigned ticks;
  float newFreq=0.0;
//  volatile int exit=FALSE;

	T2 = 0;						// start T2 with 0 counts.
	T2CON = 0x47;				// enable timer and count up.

	ftemp = (float) ms * 19.53125;	// scale 1ms to 51.2us counts.

	if(ftemp > 60000.0) ftemp = 60000.0;	// limit max time.

	if (daq_mux_id==SYS) ticks = deltaT5gen2;
	else ticks = deltaT5gen1;
	
	while (T2 < ftemp)
	{
		if (Lock)
		{
			if ( ticks > 0 )
			{
				newFreq = TICK_COUNT / (float) ticks;

				if ( ( 45 <= newFreq) && ( newFreq <= 65 ) )
				{
					pgm_edit.Freq = newFreq;
					exec_osc_pgm();
				}
				else
				{
					Lock = 0;
					return;
				}
			}
		}
		
		
		if (Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait) {
			switch (Gen_Sel) {
				case GEN1:	if (MULTIGEN_OPTION) {
								if (!eGen1_CLOSED())
									Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;
							}					
							else {
								if (!Gen1conf)
									Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;
							}
							break;
							
				case GEN2: if (MULTIGEN_OPTION) {
								if (!eGen2_CLOSED())
									Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;
							}					
							else {
								if (!Gen2conf)
									Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;
							}
							break;
							
				case GEN3: if (MULTIGEN_OPTION) {
								if (!eGen3_CLOSED())
									Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;
							}					
//							else {
//								if (!Gen1conf)
//									Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;
//							}
							break;
							
				case GEN4: if (MULTIGEN_OPTION) {
								if (!eGen1_CLOSED())
									Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;
							}					
//							else {
//								if (!Gen2conf)
//									Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;
//							}
							break;
							
				case 8:		if ((!Gen1conf) && (!Gen2conf))
								Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;
							break;					
			}
						
			if (Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down)
				Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 0;
		}
		

		switch (cbId)
		{
			case NONE:		//	0
							break;
			case OPEN_CB1:	//	1
							if (!Gen1conf) return;
							break;
			case OPEN_CB2:	//	2
							if (!Gen2conf) return;
							break;
			case OPEN_CB3:	//	3
							if (!tieBreaker_CLOSED()) return;
							break;
			case OPEN_CB4:	//	4
							if (!cb4_CLOSED()) return;
							break;
			case OPEN_CB5:	//	5
							if (!cb5_CLOSED()) return;
							break;
			case OPEN_CB6:	//	6
			case OPEN_CB7:	//	7
			case OPEN_CB8:	//	8
			case OPEN_CB9:	//	9
			case OPEN_K4:	// 	10
							break;
//multiGen stuff
			case OPEN_EG1:	// 	21
							if (!eGen1_CLOSED()) return;
							break;
			case OPEN_EG2:	// 	22
							if (!eGen2_CLOSED()) return;
							break;
			case OPEN_EG3:	// 	23
							if (!eGen3_CLOSED()) return;
							break;
			case OPEN_EG4:	// 	24
							if (!eGen4_CLOSED()) return;
							break;
			case OPEN_EXT_CB:	// 	25
							if (!EXTERNAL_CB_CLOSED()) return;
							break;

			case CLOSE_CB1:	//	11
							if (Gen1conf) return;
							break;
			case CLOSE_CB2:	//	12
							if (Gen2conf) return;
							break;
			case CLOSE_CB3:	//	13
							if (tieBreaker_CLOSED()) return;
							break;
			case CLOSE_CB4:	//	14
							if (cb4_CLOSED()) return;
							break;
			case CLOSE_CB5:	//	15
							if (cb5_CLOSED()) return;
							break;
			case CLOSE_CB6:	//	16
			case CLOSE_CB7:	//	17
			case CLOSE_CB8:	//	18
			case CLOSE_CB9:	//	19
			case CLOSE_K4:	//	20
							break;
//multiGen stuff
			case CLOSE_EG1:	//	31
							if (eGen1_CLOSED()) return;
							break;
			case CLOSE_EG2:	//	32
							if (eGen2_CLOSED()) return;
							break;
			case CLOSE_EG3:	//	33
							if (eGen3_CLOSED()) return;
							break;
			case CLOSE_EG4:	//	34
							if (eGen4_CLOSED()) return;
							break;
			case CLOSE_EXT_CB:	//	35
							if (EXTERNAL_CB_CLOSED()) return;
							break;
			default:
							break;
		}

		_nop();
	}
}
/*---------------------------------------------------------------------------*/
int test_optcon()	//test or simulator? for CONTROL_OPTION_PCB.
{
	clear_screen();

	lcd_display("        EXTENDED CONTROL OPTIONS\0",1,1);
	lcd_display("F1: TEST  CONTROL OPTION PCB\0",2,1);
//	lcd_display("F2: SIMULATE  GEN-to-GEN TRANSFERS\0",3,1);
	lcd_display("F3: SYNCHRONIZED GEN-to-GEN TRANSFERS\0",4,1);


	delay(500);
	wait_for_key();

	if (new_key==19)	return YES;
	else				return NO;
}
/*---------------------------------------------------------------------------*/

void EventLogSelectControl()	//test or simulator? for CONTROL_OPTION_PCB.
{
	clear_screen();

	lcd_display("        EVENT LOG CONTROL SCREEN        \0",1,1);
	lcd_display("F1: EVENT LOG VIEWER              Events\0",2,1);
	lcd_display("F2: EVENT LOG TRACKER                   \0",3,1);
	lcd_display("F3: EVENT LOG REGISTRY                  \0",4,1);

//	lcd_display(" View    Track    Clear    Edit    Exit \0",4,1);

	lcd_display(utoa(eventLogCount()),2,31);

	while (new_key!=18)
	{
		delay(500);
		wait_for_key();

		if (new_key==19) 		eventLogViewer();
		else if (new_key==20) 	
		{
			display_type = EVENT_LOG; 
			if (!special_function) display_function = F5; 
			new_key=0;
			pause(500);
			return;
		}
		else if (new_key==8) 	editEventRegistrationScreen();
		else if (new_key==9) 	displayEventRegistrationInformation();
	}
}
/*---------------------------------------------------------------------------*/
void abort_transfer(int abort_code)
{
	switch (abort_code)
	{
		case 0:	//TRANSFER SUCCESSFUL
				break;
		case 1: //ABORTED BY OPERATOR
				clear_screen();
				lcd_display("TRANSFER ABORTED BY OPERATOR.    \0",1,1);
//				lcd_display("FREQUENCY LOCK WAS NOT ACHIEVED. \0",2,1);
				lcd_display("IF GENERATOR FREQUENCY UNSTABLE  \0",3,1);
				lcd_display("REDUCE SHIP'S LOAD AND TRY AGAIN.\0",4,1);
				break;
		case 2: //BAD SYSTEM STATUS
				clear_screen();
				lcd_display("TRANSFER ABORTED.                \0",1,1);
				lcd_display("BAD SYSTEM STATUS.               \0",3,1);
				break;
		case 3: //GENERATOR VOLTAGE OUT OF RANGE
				clear_screen();
				lcd_display("TRANSFER ABORTED, UNABLE TO SYNC.\0",1,1);
				lcd_display("GENSET VOLTAGE BAD.              \0",2,1);
				lcd_display("DISPLAY  SELECTED  GENSET  METER.\0",3,1);
				lcd_display("CHECK CONVERTER/GENERATOR WIRING.\0",4,1);
				break;
		case 4: //GENERATOR FREQUENCY OUT OF RANGE
				clear_screen();
				lcd_display("TRANSFER ABORTED, UNABLE TO SYNC.\0",1,1);
				lcd_display("GENSET FREQUENCY BAD.            \0",2,1);
				lcd_display("DISPLAY  SELECTED  GENSET  METER.\0",3,1);
				lcd_display("CHECK CONVERTER/GENERATOR WIRING.\0",4,1);
				break;
		case 5: //SEAMLESS TRANSFER OPTION DISABLED
				clear_screen();
				lcd_display("AUTOMATIC TRANSFERS NOT ALLOWED.        \0",1,1);
				lcd_display("'SEAMLESS-TRANSFER' OPTION IS DISABLED. \0",2,1);
		  		lcd_display("Contact your local factory rep for info \0",3,1);
			  	lcd_display("SHIP POWER MUST BE TRANSFERED MANUALLY. \0",4,1);
				break;
		case 6: //AEM OPTION SET, TRANSFERS MUST BE DONE FROM SWITCHGEAR
				clear_screen();
				lcd_display("'AEM' OPTION IS ENABLED...\0",1,1);
				lcd_display("LOCAL CONTROL OF TRANSFERS DISABLED.\0",2,1);
				lcd_display("TRANSFER POWER FROM SWITCHGEAR PANEL.\0",4,1);
				break;
		case 7: //FREQUENCY LOCK NOT ACHIEVED
				clear_screen();
				lcd_display("TRANSFER ABORTED BY OPERATOR.    \0",1,1);
				lcd_display("FREQUENCY LOCK WAS NOT ACHIEVED. \0",2,1);
				lcd_display("IF GENERATOR FREQUENCY UNSTABLE  \0",3,1);
				lcd_display("REDUCE SHIP'S LOAD AND TRY AGAIN.\0",4,1);
				break;
		case 8: //TRANSFER INHIBITED DUE TO LOAD
				clear_screen();
//				lcd_display("TRANSFER INHIBITED DUE TO LOAD.\0",1,1);
//				lcd_display("LOAD EXCEEDS CONVERTER'S CAPACITY\0",3,1);
//				lcd_display("REDUCE LOAD AND TRY AGAIN.\0",4,1);
						   //1234567890123456789012345678901234567890
				lcd_display("TRANSFER INHIBITED.      INHIBIT SIGNAL\0",1,1);
				lcd_display("SYSTEM TRANSFER INHIBIT REQUEST SIGNAL\0",3,1);
				lcd_display("IS ACTIVE.\0",4,1);
				break;
		case 9: //TRANSFER INHIBITED DUE TO MULTIPLE GENSETS ONLINE
				clear_screen();
				lcd_display("TRANSFER INHIBITED. MULTIPLE GENSET's ON\0",1,1);
				lcd_display("ONLY ONE GENERATOR MAY BE ONLINE AT ONCE\0",3,1);
				lcd_display("DURING TRANSFERS DUE TO LOAD RESTRICTION\0",4,1);
				break;
		case 10: //CONVERTER ALREADY ONLINE
				clear_screen();
				lcd_display("TRANSFER ABORTED.\0",1,1);
				lcd_display("CONVERTER ALREADY ONLINE\0",3,1);
				break;
		case 11: //GENERATOR ALREADY ONLINE
				clear_screen();
				lcd_display("TRANSFER ABORTED.\0",1,1);
				lcd_display("GENERATOR ALREADY ONLINE\0",3,1);
				break;
		case 12: //GENERATOR SELECTED DOES NOT MATCH GENERATOR PRESENTLY ONLINE
				clear_screen();
				lcd_display("TRANSFER ABORTED.\0",1,1);
				lcd_display("GENERATOR SELECTED DOES NOT MATCH\0",3,1);
				lcd_display("GENERATOR THAT IS PRESENTLY ONLINE!\0",4,1);
				break;
		case 13: //NO SHORE POWER, TRANSFER TO CONVERTER PROHIBITED WHEN GENERATOR USED AS INPUT
				clear_screen();
				lcd_display("TRANSFER ABORTED.        NO SHORE POWER.\0",1,1);
				lcd_display("TRANSFER TO CONVERTER PROHIBITED WHEN   \0",3,1);
				lcd_display("CONVERTER INPUTS POWERED FROM GENERATORS\0",4,1);
				break;
		case 14: //NO INPUT POWER PRESENT
				clear_screen();
				lcd_display("TRANSFER ABORTED:        NO INPUT POWER.\0",1,1);
				lcd_display("CONVERTER'S SHORE POWER IS NOT ONLINE.  \0",3,1);
				if (GEN2GEN_OPTION && !NEW_G2G_OPTION) lcd_display("Both MASTER and SLAVE INPUTS MUST BE ON.\0",4,1);
				break;
		case 15: //NO CB_CLOSE CONFIRMATION RECEIVED
				clear_screen();
				lcd_display("TRANSFER ABORTED.\0",1,1);
				lcd_display("CONFIRMATION NOT RECEIVED FOR CB _CLOSE_\0",3,1);
				lcd_display("CHECK CB1, CB2, and CB3 AUX CONNECTIONS.\0",4,1);
				break;
		case 16: //NO CB_OPEN CONFIRMATION RECEIVED
				clear_screen();
				lcd_display("TRANSFER ABORTED.\0",1,1);
				lcd_display("CONFIRMATION NOT RECEIVED FOR CB _OPEN_ \0",3,1);
				lcd_display("CHECK CB1, CB2, and CB3 AUX CONNECTIONS.\0",4,1);
				break;
		case 17: //TRANSFER TO CONVERTER NOT COMPLETE--TIMEOUT WAITING FOR GEN_CB_OPEN CONFIRMATION
				lcd_display("TIME OUT OCCURED, GENERATOR NOT OFFLINE!\0",3,1);
				lcd_display("TRANSFER TO CONVERTER NOT COMPLETE.     \0",4,1);
				break;
		case 18: //TRANSFER TO GENERATOR NOT COMPLETE--TIMEOUT WAITING FOR GEN_CB_CLOSE CONFIRMATION
				lcd_display("TIME OUT OCCURED, GEN_CONF NOT DETECTED.\0",2,1);
				lcd_display("TRANSFER TO GENERATOR NOT COMPLETE.     \0",4,1);
				break;
		case 19: //BAD TRANSFER REQUEST
				clear_screen();
				lcd_display("TRANSFER ABORTED:  BAD TRANSFER REQUEST\0",1,1);
				lcd_display("Bus Status:  \0",2,1);
//				lcd_display("Transfer ID: \0",3,1);
//				lcd_display("TRANSFER SEQUENCE PENDING.\0",4,1);
				if (NEW_G2G_OPTION)
					lcd_display(itoa(get_busState2()),2,14);
				else
					lcd_display(itoa(get_busState()),2,14);
//				lcd_display(itoa(transferRequest),3,14);
				break;
		case 20: //PRESS SHIP'S POWER GENERATOR TO TRANSFER POWER
				clear_screen();
				lcd_display("PRESS:  SHIP'S POWER-> GENERATOR \0",2,3);
				lcd_display("TO TRANSFER POWER ON SHIP'S BUS  \0",3,3);
				break;
		case 21: //PRESS SHIP'S POWER GENERATOR TO TRANSFER POWER
				clear_screen();
				lcd_display("NO-BREAK-TRANSFERS PROHIBITED.   \0",1,1);
				lcd_display("CHECK CONVERTER STATUS AND METER.\0",2,1);
				lcd_display("CHECK GENERATOR STATUS AND METER.\0",3,1);
				lcd_display("CHECK CONVERTER/GENERATOR WIRING.\0",4,1);
				break;
		case 22: //tie breaker open and not in maintenanceMode.
				clear_screen();
				lcd_display("CONVERTER POWER CAN NOT BE APPLIED TO   \0",1,1);
				lcd_display("SHIP'S BUS DUE TO OPEN TIE_BREAKER_CB.  \0",2,1);
				lcd_display("SET MAINTENANCE MODE IF REQUIRED.\0",3,1);
				lcd_display("PRESS 'CONVERTER_POWER_OFF' TO CLEAR MSG\0",4,1);
				break;

		case 23: //NO CONVERTER POWER PRESENT
				clear_screen();
				lcd_display("TRANSFER ABORTED:    NO CONVERTER POWER.\0",1,1);

				if (MULTIGEN_BESECKE && !EXTERNAL_CB_CLOSED())
//abort transfer w/msg that 2Q1 is open
					lcd_display("CONVERTER'S OUTPUT CB, 2Q1, is OPEN.    \0",3,1);
				else
					lcd_display("CONVERTER'S OUTPUT POWER IS NOT READY.  \0",3,1);
				break;

		case 24: //V_OUT_OK UNEXPECTEDLY WENT LOW
				clear_screen();
				lcd_display("TRANSFER ABORTED:     V_OUT_OK WENT LOW.\0",1,1);
				lcd_display("V_OUT_OK UNEXPECTEDLY WENT LOW ON XFR.  \0",3,1);
				lcd_display("CHECK GENERATOR VOLTAGE IS IN 20% RANGE.\0",4,1);
				break;
		case 25: //MANUAL MODE.
				clear_screen();
				lcd_display("TRANSFER ABORTED:    MANUAL MODE IS SET.\0",1,1);
				lcd_display("SET TO 'AUTOMATIC' MODE FOR TRANSFERS.  \0",3,1);
				break;
		case 26: //MAINTENANCE.
				clear_screen();
				lcd_display("TRANSFER ABORTED:      MAINTENANCE MODE.\0",1,1);
				lcd_display("DISABLE MAINTENANCE MODE FOR TRANSFERS. \0",3,1);
				break;
		case 27: //TRIM TIMEOUT.
				clear_screen();
				lcd_display("TRANSFER ABORTED:    SPEED-TRIM TIMEOUT.\0",1,1);
				lcd_display("GENERATOR SPEED-TRIM TIMEOUT, BAD FREQ. \0",3,1);
				break;
		case 28: //SEEK TIMEOUT.
				clear_screen();
				lcd_display("TRANSFER ABORTED:          SEEK TIMEOUT.\0",1,1);
				lcd_display("TIMEOUT OCCURED SEEKING ZERO CROSSING.  \0",3,1);
				break;
		case 29: //CONVERTER CAPACITY EXCEEDED BY LOAD.
				clear_screen();
				lcd_display("TRANSFER ABORTED:         CONV. CAPACITY\0",1,1);
				lcd_display("CONVERTER CAPACITY IS EXCEEDED BY LOAD. \0",3,1);
				break;
		default: //SPARE FAILURE ENCOUNTERED
				break;
	}

	if (abort_code)
	{
		if (abort_code!=22 || abort_code!=last_transfer_status)
			LogEvent(Ev_ERR_XFR,abort_code);
	}

	last_transfer_status = (unsigned char) abort_code;

	display_function = F1; 
	display_type = SYSTEM_STATUS; 
	new_key=0;
	special_function=0;
	combo_key=86;

	pause(2000);
//	if(new_key!=18) pause(3000);

//	wait_for_key();

//	clear_screen();

	abort_power_on=0;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*
	_getbit(P7,7)		// CLOSE_CB1
	_getbit(P7,6)		// CLOSE_CB2
	tieBreaker_CLOSED()	// CLOSE_CB3
	cb4_CLOSED()		// CLOSE_CB4
	cb5_CLOSED()		// CLOSE_CB2
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void transfer_timeout_control_screen()	//ASEA3 v2.00.
{
  unsigned temp_transfer_timeout=(unsigned)transfer_timeout/1000;

	clear_screen();
	lcd_display(" GENERATOR CONFIRMATION TIMEOUT CONTROL \0",1,1);
	lcd_display(" TRANSFER TIMEOUT:     SECONDS\0",2,1);
//	lcd_display("F1 to increase, F2 to decrease,",3,1);
//	lcd_display("SYSTEM STATUS to exit screen.",4,1);
	lcd_display(" More    Less                      Exit \0",4,1);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display("     \0",2,19);	//clear field

		lcd_display(itoa(temp_transfer_timeout),2,21);

		put_cursor(2,22);
		delay(500);

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:

					if (temp_transfer_timeout<20)
					{
						temp_transfer_timeout = temp_transfer_timeout + 1;
					}
					break;

			case 20:	//F2:

					if (temp_transfer_timeout>1)
					{
						temp_transfer_timeout = temp_transfer_timeout - 1;
					}
					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(500);	
						break;

			default:
					break;
		}
	}
	transfer_timeout = temp_transfer_timeout * 1000;
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*
	if (_getbit(P7,7) && cb4_CLOSED()) return;							// CB1+4 closed.
	if (_getbit(P7,7) && tieBreaker_CLOSED() && cb5_CLOSED()) return;	// CB1+3+5 closed.
	if (_getbit(P7,6) && cb5_CLOSED()) return;							// CB2+5 closed.
	if (_getbit(P7,6) && tieBreaker_CLOSED() && cb4_CLOSED()) return;	// CB2+3+4 closed.
/*---------------------------------------------------------------------------*/
////////////////////////////////////////////////////////
void startGenset(int genset)
{
  int k;
  int running=TRUE;
  unsigned warmUp=generator_on_delay*4;	//convert seconds to 250ms units.
  float vmin=90.0;	//smallest reasonable voltage to indicate genset running.
  float vmax=600.0;	//BIG value.
  float fmin=30.0;	//max freq window.
  float fmax=70.0;	//max freq window.
//
// GENSET ON?
//
	if (gen_start_cmd_enable==ENABLED) //GENERATOR AUTOSTART W/PROGRAMMED DELAY.
//	if ((gen_start_cmd_enable==ENABLED) && (TECHNEL_OPTION)) //GENERATOR AUTOSTART W/PROGRAMMED DELAY.
	{
		if (genset==GEN2 && !MULTIGEN_OPTION)
		{
			if(!voltage_in_range(SYS,SYS_sync,vmin,vmax))
			{
				running = FALSE;
			}

			if(!frequency_in_range(SYS,SYS_sync,fmin,fmax))
			{
				running = FALSE;
			}
		}
		else if (genset>2 && !MULTIGEN_OPTION)
		{
			if(!voltage_in_range(GEN,GEN_sync,vmin,vmax))
			{
				running = FALSE;
			}
			if(!frequency_in_range(GEN,GEN_sync,fmin,fmax))
			{
				running = FALSE;
			}
			if(!voltage_in_range(SYS,SYS_sync,vmin,vmax))
			{
				running = FALSE;
			}
			if(!frequency_in_range(SYS,SYS_sync,fmin,fmax))
			{
				running = FALSE;
			}
		}
		else if ((genset==2 || genset==4) && (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE))
		{
			if(!voltage_in_range(SYS,SYS_sync,vmin,vmax))
			{
				running = FALSE;
			}
			if(!frequency_in_range(SYS,SYS_sync,fmin,fmax))
			{
				running = FALSE;
			}
		}
		else
		{
			if(!voltage_in_range(GEN,GEN_sync,vmin,vmax))
			{
				running = FALSE;
			}

			if(!frequency_in_range(GEN,GEN_sync,fmin,fmax))
			{
				running = FALSE;
			}
		}
	}
	else
	{
		return;		//genset start disabled or option not set, so exit.
	}

	if (running) return;	//genset already running, so exit.

	lcd_display("GENSET      AUTOSTART INITIATED . . . \0",3,1);

	switch (genset)
	{
		case 1:	//GEN#1
				set_gen_mux(GEN1);
				lcd_display("#1\0",3,8);
				set_GEN_START_CMD();	//ISSUE GENERATOR START COMMAND.
				break;

		case 2:	//GEN#2
				lcd_display("#2\0",3,8);

				if (MULTIGEN_OPTION)
				{
					set_gen_mux(GEN2);
#ifdef WESTPORT50
					if (SWITCHGEAR_INTERFACE_OPTION && OPTCON_PCB)
						set_GEN2_START_CMD();	//ISSUE GENERATOR START COMMAND.
					else
						set_GEN_START_CMD();	//ISSUE GENERATOR START COMMAND.
#else
						set_GEN_START_CMD();	//ISSUE GENERATOR START COMMAND.
#endif //~WESTPORT50
				}
				else
				{
					set_GEN2_START_CMD();	//ISSUE GENERATOR START COMMAND.
				}
				break;

		case 3:	//GEN#3
				if (MULTIGEN_OPTION)
				{
					set_gen_mux(GEN3);
					lcd_display("#3\0",3,8);
					set_GEN_START_CMD();	//ISSUE GENERATOR START COMMAND.
				}
				else
				{
					set_GEN_START_CMD();	//ISSUE GENERATOR START COMMAND.
					set_GEN2_START_CMD();	//ISSUE GENERATOR START COMMAND.
					lcd_display("#1,2\0",3,8);
				}
				break;

		case 4:	//GEN#4
				if (MULTIGEN_OPTION)
				{
					set_gen_mux(GEN4);
					lcd_display("#4\0",3,8);
					set_GEN_START_CMD();	//ISSUE GENERATOR START COMMAND.
				}
				else
				{
					set_GEN_START_CMD();	//ISSUE GENERATOR START COMMAND.
					set_GEN2_START_CMD();	//ISSUE GENERATOR START COMMAND.
					lcd_display("#1,2\0",3,8);
				}
				break;

		default:	//invalid genset id
				set_GEN_START_CMD();	//ISSUE GENERATOR START COMMAND.
				set_GEN2_START_CMD();	//ISSUE GENERATOR START COMMAND.
				lcd_display("#1,2\0",3,8);
				break;
	}

	for (k=0;k<warmUp;k++)	//genset WARM-UP time.
	{
//		IE_main();
		remoteManager();

		delay(250);	//delay one second (generator_on_delay) times.
		lcd_display("GENSET   WARM-UP IN PROGRESS . . .\0",3,1);
	}	

	initialize_generator_status();
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
////////////////////////////////////////////////////////
void stopGenset(int genset)
{
  int k;
  unsigned coolDown=generator_off_delay*4;	//convert seconds to 250ms units.

	if(gen_stop_cmd_enable==DISABLED) return;	//bail if feature disabled.

	lcd_display("GENSET      AUTO-STOP INITIATED . . . \0",3,1);

	for (k=0;k<coolDown;k++)	//genset COOL-DOWN time.
	{
//		IE_main();
		remoteManager();

		delay(250);	//delay one second (generator_off_delay) times.
		lcd_display("GENSET COOL-DOWN IN PROGRESS . . .\0",3,1);
	}	

	switch (genset)
	{
		case 0:	//invalid genset id
				break;

		case 1:	//GEN#1
				set_gen_mux(GEN1);
				lcd_display("#1\0",3,8);
				set_GEN_STOP_CMD();	//ISSUE GENERATOR STOP COMMAND.
				break;

		case 2:	//GEN#2
				lcd_display("#2\0",3,8);

				if (MULTIGEN_OPTION)
				{
					set_gen_mux(GEN2);
					set_GEN_STOP_CMD();	//ISSUE GENERATOR STOP COMMAND.
				}
				else
				{
					set_GEN2_STOP_CMD();	//ISSUE GENERATOR STOP COMMAND.
				}
				break;

		case 3:	//GEN#3
				if (MULTIGEN_OPTION)
				{
					set_gen_mux(GEN3);
					lcd_display("#3\0",3,8);
					set_GEN_STOP_CMD();	//ISSUE GENERATOR STOP COMMAND.
				}
				else
				{
					set_GEN_STOP_CMD();	//ISSUE GENERATOR STOP COMMAND.
					set_GEN2_STOP_CMD();	//ISSUE GENERATOR STOP COMMAND.
					lcd_display("#1,2\0",3,8);
				}
				break;

		case 4:	//GEN#4
				if (MULTIGEN_OPTION)
				{
					set_gen_mux(GEN4);
					lcd_display("#4\0",3,8);
					set_GEN_STOP_CMD();	//ISSUE GENERATOR STOP COMMAND.
				}
				else
				{
					set_GEN_STOP_CMD();	//ISSUE GENERATOR STOP COMMAND.
					set_GEN2_STOP_CMD();	//ISSUE GENERATOR STOP COMMAND.
					lcd_display("#1,2\0",3,8);
				}
				break;

		default:	//both gensets
				set_GEN_STOP_CMD();	//ISSUE GENERATOR STOP COMMAND.
				set_GEN2_STOP_CMD();	//ISSUE GENERATOR STOP COMMAND.
				lcd_display("#1,2\0",3,8);
				break;
	}

//	for (k=0;k<coolDown;k++)	//genset COOL-DOWN time.
//	{
////	IE_main();
//		remoteManager();
//		delay(250);	//delay one second (generator_off_delay) times.
//	}	

	initialize_generator_status();
}
/*---------------------------------------------------------------------------*/
void maintenance_mode_control_screen() //NEW_G2G_OPTION. (enable/disable maintenanceMode).
{
  int edit_field=0;

	clear_screen();
	lcd_display("GENERATOR MAINTENANCE MODE CONTROL\0",1,4);
	lcd_display(" MAINTENANCE MODE:                      \0",3,1);
	lcd_display("Enable  Disable                    Exit \0",4,1);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		if(maintenanceMode==ENABLED)
		{
			lcd_display(" ENABLED \0",3,20);
		}
		else
		{
			lcd_display("DISABLED \0",3,20);
		}

		put_cursor(3,20);
		pause(500);

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
					maintenanceMode = ENABLED;		//No transfers, CP_ON ok.
					break;

			case 20:	//F2:
					maintenanceMode = DISABLED;		//Transfers ok if bus well-defined.
					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
//						delay(500);	
						break;

			default:
					break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void generator_stop_control_screen() //NEW_G2G_OPTION. (not used for std. TECHNEL_OPTION).
{
  int edit_field=0;

	clear_screen();
	lcd_display("GENERATOR SHUTDOWN CONTROL\0",1,8);
	lcd_display(" COOL-DOWN TIME     SECONDS    \0",2,1);
	lcd_display(" AUTO-STOP\0",3,1);
//	lcd_display(" More    Less    Forward   Back    Exit \0",4,1);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display("   \0",2,17);	//clear field

		if (generator_off_delay < 10)
		{
			lcd_display(itoa(generator_off_delay),2,18);
		}
		else
		{
			lcd_display(itoa(generator_off_delay),2,17);
		}
	
		if(gen_stop_cmd_enable==ENABLED)
		{
			lcd_display(" ENABLED \0",3,12);
		}
		else
		{
			lcd_display("DISABLED \0",3,12);
		}

		if (edit_field==0)
		{
//			lcd_display(" More    Less    Forward   Back    Exit \0",4,1);
			lcd_display(" More    Less    Cursor            Exit \0",4,1);
////////////////////////////////////////////////////////////////
		//				 1234567 1234567 12345678 1234567 1234567
////////////////////////////////////////////////////////////////
			put_cursor(2,17);
			pause(500);
		}
		else
		{
//			lcd_display("Enable  Disable  Forward   Back    Exit \0",4,1);
			lcd_display("Enable  Disable  Cursor            Exit \0",4,1);
			put_cursor(3,12);
			pause(500);
		}

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
					if (edit_field==0)
					{
						if (generator_off_delay<600) generator_off_delay++;
					}
					else
					{
						gen_stop_cmd_enable = ENABLED;		//inhibit if overload.
					}
					break;

			case 20:	//F2:
					if (edit_field==0)
					{
						if (generator_off_delay>5) generator_off_delay--;
					}
					else
					{
						gen_stop_cmd_enable = DISABLED;		//inhibit if overload.
					}
					break;

			case 8:		//F3:
			case 9:		//F4:
					if (edit_field==0)
					{
						edit_field = 1;	//toggle edit fieldt.
//						pause(500);
					}
					else
					{
						edit_field = 0;	//toggle edit fieldt.
//						pause(500);
					}
					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
//						delay(500);	
						break;

			default:
					break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
void generator_control_screen()	//gen. start, stop, and NEW_G2G_OPTION control.
{
  int edit_field=0;
  int row[3]=	{ 2,  3, 3};
  int column[3]={22, 11,33};

  char *status[2]={	"DISABLED\0",
			   		"ENABLED \0"};
refresh:
	clear_screen();
	lcd_display(" GENERATOR CONTROL CONFIGURATION ACCESS \0",1,1);
	lcd_display("Start    Stop  Maintenance Trim    Exit \0",4,1);

	delay(500); 		//wait a 1/2 sec. for operator to see screen.

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		delay(250);
		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
						generator_start_control_screen();
						delay(250);
						new_key = 0;
						goto refresh;
//						break;

			case 20:	//F2:
						generator_stop_control_screen();
						delay(250);
						new_key = 0;
						goto refresh;
//						break;

			case 8:		//F3:
						maintenance_mode_control_screen();
						delay(250);
						new_key = 0;
						goto refresh;
//						break;

			case 9:		//F4:
						gen2genActuatorResponseEdit();
						delay(250);
						new_key = 0;
						goto refresh;
//						break;


			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(500);	
						break;

			default:
						break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
void gen2genActuatorResponseEdit() //NEW_G2G_OPTION. (not used for std. TECHNEL_OPTION).
{
  char *ms_units="ms\0";

	clear_screen();
	lcd_display("GENERATOR SPEED-TRIM CONTROL SETUP      \0",1,1);
	lcd_display(" SETTING NOW:                           \0",3,1);
////////////////////////////////////////////////////////////////
//				 1234567 1234567 12345678 1234567 1234567
//				 1234567890123456789012345678901234567890
//				          1         2         3         4
////////////////////////////////////////////////////////////////
	lcd_display(" More    Less                      Exit \0",4,1);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display("   ACTUATOR RESPONSE DELAY\0",2,14);
		lcd_display("     \0",3,15);	//clear old number.
		lcd_display(utoa(actuatorResponseDelay),3,15);
		lcd_display(ms_units,3,20);

		put_cursor(3,15);

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
					if (actuatorResponseDelay<10000) actuatorResponseDelay = actuatorResponseDelay+10;
					break;

			case 20:	//F2:
					if (actuatorResponseDelay>20) actuatorResponseDelay = actuatorResponseDelay-10;
					break;

			case 8:		//F3:
			case 9:		//F4:
			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(500);	
						break;
			default:
					break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
int do_G2G_transfer( int genset, int cbIn, int cbOut)
{
  int cbConfReceived=FALSE;
  int sync_status=OK;
  int genLock=NO;
  unsigned temp_daq_mux_id=daq_mux_id;
  unsigned temp_sync_mux_id=sync_mux_id;
  unsigned long timeOut=eventTime;
  unsigned secondTimeThrough=NO;
  float vmin=GP_OUTPUT_VOLTAGE - 0.2 * GP_OUTPUT_VOLTAGE;
  float vmax=GP_OUTPUT_VOLTAGE + 0.2 * GP_OUTPUT_VOLTAGE;
  float fmin=GP_OUTPUT_FREQUENCY - 0.1 * GP_OUTPUT_FREQUENCY;
  float fmax=GP_OUTPUT_FREQUENCY + 0.1 * GP_OUTPUT_FREQUENCY;
  float target_frequency;

//
//Validate both GEN1 & GEN2 voltage and frequency AND set target_frequency.
//
	if (genset==GEN1)
	{
		target_frequency = gp_Freq[SYS];	//target GEN2
	}
	else
	{
		target_frequency = gp_Freq[GEN];	//target GEN1
	}

	if(!voltage_in_range(SYS,SYS_sync,vmin,vmax))
	{
		sync_status = NOT_OK;
		abort_power_on = 33;
	}
	if(!frequency_in_range(SYS,SYS_sync,fmin,fmax))
	{
		sync_status = NOT_OK;
		abort_power_on = 44;
	}
	if(!voltage_in_range(GEN,GEN_sync,vmin,vmax))
	{
		sync_status = NOT_OK;
		abort_power_on = 33;
	}
	if(!frequency_in_range(GEN,GEN_sync,fmin,fmax))
	{
		sync_status = NOT_OK;
		abort_power_on = 44;
	}

	if(sync_status == NOT_OK)
	{
		if (!abort_power_on) abort_power_on=2;
		return (abort_power_on);
	}

	clear_screen();

	display_type=GENERATOR_POWER;
	display_function=F4;
	CC29IC = 0x0077;	// enable gen#1 generator sync interrupt.
	CC22IC = 0x0073;	// enable gen#2 sync interrupt.

	delay(100);	//allow a few cycles to get good freq data, v2.32

//////////////////////////////////////////////////////////////////
//v2.33 get frequency of target using LIVE DATA
//
	if (genset==GEN2)	//so, target Gen#1
	{					 
		if (deltaT5gen1 > 0)				
		{					 
			target_frequency 	= 	TICK_COUNT/(float)deltaT5gen1;	//calculate frequency from tick count.
		}
		else
		{
			target_frequency=0.0;
		}
	}
	else //genset==GEN1		so, target Gen#2
	{					 
		if (deltaT5gen2 > 0)				
		{					 
			target_frequency 	= 	TICK_COUNT/(float)deltaT5gen2;	//calculate frequency from tick count.
		}
		else
		{
			target_frequency=0.0;
		}
	}
//
//validate target frequency.
//
	if (fmax<target_frequency || target_frequency<fmin)
	{
		return (abort_power_on=44);	//abort due to target frequency out of range.
	}
//////////////////////////////////////////////////////////////////

////development code
//lcd_display("G2G TRANSFER ARGS: ( _, _ , _ , _ )\0",4,1);
//lcd_display(itoa(genset),4,22);
//lcd_display(itoa(cbIn),4,25);
//lcd_display(itoa(cbOut),4,29);
//lcd_display(itoa(shutdown_flag),4,33);
//delay(2000);
////development code

	lcd_display("CAUTION:   POWER TRANSFER IN PROGRESS   \0",1,1);
	lcd_display("GENSET #  SYNC IN PROGRESS:             \0",2,5);
	if (genset==GEN2) lcd_display("2\0",2,13);
	else lcd_display("1\0",2,13);

	genLock = whileNotInRange();
	if (genLock) goto hardLocked;

tryOnceMore:
////////////////////////////////////////WIP
	lcd_display("SPD TRIM\0",2,33);
////////////////////////////////////////WIP

	if (adjustGenSpeed(genset,target_frequency+0.1)==NOT_OK)
	{
  		lcd_display("GENSET FREQUENCY RESTORE IN PROGRESS\0",4,1);

//		deselectGenSpeedMux();	//this will cause preselected genSpeed to slew back to nominal.
		setGenSpeedMux(NONE);	//NEW_G2G_OPTION speed-trim MUX.

		init_PWM3(9,0, 50);	//initialize PWM3 to given duty & resolution.
		return (abort_power_on);	//using PWM3
	}

hardLocked:
	lcd_display("GEN SEEK\0",2,33);

	timeOut=eventTime;

	while (!(genLock=whileNotInRange()))
	{
		if (eventTime >= (timeOut+6000))	// 6000 eventTime units = 1 minute timeout.
		{
////////////////////////////////////////WIP
//Reattempt Speed-Trim Once on Time-out.
			if (secondTimeThrough++)
			{
				lcd_display("TIME-OUT\0",2,33);
				lcd_display("GENSET FREQUENCY RESTORE IN PROGRESS\0",4,1);

				setGenSpeedMux(NONE);	//NEW_G2G_OPTION speed-trim MUX.

				init_PWM3(9,0, 50);	//initialize PWM3 to given duty & resolution.
				return (abort_power_on=28);
			}
			else
			{
				goto tryOnceMore;
			}
		}
//
//abort?
//
		strobe_kybd_for_slew();		//sets 'new_key=-1' and polls kybd for active key-down event.
		if (new_key==18)	//bail on SYSTEM_STATUS key.
		{
			return (abort_power_on=23);		//operator abort during phase-lock seek
		}
	}	

	lcd_display("IN RANGE\0",2,33);
////////////////////////////////////////WIP

	lcd_display("         CIRCUIT BREAKER CLOSE COMMANDED\0",3,1);
  	cbConfReceived = FALSE;

	switch (cbIn)
	{
		case CLOSE_CB1:
						lcd_display("GEN #1 \0",3,1);
						set_GEN_1_CB_CLOSE();	//enable GENERATOR, close generator contactor.
						delayWithFreqLock(2000,CLOSE_CB1);
						if (Gen1conf)	cbConfReceived = TRUE;
						break;
		case CLOSE_CB2:
						lcd_display("GEN #2 \0",3,1);
						set_GEN_2_CB_CLOSE();	//enable GENERATOR, close generator contactor.
						delayWithFreqLock(2000,CLOSE_CB2);
						if (Gen2conf)	cbConfReceived = TRUE;
						break;
		case CLOSE_CB3:
						lcd_display("CB3 TIE\0",3,1);
						set_TIE_BREAKER_CB_CLOSE();	//toggle "TIE_BREAKER_CB_CLOSE" signal high then low.
						delayWithFreqLock(2000,CLOSE_CB3);
						if (tieBreaker_CLOSED())	cbConfReceived = TRUE;
						break;
		default:
						break;
	}

	if (cbConfReceived)
	{
		lcd_display("CIRCUIT BREAKER CLOSED.        \0",3,10);
	}
	else
	{
		lcd_display("TIME-OUT.  NO CONFIRMATION.    \0",3,10);
		output_OFF();
		remote_off_flag=YES;
		set_GEN_CBs_OPEN();	//openAllCBs();
		lcd_display("GENSET FREQUENCY RESTORE IN PROGRESS\0",4,1);

//		deselectGenSpeedMux();	//this will cause preselected genSpeed to slew back to nominal.
		setGenSpeedMux(NONE);	//NEW_G2G_OPTION speed-trim MUX.

		init_PWM3(9,0, 50);	//initialize PWM3 to given duty & resolution.
		return (abort_power_on = 60);
	}

	lcd_display("GENERATORS ARE NOW HEAD-TO-HEAD . . .\0",4,1);

	lcd_display("         CIRCUIT BREAKER OPEN COMMANDED \0",3,1);

  	cbConfReceived = FALSE;

	switch (cbOut)
	{
		case OPEN_CB1:
						lcd_display("GEN #1 \0",3,1);
						set_GEN_1_CB_OPEN();	//disable GENERATOR, open generator contactor.
						delayWithFreqLock(2000,OPEN_CB1);
						if (!Gen1conf)	cbConfReceived = TRUE;
						break;
		case OPEN_CB2:
						lcd_display("GEN #2 \0",3,1);
						set_GEN_2_CB_OPEN();	//disable GENERATOR, open generator contactor.
						delayWithFreqLock(2000,OPEN_CB2);
						if (!Gen2conf)	cbConfReceived = TRUE;
						break;
		case OPEN_CB3:
						lcd_display("CB3 TIE\0",3,1);
						set_TIE_BREAKER_CB_OPEN();	//toggle "TIE_BREAKER_CB_OPEN" signal high then low.
						delayWithFreqLock(2000,OPEN_CB3);
						if (!tieBreaker_CLOSED())	cbConfReceived = TRUE;
						break;
		default:
						break;
	}

	if (cbConfReceived)
	{
		lcd_display("CIRCUIT BREAKER OPENED.        \0",3,10);
		lcd_display("                                     \0",4,1);
	}
	else
	{
		lcd_display("TIME-OUT.  NO CONFIRMATION.    \0",3,10);
		output_OFF();
		remote_off_flag=YES;
		set_GEN_CBs_OPEN();	//openAllCBs();
		abort_power_on = 61;
	}

	last_transfer_status = (unsigned char) OK;

//
//DE-SELECT GEN SPEED CONTROL MUX
//
	lcd_display("GENSET FREQUENCY RESTORE IN PROGRESS\0",4,1);

//	deselectGenSpeedMux();	//this will cause preselected genSpeed to slew back to nominal.
	setGenSpeedMux(NONE);	//NEW_G2G_OPTION speed-trim MUX.

	init_PWM3(9,0, 50);	//initialize PWM3 to given duty & resolution.

	set_meter_mux(temp_daq_mux_id, temp_sync_mux_id);	//restore daq & sync mux settings.

	pause(1000);

	return abort_power_on;
}
/*---------------------------------------------------------------------------*/
//int adjustGenSpeed(int genset, float target_frequency, unsigned msg)	//using PWM3
int adjustGenSpeed(int genset, float target_frequency)	//using PWM3
{
  int status=NOT_OK;
  int duty;
  int direction;
  unsigned long timeOut=eventTime;
  volatile float actual_frequency=0.0;
  float remoteFreq=0.0;
//  float fmin=GP_OUTPUT_FREQUENCY - 0.025 * GP_OUTPUT_FREQUENCY;
//  float fmax=GP_OUTPUT_FREQUENCY + 0.025 * GP_OUTPUT_FREQUENCY;
  float fmin=GP_OUTPUT_FREQUENCY - 0.05 * GP_OUTPUT_FREQUENCY;
  float fmax=GP_OUTPUT_FREQUENCY + 0.05 * GP_OUTPUT_FREQUENCY;

//
//set gen speed control MUX
//
	setGenSpeedMux(genset);	//NEW_G2G_OPTION speed-trim MUX.

	if (genset==1)
	{
		duty = gen1DutyCycle;
	}
	else
	{
		duty = gen2DutyCycle;
	}

//
//show a synchroscope--now done in whileNotInRange() call.
//
//	if (msg)
//	{
//  		lcd_display("     Hz            \06\07                 Hz",3,1);
//		lcd_display("                                        ",4,1);
//	}

//
//get initial actual frequency.
//
	if (genset==GEN1 && deltaT5gen1 > 0)				
	{					 
		actual_frequency 	= 	TICK_COUNT/(float)deltaT5gen1;	//calculate frequency from tick count.

		if (deltaT5gen2 > 0)				
		{					 
			remoteFreq 	= 	TICK_COUNT/(float)deltaT5gen2;	//calculate frequency from tick count.
		}
		else
		{
			remoteFreq=0.0;
		}
	}
	else if (genset==GEN2 && deltaT5gen2 > 0)				
	{					 
		actual_frequency 	= 	TICK_COUNT/(float)deltaT5gen2;	//calculate frequency from tick count.

		if (deltaT5gen1 > 0)				
		{					 
			remoteFreq 	= 	TICK_COUNT/(float)deltaT5gen1;	//calculate frequency from tick count.
		}
		else
		{
			remoteFreq=0.0;
		}
	}

//
//validate frequency range.
//
	if (fmax<actual_frequency || actual_frequency<fmin)
	{
		abort_transfer(4);			//abort_power_on=44;
		return (status=NOT_OK);
	}

//
//do we NEED to correct speed-trim?  Check window.
//
	if (((target_frequency-0.05) < actual_frequency) && (actual_frequency < (target_frequency+0.05)))
	{
//		if (target)	//force change if no target, otherwise, good enough.
		{
			return (status=OK);		//already in frequency window.
		}
	}

//
//do we need to go faster or slower?
//
	if (actual_frequency<=target_frequency)
	{
		direction = (-1);
	}
	else
	{
		direction = (1);
	}
	
//
//adjust speed and check frequency
//
	while (((target_frequency-0.05) > actual_frequency) || (actual_frequency > (target_frequency+0.025)))
	{
//
//time-out?
//
		if (eventTime >= (timeOut+6000))	// 6000 eventTime units = 1 minute timeout.
		{
			lcd_display("TIME OUT\0",2,33);
			abort_power_on=27;			//abort_transfer(4);
			return (status=NOT_OK);
		}
//
//abort?
//
		strobe_kybd_for_slew();		//sets 'new_key=-1' and polls kybd for active key-down event.

		if (new_key==18)	//bail on SYSTEM_STATUS key.
		{
			abort_power_on=23;		//operator abort during phase-lock seek.
			return (status=NOT_OK);
		}
//
//re-establish direction
//
		if (actual_frequency<=target_frequency)		//ferret around if necessary, v1.04
		{
			direction = -1;
		}
		else
		{
			direction = 1;
		}
//
//adjust PWM3 WAS:in 1% steps, IS:in 1 count steps.
//
		duty = duty + direction;

//		if (0<duty && duty<100)
		if (0<duty && duty<PP3)
		{
//			init_PWM3(9,0, duty);	//initialize PWM3 to given duty & resolution.
			_nop();
			set_PWM3( duty );			//sets PWM3 duty cycle to given binary count, 0<=count<=PP3.
			lastDuty = duty;		//used to calibrate NEW_G2G_OPTION speed-trim Null.
		}
//
//Actuator Response Delay
//
		delay(actuatorResponseDelay);				//wait 500ms to allow genEngineSpeed time to settle.		
//
//get new frequency
//
		if (genset==GEN1 && deltaT5gen1 > 0)				
		{					 
			actual_frequency 	= 	TICK_COUNT/(float)deltaT5gen1;	//calculate frequency from tick count.

			if (deltaT5gen2 > 0)				
			{					 
				remoteFreq 	= 	TICK_COUNT/(float)deltaT5gen2;	//calculate frequency from tick count.
			}
			else
			{
				remoteFreq=0.0;
			}
		}
		else if (genset==GEN2 && deltaT5gen2 > 0)				
		{					 
			actual_frequency 	= 	TICK_COUNT/(float)deltaT5gen2;	//calculate frequency from tick count.

			if (deltaT5gen1 > 0)				
			{					 
				remoteFreq 	= 	TICK_COUNT/(float)deltaT5gen1;	//calculate frequency from tick count.
			}
			else
			{
				remoteFreq=0.0;
			}
		}
		else
		{
			actual_frequency=0.0;
		}
//
//show a synchroscope
//
		whileNotInRange();	//just used here for synchroscope display.
//		if (msg)
//		{
//			lcd_display("     Hz            \06\07                 Hz",3,1);
//			lcd_display_value(actual_frequency,3,1,"%3.1f");
//			lcd_display_value(remoteFreq,3,34,"%3.1f");
//		}

	}
	return (status=OK);
}
/*---------------------------------------------------------------------------*/
int whileNotInRange()	//returns: 1 when GEN zero crossings are within 10degrees of each other.
{						//		   0 if time-out or otherwise not coincident.
  unsigned inSync=NO;
  unsigned inRange=NO;
  int localZero=(int)(sync_count & 0xFF);
  int remoteZero=(int)(sync_1_count & 0xFF);
  int deltaZero=remoteZero-localZero;
  float localFreq=gp_Freq[GEN];
  float remoteFreq=0.0;
  float fDelta;

	if (localFreq < 40.0 || localFreq > 70.0)
	{
		return 0;
	}

	if (deltaT5gen1 > 0)				
	{					 
		localFreq 	= 	TICK_COUNT/(float)deltaT5gen1;	//calculate frequency from tick count.
	}

	if (deltaT5gen2 > 0)				
	{					 
		remoteFreq 	= 	TICK_COUNT/(float)deltaT5gen2;	//calculate frequency from tick count.
	}
	else
	{
		remoteFreq=0.0;
	}

//
//show a synchroscope
//
	lcd_display("     Hz            \06\07                 Hz\0",3,1);
	lcd_display_value(localFreq,3,1,"%5.2f\0");
	lcd_display_value(remoteFreq,3,34,"%5.2f\0");

//clear period counts for next pass (in case signal goes away).
//	deltaT5gen2=0;

	if ((remoteFreq < (localFreq-5.0)) || (remoteFreq > (localFreq+5.0)))
	{
		inSync = NO;
//		return 0;
	}

	if (deltaZero>128) deltaZero = deltaZero-256;

	fDelta = (float)deltaZero;
	fDelta = fDelta/6.4;

	if (fDelta < (-20.0)) 	fDelta = fDelta + 40.0;	//keep on scale.
	if (fDelta > 20.0) 	fDelta = fDelta - 40.0;		//keep on scale.

	deltaZero = (int)fDelta;

	if (localFreq < (remoteFreq-0.2))
	{
		inRange=NO;
		inSync=NO;
	}
	else if (localFreq > (remoteFreq+0.2))
	{
		inRange=NO;
		inSync=NO;
	}
	else
	{
		inRange=YES;
	}

	lcd_display("                                        \0",4,1);
//	lcd_display(blankLine,4,1);

	if (deltaZero<0)
	{
		inSync=NO;
		deltaZero = deltaZero + 19;
		if (0<deltaZero && deltaZero<41) lcd_display("\05\0",4,deltaZero);
	}
	else if (deltaZero>0)
	{
		inSync=NO;
		deltaZero = deltaZero + 20;
		if (0<deltaZero && deltaZero<41) lcd_display("\05\0",4,deltaZero);
	}
	else
	{	
		if (inRange==YES) 
		{
			inSync=YES;	//in sync
		}
		lcd_display("\05\05\0",4,20);
	}

	return inSync;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void tip_delay_control_screen()	//TIP_OPTION.
{
  int edit_field=0;

	clear_screen();
	lcd_display("   TRANSFER IN PROGRESS SIGNAL CONTROL\0",1,1);
	lcd_display(" TRANSFER DELAY    SECONDS     \0",2,1);
	lcd_display(" FEATURE IS:\0",3,1);
	lcd_display(" More    Less    Forward   Back    Exit \0",4,1);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display("  \0",2,17);	//clear field

		if (tip_delay < 10)
		{
			lcd_display(itoa(tip_delay),2,18);
		}
		else
		{
			lcd_display(itoa(tip_delay),2,17);
		}
	
		if(tip_cmd_enable==ENABLED)
		{
			lcd_display(" ENABLED \0",3,14);
		}
		else
		{
			lcd_display("DISABLED \0",3,14);
		}

		if (edit_field==0)
		{
			lcd_display(" More    Less    Forward   Back    Exit \0",4,1);
			put_cursor(2,17);
			pause(500);
		}
		else
		{
			lcd_display("Enable  Disable  Forward   Back    Exit \0",4,1);
			put_cursor(3,14);
			pause(500);
		}

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
					if (edit_field==0)
					{
						if (tip_delay<10) tip_delay++;
					}
					else
					{
						tip_cmd_enable = ENABLED;		//inhibit if overload.
					}
					break;

			case 20:	//F2:
					if (edit_field==0)
					{
						if (tip_delay>1) tip_delay--;
					}
					else
					{
						tip_cmd_enable = DISABLED;		//inhibit if overload.
					}
					break;

			case 8:		//F3:
			case 9:		//F4:
					if (edit_field==0)
					{
						edit_field = 1;	//toggle edit fieldt.
						pause(500);
					}
					else
					{
						edit_field = 0;	//toggle edit fieldt.
						pause(500);
					}
					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(500);	
						break;

			default:
					break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
#define MAX_CB_DELAY (5000)
/*---------------------------------------------------------------------------*/
void ext_CB_TestMode_Header (void)
{
			   //1234567890123456789012345678901234567890
	lcd_display("EXTERNAL OUTPUT CIRCUIT-BREAKER TEST    \0",1,1);
	lcd_display("CB Command =         Confirm =          \0",2,1);
	lcd_display(" Close   Open                      Exit \0",4,1);
}

void ext_CB_TestMode (void)
{
	unsigned int CB_Cmd = 0;
	unsigned int ext_Open_delay_save;
	unsigned int ext_Close_delay_save;
	
	clear_screen();
	
	if (gp_state.inpstat1 == 1) {
		lcd_display("SHORE POWER IS NOT OFFLINE.             \0",2,1);
		lcd_display("Press any key to continue...            \0",3,1);
		wait_for_key();
		new_key=0;
	}
	else {
		ext_CB_TestMode_Header();
		
		ext_Open_delay_save = external_cb_Open_delay;
		external_cb_Open_delay = 200;
		
		ext_Close_delay_save = external_cb_delay;
		external_cb_delay = 200;
		
		while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
		{
			if (CB_Cmd)
				lcd_display("CLOSE\0",2,14);
			else
				lcd_display("OPEN \0",2,14);
				
			if (EXTERNAL_CB_CLOSED())
				lcd_display("CLOSED\0",2,32);
			else
				lcd_display("OPENED\0",2,32);
				
//			wait_for_key();
	
			switch (new_key)
			{
				case 19:	//F1:
						if (gp_state.inpstat1 == 1) {
							lcd_display("SHORE POWER IS NOT OFFLINE.             \0",2,1);
							lcd_display("Press any key to continue...            \0",3,1);
							wait_for_key();
							new_key=0;
							
							ext_CB_TestMode_Header ();
						}
						else {	
							set_EXT_CB_CLOSE();
							CB_Cmd = 1;
						}
						break;
	
				case 20:	//F2:
						set_EXT_CB_OPEN();
						CB_Cmd = 0;
						break;
			
				case 10:	//F5:
						set_EXT_CB_OPEN();
						new_key = 18;	//force SYSTEM STATUS key press response.
						
						external_cb_Open_delay = ext_Open_delay_save;
						external_cb_delay = ext_Close_delay_save;
						break;
			}
		}
	}
}

void external_cb_control_Header()
{
	clear_screen();
	lcd_display("EXTERNAL OUTPUT CIRCUIT-BREAKER CONTROL \0",1,1);
	lcd_display("CB DELAY Close =      ms, Open =      ms\0",2,1);
	lcd_display("EXTERNAL OUTPUT CB MONITORING:          \0",3,1);
}

void external_cb_control_screen()	//EXT_CB_OPTION, v1.40.
{
	int edit_field=0;

	external_cb_control_Header();
////////////////////////////////////////////////////////////////
//				 1234567 1234567 12345678 1234567 1234567
//				 1234567890123456789012345678901234567890
//				          1         2         3         4
////////////////////////////////////////////////////////////////

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display("     \0",2,17);	//clear field
		lcd_display("     \0",2,33);	//clear field

		lcd_display(itoa(external_cb_delay),2,18);
		lcd_display(itoa(external_cb_Open_delay),2,34);
	
		if(monitorExtCb==ENABLED)
		{
			lcd_display(" ENABLED \0",3,32);
		}
		else
		{
			lcd_display("DISABLED \0",3,32);
		}

		if (edit_field==0)
		{
//			lcd_display(" More    Less    Cursor  Restore   Exit\0",4,1);
			lcd_display(" More    Less    Cursor  TestMode  Exit\0",4,1);
			put_cursor(2,18);
			pause(500);
		}
		else if (edit_field==1)
		{
//			lcd_display(" More    Less    Cursor  Restore   Exit\0",4,1);
			lcd_display(" More    Less    Cursor  TestMode  Exit\0",4,1);
			put_cursor(2,34);
			pause(500);
		}
		else
		{
//			lcd_display("Enable  Disable  Cursor  Restore   Exit\0",4,1);
			lcd_display("Enable  Disable  Cursor  TestMode  Exit\0",4,1);
			put_cursor(3,32);
			pause(500);
		}

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
					if (edit_field==0)
					{
						if (external_cb_delay<MAX_CB_DELAY)
						{
							external_cb_delay = external_cb_delay + 100;
						}
					}
					else if (edit_field==1)
					{
						if (external_cb_Open_delay<MAX_CB_DELAY)
						{
							external_cb_Open_delay = external_cb_Open_delay + 100;
						}
					}
					else
					{
						monitorExtCb = ENABLED;
					}
					break;

			case 20:	//F2:
					if (edit_field==0)
					{
						if (external_cb_delay>0)
						{
							external_cb_delay = external_cb_delay - 100;
						}
					}
					else if (edit_field==1)
					{
						if (external_cb_Open_delay>0)
						{
							external_cb_Open_delay = external_cb_Open_delay - 100;
						}
					}
					else
					{
						monitorExtCb = DISABLED;
					}
					break;

			case 8:		//F3:
					if (edit_field<2)
					{
						edit_field++;	//toggle edit fieldt.
						pause(500);
					}
					else
					{
						edit_field = 0;	//toggle edit fieldt.
						pause(500);
					}
					break;

			case 9:		//F4:
//					external_cb_delay = 700;				//12112014: 200;
//					external_cb_Open_delay = 700;			//12112014: 3000;
					ext_CB_TestMode();
					
					if (new_key != 18)
						external_cb_control_Header();

					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(500);	
						break;

			default:
					break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*  One Delay for Open&Close--OLD
void extertal_cb_control_screen()	//EXT_CB_OPTION, v1.40.
{
  int edit_field=0;

	clear_screen();
	lcd_display("EXTERNAL OUTPUT CIRCUIT-BREAKER CONTROL \0",1,1);
	lcd_display("CIRCUIT DELAY =       MILLISECONDS.     \0",2,1);
	lcd_display("EXTERNAL OUTPUT CB MONITORING:          \0",3,1);
	lcd_display(" More    Less    Forward   Back    Exit \0",4,1);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display("     \0",2,17);	//clear field

		if (external_cb_delay < 10000)
		{
			lcd_display(itoa(external_cb_delay),2,18);
		}
		else
		if (external_cb_delay < 100)
		{
			lcd_display(itoa(external_cb_delay),2,19);
		}
		else
		if (external_cb_delay < 10)
		{
			lcd_display(itoa(external_cb_delay),2,20);
		}
		else
		{
			lcd_display(itoa(external_cb_delay),2,17);
		}
	
		if(monitorExtCb==ENABLED)
		{
			lcd_display(" ENABLED \0",3,32);
		}
		else
		{
			lcd_display("DISABLED \0",3,32);
		}

		if (edit_field==0)
		{
			lcd_display(" More    Less    Forward   Back    Exit \0",4,1);
			put_cursor(2,20);
			pause(500);
		}
		else
		{
			lcd_display("Enable  Disable  Forward   Back    Exit \0",4,1);
			put_cursor(3,32);
			pause(500);
		}

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
					if (edit_field==0)
					{
						if (external_cb_delay<MAX_CB_DELAY)
						{
							external_cb_delay = external_cb_delay + 100;
						}
					}
					else
					{
						monitorExtCb = ENABLED;
					}
					break;

			case 20:	//F2:
					if (edit_field==0)
					{
						if (external_cb_delay>0)
						{
							external_cb_delay = external_cb_delay - 100;
						}
					}
					else
					{
						monitorExtCb = DISABLED;
					}
					break;

			case 8:		//F3:
			case 9:		//F4:
					if (edit_field==0)
					{
						edit_field = 1;	//toggle edit fieldt.
						pause(500);
					}
					else
					{
						edit_field = 0;	//toggle edit fieldt.
						pause(500);
					}
					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(500);	
						break;

			default:
					break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#ifdef VCO_MOD
//////////////////////////////////////////////////////////////////////////////
//
// Frequency Program Mode, allows freq. to be set by 0-5Vdc external signal.
//
// SIGNALS:		
//			VCO_ON_CMD	output	+15Vdc=ON, -15Vdc=OFF		P3.13	P3A-9
//			LVC			reference low-voltage-common				P3A-10
//			VCO_ON_CONF	input	5Vdc=ENABLE, 0Vdc=DISABLE	P5.13	P3A-19
//			VCO_CONTROL	input	0V=min.Freq, 5V=max.Freq	P5.10	P3A-14
//						0V=F_nom-3Hz, 2.5V=F_nom, 5V=F_nom+3Hz
//
//////////////////////////////////////////////////////////////////////////////
void doVCO()
{
  float VCO_frequency;

	frequency_mode=_getbit(P5,13);	//VCO (OFF/ON) control.

	if(frequency_mode)	//this if processes in less than 4 milliseconds.
	{
		VCO_frequency = get_average_frequency();

		if (pgm_edit.Freq!=VCO_frequency)
		{
			slew_output(newSlewRate, VCO_frequency, newVoltage);
		}
	}
	else	//recover if necessary.
	{
		if (pgm_edit.Freq!=newFrequency)
		{
			slew_output(newSlewRate, newFrequency, newVoltage);
		}
	}
}
/*---------------------------------------------------------------------------*/
unsigned get_adc_count(unsigned adc_channel)
{
  unsigned count;
  int data=0;

	ADCON=0;				//clear ADCON control register

	_nop();

	ADCON = (0x1080 | adc_channel); 	// start adc read.

	_nop();

	while (_getbit(ADCON,8)) _nop();			//stall if busy.

	data = ADDAT;						// get data from previous conversion.
	data = (data & 0x03FF);				//strip-off channel number to get data.
	count = (unsigned) data;

	ADCON = 0;							// disable adc read.

	return count;
}
/*---------------------------------------------------------------------------*/
float get_average_frequency()
{
  float fstep=(VCO_MAX-VCO_MIN)/1024.0;
  float new_frequency=0.0;
  unsigned count=0;
  int i;

	for (i=0;i<8;i++)	//collect 8 samples to average.
	{
		count = count + get_adc_count(10);	//sample from channel 10.
	}
	count = count>> 3;	//divide-by-8 to average samples.

	new_frequency = (count*fstep+VCO_MIN);
	if (new_frequency>F_MAX) new_frequency = VCO_MAX;
	if (new_frequency>F_SPAN) new_frequency = VCO_MAX;
	if (new_frequency<F_MIN) new_frequency = VCO_MIN;

	return new_frequency;
}
/*---------------------------------------------------------------------------*/
void testVCO()
{
	_nop();
}
#endif //VCO_MOD
/*---------------------------------------------------------------------------*/
void reset_cb_control_screen()	// v2.29.
{
  int edit_field=0;

	clear_screen();
	lcd_display("AUTOMATIC CIRCUIT-BREAKER RESET CONTROL \0",1,1);
	lcd_display("  RESET DELAY =       MILLISECONDS.     \0",2,1);
	lcd_display("  AUTOMATIC CB RESETS ON TRIP:          \0",3,1);

//	delay(500);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display("     \0",2,17);	//clear field

		if (resetDelay < 10000)
		{
			lcd_display(itoa(resetDelay),2,18);
		}
		else
		if (resetDelay < 100)
		{
			lcd_display(itoa(resetDelay),2,19);
		}
		else
		if (resetDelay < 10)
		{
			lcd_display(itoa(resetDelay),2,20);
		}
		else
		{
			lcd_display(itoa(resetDelay),2,17);
		}
	
		if(resetCbOn==ENABLED)
		{
			lcd_display(" ENABLED \0",3,32);
		}
		else
		{
			lcd_display("DISABLED \0",3,32);
		}

		if (edit_field==0)
		{
			lcd_display(" More    Less    Cursor            Exit \0",4,1);
			put_cursor(2,20);
			delay(500);
		}
		else
		{
			lcd_display("Enable  Disable  Cursor            Exit \0",4,1);
			put_cursor(3,32);
			delay(500);
		}

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
					if (edit_field==0)
					{
						if (resetDelay<3000)
						{
							resetDelay = resetDelay + 100;
						}
					}
					else
					{
						resetCbOn = ENABLED;
					}
					break;

			case 20:	//F2:
					if (edit_field==0)
					{
						if (resetDelay>100)
						{
							resetDelay = resetDelay - 100;
						}
					}
					else
					{
						resetCbOn = DISABLED;
					}
					break;

			case 8:		//F3:
			case 9:		//F4:
					if (edit_field==0)
					{
						edit_field = 1;	//toggle edit fieldt.
					}
					else
					{
						edit_field = 0;	//toggle edit fieldt.
					}
					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						break;

			default:
					break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void set_PWM3(unsigned count)	//initialize PWM3 to given count.
{
  unsigned period = 1 << (mach.pwm3_bits-mach.pwm3_align);

	_putbit(0,PWMCON0,3);				//stop PWM counter.
	_nop();

	PP3	= period - 1;					//set PWM period.
	_nop();

	if ((0<count) && (count<period))	//if inrange
	{
		PW3	= count;					//set PWM pulse width.
		_nop();
	}

	_putbit(1,PWMCON0,3);				//run PWM3 counter.
	PT3 = 0;							//set initial timer value.
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
//	original--no MULTIGEN & no COCBO
int process_REM_XFR_REQ()
{
  int selected_gen=autoTransferGenset;
  int Vgen1=(int)gp_Van[GEN];
  int Vgen2=(int)gp_Van[SYS];

	if (!(Remote_Panel_Xfr_Req_Stat & 0x0001))
	{
		LogEvent(Ev_XFR_REQ_RCVD,0);
		Remote_Panel_Xfr_Req_Stat |= 0x0001;
	}
	
	if (!transfer_in_progress)
	{
		if (gp_state.genstat1==ONLINE) // GEN #1 CONTACTOR CONFIRM
		{
			if (gp_state.invstat==OFFLINE)
			{
				if (gp_state.genstat2==OFFLINE)
				{	
					selected_gen = GEN1;
					g2c = TRUE;
				}
			}
		}
		else if (gp_state.genstat2==ONLINE)
		{
			if (gp_state.invstat==OFFLINE)
			{
				if (gp_state.genstat1==OFFLINE)
				{
					selected_gen = GEN2;
					g2c = TRUE;
				}
			}
		}
		else if (gp_state.invstat==ONLINE)
		{
			if (gp_state.genstat2==OFFLINE)
			{
				if (gp_state.genstat1==OFFLINE)
				{
					selected_gen = autoTransferGenset;

					if (Vgen1 > 90)
					{
						if (Vgen2 < 90)
						{
							selected_gen = GEN1;
						}
					}
					else if (Vgen2 > 90)
					{
						if (Vgen1 < 90)
						{
							selected_gen = GEN2;
						}
					}
					
					c2g = TRUE;

				}//endif (gp_state.genstat1==OFFLINE)
			}//endif (gp_state.genstat2==OFFLINE)
		}//endelse if (gp_state.invstat==ONLINE)
		else
		{
			if (gp_state.sysstat==OK)
			{							//Dead Bus Closure
				remote_off_flag=NO;
				if (EXT_CB_OPTION) {		//DHK 10142015: V9.03
					set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
					enable_output();
				}
				else {
					enable_output();
					set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
				}
				delay(2000);			//give time to release button on REMOTE PANEL
			}
			else
			{
				if (!(Remote_Panel_Xfr_Req_Stat & 0x0002))
				{				
					LogEvent(Ev_XFR_REQ_IGNORED,0);
					Remote_Panel_Xfr_Req_Stat |= 0x0002;
				}
			}
		}
	}//endif (!transfer_in_progress)
	else
	{
		if (!(Remote_Panel_Xfr_Req_Stat & 0x0002))
		{
			LogEvent(Ev_XFR_REQ_IGNORED,0);
			Remote_Panel_Xfr_Req_Stat |= 0x0002;
		}
	}//endelseif (!transfer_in_progress)

	return selected_gen;
}//end--REMOTE TRANSFER REQUEST
/*---------------------------------------------------------------------------*/
//
//	process_MULTIGEN_REM_XFR_REQ() returns a GEN_ID (1,2,3,or 4) or 0
//
//	Automatically determines transfer to do and initiates transfer.
//
//	Also used for Besecke's M/Y Bounty Hunter to determine running generator for auto-selection.
//	MULTIGEN_BESECKE flag inhibits transfer request and logging.
//
/*---------------------------------------------------------------------------*/
int process_MULTIGEN_REM_XFR_REQ()
{
//  int selected_gen=autoTransferGenset;	//ABB-do not transfer to gen if more than one gen RUNNING
  int selected_gen=0;
  int gen1_inRange=FALSE;
  int gen2_inRange=FALSE;
  int gen3_inRange=FALSE;
  int gen4_inRange=FALSE;
  float vmin=90.0;	//smallest reasonable voltage to indicate genset running.
  float vmax=600.0;	//BIG value.
  int genMuxId=get_gen_id();		//get present mux position so we can recover to same.

//#ifdef WESTPORT50
//	MULTIGEN_BESECKE = TRUE;
//#endif //WESTPORT50

	if (MULTIGEN_BESECKE)
	{
		selected_gen=autoTransferGenset;
	}
	else
	{
		LogEvent(Ev_XFR_REQ_RCVD,0);
	}

	if (!transfer_in_progress || MULTIGEN_BESECKE)
	{
		if (multipleGensetsOnline() && !MULTIGEN_BESECKE)
		{	
			if (MULTIGEN_INHIBIT) abort_transfer(9);	//added inhibit test 1/29/09
		}
		else if (gensetOnline())	//then need to determine which gen.
		{
			if (eGen1_CLOSED()) selected_gen = 1;
			else if (eGen2_CLOSED()) selected_gen = 2;
			else if (eGen3_CLOSED()) selected_gen = 3;
			else if (eGen4_CLOSED()) selected_gen = 4;
			else selected_gen = 0;

			if (!MULTIGEN_BESECKE)
				if (gp_state.invstat==OFFLINE) g2c = TRUE;		//initiate Transfer to Converter.
		}
//		else if (gp_state.invstat==ONLINE || MULTIGEN_BESECKE)
		else if (gp_state.invstat==ONLINE)
		{
			set_gen_mux(1);
			if(voltage_in_range(GEN,GEN_sync,vmin,vmax))
			{
					gen1_inRange = TRUE;
			}
			set_gen_mux(2);

			if (!SWITCHGEAR_INTERFACE_OPTION)	//v2.99 change to accomidate old Multigen w/XFR_REQ
			{
				if(voltage_in_range(GEN,GEN_sync,vmin,vmax))
				{
						gen2_inRange = TRUE;
				}
			}
			else
			{
				if(voltage_in_range(SYS,SYS_sync,vmin,vmax))
				{
						gen2_inRange = TRUE;
				}
			}
			set_gen_mux(3);
			if(voltage_in_range(GEN,GEN_sync,vmin,vmax))
			{
					gen3_inRange = TRUE;
			}
			set_gen_mux(4);

			if (!SWITCHGEAR_INTERFACE_OPTION)
			{
				if(voltage_in_range(GEN,GEN_sync,vmin,vmax))
				{
						gen4_inRange = TRUE;
				}
			}
			else
			{
				if(voltage_in_range(SYS,SYS_sync,vmin,vmax))
				{
						gen4_inRange = TRUE;
				}
			}

			if (gen1_inRange)
			{
//				if (!gen2_inRange && !gen3_inRange && !gen4_inRange)
				{
					selected_gen = GEN1;
				}
			}
			else if (gen2_inRange)
			{
//				if (!gen1_inRange && !gen3_inRange && !gen4_inRange)
				{
					selected_gen = GEN2;
				}
			}
			else if (gen3_inRange)
			{
//				if (!gen1_inRange && !gen2_inRange && !gen4_inRange)
				{
					selected_gen = GEN3;
				}
			}
			else if (gen4_inRange)
			{
				if (!gen1_inRange && !gen2_inRange && !gen3_inRange)
				{
					selected_gen = GEN4;
				}
			}

			if (!MULTIGEN_BESECKE)
			{
				if (selected_gen)				//non-zero selected_gen
				{
	 				c2g = TRUE;
	 			}
				else
				{
					LogEvent(Ev_XFR_REQ_IGNORED,0);
//					if (MULTIGEN_INHIBIT) abort_transfer(9);
				}
			}

		}//endelse if (gp_state.invstat==ONLINE)
		else	//nothing online.
		{
			if (!MULTIGEN_BESECKE)
			{
				if (gp_state.sysstat==OK)
				{							//Dead Bus Closure
					remote_off_flag=NO;
					
					if (EXT_CB_OPTION) {		//DHK 10142015: V9.03
						set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
						enable_output();
					}
					else {
						enable_output();
						set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
					}
					
					delay(2000);			//give time to release button on REMOTE PANEL
				}
				else
				{
					LogEvent(Ev_XFR_REQ_IGNORED,0);
				}
			}
		}
	}//endif (!transfer_in_progress)
	else
	{
		if (!MULTIGEN_BESECKE)
			LogEvent(Ev_XFR_REQ_IGNORED,0);
	}//endelseif (!transfer_in_progress)

	set_gen_mux(genMuxId);		//recover previous mux position

//#ifdef WESTPORT50
//	MULTIGEN_BESECKE = FALSE;
//#endif //WESTPORT50

	return selected_gen;
}//end--REMOTE TRANSFER REQUEST
/*---------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*
void initialize_FIFOs()		// initialize all queue structures.
{
#ifdef TRACE_ON
  int trace_code=0;
	trace_queue=initializeQueue(&trace);
	enqueue(trace_queue, &trace_code); 		//circular trace queue.
#endif //TRACE_ON

	command_queue=initializeQueue(&command);
	exception_queue=initializeQueue(&exception);
}
/*---------------------------------------------------------------------------*/
//struct queueType command,*command_queue;
/*---------------------------------------------------------------------------*/
//	key_code=22;						//	initialize_shore_power_status(DUAL_INPUT);
//	enqueue(command_queue, &key_code);
/*---------------------------------------------------------------------------*/
int command_interpreter(unsigned cmd)
{
//  register int command_code=0;
//	while (!queueEmpty(command_queue))
//	{ 
//		command_code = dequeue(command_queue);
//		new_key=0;
//		switch (command_code)
//		{
//		}
//	}

	switch (cmd)
	{
		case 1:	//EPO
				LogEvent(Ev_COMM_EPO,0);
				set_REM_IN_OFF();
				input_dropped=FALSE;
				autorestart=DISABLED;
				shutoffId=3;			//reason input is off, EPO.
				break;

		case 2:	//SHORE POWER ON
				LogEvent(Ev_COMM_SHORE_ON,0);
				set_REM_IN_ON();
				break;

		case 3:	//SHORE POWER OFF
				LogEvent(Ev_COMM_SHORE_OFF,0);
				input_dropped=FALSE;
				autorestart=DISABLED;
				set_REM_IN_OFF();
				shutoffId=2;	//reason input is off, remote SP OFF.
				break;

		case 4:	//CONVERTER POWER ON
				LogEvent(Ev_COMM_CONV_ON,0);
				if (EXT_CB_OPTION) {					//DHK 10142015: V9.03
					remote_off_flag=NO;
					
					set_REM_OUT_ON();
					enable_output();
				}
				else {	
					enable_output();
					set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
				}
				break;

		case 5:	//CONVERTER POWER OFF
				LogEvent(Ev_COMM_CONV_OFF,0);
				disable_output();
				remote_off_flag=YES;
				break;

		case 6:
				break;
		case 7:
				break;
		case 8:
				break;

		case 9:	//LCD Contrast change
                init_PWM1(mach.pwm1_bits,mach.pwm1_align,mach.pwm1_dutyCycle);		//unsigned(bits,align,duty)
				break;

		case 10: //Slave Load Sharing change
				setLoadShare(lastMasterCapacity);
				break;

//Generator Commands
		case 11:	//SELECT GEN#1
				LogEvent(Ev_COMM_TS_G1_MAST,0);
				remote_genset = 1;
				if (MULTIGEN_OPTION) set_gen_mux(remote_genset);
				break;

		case 12:	//SELECT GEN#2
				LogEvent(Ev_COMM_TS_G2_MAST,0);
				remote_genset = 2;
				if (MULTIGEN_OPTION) set_gen_mux(remote_genset);
				break;

		case 13:	//SELECT GEN#3
				if (MULTIGEN_OPTION) set_gen_mux(remote_genset=3);
				break;

		case 14:	//SELECT GEN#4
				if (MULTIGEN_OPTION) set_gen_mux(remote_genset=4);
				break;

		case 15:	//GENERATOR START
				LogEvent(Ev_COMM_GENSTART_ON,0);
				startGenset(remote_genset);		//beware of remoteManager() call here!
				break;

		case 16:	//GENERATOR STOP
				LogEvent(Ev_GEN_STOP_CMD,0);
				stopGenset(remote_genset);		//beware of remoteManager() call here!
				break;

		case 17:	//CLOSE GENERATOR CB
				closeGenCB(remote_genset);		//beware of remoteManager() call here!
				break;

		case 18:	//OPEN GENERATOR CB
				openGenCB(remote_genset);		//beware of remoteManager() call here!
				break;

		case 19:	//GENERATOR something
				break;

		case 20:	//reInitialize Comm
 				IE_init();
				modbusActive = TRUE;
 				break;

		case 35:	//Tie Close
				set_TIE_BREAKER_CB_CLOSE();	//CB3 //toggle "TIE_BREAKER_CB_CLOSE" signal high then low.
				break;

		case 36:	//Tie Open
				set_TIE_BREAKER_CB_OPEN();	//CB3 //toggle "TIE_BREAKER_CB_OPEN" signal high then low.
				break;

		default:
				break;
	}

	mbCommand = 0;

	return TRUE;
}
/*---------------------------------------------------------------------------*/
void closeGenCB(unsigned genId)	//close genId's CB
{
	switch (genId)
	{
		case GEN1:
					set_GEN_1_CB_CLOSE();	//toggle "GEN_1_CB_CLOSE" signal.
					break;
		case GEN2:
					if (MULTIGEN_OPTION)
					{
						if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
						{
							set_GEN_2_CB_CLOSE();	//toggle "GEN_2_CB_CLOSE" signal.
						}
						else
						{
							set_GEN_1_CB_CLOSE();	//toggle "GEN_1_CB_CLOSE" signal.
						}
					}
					else
					{
						set_GEN_2_CB_CLOSE();	//toggle "GEN_2_CB_CLOSE" signal.
					}
					break;
		case GEN3:
					if (MULTIGEN_OPTION) set_GEN_1_CB_CLOSE();	//toggle "GEN_1_CB_CLOSE" signal.
					break;
		case GEN4:
					if (MULTIGEN_OPTION)
					{
						if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
						{
							set_GEN_2_CB_CLOSE();	//toggle "GEN_2_CB_CLOSE" signal.
						}
						else
						{
							set_GEN_1_CB_CLOSE();	//toggle "GEN_1_CB_CLOSE" signal.
						}
					}
					break;
		default:	//argError--bad genId.
					break;
	}
}
/*---------------------------------------------------------------------------*/
void openGenCB(unsigned genId)	//open genId's CB
{
	switch (genId)
	{
		case GEN1:
					set_GEN_1_CB_OPEN();	//toggle "GEN_1_CB_OPEN" signal.
					break;
		case GEN2:
					set_GEN_2_CB_OPEN();	//toggle "GEN_2_CB_OPEN" signal.
					break;
		case GEN3:
					set_GEN_1_CB_OPEN();	//toggle "GEN_1_CB_OPEN" signal.
					break;
		case GEN4:
					set_GEN_2_CB_OPEN();	//toggle "GEN_2_CB_OPEN" signal.
					break;
		default:	//argError--bad genId.
					break;
	}
}
/*---------------------------------------------------------------------------*/
//HYBRID_STO==============================================================================================
//while(highZ && genOffline && converterOnline)
void processHybridSTO()
{
	if (HYBRID_STO)
	{			
		if (HighZcmd)								//High-Z Cmd from switchgear
		{
			set_TIP_CMD();

			if (Gen1conf && (gp_status_word3 & 0x0010))	//Gen CB Confirm and K4_closed
			{
				LogEvent(Ev_XFR_TO_GEN,0);
				delay(generator_transfer_delay); //200ms default crossover

		 		output_OFF();

				clear_TIP_CMD();

				if (AUTO_SHUTDOWN && autoShutdownEnable)
				{
					check_gp_status();
					if(gp_state.invstat==OFFLINE)
					{
						LogEvent(Ev_AUTO_SHUTDOWN,0);
						set_REM_IN_OFF();
					}
				}
				last_transfer_status = (unsigned char) OK;
			}
		}
		else
		{
//			transfer_in_progress=NO;
			clear_TIP_CMD();
//			abort_transfer(1);	//transfer aborted by operator (or PLC)
		}
	}
}
//HYBRID_STO==============================================================================================
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
//HYBRID_STO==============================================================================================
/*void processHybridSTO()
{
			if (HYBRID_STO) 
			{
				if (HighZcmd)	//High-Z Cmd from switchgear
				{
					set_TIP_CMD();
					transfer_in_progress=YES;

					while (HighZcmd && !Gen1conf && (gp_status_word3 & 0x0010)) //while(highZ && genOffline && converterOnline)
					{
						if ((gp_status_word3 & 0x0010) && Gen1conf)	//K4_closed && Gen CB Confirm
						{
							delay(generator_transfer_delay); //200ms default crossover
					 		output_OFF();
							clear_TIP_CMD();
							transfer_in_progress=NO;	//was after output_OFF() till v1.93e
						}
						remoteManager();
						strobe_kybd_for_slew();	//sets 'new_key=0' and polls kybd for active key-down event.
						if(new_key==18) break;	//provide method for operator to break out
						check_gp_status();
						meas_all();				// calc metering data and do CSC.
			 			display_metering(display_type, display_function);		// Show updated meter.
					}
				}
				else
				{
					clear_TIP_CMD();
					transfer_in_progress=NO;
				}
			}
}
//HYBRID_STO==============================================================================================
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
////////////////////////////////////////////////////////
