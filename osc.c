/* @(#)OSC.C */
/* ASEA SOURCE CODE */
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	FILE:			osc.c
//	PURPOSE:		OSCILLATOR & TRANSIENT Source Module for the ASEA
//
//	Proprietary Software:	ASEA POWER SYSTEMS
//
//				COPYRIGHT @ ASEA JANUARY 1999
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

/* ======================================================================================= INCLUDE FILES */
#include "ac.h"

/*=========================================================================*/
/*=========================PUBLIC==========================================*/
//extern void init_outputs(void); 			// set outputs to nominal.
//extern void initialize_waveform_ram(void);
//extern void init_waveforms(void);
//extern void initialize_program_ram(void);
//extern void exec_osc_pgm(void);
//extern void set_Vr_ILMr(void);
//extern void CSC(void);							// Continuously self calibrate.

/*=========================================================================*/
/*=========================PRIVATE=========================================*/
interrupt(0x16) using (TRAN_RB) void cycle_step(void);	// CC6INT, move cycle_based transient step.
interrupt(0x21) using (TRAN_RB) void time_step(void);	// T1INT, move time transient step.

void init_transient_ram(void);
int slew_output(float, float, float);		//create and execute transient for output transistion.
int exec_trans(void);
void load_trans_counters(void);
void move_trans_step(void);
void trans_mode_OFF(void);
float calc_Tseg(float time);		/* calc seg time for edit_trans() and make_step_data(). */
void make_step_data(char SoftStart_flag);

void initialize_program_ram(void);
void init_outputs(void); 			// set outputs to nominal.
void exec_osc_pgm(void);
void calc_pgm_values(void);
void freq_calc(float freq); 			// output frequency calculation.
void set_Vr_ILMr(void);
void store_Pgm(void);			// store pgm_edit.

void CSC(void);							// Continuously self calibrate.
void scale_volts(void); 				// scale volts to load DACs.
void load_osc_regs(void);

void initialize_waveform_ram(void);
void init_waveforms(void);
void init_osc_ram(void);
void WF_calc(void);
unsigned int wf_address(unsigned int wf);

/*=========================================================================*/
/*=========================================================================*/


/* ========================================================================================================= */
/* ======================================================================================= PERSISTENT MEMORY */
#pragma noclear
#pragma align hb=c 								// ALIGNMENT: PEC ADDRESSABLE
/* ========================================================================================================= */
unsigned huge osc_wf[WF_ARRAY];		// executed waveforms, #pragma noclear or CSTART will blow up.

#pragma combine sb=A0xEC00
volatile system unsigned int pha_volts_dac;		// phase A voltage DAC.
#pragma combine sb=A0xED00
volatile system unsigned int pha_wf_dac;		// phase A waveform DAC.

#pragma combine sb=A0xEA00
volatile system unsigned int phb_volts_dac;		// phase B voltage DAC.
#pragma combine sb=A0xEB00
volatile system unsigned int phb_wf_dac;		// phase B waveform DAC.

#pragma combine sb=A0xE800
volatile system unsigned int phc_volts_dac;		// phase C voltage DAC.
#pragma combine sb=A0xE900
volatile system unsigned int phc_wf_dac;		// phase C waveform DAC.

/* ======================================================================================= PERSISTANT MEMORY */
#pragma default_attributes
/* ======================================================================================= PERSISTANT MEMORY */
unsigned int huge user_wf[WF_ARRAY];			/* user waveforms */
system unsigned int wf_steps;				/* number of steps in one waveform */
float F_min, F_max, V_min, V_max;			/* EXECuted Prog. Limits */
float F_span;						/* upper frequency hardware limit */
float Ka_wf, Kb_wf, Kc_wf;				/* holds SINErms/WFrms ratio of active waveform */
float Kout_A, Kout_B, Kout_C;				/* correct Vout using calibration Kfactor */
float Va_out, Vb_out, Vc_out;				/* dynamic output voltage (modified by wfK, XFMR_ratio, AUTO-CAL) */
float Va_pgm, Vb_pgm, Vc_pgm;				/* programmed output voltages */
int Form;						/* Form in use */
int Coupling;						/* coupling in use */
float XFMR_ratio;					/* XFMR_ratio in use */
float Vr;						/* voltage range */
float ILMr;						/* IL range in rms amps */
int ALC_mode;						/* 0=disabled, 1=Enabled */
int AGC_state;						/* 0=disabled, 1=Enabled */
int wf_no;						/* default for WF edit */

float wf_scale;						/* Scale for copying WFs. */
unsigned int WFa, WFb, WFc;				/* selected Waveform number */
unsigned int WF_edit[MAX_WF_STEPS];			/* declare WF edit area */
unsigned int prev_wf_steps;				/* previous number of steps in a waveform */
float WFk_wf[(WF_ARRAY/MAX_WF_STEPS)];			/* SINErms/WFrms ratio of active waveform */
int pgm_edit_no;					/* pgm being edited */
int orig_pgm_no;					/* holds # of real user program thats executed (NOT pgm0). */
int active_pgm_no;					/* pgm being executed */
int pgm_no;						/* general purpose pgm # indicator. */

struct transient_seg_structure Trans_seg[TOTAL_SEGS+1];
struct transient_seg_structure seg_edit[MAX_SEGS+1]; 	/* segment in edit. */
struct trans_step_structure huge step_block[(MAX_STEPS+1)*MAX_SEGS];
struct seg_info_structure far seg_info[MAX_SEGS+1];	/* used for active trans pgm. */
unsigned char trans_seg_table[TOTAL_SEGS+1];		/* Element # is i_seg#, */
unsigned int pgm_seg_table[MAX_SEGS+1];			/* temp list of i_segments for THIS pgm. */

struct user_pgm_structure far user_pgm[MAX_PGM+5];	/* declare user programs */
struct user_pgm_structure far pgm_edit;			/* make array for editing programs */
struct user_pgm_structure far def_pgm;			/* make array for factory defaults */
//struct user_pgm_structure far prev_pgm;			/* stores last pgm_edit'ed, */

#ifdef CALIBRATION_STATUS_CHECK
unsigned long calibration_status_word;
#endif // CALIBRATION_STATUS_CHECK

#ifdef CALIBRATION_RANGE_CHECK
float MIN_KFACTOR;
float MAX_KFACTOR;
#endif // CALIBRATION_RANGE_CHECK

/* ======================================================================================= VOLITILE MEMORY */
#pragma clear	// fill iram with cleared data so noclear data is put in batt ram.
/* ======================================================================================= VOLITILE MEMORY */
iram int trans_mode;			/* indicates trans in progress if ==1 */
iram int trans_changed_flag;		/* says current trans pgm was edited. */
iram unsigned int no_Tevents;		/* # of repetitions, 0=infinite */
iram unsigned int Tcount;		/* software counter in 100uS incs. of active step time */
iram int Tstep;				/* transient step_block counter */
iram int seg_no;			/* user pgm segment #, 1 of no_segs. */
iram int last_seg;
iram int soft_start_flag;
iram int cycle_based_flag;		/* says transient has 1 step per segment, and each is started by cycle-reset\. */
iram int trans_step_count;		/* track #of steps in isr. */
iram int Tsegno;			/* track #of segs in isr. */
iram int tran_ped_toggle;		/* toggles tran ped */
iram unsigned int wfp_a, wfp_b, wfp_c;	/* waveform pointers for PECs */
iram unsigned int pwm_f, pwm_s;		/* loads pwm frequency fast & slow from PECs*/
iram unsigned int pwm_c;		/* loads pwm_f and pwm_s, count fast & slow calculations*/
iram unsigned int cc1_cs;		/* loads CC0 and CC1, pwm count fast & slow calculations*/
iram unsigned int PHb, PHc;		/* loads CC3 and CC4, phase angles*/
iram unsigned int Va, Vb, Vc;		/* loads voltage dacs */
iram unsigned int WFPa, WFPb, WFPc;	/* loads waveform pointers */
iram unsigned int ILMa;			/* loads ilim dacs */
int exec_flag;				/* flag says pgm executed if true. */

/*=========================================================================*/
//				EXTERN DATA STRUCTURES
/*=========================================================================*/
extern volatile unsigned abort_power_on;
//extern volatile system unsigned int pha_volts_dac;		// phase A voltage DAC.
//extern volatile system unsigned int phb_volts_dac;		// phase B voltage DAC.
//extern volatile system unsigned int phc_volts_dac;		// phase C voltage DAC.
extern float Kmeter_I_A;
//extern float Kout_A, Kout_B, Kout_C;				/* correct Vout using calibration Kfactor */
extern float Van_csc, Vbn_csc, Vcn_csc;	/* one cycle RMS metered output value for csc*/
//extern int zop_mode;						/* 0=disabled, 1=enabled  */
extern float amps_volts;					/* sets ammeter scaling & ILM range */

//from SID.c
extern float GP_OUTPUT_VOLTAGE;
extern float GP_OUTPUT_FREQUENCY;
extern float GP_XFMR_RATIO;
extern unsigned GP_OUTPUT_COUPLING;
extern unsigned GP_OUTPUT_FORM;
extern struct gp_state_struct gp_state;
extern void initialize_sid(void);
extern float newSlewRate,newFrequency,newVoltage;	//used with slew_output() for load management.

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//@OSC		OSC		OSC		OSC		OSC		OSC		OSC		OSC		OSC		OSC		OSC		OSC		OSC@
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
/* ======================================================================================= FUNCTION DEFINITIONS */
/* ======================================================================================= INTERRUPTS/TASKS */
/*---------------------------------------------------------------------------*/
interrupt(0x16) using (TRAN_RB) void	// CC6INT, move cycle_based transient step.
cycle_step()
{
	move_trans_step();			// move transient step.
	CC6IC = 0;				// disable cycle_based transient.
	T1 = 65386;					// wait 60microseconds.
	T1IC = 0x73;				// enable timed transient.
}
/*---------------------------------------------------------------------------*/
interrupt(0x21) using (TRAN_RB) void	// T1INT, move time transient step.
time_step()
{
	if(Tcount > 1 )			// decrement software counter and do nothing.
	{
		Tcount--;			// software 100us counter.
		return;				// return and do nothing.
	}
	move_trans_step();		// move transient step.
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
//end of interrupt definitions.

/* ============================================================================================================ */
/* ======================================================================================= FUNCTION DEFINITIONS */
/*---------------------------------------------------------------------------*/
void init_transient_ram()
{
  int i;
  
  	for (i=0; i<TOTAL_SEGS; i++) trans_seg_table[i] =0;
	Trans_seg[0].T=0;
	pgm_seg_table[0]=0;

	Trans_seg[0].Va=user_pgm[0].Va;	/* copy current Voltages */
	Trans_seg[0].Vb=user_pgm[0].Vb;	/* to starting V. */
	Trans_seg[0].Vc=user_pgm[0].Vc;

	Trans_seg[0].WFa = user_pgm[0].WFa; /* Changed to use current WF */
	Trans_seg[0].WFb = user_pgm[0].WFb; /* since ph angles and Ilim */
	Trans_seg[0].WFc = user_pgm[0].WFc; /* don't change till end of ramp. */
	Trans_seg[0].F=user_pgm[0].Freq;	/* End of SOFT Start setup. */
}
/*---------------------------------------------------------------------------*/
////////////////////////////////////////////////////////////////////////////////
//	int slew_output(	float ramp_period,			// seconds.
//					 	float target_frequency, 	// Hz.
//						float target_voltage)		// Vrms Applied to Va,Vb,Vc.
//
//	returns:	1 =TRUE for success, ramp to target values executed immediately.
//				0 =FALSE for failure, new program not executed.
////////////////////////////////////////////////////////////////////////////////
//
int slew_output(float ramp_period, float target_frequency, float target_voltage)
{
  float Trans_seg_T;	//=Trans_seg[0].T;
  int status=0;

//	if (abort_power_on && target_voltage>0.0) return status;

//		newSlewRate = ramp_period;			//added v1,61 for remote Load Management use.
//		newFrequency = target_frequency;
//		newVoltage = target_voltage;

	if (target_voltage<0.0) target_voltage=0.0;
	if (target_frequency<20.0) target_frequency=20.0;

	if (gp_state.vout_range==HIGH_RANGE) 
	{
			target_voltage = target_voltage * 0.5;
	}

	if (ramp_period <= .0002)	 // Just do it in 200us.
		ramp_period = .0002;

	if (ramp_period > .0001)	 // If SOFT-START.
	{
		pgm_edit.Freq	=	target_frequency;
		pgm_edit.Va		=	target_voltage;
		pgm_edit.Vb		=	target_voltage;
		pgm_edit.Vc		=	target_voltage;

		Trans_seg_T = Trans_seg[0].T;			// save soft start time.
		Trans_seg[0].T = ramp_period;			// ramp_period in seconds.

		Trans_seg[0].Va=user_pgm[0].Va;			// start at present voltages.
		Trans_seg[0].Vb=user_pgm[0].Vb;
		Trans_seg[0].Vc=user_pgm[0].Vc;

		Trans_seg[0].F=user_pgm[0].Freq;	

/* End of SOFT Start setup. */

		user_pgm[0]=pgm_edit;	/* load up pgm 0 */

		calc_pgm_values();

		soft_start_flag=1;		// only 1 seg to do.

		make_step_data('S');	// expand Softstart segment data into step data.

		exec_trans();			// do Softstart now.

		while(trans_mode == 1);	// wait for Softstart trans to be done.

		soft_start_flag=0;		// clr flag.

		load_osc_regs();

		Trans_seg[0].T = Trans_seg_T;	/* restore soft start time */

		status=1;
	}
	return status;
}
/*---------------------------------------------------------------------------*/
int exec_trans()		/* EXECute transient for active pgm */
{					/* step data was produced after pgm was executed */
	no_Tevents=user_pgm[active_pgm_no].no_Tevents;	/* get # of events, */
										/* track #of events in isr. */
	trans_mode=1;		/* (used in place of IE_oper_pending.) */
	load_trans_counters();
	return 1;			/* ret ok */
}
/*---------------------------------------------------------------------------*/
void load_trans_counters()	/* start 1st segment, a new transient event */
{
	CC6IC = 0;	/* disable transient interrupt */
	T1IC = 0;
	Tstep=0;			/* first block_step is 0 */
	Tsegno=0;	/* get 1st seg #. */
	Tcount=seg_info[Tsegno].t; /* load default # of 100uS steps to count */
	trans_step_count=seg_info[Tsegno].nsteps;	/* get # of steps for 1st seg, */
	tran_ped_toggle=1;	/* make tran ped toggle */

	CC6 = 0xfffd;		// start transient at zero degrees

	CC6IC = 0x73;		/* enable transient at cycle_reset */
}
/*---------------------------------------------------------------------------*/
void move_trans_step()	/* interrupt calls this to move transient step */
{
	wfp_a = seg_info[Tsegno].WFPa;	/* change waveforms at next cycle reset */
	wfp_b = seg_info[Tsegno].WFPb;
	wfp_c = seg_info[Tsegno].WFPc;

//	if (tran_ped_toggle)	_putbit(1, P6, 7);	/* tran ped low */

	tran_ped_toggle=0;

	pwm_f = step_block[Tstep].pwm_c-1;	/* load output frequency */
	pwm_s = step_block[Tstep].pwm_c;
	CC0 = step_block[Tstep].cc1_cs-(WF_STEPS/2);
	CC1 = step_block[Tstep].cc1_cs;

//	_putbit(0, P6, 7);	/* tran ped hi */

	pha_volts_dac = step_block[Tstep].Va;	/* change voltages now */
	phb_volts_dac = step_block[Tstep].Vb;
	phc_volts_dac = step_block[Tstep].Vc;

	Tstep++;	/* inc step_block counter */

	trans_step_count--;				/* Track step count. */

	if (trans_step_count<=0)		/* if this seg data has been moved, */
	{
		trans_mode_OFF();
		return;
	}
	Tcount=seg_info[Tsegno].t; /* load default # of 100uS steps to count */
}
/*---------------------------------------------------------------------------*/
void trans_mode_OFF()	/* shuts down trans operation. */
{
	T1IC = 0;		/* disable timed transient */
	CC6IC = 0;		/* disable cycle_based transient */
	trans_mode=0;			/* clr flag for LEDs, timer interrupt. */
//	_putbit(1, P6, 7);	/* tran ped low */
//	IE_oper_complete();	/* indicate operation complete to GPIB if requested. */
}
/*---------------------------------------------------------------------------*/

float calc_Tseg(float time)	// calc seg time for edit_trans() and make_step_data().
{
  float x;						// modified Tseg
  float n;						// # of steps.
  int i;

	if (time < 0.00019)
	{
		if (seg_no != 1)
			return 0.0002;		// if seg# !=1, return minimum time.
		else
			return 0;			// else, assume t=0 and return since cycle based.
	}

	if 		(  0.1 > time)	x = time * 10000.0;		// find range of Tseg and set x.
	else if (  1.0 > time)	x = time * 1000.0;
	else if ( 10.0 > time)	x = time * 100.0;
	else if (100.0 > time)	x = time * 10.0;		
	else 					x = time;			  	// set default range.

	if 		(x > 900.0) n = x / 10.0; 	  	// find n.
	else if (x > 800.0) n = x / 9.0;			
	else if (x > 700.0) n = x / 8.0;			
	else if (x > 600.0) n = x / 7.0;			
	else if (x > 500.0) n = x / 6.0;			
	else if (x > 400.0) n = x / 5.0;		  
	else if (x > 300.0) n = x / 4.0;		   
	else if (x > 200.0) n = x / 3.0;		   
	else if (x > 100.0)	n = x / 2.0;		   
	else 				n = x;			// set default for n.

	i = n + 0.5;
	n = i;			/* round # of steps */

	x = n * .0001;					/* calc step time in # of 100uS incs.. */
	x = time / x;					

	i = x + 0.5;
	x = i;			/* round # of 100uS incs */

	time = x * n * .0001;			/* calc new Tseg. */

	seg_edit[0].nsteps = n;		/* store # of steps (transitions). */
	seg_edit[0].tstep  = x;		/* store step time in 100uS incs. */

	return time;
}
/*---------------------------------------------------------------------------*/
void make_step_data(char SoftStart_flag)
{
	int start;
	int n;
	int nsteps;			/* # of steps. */
	int step=0;			/* step_block pointer. */
	int laststep;
	int s;						/* step # */
	unsigned wfa;
	unsigned wfb;
	unsigned wfc;
	unsigned Va_int;
	unsigned Vb_int;
	unsigned Vc_int;
	float freq;
	float t;
	float dF;			/* delta vars. (step sizes). */
	float dVa;
	float dVb;
	float dVc;
	float ft;
	float Va_float;
	float Vb_float;
	float Vc_float;
	float Freq_max;						/* holds highest freq of pgm (including segments) */
	float max_Vtrans;					/* Holds max v for set_Psm(). */

	trans_mode_OFF();		// v403

	cycle_based_flag=0;				/* set default trans mode=time based. */

	if (SoftStart_flag=='S')		/* see if soft-start seg is to be calcd */
	{	start=0;		/* Only 1 seg for soft-start. */
		last_seg=0;
	}

	Freq_max=0;				/* holds highest freq of segments, (including pgm) */

	for (n=start; n<last_seg+1; n++)				/* scan all segs */
	{
		freq=Trans_seg[pgm_seg_table[n]].F;		/* get freq of seg n. */
		if (freq>Freq_max) Freq_max=freq;			/* store highest Freq found */
	}

	if (Freq_max < user_pgm[pgm_edit_no].Freq)
		Freq_max=user_pgm[pgm_edit_no].Freq;

	for (n=start; n<last_seg+1; n++)				/* scan all segs */
	{
		/* Steps are actually transitions (# of transitions=states+1) */
		seg_no=n;							/* Pass seg# for calc_Tseg(). */
		calc_Tseg(Trans_seg[pgm_seg_table[n]].T);	/* calc'd segment time. */
		nsteps=seg_edit[0].nsteps;		/* get # of steps (transitions). */
		t=seg_edit[0].tstep;		/* get step time in 100uS incs. */
		seg_info[n].nsteps=nsteps;	/* store nsteps for Trans. */
		seg_info[n].t=t; /* store step time as int (in 100uS steps) */

		wfa=(Trans_seg[pgm_seg_table[n]].WFa -1); /* get WF# */
		wfa=wf_address(wfa);	/* calc waveform address */
		seg_info[n].WFPa=wfa;	/* store wf address */

		wfb=(Trans_seg[pgm_seg_table[n]].WFb -1);		/* get WF# */
		wfb=wf_address(wfb);	/* calc waveform address */
		seg_info[n].WFPb=wfb;	/* store wf address */

		wfc=(Trans_seg[pgm_seg_table[n]].WFc -1);		/* get WF# */
		wfc=wf_address(wfc);	/* calc waveform address */
		seg_info[n].WFPc=wfc;	/* store wf address */

		if(n==1) /* set seg 1 starting values. carry over from steady state values. */
		{
			Trans_seg[pgm_seg_table[0]].F = pgm_edit.Freq;
			Trans_seg[pgm_seg_table[0]].Va = pgm_edit.Va;
			Trans_seg[pgm_seg_table[0]].Vb = pgm_edit.Vb;
			Trans_seg[pgm_seg_table[0]].Vc = pgm_edit.Vc;
		}

	   		/* starting values set in exec_osc_pgm */
		dF=pgm_edit.Freq - Trans_seg[0].F;	/* d is positive if ramp up*/
		dVa=pgm_edit.Va - Trans_seg[0].Va;	/* d is positive if ramp up*/
		dVb=pgm_edit.Vb - Trans_seg[0].Vb;	/* d is positive if ramp up*/
		dVc=pgm_edit.Vc - Trans_seg[0].Vc;	/* d is positive if ramp up*/

		if (nsteps!=1)				// v403
		{
			dF=dF/(nsteps-1);						/* make dF=step size */
			dVa=dVa/(nsteps-1);						/* make dV=step size */
			dVb=dVb/(nsteps-1);						/* make dV=step size */
			dVc=dVc/(nsteps-1);						/* make dV=step size */
		}
		else
		{
//			raise_exception(-25);
		}

		laststep=nsteps;		/* else # of transitions= # of durations+1. */

		max_Vtrans=0;
		for (s=0; s< laststep; s++)			/* MAKE NSTEPS now. */
		{
			ft=Trans_seg[0].F+(dF*s); /* take start of seg + stepsize */

			freq_calc(ft);		/* sets pwm_c, cc1_cs */
			step_block[step].pwm_c=pwm_c;	/* store in step ram. */
			step_block[step].cc1_cs=cc1_cs;	/* store in step ram. */

			Ka_wf=WFk_wf[Trans_seg[pgm_seg_table[n]].WFa -1]; /* get SINErms/WFrms ratio of waveform */
			Kb_wf=WFk_wf[Trans_seg[pgm_seg_table[n]].WFb -1];
			Kc_wf=WFk_wf[Trans_seg[pgm_seg_table[n]].WFc -1];

			Va_float=(Trans_seg[0].Va+(dVa*s) );	/* take start of seg n, add stepsize */
			
			if (max_Vtrans<Va_float) max_Vtrans=Va_float; /*  Store highest vac for set_Psm(). */
 
 			Va_float *= Ka_wf * Kout_A;		// correct Vpgm for WF in use & Kautocal

			if(pgm_edit.coupling==1)
			{
				Va_int=Va_float * V_DAC;		/* scale volts, convert to int. */
			}
			else
            {
				if (XFMR_ratio!=0.0)
                {
					Va_int=Va_float * V_DAC / XFMR_ratio;		/* scale volts, convert to int. */
                }
				else
                {
//					raise_exception(-16);
           		}
			}
			step_block[step].Va=Va_int;	/* store int in step ram. */

			Vb_float=(Trans_seg[0].Vb+(dVb*s) );	/* take */

			if (max_Vtrans<Vb_float) max_Vtrans=Vb_float; /*  Store highest vac for set_Psm(). */

			Vb_float *= Kb_wf * Kout_B;					/* correct Vpgm for WF in use. */

			if(pgm_edit.coupling==1)
			{
				Vb_int=Vb_float * V_DAC;		/* scale volts, convert to int. */
			}
			else
			{
				if (XFMR_ratio!=0)
				{
					Vb_int=Vb_float * V_DAC / XFMR_ratio;		/* scale volts, convert to int. */
             	}
              	else
           		{
//				   			raise_exception(-16);
      			}
			}
			step_block[step].Vb=Vb_int;	/* store int in step ram. */
			Vc_float=(Trans_seg[0].Vc+(dVc*s) );/* take */

			if (max_Vtrans<Vc_float) max_Vtrans=Vc_float; /*  Store highest vac for set_Psm(). */
			
			Vc_float *= Kc_wf * Kout_C;					/* correct Vpgm for WF in use. */
			
			if(pgm_edit.coupling==1)
			{
				Vc_int=Vc_float * V_DAC;		/* scale volts, convert to int. */
			}
			else
			{
				if (XFMR_ratio!=0)
				{
					Vc_int=Vc_float * V_DAC / XFMR_ratio;		/* scale volts, convert to int. */
               	}
            	else
        		{
//							raise_exception(-16);
             	}
			}
			step_block[step].Vc=Vc_int;	/* store int in step ram. */
			step++;							/* inc step_block pointer. */
		}									/* do all steps */
	}										/* do all segs */
	calc_pgm_values();	/* calc Steady state values */

	trans_changed_flag=0;	/* allow trans pgm to be executed. */
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void init_waveforms()
{
//int j;
int i,temp_wf_no;
unsigned int uint;
float sine, ftemp;
float w=two_PI/MAX_WF_STEPS;
unsigned wf_array_DIV_wf_steps=WF_ARRAY/MAX_WF_STEPS;
unsigned max_steps_TIMES_3=3*MAX_WF_STEPS;
unsigned max_steps_TIMES_2=MAX_WF_STEPS<<1;

	if(prev_wf_steps==WF_STEPS)
		return;								/* if wf_steps change re init all waveforms */

	if(prev_wf_steps==0)
	{									/* if battery failed init all waveforms */
		for(i=0; i<MAX_WF_STEPS; i++)
		{								/* generate sinewave table */
			ftemp = w * i;
			sine = sin(ftemp);
//			sine *= 32767;
			sine *= -32767;		//invert osc output waveform (for sync w/gen) because sense does not.
			uint = sine;
			uint += 32768;
//			j = (i+171)%MAX_WF_STEPS;	//offset waveform for sync w/genset (shifted sync)
//			user_wf[j] = uint;             				/* generate sinewave table */
			user_wf[i] = uint;             				/* generate sinewave table */
		}
		uint=0;
		for(i=1; i<wf_array_DIV_wf_steps; i++)
		{								/* copy all sinewaves */
//			while(uint<(i*MAX_WF_STEPS))
			while(uint<(i<<10))
			{							/* copy a sinewave */
				user_wf[uint+MAX_WF_STEPS]=user_wf[uint];	/* copy a step */
				uint++;
			}
		}
	}
	prev_wf_steps=WF_STEPS;

	wf_scale=100.0;
	temp_wf_no=wf_no;			//save wf_no
	for (wf_no=1;wf_no<wf_array_DIV_wf_steps+1;wf_no++)		//scale all waveforms.
	{					//added for v2.00, 4-11-96, dave.
		WF_calc();
	}
	wf_no=temp_wf_no;			//restore wf_no

	init_osc_ram();								/* copy waveforms from user ram to osc ram. */
}
/*---------------------------------------------------------------------------*/
void init_osc_ram()	/* copy waveforms from user ram to osc ram. */
{
int i;
unsigned int user_step, osc_step;

	user_step=0;
	osc_step=0;
	for(i=1; i<=(WF_ARRAY/MAX_WF_STEPS); i++)
	{						/* copy all waveforms */
		while(osc_step<(i*WF_STEPS))
		{					/* copy a waveform */
			if(user_wf[user_step]==0)
				osc_wf[osc_step]=65535;
			else
				osc_wf[osc_step]=user_wf[user_step];	/* copy a step */
			user_step+=(MAX_WF_STEPS/WF_STEPS);
			osc_step++;
		}
	}
}
/*---------------------------------------------------------------------------*/
void freq_calc(float freq)	/* output frequency calculation */
{							/* 700microseconds */

unsigned int freq_cd;	/* freq_cd is output frequency counts difference */
unsigned long freq_cs;	/* freq_cs is output frequency counts slow */
float freq_c, pwm_ca;	/* freq_c is output frequency counts, pwm_ca is pwm counts average */

//	system_frequency=freq;

	freq_c = CLK / freq;
	pwm_ca = freq_c / WF_STEPS;
	pwm_c = pwm_ca;
	freq_cs = pwm_c;
	freq_cs *= WF_STEPS;
	freq_cd = (freq_c - freq_cs) / 2;
	if(freq_cd == 0)
	{									/* do not change pwm freq */
		cc1_cs = 32768;
	}
	else
	{
		cc1_cs = (65536 - freq_cd);		/* pwm frequency change counts slow */
	}
}
/*---------------------------------------------------------------------------*/

void store_Pgm()	/* store pgm_edit */
{
	if (pgm_edit_no==active_pgm_no)			/* if pgm_edit same as pgm being executed, */
	{
        user_pgm[0] = user_pgm[active_pgm_no];	/* copy active pgm down to pgm0. */
		active_pgm_no = 0;			/* keep original pgm values as active. */
	}						/* All this was done because */
							/* The program in use was edited, pgm 0 is now in use. */
	user_pgm[pgm_edit_no] = pgm_edit;		/* put edited pgm back into user ram. */
}
/*---------------------------------------------------------------------------*/
void exec_osc_pgm()	/* execute pgm_edit */
{
#ifdef TRACE_ON
	int trace_code=8;
	enqueue(trace_queue, &trace_code); 		//circular trace queue.
#endif //TRACE_ON

	set_Vr_ILMr();					// added for check_pgm() 6/97
	user_pgm[0]=pgm_edit;	/* load up pgm 0 */

	if (pgm_edit_no==0)	store_Pgm();	/* pgm_edit may be now stored since starting values have been read. */
			/* This is only needed for when pgm 0 gets changed & executed. */

	active_pgm_no=pgm_edit_no;	/* indicate requested pgm is now active. */

	if (active_pgm_no !=0)
		orig_pgm_no=active_pgm_no;	/* store # of real user pgm thats executed. */

	Trans_seg[0].Va=user_pgm[0].Va;	/* copy current Voltages */
	Trans_seg[0].Vb=user_pgm[0].Vb;	/* to starting V. */
	Trans_seg[0].Vc=user_pgm[0].Vc;

	Trans_seg[0].WFa = user_pgm[0].WFa; /* Changed to use current WF */
	Trans_seg[0].WFb = user_pgm[0].WFb; /* since ph angles and Ilim */
	Trans_seg[0].WFc = user_pgm[0].WFc; /* don't change till end of ramp. */
	Trans_seg[0].F=user_pgm[0].Freq;	/* End of SOFT Start setup. */

	calc_pgm_values();

	exec_flag=1;			/* flag says user pgm executed */

	load_osc_regs();
}
/*---------------------------------------------------------------------------*/

unsigned int wf_address(unsigned int wf)	/* calc waveform address */
{
	if (wf > ((WF_ARRAY/WF_STEPS)-1))
	{
		return (int)&osc_wf;
	}
	else
	{
		wf *= (WF_STEPS*2);	/* word to byte conversion */
		return (wf + ((int)&osc_wf));	/* offset to waveform number */
	}
}
/*---------------------------------------------------------------------------*/
void calc_pgm_values()	/* get ready to load hardware */
{			   
	freq_calc(pgm_edit.Freq);	/* sets pwm_c, cc1_cs */

	Form = 		pgm_edit.form;
	Coupling = 	pgm_edit.coupling;

	PHb = 	(pgm_edit.PHb * ((float)WF_STEPS/360)) + (65535-WF_STEPS) + 0.499;	/* PHb * scale plus */
	PHc = 	(pgm_edit.PHc * ((float)WF_STEPS/360)) + (65535-WF_STEPS) + 0.499;	/* Timer0 reload plus round off*/

	if (PHb < (65536-WF_STEPS))
	{
		PHb = 0xffff;				/* trap 0 deg, set to 360 deg */
	}

	if (PHc < (65536-WF_STEPS))
	{
		PHc = 0xffff;				/* trap 0 deg, set to 360 deg */
	}

	ILMa = (I_DAC * (pgm_edit.ILMa/ILMr)) / Kmeter_I_A;

	Va_pgm=	pgm_edit.Va;			/* get pgm'ed Va */

//	if (Form==2)
//		Va_pgm = Va_pgm/2;	/* if split phase, split output V */

	WFa=	pgm_edit.WFa-1;	/* get WF# */

	WFPa=	wf_address(WFa);	/* get WF address */

	Vb_pgm=	pgm_edit.Vb;	/* get pgm'ed Vb */

	WFb=	pgm_edit.WFb-1;	/* get WF# */

	WFPb=	wf_address(WFb);	/* get WF address */

//	if (Form==2)
//	{			/* if split phase mode */
		Vb_pgm=Va_pgm;
		WFPb = WFPa;
//	}

	Vc_pgm=	pgm_edit.Vc;	/* get pgm'ed Vc */
	WFc=	pgm_edit.WFc-1;	/* get WF# */
	WFPc=	wf_address(WFc);	/* get WF address */

	Va_out = Va_pgm;	/* set dynamic voltage */
	Vb_out = Vb_pgm;
	Vc_out = Vc_pgm;

    Ka_wf = WFk_wf[WFa];   //restore active waveform Kfactor, bug fix 9/30/96, dave
    Kb_wf = WFk_wf[WFb];   //restore active waveform Kfactor, bug fix 9/30/96, dave
    Kc_wf = WFk_wf[WFc];   //restore active waveform Kfactor, bug fix 9/30/96, dave

	Va_out *= Ka_wf;	/* correct Vout for WF in use */
	Vb_out *= Kb_wf;
	Vc_out *= Kc_wf;

	Va_out *= Kout_A;	/* correct Vout using calibration Kfactor. */
	Vb_out *= Kout_B;
	Vc_out *= Kout_C;

	scale_volts();
}
/*---------------------------------------------------------------------------*/
void scale_volts()		// scale Vx_out to load DACs.
{
	if(Coupling==DIRECT) 
	{
		Va = (Va_out * V_DAC);
		Vb = (Vb_out * V_DAC);
		Vc = (Vc_out * V_DAC);
	}
	else
	{
       	if (XFMR_ratio!=0.0)
   		{
			Va = (Va_out * V_DAC) / XFMR_ratio;
			Vb = (Vb_out * V_DAC) / XFMR_ratio;
			Vc = (Vc_out * V_DAC) / XFMR_ratio;
   		}
		else
       	{
//			raise_exception(-16);
       	}
	}
}
/*---------------------------------------------------------------------------*/
#ifdef EXEC_PHASE
void exec_phase(int phase)		// delay execution until this "phase" angle in degrees
{
unsigned int phase_counts;	// phase converted to counts as in T0

	phase_counts = (phase * ((float)WF_STEPS/360)) + (65535-WF_STEPS) + 0.499;	/* Timer0 reload plus round off*/
	if (phase_counts < (65536-WF_STEPS)) phase_counts = 0xffff;	/* trap 0 deg, set to 360 deg */

	CC6 = phase_counts;		// trigger phase angle
	CC6IC = 0;			// clear IR
	while(!_getbit(CC6IC, 7));	// wait for IR set, wait for phase angle
}
#endif // EXEC_PHASE
/*---------------------------------------------------------------------------*/
void load_osc_regs()	/* execute new parameters now */
{
#ifdef EXEC_PHASE
	exec_phase(user_pgm[active_pgm_no].exec_phase);	// delay execution until this phase angle
#endif // EXEC_PHASE
 
	pwm_f = pwm_c-1;	/* load output frequency */
	pwm_s = pwm_c;
	CC0 = cc1_cs-(WF_STEPS/2);
	CC1 = cc1_cs;

	pha_volts_dac = Va;	/* change voltages now */
	phb_volts_dac = Vb;
	phc_volts_dac = Vc;

	wfp_a = WFPa;	/* change waveforms at next cycle reset */
	wfp_b = WFPb;
	wfp_c = WFPc;

//	CC3 = 65450;	   //120 degrees
//	CC4 = 65493;	   //241 degrees
	CC3 = PHb;		/* change phase angle at next cycle reset */
	CC4 = PHc;

//	pha_ilim_dac = ILMa;	/* change current limit now */
//	phb_ilim_dac = ILMa;
//	phc_ilim_dac = ILMa;

	_nop();			//allow a few us's for DAC's to settle.
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
	_nop();
}
/*---------------------------------------------------------------------------*/
void WF_calc()	               /* calc K factors for wf_no. */
{

int n;
unsigned int offset;

float d;
float sum;
float WF_avg;	                                        /* holds avg of active waveform... */
float WF_rms;	                                        /* holds RMS of active waveform... */

	if (wf_no==0) return;	                        /* skip calcs since all WFs were loaded */
				                        /* 0 not a valid wf#. */
	if (wf_no==1)
	{	WFk_wf[0]=1;                            /* force SINE wave K factor=1 */
		return;
	}

	wf_no--;			                /* dec WF# for proper array access. */

	sum=0;

	for (n=0; n < MAX_WF_STEPS; n++)        	/* calc DC avg of WF for DC offset removal */
	{
		d = user_wf[(wf_no*MAX_WF_STEPS)+n];	/* get data, cvt to float. */
		sum += d;			        /* sum signed values. */
	}

	offset = sum/MAX_WF_STEPS;			/* div by MAX_WF_STEPS to get avg. */
						        /* Sum s/be WF_OFFSET if no offset. */
	WF_avg=0;				        /* clr summing reg. */
	sum=0;
	for (n=0; n < MAX_WF_STEPS; n++)		/* calc avg value of WF for avg ALC. */
	{
		d = user_wf[(wf_no*MAX_WF_STEPS)+n];	/* get data, cvt to float. */
		d -= offset;			        /* strip offset. */
		d = fabs(d);				/* ignore sign. */
		sum += (d*d);			        /* sum & square all bytes of WF for rms. */
	}

	WF_rms = sqrt(sum/MAX_WF_STEPS);                /* make rms */
	if (WF_rms==0)
	{
		WF_rms = SINE_RMS;	                /* trap wf_rms of 0. */
	}

	WFk_wf[wf_no] = SINE_RMS/WF_rms;	        /* make Kwf for fixing Vpgm. */

	wf_no++;				        /* restore user WF# */
}
/*---------------------------------------------------------------------------*/

void set_Vr_ILMr()			// set Vrange and ILMrange.
{
	Vr = VLIMIT_Direct;					// DIRECT coupled Vr.

	if (GP_OUTPUT_COUPLING==TRANSFORMER)	
	{
		Vr *= GP_XFMR_RATIO;	//TRANSFORMER coupled Vr. 
	}

//	Vr = Vr * 2;						// if split phase, double V range.

	if (Vr > 600)
	{
		Vr=600;				// limit max output Vrange to 600 VACrms.
	}

	ILMr = amps_volts*10;				// determine ILM range.

#ifndef SPLIT_PHASE
	ILMr /= 3;							// div by 3 if 3-phase mode.
#endif //~SPLIT_PHASE
}
/*---------------------------------------------------------------------------*/
void init_outputs()		// initialize all outputs to nominal.
{
	initialize_sid();		//initialize SYSTEM INTERFACE DRIVER.

	pha_volts_dac =	0x0000;			// all dacs to nominal.
  	phb_volts_dac =	0x0000;     		// 12-bit voltage DAC
	phc_volts_dac =	0x0000;

	pha_wf_dac =	0x8000;
	phb_wf_dac =	0x8000;		// 12-bit waveform DAC
	phc_wf_dac =	0x8000;
}
/*---------------------------------------------------------------------------*/
void CSC()		// Continuously self calibrate, but only after metering.
{
float Vpgm, Vdif, V_k;
int inRange; // Used as err flag after v2.14, a Vdif of 0 would have caused Vpgm to be output.

#ifndef MM3
//	if (gp_state.invstat != ONLINE) return;	// return if output not online.
	if (gp_state.inpstat1 != ONLINE) return;	// return if output not online.
	if (exec_flag!=1) return;	// return if no user pgm executed.
	if (trans_mode) return;		// Cannot correct output while running trans.
#endif //MM3

	Vpgm = user_pgm[active_pgm_no].Va; // Use stored pgm as reference.
	if (Vpgm == 0)
	{
		Va_out = 0;
		goto csc_b;
	} 						// trap 0 for Vpgm.

	Vdif = Vpgm - Van_csc; 	// Calc K factor using pgmed vs. metered L-N voltage.

	V_k = (Vdif / Vpgm);

	inRange=1;
	if (V_k < V_K_LIMIT_LO )
		inRange=0; /* Disable V comp if sense open */
	if (V_k > V_K_LIMIT_HI )
		inRange=0; /* or out of range. */


	Vdif = Vpgm * V_k * V_K_GAIN;			/* remake new Vdif. */


	if (inRange)
	{						/* If k not out of range, */
		Va_out=Va_out + (Vdif * Ka_wf);	/* make new V_out, */
	}
	else
	{
			Va_out=Vpgm * Ka_wf * Kout_A;	/* else restore orig pgm value */
	}

/* ---------------------------------- */
csc_b:
	       
//	if (GP_OUTPUT_FORM==ONE_PHASE)	//v1.58 compensate for line-line gen volts.
//	{
//		goto load;
//	}

	Vpgm = user_pgm[active_pgm_no].Vb;
	if (Vpgm == 0)
	{
		Vb_out = 0;
		goto csc_c;
	}

	Vdif = Vpgm - Vbn_csc; /* Calc K factor using pgmed vs. metered L-N voltage. */

	V_k = (Vdif / Vpgm);

	inRange=1;
	if (V_k < V_K_LIMIT_LO )
		inRange=0; /* Disable V comp if sense open */
	if (V_k > V_K_LIMIT_HI )
		inRange=0; /* or out of range. */


	Vdif = Vpgm * V_k * V_K_GAIN;	/* remake new Vdif. */

	if (inRange)
	{							/* If k not out of range, */
		Vb_out=Vb_out + (Vdif * Kb_wf); /* Make new V_out */
	}
	else
	{
		Vb_out=Vpgm * Kb_wf * Kout_B;	/* else restore orig pgm value */
	}
/* ---------------------------------- */
csc_c:
	       
	if (GP_OUTPUT_FORM==TWO_PHASE)	//v1.58 compensate for line-line gen volts.
	{
		goto load;
	}
	       
	if (GP_OUTPUT_FORM==ONE_PHASE)	//v1.58 compensate for line-line gen volts.
	{
		goto load;
	}

	Vpgm = user_pgm[active_pgm_no].Vc;
	if (Vpgm == 0)
	{
		Vc_out = 0;
		goto load;
	}

	Vdif = Vpgm - Vcn_csc; 						/* Calc K factor using pgmed vs. metered L-N voltage. */

	V_k = (Vdif / Vpgm);

	inRange=1;
	if (V_k < V_K_LIMIT_LO )
		inRange=0; 								/* Disable V comp if sense open */
	if (V_k > V_K_LIMIT_HI )
		inRange=0; 								/* or out of range. */

	Vdif = Vpgm * V_k * V_K_GAIN;				/* remake new Vdif. */

	if (inRange)
	{											/* If k not out of range, */
		Vc_out=Vc_out + (Vdif * Kc_wf); 		/* Make new V_out */
	}
	else
	{
		Vc_out=Vpgm * Kc_wf * Kout_C;			/* else restore orig pgm value */
	}

load:
	scale_volts();
	load_osc_regs();
}
/*---------------------------------------------------------------------------*/
void initialize_waveform_ram()
{
  int i;
  unsigned waveform_count=(WF_ARRAY/MAX_WF_STEPS);

	for(i=0; i<waveform_count; i++)
	{
		WFk_wf[i]=1; 			// holds SINErms/WFrms ratio of all waveforms.
	}

	prev_wf_steps = 0;				/* force all waveforms to be initialized */
	wf_steps = WF_STEPS;			/* default number of steps in one waveform */

	Ka_wf = 1;						/* holds SINErms/WFrms ratio of active waveform */
	Kb_wf = 1;
	Kc_wf = 1;

	wf_no=2;				/* default for WF edit */
}
/*---------------------------------------------------------------------------*/
void initialize_program_ram()
{
  int i=0;

	Kout_A = 1;				// Vout calibration Kfactor.
	Kout_B = 1;
	Kout_C = 1;

	F_span = (float) F_SPAN;	 	/* 600 hz. for default 256 step waveform, DO NOT set > 1200 with 128 step waveform*/
	F_min = F_MIN;					/* default 45 hz. DO NOT set < 15 */
	F_max = F_MAX;
	V_min = 0;						/* EXECuted Prog. voltage Limits */
	V_max = 600;

#ifdef EXEC_PHASE
	def_pgm.exec_phase=0;
#endif //EXEC_PHASE

	Form=3;

//	def_pgm.form=GP_OUTPUT_FORM;		/* Output form: 1=single, 2=split, 3=3phase */
	def_pgm.form=3;		/* Output form: 1=single, 2=split, 3=3phase */

//	if (Form<GP_OUTPUT_FORM) gp_diagnostic="B/C OSCILLATOR BOARD NOT CONNECTED PROPERLY."

	def_pgm.coupling=GP_OUTPUT_COUPLING;	//1=direct, 2=XFMR.

	def_pgm.XFMR_ratio_req=GP_XFMR_RATIO;
	def_pgm.Freq=GP_OUTPUT_FREQUENCY;
	def_pgm.WFa=1;
	def_pgm.WFb=1;
	def_pgm.WFc=1;
//#ifdef SPLIT_PHASE
	if (GP_OUTPUT_FORM==2 || GP_OUTPUT_FORM==1)
	{
		if (gp_state.vout_range==HIGH_RANGE)
		{
			def_pgm.Va=GP_OUTPUT_VOLTAGE * 0.5;
			def_pgm.Vb=GP_OUTPUT_VOLTAGE * 0.5;
			def_pgm.Vc=0;
		}
		else
		{
			def_pgm.Va=GP_OUTPUT_VOLTAGE;
			def_pgm.Vb=GP_OUTPUT_VOLTAGE;
			def_pgm.Vc=0;
		}
		def_pgm.PHb=180;
		def_pgm.PHc=180;
	}
	else
	{
//#else
		if (gp_state.vout_range==HIGH_RANGE)
		{
			def_pgm.Va=GP_OUTPUT_VOLTAGE * 0.5;
			def_pgm.Vb=GP_OUTPUT_VOLTAGE * 0.5;
			def_pgm.Vc=GP_OUTPUT_VOLTAGE * 0.5;
		}
		else
		{
			def_pgm.Va=GP_OUTPUT_VOLTAGE;
			def_pgm.Vb=GP_OUTPUT_VOLTAGE;
			def_pgm.Vc=GP_OUTPUT_VOLTAGE;
		}
		def_pgm.PHb=120;
		def_pgm.PHc=240;
	}
//#endif //SPLIT_PHASE

	def_pgm.no_Tevents=0;
	def_pgm.no_segs=0;

	pgm_edit=def_pgm;			/* setup pgm_edit for */

	set_Vr_ILMr();				/* setting ILMr. */

	def_pgm.ILMa=ILMr;

//	external_frequency=def_pgm.Freq;

	for(i=2; i<MAX_PGM+1; i++) user_pgm[i].Freq=0;			/* delete user pgms */

	pgm_edit_no=0;
	active_pgm_no=0;		/* default to manual mode */
	orig_pgm_no=1;			/* if no pgm was edited, edit pgm1. (always non-zero). */

	pgm_edit=def_pgm;	/* load defaults for new pgm */
	user_pgm[0]=pgm_edit;	/* set up manual mode values. */
	user_pgm[1]=pgm_edit;	/* set up pgm 1 values. */

#ifdef CALIBRATION_RANGE_CHECK
	MIN_KFACTOR = MIN_KFACTOR_DEFAULT;
	MAX_KFACTOR = MAX_KFACTOR_DEFAULT;
#endif // CALIBRATION_RANGE_CHECK

#ifdef CALIBRATION_STATUS_CHECK
	calibration_status_word=(unsigned long)0;
#endif // CALIBRATION_STATUS_CHECK
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
