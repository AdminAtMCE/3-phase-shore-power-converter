/*=========================================================================*/
/*=========================================================================*/
//	file:	lcd.c	   	LCD Module 
/*=========================================================================*/
/*=========================================================================*/
#include "ac.h"
/*=========================================================================*/
/*=========================PUBLIC==========================================*/
//extern void init_lcd(void);
//extern void lcd_display(char *, int, int);
//extern float lcd_display_value(float, int, int, char *);
//extern void clear_screen(void);
//extern void put_cursor(int,int);

/*=========================================================================*/
/*=========================PRIVATE=========================================*/
void lcd_shift_display_left(void);
void lcd_shift_display_right(void);
void return_home(void);

void init_lcd(void);
void lcd_display(char *, int, int);
float lcd_display_value(float, int, int, char *);
void clear_screen(void);
void put_cursor(int,int);
void c_on(void);
void c_off(void);
void display(void); 				// display char string 'str'
void echo(void); 					// display 1 char and track current position of cursor
void locate(int x, int y); 			// place cursor at x, y
void lcd_wdata(unsigned lcd_data); 	// write data to either lcd
void lcd_ubusy(void); 				// wait for upper lcd busy flag to go low
void lcd_lbusy(void); 				// wait for lower lcd busy flag to go low
void lcd_ucont(unsigned lcd_data); 	// write control word to upper lcd
void lcd_lcont(unsigned lcd_data); 	// write control word to lower lcd
void lcd_udata(unsigned lcd_data); 	// write data to upper lcd
void lcd_ldata(unsigned lcd_data); 	// write data to lower lcd

/*=========================================================================*/
/*=========================================================================*/
//#pragma clear	// fill iram with cleared data so noclear data is put in batt ram.
extern char *str;	//=strng;		//point to RESERVEd 160 char string + null.
extern iram char cvtbufr[161];		// buffer for sprintf conversions (== 1 line).
extern char character;				// string to be output.
extern int x, y;					// X and Y axis of cursor.
extern int start_x, start_y;		// starting axis of cursor.

extern unsigned MODEL_LC;			//S4-8
/*=========================================================================*/
/*=========================================================================*/
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//@LCD		LCD		LCD		LCD		LCD		LCD		LCD		LCD		LCD
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
/*-------------------------------------------------------------------------- */
void lcd_display(char *lcd_string, int row, int column)
{
//	if (MODEL_LC) return;	//Model 'L' return immediately.

	str=lcd_string;
	locate(x=column, y=row);				// re-locate cursor instead of clearing screen.
	display();					// show text.
}
/*-------------------------------------------------------------------------- */
float lcd_display_value(float lcd_value, int row, int column, char *format)
{
//	if (MODEL_LC) return (lcd_value);	//Model 'L' return immediately.

	sprintf(cvtbufr,format,lcd_value);	// put text string in buffer
	lcd_display(cvtbufr,row,column);
	return lcd_value;
}
/*---------------------------------------------------------------------------*/
void put_cursor(int row,int column)
{
//	if (MODEL_LC) return;	//Model 'L' return immediately.

	locate(x=column,y=row);
	c_on();
}
/*---------------------------------------------------------------------------*/
void lcd_shift_display_left()
{
		lcd_lcont(0x18);
}
/*---------------------------------------------------------------------------*/
void lcd_shift_display_right()
{
		lcd_lcont(0x1C);
}
/*---------------------------------------------------------------------------*/
void return_home()
{
		lcd_lcont(0x2);
}
/*---------------------------------------------------------------------------*/
void init_lcd()
{
unsigned char block[]={0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F};	/* block char */
unsigned char plusMinus[]={0x4,0x4,0x1F,0x4,0x4,0,0x1F,0};			/* plus_minus char */
unsigned char phi[]={4,0xE,0x15,0x15,0x15,0xE,4,0};					/* phi char */
unsigned char lightenBolt[]={0x1,0x2,0xC,0x1F,0x6,0x8,0x10,0};		/* lighten-bolt char */
unsigned char lazy_A[]={0x1,0x3,0x5,0x9,0xF,0x19,0x19,0};			/* lazy-A char */
unsigned char lazy_E[]={0x7,0x8,0x8,0xF,0x10,0x10,0xF,0};			/* lazy-E char */
unsigned char wedge1[]={0x1F,0x1F,0x1F,0xF,0x7,0x3,0x1,0x1};		/* wedge1 char */
unsigned char wedge2[]={0x1F,0x1F,0x1F,0x1E,0x1C,0x18,0x10,0x10};	/* wedge2 char */
unsigned int n;
char nums[]={2,3,0};	/* Char code 2,3 display "" */

	_putbit(1,P3,2);	//ROW #1 high for read.

//	if (MODEL_LC) return;	//Model 'L' return immediately.

	lcd_ucont(0x38);	/* 8 bit data, 2 lines, 5x7 char */
	lcd_lcont(0x38);
	lcd_ucont(0x38);
	lcd_lcont(0x38);
	lcd_ucont(0x06);	/* entry mode set */
	lcd_lcont(0x06);
	lcd_ucont(0x0e);	/* display control */
	lcd_lcont(0x0e);
	lcd_ucont(0x0c);	/* cursor off */
	lcd_lcont(0x0c);

	lcd_ucont(0x48);	/* Set upper CG RAM load mode CG RAM Char code 01 */
	lcd_lcont(0x48);	/* Set lower CG RAM load mode CG RAM Char code 01 */

	for (n=0; n<sizeof(phi); n++)	
	{				/* Load character code address 01 */
		lcd_udata(phi[n]);	/* Load upper CG RAM with m */
		lcd_ldata(phi[n]);	/* Load lower CG RAM with m */
	}

	lcd_ucont(0x50);	/* Set upper CG RAM load mode CG RAM Char code 02 */
	lcd_lcont(0x50);	/* Set lower CG RAM load mode CG RAM Char code 02 */

	for (n=0; n<sizeof(lightenBolt); n++)
	{					/* Load character code address 02 */
		lcd_udata(lightenBolt[n]);	/* Load upper CG RAM with lighten-bolt*/
		lcd_ldata(lightenBolt[n]);	/* Load lower CG RAM with lighten-bolt*/
	}

	lcd_ucont(0x58);	/* Set upper CG RAM load mode CG RAM Char code 03 */
	lcd_lcont(0x58);	/* Set lower CG RAM load mode CG RAM Char code 03 */

	for (n=0; n<sizeof(lazy_A); n++)
	{					/* Load character code address 03 */
		lcd_udata(lazy_A[n]);	/* Load upper CG RAM with lazy-A */
		lcd_ldata(lazy_A[n]);	/* Load lower CG RAM with lazy-A */
	}

	lcd_ucont(0x60);	/* Set upper CG RAM load mode CG RAM Char code 04 */
	lcd_lcont(0x60);	/* Set lower CG RAM load mode CG RAM Char code 04 */

	for (n=0; n<sizeof(lazy_E); n++)
	{					/* Load character code address 04 */
		lcd_udata(lazy_E[n]);	/* Load upper CG RAM with lazy-E */
		lcd_ldata(lazy_E[n]);	/* Load lower CG RAM with lazy-E */
	}

	lcd_ucont(0x68);	/* Set upper CG RAM load mode CG RAM Char code 05 */
	lcd_lcont(0x68);	/* Set lower CG RAM load mode CG RAM Char code 05 */

	for (n=0; n<sizeof(block); n++)
	{					/* Load character code address 05 */
		lcd_udata(block[n]);	/* Load upper CG RAM with block */
		lcd_ldata(block[n]);	/* Load lower CG RAM with block */
	}

	lcd_ucont(0x70);	/* Set upper CG RAM load mode CG RAM Char code 06 */
	lcd_lcont(0x70);	/* Set lower CG RAM load mode CG RAM Char code 06 */

	for (n=0; n<sizeof(wedge1); n++)
	{					/* Load character code address 06 */
		lcd_udata(wedge1[n]);	/* Load upper CG RAM with wedge1 */
		lcd_ldata(wedge1[n]);	/* Load lower CG RAM with wedge1 */
	}

	lcd_ucont(0x78);	/* Set upper CG RAM load mode CG RAM Char code 07 */
	lcd_lcont(0x78);	/* Set lower CG RAM load mode CG RAM Char code 07 */

	for (n=0; n<sizeof(wedge2); n++)
	{					/* Load character code address 07 */
		lcd_udata(wedge2[n]);	/* Load upper CG RAM with wedge2 */
		lcd_ldata(wedge2[n]);	/* Load lower CG RAM with wedge2 */
	}

	lcd_ucont(0x40);	/* Set upper CG RAM load mode CG RAM Char code 00 */
	lcd_lcont(0x40);	/* Set lower CG RAM load mode CG RAM Char code 00 */

	for (n=0; n<sizeof(plusMinus); n++)
	{					/* Load character code address 00 */
		lcd_udata(plusMinus[n]);	/* Load upper CG RAM with plusMinus */
		lcd_ldata(plusMinus[n]);	/* Load lower CG RAM with plusMinus */
	}
}
/*---------------------------------------------------------------------------*/
void clear_screen()
{
//	if (MODEL_LC) return;	//Model 'L' return immediately.

	lcd_ucont(1);			/* clear display */
	lcd_lcont(1);
	locate(x=1,y=1);		/* home cursor */
}
/*---------------------------------------------------------------------------*/
void c_on()
{
//	if (MODEL_LC) return;	//Model 'L' return immediately.

	if (y<3)	/* turn upper BLOCK cursor ON if Line 1 or 2 active. */
	{
		lcd_ucont(0x0f);
	}
	else	/* turn lower BLOCK cursor ON if Line 3 or 4 active. */
	{
		lcd_lcont(0x0f);
	}
}
/*---------------------------------------------------------------------------*/
void c_off()
{
//	if (MODEL_LC) return;	//Model 'L' return immediately.

		lcd_ucont(0x0c);	/* turn both BLOCK cursors OFF */
		lcd_lcont(0x0c);
}
/*---------------------------------------------------------------------------*/
void display()			/* display char string 'str' */
{
char *index;
index = str;					/* index is temp pointer */

//	if (MODEL_LC) return;	//Model 'L' return immediately.

	c_off();

	do
	{
		character=*index;		/* put char in 'character' */
		echo();				/* echo 1 char to display device */
//                if (character!='\n')	// don't count unprintable characters!! bug fix MARCH 97, dave.
		index++;		/* bump indexer */
		if (index==str+160)
			return;
	} while (*index != '\0');		/* quit when null found */
}
/*---------------------------------------------------------------------------*/
void echo()	/* display 1 char and track current position of cursor */
{
//	if (MODEL_LC) return;	//Model 'L' return immediately.

	if (character==*"\n")	// if C/R, do CR/LF.
	{
		y++; x=1;
		locate(x,y);
	}					
	else
	{
		if (x>40)
		{
			x=1;
			y++;
			locate(x,y);
		}				/* do C/R L/F if line length (x) exceeded */
		if (y>4)
		{
			y=1;
			x=1;
			locate(x,y);
		}					/* see if max # of lines exceeded from incrementing, */
							/* home cursor if so */
		x++;				/* inc tracked pos, echo subtracts 1 */
		if (y<3)
			lcd_udata(character);	/* line 1 or 2 */
		else
			lcd_ldata(character);	/* line 3 or 4 */
	}
}
/*---------------------------------------------------------------------------*/
void locate(int x, int y)	/* place cursor at x, y */
{
//	if (MODEL_LC) return;	//Model 'L' return immediately.

	start_x=x;	/* TAKE NOTE: store start of working field */
	start_y=y;

	if (y==1) lcd_ucont(x-1+0x80);	/* show on line 1 */
	if (y==2) lcd_ucont(x-1+0xc0);	/* show on line 2 */
	if (y==3) lcd_lcont(x-1+0x80);	/* show on line 3 */
	if (y==4) lcd_lcont(x-1+0xc0);	/* show on line 4 */
}
/*---------------------------------------------------------------------------*/
void lcd_wdata(unsigned lcd_data)	/* write data to either lcd */
{
//	if (MODEL_LC) return;	//Model 'L' return immediately.

	_putbit(lcd_data & 0x1,  P2, 0);
	_nop();
	_putbit(lcd_data & 0x2,  P2, 1);
	_nop();
	_putbit(lcd_data & 0x4,  P2, 2);
	_nop();
	_putbit(lcd_data & 0x8,  P2, 3);
	_nop();
	_putbit(lcd_data & 0x10, P2, 4);
	_nop();
	_putbit(lcd_data & 0x20, P2, 5);
	_nop();
	_putbit(lcd_data & 0x40, P2, 6);
	_nop();
	_putbit(lcd_data & 0x80, P2, 7);
}
/*---------------------------------------------------------------------------*/
void lcd_ubusy()	/* wait for upper lcd busy flag to go low */
{
unsigned int i;

//	if (MODEL_LC) return;	//Model 'L' return immediately.

	_putbit(0, DP2, 7);	/* read busy flag */

	P4 = 0x40;	/* R/W hi */

	for(i=0; i<900; i++) {	/* wait 2ms max */
		P4 = 0xc0;	/* E1 and R/W hi */
		_nop();	/* wait 400 ns */
		_nop();
		_nop();
		if(_getbit(P2, 7) == 0) i = 10000;	/* upper lcd not busy */
		P4 = 0x40;	/* R/W hi */
	}

	_putbit(1, DP2, 7);
}
/*---------------------------------------------------------------------------*/
void lcd_lbusy()	/* wait for lower lcd busy flag to go low */
{
unsigned int i;

//	if (MODEL_LC) return;	//Model 'L' return immediately.

	_putbit(0, DP2, 7);

	P4 = 0x40;	/* R/W hi */

	for(i=0; i<900; i++) {	/* wait 2ms max */
		P4 = 0x50;	/* E2 hi */
		_nop();	/* wait 400 ns */
		_nop();
		_nop();
		if(_getbit(P2, 7) == 0) i = 10000;	/* lower lcd not busy */
		P4 = 0x40;	/* R/W hi */
	}

	_putbit(1, DP2, 7);
}
/*---------------------------------------------------------------------------*/
void lcd_ucont(unsigned lcd_data)	/* write control word to upper lcd */
{
//	if (MODEL_LC) return;	//Model 'L' return immediately.

	lcd_ubusy();

	P4 = 0;
	_nop();
	P4 = 0x80;	/* E1 hi */

	lcd_wdata(lcd_data);

	P4 = 0;
}
/*---------------------------------------------------------------------------*/
void lcd_lcont(unsigned lcd_data)	/* write control word to lower lcd */
{
//	if (MODEL_LC) return;	//Model 'L' return immediately.

	lcd_lbusy();

	P4 = 0;
	_nop();
	P4 = 0x10;	/* E2 hi */

	lcd_wdata(lcd_data);

	P4 = 0;
}
/*---------------------------------------------------------------------------*/
void lcd_udata(unsigned lcd_data)	/* write data to upper lcd */
{
//	if (MODEL_LC) return;	//Model 'L' return immediately.

	lcd_ubusy();

	P4 = 0x20;	/* R/S hi */
	_nop();
	P4 = 0xa0;	/* E1 and R/S hi */

	lcd_wdata(lcd_data);

	P4 = 0x20;	/* R/S hi */
}
/*---------------------------------------------------------------------------*/
void lcd_ldata(unsigned lcd_data)	/* write data to lower lcd */
{
//	if (MODEL_LC) return;	//Model 'L' return immediately.

	lcd_lbusy();

	P4 = 0x20;	/* R/S hi */
	_nop();
	P4 = 0x30;	/* E2 hi */

	lcd_wdata(lcd_data);

	P4 = 0x20;	/* R/S hi */
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
