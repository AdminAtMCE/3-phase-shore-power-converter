/* @(#)SID.C */
/* ASEA SOURCE CODE */
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	FILE:			sid.c
//	PURPOSE:		SYSTEM INTERFACE DRIVER 
//					Source Module for the ASEA Controller, 3-phase models.
//
//	Proprietary Software:	ASEA POWER SYSTEMS
//
//				COPYRIGHT @ ASEA JANUARY 2001
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/*=========================================================================*/
/*=========================================================================*/
//	file:	sid.c	   	 
/*=========================================================================*/
/*=========================================================================*/
#include "ac.h"

//EVENT LOG extern
#include "event.h"
extern int LogEvent( unsigned char, unsigned char );

/* ========================================================================================================= */
/* ======================================================================================= PERSISTENT MEMORY */
#pragma noclear
/* ========================================================================================================= */
#pragma align hb=c 								// ALIGNMENT: PEC ADDRESSABLE
/* ========================================================================================================= */
/* ========================================================================================================= */
#pragma default_attributes
/*=========================================================================*/
/*=========================PUBLIC==========================================*/

#ifdef MM3
char configuration[14]="MMxx        \0";
void configDualMasterController( void );	//uses CPU's P8 header for configuration
char *make_DMC_model_number( void );
unsigned config_DMC_output_form( void );
float config_DMC_output_voltage( void );
float config_DMC_output_frequency( void );
#else
char configuration[14]="ACxx        \0";
#endif //MM3
char *gp_cid=configuration;

float IN_RATED_AMPS_LO_ONE;
float IN_RATED_AMPS_HI_ONE;
float IN_RATED_AMPS_LO_TWO;
float IN_RATED_AMPS_HI_TWO;

float OUTPUT_RATED_AMPS;
float RATED_POWER;
float GP_OUTPUT_VOLTAGE;
float GP_OUTPUT_FREQUENCY;
float GP_XFMR_RATIO;

unsigned GP_OUTPUT_FORM;
unsigned GP_OUTPUT_COUPLING;
unsigned DUAL_INPUT;
unsigned SEAMLESS_TRANSFER;
unsigned PARALLELING;
unsigned DUAL_GENSET;
unsigned TECHNEL_OPTION;
unsigned TIP_OPTION;
unsigned AEM_OPTION;
unsigned EXT_CB_OPTION;
unsigned EXT_XFMR_OPTION;
unsigned EXT_OSC_OPTION;
unsigned AEM2_OPTION;
unsigned MODEL_LC;			//S4-8
unsigned AC75_CONTROL_PCB;	//S4-7	Used in Model 'L' Series & AC75, or bigger, units.
unsigned GEN_START_OPTION;	//
unsigned SWITCHGEAR_INTERFACE_OPTION;	//S2-8
unsigned REMOTE_RUN;					//S2-4 RUN(low) input M1041

unsigned GEN_AMPS_PRESENT;	//S4-6  v2.32, or better
unsigned MODIFIED_VOUT;		//S4-5  v2.34, or better

//unsigned MOD1030_OPTION;	//S4-1
unsigned NV_MODEL;			//S4-1
unsigned transferMode;		
unsigned NEW_G2G_OPTION;	//S3-8:	Obsolete
unsigned LOW_RANGE_INPUT;	//S3-8
unsigned DELTA_OUTPUT;
unsigned GEN2GEN_OPTION;
unsigned MULTIGEN_OPTION;	//S3-5
unsigned MULTIGEN_INHIBIT;	//S3-6
unsigned CORDMAN_OPTION;	//S3-7
unsigned OPTCON_PCB;		//opcon_status bit#15
unsigned AUTO_TRANSFER;
unsigned auto_transfer_enable;		
unsigned SIX_CONTACTOR_CONFIG;	//S4-4
unsigned REMOTE_PANEL;			//S4-3
unsigned HIGH_Z;				//S2-3
unsigned TIE_CONF_PRESENT;		//S3-7
unsigned MULTIGEN_BESECKE;		//gp_status_word3 & 0x8000 (high) to configure.
unsigned AUTO_SHUTDOWN;			//M/Y BOUNTY HUNTER feature
unsigned HYBRID_STO;				//S2-3 and S2-7

float Set_Output_Voltage;

void initialize_sid(void);
int set_meter_mux(unsigned, unsigned);
int sys_config(unsigned);
int check_gp_status(void);
int set_sysstat(unsigned);
//unsigned manual_mode(void);
void setGenSpeedMux(int);		//NEW_G2G_OPTION speed-trim MUX

void makeBit( unsigned * ,unsigned, unsigned );	//word bit to value.
//	makeBit(&VIRTUALconfiguration_word2,15,1);	//set S4-1 virtual config bit.

void inhibitInputOn( void );	//REM_IN_OFF asserted HIGH.
void allowInputOn( void );		//REM_IN_OFF deasserted LOW.

/*=========================================================================*/
/*=========================PRIVATE=========================================*/

float config_output_voltage(void);
unsigned config_output_form(void);
float config_output_frequency(void);
unsigned config_output_coupling(void);
float config_xfmr_ratio(void);
float config_rated_power(void);
unsigned config_no_crossover_delay(void);
unsigned config_seamless_transfer_option(void);
unsigned config_paralleling_option(void);
unsigned config_technel_option(void);
unsigned config_aem_option(void);
unsigned config_dual_genset_option(void);
unsigned config_new_technel_option(void);	//S2-8	SWITCHGEAR_INTERFACE_OPTION
unsigned config_remote_run_option(void);	//S2-4	REMOTE_RUN
unsigned config_high_z(void);				//S2-3	HIGH_Z

void config_extended_options(void);

char *make_gps_model_number(void);

void cocboWatch(void);
void cocboEnableOutput(void);
void cocboDisableOutput(void);
void updateLEDs(void);
void ledLightShow(void);

void set_SYSSTAT_ALARM(void);		//K3 on Seamless Transfer PCB signal high.
void clear_SYSSTAT_ALARM(void);	//K3 on Seamless Transfer PCB signal low.

unsigned get_REM_XFR_REQ(void);	//REMOTE_PANEL
void EXT_OUT_OFF_CMD(void);		//reset OUTPUT_ENABLE via P35-1,-2 50ms pulse.
unsigned get_REM_RUN_REQ(void);	//REMOTE_RUN.  Returns 1 if REM_RUN_REQ received.
unsigned get_EPO_CONF(void);	//REMOTE_RUN.  Returns 1 if REM_EPO received.

/*=========================================================================*/
/*=========================================================================*/
/* ======================================================================================= PERSISTENT MEMORY */
#pragma noclear
/* ========================================================================================================= */
/* ========================================================================================================= */
#pragma align hb=c 								// ALIGNMENT: PEC ADDRESSABLE
/* ========================================================================================================= */

#ifdef NO_GP2_SIC_PCB
volatile unsigned far int gp_control_word;			// GP CONTROL WORD--DUMMY.
volatile unsigned far int gp_configuration_word;	// GP CONFIGURATION WORD--DUMMY.
volatile unsigned far int gp_configuration_word2;	//CS3B(low) GP CONFIGURATION WORD.
volatile unsigned far int gp_status_word;			// GP STATUS 1 WORD--DUMMY.
volatile unsigned far int gp_status_word2;			// GP STATUS 2 WORD--DUMMY.
volatile unsigned far int gp_status_word3;			//CS3C(low) GP STATUS WORD3.
#else
#pragma combine fb=A0x7F01E
volatile unsigned far int gp_control_word;			//CS3B(low) GP CONTROL WORD.
#pragma combine fb=A0x7F010
volatile unsigned far int gp_configuration_word;	//CS3B(low) GP CONFIGURATION WORD.
#pragma combine fb=A0x7F018
volatile unsigned far int gp_configuration_word2;	//CS3B(low) GP CONFIGURATION WORD.
#pragma combine fb=A0x7F012
volatile unsigned far int gp_status_word;			//CS3C(low) GP STATUS WORD1.
#pragma combine fb=A0x7F014
volatile unsigned far int gp_status_word2;			//CS3C(low) GP STATUS WORD2.
#pragma combine fb=A0x7F016
volatile unsigned far int gp_status_word3;			//CS3C(low) GP STATUS WORD3.
#endif // NO_GP2_SIC_PCB

#ifdef NO_OPTCON_PCB
volatile unsigned far int optcon_control;		// OPCON CONTROL WORD--DUMMY.
volatile unsigned far int optcon_control_2;		// OPCON CONTROL WORD 2.
#else												//CS3B(low)
#pragma combine sb=A0xEF80		//WCS30(low)
volatile system unsigned int optcon_control;		//CS2*Y7*(low) OPCON CONTROL WORD.
#pragma combine sb=A0xEF82		//WCS31(low)
volatile system unsigned int optcon_control_2;		//CS2*Y7*(low) OPCON CONTROL WORD 2.
#endif // ~NO_OPTCON_PCB


/* ========================================================================================================= */
#pragma default_attributes
/* ========================================================================================================= */
unsigned VIRTUALconfiguration_word;		// VIRTUAL CONFIGURATION WORD.
unsigned VIRTUALconfiguration_word2;	// VIRTUAL CONFIGURATION WORD.

volatile unsigned shutoffId;

unsigned volatile GP_CONTROL_WORD;	//image in RAM
unsigned OPTCON_CONTROL;	//image in RAM
unsigned OPTCON_CONTROL_2;	//image in RAM

int three_phase_mode_flag;	//TRUE when LATCH_1PHASE_MODE == LOW	//v1.84

unsigned last_sw1;
unsigned last_sw2;
unsigned last_sw3;

int gen_mux_id;	//added to (private) persistant ram v2.47 for SWITCHGEAR_INTERFACE_OPTION
int last_busState;

unsigned int Oil_Level_Timer, Moisture_Oil_Timer;
unsigned char Oil_Level_Timer_Set, Moist_Oil_Timer_Set;
unsigned int old_moisture_oil;
unsigned int Oil_Fault;

unsigned char EPO_Stat;

/*=========================================================================*/
//				EXTERN
/*=========================================================================*/
extern void clear_meter_data(unsigned);		// set measured meter data values to zero for given daq channel.
extern float amps2volts;					// sets ammeter scaling.
extern float volts2volts;					// sets voltmeter scaling.
extern float XFMR_ratio;					/* XFMR_ratio in use */

extern unsigned resetDelay;
extern unsigned resetCbOn;
extern volatile int display_type;
extern int active_status_code;
extern volatile unsigned transfer_in_progress;
extern unsigned external_cb_delay;
extern unsigned external_cb_Open_delay;	//seperate Open Delay added v2.90
extern unsigned sync_mux_id;
extern unsigned daq_mux_id;
extern struct gp_state_struct gp_state;
extern volatile int gp_diagnostic_code;
extern unsigned autorestart;
extern unsigned last_autorestart;
extern volatile unsigned input_dropped;
extern volatile unsigned remote_off_flag;
extern volatile unsigned shore_power_off_key; //v1.84
extern float gp_Van[8], gp_Vbn[8], gp_Vcn[8];			/* RMS metered output value */
extern float gp_Vab[8], gp_Vbc[8], gp_Vca[8];			/* RMS metered output value */
extern float gp_Ia_rms[8], gp_Ib_rms[8], gp_Ic_rms[8];	// RMS metered output values.
extern int AGC_state;						/* 0=disabled, 1=Enabled */
extern struct mach_struct mach;
extern int shoreCordRating;						// {30 to 250 amps}.
extern int alarmLevel;								// {50to100% of dsc_cordRating}.
extern unsigned alarmState;						// {DISABLED,ENABLED}.
extern int droop;
extern int droopEnable;

extern unsigned long droopOnTime;
extern unsigned long droopDampTime;

extern float newVoltage;	//used with slew_output() for load management.
extern int slew_output(float, float, float);		//create and execute transient for output transistion.

extern unsigned LAST_configuration_word;	// GP CONFIGURATION WORD--LAST.
extern unsigned LAST_configuration_word2;	//CS3B(low) GP CONFIGURATION WORD--LAST.

extern volatile unsigned long eventTime;	//unsigned long eventTime in 10ms units.
extern unsigned remote_genset;
extern unsigned c2g;
extern unsigned g2c;
extern unsigned autoTransferOnOverload;
extern unsigned autoTransferGenset;
extern unsigned long overloadAlarmTime;
extern unsigned maintenanceMode; //1=No transfers, CP_ON ok.  0=Transfers ok if bus well-defined.

//TIP_OPTION
extern unsigned tip_delay;
extern unsigned tip_cmd_enable;
extern unsigned u_TIP_signal;
//END TIP_OPTION
extern volatile unsigned abort_power_on;

extern volatile int monitor_started;
extern volatile unsigned new_key;

extern unsigned monitorExtCb;
extern volatile unsigned retryCount;
extern float system_level;						//highest level of input/output A/B/C.
extern volatile unsigned special_function;

extern unsigned gen1DutyCycle;
extern unsigned gen2DutyCycle;

extern unsigned negCrossover;

extern char *shutoffStr[15];					//08082012 DHK
extern char *EvName[];

extern unsigned int moisture_oil_init_delay;

extern Xfer_Imped_Ramp_Stat_Type Xfer_Imped_Ramp_Status;
extern unsigned int Active_DutyCycle;

extern unsigned int debug, debug2, debug3;

extern unsigned int optcon_status_rd, optcon_status_save;

extern int last_failure_code_logged;

extern int Gen_Sel;
extern unsigned gen_start_cmd_enable;



//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//@SID		SID		SID		SID		SID		SID		SID		SID		SID
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
/*---------------------------------------------------------------------------*/
_inline unsigned EXTERNAL_CB_CLOSED()
{
	if (SIX_CONTACTOR_CONFIG)
	{
		return  (_getbit(P7,4)||_getbit(P5,9));	//CRN EXTERNAL_CB Confirmation signal.
	}
	else
	{
		if (OPTCON_PCB)
			return  ((optcon_status_2 & 0x1000) == 0x1000);	//EXTERNAL_CB Confirmation signal.
		else
			return (_getbit (P5, 10));						//Converter Output C/B Confirmation
	}
}
/*---------------------------------------------------------------------------*/
/*
_inline unsigned tieBreaker_CLOSED()
{
	if (TIE_CONF_PRESENT)
		return (_getbit(P5,9));					//TIE AUX Confirmation signal.
	else
		return  ((optcon_status_2 & 4) == 4);	//CB3 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned tieBreaker_CLOSED()
{
#ifndef WESTPORT50
	if (TIE_CONF_PRESENT)
		return (_getbit(P5,9));					//TIE AUX Confirmation signal.
	else
#endif //WESTPORT50
		return  ((optcon_status_2 & 4) == 4);	//CB3 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned cb4_CLOSED()
{
	if (NEW_G2G_OPTION)
		return  ((optcon_status_2 & 0x1000) == 0x1000);	//EXTERNAL_CB Confirmation signal.
	else
		return  ((optcon_status_2 & 1) == 1);	//CB4 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned cb5_CLOSED()
{
	return  ((optcon_status_2 & 2) == 2);	//CB5 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned eGen1_CLOSED()
{
	return  ((optcon_status_2 & 0x0100) == 0x0100);	//GENERATOR N1 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned eGen2_CLOSED()
{
	return  ((optcon_status_2 & 0x0200) == 0x0200);	//GENERATOR N2 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned eGen3_CLOSED()
{
#ifdef TEST_FIXTURE
	return  ((gp_configuration_word & 0x4000)!=0);	//FOR DEBUG ONLY!!
#else
	return  ((optcon_status_2 & 0x0400) == 0x0400);	//GENERATOR FORE Confirmation signal.
#endif //TEST_FIXTURE
}
/*---------------------------------------------------------------------------*/
_inline unsigned eGen4_CLOSED()
{
	return  ((optcon_status_2 & 0x0800) == 0x0800);	//GENERATOR SPARE Confirmation signal.
}
/*--------------------------------------------------------------------------*/
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//@SID		SID		SID		SID		SID		SID		SID		SID		SID
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
/*--------------------------------------------------------------------------*/
void initialize_sid()
{
//	if (OPTCON_PCB)
		gp_control_word		=GP_CONTROL_WORD	=0x0F00;		//initialize h/w latch and the image in RAM
//	else
//		gp_control_word		=GP_CONTROL_WORD	=0x3F00;		//initialize h/w latch and the image in RAM

	optcon_control		=OPTCON_CONTROL		=0x0000;
	optcon_control_2	=OPTCON_CONTROL_2	=0x0000;
	
//	optcon_status_rd = optcon_status;

//v1.84		
 	three_phase_mode_flag = FALSE;	//TRUE when LATCH_1PHASE_MODE == LOW
	gen_mux_id = -1;	//force 1st time selection GEN#3 IPL issue w/Bounty Hunter

	_putbit(0,P3,9);	//initialize Shed OFF
	_putbit(0,P3,5);	//initialize Add OFF
}
/*--------------------------------------------------------------------------*/
void setGenSpeedMux(int genset)
{
	if (!NEW_G2G_OPTION) return;

//set gen speed control MUX
	switch (genset)
	{
		case GEN1:	//P47-3 (d1)
//					init_PWM3(9,0, gen1DutyCycle);	//initialize PWM3 to given duty & resolution.
					set_PWM3( gen1DutyCycle );			//sets PWM3 duty cycle to given binary count, 0<=count<=PP3.

					OPTCON_CONTROL = OPTCON_CONTROL & 0xFFFB; 	//clear SEL_G2 in image.
//					optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
//					delay(10);
					OPTCON_CONTROL = OPTCON_CONTROL | 0x0002; 	//set SEL_G1 in image.
					optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
					break;
		case GEN2:	//P47-7 (d2)
//					init_PWM3(9,0, gen2DutyCycle);	//initialize PWM3 to given duty & resolution.
					set_PWM3( gen2DutyCycle );			//sets PWM3 duty cycle to given binary count, 0<=count<=PP3.

					OPTCON_CONTROL = OPTCON_CONTROL & 0xFFFD; 	//clear SEL_G1 in image.
//					optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
//					delay(10);
					OPTCON_CONTROL = OPTCON_CONTROL | 0x0004; 	//set SEL_G2 in image.
					optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
					break;
		default:	
					OPTCON_CONTROL = OPTCON_CONTROL & 0xFFFD; 	//clear SEL_G1 in image.
					OPTCON_CONTROL = OPTCON_CONTROL & 0xFFFB; 	//clear SEL_G2 in image.
					optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
					break;
	}

	delay(10);	//wait 10ms for OMRON G5V-2 Relay to settle.
}
/*--------------------------------------------------------------------------*/
int get_gen_id(void)
{
  int gen_id=0;

	if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
	{
		gen_id = gen_mux_id;
	}
	else
	{
		if (OPTCON_CONTROL_2 & 0x4000) gen_id = 1;			// genset #1.
		else if (OPTCON_CONTROL_2 & 0x8000) gen_id = 2;		// genset #2.
		else if (OPTCON_CONTROL_2 & 0x0800) gen_id = 3;		// genset #3.
		else if (OPTCON_CONTROL_2 & 0x0400) gen_id = 4;		// genset #4.
	}

	return gen_id;
}
/*--------------------------------------------------------------------------*/
int set_gen_mux(int gen_mux)
{
	if (get_gen_id() == gen_mux)
 	{
		gen_mux_id = gen_mux;								//set gen_mux_id now															 
		return (gen_mux);	//only mux if we need to.
	}

	OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0x33FF; 	//select No MUX in image.
										// 0xC3FF = 1100 0011 1111 1111b OBSOLETE
										// 0x33FF = 0011 0011 1111 1111b
	optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL_2.

	delay(250);										//delay 1/10 second.

	switch(gen_mux)
	{
		case 1://G3:	
					if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
					{
					//RELAXED GEN3 MUX
//						OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0800;	// genset #1/#3.
					}
					else
					{
						OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x4000;	// genset #1.
					}

					if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
					{
						set_meter_mux(GEN,GEN_sync);
					}
					break;
		case 2://G4:	
					if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
					{
					//RELAXED GEN4 MUX
//						OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0400;	// genset #2/#4.
					}
					else
					{
						OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x8000;	// genset #2.
					}

					if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
					{
						set_meter_mux(SYS,SYS_sync);
					}
					break;
		case 3://G3:	
					//EXCITED GEN3 MUX
					OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0800;	// genset #3.

					if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
					{
						set_meter_mux(GEN,GEN_sync);
					}
					break;
		case 4://G4:	
					//EXCITED GEN4 MUX
					OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0400;	// genset #4.

					if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
					{
						set_meter_mux(SYS,SYS_sync);
					}
					break;
		case 0://none:	
		default:
					OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0000;	// none.
					break;
	}
	optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL_2.
	gen_mux_id = gen_mux;								//set gen_mux_id now															 

	delay(250);		//delay 100ms to allow MUX output time to settle.

	return gen_mux_id;
}
/*--------------------------------------------------------------------------*/
/*
void openAllCBs()		//open all external CB's.
{
	set_GEN_CBs_OPEN();
}
/*--------------------------------------------------------------------------*/
void set_TIP_CMD()		//set "TIP_CMD" signal HIGH.
{
  int k;

	if (u_TIP_signal==ON) return;

	u_TIP_signal = ON;					//virtual TIP signal for MODBUS
	transfer_in_progress=YES;
	LogEvent(Ev_TIP_CMD_HIGH,0);		//Log unconditionally

	if (NEW_G2G_OPTION) return;			//conflict w/GEN_STOP_CMD.

	if (TIP_OPTION && tip_cmd_enable)
	{
		OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0080; 	//set TIP_CMD in image.
		optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.

		GP_CONTROL_WORD = GP_CONTROL_WORD | 0x8000; 	//set TIP_CMD/GEN_START_CMD in image.
		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
	}
	else
	{
	//TIP signal always out of optcon PCB, except for NEW_G2G_OPTION.
		OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0080; 	//set TIP_CMD in image.
		optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
	}

	for (k=0;k<tip_delay;k++)
	{
		delay(1000);	//delay one second (tip_delay) times.
	}
}
/*--------------------------------------------------------------------------*/
void clear_TIP_CMD()		//set "TIP_CMD" signal LOW.
{
  int k;

	if (u_TIP_signal==OFF) return;

	if (!NEW_G2G_OPTION)
	{
		for (k=0;k<tip_delay;k++)
		{
			delay(1000);	//delay one second (tip_delay) times.
		}

		if (TIP_OPTION && tip_cmd_enable)
		{
			OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xFF7F; 	//clear TIP_CMD in image.
			optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.

			GP_CONTROL_WORD = GP_CONTROL_WORD & 0x7FFF;		//clear TIP_CMD/GEN_START_CMD in image.
			gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
		}
		else
		{
			OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xFF7F; 	//clear TIP_CMD in image.
			optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
		}
	}
	u_TIP_signal = OFF;					//virtual TIP signal for Modbus
	transfer_in_progress=NO;
	LogEvent(Ev_TIP_CMD_LOW,0);			//Log unconditionally
}
/*--------------------------------------------------------------------------*/
void clear_GEN_ON_LED()	//clear "GEN_ON_LED" signal (low).
{
	OPTCON_CONTROL = (OPTCON_CONTROL & 0xFFFE); 	//clear GEN_ON_LED in image.

	optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
}
/*--------------------------------------------------------------------------*/
void set_GEN_ON_LED()	//set "GEN_ON_LED" signal (high).
{
	OPTCON_CONTROL = (OPTCON_CONTROL | 1); 	//set GEN_ON_LED in image.

	optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
}
/*--------------------------------------------------------------------------*/
void set_CONV_BUS_A_CB_OPEN()	//CB4 //toggle "CONV_BUS_A_CB_OPEN" signal high then low.
{
	if (NEW_G2G_OPTION) return;

	OPTCON_CONTROL = OPTCON_CONTROL | 0x8000; 	//set CONV_BUS_A_CB_OPEN in image.

	optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.

	delayWithFreqLock(600,0);										//delay 600ms.

	OPTCON_CONTROL = OPTCON_CONTROL & 0x7FFF;		//clear CONV_BUS_A_CB_OPEN in image.

	optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
}
/*--------------------------------------------------------------------------*/
void set_CONV_BUS_A_CB_CLOSE()	//CB4 //toggle "CONV_BUS_A_CB_CLOSE" signal high then low.
{
	if (NEW_G2G_OPTION) return;
	
	resetCB4();
	
	OPTCON_CONTROL = OPTCON_CONTROL | 0x4000; 	//set CONV_BUS_A_CB_CLOSE in image.

	optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.

	delayWithFreqLock(600,0);										//delay 600ms.

	OPTCON_CONTROL = OPTCON_CONTROL & 0xBFFF;		//clear CONV_BUS_A_CB_CLOSE in image.

	optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
}
/*--------------------------------------------------------------------------*/
void set_CONV_BUS_B_CB_OPEN()	//CB5 //toggle "CONV_BUS_B_CB_OPEN" signal high then low.
{
	if (NEW_G2G_OPTION) return;

	OPTCON_CONTROL = OPTCON_CONTROL | 0x2000; 	//set CONV_BUS_B_CB_OPEN in image.

	optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.

	delayWithFreqLock(600,0);										//delay 600ms.

	OPTCON_CONTROL = OPTCON_CONTROL & 0xDFFF;		//clear CONV_BUS_B_CB_OPEN in image.

	optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
}
/*--------------------------------------------------------------------------*/
void set_CONV_BUS_B_CB_CLOSE()	//CB5 //toggle "CONV_BUS_B_CB_CLOSE" signal high then low.
{
	if (NEW_G2G_OPTION) return;
	
	resetCB5();

	OPTCON_CONTROL = OPTCON_CONTROL | 0x1000; 	//set CONV_BUS_B_CB_CLOSE in image.

	optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.

	delayWithFreqLock(600,0);										//delay 600ms.

	OPTCON_CONTROL = OPTCON_CONTROL & 0xEFFF;		//clear CONV_BUS_B_CB_CLOSE in image.

	optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
}
/*--------------------------------------------------------------------------*/
void set_TIE_BREAKER_CB_OPEN()	//CB3 //toggle "TIE_BREAKER_CB_OPEN" signal high then low.
{
	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.

	LogEvent(Ev_TIE_BREAKER_OPEN,0);

	OPTCON_CONTROL = OPTCON_CONTROL | 0x0800; 	//set TIE_BREAKER_CB_OPEN in image.

	optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.

	delayWithFreqLock(600,0);										//delay 600ms.

	OPTCON_CONTROL = OPTCON_CONTROL & 0xF7FF;		//clear TIE_BREAKER_CB_OPEN in image.

	optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
}
/*--------------------------------------------------------------------------*/
void set_TIE_BREAKER_CB_CLOSE()	//CB3 //toggle "TIE_BREAKER_CB_CLOSE" signal high then low.
{
	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.

	LogEvent(Ev_TIE_BREAKER_CLOSE,0);

	OPTCON_CONTROL = OPTCON_CONTROL | 0x0400; 	//set TIE_BREAKER_CB_CLOSE in image.

	optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.

	delayWithFreqLock(600,0);										//delay 600ms.

	OPTCON_CONTROL = OPTCON_CONTROL & 0xFBFF;		//clear TIE_BREAKER_CB_CLOSE in image.

	optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
}
/*--------------------------------------------------------------------------*/
void set_GEN_START_CMD()		//toggle "GEN_START_CMD" signal high then low.
{
  unsigned genStartDwellTime=3000;

	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.

	LogEvent(Ev_GEN_START_CMD,0);

#ifdef DHK_11212014
	if (TECHNEL_OPTION)		//Gen Start via OPTION PCB P42-1,-3
	{
		OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0008; 	//set GEN_START_CMD in image.
		optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
		GP_CONTROL_WORD = GP_CONTROL_WORD | 0x8000; 	//set GEN_START_CMD in image.
		gp_control_word = GP_CONTROL_WORD;	   			//write GP_CONTROL_WORD.
		delayWithFreqLock(genStartDwellTime,0);			//delay 3sec. for GMM REM_GEN_START
		OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xFFF7; 	//clear GEN_START_CMD in image.
		optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
		GP_CONTROL_WORD = GP_CONTROL_WORD & 0x7FFF;		//clear GEN_START_CMD in image.
		gp_control_word = GP_CONTROL_WORD;	   			//write GP_CONTROL_WORD.
	}
	else	//GEN_START_OPTION (only)	//Gen Start via OPTION PCB P42-5,-7
	{
		if (OPTCON_PCB)
		{
			OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0008; 	//set GEN_START_CMD in image.
			optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
			delayWithFreqLock(genStartDwellTime,0);			//delay 3sec. for GMM REM_GEN_START
			OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xFFF7; 	//clear GEN_START_CMD in image.
			optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
		}
		else
		{
			GP_CONTROL_WORD = GP_CONTROL_WORD | 0x4000; 	//set GEN#1_START_CMD in image.
			gp_control_word = GP_CONTROL_WORD;	   			//write GP_CONTROL_WORD.
			delayWithFreqLock(genStartDwellTime,0);			//delay 3sec. for GMM REM_GEN_START
			GP_CONTROL_WORD = GP_CONTROL_WORD & 0xBFFF;		//clear GEN#1_START_CMD in image.
			gp_control_word = GP_CONTROL_WORD;	   			//write GP_CONTROL_WORD.
		}
	}
#endif

	if (OPTCON_PCB) {
		OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0008; 	//set GEN_START_CMD in image.
		optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
		delayWithFreqLock(genStartDwellTime,0);			//delay 3sec. for GMM REM_GEN_START
		OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xFFF7; 	//clear GEN_START_CMD in image.
		optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
	}
	else {
		_putbit (1, P3, 15);									//set GEN_START_CMD
		delayWithFreqLock(genStartDwellTime,0);					//delay 3sec. for GMM REM_GEN_START
		_putbit (0, P3, 15);									//clear GEN_START_CMD
	}
}
/*--------------------------------------------------------------------------*/
void set_GEN2_START_CMD()		//toggle "GEN2_START_CMD" signal high then low.
{
  unsigned genStartDwellTime=3000;

	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.

	LogEvent(Ev_GEN_START_CMD,0);

#ifdef DHK_11212014
	if (TECHNEL_OPTION)		//Gen Start via OPTION PCB P42-1,-3
	{
		OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0040; 	//set GEN2_START_CMD in image.
		optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
		GP_CONTROL_WORD = GP_CONTROL_WORD | 0x8000; 	//set GEN2_START_CMD in image.
		gp_control_word = GP_CONTROL_WORD;	   			//write GP_CONTROL_WORD.
		delayWithFreqLock(genStartDwellTime,0);			//delay 3sec. for GMM REM_GEN_START
		OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xFFBF; 	//clear GEN2_START_CMD in image.
		optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
		GP_CONTROL_WORD = GP_CONTROL_WORD & 0x7FFF;		//clear GEN2_START_CMD in image.
		gp_control_word = GP_CONTROL_WORD;	   			//write GP_CONTROL_WORD.
	}
	else	//GEN_START_OPTION (only)	//Gen Start via OPTION PCB P42-9,-11
	{
		if (OPTCON_PCB)
		{
			OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0040; 	//set GEN2_START_CMD in image.
			optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
			delayWithFreqLock(genStartDwellTime,0);			//delay 3sec. for GMM REM_GEN_START
			OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xFFBF; 	//clear GEN2_START_CMD in image.
			optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
		}
		else
		{
			GP_CONTROL_WORD = GP_CONTROL_WORD | 0x2000; 	//set GEN#2_START_CMD in image.
			gp_control_word = GP_CONTROL_WORD;	   			//write GP_CONTROL_WORD.
			delayWithFreqLock(genStartDwellTime,0);			//delay 3sec. for GMM REM_GEN_START
			GP_CONTROL_WORD = GP_CONTROL_WORD & 0xDFFF;		//clear GEN#2_START_CMD in image.
			gp_control_word = GP_CONTROL_WORD;	   			//write GP_CONTROL_WORD.
		}
	}
#endif

	if (OPTCON_PCB) {
		OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0008; 	//set GEN_START_CMD in image.
		optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
		delayWithFreqLock(genStartDwellTime,0);			//delay 3sec. for GMM REM_GEN_START
		OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xFFF7; 	//clear GEN_START_CMD in image.
		optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
	}
	else {
		_putbit (1, P3, 15);									//set GEN_START_CMD
		delayWithFreqLock(genStartDwellTime,0);					//delay 3sec. for GMM REM_GEN_START
		_putbit (0, P3, 15);									//clear GEN_START_CMD
	}
}
/*--------------------------------------------------------------------------*/
void set_GEN_STOP_CMD()		//toggle "GEN_STOP_CMD" signal high then low.
{
	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.

	if (NEW_G2G_OPTION)
	{
		LogEvent(Ev_GEN_STOP_CMD,0);

		OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0080; 	//set GEN_STOP_CMD in image.
		optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.

		delayWithFreqLock(1000,0);										//delay 1second.

		OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xFF7F; 	//clear GEN_STOP_CMD in image.
		optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
	}
}
/*--------------------------------------------------------------------------*/
void set_GEN2_STOP_CMD()		//toggle "GEN2_STOP_CMD" signal high then low.
{
	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.

	if (NEW_G2G_OPTION)
	{
		LogEvent(Ev_GEN_STOP_CMD,0);

		OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0020; 	//set GEN2_STOP_CMD in image.
		optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.

		delayWithFreqLock(1000,0);										//delay 1second.

		OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xFFDF; 	//clear GEN2_STOP_CMD in image.
		optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
	}
}
/*--------------------------------------------------------------------------*/
void set_GEN_CBs_OPEN()			//toggle "GEN_1_CB_OPEN" signal high then low.
{								//AND toggle "GEN_2_CB_OPEN" signal high then low.
	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.

	if (SIX_CONTACTOR_CONFIG)
	{
	//deassert GEN#1 Close cmds
		GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFF7F;		//clear GEN_1_CB_CLOSE in image.
		GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFBF;		//clear GEN_1_CB_OPEN in image.
	//deassert GEN#2 Close cmds
		GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFDF;		//clear GEN_2_CB_CLOSE in image.
		GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFEF;		//clear GEN_2_CB_OPEN in image.
		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.

		return; //CRN has no gen open interface
	}

	LogEvent(Ev_GEN1_CB_OPEN,0);

	GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0040; 	//set GEN_1_CB_OPEN in image.

	if (!AEM_OPTION)	
	{
		LogEvent(Ev_GEN2_CB_OPEN,0);
		GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0010; 	//set GEN_2_CB_OPEN in image.
	}

	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
	
	Gen_Sel = 8;								//Gen1 and Gen2 CB

	delayWithFreqLock(700,0);										//12112014 DHK: changed to 700; delay 600ms.
	
	GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFBF;		//clear GEN_1_CB_OPEN in image.
	if (!AEM_OPTION)	
	{
		GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFEF;		//clear GEN_2_CB_OPEN in image.
	}

	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
}
/*--------------------------------------------------------------------------*/
void set_GEN_2_CB_OPEN()		//toggle "GEN_2_CB_OPEN" signal high then low.
{
	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.

	if (SIX_CONTACTOR_CONFIG) return; //CRN has no gen open interface

	if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
	{
		set_GEN_1_CB_OPEN();
		return;
	}

	if (!AEM_OPTION)	
	{
		LogEvent(Ev_GEN2_CB_OPEN,0);
		GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0010; 	//set GEN_2_CB_OPEN in image.
		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
		
		Gen_Sel = GEN2;
		delayWithFreqLock(700,0);										//12112014 DHK: changed to 700; delay 600ms.
		
		GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFEF;		//clear GEN_2_CB_OPEN in image.
		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
	}
}
/*--------------------------------------------------------------------------*/
void set_GEN_1_CB_OPEN()		//toggle "GEN_1_CB_OPEN" signal high then low.
{
	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.
	LogEvent(Ev_GEN1_CB_OPEN,0);
	if (SIX_CONTACTOR_CONFIG) return; //CRN has no gen open interface

	GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0040; 	//set GEN_1_CB_OPEN in image.
	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
	Gen_Sel = GEN1;
	
	if (negCrossover)
		delayWithFreqLock(250,0);										//delay 600ms.
	else
		delayWithFreqLock(700,0);										//12112014 DHK: changed to 700; delay 600ms.
		
	GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFBF;		//clear GEN_1_CB_OPEN in image.
	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
}
/*--------------------------------------------------------------------------*/
void set_GEN_1_CB_CLOSE()		//toggle "GEN_1_CB_CLOSE" signal high then low.
{
	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.

	if (!SIX_CONTACTOR_CONFIG) resetGen1CB();
	LogEvent(Ev_GEN1_CB_CLOSE,0);
	GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0080; 	//set GEN_1_CB_CLOSE in image.
	if (SIX_CONTACTOR_CONFIG) GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0040; 	//set GEN_1_CB_OPEN in image.
	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
	delayWithFreqLock(700,0);										//12112014 DHK: changed to 700; delay 600ms.
	GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFF7F;		//clear GEN_1_CB_CLOSE in image.
	if (SIX_CONTACTOR_CONFIG) GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFBF;		//clear GEN_1_CB_OPEN in image.
	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
}
/*--------------------------------------------------------------------------*/
void set_GEN_2_CB_CLOSE()		//toggle "GEN_2_CB_CLOSE" signal high then low.
{
	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.

	if (!AEM_OPTION)	
	{
		if (!SIX_CONTACTOR_CONFIG) resetGen2CB();
		LogEvent(Ev_GEN2_CB_CLOSE,0);
		GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0020; 	//set GEN_2_CB_CLOSE in image.
		if (SIX_CONTACTOR_CONFIG) GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0010; 	//set GEN_2_CB_OPEN in image.
		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
		delayWithFreqLock(700,0);										//12112014 DHK: changed to 700; delay 600ms.
		GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFDF;		//clear GEN_2_CB_CLOSE in image.
		if (SIX_CONTACTOR_CONFIG) GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFEF;		//clear GEN_2_CB_OPEN in image.
		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
	}
}
/*=========================================================================*/
void inhibitInputOn()	//REM_IN_OFF asserted HIGH.
{
	LogEvent(Ev_REM_IN_OFF,HIGH);
	GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0008; 	//set REM_IN_OFF in image.
	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
}
/*=========================================================================*/
void allowInputOn()		//REM_IN_OFF deasserted LOW.
{
	LogEvent(Ev_REM_IN_OFF,LOW);
	GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFF7;		//clear REM_IN_OFF in image.
	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
}
/*=========================================================================*/
void set_REM_IN_OFF()		//toggle "REM_IN_OFF" signal high then low.
{
	LogEvent(Ev_REM_IN_OFF,0);

	GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0008; 	//set REM_IN_OFF in image.

	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.

	delay(100);										//delay 1/10 second.

	GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFF7;		//clear REM_IN_OFF in image.

	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
}
/*--------------------------------------------------------------------------*/
void set_REM_IN_ON()		//toggle "REM_IN_ON" signal high then low.
{
	LogEvent(Ev_REM_IN_ON,0);

	GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0004; 	//set REM_IN_ON in image.

	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.

	delay(100);										//delay 1/10 second.

	GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFFB;		//clear REM_IN_ON in image.

	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
}
/*--------------------------------------------------------------------------*/
void set_REM_OUT_OFF()		//toggle "REM_OUT_OFF" signal high then low.
{
	LogEvent(Ev_REM_OUT_OFF,0);

	GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0002; 	//set REM_OUT_OFF in image.

	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.

	delayWithFreqLock(100,0);										//delay 1/10 second.

	GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFFD;		//clear REM_OUT_OFF in image.

	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
}
/*--------------------------------------------------------------------------*/
void set_REM_OUT_ON()		//toggle "REM_OUT_ON" signal high then low.
{
	LogEvent(Ev_REM_OUT_ON,0);

	GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0001; 	//set REM_OUT_ON in image.

	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.

	delayWithFreqLock(100,0);										//delay 1/10 second.

	GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFFE;		//clear REM_OUT_ON in image.

	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
}
/*--------------------------------------------------------------------------*/
void enable_output()  //level control "REM_OUT_OFF" high=enable output low=disable output.
{
#ifdef MM3
	return;
#else

	if (TIE_CONF_PRESENT)			//this is a work-around to accomidate Open Tie Restarts.
	{
		if (!tieBreaker_CLOSED()) 						//TB CB Open
			gp_state.generator2_contactor_state = OFF;  	// GEN #2 CONTACTOR CONFIRM
	}

 	if (MULTIGEN_BESECKE)
	{
		if ((!tieBreaker_CLOSED()) || (!EXTERNAL_CB_CLOSED())) 						//TB CB Open
		{
			gp_state.generator1_contactor_state = OFF;  	// GEN #1 CONTACTOR CONFIRM
			gp_state.generator2_contactor_state = OFF;  	// GEN #2 CONTACTOR CONFIRM
		}
	}
#ifdef WESTPORT50
///v1.92 SIO/WESTPORT50 start
	if (!tieBreaker_CLOSED())	//Tie Open, so, allow Gen#3 or Gen#4
	{
		if (!eGen1_CLOSED())
		{
			gp_state.generator1_contactor_state = OFF;  	// GEN #1 CONTACTOR CONFIRM
			gp_state.generator2_contactor_state = OFF;  	// GEN #2 CONTACTOR CONFIRM
		}
	}
///v1.92 SIO/WESTPORT50 end
#endif //WESTPORT50

	if ((transfer_in_progress) || ((!gp_state.generator1_contactor_state) && (!gp_state.generator2_contactor_state)))
	{
//		set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.

		if ((GP_CONTROL_WORD & 2) != 2)
		{
			LogEvent(Ev_REM_OUT_OFF,1);
//			debug2 = 1;
		}

		GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0002; 	//set REM_OUT_OFF in image.

		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.

		if (EXT_CB_OPTION)
		{
			delayWithFreqLock(50,0);		//give K4 time to settle.

			resetExtCB();		//toggle "EXT_CB_OPEN" signal high then low.
			set_EXT_CB_CLOSE();				//600ms strobe out of PD14.

			if (monitorExtCb &&(!EXTERNAL_CB_CLOSED()))
			{
				remote_off_flag=YES;
				gp_diagnostic_code=-24;
				set_sysstat(WARNING);
			}
			else
			{
				gp_state.output_contactor_state=ON;
				gp_state.invstat=ONLINE;
				gp_state.converter_power_state=ON;
			}
		}
	}

	if (!transfer_in_progress)
	{
		if (gp_state.generator1_contactor_state || gp_state.generator2_contactor_state)	//if either GEN#1 or GEN#2 online.
		{
			remote_off_flag=YES;
		}
	}

 	if (!MULTIGEN_BESECKE)
	{
		gp_state.generator1_contactor_state = _getbit(P7,7);   	// GEN #1 CONTACTOR CONFIRM restore
		gp_state.generator2_contactor_state = _getbit(P7,6);   	// GEN #2 CONTACTOR CONFIRM restore
	}
#ifdef WESTPORT50
///v1.92 SIO/WESTPORT50 start
	if (!tieBreaker_CLOSED())	//Tie Open, so, allow Gen#3 or Gen#4
	{
		if (!eGen1_CLOSED())
		{
			gp_state.generator1_contactor_state = OFF;  	// GEN #1 CONTACTOR CONFIRM
			gp_state.generator2_contactor_state = OFF;  	// GEN #2 CONTACTOR CONFIRM
		}
	}
///v1.92 SIO/WESTPORT50 end
#endif //WESTPORT50

#endif //~MM#
}
/*--------------------------------------------------------------------------*/
void disable_output()  //level control "REM_OUT_OFF" high=enable output low=disable output.
{
#ifdef MM3
	return;
#else
	int temp_transfer_in_progress=transfer_in_progress;
	
	if (EXT_CB_OPTION && (gp_state.output_contactor_state==ON))	//v1.61a
	{
		transfer_in_progress=YES;	//ADDED v3.06 4/27/2009 dave hold K4 while ExtCB opens.

		set_EXT_CB_OPEN();		//600ms strobe out of PD13.

		if (monitorExtCb &&(EXTERNAL_CB_CLOSED()))
		{
			gp_diagnostic_code=-23;
			set_sysstat(WARNING);
		}
		gp_state.invstat=OFFLINE;
		gp_state.output_contactor_state=OFF;
		gp_state.converter_power_state=OFF;

		transfer_in_progress=temp_transfer_in_progress;	//restore to previous value
	}

	if ((GP_CONTROL_WORD & 2) == 2)
	{
		LogEvent(Ev_REM_OUT_OFF,2);
	}

	GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFFD;		//clear REM_OUT_OFF in image.
	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.

	if (gp_status_word & 0x0040) 	// OUTPUT_ENABLE.
	{
		EXT_OUT_OFF_CMD();	//reset OUTPUT_ENABLE via P35-1,-2 10ms pulse. v2.44
	}
#endif //MM#
}
/*--------------------------------------------------------------------------*/
void set_EXT_CB_OPEN()		//toggle "EXT_CB_OPEN" signal high then low.
{							//shadow with option-control-pcb's CONV_CB_OPEN signal.
	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.

	if (SIX_CONTACTOR_CONFIG) return;

	if (external_cb_Open_delay)
	{
		LogEvent(Ev_EXT_CB_OPEN,0);
	
		OPTCON_CONTROL = OPTCON_CONTROL | 0x0200; 	//set CONV_CB_OPEN in image.
		optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.

		GP_CONTROL_WORD = GP_CONTROL_WORD | 0x2000; //set EXT_CB_OPEN in image.
		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.

//		debug = 3;
		delayWithFreqLock(external_cb_Open_delay,0);										//delay 600ms.

		OPTCON_CONTROL = OPTCON_CONTROL & 0xFDFF; 	//clear CONV_CB_OPEN in image.
		optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.

		GP_CONTROL_WORD = GP_CONTROL_WORD & 0xDFFF;	//clear EXT_CB_OPEN in image.
		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
	}
}
/*--------------------------------------------------------------------------*/
void set_EXT_CB_CLOSE()		//toggle "EXT_CB_CLOSE" signal high then low.
{							//shadow with option-control-pcb's CONV_CB_CLOSE signal.
	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.

	if (external_cb_delay)
	{
		LogEvent(Ev_EXT_CB_CLOSE,0);
	
		OPTCON_CONTROL = OPTCON_CONTROL | 0x0100; 	//set CONV_CB_CLOSE in image.
		optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.

		GP_CONTROL_WORD = GP_CONTROL_WORD | 0x4000; 	//set EXT_CB_CLOSE in image.
		if (SIX_CONTACTOR_CONFIG) {
			GP_CONTROL_WORD = GP_CONTROL_WORD | 0x2000; //set EXT_CB_OPEN in image.
//			debug = 4;
		}

		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.

		delayWithFreqLock(external_cb_delay,0);										//delay 600ms.

		OPTCON_CONTROL = OPTCON_CONTROL & 0xFEFF; 	//clear CONV_CB_CLOSE in image.
		optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.

		GP_CONTROL_WORD = GP_CONTROL_WORD & 0xBFFF;		//clear EXT_CB_CLOSE in image.
		if (SIX_CONTACTOR_CONFIG) GP_CONTROL_WORD = GP_CONTROL_WORD & 0xDFFF;	//clear EXT_CB_OPEN in image.
		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
	}
}
/*--------------------------------------------------------------------------*/
int set_meter_mux(unsigned daq_mux, unsigned sync_mux)
{
	GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0F00; 	//select No MUX in image.

#ifndef MM3
//#ifndef TEST_FIXTURE

//	delay(100);										//delay 1/10 second.
	delay(1);										//delay 1ms.

	switch(daq_mux)
	{
		case OSC:	
					GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFBFF;	// OUTPUT.
					break;

		case GEN:	GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFDFF;	// GENERATOR.
					break;

		case SYS:	GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFEFF;	// SYSTEM.
					break;
		case SP1:	
		default:	GP_CONTROL_WORD = GP_CONTROL_WORD & 0xF7FF;	// INPUT.
					break;

	}
//#endif //~TEST_FIXTURE

	gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
															 
//	delay(40);		//delay 40ms to allow MUX output time to settle.
	delay(1);										//delay 1ms.
#endif //~MM3

	daq_mux_id = daq_mux;								//set daq_mux_id now
	sync_mux_id = sync_mux;								//set sync_mux_id now

	return 0;
}
/*--------------------------------------------------------------------------*/
void set_CORD_ALARM()		//"CORD_ALARM" signal high.
{
	OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0004; 	//set SHORE_CORD_ALARM in image.
	optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.

	GP_CONTROL_WORD = GP_CONTROL_WORD | 0x1000; 	//set CORD_ALARM in image.
	gp_control_word = GP_CONTROL_WORD;	   	//write GP_CONTROL_WORD to hardware.

	if (gp_state.cord_fault==0)
	{
		gp_state.cord_fault=1;					// {OFF=0.ON=1}.
		LogEvent(Ev_SHORE_ALARM_ON,0);	
	}
}
/*--------------------------------------------------------------------------*/
void clear_CORD_ALARM()		//"CORD_ALARM" signal low.
{
	if (gp_state.system_overtemperature != 1)	//cord alarm also used for OT_x_WARNING (mod-1030).
	{
		OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xFFFB; 	//clear SHORE_CORD_ALARM in image.
		optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.

		if (!REMOTE_PANEL)
		{
			GP_CONTROL_WORD = GP_CONTROL_WORD & 0xEFFF; 	//clear CORD_ALARM in image.
			gp_control_word = GP_CONTROL_WORD;	   	//write GP_CONTROL_WORD to hardware.
		}

		if (gp_state.cord_fault==1)
		{
			gp_state.cord_fault=0;					// {OFF=0.ON=1}.
			LogEvent(Ev_SHORE_ALARM_OFF,0);	
		}
	}
}
/*--------------------------------------------------------------------------*/
void set_aem_GEN_MATCH_CONF()		//"GEN_MATCH_CONF" signal high.
{
	LogEvent(Ev_GEN_MATCH_CONF,1);	

	GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0010; 	//set GEN_MATCH_CONF in image.

	gp_control_word = GP_CONTROL_WORD;	   	//write GP_CONTROL_WORD to hardware.
}
/*--------------------------------------------------------------------------*/
void clear_aem_GEN_MATCH_CONF()		//"GEN_MATCH_CONF" signal low.
{
	LogEvent(Ev_GEN_MATCH_CONF,2);	

	GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFEF; 	//clear GEN_MATCH_CONF in image.

	gp_control_word = GP_CONTROL_WORD;	   	//write GP_CONTROL_WORD to hardware.
}
/*--------------------------------------------------------------------------*/
int check_gp_status()
{
#ifdef MM3
	gp_state.range_mode_id=LOW_RANGE;		// {NONE,LOW_RANGE,HIGH_RANGE}.
	gp_state.input1_form = THREE_PHASE;		// {INVALID,ONE_PHASE,TWO_PHASE,THREE_PHASE}.    
	three_phase_mode_flag = TRUE;
	gp_state.inpstat1=TRUE;	// FOR DEBUG ONLY!!;
	gp_state.invstat=TRUE;	// FOR DEBUG ONLY!!;
	gp_state.converter_power_state=gp_state.invstat;	// FOR DEBUG ONLY!!;
	set_sysstat(OK);
//	return gp_state.sysstat;		
#else //~MM3

  int sysLevel=(int)system_level;
  struct gp_state_struct old_state=gp_state;
  struct mach_struct old_mach=mach;
  unsigned old_AGC_state=AGC_state;
  unsigned oldTransferMode=transferMode;		
  float cordRat=(float)(100.0/shoreCordRating);
#ifndef TEST_FIXTURE
  float floatLevel;
  float B_floatLevel;
  float C_floatLevel;
  int intLevel;
  
  unsigned char temp;

	if (REMOTE_PANEL || AEM_OPTION)
	{
		if (_getbit(P7,4))	//clear systat alarm. NOTE CONFLICT w/INHIBIT TRANSFER
		{
			set_sysstat(OK);
		}
	}

	if (alarmState==ENABLED && !transfer_in_progress && !c2g && !g2c)
	{
		floatLevel = gp_Ia_rms[SP1] * cordRat;
		B_floatLevel = gp_Ib_rms[SP1] * cordRat;
		C_floatLevel = gp_Ic_rms[SP1] * cordRat;

		if (B_floatLevel > floatLevel) floatLevel = B_floatLevel;
		if (C_floatLevel > floatLevel) floatLevel = C_floatLevel;

		intLevel = (int) floatLevel;
		
//		if (intLevel >= alarmLevel)
		if ((intLevel >= alarmLevel) || (sysLevel >= alarmLevel))	//v2.30
		{
			if (gp_state.cord_fault==0)
			{
				set_CORD_ALARM();

				if (droopEnable==ENABLED)
				{
					newVoltage = GP_OUTPUT_VOLTAGE - (GP_OUTPUT_VOLTAGE * 0.01f * droop);
					slew_output(0.5, GP_OUTPUT_FREQUENCY, newVoltage);
					droopOnTime = eventTime;
				}
			}

			droopOnTime = eventTime;

			if (overloadAlarmTime==0)
			{
				overloadAlarmTime = eventTime;
			}
		}
		else
		{
			clear_CORD_ALARM();

			if (eventTime >= (droopOnTime+droopDampTime))
			{
				if (newVoltage != GP_OUTPUT_VOLTAGE)
				{
					newVoltage = GP_OUTPUT_VOLTAGE;
					slew_output(0.5, GP_OUTPUT_FREQUENCY, newVoltage);
				}
			}
			overloadAlarmTime = 0;
		}
	}

	if (alarmState==DISABLED)
	{
		clear_CORD_ALARM();

		if ((droopEnable==ENABLED) && (newVoltage != GP_OUTPUT_VOLTAGE))
		{
			newVoltage = GP_OUTPUT_VOLTAGE;
			slew_output(0.5, GP_OUTPUT_FREQUENCY, newVoltage);
		}

		overloadAlarmTime = 0;
	}

	if(gp_state.invstat==ONLINE)
	{
		if (autoTransferOnOverload && gp_state.cord_fault)
		{
			if (eventTime >= (overloadAlarmTime+250))
			{
				if (!transfer_in_progress)
				{
					remote_genset = autoTransferGenset;
					c2g = TRUE;
				}
			}
		}
	}

	if (!SWITCHGEAR_INTERFACE_OPTION && !HIGH_Z && !EXT_OSC_OPTION && !MULTIGEN_OPTION && !MULTIGEN_BESECKE)
	{
		gp_state.generator1_contactor_state = _getbit(P7,7);	// GEN #1 CONTACTOR CONFIRM
		gp_state.generator2_contactor_state = _getbit(P7,6);   	// GEN #2 CONTACTOR CONFIRM
	}

	if (HYBRID_STO)
	{
		gp_state.generator1_contactor_state = _getbit(P7,7);	// GEN CONTACTOR CONFIRM
		gp_state.generator2_contactor_state = FALSE;		   	// GEN #2 CONTACTOR CONFIRM
	}

	if (!EXT_CB_OPTION)
	{
		if (gp_status_word3 & 0x0010)	// K4_CLOSE_CMD
		{
		 	if (MULTIGEN_BESECKE)
			{
				if ((optcon_status_2 & 0x1000)==0x1000)	//EXT CONV CB CONFIRM
				{
					gp_state.invstat=ONLINE;
					gp_state.output_contactor_state=ON;
					gp_state.converter_power_state=ON;
				}
				else
				{
					gp_state.invstat=OFFLINE;
					gp_state.output_contactor_state=OFF;
					gp_state.converter_power_state=OFF;
				}
			}
			else
			{
				gp_state.invstat=ONLINE;
				gp_state.output_contactor_state=ON;
				gp_state.converter_power_state=ON;
			}
		}
		else	// K4_CLOSE_CMD(not)
		{
			gp_state.invstat=OFFLINE;
			gp_state.output_contactor_state=OFF;
			gp_state.converter_power_state=OFF;
		}//endif (gp_status_word3 & 0x0010)	// K4_CLOSE_CMD
	}//endif (!EXT_CB_OPTION)

///v2.55	
//put output in high-impedance when both units online + flag when online via GEN_1_CB_CLOSE signal.

	if (EXT_OSC_OPTION)
	{
		gp_state.generator1_contactor_state = OFF;	// GEN #1 CONTACTOR CONFIRM
		gp_state.generator2_contactor_state = OFF;   	// GEN #2 CONTACTOR CONFIRM

		if (_getbit(P7,7))			//other K4_CLOSE_CMD asserted
		{
			gp_state.inv2stat = ONLINE;
		}
		else
		{
			gp_state.inv2stat = OFFLINE;
		}

		if (gp_status_word3 & 0x0010)	// K4_CLOSE_CMD
		{
			if (_getbit(P7,7))			//other K4_CLOSE_CMD asserted
			{
				init_PWM2(mach.pwm2_bits,mach.pwm2_align,mach.xfr_dutyCycle);	//unsigned(bits,align,duty)
				
				LogEvent(Ev_HIGH_Z,1);	//HIGH
//				debug = 8;
				Xfer_Imped_Ramp_Status.bits.Imped_Comp = 0;
				Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 0;
			}
			else if ((Active_DutyCycle != mach.pwm2_dutyCycle) && (!Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down))
			{
//					init_PWM2(mach.pwm2_bits,mach.pwm2_align,mach.pwm2_dutyCycle);		//unsigned(bits,align,duty)
				Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;			//Reset the Impedance, it is in 10msec loop	
			}
//			else
//			{
//				init_PWM2(mach.pwm2_bits,mach.pwm2_align,mach.pwm2_dutyCycle);	//unsigned(bits,align,duty)
//			}
			GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0080; //set GEN_1_CB_CLOSE in image.
			gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
		}
		else if ((Active_DutyCycle != mach.pwm2_dutyCycle) && (!Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down))
//		else
		{
//			init_PWM2(mach.pwm2_bits,mach.pwm2_align,mach.pwm2_dutyCycle);	//unsigned(bits,align,duty)
			Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;			//Reset the Impedance, it is in 10msec loop	
			GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFF7F;	//clear GEN_1_CB_CLOSE in image.
			gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
		}
	}//endif (EXT_OSC_OPTION)
///v2.55

	if (gp_status_word & 0x0040) 	// OUTPUT_ENABLE.
	{
		if (!EXT_CB_OPTION)
		{
			_nop();		//OPERATOR PRESSED CONVERTER POWER 'ON' BUTTON--NO ACTION BY FIRMWARE.
		}
		else
		{
			if (NEW_G2G_OPTION && !maintenanceMode && !tieBreaker_CLOSED() && !transfer_in_progress)	//& OPERATOR PRESSED CONVERTER POWER 'ON' BUTTON.
			{
				if (get_busState2()==BsG1AG2B)
					abort_power_on=0;
				else
					abort_power_on = 37;
			}
			else
			{
				if(gp_state.invstat==OFFLINE && remote_off_flag==NO)
				{
					if (MULTIGEN_OPTION)
					{
//						debug = 2;
						if (!(optcon_status_2 & 0x0F00)) {			//No Generator Online
							enable_output();	//level control "REM_OUT_OFF" high=enable output low=disable output.
						}
						else
							remote_off_flag=YES;
					}
					else if ((!gp_state.generator1_contactor_state) && (!gp_state.generator2_contactor_state))	//if neither GEN#1 nor GEN#2 online.
					{
						enable_output();	//level control "REM_OUT_OFF" high=enable output low=disable output.
//						debug = 1;
					}
					else
					{
						remote_off_flag=YES;
					}				
				}
			}
		}
	}
	else	// OUTPUT_ENABLE(not) => operator pressed OFF.
	{
		if (!EXT_CB_OPTION)
		{
//			if (MODEL_LC){
//				if ((!moisture_oil) && (gp_state.oil_level)) {						
//					enable_output();	//level control "REM_OUT_OFF" high=enable output low=disable output.
//					remote_off_flag=NO;
//				}
//			}
//			else {
				enable_output();	//LET HARDWARE HOLD OUTPUT OFF. level control "REM_OUT_OFF" high=enable output low=disable output.
//				debug = 2;
				remote_off_flag=NO;
//			}
		}
		else
		{
			if (gp_state.output_contactor_state==ON || remote_off_flag==YES)
			{
				disable_output();	//level control "REM_OUT_OFF" high=enable output low=disable output.
//				debug2++;
//				gp_state.output_contactor_state = OFF;	//v1.84c

				if ((!gp_state.generator1_contactor_state) && (!gp_state.generator2_contactor_state))	//if neither GEN#1 nor GEN#2 online.
				{
					remote_off_flag=NO;
				}
			}
		}
	}

//	if (gp_state.inpstat1==ONLINE && (gp_status_word2 & 0x0008) == 0) 	// V_OUT_OK (low). v1.84q
//	{
//		disable_output();	//level control "REM_OUT_OFF" high=enable output low=disable output.
//	}

	if (AEM_OPTION)
	{
		gp_state.aemGenMatchCmd =  _getbit(P5,9);	// GENERATOR_MATCH_COMMAND.
	}

//OIL LEVEL LOW detection & notification
	if (MODEL_LC) {											//08082012 DHK: Oil Level Low
		gp_state.oil_level = (gp_status_word2 & 0x8000);	// {LOW,NORMAL}. //LOW==OIL_LEVEL_LOW.
	
		if (gp_state.oil_level != old_state.oil_level)
		{
			if (gp_state.oil_level == 0)
			{
//				Oil_Level_Timer_Set = 1;
				if (gp_state.inpstat1==OFFLINE)
					Oil_Level_Timer = 0;
				else
					Oil_Level_Timer = 1500;					//15 second timer using 10msec timer 
				
//#ifdef LIQUID_COOLED_MODEL
//				gp_diagnostic_code=-33;	//oil level low OR if(MOD-1041) epo issued.
//				set_sysstat(WARNING);
//#endif //LIQUID_COOLED_MODEL
			}
			else if (Oil_Fault & 0x01) {
				allowInputOn();						//ALLOW INPUT ON.
				Oil_Fault &= 0xFE;
			}
		}
		else {
			if ((gp_state.oil_level == 0) && (!Oil_Level_Timer))			// && (Oil_Level_Timer_Set))
			{
				shutoffId = 14;				//reason input is off, Oil Level Low
				gp_diagnostic_code=-33;	//oil level low OR if(MOD-1041) epo issued.
				set_sysstat(FAILURE);
				
				if (!(Oil_Fault & 0x01)) {
					inhibitInputOn();			//INHIBIT INPUT ON.
	//				set_REM_IN_OFF();
					Oil_Fault |= 0x01;
				}
			}
		}
	}
	else {
		if (!(gp_status_word2 & 0x8000)) {
			shutoffId = 14;				//reason input is off, EPO
			gp_diagnostic_code=-33;		//EPO issued.
			set_sysstat(FAILURE);		
			
			autorestart=DISABLED;			
			inhibitInputOn();
			EPO_Stat = 0x01;
		}
		else if (EPO_Stat == 0x01)
		{
			allowInputOn();
			EPO_Stat = 0;
		}
	}

	if (gp_status_word3 & 0x0080)	// K2_CLOSE_CMD
	{
		shutoffId = 0;				//reason input is off, its not.
		gp_state.inpstat1=ONLINE;

		if (input_dropped==TRUE && autorestart==ENABLED)
		{
			if (!EXT_CB_OPTION && !shore_power_off_key)	//v1.93b
				gp_reset();
		}

//v1.84 begin		
		if ((gp_status_word2 & 0x0020) && ((gp_status_word2 & 0x0040) != 0x0040))	//if ((1p_MODE==HIGH) && (LATCH_1p==LOW))
		{
			if (three_phase_mode_flag==TRUE)
			{
				set_REM_IN_OFF();
				shutoffId = 8;				//reason input is off, form changed.
				
				if (autorestart==ENABLED)
					gp_reset();
			}

			three_phase_mode_flag = FALSE;
		}
//v1.84 end
	}
	else	// ~K2_CLOSE_CMD
	{
		if (gp_state.inpstat1==ONLINE)	
		{
			if (shore_power_off_key)
			{
				input_dropped=TRUE;			//was ONLINE now (OFFLINE && A-B_OK (not)).
			}

			if (!(gp_status_word & 0x0008))	//AB_OK(not)
			{
				if (!shore_power_off_key)	//placed here & commented-out, v1.93
					input_dropped=TRUE;			//was ONLINE now (OFFLINE && A-B_OK (not)).

				if (!EXT_CB_OPTION || shore_power_off_key)
					disable_output();	//level control "REM_OUT_OFF" high=enable output low=disable output.
//v2.21 begin
				if (input_dropped && shore_power_off_key)
				{
//					if (!autorestart)	//v2.21
					{
						if (gp_diagnostic_code!=-30) 	//~INPUT_OVLD  v2.21
						{
							gp_diagnostic_code=-32;		//low range DOCK POWER BROWNOUT v2.21
							set_sysstat(WARNING);
							shutoffId = 5;				//reason input is off, dock brownout.
						}
					}
				}
			}
			else	//AB_OK
			{
				if (gp_status_word2 & 4)			//LATCH_HIGH
				{
					if ((gp_status_word & 4)!=4)	//AB_HIGH(not)
					{
						if (gp_diagnostic_code!=-30) 	//~INPUT_OVLD  v2.21
						{
							gp_diagnostic_code=-32;		//high range DOCK POWER BROWNOUT v2.21
							set_sysstat(WARNING);
							shutoffId = 5;				//reason input is off, dock brownout.
						}
					}
				}
///REMOTE_RUN M1041
				if (get_EPO_CONF())		//Returns 1 if REM_EPO_CONF is LOW received.
				{
					LogEvent(Ev_COMM_EPO,0);
				}
///
			}
//v2.21 end
			if (EXT_CB_OPTION && !shore_power_off_key)
				disable_output();	//level control "REM_OUT_OFF" high=enable output low=disable output.
		}
		gp_state.inpstat1=OFFLINE;
		gp_state.inpstat2=OFFLINE;

		if (gp_status_word & 0x0008)	//AB_OK,  this test added v1.93b
		{
			if (autorestart && input_dropped && shore_power_off_key && !get_EPO_CONF())	//v1.93b
			{
				gp_reset();
			}
		}
	}

//v1.84 begin		
	if ((gp_status_word2 & 0x0040) != 0x0040)	//if (LATCH_1p_MODE==LOW))
	{
		three_phase_mode_flag = TRUE;
	}
//v1.84 end

//#ifndef TEST_FIXTURE

//	if (gp_status_word & 0x0020)	// INV_ENABLE
//	{
//		if (gp_status_word & 0x1000)	// INV_A_TST_OK
//	}

	if (gp_status_word & 0x0010)	// PFC_ENABLE
	{
		if (gp_status_word & 0x0800)
		{
			gp_state.pfc_fault=1;	// {OK,NOT_OK}.	//NOT_OK==PFC_FAULT.
//			shutoffId = 10;				//reason input is off, pfc fault.
		}
		else if (gp_status_word2 & 0x0800)
		{
			gp_state.pfc_fault=1;	// {OK,NOT_OK}.	//NOT_OK==PFC_FAULT.
//			shutoffId = 10;				//reason input is off, pfc fault.
		}
		else if (gp_status_word3 & 0x0800)
		{
			gp_state.pfc_fault=1;	// {OK,NOT_OK}.	//NOT_OK==PFC_FAULT.
//			shutoffId = 10;				//reason input is off, pfc fault.
		}
		else
		{
			gp_state.pfc_fault=0;	// {OK,NOT_OK}.	//NOT_OK==PFC_FAULT.
		}
	}
	else
	{
		gp_state.inpstat1=OFFLINE;
		gp_state.inpstat2=OFFLINE;
		gp_state.pfc_fault=0;		// {OK,NOT_OK}.	//NOT_OK==PFC_FAULT.
	}

//
//check stati for shutdown terms.
//
	if (gp_status_word & 0x0001) 	// LVDC_OK
	{
		_nop();
	}
	else
	{
		shutoffId = 11;				//reason input is off, lvdc fault.
		gp_diagnostic_code=-29;	
		set_sysstat(FAILURE);
		gp_state.lvdc_fault=1;					// {OK,NOT_OK}.	//NOT_OK==LVDC_FAULT.
	}
#ifndef GEN2GEN_CONTROL_OPTION_TEST
	if (gp_status_word & 0x2000) 	//HVDC_A_>210
	{
		shutoffId = 9;				//reason input is off, hvdc>210 fault.
		gp_diagnostic_code=-20;	//
		set_sysstat(FAILURE);
		gp_state.hvdc_fault=1;		// HVDC>210
	}
	if (gp_status_word2 & 0x2000) 	//HVDC_B_>210
	{
		shutoffId = 9;				//reason input is off, hvdc>210 fault.
		gp_diagnostic_code=-21;	//
		set_sysstat(FAILURE);
		gp_state.hvdc_fault=1;		// HVDC>210
	}
	if (gp_status_word3 & 0x2000) 	//HVDC_C_>210
	{
		shutoffId = 9;				//reason input is off, hvdc>210 fault.
		gp_diagnostic_code=-22;	//
		set_sysstat(FAILURE);
		gp_state.hvdc_fault=1;		// HVDC>210
	}
	if (gp_diagnostic_code==-30) 	//INPUT_OVLD  v2.21
	{
		set_sysstat(FAILURE);		//detected in int10ms
		shutoffId = 7;				//reason input is off, overload.
		gp_state.input_overcurrent=1;		// shore power overload
	}
	
	if (!MODEL_LC) {
		if (gp_status_word2 & 0x0100) 	//GROUND_FAULT
		{
			shutoffId = 12;				//reason input is off, ground fault.
			gp_diagnostic_code=-249;	//GROUND_FAULT DETECTED.
			set_sysstat(FAILURE);
		}
	}
	else {								//if (!moisture_oil_init_delay) {
		if ((gp_status_word2 & 0x0100) != old_moisture_oil) {
			if (gp_status_word2 & 0x0100) {
//				Moist_Oil_Timer_Set = 1;
				if (gp_state.inpstat1==OFFLINE)
					Moisture_Oil_Timer = 0;
				else
					Moisture_Oil_Timer = 1500;
			}
			else if (Oil_Fault & 0x02) {
				allowInputOn();						//ALLOW INPUT ON.
				Oil_Fault &= 0xFD;
			}
		}
		else {
			if ((gp_status_word2 & 0x0100) && (!Moisture_Oil_Timer)) {				// && (Moist_Oil_Timer_Set)) {
				shutoffId = 12;				//Moisture in Coolant Oil detected
				gp_diagnostic_code=-249;	//Moisture in Coolant Oil.
				set_sysstat(FAILURE);
				
				if (!(Oil_Fault & 0x02)) {
					inhibitInputOn();			//INHIBIT INPUT ON.
	//				set_REM_IN_OFF();
	
					Oil_Fault |= 0x02;
				}
			}
//			else if (!Moist_Oil_Timer_Set) {
//				Moist_Oil_Timer_Set = 1;
//				if (gp_state.inpstat1==OFFLINE)
//					Moisture_Oil_Timer = 0;
//				else
//					Moisture_Oil_Timer = 1500;
//			}
//			else				
//				Moist_Oil_Timer_Set = 0;
		}
		
		old_moisture_oil = gp_status_word2 & 0x0100;
	}
	
	if (gp_status_word3 & 0x0002) 	//OUTPUT_OVLD
	{
//		shutoffId = 7;				//reason input is off, overload.
		gp_diagnostic_code=-31;	//
		set_sysstat(FAILURE);
	}
	if (gp_status_word3 & 0x0004) 	//T1_OT
	{
		shutoffId = 6;				//reason input is off, overtemperature.
		gp_diagnostic_code=-261;	//something hot, INV_OT || PFC_OT || XFMR_OT(ignored)
		set_sysstat(FAILURE);
		gp_state.system_overtemperature=1;		// {OK,NOT_OK}.	//NOT_OK==SYSTEM_OVERTEMPERATURE.
	}
#endif //GEN2GEN_CONTROL_OPTION_TEST
	if (gp_status_word & 0x0100) 	//HVDC_PS_OT
	{
		shutoffId = 6;				//reason input is off, overtemperature.
		gp_diagnostic_code=-262;	//something hot, INV_OT || PFC_OT || XFMR_OT(ignored)
		set_sysstat(FAILURE);
		gp_state.system_overtemperature=1;		// {OK,NOT_OK}.	//NOT_OK==SYSTEM_OVERTEMPERATURE.
	}
	if (gp_status_word & 0x0400) 	//INV_A_OT
	{
		shutoffId = 6;				//reason input is off, overtemperature.
		gp_diagnostic_code=-264;	//something hot, INV_OT || PFC_OT || XFMR_OT(ignored)
		set_sysstat(FAILURE);
		gp_state.system_overtemperature=1;		// {OK,NOT_OK}.	//NOT_OK==SYSTEM_OVERTEMPERATURE.
	}
	if (gp_status_word2 & 0x0400) 	//INV_B_OT
	{
		shutoffId = 6;				//reason input is off, overtemperature.
		gp_diagnostic_code=-267;	//something hot, INV_OT || PFC_OT || XFMR_OT(ignored)
		set_sysstat(FAILURE);
		gp_state.system_overtemperature=1;		// {OK,NOT_OK}.	//NOT_OK==SYSTEM_OVERTEMPERATURE.
	}
	if (gp_status_word3 & 0x0100) 	//AMBIENT_OT
	{
		shutoffId = 6;				//reason input is off, overtemperature.
		gp_diagnostic_code=-268;	//something hot, AMBIENT_OT
		set_sysstat(WARNING);
		gp_state.system_overtemperature=1;		// {OK,NOT_OK}.	//NOT_OK==SYSTEM_OVERTEMPERATURE.
	}
	if (gp_status_word3 & 0x0400) 	//INV_C_OT
	{
		shutoffId = 6;				//reason input is off, overtemperature.
		gp_diagnostic_code=-270;	//something hot, INV_OT || PFC_OT || XFMR_OT(ignored)
		set_sysstat(FAILURE);
		gp_state.system_overtemperature=1;		// {OK,NOT_OK}.	//NOT_OK==SYSTEM_OVERTEMPERATURE.
	}

	if (gp_status_word & 0x0200) 	//OT_A_WARNING
	{
		shutoffId = 6;				//reason input is off, overtemperature.
		gp_diagnostic_code=-263;	//something hot.
		set_sysstat(WARNING);
		gp_state.system_overtemperature=1;		// {OK,NOT_OK}.	//NOT_OK==SYSTEM_OVERTEMPERATURE.
	}
	if (gp_status_word2 & 0x0200) 	//OT_B_WARNING
	{
		shutoffId = 6;				//reason input is off, overtemperature.
		gp_diagnostic_code=-266;	//something hot.
		set_sysstat(WARNING);
		gp_state.system_overtemperature=1;		// {OK,NOT_OK}.	//NOT_OK==SYSTEM_OVERTEMPERATURE.
	}
	if (gp_status_word3 & 0x0200) 	//OT_C_WARNING
	{
		shutoffId = 6;				//reason input is off, overtemperature.
		gp_diagnostic_code=-269;	//something hot.
		set_sysstat(WARNING);
		gp_state.system_overtemperature=1;		// {OK,NOT_OK}.	//NOT_OK==SYSTEM_OVERTEMPERATURE.
	}
#endif //~TEST_FIXTURE

	if (gp_status_word & 0x0002)
	{
		gp_state.range_mode_id=LOW_RANGE;		// {NONE,LOW_RANGE,HIGH_RANGE}.
	}
	else
	{
		gp_state.range_mode_id=HIGH_RANGE;		// {NONE,LOW_RANGE,HIGH_RANGE}.
	}

	if (gp_status_word2 & 0x0020)  	//1p_MODE
	{
		gp_state.input1_form = ONE_PHASE;		// {INVALID,ONE_PHASE,TWO_PHASE,THREE_PHASE}.    
		gp_state.input2_form = ONE_PHASE;		// {INVALID,ONE_PHASE,TWO_PHASE,THREE_PHASE}.    
#ifdef DUAL_SHORE_CORD
		gp_state.inpstat2 = OFFLINE;
#endif //DUAL_SHORE_CORD
	}
	else
	{
		gp_state.input1_form = THREE_PHASE;		// {INVALID,ONE_PHASE,TWO_PHASE,THREE_PHASE}.    
		gp_state.input2_form = THREE_PHASE;		// {INVALID,ONE_PHASE,TWO_PHASE,THREE_PHASE}.    
#ifdef DUAL_SHORE_CORD
		if (gp_state.inpstat1==ONLINE)
		{
			gp_state.inpstat2 = ONLINE;
		}
#endif //DUAL_SHORE_CORD
	}

#ifdef DUAL_SHORE_CORD
	gp_state.input1_form = ONE_PHASE;		// {INVALID,ONE_PHASE,TWO_PHASE,THREE_PHASE}.    
	gp_state.input2_form = ONE_PHASE;		// {INVALID,ONE_PHASE,TWO_PHASE,THREE_PHASE}.    
#endif //DUAL_SHORE_CORD


#ifdef TEST_FIXTURE
	gp_state.range_mode_id=LOW_RANGE;		// {NONE,LOW_RANGE,HIGH_RANGE}.
	gp_state.input1_form = THREE_PHASE;		// {INVALID,ONE_PHASE,TWO_PHASE,THREE_PHASE}.    
	three_phase_mode_flag = TRUE;

	gp_state.inpstat1=_getbit(P3,3);	// FOR DEBUG ONLY!!;
	gp_state.invstat=_getbit(P3,2);	// FOR DEBUG ONLY!!;
	gp_state.converter_power_state=gp_state.invstat;	// FOR DEBUG ONLY!!;

//	gp_state.inpstat1=TRUE;	// FOR DEBUG ONLY!!;
//	gp_state.inpstat1=((gp_configuration_word & 0x0100)!=0);	// FOR DEBUG ONLY!!;
//	gp_state.invstat=((gp_configuration_word & 0x1000)!=0);	// FOR DEBUG ONLY!!;
//	gp_state.converter_power_state=((gp_configuration_word & 0x8000)!=0);	// FOR DEBUG ONLY!!;
//
//	if ((gp_configuration_word & 0x2000)!=0) gp_diagnostic_code=-30;
//	if (gp_diagnostic_code==-30) 	//INPUT_OVLD  v2.21
//	{
//		set_sysstat(FAILURE);		//detected in int10ms
//		shutoffId = 7;				//reason input is off, overload.
//		gp_state.input_overcurrent=1;		// shore power overload
//		gp_state.inpstat1=OFFLINE;	// FOR DEBUG ONLY!!;
//		gp_state.invstat=OFFLINE;	// FOR DEBUG ONLY!!;
//		gp_state.converter_power_state=OFF;
//	}
#endif //TEST_FIXTURE


#ifdef GEN2GEN_CONTROL_OPTION_TEST
	if (GEN2GEN_OPTION)
	{
		gp_state.inpstat1=((gp_configuration_word & 0x8000)!=0);	// FOR DEBUG ONLY!!;

//		if ((gp_configuration_word & 0x0100)!=0)
//		{
//			gp_diagnostic_code=-26;	//something hot, INV_OT || PFC_OT || XFMR_OT(ignored)
//			set_sysstat(FAILURE);
//			gp_state.system_overtemperature=1;		// {OK,NOT_OK}.	//NOT_OK==SYSTEM_OVERTEMPERATURE.
//		}
	}
//FOR DEBUG ONLY!!
#endif //GEN2GEN_CONTROL_OPTION_TEST

	if (autorestart != last_autorestart)
	{
		if (autorestart)
			LogEvent(Ev_AUTORESTART_ON,0);
		else
			LogEvent(Ev_AUTORESTART_OFF,0);

		last_autorestart = autorestart;
	}

	if (gp_state.inpstat1 != old_state.inpstat1)
	{
		if (gp_state.inpstat1 == ONLINE)
			LogEvent(Ev_SP1_ONLINE,0);
		else
			LogEvent(Ev_SP1_OFFLINE,0);
	}

#ifdef DHK_RELOCATE_UP
//OIL LEVEL LOW detection & notification
	if (MODEL_LC) {											//08082012 DHK: Oil Level Low
		gp_state.oil_level = (gp_status_word2 & 0x8000);	// {LOW,NORMAL}. //LOW==OIL_LEVEL_LOW.
	
		if (gp_state.oil_level != old_state.oil_level)
		{
			if (gp_state.oil_level == 0)
			{
//				Oil_Level_Timer_Set = 1;
				if (gp_state.inpstat1==OFFLINE)
					Oil_Level_Timer = 0;
				else
					Oil_Level_Timer = 1500;					//15 second timer using 10msec timer 
				
//#ifdef LIQUID_COOLED_MODEL
//				gp_diagnostic_code=-33;	//oil level low OR if(MOD-1041) epo issued.
//				set_sysstat(WARNING);
//#endif //LIQUID_COOLED_MODEL
			}
			else if (Oil_Fault & 0x01) {
				allowInputOn();						//ALLOW INPUT ON.
				Oil_Fault &= 0xFE;
			}
		}
		else {
			if ((gp_state.oil_level == 0) && (!Oil_Level_Timer))			// && (Oil_Level_Timer_Set))
			{
				shutoffId = 14;				//reason input is off, Oil Level Low
				gp_diagnostic_code=-33;	//oil level low OR if(MOD-1041) epo issued.
				set_sysstat(FAILURE);
				
				if (!(Oil_Fault & 0x01)) {
					inhibitInputOn();			//INHIBIT INPUT ON.
	//				set_REM_IN_OFF();
					Oil_Fault |= 0x01;
				}
			}
//			else if (!Oil_Level_Timer_Set) {
//				Oil_Level_Timer_Set = 1;
//				if (gp_state.inpstat1==OFFLINE)
//					Oil_Level_Timer = 0;
//				else
//					Oil_Level_Timer = 1500;					//15 second timer using 10msec timer 
//			}
//			else
//				Oil_Level_Timer_Set = 0;
		}
	}
	else {
		if (!(gp_status_word2 & 0x8000)) {
			shutoffId = 14;				//reason input is off, EPO
			gp_diagnostic_code=-33;		//EPO issued.
			set_sysstat(FAILURE);		
			
			autorestart=DISABLED;			
			inhibitInputOn();
			EPO_Stat = 0x01;
		}
		else if (EPO_Stat == 0x01)
		{
			allowInputOn();
			EPO_Stat = 0;
		}
	}
#endif

	if (!SWITCHGEAR_INTERFACE_OPTION && !MULTIGEN_BESECKE)
	{
		if (gp_state.generator1_contactor_state != old_state.generator1_contactor_state)
		{
			if (gp_state.generator1_contactor_state == ONLINE)
				LogEvent(Ev_GEN1_ONLINE,0);
			else
				LogEvent(Ev_GEN1_OFFLINE,0);
		}

		if (gp_state.generator2_contactor_state != old_state.generator2_contactor_state)
		{
			if (gp_state.generator2_contactor_state == ONLINE)
				LogEvent(Ev_GEN2_ONLINE,0);
			else
				LogEvent(Ev_GEN2_OFFLINE,0);
		}
	}

	if (MULTIGEN_OPTION)
	{
		gp_state.busState=get_busState();

		if (gp_state.busState != old_state.busState)
		{
			if ((MULTIGEN_OPTION) && ((gp_state.busState == BsGEN1) ||(gp_state.busState == BsGEN2))) {
				temp = get_gen_id();
				
				if (temp > 2)
					temp += 2;
					
				LogEvent(Ev_BUSSTATE,temp);
			}
			else	
				LogEvent(Ev_BUSSTATE,(unsigned char)gp_state.busState);
		}
	}

	if (gp_state.inv2stat != old_state.inv2stat)
	{
		if (gp_state.inv2stat == ONLINE)
			LogEvent(Ev_OTHER_AC_ONLINE,0);
		else
			LogEvent(Ev_OTHER_AC_OFFLINE,0);
	}

	if (gp_state.invstat != old_state.invstat)
	{
		if (gp_state.invstat == ONLINE)
			LogEvent(Ev_AC_ONLINE,0);
		else
			LogEvent(Ev_AC_OFFLINE,0);
	}

	if (AGC_state != old_AGC_state)
	{
		if (AGC_state)
			LogEvent(Ev_AGC_ON,0);
		else
			LogEvent(Ev_AGC_OFF,0);
	}

//	if (mach.pwm2_dutyCycle != old_mach.pwm2_dutyCycle)
//	{
//		LogEvent(Ev_PGM_PWM2_NOM,0);
//	}
//
//	if (mach.xfr_dutyCycle != old_mach.xfr_dutyCycle)
//	{
//		LogEvent(Ev_PGM_PWM2_XFR,0);
//	}

	if (NEW_G2G_OPTION)
	{
		if (manual_mode())	//check if MANUAL mode.
		{
			transferMode = MANUAL;
		}
		else if ((gp_state.inpstat1!=ONLINE) || (gp_Vab[SP1] < 90.0))	//bail with msg if no shore power (voltage).
		{
			transferMode = RESTRICTED;
		}
		else
		{
			transferMode = AUTOMATIC;
		}
	}
	else
	{
		transferMode = AUTOMATIC;
	}
#ifdef GEN2GEN_CONTROL_OPTION_TEST
	if (GEN2GEN_OPTION)
	{
		transferMode = AUTOMATIC;
	}
//FOR DEBUG ONLY!!
#endif //GEN2GEN_CONTROL_OPTION_TEST

	if (NEW_G2G_OPTION)
	{
		if (transferMode != oldTransferMode)
		{
			LogEvent(Ev_XFR_MODE,(unsigned char)transferMode);
		}
	}
#ifndef WESTPORT50
	if (TIE_CONF_PRESENT || MULTIGEN_BESECKE)
#endif //WESTPORT50
	{
		gp_state.tieBreaker_CB_state = tieBreaker_CLOSED();	//TIE AUX Confirmation signal.

		if (gp_state.tieBreaker_CB_state != old_state.tieBreaker_CB_state)
		{
			if (gp_state.tieBreaker_CB_state)
			{
				LogEvent(Ev_TIE_BREAKER_CLOSE,0);
			}
			else
			{
				LogEvent(Ev_TIE_BREAKER_OPEN,0);
			}
		}
	}

	if (AEM_OPTION)
	{
		if (gp_state.aemGenMatchCmd != old_state.aemGenMatchCmd)
		{
			if (gp_state.aemGenMatchCmd)
			{
				LogEvent(Ev_GEN_MATCH_CMD,1);
			}
			else
			{
				LogEvent(Ev_GEN_MATCH_CMD,2);
			}
		}

		if (GP_CONTROL_WORD & 0x0080) //if EXT_OUT_OFF_CMD == HIGH
		{	//the first part of this command is in int10ms() interrupt.
			delay(10);										//delay 10ms.	
			GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFF7F;	//clear GEN_1_CB_CLOSE in image.
			gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
		}
	}

// =========================================================================================
	if (!SEAMLESS_TRANSFER || HYBRID_STO)
	{
		if (!transfer_in_progress && HIGH_Z)
		{
			if (gp_status_word3 & 0x0010)	// K4_CLOSE_CMD
			{
				if (_getbit(P7,7) || _getbit(P7,6))	//if GEN#1 or GEN#2 online.
				{
					init_PWM2(9,0,mach.xfr_dutyCycle);				//unsigned(bits,align,duty)
					
					LogEvent(Ev_HIGH_Z,1);	//HIGH
//					debug = 7;
					Xfer_Imped_Ramp_Status.bits.Imped_Comp = 0;
					Xfer_Imped_Ramp_Status.bits.CB_Open_Confirm_Wait = 0;
				}
				else if ((Active_DutyCycle != mach.pwm2_dutyCycle) && (!Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down))
				{
//					init_PWM2(mach.pwm2_bits,mach.pwm2_align,mach.pwm2_dutyCycle);		//unsigned(bits,align,duty)
					Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;			//Reset the Impedance, it is in 10msec loop	
				}
			}
//			else
			else if ((Active_DutyCycle != mach.pwm2_dutyCycle) && (!Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down))
			{
//				init_PWM2(mach.pwm2_bits,mach.pwm2_align,mach.pwm2_dutyCycle);		//unsigned(bits,align,duty)
				Xfer_Imped_Ramp_Status.bits.Activate_Imped_Ramp_Down = 1;			//Reset the Impedance, it is in 10msec loop	
			}
		}
	}
// =========================================================================================
#ifdef EXT_CB_09102015
	if (gp_state.local_master == MASTER) {
		if (gp_state.invstat==ONLINE) {
			if (monitorExtCb &&(!EXTERNAL_CB_CLOSED()))
	//		if (!EXTERNAL_CB_CLOSED())
			{
				gp_diagnostic_code=-241;
				set_sysstat(WARNING);
			}
			else if (gp_diagnostic_code==-241) {
				set_sysstat(OK);
			}
		}
		else if (gp_diagnostic_code==-241) {
			set_sysstat(OK);
		}
	}
#endif

//	if (MODEL_LC)
//	{
//		updateLEDs();
//	}
#endif //~MM3

	return gp_state.sysstat;		
}
/*---------------------------------------------------------------------------*/
void updateLEDs()
{
  int i;

//RED LED's
	if (gp_state.hvdc_fault)				//HVDC FAULT
		_putbit(1,P2,2);
	else
		_putbit(0,P2,2);

	if (gp_state.system_overtemperature)	//OVERTEMP
		_putbit(1,P2,3);
	else
		_putbit(0,P2,3);

	if (gp_state.input_overcurrent)			//SHORE POWER OVERLOAD
		_putbit(1,P2,4);
	else
		_putbit(0,P2,4);

	if (system_level > 100.0)				//CONVERTER POWER OVERLOAD
		_putbit(1,P2,5);
	else
		_putbit(0,P2,5);

//	if ()									//RESERVED
//		_putbit(1,P2,6);
//	else
		_putbit(0,P2,6);

	if (gp_state.sysstat)					//OTHER
		_putbit(1,P2,7);
	else
		_putbit(0,P2,7);

//GREEN LED's
	if (gp_state.input1_form == THREE_PHASE)	//SHORE POWER NORMAL
		_putbit(1,P4,4);
	else
		_putbit(0,P4,4);

	if (gp_status_word & 0x0001) 	// LVDC_OK == LVDC NORMAL
		_putbit(1,P4,5);
	else
		_putbit(0,P4,5);

	if (!(gp_status_word & 0x1000))			//INV A TEST not NORMAL
		_putbit(0,P4,6);
	else if (!(gp_status_word2 & 0x1000))	//INV B TEST not NORMAL
		_putbit(0,P4,6);
	else if (!(gp_status_word3 & 0x1000))	//INV C TEST not NORMAL
		_putbit(0,P4,6);
	else
		_putbit(1,P4,6);

	if (gp_status_word & 0x0020)		// INV_ENABLE
		_putbit(1,P4,7);
	else
		_putbit(0,P4,7);

	if (gp_state.inpstat1 == ONLINE)	// CONVERTER POWER NORMAL
		_putbit(1,P2,0);
	else
		_putbit(0,P2,0);

	if (autorestart)
		_putbit(1,P2,1);				//RESTART ENABLED
	else
		_putbit(0,P2,1);

	if (special_function)
	{
		if (gp_state.inpstat1==OFFLINE && !shutoffId) shutoffId=4;

		_putbit(1,P2,6);
		delay(1000);
		_putbit(0,P2,6);
		delay(2000);

		for (i=0;i<shutoffId;i++)
		{
			//RED RESERVED LED
			_putbit(1,P2,6);
			delay(200);
			_putbit(0,P2,6);
			delay(200);
		}
		special_function=NO;
		new_key=0;
	}
}
/*---------------------------------------------------------------------------*/
void ledLightShow()
{
  int i;
  unsigned delayTime=200;

	for (i=0;i<3;i++)
	{
	//GREEN LED's
	//SHORE POWER NORMAL
		_putbit(1,P4,4);
		delay(delayTime);
		_putbit(0,P4,4);

	//LVDC NORMAL
		_putbit(1,P4,5);
		delay(delayTime);
		_putbit(0,P4,5);

	//INV A TEST not NORMAL
		_putbit(1,P4,6);
		delay(delayTime);
		_putbit(0,P4,6);

	// INV_ENABLE
		_putbit(1,P4,7);
		delay(delayTime);
		_putbit(0,P4,7);

	// CONVERTER POWER NORMAL
		_putbit(1,P2,0);
		delay(delayTime);
		_putbit(0,P2,0);

	//RESTART ENABLED
		_putbit(1,P2,1);				
		delay(delayTime);
		_putbit(0,P2,1);

	//RED LED's
	//HVDC FAULT
		_putbit(1,P2,2);
		delay(delayTime);
		_putbit(0,P2,2);

	//OVERTEMP
		_putbit(1,P2,3);
		delay(delayTime);
		_putbit(0,P2,3);

	//SHORE POWER OVERLOAD
		_putbit(1,P2,4);
		delay(delayTime);
		_putbit(0,P2,4);

	//CONVERTER POWER OVERLOAD
		_putbit(1,P2,5);
		delay(delayTime);
		_putbit(0,P2,5);

	//RESERVED
		_putbit(1,P2,6);
		delay(delayTime);
		_putbit(0,P2,6);

	//OTHER
		_putbit(1,P2,7);
		delay(delayTime);
		_putbit(0,P2,7);
	}
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
unsigned manual_mode()
{
//	if (OPTCON_PCB)	return ((optcon_status & 0x0100)==0x0100);

	if (NEW_G2G_OPTION || MULTIGEN_BESECKE)	return ((optcon_status & 0x0100)==0x0100);
	else return NO;
}
/*---------------------------------------------------------------------------*/
int set_sysstat(unsigned new_sysstat)
{
	switch (new_sysstat)
	{
		case 0: //OK

				if (gp_state.sysstat != OK)
				{
					LogEvent(Ev_SYSTAT_OK,0);	
				}

				_putbit(0,P3,1); 	//ALARM=OK=ALARM OFF.

				OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xFFFE; 	//clear SUMMARY ALARM in image.
				optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.

//				if (MOD1030_OPTION && MULTIGEN_OPTION)	//SHOULD BE BASED ON OPT-CON PCB PRESENT?
//				if (MOD1030_OPTION)						//SHOULD BE BASED ON OPT-CON PCB PRESENT?
//				{
//					_putbit (0, P3, 15);		//Clear PRE-ALARM
					_putbit (0, P3, 8);		//Clear PRE-ALARM
					
					OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xEFFF; 	//clear OT WARNING ALARM (mod1030) in image.
					optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
//				}

				clear_CORD_ALARM();				//clear OT_WARNING.
				clear_SYSSTAT_ALARM();		//K3 on Seamless Transfer PCB signal low.

				gp_state.shutdown = 0;
				gp_state.input_overcurrent=0;			// {OK,NOT_OK}.	//NOT_OK==INPUT_OVERCURRENT.
				gp_state.hardware_current_limit=0;		// {OK,NOT_OK}.	//NOT_OK==CURRENT_LIMITING.
				gp_state.pfc_fault=0;					// {OK,NOT_OK}.	//NOT_OK==PFC_FAULT.
				gp_state.lvdc_fault=0;					// {OK,NOT_OK}.	//NOT_OK==LVDC_FAULT.
				gp_state.hvdc_fault=0;					// {OK,NOT_OK}.	//NOT_OK==HVDC>210.
				gp_state.inverter_fault=0;				// {OK,NOT_OK}.	//NOT_OK==INVERTER_FAULT.
				gp_state.neg_hvdc_overvoltage=0;		// {OK,NOT_OK}.	//NOT_OK==NEGATIVE_HVDC_OVERVOLTAGE.
				gp_state.pos_hvdc_overvoltage=0;		// {OK,NOT_OK}.	//NOT_OK==POSITIVE_HVDC_OVERVOLTAGE.
				gp_state.system_overtemperature=0;		// {OK,NOT_OK}.	//NOT_OK==SYSTEM_OVERTEMPERATURE.

				active_status_code = OK;
				gp_diagnostic_code = OK;
				gp_state.gp_diagnostic_code	= OK;		// -n < gp_diagnostic_code < -m
				
				last_failure_code_logged = 0;

				if (gp_state.sysstat!=OK) display_type = SYSTEM_STATUS; 

//				retryCount=0;
				break;

		case 1://WARNING

				if (gp_state.sysstat != WARNING)
				{
					LogEvent(Ev_WARNING_ALARM,0);	
					display_type = HELP; 
				}
				

				if (gp_diagnostic_code==(-268) || gp_diagnostic_code==(-269) || gp_diagnostic_code==(-266) || gp_diagnostic_code==(-263))
				{
//					if (MOD1030_OPTION && MULTIGEN_OPTION)	//SHOULD BE BASED ON OPT-CON PCB PRESENT?
//					if (MOD1030_OPTION)						//SHOULD BE BASED ON OPT-CON PCB PRESENT?
//					{
//						if (gp_diagnostic_code==(-269))
							OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x1000; 	//set OT WARNING ALARM (mod1030) in image.
							optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
							
//							_putbit (1, P3, 15);		//Set PRE-ALARM
							_putbit (1, P3, 8);			//Set PRE-ALARM
//						}
//					}
//					else
//					{
//						set_CORD_ALARM();
//					}
				}

				set_SYSSTAT_ALARM();		//K3 on Seamless Transfer PCB signal high.
				break;

		case 2://FAILURE
				if (gp_state.sysstat != FAILURE)
				{
					LogEvent(Ev_FAILURE_ALARM,0);	
				
//					if ((EXT_CB_OPTION || MODEL_LC) && (!_getbit(P3,1))) {
					if (EXT_CB_OPTION && (!_getbit(P3,1))) {
						disable_output();	//level control "REM_OUT_OFF" high=enable output low=disable output.
					}

					_putbit(1,P3,1); 	//ALARM=ALARM ON.

					OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0001; 	//set SUMMARY ALARM in image.
					optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.
					
					display_type = HELP; 
				}
						
				set_SYSSTAT_ALARM();		//K3 on Seamless Transfer PCB signal high.
				break;

		default://undefined sysstat requested
				return -1;
	}
	gp_state.sysstat=new_sysstat;
	return 0;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
//	virtualConfig(S2,3,1);
//uses:
//	makeBit(&VIRTUALconfiguration_word,13,1);	//set S2-3 virtual config bit.
/*---------------------------------------------------------------------------*/
void makeBit(unsigned *word, unsigned bitNum, unsigned bitValue)	//word bit to value.
{
//	unsigned setMask=0x0001;	//logical OR
//	unsigned clearMask=0xFFFE;	//logical AND

	if (bitValue)	//then set
	{
		*word = *word | _rol(0x0001,bitNum);	//logical OR w/setMask.
	}
	else		//else clear
	{
		*word = *word & _rol(0xFFFE,bitNum);	//logical AND w/clearMask.
	}
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
int sys_config(unsigned gp_series)
{
	
	if (gp_series==2)
	{
#ifndef MM3
		VIRTUALconfiguration_word	=gp_configuration_word;		// VIRTUAL CONFIGURATION WORD.

		GEN2GEN_OPTION = 0;		//S3-4 OBSOLETE

		config_extended_options();

		GP_OUTPUT_FORM		= config_output_form();	//note: FORM must be configured before VOLTAGE!
		GP_OUTPUT_VOLTAGE	= config_output_voltage();
		Set_Output_Voltage = GP_OUTPUT_VOLTAGE;		//06162015 DHK
		GP_OUTPUT_FREQUENCY	= config_output_frequency();
		GP_XFMR_RATIO		= config_xfmr_ratio();
		GP_OUTPUT_COUPLING	= config_output_coupling();
#ifdef DUAL_SHORE_CORD
		DUAL_INPUT			= TRUE;
#else
		DUAL_INPUT			= FALSE;
#endif //DUAL_SHORE_CORD
		SEAMLESS_TRANSFER	= config_seamless_transfer_option();
		PARALLELING			= config_paralleling_option();
		RATED_POWER			= config_rated_power();
		TECHNEL_OPTION		= config_technel_option();
		AEM_OPTION			= config_aem_option();
		DUAL_GENSET			= config_dual_genset_option();
		HIGH_Z				= config_high_z();
		HYBRID_STO			= (HIGH_Z && SEAMLESS_TRANSFER);

		NEW_G2G_OPTION 		= FALSE;	//software feature switch. DHK 05242013 - Added

		SWITCHGEAR_INTERFACE_OPTION = config_new_technel_option();

		REMOTE_RUN = config_remote_run_option();		//S2-4 RUN(low) input M1041
				
		gp_cid				= make_gps_model_number();
		
		strcpy(gp_state.gp_cid, gp_cid);

//SP-OFF key
		if (gp_status_word & 0x8000) 	//Rev.D Control Logic PCB
		{
			shore_power_off_key = TRUE;
		}
		else if (gp_configuration_word & 0x1000)	//OBSOLETE
		{
			shore_power_off_key = TRUE;
		}
		else
		{
			shore_power_off_key = FALSE;	//v1.99
		}
#else	//MM3--DMC, Dual Master Controller Configuration

		configDualMasterController();

#endif //MM3
		
		if (HYBRID_STO)
		{
//			HIGH_Z 			= TRUE;
//			SEAMLESS_TRANSFER	= TRUE;
			DUAL_GENSET 		= FALSE;
			MULTIGEN_OPTION 	= FALSE;	//S3-5
			MULTIGEN_INHIBIT 	= FALSE;	//S3-6 OBSOLETE
			SWITCHGEAR_INTERFACE_OPTION = FALSE;
			AEM_OPTION			= FALSE;	//S2-2
//			REMOTE_RUN			= FALSE;		//Commented Out 05252016
			AEM_OPTION				= FALSE;
			AEM2_OPTION				= FALSE;
			SIX_CONTACTOR_CONFIG 	= FALSE;
			GEN2GEN_OPTION 			= FALSE;
//			NEW_G2G_OPTION 			= FALSE;
		    EXT_OSC_OPTION 			= FALSE;		//S3-6 V2.55
			PARALLELING			= FALSE;
			TIE_CONF_PRESENT	= FALSE;	//S3-7 Master only
			TECHNEL_OPTION		= FALSE;	//S2-1
			GEN_START_OPTION 	= FALSE;		//S3-4
		}


#ifdef TEST_FIXTURE		//FORCE CONFIGURATION
		HIGH_Z	 			= 0;
		REMOTE_RUN			= 0;		//S2-4 RUN(low) input M1041
		TECHNEL_OPTION		= 0;	//S2-1
		AEM_OPTION			= 0;	//S2-2
//		s3s4Present			= 1;	//S2-3 OBSOLETE
		shore_power_off_key = 0;	//S2-4 OBSOLETE
		DUAL_GENSET			= 1;	//S2-5
		PARALLELING			= 0;	//S2-6
		SEAMLESS_TRANSFER 	= 1;	//S2-7
//		config_no_crossover_delay();	//S2-8 OBSOLETE
		SWITCHGEAR_INTERFACE_OPTION		= 0;	//S2-8
		EXT_CB_OPTION 		= 0;	//S3-1
//		debug3 = 1;
		DELTA_OUTPUT 		= 0;	//S3-2
		EXT_XFMR_OPTION 	= 0;	//S3-3
		GEN2GEN_OPTION 		= 0;	//S3-4 OBSOLETE, Is:GEN_START_OPTION (Westport)
		MULTIGEN_OPTION 	= 0;	//S3-5
		MULTIGEN_INHIBIT 	= 1;	//S3-6 OBSOLETE
		CORDMAN_OPTION 		= 1;	//S3-7 Slave only
		TIE_CONF_PRESENT	= 1;	//S3-7 Master only
//		NEW_G2G_OPTION 		= 0;	//S3-8
		MOD1030_OPTION 		= 0;	//S4-1
		AEM2_OPTION 		= 0;	//S4-2
		REMOTE_PANEL 		= 0;	//S4-3
		SIX_CONTACTOR_CONFIG = 0;	//S4-4
		MODIFIED_VOUT 		= 0;	//S4-5
		GEN_AMPS_PRESENT 	= 0;	//S4-6
		AC75_CONTROL_PCB 	= 0;	//S4-7	Used in Model 'L' Series & AC75, or bigger, units.
//		MODEL_LC 			= 1;	//S4-8
		MODEL_LC 			= 0;	//S4-8
#endif //TEST_FIXTURE

		if (EXT_OSC_OPTION)
		{
//			TECHNEL_OPTION 			= FALSE;	//MUTUALLY EXCLUSIVE.	12012014 DHK; 
			AEM_OPTION 				= FALSE;
			AEM2_OPTION				= FALSE;
			GEN2GEN_OPTION 			= FALSE;	//software feature switch.
//			NEW_G2G_OPTION 			= FALSE;	//software feature switch.
			SIX_CONTACTOR_CONFIG 	= FALSE;
			GEN_START_OPTION 		= FALSE;		//S3-4
			SWITCHGEAR_INTERFACE_OPTION	= FALSE;	//S2-8
			SEAMLESS_TRANSFER 		= FALSE;
			DUAL_GENSET 			= FALSE;
			MULTIGEN_OPTION 		= FALSE;
			HYBRID_STO			= FALSE;
		}

		if (AEM2_OPTION)	//AEM2 option does all the same except...see aem_sync2gen() in ac.c
		{
			AEM_OPTION = TRUE;
//			TECHNEL_OPTION = FALSE;	//MUTUALLY EXCLUSIVE.	12012014 DHK
			HYBRID_STO			= FALSE;
		}

//wip
		if (SWITCHGEAR_INTERFACE_OPTION)
		{
			MULTIGEN_OPTION = TRUE;
//			TECHNEL_OPTION = TRUE;	
			HYBRID_STO			= FALSE;
//			SWITCHGEAR_INTERFACE_OPTION = FALSE;
		}

		if (MULTIGEN_OPTION)
		{
			SEAMLESS_TRANSFER = TRUE;
			DUAL_GENSET = YES;
			HYBRID_STO			= FALSE;
		}

//		if (TECHNEL_OPTION && !SWITCHGEAR_INTERFACE_OPTION)
//		{
//			TIP_OPTION = FALSE;
////			GEN_START_OPTION = TRUE;
//		}
//		else
//		{
			TIP_OPTION = TRUE;
//		}

		MULTIGEN_INHIBIT = TRUE;	//WAS: S3-6,  IS:Default

		if (TIE_CONF_PRESENT || TECHNEL_OPTION || AEM_OPTION || AEM2_OPTION || REMOTE_RUN || SWITCHGEAR_INTERFACE_OPTION || HYBRID_STO)
//		if (TIE_CONF_PRESENT || TECHNEL_OPTION || AEM_OPTION || AEM2_OPTION || REMOTE_RUN || MULTIGEN_OPTION || HYBRID_STO)
		{
			MULTIGEN_INHIBIT = FALSE;
		}


#ifdef MM3
	shore_power_off_key = FALSE;	//v1.99
		SEAMLESS_TRANSFER = FALSE;
		TIP_OPTION = FALSE;
		MULTIGEN_INHIBIT = FALSE;	//S3-6
#else
		if (EXT_XFMR_OPTION)
		{
			OUTPUT_RATED_AMPS = OUTPUT_RATED_AMPS / XFMR_ratio;
			GP_OUTPUT_COUPLING	= TRANSFORMER;
			GP_XFMR_RATIO		= 1.154;
			XFMR_ratio = GP_XFMR_RATIO;
			volts2volts = VEXT_VOLTS * XFMR_ratio;
			amps2volts = AMPS_VOLTS / XFMR_ratio;
			GP_OUTPUT_VOLTAGE = 138.5 * gp_state.vout_range;	//force 277Vac output;
		}

		if (NEW_G2G_OPTION)
		{
			SEAMLESS_TRANSFER = TRUE;
			DUAL_GENSET = YES;
			EXT_CB_OPTION = TRUE;
			GEN2GEN_OPTION = TRUE;		//software feature switch.
			MULTIGEN_OPTION = FALSE;	//software feature switch.
			makeBit(&VIRTUALconfiguration_word2,3,0);	//virtual S3-5 MULTIGEN_OPTION
			TIP_OPTION = FALSE;
			TECHNEL_OPTION = FALSE;
			SWITCHGEAR_INTERFACE_OPTION	= FALSE;	//S2-8
			HYBRID_STO			= FALSE;
		}

		if (GEN2GEN_OPTION)
		{
			SEAMLESS_TRANSFER = TRUE;
			DUAL_GENSET = YES;
			EXT_CB_OPTION = TRUE;
			MULTIGEN_OPTION = FALSE;	//software feature switch.
			TIP_OPTION = FALSE;
			TECHNEL_OPTION = FALSE;
			SWITCHGEAR_INTERFACE_OPTION	= FALSE;	//S2-8
			HYBRID_STO			= FALSE;
		}
		
		if (SIX_CONTACTOR_CONFIG)	//S4-4
		{
			DUAL_GENSET = YES;
			MULTIGEN_OPTION = FALSE;	//software feature switch.
			TECHNEL_OPTION = FALSE;	
			GEN2GEN_OPTION = FALSE;		//software feature switch.
//			NEW_G2G_OPTION = FALSE;		//software feature switch.
			AEM_OPTION			= FALSE;
			AEM2_OPTION			= FALSE;
			EXT_CB_OPTION = TRUE;
			TIP_OPTION = TRUE;
			SWITCHGEAR_INTERFACE_OPTION	= FALSE;	//S2-8
			HYBRID_STO			= FALSE;
		}
				

		if (REMOTE_PANEL)	//Westport	//S4-3
		{
//			TECHNEL_OPTION 			= FALSE;	//MUTUALLY EXCLUSIVE.	12012014 DHK
			AEM_OPTION 				= FALSE;
			AEM2_OPTION				= FALSE;
			SIX_CONTACTOR_CONFIG 	= FALSE;
			GEN2GEN_OPTION 			= FALSE;	//software feature switch.
//			NEW_G2G_OPTION 			= FALSE;	//software feature switch.
			TIP_OPTION 				= TRUE;
//			SWITCHGEAR_INTERFACE_OPTION	= FALSE;	//S2-8
//			MOD1030_OPTION 			= FALSE;	//S4-1
//			MULTIGEN_OPTION 		= FALSE;	//must allow for Perini Hull 2099

#ifdef DHK_03312015
			if (GEN_START_OPTION)		//S3-4
			{
				if (!OPTCON_PCB) {
					EXT_CB_OPTION = FALSE;
//					debug3 = 2;
				}
			}
#endif
			
//			HYBRID_STO			= FALSE;			//Commented out 05242016
		}

#ifdef TEST_FIXTURE
		MULTIGEN_BESECKE = TRUE;
#else
		if (gp_status_word3 & 0x8000)
		{
			MULTIGEN_BESECKE = TRUE;
			AUTO_SHUTDOWN = TRUE;		//M/Y BOUNTY HUNTER feature
			HYBRID_STO			= FALSE;
		}
		else
		{
			MULTIGEN_BESECKE = FALSE;
			AUTO_SHUTDOWN = FALSE;		//M/Y BOUNTY HUNTER feature
		}
#endif //TEST_FIXTURE

		if (MULTIGEN_BESECKE)  //M/Y Bounty Hunter  //gp_status_word3 & 0x8000 (high) to configure.
		{
			SEAMLESS_TRANSFER 		= TRUE;
			DUAL_GENSET 			= TRUE;
			MULTIGEN_OPTION 		= TRUE;
			EXT_CB_OPTION 			= FALSE;
			TIP_OPTION 				= TRUE;
			MULTIGEN_INHIBIT = FALSE;
			TECHNEL_OPTION 			= FALSE;
			AEM_OPTION 				= FALSE;
			AEM2_OPTION				= FALSE;
			SIX_CONTACTOR_CONFIG 	= FALSE;
			GEN2GEN_OPTION 			= FALSE;
//			NEW_G2G_OPTION 			= FALSE;
//			MOD1030_OPTION 			= FALSE;	//S4-1
			GEN_START_OPTION 		= FALSE;		//S3-4

			SWITCHGEAR_INTERFACE_OPTION	= FALSE;	//S2-8

		    EXT_OSC_OPTION = 0;		//S3-6 V2.55
		    TIE_CONF_PRESENT = 0;	//S3-7 Master
//		    CORDMAN_OPTION = 1;		//S3-7 Slave
			HYBRID_STO			= FALSE;
		}//endif (MULTIGEN_BESECKE)
#endif //MM3

		if (TECHNEL_OPTION)
		{
			makeBit(&VIRTUALconfiguration_word,15,1);	//virtual S2-1 TECHNEL_OPTION
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word,15,0);	//virtual S2-1 TECHNEL_OPTION
		}
		if (AEM_OPTION)
		{
			makeBit(&VIRTUALconfiguration_word,14,1);	//virtual S2-2 AEM_OPTION
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word,14,0);	//virtual S2-2 AEM_OPTION
		}
//s3s4Present virtual setting done in config_extended_options()
//		if (xxxx)							// Is:UNUSED
//		{
//			makeBit(&VIRTUALconfiguration_word,13,1);	//virtual S2-3 Was:s3s4Present, Is:UNUSED
//		}
//		else
//		{
//			makeBit(&VIRTUALconfiguration_word,13,0);	//virtual S2-3 Was:s3s4Present, Is:UNUSED
//		}
		if (shore_power_off_key)			// Is:UNUSED
		{
			makeBit(&VIRTUALconfiguration_word,12,1);	//virtual S2-4 Was:shore_power_off_key, Is:UNUSED
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word,12,0);	//virtual S2-4 Was:shore_power_off_key, Is:UNUSED
		}
		if (DUAL_GENSET)
		{
			makeBit(&VIRTUALconfiguration_word,11,1);	//virtual S2-5 DUAL_GENSET
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word,11,0);	//virtual S2-5 DUAL_GENSET
		}
		if (PARALLELING)
		{
			makeBit(&VIRTUALconfiguration_word,10,1);	//virtual S2-6 PARALLELING
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word,10,0);	//virtual S2-6 PARALLELING
		}
		if (SEAMLESS_TRANSFER)
		{
			makeBit(&VIRTUALconfiguration_word,9,1);	//virtual S2-7 SEAMLESS_TRANSFER
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word,9,0);	//virtual S2-7 SEAMLESS_TRANSFER
		}
		if (config_no_crossover_delay())	// Is:UNUSED
		{
			makeBit(&VIRTUALconfiguration_word,8,1);	//virtual S2-8 Was:config_no_crossover_delay, Is:UNUSED
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word,8,0);	//virtual S2-8 Was:config_no_crossover_delay, Is:UNUSED
		}
		if (EXT_CB_OPTION)
		{
			makeBit(&VIRTUALconfiguration_word2,7,1);	//virtual S3-1 EXT_CB_OPTION
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,7,0);	//virtual S3-1 EXT_CB_OPTION
		}
		if (DELTA_OUTPUT)
		{
			makeBit(&VIRTUALconfiguration_word2,6,1);	//virtual S3-2 DELTA_OUTPUT
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,6,0);	//virtual S3-2 DELTA_OUTPUT
		}
		if (EXT_XFMR_OPTION)
		{
			makeBit(&VIRTUALconfiguration_word2,5,1);	//virtual S3-3 EXT_XFMR_OPTION
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,5,0);	//virtual S3-3 EXT_XFMR_OPTION
		}
		if (GEN2GEN_OPTION)				// Is:GEN_START_OPTION (Westport)
		{
			makeBit(&VIRTUALconfiguration_word2,4,1);	//virtual S3-4 Was:GEN2GEN_OPTION, Is:GEN_START_OPTION (Westport)
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,4,0);	//virtual S3-4 Was:GEN2GEN_OPTION, Is:GEN_START_OPTION (Westport)
		}
		if (MULTIGEN_OPTION)
		{
			makeBit(&VIRTUALconfiguration_word2,3,1);	//virtual S3-5 MULTIGEN_OPTION
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,3,0);	//virtual S3-5 MULTIGEN_OPTION
		}
		if (MULTIGEN_INHIBIT)			// Is:UNUSED
		{
			makeBit(&VIRTUALconfiguration_word2,2,1);	//virtual S3-6 Was:MULTIGEN_INHIBIT, Is:UNUSED
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,2,0);	//virtual S3-6 Was:MULTIGEN_INHIBIT, Is:UNUSED
		}
		if (CORDMAN_OPTION)
		{
			makeBit(&VIRTUALconfiguration_word2,1,1);	//virtual S3-7 CORDMAN_OPTION
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,1,0);	//virtual S3-7 CORDMAN_OPTION
		}
//		if (NEW_G2G_OPTION)
		if (LOW_RANGE_INPUT)
		{
			makeBit(&VIRTUALconfiguration_word2,0,1);	//virtual S3-8 LOW_RANGE_INPUT			//NEW_G2G_OPTION
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,0,0);	//virtual S3-8 LOW_RANGE_INPUT			//NEW_G2G_OPTION
		}
//		if (MOD1030_OPTION)
		if (NV_MODEL)
		{
			makeBit(&VIRTUALconfiguration_word2,15,1);	//virtual S4-1 NV_MODEL			//MOD1030_OPTION
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,15,0);	//virtual S4-1 NV_MODEL			//MOD1030_OPTION
		}
		if (AEM2_OPTION)
		{
			makeBit(&VIRTUALconfiguration_word2,14,1);	//virtual S4-2 AEM2_OPTION
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,14,0);	//virtual S4-2 AEM2_OPTION
		}
		if (REMOTE_PANEL)
		{
			makeBit(&VIRTUALconfiguration_word2,13,1);	//virtual S4-3 REMOTE_PANEL
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,13,0);	//virtual S4-3 REMOTE_PANEL
		}
		if (SIX_CONTACTOR_CONFIG)
		{
			makeBit(&VIRTUALconfiguration_word2,12,1);	//virtual S4-4 SIX_CONTACTOR_CONFIG
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,12,0);	//virtual S4-4 SIX_CONTACTOR_CONFIG
		}
		if (MODIFIED_VOUT)
		{
			makeBit(&VIRTUALconfiguration_word2,11,1);	//virtual S4-5 MODIFIED_VOUT
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,11,0);	//virtual S4-5 MODIFIED_VOUT
		}
		if (GEN_AMPS_PRESENT)
		{
			makeBit(&VIRTUALconfiguration_word2,10,1);	//virtual S4-6 GEN_AMPS_PRESENT
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,10,0);	//virtual S4-6 GEN_AMPS_PRESENT
		}
		if (AC75_CONTROL_PCB)
		{
			makeBit(&VIRTUALconfiguration_word2,9,1);	//virtual S4-7 AC75_CONTROL_PCB
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,9,0);	//virtual S4-7 AC75_CONTROL_PCB
		}
		if (MODEL_LC)
		{
			makeBit(&VIRTUALconfiguration_word2,8,1);	//virtual S4-8 MODEL_LC
		}
		else
		{
			makeBit(&VIRTUALconfiguration_word2,8,0);	//virtual S4-8 MODEL_LC
		}

#ifdef TEST_FIXTURE		//FORCE CONFIGURATION
		makeBit(&VIRTUALconfiguration_word,13,1);	//virtual S2-3 Was:s3s4Present, Is:UNUSED
#endif //TEST_FIXTURE
#ifndef MM3
//		if (PARALLELING && CORDMAN_OPTION) GP_OUTPUT_VOLTAGE = 0.0;	//if (slave) no osc output. //06162015 DHK Commented Out
		if (PARALLELING) GP_OUTPUT_VOLTAGE = 0.0;	//if (slave) no osc output.			// 06162015 DHK
#endif //MM3
//		if (PARALLELING) GP_OUTPUT_VOLTAGE = 0.0;	//if (slave) no osc output.

		return 1;
	}
	else
	{
		return 0;
	}
}
/*---------------------------------------------------------------------------*/
void config_extended_options()
{
  int s3s4Present;
//  unsigned int i;
//  unsigned int optcon_status_rd;		// = optcon_status;;


	if (gp_status_word & 0x8000) 	//Rev.D Control Logic PCB
	{
#ifdef TEST_FIXTURE		//FORCE CONFIGURATION
		VIRTUALconfiguration_word2	=0;	// VIRTUAL CONFIGURATION WORD.
#else
		VIRTUALconfiguration_word2	=gp_configuration_word2;	// VIRTUAL CONFIGURATION WORD.
#endif //TEST_FIXTURE
		s3s4Present=TRUE;
		makeBit(&VIRTUALconfiguration_word,13,1);	//virtual S2-3 Was:s3s4Present, Is:UNUSED
	}
	else if ((gp_configuration_word & 0x2000)!=0)	//S2-3
	{
#ifdef TEST_FIXTURE		//FORCE CONFIGURATION
		VIRTUALconfiguration_word2	=0;	// VIRTUAL CONFIGURATION WORD.
#else
		VIRTUALconfiguration_word2	=gp_configuration_word2;	// VIRTUAL CONFIGURATION WORD.
#endif //TEST_FIXTURE
		s3s4Present=TRUE;
		makeBit(&VIRTUALconfiguration_word,13,1);	//virtual S2-3 Was:s3s4Present, Is:UNUSED
	}
	else
	{
		VIRTUALconfiguration_word2	=0;	// VIRTUAL CONFIGURATION WORD.
		s3s4Present=FALSE;
		makeBit(&VIRTUALconfiguration_word,13,0);	//virtual S2-3 Was:s3s4Present, Is:UNUSED
	}

	if (s3s4Present)
	{				
		EXT_CB_OPTION 		= ((gp_configuration_word2 & 0x0080)!=0);	//S3-1
		DELTA_OUTPUT 		= ((gp_configuration_word2 & 0x0040)!=0);	//S3-2
		EXT_XFMR_OPTION 	= ((gp_configuration_word2 & 0x0020)!=0);	//S3-3
//		GEN2GEN_OPTION 		= ((gp_configuration_word2 & 0x0010)!=0);	//S3-4 OBSOLETE
		GEN_START_OPTION	= ((gp_configuration_word2 & 0x0010)!=0);	//S3-4 NEW v2.43

		MULTIGEN_OPTION 	= ((gp_configuration_word2 & 0x0008)!=0);	//S3-5
//		MULTIGEN_INHIBIT 	= ((gp_configuration_word2 & 0x0004)!=0);	//S3-6 OBSOLETE
		EXT_OSC_OPTION	 	= ((gp_configuration_word2 & 0x0004)!=0);	//S3-6 v2.55
		
		CORDMAN_OPTION = 0;		//S3-7 Slave
		TIE_CONF_PRESENT = 0;	//S3-7 Master

		if (config_paralleling_option())	//slave cabinet?
		{
			CORDMAN_OPTION 		= ((gp_configuration_word2 & 0x0002)!=0);	//S3-7 Slave
		}
		else
		{
			TIE_CONF_PRESENT	= ((gp_configuration_word2 & 0x0002)!=0);	//S3-7 Master
		}

//		NEW_G2G_OPTION		= ((gp_configuration_word2 & 0x0001)!=0);	//S3-8
		LOW_RANGE_INPUT		= ((gp_configuration_word2 & 0x0001)!=0);	//S3-8
//		MOD1030_OPTION 		= ((gp_configuration_word2 & 0x8000)!=0);	//S4-1
		NV_MODEL	 		= ((gp_configuration_word2 & 0x8000)!=0);	//S4-1
		AEM2_OPTION 		= ((gp_configuration_word2 & 0x4000)!=0);	//S4-2
		REMOTE_PANEL		= ((gp_configuration_word2 & 0x2000)!=0);	//S4-3
		SIX_CONTACTOR_CONFIG= ((gp_configuration_word2 & 0x1000)!=0);	//S4-4

		MODIFIED_VOUT		= ((gp_configuration_word2 & 0x0800)!=0);	//S4-5
		GEN_AMPS_PRESENT	= ((gp_configuration_word2 & 0x0400)!=0);	//S4-6
		AC75_CONTROL_PCB	= ((gp_configuration_word2 & 0x0200)!=0);	//S4-7
		MODEL_LC 			= ((gp_configuration_word2 & 0x0100)!=0);	//S4-8
	}
	else
	{
		EXT_CB_OPTION = 0;		//S3-1
//		debug3 = 3;
		DELTA_OUTPUT = 0;		//S3-2
		EXT_XFMR_OPTION = 0;	//S3-3
//		GEN2GEN_OPTION = 0;		//S3-4 OBSOLETE
		GEN_START_OPTION = 0;	//S3-4 NEW v2.43

		MULTIGEN_OPTION = 0;	//S3-5
//		MULTIGEN_INHIBIT = 0;	//S3-6 OBSOLETE
		EXT_OSC_OPTION = 0;		//S3-6 V2.55
		CORDMAN_OPTION = 0;		//S3-7 Slave
		TIE_CONF_PRESENT = 0;	//S3-7 Master
//		NEW_G2G_OPTION = 0;		//S3-8
		LOW_RANGE_INPUT = 0;	//S3-8
//		MOD1030_OPTION = 0;		//S4-1
		NV_MODEL = 0;			//S4-1
		AEM2_OPTION = 0;		//S4-2
		REMOTE_PANEL = 0;		//S4-3
		SIX_CONTACTOR_CONFIG = 0;	//S4-4

		MODIFIED_VOUT = 0;		//S4-5
		GEN_AMPS_PRESENT = 0;	//S4-6
		AC75_CONTROL_PCB = 1;	//S4-7	Used in Model 'L' Series & AC75, or bigger, units.
		MODEL_LC = 0;			//S4-8
	}
	
	optcon_status_rd = optcon_status;
	optcon_status_save = optcon_status_rd;
	OPTCON_PCB = (((optcon_status_rd = optcon_status) & 0x8000) == 0x8000);

	
//	OPTCON_PCB = ((optcon_status) & 0x8000 == 0x8000);
	
//	debug = optcon_status;
//	debug2 = OPTCON_PCB;
//	debug3 = optcon_status;

//#ifdef GEN2GEN_CONTROL_OPTION_TEST
//	NEW_G2G_OPTION = TRUE;				//software feature switch.
//#endif  // GEN2GEN_CONTROL_OPTION_ON

#ifdef MM3
	EXT_CB_OPTION = 0;		//S3-1
//	debug3 = 4;
	DELTA_OUTPUT = 0;		//S3-2
	EXT_XFMR_OPTION = 0;	//S3-3
	GEN_START_OPTION = 0;	//S3-4 NEW v2.43

	MULTIGEN_OPTION = 0;	//S3-5
	EXT_OSC_OPTION = 0;		//S3-6
	CORDMAN_OPTION = 0;		//S3-7
//	NEW_G2G_OPTION = 0;		//S3-8
	LOW_RANGE_INPUT = 0;	//S3-8

//	MOD1030_OPTION = 0;		//S4-1
	AEM2_OPTION = 0;		//S4-2
	REMOTE_PANEL = 0;		//S4-3
	SIX_CONTACTOR_CONFIG = 0;	//S4-4

	MODIFIED_VOUT = 0;		//S4-5
	GEN_AMPS_PRESENT = 0;	//S4-6
	AC75_CONTROL_PCB = 1;	//S4-7	Used in Model 'L' Series & AC75, or bigger, units.
	MODEL_LC = 1;			//S4-8
#endif //MM3

	if (MODEL_LC) {				//08082012 DHK: Liquid Cooled
		shutoffStr[12] = "MOISTURE IN COOLANT OIL SHUTDOWN";
		shutoffStr[14] = "OIL LEVEL LOW SHUTDOWN";
		
		EvName[Ev_GROUND_FAULT] = "MOISTURE IN OIL";
		EvName[Ev_SPARE_SW2_15] = "OIL LEVEL LOW";
	}
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
unsigned config_remote_run_option()		//REMOTE_RUN
{
#ifdef MM3
	return FALSE;	//S2-4 RUN(low) input M1041
#else
	return ((gp_configuration_word & 0x1000)!=0);	//S2-4 RUN(low) input M1041
#endif //MM3
}
/*---------------------------------------------------------------------------*/
unsigned config_new_technel_option()	//SWITCHGEAR_INTERFACE_OPTION
{
#ifdef MM3
	return FALSE;		//S2-8
#else
	return ((gp_configuration_word & 0x0100)!=0);	//S2-8
#endif //MM3
}
/*---------------------------------------------------------------------------*/
unsigned config_high_z()
{
	return ((gp_configuration_word & 0x2000)!=0);	//S2-3
}
/*---------------------------------------------------------------------------*/
unsigned config_dual_genset_option()
{
#ifdef MM3
	return FALSE;		//S2-5
#else
	return ((gp_configuration_word & 0x0800)!=0);	//S2-5
#endif //MM3
}
/*---------------------------------------------------------------------------*/
float config_output_voltage()
{
  float voltage;
  unsigned config_bits = (gp_configuration_word & 0x00C0);

	switch (config_bits)
	{				//	THREE-PHASE			TWO-PHASE			ONE-PHASE
		case 0x0000:// 	110/190, 220/380,	110/220, 220/440,	110, 220	VAC
					if (MODIFIED_VOUT)
					{
						voltage = 112.5 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE}
					}
					else
					{
						voltage = 110.0 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE}
					}
					break;

		case 0x0040:// 	120/208, 240/416, 	120/240, 240/480,	120, 240	VAC
					if (MODIFIED_VOUT)
					{	// LowRange=138.5/240, HighRange=277/480 vac output
						voltage = 138.5 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE};
					}
					else
					{
						voltage = 120.0 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE};
					}
					break;

		case 0x0080:// 	115/200, 230/400, 	115/230, 230/460,	115, 230	VAC
					if (MODIFIED_VOUT)							//06032015 DHK Added
						voltage = 132.8 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE}; 230VAC delta (132.8 * 1.732 = 230VAC)
					else
						voltage = 115.0 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE};
					break;

		case 0x00C0:// 	127/220, 254/440,	127/254, 254/508,	127, 254	VAC
					if (MODIFIED_VOUT)
					{	//Bennetti 131/227 vac output
						voltage = 131.05 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE};
					}
					else
					{
						voltage = 127.0 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE};
					}
					break;
	}
//#ifdef MM3
//	voltage = 115.0 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE};
//#endif //MM3
	return voltage;
}
/*---------------------------------------------------------------------------*/
unsigned config_output_form()
{
  unsigned form;
  unsigned config_bits = (gp_configuration_word & 0x0030);

	switch (config_bits)
	{
		case 0x0000:// 1-phase, 220 VAC line-to-line
					form = 1;
					gp_state.vout_range = LOW_RANGE;
					break;

		case 0x0010:// 2-phase, 120/240 VAC
					form = 2;
					gp_state.vout_range = LOW_RANGE;
					break;

		case 0x0020:// 3-phase, 208/240 VAC
					form = 3;
					gp_state.vout_range = LOW_RANGE;
					break;

		case 0x0030:// 3-phase, 380/415 VAC
					form = 3;
					gp_state.vout_range = HIGH_RANGE;
					break;
	}
//#ifdef MM3
//	form = 3;
//	gp_state.vout_range = HIGH_RANGE;
//
//	
//#endif //MM3
	return form;
}
/*---------------------------------------------------------------------------*/
float config_output_frequency()
{
  float frequency;
  unsigned config_bits = (gp_configuration_word & 0x0008);

	switch (config_bits)
	{
		case 0x0000:// 50Hz
					frequency = 50.0;
					break;

		case 0x0008:// 60Hz
					frequency = 60.0;
					break;
 	}
//#ifdef MM3
//	frequency = 50.0;
//#endif //MM3
	return frequency;
}
/*---------------------------------------------------------------------------*/
unsigned config_output_coupling()
{
	return DIRECT;
}
/*---------------------------------------------------------------------------*/
float config_xfmr_ratio()
{
// DIRECT==1
// TRANSFORMER==2

	return 1.0;
}
/*---------------------------------------------------------------------------*/
float config_rated_power()
{
  float rated_power;
  unsigned config_bits = (gp_configuration_word & 0x0007);

	switch (config_bits)
	{
		case 0x0000:		// 50 kVA
					if (AC75_CONTROL_PCB)		//New Model March 2007, v2.88, or better
					{
						rated_power = 50.0;		//AC50		//v2.88
						
						OUTPUT_RATED_AMPS=139.0;				
						IN_RATED_AMPS_LO_ONE=139.0;	
#ifdef INPUT_400V									
						IN_RATED_AMPS_HI_ONE=72.17;			//Changed from 69.5 for 400Vac Input from 416Vac Input			
						IN_RATED_AMPS_LO_TWO=72.17;			//Changed from 69.5 for 400Vac Input from 416Vac Input				
						IN_RATED_AMPS_HI_TWO=36.085;		//Changed from 34.75 for 400Vac Input from 416Vac Input
#else
						IN_RATED_AMPS_HI_ONE=69.5;			//416Vac Input	
						IN_RATED_AMPS_LO_TWO=69.5;				
						IN_RATED_AMPS_HI_TWO=34.75;
#endif						
					}
					else	// 30 kVA
					{
						rated_power = 30.0;
						OUTPUT_RATED_AMPS=83.3;
						IN_RATED_AMPS_LO_ONE=83.3;
#ifdef INPUT_400V															
						IN_RATED_AMPS_HI_ONE=43.3;			//Changed from 41.6 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_LO_TWO=43.3;			//Changed from 41.6 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_HI_TWO=21.65;			//Changed from 20.8 for 400Vac Input from 416Vac Input
#else
						IN_RATED_AMPS_HI_ONE=41.6;
						IN_RATED_AMPS_LO_TWO=41.6;
						IN_RATED_AMPS_HI_TWO=20.8;
#endif						
					}
					break;

		case 0x0001:// 36 kVA
					rated_power = 36.0;
					OUTPUT_RATED_AMPS=100.0;
					IN_RATED_AMPS_LO_ONE=100.0;
#ifdef INPUT_400V															
					IN_RATED_AMPS_HI_ONE=51.96;			//Changed from 50.0 for 400Vac Input from 416Vac Input
					IN_RATED_AMPS_LO_TWO=51.96;			//Changed from 100(?) for 400Vac Input from 416Vac Input
					IN_RATED_AMPS_HI_TWO=25.98;			//Changed from 50(?) for 400Vac Input from 416Vac Input
#else
					IN_RATED_AMPS_HI_ONE=50.0;
					IN_RATED_AMPS_LO_TWO=100.0;
					IN_RATED_AMPS_HI_TWO=50.0;
#endif					
					break;

		case 0x0002:// 45 kVA
					if (AC75_CONTROL_PCB)		//Then Model 'L'
					{
//						rated_power = 150.0;					//40.0;
//						OUTPUT_RATED_AMPS=418.0;
//						IN_RATED_AMPS_LO_ONE=418.0;
//						IN_RATED_AMPS_HI_ONE=209.0;
//						IN_RATED_AMPS_LO_TWO=209.0;
//						IN_RATED_AMPS_HI_TWO=104.5;
						rated_power = 150.0;				//DHK 07302012: AC150
						OUTPUT_RATED_AMPS=417.0;
						IN_RATED_AMPS_LO_ONE=417.0;
#ifdef INPUT_400V																					
						IN_RATED_AMPS_HI_ONE=216.51;		//Changed from 208.5 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_LO_TWO=216.51;		//Changed from 208.5 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_HI_TWO=108.255;		//Changed from 104.25 for 400Vac Input from 416Vac Input
#else
						IN_RATED_AMPS_HI_ONE=208.5;
						IN_RATED_AMPS_LO_TWO=208.5;
						IN_RATED_AMPS_HI_TWO=104.25;
#endif						
					}
					else
					{
						rated_power = 45.0;
						OUTPUT_RATED_AMPS=125.0;
						IN_RATED_AMPS_LO_ONE=125.0;
#ifdef INPUT_400V																					
						IN_RATED_AMPS_HI_ONE=64.95;			//Changed from 62.0 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_LO_TWO=64.95;			//Changed from 62.0 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_HI_TWO=32.475;		//Changed from 61.0 for 400Vac Input from 416Vac Input
#else
						IN_RATED_AMPS_HI_ONE=62.0;
						IN_RATED_AMPS_LO_TWO=62.0;
						IN_RATED_AMPS_HI_TWO=31.0;
#endif						
					}
					break;

		case 0x0003:// 54 kVA
					if (AC75_CONTROL_PCB)		//New Model AC55 upgradeable to AC75
					{
						rated_power = 55.0;			//AC55
						OUTPUT_RATED_AMPS=153.0;
						IN_RATED_AMPS_LO_ONE=153.0;
#ifdef INPUT_400V																					
						IN_RATED_AMPS_HI_ONE=79.39;			//Changed from 76.5 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_LO_TWO=79.39;			//Changed from 76.5 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_HI_TWO=39.695;		//Changed from 38.0 for 400Vac Input from 416Vac Input
#else
						IN_RATED_AMPS_HI_ONE=76.5;
						IN_RATED_AMPS_LO_TWO=76.5;
						IN_RATED_AMPS_HI_TWO=38.0;
#endif						
					}
					else
					{
						rated_power = 54.0;
						OUTPUT_RATED_AMPS=150.0;
						IN_RATED_AMPS_LO_ONE=150.0;
#ifdef INPUT_400V																					
						IN_RATED_AMPS_HI_ONE=77.94;			//Changed from 75.0 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_LO_TWO=77.94;			//Changed from 75.0 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_HI_TWO=38.97;			//Changed from 37.0 for 400Vac Input from 416Vac Input
#else
						IN_RATED_AMPS_HI_ONE=75.0;
						IN_RATED_AMPS_LO_TWO=75.0;
						IN_RATED_AMPS_HI_TWO=37.0;
#endif						
					}
					break;

		case 0x0004:// 25 kVA
					if (AC75_CONTROL_PCB)		//Then Model 'L'
					{
						rated_power = 45.0;
						OUTPUT_RATED_AMPS=125.0;
						IN_RATED_AMPS_LO_ONE=125.0;
#ifdef INPUT_400V																					
						IN_RATED_AMPS_HI_ONE=64.95;			//Changed from 62.0 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_LO_TWO=64.95;			//Changed from 62.0 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_HI_TWO=32.475;		//Changed from 61.0 for 400Vac Input from 416Vac Input
#else
						IN_RATED_AMPS_HI_ONE=62.0;
						IN_RATED_AMPS_LO_TWO=62.0;
						IN_RATED_AMPS_HI_TWO=31.0;
#endif						
					}
					else
					{
#ifdef DUAL_SHORE_CORD
						rated_power = 24.0;
						OUTPUT_RATED_AMPS=70.0;
						IN_RATED_AMPS_LO_ONE=70.0;
#ifdef INPUT_400V																					
						IN_RATED_AMPS_HI_ONE=34.64;			//Changed from 35.0 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_LO_TWO=34.64;			//Changed from 33.33 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_HI_TWO=17.32;			//Changed from 16.67 for 400Vac Input from 416Vac Input
#else
						IN_RATED_AMPS_HI_ONE=35.0;
						IN_RATED_AMPS_LO_TWO=33.33;
						IN_RATED_AMPS_HI_TWO=16.67;
#endif						
#else
						rated_power = 25.0;
						OUTPUT_RATED_AMPS=70.0;
						IN_RATED_AMPS_LO_ONE=70.0;
#ifdef INPUT_400V																					
						IN_RATED_AMPS_HI_ONE=34.64;			//Changed from 35.0 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_LO_TWO=34.64;			//Changed from 33.33 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_HI_TWO=17.32;			//Changed from 16.67 for 400Vac Input from 416Vac Input
#else
						IN_RATED_AMPS_HI_ONE=35.0;
						IN_RATED_AMPS_LO_TWO=33.33;
						IN_RATED_AMPS_HI_TWO=16.67;
#endif						
#endif //~DUAL_SHORE_CORD
					}
					break;

		case 0x0005:// 100 kVA
					if (AC75_CONTROL_PCB)		//New Model April 2007, v2.85, or better
					{
						rated_power = 125.0;				//AC125
						OUTPUT_RATED_AMPS=347.0;			//333.34;	//266.67;
						IN_RATED_AMPS_LO_ONE=347.0;			//362.0 //333.34;	//266.67;
#ifdef INPUT_400V																					
						IN_RATED_AMPS_HI_ONE=180.42;		//181.0 //166.67;	//133.33;
						IN_RATED_AMPS_LO_TWO=180.42;		//181.0 //166.67;	//133.33;
						IN_RATED_AMPS_HI_TWO=90.21;			//90.5	//83.34;	//66.67;
#else
						IN_RATED_AMPS_HI_ONE=181.0;			//166.67;	//133.33;
						IN_RATED_AMPS_LO_TWO=181.0;			//166.67;	//133.33;
						IN_RATED_AMPS_HI_TWO=90.5;			//83.34;	//66.67;
#endif						
					}
					else
					{
						rated_power = 100.0;		//AC100		//OLD #'s (Changed v2.85)
						OUTPUT_RATED_AMPS=278.0;				//266.67;
						IN_RATED_AMPS_LO_ONE=278.0;				//266.67;
#ifdef INPUT_400V																					
						IN_RATED_AMPS_HI_ONE=144.34;				//Changed from 139.0 for 400Vac Input from 416Vac Input	//133.33;
						IN_RATED_AMPS_LO_TWO=144.34;				//Changed from 139.0 for 400Vac Input from 416Vac Input				//133.33;
						IN_RATED_AMPS_HI_TWO=72.17;				//Changed from 69.5 for 400Vac Input from 416Vac Input				//66.67;
#else
						IN_RATED_AMPS_HI_ONE=139.0;				//133.33;
						IN_RATED_AMPS_LO_TWO=139.0;				//133.33;
						IN_RATED_AMPS_HI_TWO=69.5;				//66.67;
#endif						
					}
					break;

		case 0x0006:// 75 kVA
					if (AC75_CONTROL_PCB)		//New Model AC63 upgradeable to AC75
					{
						rated_power = 63.0;			//AC63
						OUTPUT_RATED_AMPS=175.0;
						IN_RATED_AMPS_LO_ONE=175.0;
#ifdef INPUT_400V																					
						IN_RATED_AMPS_HI_ONE=90.93;			//Changed from 87.5 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_LO_TWO=90.93;			//Changed from 87.5 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_HI_TWO=45.465;		//Changed from 43.75 for 400Vac Input from 416Vac Input
#else
						IN_RATED_AMPS_HI_ONE=87.5;
						IN_RATED_AMPS_LO_TWO=87.5;
						IN_RATED_AMPS_HI_TWO=43.75;
#endif						
					}
					else
					{
						rated_power = 75.0;			//AC75		//OLD #'s (Changed v2.85)
						OUTPUT_RATED_AMPS=209.0;				//200.0;
						IN_RATED_AMPS_LO_ONE=209.0;
#ifdef INPUT_400V																					
						IN_RATED_AMPS_HI_ONE=108.25;			//Changed from 104.0 for 400Vac Input from 416Vac Input				//100.0;
						IN_RATED_AMPS_LO_TWO=108.25;			//Changed from 104.5 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_HI_TWO=54.125;			//Changed from 52.0 for 400Vac Input from 416Vac Input
#else
						IN_RATED_AMPS_HI_ONE=104.0;				//100.0;
						IN_RATED_AMPS_LO_TWO=104.0;
						IN_RATED_AMPS_HI_TWO=52.0;
#endif						
					}
					break;

		case 0x0007:// 90 kVA & 165kVA														//200 kVA
					if (AC75_CONTROL_PCB)				//DHK 07302012: AC165				//AC200 New Model May 2009, v3.06, or better
					{
//						rated_power = 200.0;
//						OUTPUT_RATED_AMPS=556.0;
//						IN_RATED_AMPS_LO_ONE=556.0;
//						IN_RATED_AMPS_HI_ONE=278.0;
//						IN_RATED_AMPS_LO_TWO=278.0;
//						IN_RATED_AMPS_HI_TWO=139.0;
						rated_power = 165.0;				//AC165; 07302012 DHK
						OUTPUT_RATED_AMPS=458.0;			//333.34;	//266.67;
						IN_RATED_AMPS_LO_ONE=458.0;			//478.0 //333.34;	//266.67;
						IN_RATED_AMPS_HI_ONE=238.16;			//239.0 //166.67;	//133.33;
						IN_RATED_AMPS_LO_TWO=238.16;			//239.0 //166.67;	//133.33;
						IN_RATED_AMPS_HI_TWO=119.08;			//119.5 //83.34;	//66.67;
					}
					else
					{
						rated_power = 90.0;
						OUTPUT_RATED_AMPS=250.0;
						IN_RATED_AMPS_LO_ONE=250.0;
#ifdef INPUT_400V																					
						IN_RATED_AMPS_HI_ONE=130.0;			//Changed from 125 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_LO_TWO=130.0;			//Changed from 125 for 400Vac Input from 416Vac Input
						IN_RATED_AMPS_HI_TWO=65.0;			//Changed from 62 for 400Vac Input from 416Vac Input
#else
						IN_RATED_AMPS_HI_ONE=125.0;
						IN_RATED_AMPS_LO_TWO=125.0;
						IN_RATED_AMPS_HI_TWO=62.0;
#endif						
					}
					break;
 	}
#ifdef MM3
	rated_power = 55.0;			//AC55
	OUTPUT_RATED_AMPS=153.0;
	IN_RATED_AMPS_LO_ONE=153.0;
#ifdef INPUT_400V																					
	IN_RATED_AMPS_HI_ONE=79.4;			//Changed from 76.5 for 400Vac Input from 416Vac Input
	IN_RATED_AMPS_LO_TWO=79.4;			//Changed from 76.5 for 400Vac Input from 416Vac Input
	IN_RATED_AMPS_HI_TWO=39.7;			//Changed from 38.0 for 400Vac Input from 416Vac Input
#else
	IN_RATED_AMPS_HI_ONE=76.5;
	IN_RATED_AMPS_LO_TWO=76.5;
	IN_RATED_AMPS_HI_TWO=38.0;
#endif	
#endif //MM3

	if ((rated_power >= 75.0) || (AC75_CONTROL_PCB) || (NV_MODEL))
	{
		if (IN_RATED_AMPS_LO_ONE < shoreCordRating)
		{
			shoreCordRating = IN_RATED_AMPS_LO_ONE;		//worst case: 3-phase, Low Range.
		}
	}
	else
	{
		if ((IN_RATED_AMPS_LO_ONE * 1.5) < shoreCordRating)
		{
			shoreCordRating = IN_RATED_AMPS_LO_ONE * 1.5;	//worst case: 1-phase, Low Range.
		}
	}

	if(GP_OUTPUT_FORM==ONE_PHASE)
	{
		OUTPUT_RATED_AMPS = OUTPUT_RATED_AMPS * 1.5; 
	}

	if(GP_OUTPUT_FORM==TWO_PHASE)
	{
		OUTPUT_RATED_AMPS = OUTPUT_RATED_AMPS * 1.5; 
	}

#ifdef MM3
	rated_power *= 2.0;			//AC55 x2
	OUTPUT_RATED_AMPS *= 2.0;
	IN_RATED_AMPS_LO_ONE *= 2.0;
	IN_RATED_AMPS_HI_ONE *= 2.0;
	IN_RATED_AMPS_LO_TWO *= 2.0;
	IN_RATED_AMPS_HI_TWO *= 2.0;
#endif //MM3

	return rated_power;
}
/*---------------------------------------------------------------------------*/
unsigned config_no_crossover_delay()
{
#ifdef MM3
	return TRUE;
#else
	if (gp_status_word & 0x8000) 	//Rev.D Control Logic PCB
	{
		return FALSE;
	}
	else
	{
		return ((gp_configuration_word & 0x0100)!=0);	//S2-8
	}
#endif //MM3
}
/*---------------------------------------------------------------------------*/
unsigned config_seamless_transfer_option()
{
#ifdef MM3
	return FALSE;
#else
	return ((gp_configuration_word & 0x0200)!=0);	//S2-7
#endif //MM3
}
/*---------------------------------------------------------------------------*/
unsigned config_paralleling_option()
{
#ifdef MM3
	return FALSE;
#else
	return ((gp_configuration_word & 0x0400)!=0);	//S2-6
#endif //MM3
}
/*---------------------------------------------------------------------------*/
unsigned config_technel_option()
{
#ifdef MM3
	return FALSE;
#else
	return ((gp_configuration_word & 0x8000)!=0);	//S2-1
#endif //MM3
}
/*---------------------------------------------------------------------------*/
unsigned config_aem_option()
{
#ifdef MM3
	return FALSE;
#else
	return ((gp_configuration_word & 0x4000)!=0);	//S2-2
#endif //MM3
}
/*---------------------------------------------------------------------------*/
char *make_gps_model_number()
{
  char *gps_model_number=configuration;	// "ACxx        ";
  char temp;

	temp = (char)(gp_configuration_word & 0x0007);		// Power Level.

	switch (temp)
	{
		case 0: // 30kVA
				if (AC75_CONTROL_PCB)				//Then Model 'L'
				{
					*(gps_model_number+2) = 0x35;	//"AC50"
					*(gps_model_number+3) = 0x30;
					
					if (NV_MODEL) {
						*(gps_model_number+4) = 0x4E;	//"AC50NV"
						*(gps_model_number+5) = 0x56;	//"AC50NV"
					}					
				}
				else
				{
					*(gps_model_number+2) = 0x33;	//"AC30"
					*(gps_model_number+3) = 0x30;
					
					if (NV_MODEL) {
						*(gps_model_number+4) = 0x4E;	//"AC30N"
					}					
				}
				break;

		case 1: // 36kVA
				*(gps_model_number+2) = 0x33;		//"AC36"
				*(gps_model_number+3) = 0x36;

				if (AC75_CONTROL_PCB) {				//Then Model 'L'
					*(gps_model_number+4) = 0x4C;	//"AC36L"
				}
				else if (NV_MODEL) {
					*(gps_model_number+4) = 0x4E;	//"AC36NV"
					*(gps_model_number+5) = 0x56;	//"AC36NV"
				}					
					
				break;

		case 2: // 45kVA
//				if (AC75_CONTROL_PCB)				//Then Model 'L'
//				{
//					*(gps_model_number+2) = 0x34;
//					*(gps_model_number+3) = 0x30;
//					*(gps_model_number+4) = 0x4C;	//"AC40L"
//				}
				if (AC75_CONTROL_PCB)				//DHK 07302012: AC150
				{
					*(gps_model_number+2) = 0x31;
					*(gps_model_number+3) = 0x35;
					*(gps_model_number+4) = 0x30;
				}
				else
				{
					*(gps_model_number+2) = 0x34;	//"AC45"
					*(gps_model_number+3) = 0x35;
					
					if (NV_MODEL) {
						*(gps_model_number+4) = 0x4E;	//"AC45NV"
						*(gps_model_number+5) = 0x56;	//"AC45NV"
					}					
				}
				break;

		case 3: // 54kVA
				if (AC75_CONTROL_PCB)				//Then Model 'L'
				{
					*(gps_model_number+2) = 0x35;	//"AC55"
					*(gps_model_number+3) = 0x35;
				}
				else
				{
					*(gps_model_number+2) = 0x35;	//"AC54"
					*(gps_model_number+3) = 0x34;
					
					if (NV_MODEL) {
						*(gps_model_number+4) = 0x4E;	//"AC54NV"
						*(gps_model_number+5) = 0x56;	//"AC54NV"
					}					
				}
				break;

		case 4: // 25 kVA
				if (AC75_CONTROL_PCB)				//Then Model 'L'
				{
					*(gps_model_number+2) = 0x34;
					*(gps_model_number+3) = 0x35;
					*(gps_model_number+4) = 0x4C;	//"AC45L"
				}
				else
				{
#ifdef DUAL_SHORE_CORD
					*(gps_model_number+2) = 0x32;
					*(gps_model_number+3) = 0x34;
#else
					*(gps_model_number+2) = 0x32;
					*(gps_model_number+3) = 0x35;
					
					if (NV_MODEL) {
						*(gps_model_number+4) = 0x4E;	//"AC25N"
						*(gps_model_number+5) = 0x56;	//"AC25NV"
					}					
#endif //DUAL_SHORE_CORD
				}
				break;

		case 5: // 100 kVA
				if (AC75_CONTROL_PCB)				//Then Model 'L'
				{
					*(gps_model_number+2) = 0x31;	//"AC125"
					*(gps_model_number+3) = 0x32;
					*(gps_model_number+4) = 0x35;
				}
				else
				{
					*(gps_model_number+2) = 0x31;	//"AC100"
					*(gps_model_number+3) = 0x30;
					*(gps_model_number+4) = 0x30;
				}
				break;

		case 6: // 75kVA
				if (AC75_CONTROL_PCB)				//Then Model 'L'
				{
					*(gps_model_number+2) = 0x36;	//AC63
					*(gps_model_number+3) = 0x33;
				}
				else
				{
					*(gps_model_number+2) = 0x37;	//AC75
					*(gps_model_number+3) = 0x35;
				}
				
				if (NV_MODEL) {
					*(gps_model_number+4) = 0x4E;	//"ACxxN"
					*(gps_model_number+5) = 0x56;	//"ACxxNV"
				}					
				break;

		case 7: // 90kVA
				if (AC75_CONTROL_PCB)
				{
					*(gps_model_number+2) = 0x31;	//"AC165"
					*(gps_model_number+3) = 0x36;
					*(gps_model_number+4) = 0x35;
				}
				else
				{
					*(gps_model_number+2) = 0x39;
					*(gps_model_number+3) = 0x30;
				}
				break;

		default: //undefined--logic error--show:	"AC00"
				*(gps_model_number+2) = 0x30;
				*(gps_model_number+3) = 0x30;
				break;
	}
#ifdef MM3
	*(gps_model_number+2) = 0x35;	//"MM55"
	*(gps_model_number+3) = 0x35;
#endif //MM3

	*(gps_model_number+6) = (char)NULL;

	return gps_model_number;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*
//method 1
void delay1ms()		// delay 1 millisecond --reentrant-- safe for interrupts.
{
 unsigned long t8cnt=(T8+625);	// 625 T8 counts = 1ms. (64911=65536-625)
 unsigned long t8;

	if (t8cnt > 0x10000)
	{
		t8cnt = 625-(0x10000-T8)+59286;

		while(t8cnt != (t8=T8));
	}
	else
	{
		while(t8cnt < (t8=T8));	//wait 625 counts of T8
	}
}
/*---------------------------------------------------------------------------*/
//method 2
void delay1ms()		// delay 1 millisecond --reentrant-- safe for interrupts.
{
 volatile unsigned oldT8=T8;
 volatile unsigned deltaT8=0;
 volatile unsigned newT8;

	while(deltaT8 < 620)	// 625 T8 counts = 1ms, so, 0 to 624  (we start counting at 0 + account for overhead).
	{
		newT8 = T8;

		if (newT8 < oldT8)
		{
			deltaT8 = ((65535 - oldT8) + (newT8-T8REL));	//calculate delta for wrapped timer.
		}
		else
		{
			deltaT8 = newT8 - oldT8;				//calculate delta.
		}
	}
}
/*---------------------------------------------------------------------------*/
void delay(unsigned int ticks)	// number of 1 millisecond ticks to wait
{								// 60000ms max = 1minute
  int i;

	for (i=0;i<ticks;i++)
	{
		delay1ms();

		if (EXT_CB_OPTION && monitor_started && !manual_mode())
		{
//
//respond to CONVERTER POWER button events in COCBO-configured systems.
//
			cocboWatch();
		}
	}
}
/*---------------------------------------------------------------------------*/
void pause(unsigned int ticks)	// number of 1 millisecond ticks to wait
{								// 60000ms max = 1minute
  int i;

	new_key=0;

	for (i=0;i<ticks;i++)
	{
		delay1ms();

		if (EXT_CB_OPTION && monitor_started && !manual_mode())
		{
//
//respond to CONVERTER POWER button events in COCBO-configured systems.
//
			cocboWatch();
		}
		if(new_key==18) break;
	}
}
/*---------------------------------------------------------------------------*/
/*
void delay(unsigned int ticks)	// number of 1millisecond ticks to wait
{								// 3,000 max ticks or 3 seconds
  float ftemp;

	T2 = 0;								// start T2 with 0 counts
	T2CON = 0x47;						// enable timer and count up

	ftemp = ticks * 19.53125;			// scale 1ms to 51.2us counts
	if(ftemp > 60000) ftemp = 60000;	// limit max time

	while( T2 < ftemp)
	{
		if (EXT_CB_OPTION && monitor_started && !manual_mode())
		{
//
//respond to CONVERTER POWER button events in COCBO-configured systems.
//
			cocboWatch();
		}
	}
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
//void delay(unsigned int ticks)	/* number of 1millisecond ticks to wait */
//{								/* 3,000 max ticks or 3 seconds */
//  float ftemp;
//
//	T2 = 0;						/* start T2 with 0 counts */
//	T2CON = 0x47;				/* enable timer and count up */
//
//	ftemp = ticks * 19.53125;	/* scale 1ms to 51.2us counts */
//	if(ftemp > 60000) ftemp = 60000;	/* limit max time */
//
//	while( T2 < ftemp);
////	{
////		if (EXT_CB_OPTION && monitor_started)
////			check_gp_status();	//respond to CONVERTER POWER button events.
////	}
//}
/*---------------------------------------------------------------------------*/
//void pause(unsigned int ticks)	/* number of 1millisecond ticks to wait */
//{								/* 3,000 max ticks or 3 seconds */
//  float ftemp;
//
//	T2 = 0;						/* start T2 with 0 counts */
//	T2CON = 0x47;				/* enable timer and count up */
//
//	ftemp = ticks * 19.53125;	/* scale 1ms to 51.2us counts */
//	if(ftemp > 60000) ftemp = 60000;	/* limit max time */
//
//	new_key=0;
//	while( T2 < ftemp && new_key!=18)
//	{
//		if (EXT_CB_OPTION && monitor_started && !manual_mode())
//		{
////
////respond to CONVERTER POWER button events in COCBO-configured systems.
////
//			cocboWatch();
//		}
//	}
//}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void cocboWatch()
{
	if (!EXT_CB_OPTION) return;		//handle non-cocbo systems in check_status().
//
//respond to CONVERTER POWER button events in COCBO-configured systems.
//
	if (gp_status_word & 0x0040) 	// OUTPUT_ENABLE.
	{
		if (NEW_G2G_OPTION && !maintenanceMode && !tieBreaker_CLOSED() && !transfer_in_progress)	//& OPERATOR PRESSED CONVERTER POWER 'ON' BUTTON.
		{
			if (get_busState2()==BsG1AG2B) abort_power_on=0;
			else abort_power_on = 37;
		}
		else
		{
			if(gp_state.invstat==OFFLINE && remote_off_flag==NO)
			{
				if (MULTIGEN_OPTION) {
//					debug = 31;
					if (!(optcon_status_2 & 0x0F00))
						cocboEnableOutput();	//level control "REM_OUT_OFF" high=enable output low=disable output.
					else
						remote_off_flag=YES;						
				}
				else if ((!gp_state.generator1_contactor_state) && (!gp_state.generator2_contactor_state))	//if neither GEN#1 nor GEN#2 online.
					cocboEnableOutput();	//level control "REM_OUT_OFF" high=enable output low=disable output.
				else
					remote_off_flag=YES;
			}
		}
	}
	else	// OUTPUT_ENABLE(not) => operator pressed OFF.
	{
		if (gp_state.output_contactor_state==ON || remote_off_flag==YES)
		{
			cocboDisableOutput();
			if ((!gp_state.generator1_contactor_state) && (!gp_state.generator2_contactor_state)) remote_off_flag=NO;
		}
	}
//
//respond to loss of shore power events in COCBO-configured systems.
//
	if (!(gp_status_word3 & 0x0080))	// K2_CLOSE_CMD==LOW
	{
		if (gp_state.inpstat1==ONLINE)	
		{
			if (shore_power_off_key) input_dropped=TRUE;		//was ONLINE now (OFFLINE && A-B_OK (not)).
			if (!(gp_status_word & 0x0008))						//AB_OK(not)
			{
				if (shore_power_off_key) 
					cocboDisableOutput();
				else 
					input_dropped=TRUE;						//was ONLINE now (OFFLINE && A-B_OK (not)).
//v2.21 begin
				if (input_dropped && shore_power_off_key)
				{
//					if (!autorestart)	//v2.21
					{
					   if (gp_diagnostic_code!=-30) 	//~INPUT_OVLD  v2.21
						{
							gp_diagnostic_code=-32;		//DOCK POWER BROWNOUT v2.21
							set_sysstat(WARNING);
						}
					}
				}
//v2.21 end
			}
			if (!shore_power_off_key) 
				cocboDisableOutput();
		}
		gp_state.inpstat1=OFFLINE;
		gp_state.inpstat2=OFFLINE;

		if (gp_status_word & 0x0008)							//AB_OK,  this test added v1.93b
		{
			if (autorestart && input_dropped && shore_power_off_key)	//v1.93b
			{
				gp_reset();
			}
		}
	}//endif (!(gp_status_word3 & 0x0080))	// K2_CLOSE_CMD==LOW
}
/*--------------------------------------------------------------------------*/
void cocboEnableOutput()  //level control "REM_OUT_OFF" high=enable output low=disable output.
{
  unsigned long my_time=eventTime;
  unsigned long cbDelay=(external_cb_delay/10)+my_time;	//used for EXT_CB delay, if required.

	if (!EXT_CB_OPTION) return;		//handle non-cocbo systems in check_status().

	if ((transfer_in_progress) || ((!gp_state.generator1_contactor_state) && (!gp_state.generator2_contactor_state)))
	{
		if ((GP_CONTROL_WORD & 2) != 2) {
			LogEvent(Ev_REM_OUT_OFF,1);
		}
		
		GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0002; 	//set REM_OUT_OFF in image.
		gp_control_word = GP_CONTROL_WORD;	   			//write GP_CONTROL_WORD.

		while (eventTime < (my_time+5));	//give K4 time to settle, 50ms in event time.
//
//toggle "EXT_CB_CLOSE" signal high then low.
//shadow with option-control-pcb's CONV_CB_CLOSE signal.
//
		LogEvent(Ev_EXT_CB_CLOSE,0);
		OPTCON_CONTROL = OPTCON_CONTROL | 0x0100; 		//set CONV_CB_CLOSE in image.
		optcon_control = OPTCON_CONTROL;	   			//write OPTCON_CONTROL.
		GP_CONTROL_WORD = GP_CONTROL_WORD | 0x4000; 	//set EXT_CB_CLOSE in image.
		if (SIX_CONTACTOR_CONFIG) GP_CONTROL_WORD = GP_CONTROL_WORD | 0x2000; //set EXT_CB_OPEN in image.
		gp_control_word = GP_CONTROL_WORD;	   			//write GP_CONTROL_WORD.

		while (eventTime < cbDelay);		//(external_cb_delay)ms in event time(10ms units).

		OPTCON_CONTROL = OPTCON_CONTROL & 0xFEFF; 		//clear CONV_CB_CLOSE in image.
		optcon_control = OPTCON_CONTROL;	   			//write OPTCON_CONTROL.
		GP_CONTROL_WORD = GP_CONTROL_WORD & 0xBFFF;		//clear EXT_CB_CLOSE in image.
		if (SIX_CONTACTOR_CONFIG) GP_CONTROL_WORD = GP_CONTROL_WORD & 0xDFFF;	//clear EXT_CB_OPEN in image.
		gp_control_word = GP_CONTROL_WORD;	   			//write GP_CONTROL_WORD.

		if (monitorExtCb &&(!EXTERNAL_CB_CLOSED()))
		{
			remote_off_flag=YES;
			gp_diagnostic_code=-24;
			set_sysstat(WARNING);
		}
		else
		{
			gp_state.output_contactor_state=ON;
			gp_state.invstat=ONLINE;
			gp_state.converter_power_state=ON;
		}
	}
}
/*--------------------------------------------------------------------------*/
void cocboDisableOutput()  //level control "REM_OUT_OFF" high=enable output low=disable output.
{
  unsigned long my_time=eventTime;
  unsigned long cbDelay=(external_cb_Open_delay/10)+my_time;	//used for EXT_CB delay, if required.

	if (!EXT_CB_OPTION) return;		//handle non-cocbo systems in check_status().

	if (gp_state.output_contactor_state==ON)
	{
//
//toggle "EXT_CB_OPEN" signal (PD13) high then low.
//shadow with option-control-pcb's CONV_CB_OPEN signal.
//
		if (!SIX_CONTACTOR_CONFIG)
		{
			LogEvent(Ev_EXT_CB_OPEN,0);
			OPTCON_CONTROL = OPTCON_CONTROL | 0x0200; 	//set CONV_CB_OPEN in image.
			optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
			GP_CONTROL_WORD = GP_CONTROL_WORD | 0x2000; //set EXT_CB_OPEN in image.
			gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.

//			debug = 5;

			while (eventTime < cbDelay);	//(external_cb_Open_delay)ms in event time(10ms units).

			OPTCON_CONTROL = OPTCON_CONTROL & 0xFDFF; 	//clear CONV_CB_OPEN in image.
			optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
			GP_CONTROL_WORD = GP_CONTROL_WORD & 0xDFFF;	//clear EXT_CB_OPEN in image.
			gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.

			if (monitorExtCb &&(EXTERNAL_CB_CLOSED()))
			{
				gp_diagnostic_code=-23;
				set_sysstat(WARNING);
			}
			gp_state.invstat=OFFLINE;
			gp_state.output_contactor_state=OFF;
			gp_state.converter_power_state=OFF;
		}//endif (!SIX_CONTACTOR_CONFIG)
	}

	if ((GP_CONTROL_WORD & 2) == 2)
	{
		LogEvent(Ev_REM_OUT_OFF,2);
	}
	GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFFD;		//clear REM_OUT_OFF in image.
	gp_control_word = GP_CONTROL_WORD;	   			//write GP_CONTROL_WORD.

	if (SIX_CONTACTOR_CONFIG)
	{
		gp_state.invstat=OFFLINE;
		gp_state.output_contactor_state=OFF;
		gp_state.converter_power_state=OFF;
	}

}
/*--------------------------------------------------------------------------*/
void resetExtCB()		//toggle "EXT_CB_OPEN" signal high then low.
{							//shadow with option-control-pcb's CONV_CB_OPEN signal.
	if (!NEW_G2G_OPTION) return;
	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.
	if (SIX_CONTACTOR_CONFIG) return;
	if (EXTERNAL_CB_CLOSED()) return;

	if (resetCbOn)
	{
		if (EXT_CB_OPTION)
		{
			OPTCON_CONTROL = OPTCON_CONTROL | 0x0200; 	//set CONV_CB_OPEN in image.
			optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
			GP_CONTROL_WORD = GP_CONTROL_WORD | 0x2000; //set EXT_CB_OPEN in image.
			gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
			
//			debug = 6;

			delayWithFreqLock(resetDelay,0);  			//default 600ms.
			OPTCON_CONTROL = OPTCON_CONTROL & 0xFDFF; 	//clear CONV_CB_OPEN in image.
			optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
			GP_CONTROL_WORD = GP_CONTROL_WORD & 0xDFFF;	//clear EXT_CB_OPEN in image.
			gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
		}
	}
}
/*--------------------------------------------------------------------------*/
void resetCB4()	//CB4 //toggle "CONV_BUS_A_CB_OPEN" signal high then low.
{
	if (NEW_G2G_OPTION) return;
	if (SIX_CONTACTOR_CONFIG)  return;
	if (cb4_CLOSED()) return;

	if (resetCbOn)
	{
		OPTCON_CONTROL = OPTCON_CONTROL | 0x8000; 	//set CONV_BUS_A_CB_OPEN in image.
		optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
		delayWithFreqLock(resetDelay,0);										//delay 600ms.
		OPTCON_CONTROL = OPTCON_CONTROL & 0x7FFF;		//clear CONV_BUS_A_CB_OPEN in image.
		optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
	}
}
/*--------------------------------------------------------------------------*/
void resetCB5()	//CB5 //toggle "CONV_BUS_B_CB_OPEN" signal high then low.
{
	if (NEW_G2G_OPTION) return;
	if (SIX_CONTACTOR_CONFIG)  return;
	if (cb5_CLOSED()) return;

	if (resetCbOn)
	{
		OPTCON_CONTROL = OPTCON_CONTROL | 0x2000; 	//set CONV_BUS_B_CB_OPEN in image.
		optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
		delayWithFreqLock(resetDelay,0);										//delay 600ms.
		OPTCON_CONTROL = OPTCON_CONTROL & 0xDFFF;		//clear CONV_BUS_B_CB_OPEN in image.
		optcon_control = OPTCON_CONTROL;	   		//write OPTCON_CONTROL.
	}
}
/*--------------------------------------------------------------------------*/
void resetGen2CB()		//toggle "GEN_2_CB_OPEN" signal high then low.
{
	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.
	if (MULTIGEN_OPTION) return;
	if (SIX_CONTACTOR_CONFIG)  return;
	if (_getbit(P7,6))	// GEN #2 CONTACTOR CONFIRM
		return;
//	if (gp_state.genstat2==ONLINE) return;

	if (resetCbOn)
	{
		if (!AEM_OPTION)	
		{
			GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0010; 	//set GEN_2_CB_OPEN in image.
			gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
			delayWithFreqLock(resetDelay,0);			//delay 600ms.
			GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFEF;		//clear GEN_2_CB_OPEN in image.
			gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
		}
	}
}
/*--------------------------------------------------------------------------*/
void resetGen1CB()		//toggle "GEN_1_CB_OPEN" signal high then low.
{
	if (manual_mode()) return;	//check if MANUAL mode--no external CB control if yes.
	if (MULTIGEN_OPTION) return;
	if (SIX_CONTACTOR_CONFIG)  return;
	if (gp_state.generator1_contactor_state)		// GEN #1 CONTACTOR CONFIRM
		return;
//	if (gp_state.genstat1==ONLINE) return;

	if (resetCbOn)
	{
		GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0040; 	//set GEN_1_CB_OPEN in image.
		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
		delayWithFreqLock(resetDelay,0);			//delay 600ms.
		GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFBF;		//clear GEN_1_CB_OPEN in image.
		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
	}
}
/*--------------------------------------------------------------------------*/
void set_SYSSTAT_ALARM()	//K3 on Seamless Transfer PCB signal high.
{								//Note conflict with GEN_2_CB_CLOSE
	if (AEM_OPTION || REMOTE_RUN)	
	{
		GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0020; //set GEN_2_CB_CLOSE in image.
		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
	}
	else if (REMOTE_PANEL)	//Sysstat Alarm out at PD12 (K8 & K10)
	{
		GP_CONTROL_WORD = GP_CONTROL_WORD | 0x1000; 	//set CORD_ALARM in image.
		gp_control_word = GP_CONTROL_WORD;	   	//write GP_CONTROL_WORD to hardware.
	}
}
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
void clear_SYSSTAT_ALARM()	//K3 on Seamless Transfer PCB signal low.
{								//Note conflict with GEN_2_CB_CLOSE
	if (AEM_OPTION || REMOTE_RUN)	
	{
		GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFDF;	//clear GEN_2_CB_CLOSE in image.
		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
	}
	else if (REMOTE_PANEL)	
	{
		GP_CONTROL_WORD = GP_CONTROL_WORD & 0xEFFF; 	//clear CORD_ALARM in image.
		gp_control_word = GP_CONTROL_WORD;	   	//write GP_CONTROL_WORD to hardware.
	}
}
/*--------------------------------------------------------------------------*/
void set_Igen_mux(int Igen_mux)	//switches DELTROL 375 using P44-47,-48 (K42)
{
	delay(20);										//delay 1/50 second.

	switch(Igen_mux)
	{
		case 2://GEN2:	
					OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x2000;	// CLOSED = genset #2.
					break;
		case 0://none:	
		case 1://GEN1:	
		default:
					OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xDFFF;	// OPEN = genset #1.
					break;
	}

	optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL_2.
															 
	delay(40);		//delay 40ms to allow DELTROL 375 output time to settle.
}
/*--------------------------------------------------------------------------*/
int getIgen()	//returns 1 if Igen MUX set to GEN1 and 2 if set to GEN2.
{
	return (((OPTCON_CONTROL_2 & 0x2000) >> 13) + 1);
}
/*--------------------------------------------------------------------------*/
unsigned get_REM_XFR_REQ()	//REMOTE_PANEL.  Returns 1 if REM_XFR_REQ received.
{							//Seamless Xfr PCB TB11-8,-7
  int i;
  volatile unsigned transferRequest=FALSE;

	if (REMOTE_PANEL)	//requires REMOTE_PANEL option (Westport).
	{
		if (_getbit(P5,9))				//if we see request, then
		{
			for (i=0;i<100;i++)			//100ms filter on XFR_REQ
			{
				delay1ms();

				if (!_getbit(P5,9))			//if request goes Low, then
				{
					return transferRequest;	//return FALSE.
				}
			}
			transferRequest = (unsigned)_getbit(P5,9);	//set return value HIGH=TRUE.
		}
	}

	return transferRequest;
}
/*--------------------------------------------------------------------------*/
unsigned get_REM_RUN_REQ()	//REMOTE_RUN.  Returns 1 if REM_RUN_REQ received.
{							//Seamless Xfr PCB TB11-8,-7
  int i;
  volatile unsigned runRequest=FALSE;

	if (REMOTE_RUN)	//requires REMOTE_RUN option, S2-4.
	{
		if (_getbit(P5,9))				//if we see request(active high), then
		{
			for (i=0;i<100;i++)			//100ms filter on XFR_REQ
			{
				delay1ms();

				if (!_getbit(P5,9))			//if request goes Low, then
				{
					return runRequest;	//return FALSE.
				}
			}
			runRequest = (unsigned)_getbit(P5,9);	//set return value HIGH=TRUE.
		}

		if (get_EPO_CONF())		//Returns 1 if REM_EPO_CONF is LOW received.
		{
			runRequest = FALSE;
		}
	}

	return runRequest;
}
/*--------------------------------------------------------------------------*/
unsigned get_EPO_CONF()	//REMOTE_RUN.  Returns 1 if REM_EPO received.
{							//Seamless Xfr PCB TB11-4,-3 (LOW)
  int i;
  volatile unsigned epoConf=FALSE;

	if (REMOTE_RUN)	//requires REMOTE_RUN option, S2-4.
	{
		if (!_getbit(P7,4))				//if we see request(active LOW), then
		{
			for (i=0;i<100;i++)					//100ms filter on REM_EPO
			{
				delay1ms();

				if (_getbit(P7,4))				//if request goes High, then
				{
					return epoConf;				//return FALSE.
				}
			}
			epoConf = (unsigned)!_getbit(P7,4);	//set return value LOW=TRUE.
		}
	}

	return epoConf;
}
/*--------------------------------------------------------------------------*/
//==========================================================================
//	EXT_OUT_OFF_CMD();	//reset OUTPUT_ENABLE via P35-1,-2 50ms pulse.
//	Hardware rqmt:  Add jumper from 604270 TB10-1,-3 to 604172 P35-1,-2
//==========================================================================
void EXT_OUT_OFF_CMD()	//reset OUTPUT_ENABLE, toggle "GEN_1_CB_CLOSE" signal high then low.
{
//	LogEvent(Ev_EXT_OUT_OFF_CMD,0);

	if (AEM_OPTION)
	{
		GP_CONTROL_WORD = GP_CONTROL_WORD | 0x0080; //set GEN_1_CB_CLOSE in image.
		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
		delay(10);										//delay 10ms.	
		GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFF7F;	//clear GEN_1_CB_CLOSE in image.
		gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
	}
}
/*---------------------------------------------------------------------------*/
#ifdef MM3
void configDualMasterController()
{
		GEN2GEN_OPTION = 0;		//S3-4 OBSOLETE

		config_extended_options();

		GP_OUTPUT_FORM		= config_DMC_output_form();	//note: FORM must be configured before VOLTAGE!
		gp_cid				= make_DMC_model_number();
//		RATED_POWER			= config_rated_power();	//done in make_DMC_model_number()
		newVoltage = GP_OUTPUT_VOLTAGE	= config_DMC_output_voltage();
		GP_OUTPUT_FREQUENCY	= config_DMC_output_frequency();
		GP_XFMR_RATIO		= config_xfmr_ratio();
		GP_OUTPUT_COUPLING	= config_output_coupling();

		PARALLELING			= FALSE;
		TECHNEL_OPTION		= FALSE;
		AEM_OPTION			= FALSE;
		DUAL_GENSET			= FALSE;
		HIGH_Z				= FALSE;
		REMOTE_RUN 			= FALSE;
		DUAL_INPUT			= FALSE;
		shore_power_off_key = FALSE;
		SEAMLESS_TRANSFER 	= FALSE;
		TIP_OPTION 			= FALSE;
		MULTIGEN_INHIBIT 	= FALSE;
		SWITCHGEAR_INTERFACE_OPTION = FALSE;
		HYBRID_STO			= FALSE;
}
/*---------------------------------------------------------------------------*/
char *make_DMC_model_number()
{
  char *dmc_model_number=configuration;	// "MMxx        ";
  unsigned temp=0;
  unsigned uNotABitType;
  unsigned HF_model=(unsigned)_getbit(P4,4);	//System Type (Std. or HF)
  float rated_power=0;

	uNotABitType = (unsigned)_getbit(P2,0);		// Power Level.
	temp = (uNotABitType<<3);					// Power Level.
	uNotABitType = (unsigned)_getbit(P4,7);		// Power Level.
	temp = (unsigned)temp|(uNotABitType<<2);	// Power Level.
	uNotABitType = (unsigned)_getbit(P4,6);	// Power Level.
	temp = (unsigned)temp|(uNotABitType<<1);	// Power Level.
	uNotABitType = (unsigned)_getbit(P4,5);	// Power Level.
	temp = (unsigned)temp|uNotABitType;			// Power Level.

//	LogEvent(Ev_ERR,(unsigned char)temp);

	switch (temp)
	{
		case 0: // 25kVA
				*(dmc_model_number+2) = 0x32;	//"MM25"
				*(dmc_model_number+3) = 0x35;
				rated_power = 25.0;
				OUTPUT_RATED_AMPS=70.0;
				IN_RATED_AMPS_LO_ONE=70.0;
				IN_RATED_AMPS_HI_ONE=35.0;
				IN_RATED_AMPS_LO_TWO=33.33;
				IN_RATED_AMPS_HI_TWO=16.67;
				break;
		case 1: // 30kVA
				*(dmc_model_number+2) = 0x33;	//"MM30"
				*(dmc_model_number+3) = 0x30;
				rated_power = 30.0;
				OUTPUT_RATED_AMPS=83.3;
				IN_RATED_AMPS_LO_ONE=83.3;
				IN_RATED_AMPS_HI_ONE=41.6;
				IN_RATED_AMPS_LO_TWO=41.6;
				IN_RATED_AMPS_HI_TWO=20.8;
				break;
		case 2: // 36kVA
				*(dmc_model_number+2) = 0x33;	//"MM36"
				*(dmc_model_number+3) = 0x36;
				rated_power = 36.0;
				OUTPUT_RATED_AMPS=100.0;
				IN_RATED_AMPS_LO_ONE=100.0;
				IN_RATED_AMPS_HI_ONE=50.0;
				IN_RATED_AMPS_LO_TWO=100.0;
				IN_RATED_AMPS_HI_TWO=50.0;
				break;
		case 3: // 40kVA
				*(dmc_model_number+2) = 0x34;	//"MM40"
				*(dmc_model_number+3) = 0x30;
				rated_power = 40.0;
				OUTPUT_RATED_AMPS=111.0;
				IN_RATED_AMPS_LO_ONE=111.0;
				IN_RATED_AMPS_HI_ONE=55.5;
				IN_RATED_AMPS_LO_TWO=55.5;
				IN_RATED_AMPS_HI_TWO=27.7;
				break;
		case 4: // 45kVA
				*(dmc_model_number+2) = 0x34;	//"MM45"
				*(dmc_model_number+3) = 0x35;
				rated_power = 45.0;
				OUTPUT_RATED_AMPS=125.0;
				IN_RATED_AMPS_LO_ONE=125.0;
				IN_RATED_AMPS_HI_ONE=62.0;
				IN_RATED_AMPS_LO_TWO=62.0;
				IN_RATED_AMPS_HI_TWO=31.0;
				break;
		case 5: // 54kVA
				*(dmc_model_number+2) = 0x35;	//"MM54"
				*(dmc_model_number+3) = 0x34;
				rated_power = 54.0;
				OUTPUT_RATED_AMPS=150.0;
				IN_RATED_AMPS_LO_ONE=150.0;
				IN_RATED_AMPS_HI_ONE=75.0;
				IN_RATED_AMPS_LO_TWO=75.0;
				IN_RATED_AMPS_HI_TWO=37.0;
				break;
		case 6: // 55kVA
				*(dmc_model_number+2) = 0x35;	//"MM55"
				*(dmc_model_number+3) = 0x35;
				rated_power = 55.0;			//AC55
				OUTPUT_RATED_AMPS=153.0;
				IN_RATED_AMPS_LO_ONE=153.0;
				IN_RATED_AMPS_HI_ONE=76.5;
				IN_RATED_AMPS_LO_TWO=76.5;
				IN_RATED_AMPS_HI_TWO=38.0;
				break;
		case 7: // 50kVA
				*(dmc_model_number+2) = 0x35;	//"MM50"
				*(dmc_model_number+3) = 0x30;
				rated_power = 50.0;		//AC50		//v2.88
				OUTPUT_RATED_AMPS=139.0;				
				IN_RATED_AMPS_LO_ONE=139.0;				
				IN_RATED_AMPS_HI_ONE=69.5;				
				IN_RATED_AMPS_LO_TWO=69.5;				
				IN_RATED_AMPS_HI_TWO=34.75;
				break;
		case 8: // 63kVA
				*(dmc_model_number+2) = 0x36;	//"MM63"
				*(dmc_model_number+3) = 0x33;
				rated_power = 63.0;			//AC63
				OUTPUT_RATED_AMPS=175.0;
				IN_RATED_AMPS_LO_ONE=175.0;
				IN_RATED_AMPS_HI_ONE=87.5;
				IN_RATED_AMPS_LO_TWO=87.5;
				IN_RATED_AMPS_HI_TWO=43.75;
				break;
		case 9: // kVA24
				*(dmc_model_number+2) = 0x32;	//"MM24"
				*(dmc_model_number+3) = 0x34;
				rated_power = 24.0;
				OUTPUT_RATED_AMPS=70.0;
				IN_RATED_AMPS_LO_ONE=70.0;
				IN_RATED_AMPS_HI_ONE=35.0;
				IN_RATED_AMPS_LO_TWO=33.33;
				IN_RATED_AMPS_HI_TWO=16.67;
				break;
		case 10: // undefined
				*(dmc_model_number) = 0x42;
				*(dmc_model_number+1) = 0x69;		//"*ERR*"
				*(dmc_model_number+2) = 0x82;
				*(dmc_model_number+3) = 0x82;
				*(dmc_model_number+4) = 0x42;
				break;
		case 11: // 75kVA
				*(dmc_model_number+2) = 0x37;	//"MM75"
				*(dmc_model_number+3) = 0x35;
				rated_power = 75.0;			//AC75		//OLD #'s (Changed v2.85)
				OUTPUT_RATED_AMPS=209.0;				//200.0;
				IN_RATED_AMPS_LO_ONE=209.0;
				IN_RATED_AMPS_HI_ONE=104.0;				//100.0;
				IN_RATED_AMPS_LO_TWO=104.0;
				IN_RATED_AMPS_HI_TWO=52.0;
				break;
		case 12: // 125kVA
				*(dmc_model_number+2) = 0x31;	//"MM125"
				*(dmc_model_number+3) = 0x32;
				*(dmc_model_number+4) = 0x35;
				rated_power = 125.0;				//AC125
				OUTPUT_RATED_AMPS=347.0;			//333.34;	//266.67;
				IN_RATED_AMPS_LO_ONE=362.0;			//333.34;	//266.67;
				IN_RATED_AMPS_HI_ONE=181.0;			//166.67;	//133.33;
				IN_RATED_AMPS_LO_TWO=181.0;			//166.67;	//133.33;
				IN_RATED_AMPS_HI_TWO=90.5;			//83.34;	//66.67;
				break;
		case 13: // 150kVA
				*(dmc_model_number+2) = 0x31;	//"MM150"
				*(dmc_model_number+3) = 0x35;
				*(dmc_model_number+4) = 0x30;
				rated_power = 150.0;		//AC100		//OLD #'s (Changed v2.85)
				OUTPUT_RATED_AMPS=417.0;				//266.67;
				IN_RATED_AMPS_LO_ONE=417.0;				//266.67;
				IN_RATED_AMPS_HI_ONE=208.5;				//133.33;
				IN_RATED_AMPS_LO_TWO=208.5;				//133.33;
				IN_RATED_AMPS_HI_TWO=104.25;				//66.67;
				break;
		case 14: // 90kVA
				*(dmc_model_number+2) = 0x39;	//"MM90"
				*(dmc_model_number+3) = 0x30;
				rated_power = 90.0;
				OUTPUT_RATED_AMPS=250.0;
				IN_RATED_AMPS_LO_ONE=250.0;
				IN_RATED_AMPS_HI_ONE=125.0;
				IN_RATED_AMPS_LO_TWO=125.0;
				IN_RATED_AMPS_HI_TWO=62.0;
				break;
		case 15: // 100kVA
				*(dmc_model_number+2) = 0x31;	//"MM100"
				*(dmc_model_number+3) = 0x30;
				*(dmc_model_number+4) = 0x30;
				rated_power = 100.0;		//AC100		//OLD #'s (Changed v2.85)
				OUTPUT_RATED_AMPS=278.0;				//266.67;
				IN_RATED_AMPS_LO_ONE=278.0;				//266.67;
				IN_RATED_AMPS_HI_ONE=139.0;				//133.33;
				IN_RATED_AMPS_LO_TWO=139.0;				//133.33;
				IN_RATED_AMPS_HI_TWO=69.5;				//66.67;
				break;
		default: //undefined--logic error--show:	"*ERR*"
				*(dmc_model_number) = 0x42;
				*(dmc_model_number+1) = 0x69;		//"*ERR*"
				*(dmc_model_number+2) = 0x82;
				*(dmc_model_number+3) = 0x82;
				*(dmc_model_number+4) = 0x42;
				break;
	}

	if (HF_model)
	{
		*(dmc_model_number+5) = 0x72;	//"H"
		*(dmc_model_number+6) = 0x70;	//"F"
	}

	if ((rated_power >= 75.0) || AC75_CONTROL_PCB)
	{
		if (IN_RATED_AMPS_LO_ONE < shoreCordRating)
		{
			shoreCordRating = IN_RATED_AMPS_LO_ONE;		//worst case: 3-phase, Low Range.
		}
	}
	else
	{
		if ((IN_RATED_AMPS_LO_ONE * 1.5) < shoreCordRating)
		{
			shoreCordRating = IN_RATED_AMPS_LO_ONE * 1.5;	//worst case: 1-phase, Low Range.
		}
	}
	if(GP_OUTPUT_FORM==TWO_PHASE)
	{
		OUTPUT_RATED_AMPS = OUTPUT_RATED_AMPS * 1.5; 
	}
	if(GP_OUTPUT_FORM==ONE_PHASE)
	{
		OUTPUT_RATED_AMPS = OUTPUT_RATED_AMPS * 1.5; 
	}

#ifdef MM3
	rated_power *= 2.0;			//AC55 x2			//if both converters are online!
	OUTPUT_RATED_AMPS *= 2.0;
	IN_RATED_AMPS_LO_ONE *= 2.0;
	IN_RATED_AMPS_HI_ONE *= 2.0;
	IN_RATED_AMPS_LO_TWO *= 2.0;
	IN_RATED_AMPS_HI_TWO *= 2.0;
#endif //MM3

	RATED_POWER			= rated_power;

	return dmc_model_number;
}
/*---------------------------------------------------------------------------*/
float config_DMC_output_voltage()
{
  float voltage;
  unsigned config = 0;
  unsigned uNotABitType;

	gp_state.vout_range = (unsigned) (_getbit(P2,7)+1);	// {NONE,LOW_RANGE,HIGH_RANGE};// Voltage bit#3

	uNotABitType = (unsigned)_getbit(P2,6);		// Voltage.
	config = (uNotABitType<<2);					// Voltage bit#2.
	uNotABitType = (unsigned)_getbit(P2,5);		// Voltage.
	config = config | (uNotABitType<<1);					// Voltage bit#1.
	uNotABitType = (unsigned)_getbit(P2,4);		// Voltage.
	config = config | uNotABitType;				// Voltage bit#0.

//	LogEvent(Ev_ERR,(unsigned char)config);

	switch (config)
	{
		case 0:// Vout
				voltage = 100.0 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE};
				break;
		case 1:// Vout
				voltage = 110.0 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE};
				break;
		case 2:// Vout
				voltage = 112.5 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE};
				break;
		case 3:// Vout
				voltage = 115.0 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE};
				break;
		case 4:// Vout
				voltage = 120.0 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE};
				break;
		case 5:// Vout
				voltage = 127.0 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE};
				break;
		case 6:// Vout
				voltage = 138.5 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE};
				break;
		case 7:// Vout
		default:
				voltage = 150.0 * gp_state.vout_range;	// {NONE,LOW_RANGE,HIGH_RANGE};
				break;
	}
	return voltage;
}
/*---------------------------------------------------------------------------*/
unsigned config_DMC_output_form()
{
  unsigned form;
  unsigned config=0;
  unsigned uNotABitType;

	uNotABitType = (unsigned)_getbit(P2,3);		// Power Form.
	config = (uNotABitType<<1);					// Power Form bit#1.
	uNotABitType = (unsigned)_getbit(P2,2);		// Power Form.
	config = (config | uNotABitType);				// Power Form bit#0.

	switch (config)
	{
		case 0:// 3-phase Wye
					form = 3;
					DELTA_OUTPUT	=FALSE;
					break;

		case 1:// 1-phase Wye
					form = 1;
					DELTA_OUTPUT	=FALSE;
					break;

		case 2:// 2-phase Delta
					form = 2;
					DELTA_OUTPUT	=TRUE;
					break;

		case 3:// 3-phase Delta
		default:
					form = 3;
					DELTA_OUTPUT	=TRUE;
					break;
	}
	return form;
}
/*---------------------------------------------------------------------------*/
float config_DMC_output_frequency()
{
  float frequency;
  unsigned config_bit = (unsigned)_getbit(P2,1);

	switch (config_bit)
	{
		case 0:// 50Hz
					frequency = 50.0;
					break;

		case 1:// 60Hz
					frequency = 60.0;
					break;
		default:
					frequency = 60.0;
					break;
 	}
	return frequency;
}
#endif //MM3
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
