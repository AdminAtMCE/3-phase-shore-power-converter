/*******************************************************************************
modbus.c			MODBUS RTU SERVER

	Written by:	Dave Newberry for ASEA Power Systems

	Copyright(c)2006, ASEA Power Systems
*******************************************************************************/
#include "ac.h"				//Asea 3phase Converter Controller main header file.
#include "modbus.h"			//ASEA MODBUS HEADER data types, prototypes, defines.

//#define MODBUS_DEBUG_1
#ifdef MODBUS_DEBUG_1
extern void clear_screen(void);
extern void lcd_display(char *, int, int);
#endif //MODBUS_DEBUG_1

/*============================================================================*/
///externs
/*============================================================================*/
extern MAP_DESC_TYP coil_map[];
extern MAP_DESC_TYP discrete_inputs_map[];
extern MAP_DESC_TYP register_map[];
extern MAP_DESC_TYP inRegister_map[];
extern volatile unsigned long eventTime;	//unsigned long eventTime in 10ms units.
extern unsigned remoteActive;
extern UINT16 u_password;		//this adr must contain 411 to unlock REG's & COIL's
/*============================================================================*/
///server data start
/*============================================================================*/
#pragma noclear
UINT16 modbusActive;
UINT16 ASEAdeviceId;
BYTE modbusNodeId;
MAP_DESC_TYP *temp_mbData;

#pragma clear
//#pragma align hb=c 								// ALIGNMENT: PEC ADDRESSABLE
#pragma default_attributes
/*============================================================================*/
UINT16 modbus_state;				//should be initialized to modbus_state=0;
UINT16 basic_function;				//should be initialized to basic_function=0;
volatile UINT16 UART_rcv_count;                        //Receive Buffer Counter

//MAP_DESC_TYP *temp_mbData;

volatile UINT16 mbErrParity;
volatile UINT16 mbErrFraming;
volatile UINT16 mbErrOverrun;
unsigned modbusTime;				// MODBUS TIMER
volatile unsigned long tmpTime;		// MODBUS TIMER
UINT16 mbColon;
UINT16 mbAstrik;
UINT16 mbLineFeed;
UINT16 secondTimeThrough;

/*============================================================================*/
//#pragma clear
struct mbQueueType mbIn,*mbInQueue;
struct mbQueueType mbOut,*mbOutQueue;

IN_BUF_TYP in_buffer;

// These are the main input and output buffers for the modbus message.
BYTE UART_RX_buf[MX_RX_TX_BUFFERSIZE];      //Receive Buffer 
BYTE UART_TX_Buffer[MX_RX_TX_BUFFERSIZE];   //Transmit Buffer

UINT16 u_mbInQmax;
UINT16 u_mbOutQmax;

UINT16 mbCommand;
/*============================================================================*/
#include "crc.h"	//CRC TABLES
/*============================================================================*/
///server data end
/*============================================================================*/
/*============================================================================*/

/*============================================================================*/
/*============================================================================*/
//	Modbus Server Code
/*============================================================================*/
/*============================================================================*/
VOID initializeModbusRAM()	//called from init_ram() on RAM CLEAR/battery change.
{
	modbusActive = FALSE;
	modbusNodeId = MODBUS_NODE_ID;
	ASEAdeviceId = ASEA_PRODUCT_ID;

	mbErrParity	=0;
	mbErrFraming=0;
	mbErrOverrun=0;

	mbColon		=0;
	mbAstrik	=0;
   	mbLineFeed	=0;

	temp_mbData	=0;

	u_password	=0;		//this adr must contain 411 to unlock REG's & COIL's
}
/*============================================================================*/
// Initiate the modbus buffers & serial RxD interrupt
VOID modbus_init()
{
	UINT16 i;

	_putbit(0,S0RIC,6);		// disable serial RxD interrupt 
	modbus_state = 0;
	basic_function = 0;

	u_mbInQmax=0;
	u_mbOutQmax=0;

	mbInQueue = mbInitializeQueue(&mbIn);
	mbOutQueue = mbInitializeQueue(&mbOut);

	clr_rx();

	for (i=0;i<MX_RX_TX_BUFFERSIZE;i++)
	{
		UART_RX_buf[i]=0;
		UART_TX_Buffer[i]=0;
	}

	convertStaticRegisters();

	_putbit(1,S0RIC,6);		// enable serial RxD interrupt 
}
/*============================================================================*/
VOID clr_rx()
{
	UART_rcv_count = 0;
	secondTimeThrough = FALSE;
}
/*============================================================================*/
// Procedure to check the checksum of the message
UINT16 modrtuChecksum( BYTE *chkbuf, UINT16 len )
{
   BYTE    uchCRCHi = 0xff;
   BYTE    uchCRCLo = 0xff;
   UINT16  uIndex;
   UINT16  temp_code;

   while ( len )
   {
      uIndex = (UINT16) ( uchCRCHi ^ *chkbuf++ );
      uchCRCHi = (BYTE) ( uchCRCLo ^ auchCRCHi_exp[uIndex] );
      uchCRCLo = auchCRCLo_exp[uIndex];
      len-- ;
   }
   temp_code = (UINT16) uchCRCHi;
   temp_code = (UINT16) (temp_code << 8);

   return (UINT16)(temp_code | uchCRCLo );
}
/*============================================================================*/
VOID idle_modbus( VOID )
{
	UINT16 rc_poll;

   switch (modbus_state)
   {
      case 0:
			 /******************************************************************/
			 /*     WAIT FOR A MESSAGE TO ARRIVE AND CHECK FOR COMPLETNESS     */
			 /******************************************************************/

				rc_poll = modbusPollReceiveComplete();

				if (rc_poll==CMPLT_COMPLETE)
				{
					modbus_state++;
				}				
				else if (rc_poll==CMPLT_NO_HOPE_1)
				{
					modbus_state=4;	   //bad UART_RX_buf[1]...function code not correct
				}
				else if (rc_poll==CMPLT_NO_HOPE_2)
				{
					modbus_state=4;		//bad checksum, bail.
				}
				break;
      case 1:
			 /******************************************************************/
			 /*       MESSAGE IS COMPLETE, CHECK ADDRESS AND PARSE             */
			 /******************************************************************/

				if ( modbusCheckAddress() )
				{
					basic_function=modbusParseMessage();
					clr_rx();
					modbus_state++;
				}
				else
				{
					clr_rx();
					modbus_state=0;
				}
				break;
      case 2:
			 /******************************************************************/
			 /*       STORE OR FETCH DATA FROM SYSTEM                          */
			 /******************************************************************/

				if (basic_function==READ)
				{
					temp_mbData=modbusFetchData();
				}
				else if (basic_function==WRITE)
				{
					temp_mbData=modbusStoreData();
					ASEA_storeData((UINT16)in_buffer.function, temp_mbData);
				}
				else
				{
					modbusReturnException( ILLEGAL_FUNCTION );
					modbus_state=0;
					break;
				}

				modbus_state++;
				break;
      case 3:
			 /******************************************************************/
			 /*       RESPOND TO MODBUS MESSAGE APPROPRIATELY                  */
			 /******************************************************************/

				modbusRespond(temp_mbData);
				modbus_state=0;
				remoteActive=YES;			//flag for display of 'RC'
				break;
      case 4:
			 /******************************************************************/
			 /*     IF THERE IS AN ERROR REMOVE ONE CHARACTER AND TRY AGAIN    */
			 /******************************************************************/

				if (UART_RX_buf[0]==0x3A)
				{
					mbColon++;		//':'=0x3A=58
					modbusActive=0;
				}
				else if (UART_RX_buf[0]==0x2A)
				{
					mbAstrik++;		//'*'=0x2A=42
					modbusActive=0;
				}

				if (secondTimeThrough)
				{
					mbInQueue = mbInitializeQueue(&mbIn);
					mbOutQueue = mbInitializeQueue(&mbOut);

					clr_rx();

					UART_RX_buf[0]=0;
					UART_RX_buf[1]=0;

					secondTimeThrough = FALSE;
				}
				else
				{
					if (modbusActive)	//protocol switch, so, clean-up
					{
						modbusGiveUpCharacter();
					}
					secondTimeThrough = TRUE;
				}

      default:

				modbus_state=0;
				basic_function=0;
				break;
   }
}
/*=========================================================================*/
// Send exception response to client
VOID modbusReturnException( BYTE exception )
{
	if ( modbusCheckAddress() )
	{
		UART_TX_Buffer[1] = (BYTE) (UART_TX_Buffer[1] | (BYTE)0x80);      //function code for exception
		UART_TX_Buffer[2]  = (BYTE) exception;
		modbusSendUARTPacket((BYTE)3);
	}
}
/*=========================================================================*/
// Calculate the number of bytes from the number of bits requested
BYTE modbusByteFromBitLength( UINT16 coils )
{
	BYTE coilCnt=(BYTE)coils;
	BYTE len = (BYTE)(coilCnt >> 3) ;  // divide the number of bits by 8

	if ( coilCnt & 0x07 )
	{								//if there was a remainder then add 1 more
		len++;
	}
	return len;
}
/*============================================================================*/
// Return a pointer to the map descriptor containing the data requested
MAP_DESC_TYP *find_map_desc_with_data(BYTE function,UINT16 address)
{
	BYTE index;
	MAP_DESC_TYP *rtn_value=0;

	switch ( function )
	{
		case MODBUS_READ_COILS :
				for ( index=0;index<(BYTE)MX_COILS;index++ )
				{
					if ( (coil_map[index].address-1)==(UINT16)address )
					{
						da_get_BIT(&coil_map[index]);
						rtn_value = &coil_map[index];
					}
				}
				break;

		case MODBUS_READ_DISCRETEINPUTS :
				for ( index=0;index<(BYTE)MX_DI;index++ )
				{
					if ( (discrete_inputs_map[index].address-1)==(UINT16)address )
					{
						da_get_BIT(&discrete_inputs_map[index]);
						rtn_value = &discrete_inputs_map[index];
					}
				}
				break;

		case MODBUS_READ_INPUTREGISTERS:
				for ( index=0;index<(BYTE)MX_IN;index++ )
				{
					if ( (UINT16)(inRegister_map[index].address-1)==(UINT16)address )
					{
						da_get_UINT16(&inRegister_map[index]);
						rtn_value = &inRegister_map[index];
					}
				}
				break;

		case MODBUS_READ_REGISTER:	//MODBUS_READ_REGISTER holding registers
				for ( index=0;index<(BYTE)MX_REG;index++ )
				{
					if ( (UINT16)(register_map[index].address-1)==(UINT16)address )
					{
						da_get_UINT16(&register_map[index]);
						rtn_value = &register_map[index];
					}
				}
				break;

		default:
				break;
	}
	return rtn_value;	//better way to do it
}
/*============================================================================*/
// Return a pointer to a map descriptor where data will be stored
MAP_DESC_TYP *store_data_to_data_array(BYTE function, UINT16 address, UINT16 data)
{
	BYTE index;

	switch ( function )
	{
		case MODBUS_WRITE_COIL :
					for ( index=0;index<(BYTE)MX_COILS;index++ )
					{
						if ( (coil_map[index].address-1)==(UINT16)address )
						{
							da_put_BIT(&coil_map[index],data);
							return &coil_map[index];
						}
					}
					return 0;
		case MODBUS_WRITE_REGISTER:
					for ( index=0;index<(BYTE)MX_REG;index++ )
					{
						if ( (register_map[index].address-1)==(UINT16)address )
						{
							da_put_UINT16(&register_map[index],data);
							return &register_map[index];
						}
					}
					return 0;
		default:
					return 0;
	}
}
/*=========================================================================*/
// Respond to the modbus poll from the client
VOID modbusRespond( MAP_DESC_TYP *temp_md )
{
   	BYTE byte_cnt, i, t, bool_val;
   	BYTE byte_store, temp_bool;
	BYTE local_length;

	switch ( UART_RX_buf[1] )
    {
      /******************************************************************/
      /*       FUNCTION CODE 0X01 -> READ COILS                         */
      /******************************************************************/
      case MODBUS_READ_COILS:
				UART_TX_Buffer[1] = UART_RX_buf[1];

				if ((in_buffer.address+in_buffer.length) > MX_COILS)
				{
					modbusReturnException( ILLEGAL_DATA_ADDRESS );
					return;
				}

				byte_cnt = modbusByteFromBitLength( in_buffer.length );
				UART_TX_Buffer[2] = byte_cnt;

				for (i=0; i < byte_cnt; i++)
				{
					byte_store = 0;

					for (t=0; t < (BYTE) BYTE_SIZE; t++)
					{
						if ( !temp_md )
						{
							modbusReturnException( ILLEGAL_DATA_ADDRESS );
							return;
						}
						bool_val = (BYTE)temp_md->data;
						temp_bool = (BYTE)(bool_val << t);
						byte_store = (BYTE)(byte_store|temp_bool);
						in_buffer.address++;

						temp_md=modbusFetchData();
					}
					UART_TX_Buffer[3+i] = byte_store;
				}
				modbusSendUARTPacket( (BYTE)(3+byte_cnt) );
				break;
         /******************************************************************/
         /*       FUNCTION CODE 0X02 -> READ DISCRETE INPUTS               */
         /******************************************************************/
		case MODBUS_READ_DISCRETEINPUTS:
				UART_TX_Buffer[1] = UART_RX_buf[1];

				if ((in_buffer.address+in_buffer.length) > MX_DI)
				{
					modbusReturnException( ILLEGAL_DATA_ADDRESS );
					return;
				}

				byte_cnt = modbusByteFromBitLength(in_buffer.length); //divides length by 8
				UART_TX_Buffer[2] = byte_cnt;

				for (i=0; i < byte_cnt; i++)
				{
					byte_store = 0;

					for (t=0; t < (BYTE) BYTE_SIZE; t++)
					{
						if ( !temp_md )
						{
							modbusReturnException( ILLEGAL_DATA_ADDRESS );
							return;
						}
						bool_val = (BYTE)temp_md->data;
						temp_bool = (BYTE)(bool_val << t);
						byte_store = (BYTE)(byte_store|temp_bool);
						in_buffer.address++;
						
						temp_md=modbusFetchData();
					}
					UART_TX_Buffer[3+i] = byte_store;
				}
				modbusSendUARTPacket( (BYTE)(3+byte_cnt) );
				break;
         /******************************************************************/
         /*       FUNCTION CODE 0X05 -> WRITE COIL                         */
         /******************************************************************/
      case MODBUS_WRITE_COIL:
				UART_TX_Buffer[1] = UART_RX_buf[1];

				if ((in_buffer.address+in_buffer.length) > MX_COILS)
				{
					modbusReturnException( ILLEGAL_DATA_ADDRESS );
					return;
				}

				if ( !temp_md )
				{
					modbusReturnException (ILLEGAL_DATA_ADDRESS);
					return;
				}

				for (i=0; i < (BYTE) WRITE_COIL_RESPONSE_LENGTH; i++)
				{
					UART_TX_Buffer[i] = UART_RX_buf[i];
				}
				modbusSendUARTPacket( WRITE_COIL_RESPONSE_LENGTH );
				break;
         /******************************************************************/
         /*       FUNCTION CODE 0X06 -> WRITE REGISTER                     */
         /******************************************************************/
      case MODBUS_WRITE_REGISTER:
				UART_TX_Buffer[1] = UART_RX_buf[1];

				if ((in_buffer.address+in_buffer.length) > MX_REG)
				{
					modbusReturnException( ILLEGAL_DATA_ADDRESS );
					return;
				}

				if ( !temp_md )
				{
					modbusReturnException(ILLEGAL_DATA_ADDRESS);
					return;
				}

				for ( i = 0 ; i < (BYTE)WRITE_COIL_RESPONSE_LENGTH ; i++ )
				{
					UART_TX_Buffer[i] = UART_RX_buf[i];
				}
				modbusSendUARTPacket( WRITE_COIL_RESPONSE_LENGTH );
				break;
         /******************************************************************/
         /*       FUNCTION CODE 0X04  -> READ REGISTER                     */
         /******************************************************************/
      case MODBUS_READ_INPUTREGISTERS:
				UART_TX_Buffer[1] = UART_RX_buf[1];

				if ((in_buffer.address+in_buffer.length) > MX_IN)
				{
					modbusReturnException( ILLEGAL_DATA_ADDRESS );
					return;
				}

				i = 3;
				local_length=(BYTE)(in_buffer.length);

				for (byte_cnt=0; byte_cnt < local_length; byte_cnt++)
				{
					if ( !temp_md )
					{
						modbusReturnException( ILLEGAL_DATA_ADDRESS );
						return;
					}
					UART_TX_Buffer[i] = (BYTE)(temp_md->data>>8); 	//high 8 bits
					UART_TX_Buffer[i+1] = (BYTE)(temp_md->data);  	//low 8 bits
					i = (BYTE)(i + 2);
					in_buffer.address++;
				   
					temp_md=modbusFetchData();
				}
				UART_TX_Buffer[2] = (BYTE)(UART_RX_buf[5] * 2); 		// num data bytes
				modbusSendUARTPacket( (BYTE)(3 + UART_TX_Buffer[2]) );
				break;

         /******************************************************************/
         /*       FUNCTION CODE 0X03  -> READ HOLDING REGISTER             */
         /******************************************************************/
      case MODBUS_READ_REGISTER:
				UART_TX_Buffer[1] = UART_RX_buf[1];

				if ((in_buffer.address+in_buffer.length) > MX_REG)
				{
					modbusReturnException( ILLEGAL_DATA_ADDRESS );
					return;
				}

				i = 3;
				local_length=(BYTE)(in_buffer.length);

				for (byte_cnt=0; byte_cnt < local_length; byte_cnt++)
				{
					if ( !temp_md )
					{
						modbusReturnException( ILLEGAL_DATA_ADDRESS );
						return;
					}
					UART_TX_Buffer[i] = (BYTE)(temp_md->data>>8); 	//high 8 bits
					UART_TX_Buffer[i+1] = (BYTE)(temp_md->data);  	//low 8 bits
					i = (BYTE)(i + 2);
					in_buffer.address++;
				   
					temp_md=modbusFetchData();
				}
				UART_TX_Buffer[2] = (BYTE)(UART_RX_buf[5] * 2); 		// num data bytes
				modbusSendUARTPacket( (BYTE)(3 + UART_TX_Buffer[2]) );
				break;

      default:
				break;
   }
}
/*============================================================================*/
// Make sure that the modbus poll received is complete as a message.
UINT16 modbusPollReceiveComplete()
{
   BYTE   target,count_tmp;
   UINT16 check,temp_code2,temp_code3;
   UINT16 temp_code1;

   	get_UART_data();

   /******************************************************************/
   /*      LESS THAN FOUR BYTES RECEIVED CMPLT_HOPE                  */
   /******************************************************************/

   if ( UART_rcv_count < 4 )  //received less than 4 bytes
   {
		return CMPLT_HOPE; // hope yet
   }
   /******************************************************************/
   /*      CHECK FOR CORRECT FUNCTION CODE                           */
   /******************************************************************/
#define TARGET_CNT 6

   switch ( UART_RX_buf[1] )
   {
		case MODBUS_READ_COILS:
					target = (BYTE)TARGET_CNT;
					break;
		case MODBUS_READ_DISCRETEINPUTS:
					target = (BYTE)TARGET_CNT;
					break;
		case MODBUS_READ_REGISTER:
					target = (BYTE)TARGET_CNT;
					break;
		case MODBUS_READ_INPUTREGISTERS:
					target = (BYTE)TARGET_CNT;
					break;
		case MODBUS_WRITE_COIL:
					target = (BYTE)TARGET_CNT;
					break;
		case MODBUS_WRITE_REGISTER:
					target = (BYTE)TARGET_CNT;
					break;
		default:
					return CMPLT_NO_HOPE_1;
   }
   /******************************************************************/
   /*      HANDLES RESPONSES FROM OTHER SERVERS                      */
   /******************************************************************/

   if ( UART_rcv_count >= target )
   {
		temp_code1 = UART_RX_buf[target-2];
		temp_code2 = (UINT16) (temp_code1 << 8);
		temp_code1 = UART_RX_buf[target-1];
		temp_code3 = (UINT16)(temp_code2|temp_code1);

		if ( modrtuChecksum(UART_RX_buf,(UINT16)(target-2)) == temp_code3)
		{
			return CMPLT_COMPLETE;
		}
   }
   /******************************************************************/
   /*      CHECK FOR CORRECT MESSAGE LENGTH                          */
   /******************************************************************/

   temp_code1 = (UINT16)(target + 2);
   count_tmp=(BYTE)temp_code1;

   if ( UART_rcv_count < count_tmp )
   {
		return CMPLT_HOPE;
   }
   /******************************************************************/
   /*      CHECK FOR CHECKSUM                                        */
   /******************************************************************/

   check = modrtuChecksum(UART_RX_buf,(UINT16)target);

   temp_code1 = UART_RX_buf[target];
   temp_code2 = (UINT16) (temp_code1 << 8);
   temp_code1 = UART_RX_buf[target+1];
   temp_code3 = (UINT16)(temp_code2|temp_code1);

   if (check==temp_code3)
   {
		return CMPLT_COMPLETE;
   }

   return CMPLT_NO_HOPE_2;
}
/*============================================================================*/
// Extract characters from the UART buffer
VOID get_UART_data(VOID)   // this function reads data from uart circular buffer into UART_RX_buf
{
	BYTE ch;

	while(ASEA_getRxBufDataCnt())
	{
		ch = ASEA_getch();
	
		if (UART_rcv_count < MX_RX_TX_BUFFERSIZE)
		{
			UART_RX_buf[UART_rcv_count] = ch;
			UART_rcv_count++;
		}
	}
}
/*=========================================================================*/
BYTE modbusCheckAddress()	// Check the modbus address
{
	BYTE id_temp;
	id_temp= (BYTE)Get_Modbus_Node_ID();

	if (UART_RX_buf[0] == id_temp )
	{
		return TRUE;
	}
	return FALSE;
}
/*============================================================================*/
// In the event of the message being incomplete give up one character and recheck the message
VOID modbusGiveUpCharacter()
{
	UINT16 i;

//	if ((UART_rcv_count>0) && (UART_rcv_count < MX_RX_TX_BUFFERSIZE))
//	{
		for (i=1;i<UART_rcv_count;i++)
		{
			UART_RX_buf[i-1] = UART_RX_buf[i];	//added this line to bump one byte only
		}
		UART_rcv_count--;
//	}

	if (UART_rcv_count==0)
	{
		modbusReturnException(ILLEGAL_DATA_ADDRESS);
		return ;
	}
}
/*============================================================================*/
// Extract relevant data from the message received from the client
UINT16 modbusParseMessage()
{
   UINT16 basic_funct=0;
   UINT16 u2,u3,u4,u5,u6,u7;
   u2 = UART_RX_buf[2];
   u3 = UART_RX_buf[3];
   u4 = UART_RX_buf[4];
   u5 = UART_RX_buf[5];
   u6 = UART_RX_buf[6];
   u7 = UART_RX_buf[7];

   in_buffer.node_id	= UART_RX_buf[0];
   in_buffer.function	= UART_RX_buf[1];
   in_buffer.address 	= ((u2<<8) | u3);
   in_buffer.length  = 1;					//for single-coil/register writes

   switch (in_buffer.function)
   {
      case MODBUS_READ_COILS:
				 in_buffer.length  = ((u4<<8) | u5);
				 basic_funct=READ;
				 break;
      case MODBUS_READ_DISCRETEINPUTS:
				 in_buffer.length  = ((u4<<8) | u5);
				 basic_funct=READ;
				 break;
	  case MODBUS_READ_REGISTER:
				 in_buffer.length  = ((u4<<8) | u5);
				 basic_funct=READ;
				 break;
      case MODBUS_READ_INPUTREGISTERS:
				 in_buffer.length  = ((u4<<8) | u5);
				 basic_funct=READ;
				 break;
      case MODBUS_WRITE_COIL:
				 in_buffer.data = ((u4<<8) | u5);
				 basic_funct=WRITE;
				 break;
      case MODBUS_WRITE_REGISTER:
				 in_buffer.data = ((u4<<8) | u5);
				 basic_funct=WRITE;
				 break;
      default:
				break;
   }
   in_buffer.checksum= ((u6<<8) | u7);

   return basic_funct;
}
/*============================================================================*/
// Returns a map descriptor containing the data required
MAP_DESC_TYP *modbusFetchData()
{
   MAP_DESC_TYP *temp_md;
   temp_md = find_map_desc_with_data(in_buffer.function,in_buffer.address);
   return temp_md;
}
/*============================================================================*/
// Return a map descriptor that the data will be stored to
MAP_DESC_TYP *modbusStoreData()
{
   MAP_DESC_TYP *temp_md;
   temp_md=0;

   if ( in_buffer.function==(BYTE)MODBUS_WRITE_COIL )
   {
      if ( in_buffer.data==(UINT16)COIL_ON )
      {
         temp_md=store_data_to_data_array(in_buffer.function,in_buffer.address,ON);
      }
      else if ( in_buffer.data==(UINT16)COIL_OFF )
      {
         temp_md=store_data_to_data_array(in_buffer.function,in_buffer.address,OFF);
      }
   }
   else
   {
      temp_md=store_data_to_data_array(in_buffer.function,in_buffer.address,in_buffer.data);
   }
   return temp_md;
}

/*============================================================================*/
/*============================================================================*/
//	config.c
/*============================================================================*/
/*============================================================================*/
//	ASEA SERIAL PORT I/O FUNCTIONS for Modbus support.
/*============================================================================*/
BYTE Get_Modbus_Node_ID()	// Access the node ID of the system
{
	BYTE Node_Address = (BYTE)modbusNodeId;
	return Node_Address ;
}
/*============================================================================*/
VOID modbusSendUARTPacket(BYTE length)	// Send response to client
{
	UINT16 check;
	UINT16 msgSize=(UINT16)length;

	UART_TX_Buffer[0] = (Get_Modbus_Node_ID());

	check = modrtuChecksum( UART_TX_Buffer, msgSize);

	UART_TX_Buffer[msgSize++] = (BYTE)(check >> 8);
	UART_TX_Buffer[msgSize++] = (BYTE)(check & 0xff);

	ASEA_putString(&UART_TX_Buffer[0],msgSize);		//load msg into TxD buffer
	ASEA_TxD(msgSize);								//send msg out serial port

	mbFlush_queue(mbOutQueue);

	modbusTime = (unsigned) (eventTime-tmpTime);	//stop MODBUS MSG TIMER
	tmpTime = eventTime;							//start MODBUS MSG TIMER
}
/*============================================================================*/
VOID	ASEA_putString(BYTE *s, UINT16 length)
{
  UINT16 n;

	for (n=0;n<length;n++)
	{	
		mbEnqueue(mbOutQueue,s++);
	}

	if ((mbOutQueue->count)>u_mbOutQmax) u_mbOutQmax = (mbOutQueue->count); //just collecting max Q size.
}
/*============================================================================*/
/*
BYTE ASEA_putByte(BYTE b)	// add byte to buffer if not full--not used.
{
	if(mbQueueFull(mbOutQueue))
	{
		return (BYTE) 0;			//return Failure
	}
	else
	{
		mbEnqueue(mbOutQueue,&b);
		return (BYTE) 1;			// return Success
	}
}
/*============================================================================*/
VOID	ASEA_TxD(UINT16 byteCnt)					// send msg out Serial port
{
	while (byteCnt--)
	{
		ASEA_sendByte(mbDequeue(mbOutQueue));		// TxD byte out serial port.
	}
}
/*============================================================================*/
VOID ASEA_sendByte(BYTE msgByte)				// send byte out Serial port
{
	_putbit(0,S0TIC,7);				// set S0TIR, ready to transmit bit

	S0TBUF = msgByte;				// put byte in transmit data buffer

	while(!_getbit(S0TIC,7));		// check S0TIR, wait until transmit buffer empty
}
/*============================================================================*/
/*
UINT16	ASEA_getTxBufDataCnt()
{
	return (UINT16) (mbOutQueue->count);	// give'em unprocessed TxD count.
}
/*============================================================================*/
/*============================================================================*/
BYTE	ASEA_getch()
{
  mbDataType chIn=0;

	_putbit(0,S0RIC,6);		// disable serial interrupt as semaphore

	if (ASEA_getRxBufDataCnt())
	{
		chIn = mbDequeue(mbInQueue);
	}

	_putbit(1,S0RIC,6);		// enable serial interrupt, release semaphore

	return (BYTE) chIn;						// give byte from input buffer.
}
/*============================================================================*/
UINT16	ASEA_getRxBufDataCnt()
{
	return (UINT16) (mbInQueue->count);		// give'em unprocessed RxD count.
}
/*============================================================================*/
VOID 	isr_Modbus_RxD()		// if receive interrupt request flag then RxD a byte.
{
  mbDataType mbByte;

	mbByte=S0RBUF;					// get the contents of the serial receive buffer S0RBUF
	mbEnqueue(mbInQueue,&mbByte);

	if ((mbInQueue->count)>u_mbInQmax) u_mbInQmax = (mbInQueue->count);	//just collecting max queue size.
}
/*============================================================================*/
void isr_Modbus_Error()		// if S0EIR error interrupt request flag
{
	if (_getbit(S0CON,8)==1)	// if PARITY err,
	{
		mbErrParity++;
		_putbit(0,S0CON,8);								// clr parity error flag
	}

	if (_getbit(S0CON,9)==1)	// if FRAMING err,
	{
		mbErrFraming++;
		_putbit(0,S0CON,9);								// clr frame error flag
	}

	if (_getbit(S0CON,10)==1)	// if OVERRUN err,
	{
		mbErrOverrun++;
		_putbit(0,S0CON,10);							// clr overrun error flag
	}

	_putbit(0,S0EIC,7);			// clr error interrupt request flag
}
/*============================================================================*/
//===============================================================
//Circular Queue Module used for RxD & TxD of Modbus msg's
//===============================================================
//----------------------------------------------------------------
//Function Definitions
//----------------------------------------------------------------
struct mbQueueType *mbInitializeQueue(struct mbQueueType *q)
{
  q->size=MB_QSIZE;     	//initialize queue
  q->count=0;
  q->head=q->queue;
  q->tail=q->queue;
  return q;
}
//----------------------------------------------------------------
VOID mbEnqueue(struct mbQueueType *q, mbDataType *item)  //put item in queue.
{
	if(!mbQueueFull(q))
	{
		*q->tail++=*item;
		if(q->tail>&q->queue[q->size-1]) 
			q->tail=q->queue; 				//wrap
		q->count++;
		if(q->count>q->size)
			q->count=q->size;			//limit #of entries to sizeof queue.
	}
}
//----------------------------------------------------------------
mbDataType mbDequeue(struct mbQueueType *q)	//get item from queue.
{
  mbDataType item;

	if(mbQueueEmpty(q))
	{
		item=(mbDataType)0;					//ERROR = 0
	}
	else
	{
		item=*q->head++;
		if(q->head>&q->queue[q->size-1])
			q->head=q->queue; 				//wrap
		q->count--;
	}
	return item;
}
//----------------------------------------------------------------
UINT16 mbQueueFull(struct mbQueueType *q)	//boolean check if queue full.
{
	if(q->count>=q->size)
		return 1;
	else
		return 0;
}
//----------------------------------------------------------------
UINT16 mbQueueEmpty(struct mbQueueType *q)	//boolean check if queue empty.
{
	if(q->count==0)
		return 1;
	else
		return 0;
}
//----------------------------------------------------------------
VOID mbFlush_queue(struct mbQueueType *q)	//clear all queue entries.
{
	q->count=0;
	q->head=q->queue;
	q->tail=q->queue;
}
//----------------------------------------------------------------
//end of queue module
//----------------------------------------------------------------
/*============================================================================*/
//	END OF MODBUS SERVER CODE MODULE
/*============================================================================*/

