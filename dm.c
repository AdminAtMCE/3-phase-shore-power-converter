/* @(#)DM.C */
/* ASEA SOURCE CODE */
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	FILE:			dm.c
//	PURPOSE:		DISPLAY MANAGER Source Module for ASEA Power Systems.
//
//	Proprietary Software:	ASEA POWER SYSTEMS
//
//				COPYRIGHT @ ASEA JANUARY 1999
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/*=========================================================================*/
/*=========================================================================*/
#include "ac.h"
#define INFO	10	//new display_type
#define BUS		12	//for Besecke
/*=========================================================================*/
/*=========================PUBLIC==========================================*/
//extern void display_metering(int, int); 			/* Show meter & ENTRY values & update */
//extern void show_signon_screen(void);

/*=========================================================================*/
/*=========================PRIVATE=========================================*/

//void show_graphicalPower_display(void);

void auto_shutdown_control_screen(void);

void show_shutdown_info(void);
void synchroscope(void);

void show_status_display(void);

int display_2phase_power_f1(int);
int display_2phase_power_f2(int);

int display_voltage_f1(int);

int display_output_power_f1(int);
int display_output_power_f2(int);

void display_metering(int, int); 			/* Show meter & ENTRY values & update */
char *get_display_name(int);
//int display_1phase_power_f1(int);
//int display_1phase_power_f2(int);
int display_power_f1(int);
int display_power_f2(int);
int display_power_f4(int);
void display_power_f5(void);
//void display_power_f6(void);
int display_power_f7(int);
int display_power_f8(int);
int display_power_f9(int);
void display_power_f10(void);
void display_power_f11(void);

void show_time(int,int);
void show_summary_display(void);
void show_info(void);
void show_signon_screen(void);
void show_sysstat_OK(void);

void calibrate_system(void);
void calibrate_oscillators(void);		//put in osc.c module !!
void calibrate_sp1_input(void);
void calibrate_sp2_input(void);
void calibrate_generator(void);
void calibrate_converter(void);
void calibrate(int,char *, float *[]);
void calibrate_1phase(int,char *, float *[]);
void calibrate_2phase(int,char *, float *[]);
void calibrate_generator(void);
void display_not_calibration_mode(void);

void edit_serial_comm_config(void);

//#ifdef XMFR_TAP_DISP
_inline unsigned XFMR_400V_Tap(void);
_inline unsigned XFMR_480V_Tap(void);
//#endif

/*=========================================================================*/
/*=========================================================================*/
/* ======================================================================================= PERSISTENT MEMORY */
#pragma noclear
/* ========================================================================================================= */
/* ========================================================================================================= */
#pragma align hb=c 								// ALIGNMENT: PEC ADDRESSABLE
/* ========================================================================================================= */

/* ========================================================================================================= */
#pragma default_attributes
/* ========================================================================================================= */
//unsigned last_EvCount;
char *cbState[2]={"OPEN  \0",
				  "CLOSED\0"};

//#ifdef XMFR_TAP_DISP
unsigned char XFMR_Tap_Check = 0;
unsigned char XFMR_Tap_Selected = 0;
int gp_state_save;
unsigned int XFMR_Tap_Delay_Rd = 0;
unsigned char test_tap_sel = 1;
//#endif

/*=========================================================================*/
//				EXTERN DATA STRUCTURES
/*=========================================================================*/
//EVENT LOG extern
#include "event.h"
extern int LogEvent( unsigned char, unsigned char );
extern unsigned eventLogCount( void );
extern void eventLogViewer( void );
extern void eventLogTracker( void );
extern char *getErrOnCodeDescriptor( unsigned char );
extern int last_failure_code;

extern void scan_kybd(void);
extern int wait_for_key(void);
extern volatile unsigned new_key;
//from upc_lcd.c	LCD
//extern void init_lcd(void);
extern void lcd_display(char *, int, int);
extern float lcd_display_value(float, int, int, char *);
extern void clear_screen(void);
extern void put_cursor(int,int);
//from upc_meas.c	METER
extern void meas_all(void);			/* calc new meter values. */
extern void clear_meter_data(unsigned);		// set measured meter data values to zero for given daq channel.
extern system volatile unsigned int adc_samples;			/* number of samples in metering one waveform */
extern volatile unsigned sync_count;		// global temp.
extern volatile unsigned sync_1_count;		// global temp.
extern volatile unsigned deltaT5gen1;	// Relative Tick count for one cycle, used for frequency calc.
extern volatile unsigned deltaT5gen2;	// Relative Tick count for one cycle, used for frequency calc.

//from upc_osc.c	OSCILLATOR & TRANSIENT
extern void exec_osc_pgm(void);
extern int slew_output(float, float, float);		//create and execute transient for output transistion.
extern float GP_OUTPUT_VOLTAGE;
extern float GP_OUTPUT_FREQUENCY;
extern float newSlewRate,newFrequency,newVoltage;	//used with slew_output() for load management.

//from upc_sid.c	SYSTEM INTERFACE DRIVER
extern volatile unsigned shutoffId;
extern unsigned manual_mode(void);
extern int check_gp_status(void);
extern int set_meter_mux(unsigned, unsigned);
extern unsigned daq_mux_id;
extern unsigned sync_mux_id;
extern unsigned TECHNEL_OPTION;
extern unsigned GP_CONTROL_WORD;	//image in RAM
extern unsigned GP_OUTPUT_FORM;
extern unsigned DUAL_INPUT;
extern unsigned DUAL_GENSET;
extern unsigned DELTA_OUTPUT;
extern float GP_XFMR_RATIO;
extern unsigned EXT_CB_OPTION;

extern unsigned MULTIGEN_OPTION;	//S3-5
extern unsigned MULTIGEN_INHIBIT;	//S3-6
extern unsigned AUTO_TRANSFER;		
extern unsigned auto_transfer_enable;		
extern float IN_RATED_AMPS_LO_ONE;
extern float IN_RATED_AMPS_HI_ONE;
extern float RATED_POWER;
extern unsigned CORDMAN_OPTION;	//S3-7
extern unsigned NEW_G2G_OPTION;	//S3-8
extern unsigned MODEL_LC;			//S4-8
extern unsigned transferMode;		
extern unsigned maintenanceMode;		

extern unsigned GEN_AMPS_PRESENT;	//S4-6  v2.32, or better
extern unsigned REMOTE_RUN;			//S2-4 RUN(low) input M1041
extern unsigned SWITCHGEAR_INTERFACE_OPTION;	//S2-8
extern unsigned EXT_OSC_OPTION;		//S3-6
extern unsigned MULTIGEN_BESECKE;
extern unsigned HYBRID_STO;				//S2-3 and S2-7

extern volatile unsigned far int gp_status_word;		// STATUS WORD1.
extern volatile unsigned far int gp_status_word2;		// STATUS WORD2.
extern volatile unsigned far int gp_status_word3;		// STATUS WORD3.
extern volatile unsigned far int gp_configuration_word;	// CONFIGURATION WORD.
extern volatile unsigned far int gp_configuration_word2;	// EXTENDED CONFIGURATION WORD.
extern unsigned VIRTUALconfiguration_word;	// VIRTUAL CONFIGURATION WORD.
extern unsigned VIRTUALconfiguration_word2;	// VIRTUAL CONFIGURATION WORD.

extern int shoreCordRating;						// {30 to 250 amps}.
extern int alarmLevel;								// {50to100% of dsc_cordRating}.
extern unsigned alarmState;						// {DISABLED,ENABLED}.
extern int droop;
extern int droopEnable;
extern int slaveLoadShare;
extern int loadShareTable[100][3];	//dutyCycle,percentOfLoad(LOW_RANGE),percentOfLoad(HIGH_RANGE)
extern float lastMasterCapacity;
extern unsigned autoTransferOnOverload;
extern unsigned autoTransferGenset;
extern unsigned long droopDampTime;

extern volatile unsigned long lastWattTime;	//unsigned long wattTime in 10ms units.
extern volatile unsigned long wattTime;	//unsigned long wattTime in 10ms units.
extern float wattHours;
extern unsigned wattHourMeterON;

extern struct gp_state_struct gp_state;
extern struct mach_struct mach;

extern char *ship_name;
extern char *gp_cid;
extern volatile int gp_diagnostic_code;
extern volatile unsigned milliseconds;
extern volatile unsigned seconds;
extern volatile unsigned minutes;
extern volatile unsigned long hours;
extern volatile unsigned special_function;
extern volatile int gp_mode;
extern char *VER;
extern char *copyright;
extern char *sumstat[5];
extern char *system_status[3];
extern char *master_slave[2];
extern char *shutoffStr[15];
extern volatile unsigned flash;
extern int ALC_mode;
extern int AGC_state;
extern volatile int combo_key;

extern float gp_Van_DC[8], gp_Vbn_DC[8], gp_Vcn_DC[8];	/* RMS metered output value */
extern float gp_Van[8], gp_Vbn[8], gp_Vcn[8];			/* RMS metered output value */
extern float gp_Vab[8], gp_Vbc[8], gp_Vca[8];			/* RMS metered output value */
extern float gp_Ia_rms[8], gp_Ib_rms[8], gp_Ic_rms[8];	/* RMS metered output value */
extern float gp_Ia_pk[8], gp_Ib_pk[8], gp_Ic_pk[8]; 	/* peak currents */
extern float gp_KWa[8], gp_KWb[8], gp_KWc[8];			/* metered output value */
extern float gp_KVAa[8], gp_KVAb[8], gp_KVAc[8];	   	/* calculated output value */
extern float gp_PFa[8], gp_PFb[8], gp_PFc[8];			/* calculated output value */
extern float gp_ICFa[8], gp_ICFb[8], gp_ICFc[8];	  	/* calculated output value */
extern float gp_level_a[8], gp_level_b[8], gp_level_c[8];	  	/* calculated output Load Level (% rating) */
extern float gp_Freq[8];							  	/* calculated frequencye */

extern float maxSystemLevel;
extern float maxSystemPower;

extern float maxLevelIn1;
extern float maxPowerIn1;

extern float maxGenFreq;
extern float minGenFreq;

extern float averSystemLevel;
extern float averSystemPower;
extern float system_level;						//highest level of input/output A/B/C.

extern float system_temperature;
extern float positive_hvdc;
extern float negative_hvdc;

extern float Kout_A, Kout_B, Kout_C;						// correct Vout using calibration Kfactor.
extern float Kmeter_Vint_A, Kmeter_Vint_B, Kmeter_Vint_C;	// Correct Vmetering based on EXT ref cal.
extern float Kmeter_Vext_A, Kmeter_Vext_B, Kmeter_Vext_C;
extern float Kmeter_I_A, Kmeter_I_B, Kmeter_I_C;
extern float Kmeter_I_A2;
extern float Kmeter_I_A3;
extern float Kmeter_Vgen_A, Kmeter_Vgen_B, Kmeter_Vgen_C;	//Generator Voltage Kfactors.
extern float Kmeter_Igen_A, Kmeter_Igen_B, Kmeter_Igen_C;	//Generator Current Kfactors.
//extern float Kmeter_Vsp1_A, Kmeter_Vsp1_B, Kmeter_Vsp1_C;	//Shore Power #1 Voltage Kfactors.
//extern float Kmeter_Isp1_A, Kmeter_Isp1_B, Kmeter_Isp1_C;	//Shore Power #1 Current Kfactors.
extern float Kmeter_Vsp1_1, Kmeter_Vsp1_A, Kmeter_Vsp1_B, Kmeter_Vsp1_C;	//Shore Power #1 Voltage Kfactors.
extern float Kmeter_Isp1_1, Kmeter_Isp1_A, Kmeter_Isp1_B, Kmeter_Isp1_C;	//Shore Power #1 Current Kfactors.
extern float Kmeter_Vsp2_1, Kmeter_Vsp2_A, Kmeter_Vsp2_B, Kmeter_Vsp2_C;	//Shore Power #2 Voltage Kfactors.
extern float Kmeter_Isp2_1, Kmeter_Isp2_A, Kmeter_Isp2_B, Kmeter_Isp2_C;	//Shore Power #2 Current Kfactors.
extern float Kmeter_Vsys_A, Kmeter_Vsys_B, Kmeter_Vsys_C;	//System Voltage Kfactors.
extern float Kmeter_Isys_A, Kmeter_Isys_B, Kmeter_Isys_C;	//System Current Kfactors.
extern float Kmeter_Vaux_A, Kmeter_Vaux_B, Kmeter_Vaux_C;	//Aux In Voltage Kfactors.
extern float Kmeter_Iaux_A, Kmeter_Iaux_B, Kmeter_Iaux_C;	//Aux In Current Kfactors.
extern float Va_out, Vb_out, Vc_out;					   	// dynamic output voltage.
extern int gp_OverRange[8][8];								// Metering overrange indicators.

extern int sp1a_raw[8];	//first n samples of raw shore power voltage.
extern int sp1b_raw[8];
extern int sp1c_raw[8];
extern int sp2a_raw[8];
extern int sp2b_raw[8];
extern int sp2c_raw[8];
extern int sp2ba_raw[8];
extern int sp2cb_raw[8];
extern int sp2ac_raw[8];

extern volatile int first_time_through;
extern volatile unsigned shore_power_off_key; //v1.84

extern int autorestart;	
extern long int ser_baud_rate;		/* Baud rate */
extern unsigned remoteActive;

extern unsigned int debug, debug2, debug3;

#ifdef NO_OPTCON_PCB
extern volatile unsigned far int optcon_control;		// OPCON CONTROL WORD--DUMMY.
extern volatile unsigned far int optcon_control_2;		// OPCON CONTROL WORD 2.
#else												//CS3B(low)
extern volatile system unsigned int optcon_control;		//CS2*Y7*(low) OPCON CONTROL WORD.
extern volatile system unsigned int optcon_control_2;		//CS2*Y7*(low) OPCON CONTROL WORD 2.
#endif

extern int RAM_reset;           				//flag to indicate RAM has been reset.

extern volatile int display_type;
extern volatile int display_function;
//#ifdef MODBUS
extern unsigned modbusActive;
extern unsigned char modbusNodeId;
//#endif //MODBUS

extern unsigned char last_transfer_status;
extern char *getXfrErrCodeDescriptor( unsigned char );
extern int process_MULTIGEN_REM_XFR_REQ(void);	//v2.71
extern volatile unsigned transfer_in_progress;

extern unsigned autoShutdownEnable;		//M/Y BOUNTY HUNTER feature

extern float LmShedOffLevel;
extern float LmShedOnLevel;
extern float LmAddOnLevel;
extern float LmAddOffLevel;
extern unsigned LmShedOnDelay;
extern unsigned LmShedOffDelay;
extern unsigned LmAddOnDelay;
extern unsigned LmAddOffDelay;

extern unsigned OPTCON_PCB;		//opcon_status bit#15

extern Xfer_Imped_Ramp_Stat_Type Xfer_Imped_Ramp_Status;

extern unsigned int optcon_status_rd;
extern int gen_mux_id;	
extern float Debug_Var[];

//Northern Gen2Gen Test for Gen on Input of converter
/*---------------------------------------------------------------------------*/
_inline unsigned cb6_CLOSED()
{
	return  ((optcon_status_2 & 0x0010) == 0x0010);	//CB6 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
//multiGen stuff
/*---------------------------------------------------------------------------*/
_inline unsigned eGen1_CLOSED()
{
	return  ((optcon_status_2 & 0x0100) == 0x0100);	//GENERATOR N1 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned eGen2_CLOSED()
{
	return  ((optcon_status_2 & 0x0200) == 0x0200);	//GENERATOR N2 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned eGen3_CLOSED()
{
#ifdef TEST_FIXTURE
	return  ((gp_configuration_word & 0x4000)!=0);	//FOR DEBUG ONLY!!
#else
	return  ((optcon_status_2 & 0x0400) == 0x0400);	//GENERATOR FORE Confirmation signal.
#endif //TEST_FIXTURE
}
/*---------------------------------------------------------------------------*/
_inline unsigned eGen4_CLOSED()
{
	return  ((optcon_status_2 & 0x0800) == 0x0800);	//GENERATOR SPARE Confirmation signal.
}
/*---------------------------------------------------------------------------*/
//#ifdef XMFR_TAP_DISP

_inline unsigned XFMR_400V_Tap(void)
{
	if (OPTCON_PCB) {
		return (optcon_status & 0x4000);
//		return  ((optcon_status & 0x4000) == 0x4000);	//Transformer 400V Tap selected.
	}
	else
		return (_getbit (P5, 8));
		
//		return (_getbit (P5, 8));
}
/*---------------------------------------------------------------------------*/
_inline unsigned XFMR_480V_Tap(void)
{
	if (OPTCON_PCB) {
		return (optcon_status & 0x0200);
//		return  (optcon_status & 0x0200);	//Transformer 480V Tap selected.
	}
	else {
		return (_getbit (P5, 14));
	}
		
//		return (_getbit (P5, 14));
}
//#endif
/*---------------------------------------------------------------------------*/

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//@DISPLAY_MANAGER 		DISPLAY_MANAGER 		DISPLAY_MANAGER		DISPLAY_MANAGER		DISPLAY_MANAGER		DISPLAY_MANAGER
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
/*---------------------------------------------------------------------------*/
void display_metering(int meter_type, int meter_function)	// display active meter on 4x40 LCD.
{
	static int last_meter_type;
	static int last_meter_function;

	if ((meter_type != last_meter_type) || (meter_function != last_meter_function))
	{
		clear_screen();
		last_meter_type = meter_type;
		last_meter_function = meter_function;
	}

//	if (MULTIGEN_OPTION != TRUE)
//	{
//		clear_screen();			
//	}
//	else
//	{
//		if (meter_type != GENERATOR_POWER)
//		clear_screen();			
//	}

	if (DUAL_GENSET==NO)
	{
		gp_state.active_display=GEN1;

		if (meter_type==SYSTEM_POWER || meter_type==GENERATOR_POWER)
		{
			if (meter_function==F2) meter_function=F1;
		}
	}

	switch (meter_type)
	{
		case SHORE_POWER_1:								//SHORE POWER #1 DISPLAYS		
				switch (meter_function) 
				{
					case F1:	
#ifdef DUAL_SHORE_CORD
								display_2phase_power_f1(SP1);
#else
								display_power_f1(SP1);	
#endif
								break;
					case F2:	
#ifdef DUAL_SHORE_CORD
								display_2phase_power_f2(SP1);
#else
								display_power_f2(SP1);	
#endif
								break;
					case F3:	calibrate_sp1_input();	break;
					case F4:	display_power_f4(SP1);	break;
					case F5:	display_power_f5();	break;
//					case F6:	display_power_f6();		break;
					case F7:	display_power_f7(SP1);	break;
					case F8:	display_power_f8(SP1);	break;
					case F9:	display_power_f9(SP1);	break;
					case F10:	display_power_f10();	break;
					case F11:	display_power_f11();	break;
					default:	raise_exception(-27);	break;
				}
				gp_state.shore_power_viewed = TRUE;
				gp_state.active_display=OTHER;
				break;

		case SHORE_POWER_2:
				switch (meter_function) 					//SHORE POWER #2 DISPLAYS
				{
					case F1:	display_power_f1(SP2);	break;
					case F2:	display_power_f2(SP2);	break;
					case F3:	calibrate_sp2_input();	break;
					case F4:	display_power_f4(SP2);	break;
					case F5:	display_power_f5();	break;
//					case F6:	display_power_f6();		break;
					case F7:	display_power_f7(SP2);	break;
					case F8:	display_power_f8(SP2);	break;
					case F9:	display_power_f9(SP2);	break;
					case F10:	display_power_f10();	break;
					case F11:	display_power_f11();	break;
					default:	raise_exception(-27);	break;
				}
				gp_state.shore_power_viewed = TRUE;
				gp_state.active_display=OTHER;
				break;

		case GENERATOR_POWER:
				if (gp_state.local_master)
				{
					switch (meter_function) 				//GENERATOR POWER DISPLAYS
					{
						case F1:	
									if (MULTIGEN_OPTION) 
									{
										set_gen_mux(1);
//										clear_screen();
									}
									gp_state.active_display=GEN1;

									if (GEN_AMPS_PRESENT)
									{
										set_Igen_mux(GEN1);
										if (GP_OUTPUT_FORM==ONE_PHASE) display_output_power_f1(GEN);
										if (GP_OUTPUT_FORM==TWO_PHASE) display_2phase_power_f1(GEN);
										if (GP_OUTPUT_FORM==THREE_PHASE) display_power_f1(GEN);	
									}
									else
									{
										display_voltage_f1(GEN);
									}
									break;

						case F2:	
									if (MULTIGEN_OPTION)
									{
										set_gen_mux(2);
										gp_state.active_display=GEN1;
//										clear_screen();
										if (GEN_AMPS_PRESENT)
										{
											set_Igen_mux(GEN1);
											if (GP_OUTPUT_FORM==ONE_PHASE) display_output_power_f1(GEN);
											if (GP_OUTPUT_FORM==TWO_PHASE) display_2phase_power_f1(GEN);
											if (GP_OUTPUT_FORM==THREE_PHASE) display_power_f1(GEN);	
										}
										else
										{
											if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
											{
												display_voltage_f1(SYS);
												gp_state.active_display=GEN2;
											}
											else
											{
												display_voltage_f1(GEN);
												gp_state.active_display=GEN1;
											}
//											display_voltage_f1(GEN);
										}
									}
									else
									{
										gp_state.active_display=GEN2;
										if (GEN_AMPS_PRESENT)
										{
											set_Igen_mux(GEN2);
											if (GP_OUTPUT_FORM==ONE_PHASE) display_output_power_f1(SYS);
											if (GP_OUTPUT_FORM==TWO_PHASE) display_2phase_power_f1(SYS);
											if (GP_OUTPUT_FORM==THREE_PHASE) display_power_f1(SYS);	
										}
										else
										{
											display_voltage_f1(SYS);
										}
									}

									break;

						case F3:	calibrate_generator();
									break;

						case F4:	//F4 pressed
									if (NEW_G2G_OPTION)
									{
										synchroscope();
									}
									else
									{
										if (MULTIGEN_OPTION)
										{
											set_gen_mux(4);
										}
										if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
										{
											display_voltage_f1(SYS);
											gp_state.active_display=GEN2;
										}
										else
										{
											display_voltage_f1(GEN);
											gp_state.active_display=GEN1;
										}
									}
									break;

						case F5:	display_power_f5();	break;

						case F7:	//F3 pressed
									if (GEN_AMPS_PRESENT)
									{
										if (new_key!=0)
										{
											if (getIgen()==GEN2)	//returns 1 if Igen MUX set to GEN1 and 2 if set to GEN2.
											{
												display_power_f2(GEN);
												set_Igen_mux(GEN1);
											}
											else
											{
												display_power_f2(SYS);
												set_Igen_mux(GEN2);
											}
											delay(500);
											new_key=0;
										}

										if (getIgen()==GEN2)	//returns 1 if Igen MUX set to GEN1 and 2 if set to GEN2.
										{
											display_power_f2(SYS);
										}
										else
										{
											display_power_f2(GEN);
										}
									}
									else
									{
										if (MULTIGEN_OPTION)
										{
											set_gen_mux(3);
										}
										display_voltage_f1(GEN);
										gp_state.active_display=GEN1;
									}

									break;

						case F6:	
									if (gp_state.active_display==GEN1)
										display_voltage_f1(GEN);
									else
										display_voltage_f1(SYS);
									break;
						case F8:	
									if (gp_state.active_display==GEN1)
										display_voltage_f1(GEN);
									else
										display_voltage_f1(SYS);
									break;
						case F9:	
									if (gp_state.active_display==GEN1)
										display_voltage_f1(GEN);
									else
										display_voltage_f1(SYS);
									break;
						case F10:	display_power_f10();	break;
						case F11:	display_power_f11();	break;
						default:	raise_exception(-27);	break;
					}
				}
				else if (EXT_OSC_OPTION)	//v2.56, S3-6 configuration
				{
					lcd_display("EXTERNAL OSCILLATOR OPTION is ON.\0",1,1);
					lcd_display(" GENERATOR METERING IS NOT AVAILABLE\0",2,1);
					lcd_display(" WITH THE EXTERNAL OSCILLATOR OPTION.\0",3,1);
					lcd_display(" Press 'SYSTEM STATUS' key to clear msg.\0",4,1);
				}
				else
				{
					lcd_display("SLAVE CABINET.\0",1,1);
					lcd_display(" GENERATOR METERING MUST BE VIEWED FROM \0",2,1);
					lcd_display(" THE MASTER CABINET FRONT PANEL.\0",3,1);
					lcd_display(" Press 'SYSTEM STATUS' key to clear msg.\0",4,1);
				}
				break;

		case CONVERTER_POWER:							//CONVERTER POWER DISPLAYS
		
				switch (meter_function) 
				{
					case F1:	
								if (GP_OUTPUT_FORM==ONE_PHASE) display_output_power_f1(OSC);
								if (GP_OUTPUT_FORM==TWO_PHASE) display_2phase_power_f1(OSC);
								if (GP_OUTPUT_FORM==THREE_PHASE) display_power_f1(OSC);	
								break;
					case F2:	
								if (GP_OUTPUT_FORM==ONE_PHASE) display_output_power_f2(OSC);
								if (GP_OUTPUT_FORM==TWO_PHASE) display_2phase_power_f2(OSC);
								if (GP_OUTPUT_FORM==THREE_PHASE) display_power_f2(OSC);	
								break;
					case F3:	calibrate_converter();	break;
					case F4:	
								display_power_f4(OSC);	
								break;
					case F5:	display_power_f5();	break;
//					case F6:	display_power_f6();		break;
					case F7:	display_power_f7(OSC);	break;
					case F8:	display_power_f8(OSC);	break;
					case F9:	display_power_f9(OSC);	break;
					case F10:	display_power_f10();	break;
					case F11:	display_power_f11();	break;
					default:	raise_exception(-27);	break;
				}
				gp_state.active_display=OTHER;
				break;
								 
		case SYSTEM_POWER:							//CONVERTER POWER DISPLAYS
				switch (meter_function) 
				{
					case F1:	display_voltage_f1(GEN);
								gp_state.active_display=GEN1;
								break;

					case F2:	display_voltage_f1(SYS);
								gp_state.active_display=GEN2;
								break;

					case F3:	calibrate_generator();
								break;
					case F4:	display_power_f4(SYS);	break;
					case F5:	display_power_f5();	break;
//					case F6:	display_power_f6();		break;
					case F7:	display_power_f7(SYS);	break;
					case F8:	display_power_f8(SYS);	break;
					case F9:	display_power_f9(SYS);	break;
					case F10:	display_power_f10();	break;
					case F11:	display_power_f11();	break;
					default:	raise_exception(-27);	break;
				}
				gp_state.active_display=GEN2;
				break;

		case EVENT_LOG:
				display_power_f5();
				break;

		case SYSTEM_STATUS:
				show_summary_display();
				gp_state.active_display=OTHER;
				break;

		case STATUS2:	
				if(gp_state.sysstat)
				{
					show_status_display();
				}
				else
				{
					show_status_display();
				}
				break;

		case HELP:	
				if(gp_state.sysstat)
				{
					raise_exception(gp_diagnostic_code);

					if (flash)
						lcd_display("       \0",1,26);
					else
						lcd_display(system_status[gp_state.sysstat],1,26);
				}
				else
				{
					show_sysstat_OK();
				}
				break;

		case INFO:	
				show_shutdown_info();
				break;

		case BUS:
				besecke_transfer_info_display();
				break;

		case EDIT_SERIAL_COMM_PARAMS:	
				edit_serial_comm_config();
				break;

		default://invalid 'display_typ'e, something wrong, so, show system status.	
				if(gp_state.sysstat)
				{
//					raise_exception(-27);
					display_function = F1; 
					display_type = SYSTEM_STATUS; 
					new_key=0;
					gp_mode=NORMAL_MODE;
				}
				else
				{
					show_sysstat_OK();
				}
				break;
	}
}
/*---------------------------------------------------------------------------*/
//
/* code from LVR to calculate CC0 & CC1 without floating point variables.
//
	if (9618 < deltaT6 && deltaT6 < 13893)	//45 to 65 Hz test.  //abs.max.deltaT6=21474
	{
		ul_freq = (unsigned long) deltaT6 * 200000 / 6252;	//CLK/100 / TICK_COUNT/100 to stay in range of u-long.
		ul_pwm = ul_freq>>10;						//1024 spc
		ul_freq = (ul_freq - (ul_pwm<<10)) >> 1;	//1024 spc
		if (ul_freq==0) cc1_cs = 32768;
		else cc1_cs = (unsigned) 65536 - ul_freq;
		pwm_f = (unsigned) ul_pwm - 1;
		pwm_s = (unsigned) ul_pwm;
		CC0 = cc1_cs - 512;							//1024 spc
		CC1 = cc1_cs;
		Track = 1;
	}
/*---------------------------------------------------------------------------*/
void synchroscope()
{
  unsigned inRange=NO;
  int localZero=(int)(sync_count & 0xFF);
  int remoteZero=(int)(sync_1_count & 0xFF);
  int deltaZero=remoteZero-localZero;
  float localFreq=gp_Freq[GEN];
  float remoteFreq;
  float fDelta;
  char *phaseDelta[4]={	"IN SYNC\0",
			   			"LEADING\0",
			   			"LAGGING\0",
			   			"\0000.3 Hz\0"};

	clear_screen();

	lcd_display("\03\02\04\03  SYNCHROSCOPE\0",1,12);
	lcd_display("GENSET #1                      GENSET #2\0",2,1);
	lcd_display("      Hz           \06\07                 Hz\0",3,1);

//
// keep T0 count reasonably relative to local gen freq for synchroscope.
//
	if (localFreq > 40.0 && localFreq < 70.0)
	{
		if (localFreq > (newFrequency+0.3) || localFreq < (newFrequency-0.3))
		{
//			freq_calc(gmm_Freq[GEN]);	// output frequency calculation
		}
	}
	else
	{
		lcd_display("-BAD-\0",3,1);
		lcd_display("                                        \0",4,1);
		return;
	}

	if (deltaT5gen1 > 0)				
	{					 
		localFreq 	= 	TICK_COUNT/(float)deltaT5gen1;	//calculate frequency from tick count.
	}

	if (deltaT5gen2 > 0)				
	{					 
		remoteFreq 	= 	TICK_COUNT/(float)deltaT5gen2;	//calculate frequency from tick count.
	}
	else
	{
		remoteFreq=0.0;
	}
//
//clear period counts for next pass (in case signal goes away).
	deltaT5gen2=0;

	if ((remoteFreq < (localFreq-5.0)) || (remoteFreq > (localFreq+5.0)))
	{
		lcd_display("-BAD-\0",3,34);
		lcd_display("                                        \0",4,1);
		return;
	}
//
//show local & remote frequency
//
	lcd_display_value(localFreq,3,1,"%5.2f\0");
	lcd_display_value(remoteFreq,3,33,"%5.2f\0");

	if (deltaZero>128) deltaZero = deltaZero-256;

	fDelta = (float)deltaZero;
	fDelta = fDelta/6.4;

	if (fDelta < (-20.0)) 	fDelta = fDelta + 40.0;	//keep on scale.
	if (fDelta > 20.0) 	fDelta = fDelta - 40.0;	//keep on scale.

	deltaZero = (int)fDelta;

	lcd_display_value((fDelta*9.0),2,23,"%4.0f�\0");

	if (localFreq < (remoteFreq-0.3))
	{
		lcd_display(phaseDelta[2],2,15);	//lagging
		inRange=NO;
	}
	else if (localFreq > (remoteFreq+0.3))
	{
		lcd_display(phaseDelta[1],2,15);	//leading
		inRange=NO;
	}
	else
	{
		lcd_display(phaseDelta[3],2,15);	// +/-0.3
		inRange=YES;
	}

	lcd_display("                                        \0",4,1);

	if (deltaZero<0)
	{
		deltaZero = deltaZero + 19;
		if (0<deltaZero && deltaZero<41) lcd_display("\05\0",4,deltaZero);
	}
	else if (deltaZero>0)
	{
		deltaZero = deltaZero + 20;
		if (0<deltaZero && deltaZero<41) lcd_display("\05\0",4,deltaZero);
	}
	else
	{	
		if (inRange==YES) lcd_display(phaseDelta[0],2,15);	//in sync
		lcd_display("\05\05\0",4,20);
	}
}
/*---------------------------------------------------------------------------*/
/*
void show_graphicalPower_display()
{
  int i;
  float fCols=system_level/3;
  unsigned lastCol=(unsigned)fCols;

	clear_screen();			

	lcd_display("Dynamic Power Meter\0",1,19);

	for (i=1;i<lastCol;i++)
	{
		lcd_display("\05\0",3,i);	//fill in to Level (3% per col).
	}
}
/*---------------------------------------------------------------------------*/
void show_summary_display()
{
	clear_screen();			
	lcd_display("SUMMARY DISPLAY\0",1,1);
#ifdef DUAL_SHORE_CORD
	lcd_display("INPUT#1:              CONVERTER:        \0",2,1);
	lcd_display("INPUT#2:                                \0",3,1);
	lcd_display("LOAD LEVEL:      %\0",4,1);
#else
	lcd_display("INPUT:               CONVERTER:         \0",2,1);
	lcd_display("LOAD LEVEL:      %\0",3,1);
#endif //~DUAL_SHORE_CORD
	lcd_display("INFO\0",4,28);

	if (MULTIGEN_BESECKE)
		lcd_display("BUS\0",4,20);
////////////////////////////////////////////////////////////////
//				 1234567 1234567 12345678 1234567 1234567
////////////////////////////////////////////////////////////////

	if (MODEL_LC) {
		lcd_display(gp_cid,1,26);
		lcd_display("LC", 1, 31);
	}
	else
		lcd_display(gp_cid,1,26);
		
	lcd_display(sumstat[gp_state.invstat],2,34);

#ifdef DUAL_SHORE_CORD
	lcd_display(sumstat[gp_state.inpstat1],2,11);
	lcd_display(sumstat[gp_state.inpstat2],3,11);
	lcd_display_value(system_level,4,13,"%4.1f\0");
#else
	lcd_display(sumstat[gp_state.inpstat1],2,9);
	lcd_display_value(system_level,3,13,"%4.1f\0");
#endif //~DUAL_SHORE_CORD

	if (gp_state.local_master)
	{
		lcd_display("MASTER\0",1,34);

		if (!EXT_CB_OPTION || shore_power_off_key)
		{
			lcd_display("AUTO-RESTART:\0",3,23);

			if (REMOTE_RUN)
			{
				lcd_display(" REM\0",3,36);
			}
			else if (autorestart)
			{
				lcd_display(" ON \0",3,36);
			}
			else
			{
				lcd_display(" OFF\0",3,36);
			}
		}
	}
	else
	{
		lcd_display("SLAVE \0",1,34);
	}

	if (remoteActive)
	{
		lcd_display("RC\0",1,21);
		remoteActive=NO;
	}
	else
	{
		lcd_display("   \0",1,21);
	}

//	lcd_display(utoa(debug),4,1);
//	lcd_display(utoa(OPTCON_PCB),4,21);
//	lcd_display(utoa(optcon_status),4,14);
//	lcd_display(utoa(optcon_status_rd),4,21);
//#ifdef XMFR_TAP_DISP
#ifndef DUAL_SHORE_CORD
	if (gp_state.inpstat1 != 1) {
		XFMR_Tap_Selected = 0;
		XFMR_Tap_Check = 0;		
	}
	else if (gp_state.inpstat1 != gp_state_save) {
		XFMR_Tap_Delay_Rd = 10;							//100msec delay
	}
	else if ((!XFMR_Tap_Check) && (!XFMR_Tap_Delay_Rd)) {
//	else if (!XMFR_Tap_Check) {
		if (XFMR_400V_Tap()) {
			if (XFMR_480V_Tap())
				XFMR_Tap_Selected = 0xFF;
			else
				XFMR_Tap_Selected = 0x01;
		}
		else if (XFMR_480V_Tap())
			XFMR_Tap_Selected = 0x02;
		else
			XFMR_Tap_Selected = 0;
			
//		if (test_tap_sel == 1) {
//			XMFR_Tap_Selected = 0x01;
//			test_tap_sel = 2;
//		}
//		else if (test_tap_sel == 2) {
//			XMFR_Tap_Selected = 0x02;
//			test_tap_sel = 3;
//		}
//		else if (test_tap_sel == 3) {
//			XMFR_Tap_Selected = 0xFF;
//			test_tap_sel = 1;
//		}
	
		XFMR_Tap_Check = 0xFF;
	}
	
	if (XFMR_Tap_Selected == 0x01)
		lcd_display("XFMR: 400V", 4, 1);
	else if (XFMR_Tap_Selected == 0x02)
		lcd_display("XFMR: 480V", 4, 1);
	else if (XFMR_Tap_Selected == 0xFF)
		lcd_display("XFMR: ERR ", 4, 1);
	else
		lcd_display("          ", 4, 1);
			
	gp_state_save = gp_state.inpstat1;
	
//	lcd_display(utoa(XMFR_Tap_Check),4,13);
//	lcd_display(utoa(gp_state.inpstat1),4,19);

#endif
//#endif

//	lcd_display(utoa(debug),4,13);
//	lcd_display(utoa(debug2),4,19);
//	lcd_display(utoa(OPTCON_PCB),4,19);
//	lcd_display(utoa(optcon_status),4,22);
//
//	lcd_display_value (gen_mux_id, 4, 20, "%5d\0");
//	lcd_display_value (gp_state.busState, 4, 26, "%5d\0");
	

//	lcd_display(utoa(Xfer_Imped_Ramp_Status.all),4,19);

//	lcd_display_value(Debug_Var[0],4,1,"%4.0f\0");
//	lcd_display_value(Debug_Var[1],4,10,"%4.0f\0");
	
	if (flash)
		lcd_display(" STATUS\0",4,34);
	else
		lcd_display(system_status[gp_state.sysstat],4,34);	//char *system_status[3]={	"  OK   ","WARNING","FAILURE"};

	if (first_time_through==2)
	{
		first_time_through=0;
		delay(2000);	//stall 2 seconds on this display if autorestart state changes.
	}
	
	if (display_function==F4)
	{
		display_type = INFO;
		display_function = F1;
	}
	
	if (MULTIGEN_BESECKE && display_function==F7)
	{
		display_type = BUS;
		display_function = F1;
	}
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void show_shutdown_info()
{
/*
  char *shutoffStr[15]={"INPUT is ONLINE",					//0
			   			"SHORE POWER OFF KEY PRESSED",		//1
			   			"REMOTE SHORE POWER OFF",			//2
			   			"REMOTE EMERGENCY POWER OFF",		//3
			   			"BLACKOUT OR RED-EMERGENCY SWITCH",	//4
			   			"DOCK POWER BROWNOUT SHUTDOWN",		//5
			   			"OVERTEMPERATURE SHUTDOWN",			//6
			   			"OVERLOAD SHUTDOWN",				//7
			   			"INPUT POWER FORM CHANGE SHUTDOWN",	//8
			   			"HVDC > 210 SHUTDOWN",				//9
			   			"PFC FAULT SHUTDOWN",				//10, not used.
			   			"LOW-VOLTAGE DC SHUTDOWN",			//11
			   			"GROUND FAULT SHUTDOWN",			//12
			   			"INVERTER FAULT SHUTDOWN",			//13, not used.
			   			"Vout OK LOW, OUTPUT SHUTDOWN"};	//14, not used.
*/
////////////////////////////////////////////////////////////////
//				 1234567 1234567 12345678 1234567 1234567
////////////////////////////////////////////////////////////////
	lcd_display("CONVERTER SHUTDOWN INFO DISPLAY\0",1,1);
	lcd_display("                                        \0",3,1);

	if (gp_state.inpstat1==OFFLINE && !shutoffId) shutoffId=4;

	lcd_display(shutoffStr[shutoffId],3,1);
	
	if (display_function==F7)
	{
		lcd_display("LAST FAILURE:             \0",4,1);
		lcd_display(getErrOnCodeDescriptor(last_failure_code),4,15);
	}
}
/*---------------------------------------------------------------------------*/
void show_time(int row, int col)
{
//	lcd_display("HHHHH:MM:SS",row,col);
	lcd_display("     :00:00 \0",row,col);
	lcd_display_value(hours,row,col,"%5.0f\0");
	lcd_display_value(minutes,row,col+6,"%2.0f\0");
	lcd_display_value(seconds,row,col+9,"%2.0f\0");
//	lcd_display_value(milliseconds,row,col+11,"%3.0f");

	if (minutes<10) lcd_display("0\0",row,col+6);
	if (seconds<10) lcd_display("0\0",row,col+9);
}
/*---------------------------------------------------------------------------*/
void show_signon_screen()
{
	show_info();
} 
/*---------------------------------------------------------------------------*/
void show_info()
{
	clear_screen();	//added v1.40

//	lcd_display("\
//                                        \n\
// AC MARINE POWER CONVERTER Version x.xx \n\
//       (c)2004 ASEA POWER SYSTEMS       \n\
//                                        ",1,1);

	lcd_display(" AC MARINE POWER CONVERTER Version\0",2,1);
	lcd_display(VER,2,36);
	lcd_display(copyright,3,1);
}
/*---------------------------------------------------------------------------*/
void show_status_display()
{
//  unsigned GP_CONFIG_WORD=gp_configuration_word;	// GP CONFIGURATION WORD into image.
  unsigned GP_CONFIG_WORD=VIRTUALconfiguration_word;	// Virtual CONFIGURATION WORD.
  unsigned GP_STATUS_WORD_1=gp_status_word;		// GP STATUS WORD1--into image.
  unsigned GP_STATUS_WORD_2=gp_status_word2;	// GP STATUS WORD2--into image.
  unsigned GP_STATUS_WORD_3=gp_status_word3;	// GP STATUS WORD2--into image.
  unsigned i;

//	lcd_display("\
//BIT: FEDCBA9876543210      CONFIG:  0000\n\
//SW1: ----------------      STATUS1: 0000\n\
//SW2: ----------------      STATUS2: 0000\n\
//SW3: ----------------      STATUS3: 0000",1,1);

	lcd_display("\
BIT: FEDCBA9876543210      CON:         \n\
SW1: ----------------      STATUS1:     \n\
SW2: ----------------      STATUS2:     \n\
SW3: ----------------      STATUS3:     \0",1,1);

	for (i=0;i<16;i++)
	{
		if (GP_STATUS_WORD_1 & (0x8000>>i))
		{
			lcd_display("1\0",2,6+i);
		}
		else
		{
			lcd_display("0\0",2,6+i);
		}

		if (GP_STATUS_WORD_2 & (0x8000>>i))
		{
			lcd_display("1\0",3,6+i);
		}
		else
		{
			lcd_display("0\0",3,6+i);
		}

		if (GP_STATUS_WORD_3 & (0x8000>>i))
		{
			lcd_display("1\0",4,6+i);
		}
		else
		{
			lcd_display("0\0",4,6+i);
		}
	}
	lcd_display(utoX(GP_CONFIG_WORD),1,33);
//	lcd_display(utoX(gp_configuration_word2),1,37);
	lcd_display(utoX(VIRTUALconfiguration_word2),1,37);
	lcd_display(utoX(GP_STATUS_WORD_1),2,37);
	lcd_display(utoX(GP_STATUS_WORD_2),3,37);
	lcd_display(utoX(GP_STATUS_WORD_3),4,37);

	if(gp_state.sysstat)
	{
		lcd_display("             \0",1,28);	//clear config hexvalue.

		if (flash)
			lcd_display("       \0",1,28);
		else
			lcd_display(system_status[gp_state.sysstat],1,28);

//		flash = flash^1;							//toggle flash msg flag.
	}
}
/*---------------------------------------------------------------------------*/
void show_sysstat_OK()
{
#ifdef DEBUG
  int i;
#endif //DEBUG

	clear_screen();			

	show_info();
	lcd_display(ship_name,1,1);
	lcd_display("SYSTEM STATUS: OK\0",1,24);

#ifdef DEBUG
	lcd_display("CONFIGURATION_WORD: \0",1,24);

	for (i=0;i<16;i++)
	{
		if (gp_configuration_word & (0x8000>>i))
		{
			lcd_display("1\0",4,21+i);
		}
		else
		{
			lcd_display("0\0",4,21+i);
		}
	}
#else
	lcd_display("HOURS OF OPERATION: \0",4,6);
	show_time(4,25);
#endif //DEBUG

}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
int display_2phase_power_f1(int mux_id)
{
	if (mux_id==SP1)
	{
		if (DUAL_INPUT)
		{
			lcd_display("\
SHORE POWER    INPUT#1   INPUT#2      Hz\n\
  VOLTAGE:           V         V        \n\
  CURRENT:           A         A        \n\
     LOAD:         kVA       kVA        \0",1,1);
		}
		else
		{
			lcd_display("\
SHORE POWER      INPUT                Hz\n\
  VOLTAGE:           V                  \n\
  CURRENT:           A                  \n\
     LOAD:         kVA                  \0",1,1);
		}
	}

	if (mux_id==OSC)
	{
		lcd_display("\
CONVERTER     OUTPUT#1  OUTPUT#2      Hz\n\
  VOLTAGE:           V         V        \n\
  CURRENT:           A         A        \n\
     LOAD:         kVA       kVA        \0",1,1);
	 }

	if (gp_Freq[mux_id]<100.0)
	{
		lcd_display_value(gp_Freq[mux_id],1,35,"%4.1f\0");
	}
	else
	{
		lcd_display("INVALID\0",1,34);
	}

	if (gp_OverRange[mux_id][A_VOLTS])
	{
		lcd_display("v\0",2,22);
	}
	lcd_display_value(gp_Van[mux_id],2,17,"%3.0f\0");

	if (gp_OverRange[mux_id][A_AMPS])
	{
		lcd_display("a\0",3,22);
	}
	lcd_display_value(gp_Ia_rms[mux_id],3,17,"%3.0f\0");

	lcd_display_value(gp_KVAa[mux_id],4,16,"%3.1f\0");

	if (DUAL_INPUT)
	{
		if (gp_OverRange[mux_id][B_VOLTS])
		{
			lcd_display("v\0",2,32);
		}
		lcd_display_value(gp_Vbn[mux_id],2,27,"%3.0f\0");

		if (gp_OverRange[mux_id][B_AMPS])
		{
			lcd_display("a\0",3,32);
		}
		lcd_display_value(gp_Ib_rms[mux_id],3,27,"%3.0f\0");

		lcd_display_value(gp_KVAb[mux_id],4,26,"%3.1f\0");
	}

	if (mux_id==OSC)
	{
		if (gp_OverRange[mux_id][B_VOLTS])
		{
			lcd_display("v",2,32);
		}
		lcd_display_value(gp_Vbn[mux_id],2,27,"%3.0f\0");

		if (gp_OverRange[mux_id][B_AMPS])
		{
			lcd_display("a",3,32);
		}
		lcd_display_value(gp_Ib_rms[mux_id],3,27,"%3.0f\0");

		lcd_display_value(gp_KVAb[mux_id],4,26,"%3.1f\0");
	}

	return mux_id;
}
/*---------------------------------------------------------------------------*/
int display_2phase_power_f2(int mux_id)
{
	if (mux_id==SP1 || mux_id==SP2)
	{
		if (DUAL_INPUT)
		{
		lcd_display("\
SHORE POWER    INPUT#1   INPUT#2      Hz\n\
     POWER:         kW        kW        \n\
PWR FACTOR:         PF        PF        \n\
LOAD LEVEL:          %         %        \0",1,1);
		}
		else
		{
		lcd_display("\
SHORE POWER      INPUT                Hz\n\
     POWER:         kW                  \n\
PWR FACTOR:         PF                  \n\
LOAD LEVEL:          %                  \0",1,1);
		}

		lcd_display_value(gp_KWa[mux_id],2,17,"%3.1f\0");
		lcd_display_value(gp_PFa[mux_id],3,17,"%3.1f\0");
		lcd_display_value(gp_level_a[mux_id],4,18,"%4.1f\0");

		if (DUAL_INPUT)
		{
			lcd_display_value(gp_KWb[mux_id],2,27,"%3.1f\0");
			lcd_display_value(gp_PFb[mux_id],3,27,"%3.1f\0");
			lcd_display_value(gp_level_b[mux_id],4,28,"%4.1f\0");
		}
	}

	if (mux_id==OSC)
	{
		lcd_display("\
CONVERTER     OUTPUT#1  OUTPUT#2      Hz\n\
     POWER:         kW        kW        \n\
PWR FACTOR:         PF        PF        \n\
LOAD LEVEL:          %         %        \0",1,1);

		lcd_display_value(gp_KWa[mux_id],2,17,"%3.1f\0");
		lcd_display_value(gp_PFa[mux_id],3,17,"%3.1f\0");
		lcd_display_value(gp_level_a[mux_id],4,18,"%4.1f\0");

		lcd_display_value(gp_KWb[mux_id],2,27,"%3.1f\0");
		lcd_display_value(gp_PFb[mux_id],3,27,"%3.1f\0");
		lcd_display_value(gp_level_b[mux_id],4,28,"%4.1f\0");
	}

	if (gp_Freq[mux_id]<100.0)
	{
		lcd_display_value(gp_Freq[mux_id],1,35,"%4.1f\0");
	}
	else
	{
		lcd_display("INVALID\0",1,34);
	}

	return mux_id;
}
/*---------------------------------------------------------------------------*/
int display_power_f1(int mux_id)
{
  int line_to_line_only=FALSE;

//	clear_screen();
													  
	if (mux_id==SP1 || mux_id==SP2 || (mux_id==OSC && DELTA_OUTPUT) || (mux_id==GEN && DELTA_OUTPUT) || (mux_id==SYS && DELTA_OUTPUT))
	{
		line_to_line_only=TRUE;

		lcd_display("\
          PHASE A-B PHASE B-C PHASE C-A \n\
  VOLTAGE:        V         V         V \0",1,1);

	}
	else
	{
		lcd_display("\
            PHASE A   PHASE B   PHASE C \n\
  VOLTAGE:    /   V     /   V     /   V \0",1,1);
	}

	lcd_display("\
  CURRENT:        A         A         A \n\
     LOAD:      kVA       kVA       kVA \0",3,1);


	lcd_display(get_display_name(mux_id),1,1);

	if (gp_OverRange[mux_id][A_VOLTS])
	{
		lcd_display("v\0",2,19);
	}
	if (!line_to_line_only)
		lcd_display_value(gp_Van[mux_id],2,12,"%3.0f\0");
 	lcd_display_value(gp_Vab[mux_id],2,16,"%3.0f\0");

	if (gp_OverRange[mux_id][B_VOLTS])
	{
		lcd_display("v\0",2,29);
	}
	if (!line_to_line_only)
		lcd_display_value(gp_Vbn[mux_id],2,22,"%3.0f\0");
	lcd_display_value(gp_Vbc[mux_id],2,26,"%3.0f\0");

	if (gp_OverRange[mux_id][C_VOLTS])
	{
		lcd_display("v\0",2,39);
	}
	if (!line_to_line_only)
		lcd_display_value(gp_Vcn[mux_id],2,32,"%3.0f\0");
	lcd_display_value(gp_Vca[mux_id],2,36,"%3.0f\0");

	if (gp_OverRange[mux_id][A_AMPS])
	{
		lcd_display("a\0",3,19);
	}
	lcd_display_value(gp_Ia_rms[mux_id],3,16,"%3.0f\0");

	if (gp_OverRange[mux_id][B_AMPS])
	{
		lcd_display("a\0",3,29);
	}
	lcd_display_value(gp_Ib_rms[mux_id],3,26,"%3.0f\0");

	if (gp_OverRange[mux_id][C_AMPS])
	{
		lcd_display("a\0",3,39);
	}
	lcd_display_value(gp_Ic_rms[mux_id],3,36,"%3.0f\0");

	lcd_display_value(gp_KVAa[mux_id],4,13,"%3.1f\0");
	lcd_display_value(gp_KVAb[mux_id],4,23,"%3.1f\0");
	lcd_display_value(gp_KVAc[mux_id],4,33,"%3.1f\0");
	

	return mux_id;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
int display_power_f2(int mux_id)
{

		if (mux_id==SP1 || mux_id==SP2 || (mux_id==OSC && DELTA_OUTPUT) || (mux_id==GEN && DELTA_OUTPUT) || (mux_id==SYS && DELTA_OUTPUT))
		{
			lcd_display("\
          PHASE A-B PHASE B-C PHASE C-A\n\
    POWER:       kW        kW        kW\n\
PWR FACTR:                             \n\
FREQUENCY:       Hz       FORM: 3-PHASE\0",1,1);
		}
		else
			lcd_display("\
		           PHASE A   PHASE B   PHASE C\n\
    POWER:       kW        kW        kW\n\
PWR FACTR:                             \n\
FREQUENCY:       Hz       FORM: 3-PHASE\0",1,1);


		lcd_display(get_display_name(mux_id),1,1);

		lcd_display_value(gp_KWa[mux_id],2,14,"%3.1f\0");
		lcd_display_value(gp_KWb[mux_id],2,24,"%3.1f\0");
		lcd_display_value(gp_KWc[mux_id],2,34,"%3.1f\0");

//		if (mux_id==SP1) {
//			lcd_display_value(Debug_Var[0],3,14,"%4.2f\0");
//			lcd_display_value(Debug_Var[1],3,20,"%4.2f\0");
//			lcd_display_value(Debug_Var[2],3,30,"%4.2f\0");
//		}
//		else {
			lcd_display_value(gp_PFa[mux_id],3,14,"%4.2f\0");
			lcd_display_value(gp_PFb[mux_id],3,24,"%4.2f\0");
			lcd_display_value(gp_PFc[mux_id],3,34,"%4.2f\0");
//		}

		

		
		if (gp_Freq[mux_id]<100.0)
		{
			lcd_display_value(gp_Freq[mux_id],4,13,"%4.1f\0");
		}
		else
		{
			lcd_display("INVALID\0",4,13);
		}

		if (mux_id==SP1)
		{
			if (gp_state.input1_form==INVALID)
			{
				lcd_display("INVALID\0",4,33);
			}
			else
			{
				lcd_display(itoa(gp_state.input1_form),4,33);
			}
		}

		if (mux_id==SP2)
		{
			if (gp_state.input2_form==INVALID)
			{
				lcd_display("INVALID\0",4,33);
			}
			else
			{
				lcd_display(itoa(gp_state.input2_form),4,33);
			}
		}

	return mux_id;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
int display_power_f4(int mux_id)
{
  float KVAR_a=sqrt((gp_KVAa[mux_id]*gp_KVAa[mux_id])-(gp_KWa[mux_id]*gp_KWa[mux_id]));
  float KVAR_b=sqrt((gp_KVAb[mux_id]*gp_KVAb[mux_id])-(gp_KWb[mux_id]*gp_KWb[mux_id]));
  float KVAR_c=sqrt((gp_KVAc[mux_id]*gp_KVAc[mux_id])-(gp_KWc[mux_id]*gp_KWc[mux_id]));

#ifdef DUAL_SHORE_CORD
		if (mux_id==SP1)
		{
			if (gp_state.inpstat2==ONLINE)
			{
				goto twoPhase;
			}
			else
			{
				goto onePhase;
			}
		}
#endif //DUAL_SHORE_CORD

	if ((GP_OUTPUT_FORM==TWO_PHASE) && (mux_id==OSC || mux_id==GEN || mux_id==SYS))
	{
#ifdef DUAL_SHORE_CORD
twoPhase:
#endif
		lcd_display("\
                 %         %           \n\
Power:           kW        kW          \n\
Load:           kVA       kVA          \n\
KVAR:           kVAR      kVAR         \0",1,1);
//Irms:             A         A          ",1,1);

		lcd_display_value(gp_level_b[mux_id],1,23,"%4.1f\0");
		lcd_display_value(gp_KWb[mux_id],2,22,"%4.2f\0");
		lcd_display_value(gp_KVAb[mux_id],3,22,"%4.2f\0");
//		lcd_display_value(gp_Ib_rms[mux_id],4,22,"%4.2f");
		lcd_display_value(KVAR_b,4,22,"%4.2f\0");
	}
	else if (((GP_OUTPUT_FORM==ONE_PHASE) && (mux_id==OSC || mux_id==GEN || mux_id==SYS)) || ((gp_state.input1_form==ONE_PHASE) && mux_id==SP1)) 
	{
#ifdef DUAL_SHORE_CORD
onePhase:
#endif
		lcd_display("\
                 % OF RATED LOAD\n\
Power:           kW\n\
Load:           kVA\n\
KVAR:           kVAR\0",1,1);
//Irms:             A",1,1);
	}
	else
	{
		lcd_display("\
                 %A        %B        %C\n\
Power:           kW        kW        kW\n\
Load:           kVA       kVA       kVA\n\
KVAR:           kVAR      kVAR      kVAR\0",1,1);
//Irms:             A         A         A",1,1);

		lcd_display_value(gp_level_b[mux_id],1,23,"%4.1f\0");
		lcd_display_value(gp_level_c[mux_id],1,33,"%4.1f\0");
		
//		if (mux_id == OSC) {
//			lcd_display_value(Debug_Var[3],2,22,"%4.2f\0");
//			lcd_display_value(Debug_Var[4],3,22,"%5.1f\0");
//			lcd_display_value(Debug_Var[5],4,22,"%4.2f\0");
//		}
//		else {
			lcd_display_value(gp_KWb[mux_id],2,22,"%4.2f\0");
			if (gp_KVAb[mux_id] >= 100.0)
				lcd_display_value(gp_KVAb[mux_id],3,22,"%5.1f\0");
			else
				lcd_display_value(gp_KVAb[mux_id],3,22,"%4.2f\0");
			
			lcd_display_value(KVAR_b,4,22,"%4.2f\0");
//		}
		
		lcd_display_value(gp_KWc[mux_id],2,32,"%4.2f\0");
		
		if (gp_KVAc[mux_id] >= 100.0)
			lcd_display_value(gp_KVAc[mux_id],3,32,"%5.1f\0");
		else
			lcd_display_value(gp_KVAc[mux_id],3,32,"%4.2f\0");
//		lcd_display_value(gp_Ib_rms[mux_id],4,22,"%4.2f");
//		lcd_display_value(gp_Ic_rms[mux_id],4,32,"%4.2f");
		lcd_display_value(KVAR_c,4,32,"%4.2f\0");
	}

	lcd_display(get_display_name(mux_id),1,1);

	lcd_display_value(gp_level_a[mux_id],1,13,"%4.1f\0");

//	if (mux_id == OSC) {
//		lcd_display_value(Debug_Var[0],2,12,"%4.2f\0");
//		lcd_display_value(Debug_Var[1],3,12,"%5.1f\0");
//		lcd_display_value(Debug_Var[2],4,12,"%4.2f\0");
//	}
//	else {
		lcd_display_value(gp_KWa[mux_id],2,12,"%4.2f\0");
		
		if (gp_KVAa[mux_id] >= 100.0)
			lcd_display_value(gp_KVAa[mux_id],3,12,"%5.1f\0");
		else
			lcd_display_value(gp_KVAa[mux_id],3,12,"%4.2f\0");
			
	//	lcd_display_value(gp_Ia_rms[mux_id],4,12,"%4.2f");
		lcd_display_value(KVAR_a,4,12,"%4.2f\0");
//	}
	
	return mux_id;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void display_power_f5()
{
	eventLogTracker();
}
/*---------------------------------------------------------------------------*/
void display_power_f10()
{
	extreme_values_screen();
}
/*---------------------------------------------------------------------------*/
void display_power_f11()
{
	wattHour_screen();
}
/*---------------------------------------------------------------------------*/
/*
void display_power_f6()
{												   
		lcd_display(" ASEA     WARE DESIGNED & DEVELOPED BY: \0",1,1);

//	if (flash)
	{
		lcd_display("FIRM\0",1,7);
		lcd_display("             DAVE  NEWBERRY             \0",2,1);
		lcd_display("              714.329.7522              \0",3,1);
	}

	lcd_display("HOURS OF OPERATION: \0",4,6);
	show_time(4,25);

	delay(1000);
}
/*---------------------------------------------------------------------------*/
int display_power_f7(int mux_id)
{
#ifdef DUAL_SHORE_CORD
		if (mux_id==SP1)
		{
			if (gp_state.inpstat2==ONLINE)
			{
				goto TwoPhase;
			}
			else
			{
				goto OnePhase;
			}
		}
#endif //DUAL_SHORE_CORD

	if ((GP_OUTPUT_FORM==TWO_PHASE) && (mux_id==OSC || mux_id==GEN || mux_id==SYS))
	{
#ifdef DUAL_SHORE_CORD
TwoPhase:
#endif
		lcd_display("\
           OUTPUT#1  OUTPUT#2          \n\
Ipeak:            A         A          \n\
Icrest:                                \n\
Irms:             A         A          \0",1,1);
		if (mux_id==SP1)
		{
			lcd_display("            INPUT#1   INPUT#2",1,1);
		}
		lcd_display_value(gp_Ib_pk[mux_id],2,23,"%4.2f\0");
		lcd_display_value(gp_ICFb[mux_id],3,24,"%4.2f\0");
		lcd_display_value(gp_Ib_rms[mux_id],4,23,"%4.2f\0");
	}
	else if ((GP_OUTPUT_FORM==ONE_PHASE) && (mux_id==OSC || mux_id==GEN || mux_id==SYS)) 
	{
#ifdef DUAL_SHORE_CORD
OnePhase:
#endif
		lcd_display("\
             OUTPUT\n\
Ipeak:            A\n\
Icrest:\n\
Irms:             A\0",1,1);
		if (mux_id==SP1)
		{
			lcd_display("              INPUT",1,1);
		}
	}
	else
	{
		lcd_display("\
            PHASE A   PHASE B   PHASE C\n\
Ipeak:            A         A         A\n\
Icrest:                                \n\
Irms:             A         A         A\0",1,1);

		if (mux_id==SP1 || mux_id==SP2 || (mux_id==OSC && DELTA_OUTPUT))
		{
			lcd_display("          PHASE A-B PHASE B-C PHASE C-A\0",1,1);
		}
		lcd_display_value(gp_Ib_pk[mux_id],2,23,"%4.2f\0");
		lcd_display_value(gp_Ic_pk[mux_id],2,33,"%4.2f\0");
		lcd_display_value(gp_ICFb[mux_id],3,24,"%4.2f\0");
		lcd_display_value(gp_ICFc[mux_id],3,34,"%4.2f\0");
		lcd_display_value(gp_Ib_rms[mux_id],4,23,"%4.2f\0");
		lcd_display_value(gp_Ic_rms[mux_id],4,33,"%4.2f\0");
	}
		
	lcd_display(get_display_name(mux_id),1,1);

	lcd_display_value(gp_Ia_pk[mux_id],2,13,"%4.2f\0");
	lcd_display_value(gp_ICFa[mux_id],3,14,"%4.2f\0");
	lcd_display_value(gp_Ia_rms[mux_id],4,13,"%4.2f\0");

//	lcd_display_value(gp_Van_DC[mux_id],4,12,"%4.2f");
//	lcd_display_value(gp_Vbn_DC[mux_id],4,22,"%4.2f");
//	lcd_display_value(gp_Vcn_DC[mux_id],4,32,"%4.2f");

	return mux_id;
}
/*---------------------------------------------------------------------------*/
int display_power_f8(int mux_id)
{
		lcd_display("\
            PHASE A   PHASE B   PHASE C\n\
V_kfactor:                              \n\
I_kfactor:                              \n\
OSC_Kout :                              \0",1,1);

//		lcd_display(get_display_name(mux_id),1,1);
		lcd_display("CONVERTER\0",1,1);
		lcd_display_value(Kmeter_Vint_A,2,14,"%5.3f\0");
		lcd_display_value(Kmeter_Vint_B,2,24,"%5.3f\0");
		lcd_display_value(Kmeter_Vint_C,2,34,"%5.3f\0");

		lcd_display_value(Kmeter_I_A,3,14,"%5.3f\0");
		lcd_display_value(Kmeter_I_B,3,24,"%5.3f\0");
		lcd_display_value(Kmeter_I_C,3,34,"%5.3f\0");

		lcd_display_value(Kout_A,4,14,"%5.3f\0");
		lcd_display_value(Kout_B,4,24,"%5.3f\0");
		lcd_display_value(Kout_C,4,34,"%5.3f\0");

	return mux_id;
}
/*---------------------------------------------------------------------------*/
int display_power_f9(int mux_id)
{
	lcd_display("ADC SAMPLES:        AGC_STATE:          \0",1,1);
	lcd_display("                    Va_out:             \0",2,1);
	lcd_display("                    Vb_out:             \0",3,1);
	lcd_display("MUX ID:             Vc_out:             \0",4,1);

	if (ALC_mode)
		lcd_display("ON \0",1,32);
	else
		lcd_display("OFF\0",1,32);

	lcd_display_value(adc_samples,1,14,"%4.0f\0");
	lcd_display_value(Va_out,2,30,"%6.4f\0");
	lcd_display_value(Vb_out,3,30,"%6.4f\0");
	lcd_display_value(Vc_out,4,30,"%6.4f\0");
	lcd_display_value(daq_mux_id,4,9,"%4.0f\0");

	return mux_id;
}
/*---------------------------------------------------------------------------*/
char *get_display_name(int mux_id)
{
	char *display_name="         \0";

		switch (mux_id)
		{
			case SP1:	display_name = "INPUT    \0";	break;
			case SP2:	display_name = "INPUT #2 \0";	break;
			case GEN:
						if (MULTIGEN_OPTION)
						{
							switch (get_gen_id())
							{
								case 0:
										display_name = "GENSET #0\0";
										break;
								case 1:
										display_name = "GENSET #1\0";
										break;
								case 2:
										display_name = "GENSET #2\0";
										break;
								case 3:
										display_name = "GENSET #3\0";
										break;
								case 4:
										display_name = "GENSET #4\0";
										break;
								default:
										display_name = "GENSET #x\0";
										break;
							}
						}
						else
						{
							if (!DUAL_GENSET)
								display_name = "GENERATOR\0";
							else
								display_name = "GENSET #1\0";
						}
						break;

			case OSC:	display_name = "CONVERTER\0";	break;	
			case EXT:	display_name = "EXTERNAL \0";	break;	
			case SYS:	
						if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
						{
							switch (get_gen_id())
							{
								case 0:
										display_name = "GENSET #0\0";
										break;
								case 1:
										display_name = "GENSET #1\0";
										break;
								case 2:
										display_name = "GENSET #2\0";
										break;
								case 3:
										display_name = "GENSET #3\0";
										break;
								case 4:
										display_name = "GENSET #4\0";
										break;
								default:
										display_name = "GENSET #x\0";
										break;
							}
						}
						else
						{
							display_name = "GENSET #2\0";
						}
		   				break;
			case AUX:	display_name = "AUXILIARY\0";	break;
			case XXX:	display_name = "SPARE    \0";	break;
			default:	break;
		}
		return display_name;
}
/*---------------------------------------------------------------------------*/
void display_not_calibration_mode()
{
	lcd_display("\
CALIBRATION MODE  NOT  ALLOWED BECAUSE\n\
OF BAD SYSTEM STATUS.  PRESS 'F2' AND \n\
'SYSTEM STATUS' KEYS TO CLEAR STATUS. \n\
*SEE MANUAL FOR PROCEDURE & OPERATION*\0",1,1);

	pause(3000); //let em read it.
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void calibrate_system()
{
  char *title=" GENSET #2   POWER  METER CALIBRATION   \0";
  float *kfactors[6];

	kfactors[0] = &Kmeter_Vsys_A;
	kfactors[1] = &Kmeter_Vsys_B;
	kfactors[2] = &Kmeter_Vsys_C;
	kfactors[3] = &Kmeter_Isys_A;
	kfactors[4] = &Kmeter_Isys_B;
	kfactors[5] = &Kmeter_Isys_C;

	if (GP_OUTPUT_FORM==ONE_PHASE)
	{
		calibrate_1phase(SYS,title,kfactors);
	}
	else if (GP_OUTPUT_FORM==TWO_PHASE)
	{
		calibrate_2phase(SYS,title,kfactors);
	}
	else
	{
		calibrate(SYS,title,kfactors);
	}
}
/*---------------------------------------------------------------------------*/
void calibrate_sp1_input()
{
  char *title="   INPUT     POWER  METER CALIBRATION   \0";
  float *kfactors[6];

	if (gp_state.input1_form==ONE_PHASE)
	{
		kfactors[0] = &Kmeter_Vsp1_1;
		kfactors[1] = &Kmeter_Vsp1_B;	//used with DUAL_SHORE_CORD AC24 system
		kfactors[2] = NULL;
		kfactors[3] = &Kmeter_Isp1_1;
		kfactors[4] = &Kmeter_Isp1_B;	//used with DUAL_SHORE_CORD AC24 systems
		kfactors[5] = NULL;
	}
	else
	{
		kfactors[0] = &Kmeter_Vsp1_A;
		kfactors[1] = &Kmeter_Vsp1_B;
		kfactors[2] = &Kmeter_Vsp1_C;
		kfactors[3] = &Kmeter_Isp1_A;
		kfactors[4] = &Kmeter_Isp1_B;
		kfactors[5] = &Kmeter_Isp1_C;
	}

//	if (DUAL_INPUT)
//	{
		calibrate(SP1,title,kfactors);
//	}
//	else
//	{
//		calibrate_1phase(SP1,title,kfactors);
//	}
}
/*---------------------------------------------------------------------------*/
void calibrate_sp2_input()
{
  char *title=" INPUT #2    POWER  METER CALIBRATION   \0";
  float *kfactors[6];

	if (gp_state.input2_form==ONE_PHASE)
	{
		kfactors[0] = &Kmeter_Vsp2_1;
		kfactors[1] = NULL;
		kfactors[2] = NULL;
		kfactors[3] = &Kmeter_Isp2_1;
		kfactors[4] = NULL;
		kfactors[5] = NULL;
	}
	else
	{
		kfactors[0] = &Kmeter_Vsp2_A;
		kfactors[1] = &Kmeter_Vsp2_B;
		kfactors[2] = &Kmeter_Vsp2_C;
		kfactors[3] = &Kmeter_Isp2_A;
		kfactors[4] = &Kmeter_Isp2_B;
		kfactors[5] = &Kmeter_Isp2_C;
	}

	calibrate(SP2,title,kfactors);
}
/*---------------------------------------------------------------------------*/
void calibrate_generator()
{
  char *title=" GENSET #1   POWER  METER CALIBRATION   \0";
  float *kfactors[6];

	if (gp_state.active_display==GEN1)
	{
		kfactors[0] = &Kmeter_Vgen_A;
		kfactors[1] = &Kmeter_Vgen_B;
		kfactors[2] = &Kmeter_Vgen_C;
		kfactors[3] = &Kmeter_Igen_A;
		kfactors[4] = &Kmeter_Igen_B;
		kfactors[5] = &Kmeter_Igen_C;

		if (GEN_AMPS_PRESENT)
		{
			set_Igen_mux(GEN1);
		}

		if (GP_OUTPUT_FORM==ONE_PHASE)
		{
			calibrate_1phase(GEN,title,kfactors);
		}
		else if (GP_OUTPUT_FORM==TWO_PHASE)
		{
			calibrate_2phase(GEN,title,kfactors);
		}
		else
		{
			calibrate(GEN,title,kfactors);
		}
	}
	else
	{
		if (GEN_AMPS_PRESENT)
		{
			set_Igen_mux(GEN2);
		}
		calibrate_system();
	}
}
/*---------------------------------------------------------------------------*/
void calibrate_converter()
{
  char *title=" CONVERTER   POWER  METER CALIBRATION   \0";
  float *kfactors[6];

	ALC_mode = DISABLED;
		
	kfactors[0] = &Kmeter_Vint_A;
	kfactors[1] = &Kmeter_Vint_B;
	kfactors[2] = &Kmeter_Vint_C;				  
	kfactors[3] = &Kmeter_I_A;
	kfactors[4] = &Kmeter_I_B;
	kfactors[5] = &Kmeter_I_C;

//	if (GP_OUTPUT_FORM==ONE_PHASE)
//	{
//		kfactors[1] = NULL;
//		kfactors[2] = NULL;				  
//		kfactors[4] = NULL;
//		kfactors[5] = NULL;
//
//		calibrate_1phase(OSC,title,kfactors);
//	}
//	else if (GP_OUTPUT_FORM==TWO_PHASE)
	if (GP_OUTPUT_FORM==ONE_PHASE || GP_OUTPUT_FORM==TWO_PHASE)
	{
		kfactors[2] = NULL;				  
		kfactors[5] = NULL;

		calibrate_2phase(OSC,title,kfactors);
	}
	else
	{
		calibrate(OSC,title,kfactors);
	}
}
/*---------------------------------------------------------------------------*/
void calibrate_oscillators()
{
  float Va,Vb,Vc;

	if (gp_state.invstat==ONLINE && gp_state.sysstat==OK)
	{
		clear_screen();
		lcd_display("CALIBRATION IN PROGRESS . . . \0",2,5);
		delay(1500);
		new_key=0;

		meas_all();		

		if (gp_state.vout_range==HIGH_RANGE)
		{
			Va = gp_Van[OSC] * 0.5;
			Vb = gp_Vbn[OSC] * 0.5;
			Vc = gp_Vcn[OSC] * 0.5;
		}
		else
		{
			Va = gp_Van[OSC];
			Vb = gp_Vbn[OSC];
			Vc = gp_Vcn[OSC];
		}

		if (ABS(Va_out-Va) < (0.2*Va_out))	
		{
			Kout_A = Va_out / Va / GP_XFMR_RATIO;	// * Kout_A;
			RAM_reset = 0;           				//flag to indicate RAM has been reset.
		}
		
		if (GP_OUTPUT_FORM==TWO_PHASE || GP_OUTPUT_FORM==THREE_PHASE)
		{
			if (ABS(Vb_out-Vb) < (0.2*Vb_out))	
			{
				Kout_B = Vb_out / Vb / GP_XFMR_RATIO;	// * Kout_B;
			}
		}
		if (GP_OUTPUT_FORM==THREE_PHASE)
		{
			if (ABS(Vc_out-Vc) < (0.2*Vc_out))	
			{
				Kout_C = Vc_out / Vc / GP_XFMR_RATIO;	// * Kout_C;
			}
		}

		exec_osc_pgm();                	// re-execute pgm with new OUTput Kfactor
	}
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void calibrate(int daq_mux_channel ,char *display_title, float *kfactors[])
{
  static unsigned i;
  int row[6]=	{ 3, 3, 3, 4, 4, 4};
  int column[6]={14,24,34,14,24,34};
  int cal_term[6]={0,1,2,3,4,5};		//use Avolts, Bvolts, Cvolts, Aamps, Bamps, Camps.
  char *calFormatV="%5.2f \0";
  char *calFormatA="%4.2f \0";
												  
	if (gp_mode==CALIBRATION_MODE)
	{									 
		special_function=1;

//		lcd_display("\
//            PHASE A   PHASE B   PHASE C \n\
//  VOLTAGE:        V         V         V ",2,1);
////////////////////////////////////////////////////////////////
//1234567 1234567 12345678 1234567 1234567
////////////////////////////////////////////////////////////////

		lcd_display("PHASE A   PHASE B   PHASE C \0",2,13);
		lcd_display("  VOLTAGE:        V         V         V \0",3,1);
////////////////////////////////////////////////////////////////
//				 	 1234567 1234567 12345678 1234567 1234567
////////////////////////////////////////////////////////////////

		if (daq_mux_channel==SP1 || daq_mux_channel==SP2)
		{
			lcd_display("          PHASE A-B PHASE B-C PHASE C-A \0",2,1);
		}

		lcd_display(display_title,1,1);

		lcd_display_value(gp_Van[daq_mux_channel],3,13,calFormatV);
		lcd_display_value(gp_Vbn[daq_mux_channel],3,23,calFormatV);
		lcd_display_value(gp_Vcn[daq_mux_channel],3,33,calFormatV);

		if (GEN_AMPS_PRESENT || (daq_mux_channel!=GEN && daq_mux_channel!=SYS))
		{
			lcd_display("  CURRENT:        A         A         A \0",4,1);
			lcd_display_value(gp_Ia_rms[daq_mux_channel],4,14,calFormatA);
			lcd_display_value(gp_Ib_rms[daq_mux_channel],4,24,calFormatA);
			lcd_display_value(gp_Ic_rms[daq_mux_channel],4,34,calFormatA);
		}

		if (first_time_through==1)
		{
			first_time_through=0;
			i=0;
		}
		
		put_cursor(row[i],column[i]);

		if (combo_key==0)
		{
			if (i<3)	//voltage calibration.
				*kfactors[cal_term[i]] = *kfactors[cal_term[i]]+0.0005;
			else		//current calibration.
				*kfactors[cal_term[i]] = *kfactors[cal_term[i]]+0.007;
		}
		if (combo_key==1)
		{
			if (i<3)	//voltage calibration.
				*kfactors[cal_term[i]] = *kfactors[cal_term[i]]-0.0005;
			else		//current calibration.
				*kfactors[cal_term[i]] = *kfactors[cal_term[i]]-0.007;
		}
		if (combo_key==2) //advance cursor to next field for edit;
		{
			if (GEN_AMPS_PRESENT || (daq_mux_channel!=GEN && daq_mux_channel!=SYS))
			{
	  			if (i>=5) i=0;
				else i++;
			}
			else
			{
	  			if (i>=2) i=0;
				else i++;
			}

			put_cursor(row[i],column[i]);	//ASEA3
			delay(500);						//ASEA3
		}
		if (combo_key==3)
		{
			if (daq_mux_channel==OSC) calibrate_oscillators();
		}
	}
	else
	{
		display_not_calibration_mode();
	}

	combo_key=86;
}
/*---------------------------------------------------------------------------*/
void calibrate_1phase(int daq_mux_channel ,char *display_title, float *kfactors[])
{
  static unsigned i;
  int row[2]=	{ 3, 4};
  int column[2]={13,14};
  int cal_term[2]={0,3};		//use Avolts, Aamps.
  char *calFormatV="%5.2f \0";
  char *calFormatA="%4.2f \0";
												  
	if (gp_mode==CALIBRATION_MODE)
	{									 
		special_function=1;

		if (first_time_through==1)
		{
			first_time_through=0;
			i=0;
		}

		lcd_display(display_title,1,1);

		lcd_display("\
            PHASE A                     \n\
  VOLTAGE:        V                     \n\
  CURRENT:        A\0",2,1);


		if (daq_mux_channel==OSC)
		{
			lcd_display_value(gp_Vab[daq_mux_channel],3,13,calFormatV);
		}
		else
		{
			lcd_display_value(gp_Van[daq_mux_channel],3,13,calFormatV);
		}


		lcd_display_value(gp_Ia_rms[daq_mux_channel],4,14,calFormatA);
		
		put_cursor(row[i],column[i]);

		if (combo_key==0)
		{
			if (i<1)	//voltage calibration.
				*kfactors[cal_term[i]] = *kfactors[cal_term[i]]+0.0005;
			else		//current calibration.
				*kfactors[cal_term[i]] = *kfactors[cal_term[i]]+0.007;
		}
		if (combo_key==1)
		{
			if (i<1)	//voltage calibration.
				*kfactors[cal_term[i]] = *kfactors[cal_term[i]]-0.0005;
			else		//current calibration.
				*kfactors[cal_term[i]] = *kfactors[cal_term[i]]-0.007;
		}
		if (combo_key==2) //advance cursor to next field for edit;
		{
			if (i==1) i=0;
			else i=1;	

			put_cursor(row[i],column[i]);	//ASEA3
			delay(500);						//ASEA3
		}
		if (combo_key==3)
		{
			if (daq_mux_channel==OSC) calibrate_oscillators();
		}
		combo_key=86;
	}
	else
	{
		display_not_calibration_mode();
	}
}
/*---------------------------------------------------------------------------*/
void calibrate_2phase(int daq_mux_channel ,char *display_title, float *kfactors[])
{
  static unsigned i;
  int row[4]=	{ 3, 3, 4, 4};
  int column[4]={14,24,14,24};
  int cal_term[4]={0,1,3,4};		//use Avolts, Bvolts, Aamps, Bamps.
  char *calFormatV="%5.2f \0";
  char *calFormatA="%4.2f \0";
												  
	if (gp_mode==CALIBRATION_MODE)
	{									 
		special_function=1;

		lcd_display("\
           OUTPUT#1  OUTPUT#2           \n\
  VOLTAGE:        V         V           \0",2,1);

		lcd_display(display_title,1,1);

		lcd_display_value(gp_Van[daq_mux_channel],3,13,calFormatV);
		lcd_display_value(gp_Vbn[daq_mux_channel],3,23,calFormatV);

		if (daq_mux_channel!=GEN && daq_mux_channel!=SYS)
		{
			lcd_display("  CURRENT:        A         A          \0",4,1);
			lcd_display_value(gp_Ia_rms[daq_mux_channel],4,14,calFormatA);
			lcd_display_value(gp_Ib_rms[daq_mux_channel],4,24,calFormatA);
		}

		if (first_time_through==1)
		{
			first_time_through=0;
			i=0;
		}
		
		put_cursor(row[i],column[i]);

		if (combo_key==0)
		{
			if (i<2)	//voltage calibration.
				*kfactors[cal_term[i]] = *kfactors[cal_term[i]]+0.0005;
			else		//current calibration.
				*kfactors[cal_term[i]] = *kfactors[cal_term[i]]+0.007;
		}
		if (combo_key==1)
		{
			if (i<2)	//voltage calibration.
				*kfactors[cal_term[i]] = *kfactors[cal_term[i]]-0.0005;
			else		//current calibration.
				*kfactors[cal_term[i]] = *kfactors[cal_term[i]]-0.007;
		}
		if (combo_key==2) //advance cursor to next field for edit;
		{
			if (daq_mux_channel!=GEN && daq_mux_channel!=SYS)
			{
	  			if (i>=3) i=0;
				else i++;
			}
			else
			{
	  			if (i>=1) i=0;
				else i++;
			}

			put_cursor(row[i],column[i]);	//ASEA3
			delay(500);						//ASEA3
		}
		if (combo_key==3)
		{
			if (daq_mux_channel==OSC) calibrate_oscillators();
		}
	}
	else
	{
		display_not_calibration_mode();
	}

	combo_key=86;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*
void edit_serial_comm_config()
{
  unsigned baud_rate[7]={300,1200,2400,4800,9600,19200,38400};
  unsigned i;

	lcd_display("\
     REMOTE INTERFACE CONFIGURATION     \n\
BAUD: 19200   8-DATA BITS,1 START,1 STOP\n\
PARITY: NONE    EOS: CR/LF   DEVICE: DCE\n\
HANDSHAKING: NONE            HALF-DUPLEX\0",1,1);

	switch (ser_baud_rate)
	{
		case 300:	i=0;	break;
		case 1200:	i=1;	break;
		case 2400:	i=2;	break;
		case 4800:	i=3;	break;
		case 9600:	i=4;	break;
		case 19200: i=5;	break;
		case 38400: i=6;	break;
		default:	i=5;	break;
	}

	while (new_key!=18)	//exit when SYSTEM_STATUS is pressed.
	{
		lcd_display("     \0",2,7);	//clear field

		if (ser_baud_rate < 10000)
		{
			lcd_display(utoa(ser_baud_rate),2,8);
		}
		else
		if (ser_baud_rate < 1000)
		{
			lcd_display(utoa(ser_baud_rate),2,9);
		}
		else
		{
  			lcd_display(utoa(ser_baud_rate),2,7);
		}

		put_cursor(2,11);
		pause(500);
		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:

					if (i<6)
					{
						ser_baud_rate = baud_rate[++i];
					}
					break;

			case 20:	//F2:

					if (i>1)
					{
						ser_baud_rate = baud_rate[--i];
					}
					break;

			default:
					break;
		}
	}
	new_key = 0;

	IE_init();	//reestablish serial comm. with new baud rate.
}
/*---------------------------------------------------------------------------*/
void edit_serial_comm_config()
{
  int edit_field=0;
  unsigned baud_rate[7]={300,1200,2400,4800,9600,19200,38400};
  unsigned i;

	lcd_display("\
     REMOTE INTERFACE CONFIGURATION     \n\
BAUD: 19200   8-DATA BITS,1 START,1 STOP\n\
PARITY:NONE     EOS:CR/LF     DEVICE:DCE\n\
HANDSHAKING:NONE                        \0",1,1);

  while (new_key!=18)	//exit when SYSTEM_STATUS is pressed.	while added v2.79
  {

	switch (ser_baud_rate)
	{
		case 300:	i=0;	break;
		case 1200:	i=1;	break;
		case 2400:	i=2;	break;
		case 4800:	i=3;	break;
		case 9600:	i=4;	break;
		case 19200: i=5;	break;
		case 38400: i=6;	break;
		default:	i=5;	break;
	}

	if (modbusActive)
	{
		lcd_display("Modbus\0",4,35);			//clear field
		lcd_display("Node Id:     \0",3,16);	//clear field
		lcd_display(utoa(modbusNodeId),3,25);	//show Node Id
	}
	else
	{
		lcd_display("  SCPI\0",4,35);	//clear field		
		lcd_display(" EOS:CR/LF   \0",3,16);	//clear field
		edit_field=0;					//only Baud Rate may be modified
	}

	lcd_display("     \0",2,7);	//clear field

	if (ser_baud_rate < 10000)
	{
		lcd_display(utoa(ser_baud_rate),2,8);
	}
	else if (ser_baud_rate < 1000)
	{
		lcd_display(utoa(ser_baud_rate),2,9);
	}
	else
	{
		lcd_display(utoa(ser_baud_rate),2,7);
	}

	switch (edit_field)
	{
		case 0://Baud Rate
					put_cursor(2,11);
					break;
		case 1://Modbus Node Id
					put_cursor(3,24);
					break;
		default:	edit_field=0;
					break;
	}

//	if (fresh_screen) delay(250);        //give time for operator to release key.

	wait_for_key();

//	switch (key_press)
	switch (new_key)
	{
			case 19:	//F1:
				if (edit_field)
				{
					if(modbusNodeId < 247) modbusNodeId++;
//					if(modbusNodeId < 31) modbusNodeId++;
					if ((modbusNodeId==10) || (modbusNodeId==13)) modbusNodeId++;	//v1.72, no LF or CR address
				}
				else
				{
					if (i<6)
					{
						ser_baud_rate = baud_rate[++i];
						IE_init();	//reestablish serial comm. with new baud rate.
					}
				}
//				key_press=NONE;
				new_key = 0;
				break;

			case 20:	//F2:
				if (edit_field)
				{
					if(modbusNodeId > 1) modbusNodeId--;
					if ((modbusNodeId==10) || (modbusNodeId==13)) modbusNodeId--;	//v1.72, no LF or CR address
				}
				else
				{
					if (i>1)
					{
						ser_baud_rate = baud_rate[--i];
						IE_init();	//reestablish serial comm. with new baud rate.
					}
				}
//				key_press=NONE;
				new_key = 0;
				break;

			case 8:	//F3:
				if (edit_field)
				{
					edit_field=0;
				}
				else
				{
					edit_field=1;
				}
//				key_press=NONE;
				new_key = 0;
				break;

			case 9:	//F4
				if (modbusActive)
				{
					modbusActive=FALSE;		//SCPI Protocol
				}
				else
				{
					modbusActive=TRUE;		//Modbus Protocol
				}
				IE_init();					//reinitialize all comm buffers.
//				key_press=NONE;
				new_key = 0;
				break;

			case 18:	//SYSTEM_STATUS:
			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						break;

		default:
//				key_press=NONE;
				new_key = 0;
				break;
	}
  }
  new_key = 0;
}
/*---------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
int display_output_power_f1(int mux_id)
{
	lcd_display("\
CONVERTER       OUTPUT                Hz\n\
  VOLTAGE:           V                  \n\
  CURRENT:           A                  \n\
     LOAD:         kVA  \0",1,1);

	if (gp_Freq[mux_id]<100.0)
	{
		lcd_display_value(gp_Freq[mux_id],1,35,"%4.1f\0");
	}
	else
	{
		lcd_display("INVALID\0",1,34);
	}

	if (gp_OverRange[mux_id][A_VOLTS] || gp_OverRange[mux_id][B_VOLTS])
	{
		lcd_display("v\0",2,22);
	}
	lcd_display_value(gp_Vab[mux_id],2,17,"%3.0f\0");

	if (gp_OverRange[mux_id][A_AMPS] || gp_OverRange[mux_id][B_AMPS])
	{
		lcd_display("a\0",3,22);
	}
	lcd_display_value(gp_Ia_rms[mux_id],3,17,"%3.0f\0");

	lcd_display_value(gp_KVAa[mux_id],4,16,"%3.1f\0");

	return mux_id;
}
/*---------------------------------------------------------------------------*/
int display_output_power_f2(int mux_id)
{
	lcd_display("\
CONVERTER       OUTPUT                Hz\n\
     POWER:         kW                  \n\
PWR FACTOR:         PF                  \n\
LOAD LEVEL:          %                  \0",1,1);

	lcd_display_value(gp_KWa[mux_id],2,17,"%3.1f\0");
	lcd_display_value(gp_PFa[mux_id],3,17,"%3.1f\0");
	lcd_display_value(gp_level_a[mux_id],4,17,"%4.1f\0");

	if (gp_Freq[mux_id]<100.0)
	{
		lcd_display_value(gp_Freq[mux_id],1,35,"%4.1f\0");
	}
	else
	{
		lcd_display("INVALID\0",1,34);
	}

	return mux_id;
}
/*---------------------------------------------------------------------------*/
int display_voltage_f1(int mux_id)
{
	if (GP_OUTPUT_FORM==ONE_PHASE)
	{
	lcd_display("\
           OUTPUT#1                     \n\
  VOLTAGE:        V                     \n\
FREQUENCY:       Hz       FORM: 1-PHASE \n\
   STATUS:                              \0",1,1);

		lcd_display(get_display_name(mux_id),1,1);

		if (gp_OverRange[mux_id][A_VOLTS])
		{
			lcd_display("v\0",2,19);
		}
		lcd_display_value(gp_Van[mux_id],2,12,"%3.0f\0");
	}
	else if (GP_OUTPUT_FORM==TWO_PHASE)
	{
	lcd_display("\
           OUTPUT#1  OUTPUT#2           \n\
  VOLTAGE:        V         V           \n\
FREQUENCY:       Hz       FORM: 2-PHASE \n\
   STATUS:                              \0",1,1);

		lcd_display(get_display_name(mux_id),1,1);

		if (gp_OverRange[mux_id][A_VOLTS])
		{
			lcd_display("v\0",2,19);
		}
		lcd_display_value(gp_Van[mux_id],2,12,"%3.0f\0");

		if (gp_OverRange[mux_id][B_VOLTS])
		{
			lcd_display("v\0",2,29);
		}
		lcd_display_value(gp_Vbn[mux_id],2,22,"%3.0f\0");
	}
	else
	{
	lcd_display("\
            PHASE A   PHASE B   PHASE C \n\
  VOLTAGE:    /   V     /   V     /   V \n\
FREQUENCY:       Hz       FORM: 3-PHASE \n\
   STATUS:                              \0",1,1);

		lcd_display(get_display_name(mux_id),1,1);


		if (gp_OverRange[mux_id][A_VOLTS])
		{
			lcd_display("v\0",2,19);
		}
		lcd_display_value(gp_Van[mux_id],2,12,"%3.0f\0");
	 	lcd_display_value(gp_Vab[mux_id],2,16,"%3.0f\0");

		if (gp_OverRange[mux_id][B_VOLTS])
		{
			lcd_display("v\0",2,29);
		}
		lcd_display_value(gp_Vbn[mux_id],2,22,"%3.0f\0");
		lcd_display_value(gp_Vbc[mux_id],2,26,"%3.0f\0");

		if (gp_OverRange[mux_id][C_VOLTS])
		{
			lcd_display("v\0",2,39);
		}
		lcd_display_value(gp_Vcn[mux_id],2,32,"%3.0f\0");
		lcd_display_value(gp_Vca[mux_id],2,36,"%3.0f\0");
	}

//	if ((gp_Freq[mux_id]>20.0) && (gp_Freq[mux_id]<100.0))
//	{
		lcd_display_value(gp_Freq[mux_id],3,13,"%4.1f\0");
//	}
//	else
//	{
//		lcd_display("INVALID\0",4,13);
//	}

	if (mux_id==GEN)
	{
		lcd_display(sumstat[gp_state.genstat1],4,12);

		if (MULTIGEN_OPTION)
		{
			switch (get_gen_id())
			{
				case 0:
						break;
				case 1:
						if (eGen1_CLOSED())
							lcd_display(sumstat[ONLINE],4,12);
						else
							lcd_display(sumstat[OFFLINE],4,12);
						break;
				case 2:
						if (eGen2_CLOSED())
							lcd_display(sumstat[ONLINE],4,12);
						else
							lcd_display(sumstat[OFFLINE],4,12);
						break;
				case 3:
						if (eGen3_CLOSED())
							lcd_display(sumstat[ONLINE],4,12);
						else
							lcd_display(sumstat[OFFLINE],4,12);
						break;
				case 4:
						if (eGen4_CLOSED())
							lcd_display(sumstat[ONLINE],4,12);
						else
							lcd_display(sumstat[OFFLINE],4,12);
						break;
				default:
						break;
			}
		}
	}

	if (mux_id==SYS)
	{
		if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
		{
			switch (get_gen_id())
			{
				case 0:
						break;
				case 1:
						if (eGen1_CLOSED())
							lcd_display(sumstat[ONLINE],4,12);
						else
							lcd_display(sumstat[OFFLINE],4,12);
						break;
				case 2:
						if (eGen2_CLOSED())
							lcd_display(sumstat[ONLINE],4,12);
						else
							lcd_display(sumstat[OFFLINE],4,12);
						break;
				case 3:
						if (eGen3_CLOSED())
							lcd_display(sumstat[ONLINE],4,12);
						else
							lcd_display(sumstat[OFFLINE],4,12);
						break;
				case 4:
						if (eGen4_CLOSED())
							lcd_display(sumstat[ONLINE],4,12);
						else
							lcd_display(sumstat[OFFLINE],4,12);
						break;
				default:
						break;
			}
		}
		else
		{
			lcd_display(sumstat[gp_state.genstat2],4,12);
		}
	}

 	return mux_id;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*
void pwm2_setup_screen()	//extern struct gp_state_struct gp_state;
{
  int edit_field=0;
  int row[4]=	{ 2, 2, 3, 3};
  int column[4]={ 9,37,14,32};

  char *status[2]={	"DISABLED",
			   		"ENABLED "};

  char *align[2]={	"EDGE  ",
			   		"CENTER"};

	clear_screen();
	lcd_display("PWM2 IMPEDANCE CONTROL                  ",1,1);
	lcd_display(" State: DISABLED        Duty Cycle: 100%",2,1);
	lcd_display(" Resolution:  9 bits    Align: CENTER   ",3,1);
	lcd_display("SYSTEM STATUS to exit screen.",4,1);

	lcd_display(status[mach.pwm2_state],row[0],column[0]);				//edit_field==0
	lcd_display_value(mach.pwm2_dutyCycle,row[1],column[1],"%3.0f");	//edit_field==1
	lcd_display_value(mach.pwm2_bits,row[2],column[2],"%2.0f");			//edit_field==2
	lcd_display(align[mach.pwm2_align],row[3],column[3]);				//edit_field==3

	delay(500); //wait a 1/2 sec. for operator to see screen.

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display(status[mach.pwm2_state],row[0],column[0]);				//edit_field==0
		lcd_display_value(mach.pwm2_dutyCycle,row[1],column[1],"%3.0f");	//edit_field==1
		lcd_display_value(mach.pwm2_bits,row[2],column[2],"%2.0f");			//edit_field==2
		lcd_display(align[mach.pwm2_align],row[3],column[3]);				//edit_field==3

		put_cursor(row[edit_field],column[edit_field]);

		delay(250);			//adjust keypad response here (i.e. slew rate control).

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1 (++) ENABLE:
					switch (edit_field)
					{
						case 0:	//mach.pwm2_state field
								mach.pwm2_state = ENABLED;
								break;
						case 1:	//mach.pwm2_dutyCycle field
								if (mach.pwm2_dutyCycle < 100)
								{
									mach.pwm2_dutyCycle++;
								}
								break;
						case 2:	//mach.pwm2_bits field
								if (mach.pwm2_bits < 16)
								{
									mach.pwm2_bits++;
								}
								break;
						case 3:	//mach.pwm2_align field
								mach.pwm2_align = 1;	//CENTER Alignment.
								break;
						default: 
								edit_field=0;
								break;
					}
					break;

			case 20:	//F2 (--) DISABLE:
					switch (edit_field)
					{
						case 0:	//mach.pwm2_state field
								mach.pwm2_state = DISABLED;
								break;
						case 1:	//mach.pwm2_dutyCycle field
								if (mach.pwm2_dutyCycle > 0)
								{
									mach.pwm2_dutyCycle--;
								}
								break;
						case 2:	//mach.pwm2_bits field
								if (mach.pwm2_bits > 8)
								{
									mach.pwm2_bits--;
								}
								break;
						case 3:	//mach.pwm2_align field
								mach.pwm2_align = 0;	//EDGE Alignment.
								break;
						default: 
								edit_field=0;
								break;
					}
					break;

			case 17:		//CONVERTER POWER	//advance cursor.
					if (edit_field<=0)
					{
						edit_field=3;
					}
					else
					{
						edit_field--;	
					}
					put_cursor(row[edit_field],column[edit_field]);
					delay(500);	
					break;

			case 16:		//SHORE POWER		//backup cursor.
					if (edit_field>=3)
					{
						edit_field=0;
					}
					else
					{
						edit_field++;	
					}
					put_cursor(row[edit_field],column[edit_field]);
					delay(500);	
					break;

			default:
					break;
		}

		if (mach.pwm2_state==ENABLED)
		{
			if (gp_state.local_master==SLAVE)
			{
				init_PWM2(mach.pwm2_bits,mach.pwm2_align,mach.pwm2_dutyCycle);		//unsigned(bits,align,duty)
			}
			else
			{
				init_PWM2(9,0,50);				//unsigned(bits,align,duty)
			}
		}
		else
		{
			init_PWM2(9,0,50);	//unsigned(bits,align,duty)
		}
	}

	new_key = 0;
}
/*---------------------------------------------------------------------------*/
#ifdef GNOP_ON
#define MIN_TIME_STEP 150
/*---------------------------------------------------------------------------*/

void pong()	//4x40 LCD Pong Game.	uses around 3.1kB EPROM space
{
  unsigned time_step = 400;			//milliseconds between movement.
  unsigned time_change = 50;
  unsigned newGame=1;
  unsigned x;
  unsigned y;

  char *puck={"O\0"};
  char *lbat={"]\0"};
  char *rbat={"[\0"};
  char *blank={" \0"};

  struct Puck {	unsigned x;
				unsigned y;
				unsigned vector_id;
				unsigned direction;
  } P;
  
  struct Bat {	unsigned x;
				unsigned y;
				unsigned vector_id;
				unsigned points;
  } L,R;

//  unsigned W[3][3]={0,1,2,			//puck-bat transfer function.
//					1,2,0,
//					2,0,1};

  unsigned W[3][3]={1,2,0,			//puck-bat transfer function.
					0,1,2,
					2,0,1};

//check_system_status();if(shorepower online)bail;
	check_gp_status();
	if (gp_state.sysstat) return;
	if (gp_state.inpstat1!=OFFLINE || gp_state.invstat!=OFFLINE)	//bail if sp1 online.
		return;

//display intro...
	lcd_display("                 GNOP                   \0",1,1);
	lcd_display("  Left Player             Right Player  \0",2,1);
	lcd_display("      F1          up          F5        \0",3,1);
	lcd_display("  SHORE POWER    down      CALIBRATE    \0",4,1);
	pause(3000);	//show for our new viewers.
	pause(3000);	//show for our new viewers.

//initialize points.
	L.points=0;
	R.points=0;
	x=y=3;		//initialize to some unused position.


	while (new_key!=18 && L.points<10 && R.points<10)	//bail on ss key or game over.
	{
		if (newGame)
		{
			newGame = 0;	//initialize all new game values.
			delay(350);
			srand(milliseconds);	//seed random number generator.
			time_step = 400;
			P.x = 19;
			P.y = (unsigned) (rand()%4)+1;
			P.vector_id = (unsigned) rand()%3;
			P.direction = (unsigned) rand()%2;
			L.x = 2;
			L.y = 2;
			L.vector_id = 0;
			R.x = 39;
			R.y = 3;
			R.vector_id = 0;
	
		//display points 
			clear_screen();
			lcd_display("                  GNOP                  \0",1,1);
			lcd_display("    LBAT:  0                RBAT:  0    \0",3,1);
			lcd_display("      Press:  SYSTEM STATUS  to quit    \0",4,1);
			lcd_display_value(L.points,3,12,"%2.0f\0");
			lcd_display_value(R.points,3,35,"%2.0f\0");
			delay(3000);	//show for our new viewers.
			
			lcd_display("L                                      R\0",1,1);
			lcd_display("B]                                     B\0",2,1);
			lcd_display("A                                     [A\0",3,1);
			lcd_display("T                                      T\0",4,1);
		}

//check_system_status();if(shorepower online)bail;
		check_gp_status();
		if (gp_state.sysstat) return;
		if (gp_state.inpstat1!=OFFLINE || gp_state.invstat!=OFFLINE)	//bail if sp1 online.
			return;

		delay(time_step);		//half a time step.
//move the puck. (part 1) vanish old puck (last new puck still visable).
		lcd_display(blank,y,x);		//clear old puck (note: assume puck moved).

//move the bats.
		scan_kybd();

		switch (new_key)
		{
			case 19:		//F1 				//LBAT UP.
					if (L.y>1)
					{
						lcd_display(blank,L.y,L.x);
						L.y--;
						lcd_display(lbat,L.y,L.x);
					}
		
					if (L.vector_id == 2)
						L.vector_id = 0;
					else
						L.vector_id = 2;
		
					break;

			case 16:		//SHORE POWER		//LBAT DOWN.
					if (L.y<4)
					{
						lcd_display(blank,L.y,L.x);
						L.y++;
						lcd_display(lbat,L.y,L.x);
					}
					L.vector_id = 1;
 					break;

			case 10:		//F5				//RBAT UP.
					if (R.y>1)
					{
						lcd_display(blank,R.y,R.x);
						R.y--;
						lcd_display(rbat,R.y,R.x);
					}
					R.vector_id = 2;
					break;

			case 99:		//CALIBRATE			//RBAT DOWN.
					if (R.y<4)
					{
						lcd_display(blank,R.y,R.x);
						R.y++;
						lcd_display(rbat,R.y,R.x);
					}					   
					R.vector_id = 1;
 					break;

			case 0:		//no key_press
//					L.vector_id = 0;
					R.vector_id = 0;
					break;

			default:
					break;
		}

//move puck (part 2) show new puck and test for transformation application (i.e. bat-back).		
		x = P.x;
		y = P.y;

		if (P.direction==0)	//Right-to-Left.
		{
			switch (P.vector_id)
			{
				case 0:	//straight-on
						if (P.x==3)		//check for bat-back.
						{
							if (L.y==P.y)	//got it!
							{
								P.direction = 1;	//bat-back puck.
								P.vector_id = W[P.vector_id][L.vector_id];
								if (time_step>MIN_TIME_STEP) time_step=time_step-time_change;
							}
							else	//missed it.
							{
								lcd_display(puck,P.y,--P.x);
								R.points++;
								newGame = 1;	//initialize new game values flag.
							}
						}
						else
						{
							P.x--;
						}
						break;

				case 1:	//downward
						if (P.x==3)		//check for bat-back.
						{
							if (L.y==P.y)	//got it!
							{
								P.direction = 1;	//bat-back puck.
								P.vector_id = W[P.vector_id][L.vector_id];
								if (time_step>MIN_TIME_STEP) time_step=time_step-time_change;
							}
							else	//missed it.
							{
								lcd_display(puck,P.y,--P.x);
								R.points++;
								newGame = 1;	//initialize new game values flag.
							}
						}
						else
						{
							P.x--;
						}

						if (P.direction==0)
							if (P.y<4)
							{
								P.y++;
							}
							else
							{
								P.y = 3;
								P.vector_id = 2;
							}

						break;

				case 2:	//upward
						if (P.x==3)		//check for bat-back.
						{
							if (L.y==P.y)	//got it!
							{
								P.direction = 1;	//bat-back puck.
								P.vector_id = W[P.vector_id][L.vector_id];
								if (time_step>MIN_TIME_STEP) time_step=time_step-time_change;
							}
							else	//missed it.
							{
								lcd_display(puck,P.y,--P.x);
								R.points++;
								newGame = 1;	//initialize new game values flag.
							}
						}
						else
						{
							P.x--;
						}

						if (P.direction==0)
							if (P.y>1)
							{
								P.y--;
							}
							else
							{
								P.y = 2;
								P.vector_id = 1;
							}

						break;

				default:
						P.vector_id = 0;
						break;
			}
		}
		else	//P.direction==1, Left-to-Right.
		{
			switch (P.vector_id)
			{
				case 0:	//straight-on
						if (P.x==38)		//check for bat-back.
						{
							if (R.y==P.y)	//got it!
							{
								P.direction = 0;	//bat-back puck.
								P.vector_id = W[P.vector_id][R.vector_id];
								if (time_step>MIN_TIME_STEP) time_step=time_step-time_change;
							}
							else	//missed it.
							{
								lcd_display("O\0",P.y,++P.x);
								L.points++;
								newGame = 1;	//initialize new game values flag.
							}
						}
						else
						{
							P.x++;
						}
						break;

				case 1:	//downward
						if (P.x==38)		//check for bat-back.
						{
							if (R.y==P.y)	//got it!
							{
								P.direction = 0;	//bat-back puck.
								P.vector_id = W[P.vector_id][R.vector_id];
								if (time_step>MIN_TIME_STEP) time_step=time_step-time_change;
							}
							else	//missed it.
							{
								lcd_display(puck,P.y,++P.x);
								L.points++;
								newGame = 1;	//initialize new game values flag.
							}
						}
						else
						{
							P.x++;
						}

						if (P.direction==1)
							if (P.y<4)
							{
								P.y++;
							}
							else
							{
								P.y = 3;
								P.vector_id = 2;
							}

						break;

				case 2:	//upward
						if (P.x==38)		//check for bat-back.
						{
							if (R.y==P.y)	//got it!
							{
								P.direction = 0;	//bat-back puck.
								P.vector_id = W[P.vector_id][R.vector_id];
								if (time_step>MIN_TIME_STEP) time_step=time_step-time_change;
							}
							else	//missed it.
							{
								lcd_display(puck,P.y,++P.x);
								L.points++;
								newGame = 1;	//initialize new game values flag.
							}
						}
						else
						{
							P.x++;
						}

						if (P.direction==1)
							if (P.y>1)
							{
								P.y--;
							}
							else
							{
								P.y = 2;
								P.vector_id = 1;
							}

						break;

				default:
						P.vector_id = 0;
						break;
			}
		}

		if (newGame==0)
		{
			lcd_display(puck,P.y,P.x);	//show new puck.
		}
	}

	if (new_key!=18)	//skip if ss key used to bail-out.
	{	//display game over 
		clear_screen();	_nop();
		lcd_display(" ************ GAME  OVER ************** \0",1,1);
		lcd_display("    LBAT:  0                RBAT:  0    \0",3,1);
		lcd_display(" ************************************** \0",4,1);
		lcd_display_value(L.points,3,12,"%2.0f\0");
		lcd_display_value(R.points,3,35,"%2.0f\0");
		delay(3000);	//show for our winners & losers.
		delay(1000);	//show for our winners & losers.
		lcd_display(" *** GNOP Designed by Dave Newberry *** \0",4,1);
		pause(500);	//show for our winners & losers.
	}
}
/*---------------------------------------------------------------------------*/
#endif // GNOP_ON
/*---------------------------------------------------------------------------*/
//
//GENERATOR-TO-GENERATOR TRANSFER CONTROL
//
//  Front-end for gen2gen systems.  One of Two Option selections got us here.
//
//			1.	Orignal GEN2GEN_OPTION for Northern.  Used Gen Input to Conv.
//					and transfers through converter for gen-to-gen transfers.
//
//			2.	NEW_G2G_OPTION for Trinity.  Used Gen Speed-Trim interface via
//					pwm control for gen-to-gen transfers and had various Modes:
//
//					AUTOMATIC => Full transfer capibility allowed.
//					RESTRICTED => Low-Power Only (No Input Power)
//					MANUAL => No Automatic transfers (inhibit for maintenance)
//
/*---------------------------------------------------------------------------*/
int gen2gen_transfer_control()
{
  int f1=1;					//transferRequest choice assigned to F1 key.
  int f2=2;					//transferRequest choice assigned to F2 key.
  int f3=3;					//transferRequest choice assigned to F3 key.
  int f4=4;					//transferRequest choice assigned to F4 key.
  int other=3;				//for use with RESTRICTED transferModes.
  int transferRequest=0;	//0=none/abort;1=genset#1(A+B);2=genset#(A+B)2;3=converter(A+B);4=split(g1/A+g2/B).
  int busState;
  int abort_transfer_request=0;
  char *strTransferRequest=NULL;
  unsigned local_transferMode=transferMode;		

  char *transferRequestName[6]={"BLACK-OUT \0",
  "GENSET #1 \0", "GENSET #2 \0", "CONVERTER \0", "SPLIT BUS \0","RESTRICTED\0"};

	clear_screen();

	lcd_display("Press:'F1' for TRANSFER to\0",2,1);
	lcd_display("      'F2' for TRANSFER to\0",3,1);
	lcd_display("      'F3' for TRANSFER to\0",4,1);

	while ((new_key!=18) && (!transferRequest) && !abort_transfer_request)	//exit when SYSTEM_STATUS is pressed, or valid transferRequest.
	{
		//get fresh status.
		//
		check_gp_status();		

		if (NEW_G2G_OPTION)
		{
			busState = get_busState2();
		}
		else
		{
			busState = get_busState();
		}
//
////////////////////////////////////////////////////////
//
//QUALIFY SYSTEM FOR TRANSFERS.  check if system conditions exist for seamless-transfers.
//
//int ok2Transfer(int , int );		//if(ok2Transfer(fromBusState,toBusState)) doit;
//
		if (NEW_G2G_OPTION)
		{
			if (maintenanceMode)	//check if maintenanceMode.
			{
				local_transferMode = MANUAL;
				clear_screen();
				lcd_display("TRANSFER ABORTED.\0",1,1);
				lcd_display("'MAINTENANCE' MODE IS ENABLED.  \0",2,1);
				lcd_display("DISABLE 'MAINTENANCE' MODE FOR TRANSFERS\0",3,1);
				lcd_display("             Press any key to exit . . .\0",4,1);
				put_cursor(4,40);
				pause(500);
				wait_for_key();
				new_key=18;
				return transferRequest;
			}
			else if (manual_mode())	//check if MANUAL mode.
			{
				local_transferMode = MANUAL;
				clear_screen();
				lcd_display("TRANSFER ABORTED.  'MANUAL' MODE IS SET.\0",1,1);
				lcd_display("SET 'AUTOMATIC' MODE TO TRANSFER POWER. \0",2,1);
				lcd_display("             Press any key to exit . . .\0",4,1);
				put_cursor(4,40);
				pause(500);
				wait_for_key();
				new_key=18;
				return transferRequest;
			}
			else if (gp_Vab[SP1] < 90.0)	//bail with msg if no shore power (voltage).
			{
				local_transferMode = RESTRICTED;
				other=5;
			}
			else if (gp_state.inpstat1!=ONLINE)	//bail with msg if sp1 not online.
			{
				local_transferMode = RESTRICTED;
				other=5;
			}
			else
			{
				local_transferMode = AUTOMATIC;
				other=3;
			}
		}
		else	//Orignal GEN2GEN_OPTION for Northern.
		{
			if (gp_state.inpstat1!=ONLINE)	//bail with msg if sp1 not online.
			{
				abort_transfer_request=4;
			}

			if ((slaveK2_ON()==FALSE) && !GEN_AMPS_PRESENT)	//bail with msg if sp1 not online.
			{
				abort_transfer_request=4;
			}

			local_transferMode = AUTOMATIC;

			if (cb6_CLOSED())	//Generator Power on Input to Converter.
			{
				other=5;
			}
		}

		if (abort_transfer_request)	//bail with msg if sp1 not online.
		{
			lcd_display("TRANSFER ABORTED:                       \0",1,1);
			lcd_display("CONVERTER'S SHORE POWER IS NOT ONLINE.  \0",2,1);
			lcd_display("BOTH MASTER AND SLAVE INPUTS MUST BE ON.\0",3,1);
			lcd_display("             Press any key to exit . . .\0",4,1);
			put_cursor(4,40);
			pause(500);
			wait_for_key();
			lcd_display("                       EXIT IN PROGRESS.\0",4,1);
			new_key=18;
		}
		else
		{
////////////////////////////////////////////////////////
//
//  BUS STATES & transferRequest DEFINED CONSTANTS:
//
//#define BsDEAD		0	// A=off  + B=off
//#define BsGEN1		1	// A=gen1 + B=gen1
//#define BsGEN2		2 	// A=gen2 + B=gen2
//#define BsCONV		3 	// A=conv + B=conv
//#define BsG1AG2B		4 	// A=gen1 + B=gen2
//#define BsG1A			5 	// A=gen1 + B=OFF
//#define BsG2B			6 	// A=OFF  + B=gen2
//#define BsCA			7 	// A=conv + B=OFF
//#define BsCB			8 	// A=OFF  + B=conv
//#define BsG1ACB		9 	// A=gen1 + B=conv
//#define BsCAG2B		10	// A=conv + B=gen2
//
////////////////////////////////////////////////////////
//		if(systat bad) abort
//		if(remoteActivity) abort


//		if(!optconPcbPresent()) abort


			{//do not remove THIS bracket.
			//
			//display present busState and transferRequest choices.	
			//
				// set defaults to make less code.
				f1=1;
				f2=2;
				f3=4;

				switch (busState)	//assign transferRequest function keys for standard requests.
				{
					case 0:					//NO POWER:
							lcd_display(strTransferRequest="NO POWER on BUS-A      NO POWER on BUS-B\0",1,1);
//							lcd_display("BUS-A  OFFLINE            BUS-B  OFFLINE\0",1,1);
							if (NEW_G2G_OPTION)	abort_transfer_request = 6;	
							break;
					case 1:					//GENSET1:
							f1=2;
							f2=other;
							f3=4;
							lcd_display(strTransferRequest="GEN #1 ONLINE  Powering BUS-A and BUS-B\0",1,1);
							break;
					case 2:					//GENSET2:
							f1=1;
							f2=other;
							f3=4;
							lcd_display(strTransferRequest="GEN #2 ONLINE  Powering BUS-A and BUS-B\0",1,1);
							break;
					case 3:					//CONVERTER:
							lcd_display(strTransferRequest="CONVERTER ONLINE  Powering BUS-A & BUS-B\0",1,1);
							break;
					case 4:				//SPLIT-BUS, G1onA G2onB.
							f1=1;
							f2=2;
							f3=other;
							lcd_display(strTransferRequest="GEN #1 ONLINE BUS-A, GEN #2 ONLINE BUS-B\0",1,1);
							break;

//For 5 thru 8 busStats tell 'em to CLOSE TIE-BREAKER and then transfer to 1 of 4 seamless choices.
//	abort_transfer_request = 1;	
					case 5:
							lcd_display(strTransferRequest="GEN #1 ONLINE BUS-A,       OFFLINE BUS-B\0",1,1);
// 							lcd_display("GEN #1 ONLINE BUS-A,   NO POWER on BUS-B\0",1,1);
							if (NEW_G2G_OPTION)	abort_transfer_request = 6;	
							break;
					case 6:
							lcd_display(strTransferRequest="OFFLINE BUS-A,       GEN #2 ONLINE BUS-B\0",1,1);
							if (NEW_G2G_OPTION)	abort_transfer_request = 6;	
							break;
					case 7:
							lcd_display(strTransferRequest="CONVERTER ONLINE BUS-A,    OFFLINE BUS-B\0",1,1);
							if (NEW_G2G_OPTION)	abort_transfer_request = 6;	
							break;
					case 8:
							lcd_display(strTransferRequest="OFFLINE BUS-A,    CONVERTER ONLINE BUS-B\0",1,1);
							if (NEW_G2G_OPTION)	abort_transfer_request = 6;	
							break;

//For 9 & 10 busStats tell 'em to do a SPECIAL COCBO-ONLY TRANSFER.
//:AC3 lets K4 remain open, sync's w/Genset, CLOSE OTHER_EXT_CB and CLOSE TIE-BREAKER.
//	abort_transfer_request = 2;	
					case 9:
							lcd_display(strTransferRequest="GEN #1 ONLINE BUS-A,  CONV. ONLINE BUS-B\0",1,1);
							if (NEW_G2G_OPTION)	abort_transfer_request = 6;	
							break;
					case 10:
							lcd_display(strTransferRequest="CONV. ONLINE BUS-A,  GEN #2 ONLINE BUS-B\0",1,1);
							if (NEW_G2G_OPTION)	abort_transfer_request = 6;	
							break;

					case -1://BsERR1
							lcd_display(strTransferRequest="UNRECOGNIZED BUS STATE\0",1,1);
							if (NEW_G2G_OPTION)	abort_transfer_request = 6;	
							else abort_transfer_request = 4;	
							break;

					case -2://BsERR2
							lcd_display(strTransferRequest="ILLEGAL HEAD-TO-HEAD GENERATOR OPERATION\0",1,1);
							//Operator Choices: 
							//F1 OPEN_CB1 (take GEN1 OFFLINE), 
							//F2 OPEN_CB2 (take GEN2 OFFLINE),
							//F3 OPEN_CB3 (open Tie-Breaker).
							if (NEW_G2G_OPTION)	abort_transfer_request = 6;	
							else abort_transfer_request = 3;	
							break;

					default:				//any other, for now.
							lcd_display(strTransferRequest="BOZO on BUS-A,             BOZO on BUS-B\0",1,1);
							if (NEW_G2G_OPTION)	abort_transfer_request = 6;	
							else abort_transfer_request = 5;	//tell 'em we fucked up majorly and to find a port FAST!
							break;
				}
			}// DO NOT REMOVE THIS BRACKET!!
		}

		if (abort_transfer_request)
		{
			transferRequest=0;
		}
		else
		{
			lcd_display(transferRequestName[f1],2,28);
			lcd_display(transferRequestName[f2],3,28);
			lcd_display(transferRequestName[f3],4,28);

			put_cursor(2,6);
			pause(500);
			wait_for_key();

			switch (new_key)
			{
				case 19://F1:
						transferRequest=f1;
						break;

				case 20://F2:
						transferRequest=f2;
						break;

				case 8:	//F3:
						transferRequest=f3;
						break;

				case 18:	//SYSTEM_STATUS = user abort.
						abort_transfer_request=4;
				default:
						transferRequest=0;
						break;
			}
		}
	}

	if (transferRequest)
	{
		if (transferRequest==5	)	//RESTRICTED
		{
			clear_screen();
			lcd_display("TRANSFER ABORTED.  RESTRICTED SELECTION.\0",1,1);
			lcd_display("SHORE POWER IS NOT ONLINE.\0",2,1);
			lcd_display("             Press any key to exit . . .\0",4,1);
			put_cursor(4,40);
			pause(500);
			wait_for_key();
			lcd_display("                       EXIT IN PROGRESS.\0",4,1);
			transferRequest=0;		//invalidate request.
		}
		else
		{
			clear_screen();
			lcd_display("CAUTION:   POWER TRANSFER IN PROGRESS   \0",1,1);
		}
	}
	else
	{
		if (!abort_transfer_request) abort_transfer_request=4;
	}

	if (abort_transfer_request==3)	//Operator Choices: 
	{								//F1 OPEN_CB1 (take GEN1 OFFLINE), 
									//F2 OPEN_CB2 (take GEN2 OFFLINE),
									//F3 OPEN_CB3 (open Tie-Breaker).
		transferRequest=0;		//clear transferRequest flag.
		lcd_display("Press: 'F1' to take GEN #1 OFFLINE      \0",2,1);
		lcd_display("       'F2' to take GEN #2 OFFLINE      \0",3,1);
		lcd_display("       'F3' to OPEN TIE-BREAKER         \0",4,1);

		put_cursor(2,6);
		pause(500);
		wait_for_key();

		switch (new_key)
		{
			case 19://F1:
					set_GEN_1_CB_OPEN();
					break;

			case 20://F2:
					set_GEN_2_CB_OPEN();
					break;

			case 8:	//F3:
					set_TIE_BREAKER_CB_OPEN();
					break;

			case 18:	//SYSTEM_STATUS = user abort.
			default:
					abort_transfer_request=4;
					break;
		}
	}
	
	if (abort_transfer_request==4)	//==4
	{
		clear_screen();
		lcd_display("TRANSFER ABORTED BY OPERATOR.           \0",1,1);
		lcd_display("             Press any key to exit . . .\0",4,1);
		put_cursor(4,40);
		pause(500);
		wait_for_key();
		lcd_display("                       EXIT IN PROGRESS.\0",4,1);
	}
	
	if (abort_transfer_request==5)	//==
	{
		clear_screen();
		lcd_display("     WE'RE ALL BOZO's ON THIS BUS!      \0",1,1);
		lcd_display("Report this to the proper authorities,  \0",2,1);
		lcd_display("at once!                                \0",3,1);
		lcd_display("             Press any key to exit . . .\0",4,1);
		put_cursor(4,40);
		pause(500);
		wait_for_key();
		lcd_display("                       EXIT IN PROGRESS.\0",4,1);
	}
	
	if (abort_transfer_request==6)	//==6
	{
		lcd_display("TRANSFER ABORTED.  SHIP'S BUS STATE IS  \0",2,1);
		lcd_display("DEAD BUS OR INVALID STATE FOR TRANSFERS.\0",3,1);
		lcd_display("             Press any key to exit . . .\0",4,1);
		put_cursor(4,40);
		pause(500);
		wait_for_key();
		lcd_display("                       EXIT IN PROGRESS.\0",4,1);
	}

	new_key = 0;

	return transferRequest;
}
/*--------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void pwm2_setup_screen()	//extern struct gp_state_struct gp_state;
{
  int edit_field=0;
  int row[2]=	{ 2, 3};
  int column[2]={ 37,37};
  unsigned temp_daq_mux_id=daq_mux_id;
  unsigned temp_sync_mux_id=sync_mux_id;

	set_meter_mux(OSC, OSC_sync);

	clear_screen();
	lcd_display("   CONVERTER OUTPUT IMPEDANCE CONTROL   \0",1,1);
	lcd_display("Nominal Impedance (Zo)  Duty Cycle:    %\0",2,1);
	if (gp_state.local_master==MASTER)
	{
		lcd_display("Transfer Impedance      Duty Cycle:    %\0",3,1);
		lcd_display(" More    Less    Forward   Back    Exit \0",4,1);
	}
	else
	{
		lcd_display("SLAVE LOAD MANAGEMENT IMPEDANCE CONTROL \0",1,1);
		lcd_display(" More    Less                      Exit \0",4,1);
	}

	lcd_display_value(mach.pwm2_dutyCycle,row[0],column[0],"%3.0f\0");	//edit_field==1

	if (gp_state.local_master==MASTER)
	{
		lcd_display_value(mach.xfr_dutyCycle,row[1],column[1],"%3.0f\0");	//edit_field==1
	}

	put_cursor(row[edit_field],column[edit_field]);

	delay(250); //wait a 1/2 sec. for operator to see screen.

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{

		lcd_display_value(mach.pwm2_dutyCycle,row[0],column[0],"%3.0f\0");	//edit_field==1

		if (gp_state.local_master==MASTER)
		{
			lcd_display_value(mach.xfr_dutyCycle,row[1],column[1],"%3.0f\0");	//edit_field==1
		}

		put_cursor(row[edit_field],column[edit_field]);

		delay(250);			//adjust keypad response here (i.e. slew rate control).
		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1 (++):
					switch (edit_field)
					{
						case 0:	//mach.pwm2_state field
								if (mach.pwm2_dutyCycle < 100)
								{
									mach.pwm2_dutyCycle++;
								}
								break;
						case 1:	//mach.xfr_dutyCycle field
								if (mach.xfr_dutyCycle < 100)
								{
									mach.xfr_dutyCycle++;
								}
								break;
						default: 
								edit_field=0;
								break;
					}
					break;

			case 20:	//F2 (--):
					switch (edit_field)
					{
						case 0:	//mach.pwm2_state field
								if (mach.pwm2_dutyCycle > 0)
								{
									mach.pwm2_dutyCycle--;
								}
								break;
						case 1:	//mach.xfr_dutyCycle field
								if (mach.xfr_dutyCycle > 0)
								{
									mach.xfr_dutyCycle--;
								}
								break;
						default: 
								edit_field=0;
								break;
					}
					break;

			case 8:			//F3 				//advance cursor.
			case 17:		//CONVERTER POWER	//advance cursor.
					if (gp_state.local_master==MASTER)
					{
						if (edit_field<=0)
						{
							edit_field=1;
						}
						else
						{
							edit_field--;	
						}
						put_cursor(row[edit_field],column[edit_field]);
						delay(250);	
					}
					break;

			case 9:			//F4				//backup cursor.
			case 16:		//SHORE POWER		//backup cursor.
					if (gp_state.local_master==MASTER)
					{
						if (edit_field>=1)
						{
							edit_field=0;
						}
						else
						{
							edit_field++;	
						}
						put_cursor(row[edit_field],column[edit_field]);
						delay(250);	
					}
					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(250);	
						break;

			default:
					break;
		}
		init_PWM2(mach.pwm2_bits,mach.pwm2_align,mach.pwm2_dutyCycle);		//unsigned(bits,align,duty)
	}
	set_meter_mux(temp_daq_mux_id, temp_sync_mux_id);	//restore daq & sync mux settings.

	new_key = 0;
}
/*---------------------------------------------------------------------------*/
#ifdef TEST_FW
//
//ASEA TEST PURPOSES ONLY
//
#define VoMIN  (GP_OUTPUT_VOLTAGE - 1.00 * GP_OUTPUT_VOLTAGE)
#define VoMAX  (GP_OUTPUT_VOLTAGE + 0.10 * GP_OUTPUT_VOLTAGE)
void vout_setup_screen()	//programmable Vout via Front Panel, v1.99.
{
  float oldVoltage=newVoltage;

	clear_screen();
	lcd_display("    CONVERTER OUTPUT VOLTAGE CONTROL    \0",1,1);
	lcd_display(" Vout =       \0",2,1);
//	lcd_display(" More    Less     xxxV             Exit \0",4,1);
	lcd_display(" More    Less     MORE     LESS    Exit \0",4,1);

//	lcd_display_value(GP_OUTPUT_VOLTAGE,4,19,"%3.0f\0");	//show default Vprog/Vout.

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		if (newVoltage<0.0) newVoltage=0.0;

		lcd_display_value(newVoltage,2,9,"%3.1f\0");	//show Vprog/Vout

		put_cursor(2,9);
		pause(250);
		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:

					if (newVoltage<VoMAX)
					{
						newVoltage = newVoltage + 0.5;
						slew_output(0.5, newFrequency, newVoltage);
					}
					break;

			case 20:	//F2:

					if (newVoltage>VoMIN)
					{
						newVoltage = newVoltage - 0.5;
						slew_output(0.5, newFrequency, newVoltage);
					}
					break;
			case 8:	//F3:

					if (newVoltage<VoMAX)
					{
						newVoltage = newVoltage + 5.0;
						slew_output(0.5, newFrequency, newVoltage);
					}
					break;

			case 9:	//F4:

					if (newVoltage>VoMIN)
					{
						newVoltage = newVoltage - 5.0;
						slew_output(0.5, newFrequency, newVoltage);
					}
					break;

//			case 8:	//F3:
//					newVoltage = GP_OUTPUT_VOLTAGE;
//					slew_output(0.5, newFrequency, newVoltage);
//					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(250);	
						break;

			default:
					break;
		}
	}
	new_key = 0;

//	if (newVoltage != oldVoltage) LogEvent(Ev_PGM_VOUT,0);
}
/*---------------------------------------------------------------------------*/
#else
//
//ASEA PRODUCTION VERSION
//
#define VoMIN  (GP_OUTPUT_VOLTAGE - 0.05 * GP_OUTPUT_VOLTAGE)
#define VoMAX  (GP_OUTPUT_VOLTAGE + 0.05 * GP_OUTPUT_VOLTAGE)
void vout_setup_screen()	//programmable Vout via Front Panel, v1.99.
{
  float oldVoltage=newVoltage;

	clear_screen();
	lcd_display("    CONVERTER OUTPUT VOLTAGE CONTROL    \0",1,1);
	lcd_display(" Vout =       \0",2,1);
	lcd_display(" More    Less        V             Exit \0",4,1);

	lcd_display_value(GP_OUTPUT_VOLTAGE,4,19,"%3.0f\0");	//show default Vprog/Vout.

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display_value(newVoltage,2,9,"%3.1f\0");	//show Vprog/Vout

		put_cursor(2,9);
		pause(250);
		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:

					if (newVoltage<VoMAX)
					{
						newVoltage = newVoltage + 0.5;
						slew_output(0.5, newFrequency, newVoltage);
					}
					break;

			case 20:	//F2:

					if (newVoltage>VoMIN)
					{
						newVoltage = newVoltage - 0.5;
						slew_output(0.5, newFrequency, newVoltage);
					}
					break;

			case 8:	//F3:

					newVoltage = GP_OUTPUT_VOLTAGE;
					slew_output(0.5, newFrequency, newVoltage);
					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(250);	
						break;

			default:
					break;
		}
	}
	new_key = 0;

//	if (newVoltage != oldVoltage) LogEvent(Ev_PGM_VOUT,0);
}
#endif //TEST_FW
/*---------------------------------------------------------------------------*/
void AGC_control_screen()	//automatic gain control enable/disable screen.
{
	clear_screen();
	lcd_display("           AGC CONTROL SCREEN           \0",1,1);
	lcd_display(" Automatic Gain Control is:             \0",2,1);
	lcd_display("Enable  Disable                    Exit \0",4,1);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		if(AGC_state==ENABLED)
		{
			lcd_display(" ENABLED \0",2,29);
		}
		else
		{
			lcd_display("DISABLED \0",2,29);
		}

		put_cursor(2,29);
		delay(250);
		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
					AGC_state = ENABLED;
					ALC_mode = AGC_state;
					break;

			case 20:	//F2:
					AGC_state = DISABLED;
					ALC_mode = AGC_state;
					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(250);	
						break;

			default:
					break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
/*
void extreme_values_screen()	//clearable, extremes over system life..
{
	clear_screen();
	lcd_display("      EXTREME SYSTEM MEASUREMENTS       ",1,1);
	lcd_display(" Maximum System Level: xxx  %           ",2,1);
	lcd_display(" Maximum System Power: xxx  kW          ",3,1);
	lcd_display("Refresh                            Exit ",4,1);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display("     ",2,24);	//clear field
		lcd_display("     ",3,24);	//clear field
		lcd_display_value(maxSystemLevel,2,24,"%4.1f");
		lcd_display_value(maxSystemPower,3,24,"%4.2f");

		pause(250);
		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
						maxSystemLevel = 0.0f;
						maxSystemPower = 0.0f;
						break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(250);	
						break;
			default:
						break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void extreme_values_screen()	//clearable, extremes over system life..
{
	clear_screen();
	lcd_display("GENERATOR FREQUENCY ANALYSIS DISPLAY\0",1,3);
	lcd_display("Generator MIN Frequency:       Hz       \0",2,1);
	lcd_display("Generator MAX Frequency:       Hz       \0",3,1);
	lcd_display("Refresh                            Exit \0",4,1);

	lcd_display("      \0",2,26);	//clear field
	lcd_display("      \0",3,26);	//clear field

	lcd_display_value(minGenFreq,2,26,"%6.2f\0");

	if (maxGenFreq<1000.0) lcd_display_value(maxGenFreq,3,26,"%6.2f\0");
	else lcd_display(">1000\0",3,26);

	special_function=1;

	switch (combo_key)
//	switch (new_key)
	{
		case 0:	//F1:
//		case 19:	//F1:
					minGenFreq = GP_OUTPUT_FREQUENCY;
					maxGenFreq = 0.0f;
					combo_key = 86;
					break;

		case 5:	//F5:
//		case 10:	//F5:
					special_function=0;
					new_key = 18;	//force SYSTEM STATUS key press response.
					display_function = F1; 
					display_type = SYSTEM_STATUS; //bail to default display.
					combo_key = 86;
					break;
		default:
					break;
	}

//	combo_key = 86;
//	delay(250);	
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void wattHour_screen()	//Resetable Watt-Hour Meter.
{
  unsigned long wDays=0;
  unsigned long wHours=0;
  unsigned long wMinutes=0;
  unsigned long wSeconds=0;
  unsigned long tempTs=0;			//timeType is UNSIGNED LONG.
  unsigned row=2;
  
	lcd_display("            kWATT-HOUR METER            \0",1,1);
	lcd_display("kW-Hours:          Run-Time:            \0",2,1);
	lcd_display(" Max Level:      % Max Power:         kW\0",3,1);
	lcd_display(" Clear                             Exit \0",4,1);

	if (maxLevelIn1<999.99) lcd_display_value(maxLevelIn1,3,13,"%4.1f\0");
	else lcd_display(">1000\0",3,13);

	if (maxPowerIn1<999.99) lcd_display_value(maxPowerIn1,3,31,"%4.2f\0");
	else lcd_display(">1000\0",3,33);

	//display EvTs
	tempTs = wattTime;
	wDays = tempTs/8640000;
	tempTs = tempTs - (wDays * 8640000);
	wHours = tempTs/360000;
	tempTs = tempTs - (wHours * 360000);
	wMinutes = tempTs/6000;
	tempTs = tempTs - (wMinutes * 6000);
	wSeconds = tempTs/100;

	lcd_display_value(wDays,row,29,"%3.0f\0");
	lcd_display(":\0",row,32);
	lcd_display_value(wHours,row,33,"%2.0f\0");
	lcd_display(":\0",row,35);
	lcd_display_value(wMinutes,row,36,"%2.0f\0");
	lcd_display(":\0",row,38);
	lcd_display_value(wSeconds,row,39,"%2.0f\0");
	if (wHours<10) lcd_display("0\0",row,33);
	if (wMinutes<10) lcd_display("0\0",row,36);
	if (wSeconds<10) lcd_display("0\0",row,39);

//	lcd_display_value(wattHours,row,10,"%.2f\0");

	if (wattHours<=0.009) lcd_display("0.00\0",row,10);
	else lcd_display_value(wattHours,row,10,"%.2f\0");			//Shore wattHours

//	delay(250);	
	special_function=1;

	switch (combo_key)
	{
		case 0:	//F1:
					wattTime = 0;
					lastWattTime = 1000;
					wattHours = 0.0;
					maxLevelIn1 = 0.0;
					maxPowerIn1 = 0.0;
					maxSystemLevel = 0.0;
					maxSystemPower = 0.0;
					combo_key = 86;
					break;

		case 5:	//F5:
					special_function=0;
					new_key = 18;	//force SYSTEM STATUS key press response.
					display_function = F1; 
					display_type = SYSTEM_STATUS; //bail to default display.
					combo_key = 86;
					break;
		default:
					break;
	}

//	combo_key = 86;
//	delay(250);	
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*
void setup_screen()
{
  static unsigned itemSelected=1;
  char *ms_units="ms";

	if (fresh_screen)
	{		
		if (valid_password_entered())
		{
			delay(250);        //give time for operator to release key.
		}
		else
		{
			display_type = CLEAR;
			return;
		}
	}

	lcd_display("THREE PHASE POWER CONVERTER SETUP SCREEN",1,1);
	lcd_display("SELECT ITEM:                            ",2,1);
	lcd_display(" SETTING NOW:                           ",3,1);
	lcd_display(" More    Less    Cursor    Back         ",4,1);
////////////////////////////////////////////////////////////////
//				 1234567 1234567 12345678 1234567 1234567
//				 1234567890123456789012345678901234567890
//				          1         2         3         4
////////////////////////////////////////////////////////////////

	switch (itemSelected)
	{
		case 1:	//parallelFilter //default = FALSE;
				lcd_display("1. PARALLELING FILTER     ",2,14);
				lcd_display(state[parallelFilter],3,15);
				break;
		case 2:	//parallelGainDelay //default = 1;
				lcd_display("2. PARALLEL GAIN DELAY    ",2,14);
				lcd_display(utoa(parallelGainDelay),3,15);
				break;
		case 3:	//parrallelPowerCorrectionThreshold //default = 20
				lcd_display("3. PARALLELING WINDOW     ",2,14);
				lcd_display_value(parrallelPowerCorrectionThreshold,3,15,"%5.1f");
				break;

		case 4:	//actuatorResponseDelay //default = 500ms
				lcd_display("4. ACTUATOR RESPONSE DELAY",2,14);
				lcd_display(utoa(actuatorResponseDelay),3,15);
				lcd_display(ms_units,3,20);
				break;

		case 5:	//parallelPowerOffset //default = 512
				lcd_display("5. PARALLEL POWER OFFSET  ",2,14);
				lcd_display(itoa(parallelPowerOffset),3,15);
				lcd_display_value(get_parallel_power(),3,30,"%8.3f");	//PARALLELING POWER DIFFERENCE TERM.
				break;

		case 6:	//DEF_dac_2A_value //default = C000
				lcd_display("6. LOAD VOLTAGE NULL LEVEL",2,14);
				lcd_display(utoa(DEF_dac_2A_value),3,15);
				lcd_display_value(get_parallel_power(),3,30,"%8.3f");	//PARALLELING POWER DIFFERENCE TERM.
				break;

		case 7:	//measuredPowerScale //default = C000
				lcd_display("7. LOAD GAIN ADJUSTMENT   ",2,14);
				lcd_display(utoa(measuredPowerScale),3,15);
				break;

		case 8:	//pulseDwell //default = 200ms
				lcd_display("8. SPEED-TRIM PULSE DWELL ",2,14);
				lcd_display(utoa(pulseDwell),3,15);
				lcd_display(ms_units,3,20);
				break;

		default:
				itemSelected=1;
				break;
	}

	switch (edit_field)
	{
		case 0:	//itemSelected
				lcd_display(" Next    Prev.   Select",4,1);
				put_cursor(2,13);
				break;
		case 1:	//itemValue
				if (itemSelected==1) lcd_display("  On      Off ",4,1);
				put_cursor(3,15);
				break;
		case 2:	//auxValue
				put_cursor(3,36);
				break;
		default:
				edit_field=0;
				break;
	}

	if (fresh_screen) 
	{
		delay(250);        //give time for operator to release key.
		key_press=NONE;
	}

	if (itemSelected==5||itemSelected==6) 
	{
		strobe_kybd_for_slew();	//dynamic data displayed.
		delay(100);
	}
	else
	{
		wait_for_key();
	}

	switch (key_press)
	{
		case F1:
				switch (edit_field)
				{
					case 0:	//itemSelected
							if (PulsedSpeedTrim)
							{
								if (itemSelected<8) itemSelected++;
							}
							else
							{
								if (itemSelected<7) itemSelected++;
							}
							break;
					case 1:	//itemValue
							switch (itemSelected)
							{
								case 1:	//parallelFilter
										parallelFilter=TRUE;
										break;
								case 2:	//parallelGainDelay
										if (parallelGainDelay<1000) parallelGainDelay++;
										break;
								case 3:	//parrallelPowerCorrectionThreshold
										if (parrallelPowerCorrectionThreshold<1000) parrallelPowerCorrectionThreshold = parrallelPowerCorrectionThreshold+1;
										break;
								case 4:	//actuatorResponseDelay
										if (actuatorResponseDelay<10000) actuatorResponseDelay = actuatorResponseDelay+10;
										break;
								case 5:	//parallelPowerOffset
										if (parallelPowerOffset<600) parallelPowerOffset = parallelPowerOffset+1;
										break;
								case 6:	//DEF_dac_2A_value
										if (DEF_dac_2A_value<53248) DEF_dac_2A_value = DEF_dac_2A_value+1;
										break;
								case 7:	//measuredPowerScale
										if (measuredPowerScale<2000) measuredPowerScale = measuredPowerScale+1;
										break;
								case 8:	//pulseDwell
										if (pulseDwell<10000) pulseDwell = pulseDwell+10;
										break;

								default:
										itemSelected=1;
										break;
							}
							break;

					case 2:	//auxValue
//							switch (itemSelected)
//							{
//								case 1:	//speed-trim DAC
//										if (dac_1B_value<0xFF00) dac_1B_value = dac_1B_value+0x10;
//										break;
//								case 2:	//voltage-trim DAC
//										if (dac_2B_value<0xFFF0) dac_2B_value = dac_2B_value+0x10;
//										break;
//								case 3:	//spare DAC
//										if (dac_3B_value<0xFF00) dac_3B_value = dac_3B_value+0x10;
//										break;
//								default:
//										itemSelected=1;
//										break;
//							}
//							break;

					default:
							edit_field=0;
							break;
				}
				key_press=NONE;
				break;

		case F2:
				switch (edit_field)
				{
					case 0:	//itemSelected
							if (itemSelected>1) itemSelected--;
							break;
					case 1:	//dac_A_value
							switch (itemSelected)
							{
								case 1:	//parallelFilter
										parallelFilter=FALSE;
										break;
								case 2:	//parallelGainDelay
										if (parallelGainDelay>1) parallelGainDelay--;
										break;
								case 3:	//parrallelPowerCorrectionThreshold
										if (parrallelPowerCorrectionThreshold>1) parrallelPowerCorrectionThreshold = parrallelPowerCorrectionThreshold-1;
										break;
								case 4:	//actuatorResponseDelay
										if (actuatorResponseDelay>200) actuatorResponseDelay = actuatorResponseDelay-10;
										break;
								case 5:	//parallelPowerOffset
//										if (parallelPowerOffset>400) parallelPowerOffset = parallelPowerOffset-1;
										if (parallelPowerOffset>0) parallelPowerOffset = parallelPowerOffset-1;
										break;
								case 6:	//DEF_dac_2A_value
										if (DEF_dac_2A_value>0) DEF_dac_2A_value = DEF_dac_2A_value-1;
										break;
								case 7:	//measuredPowerScale
										if (measuredPowerScale>20) measuredPowerScale = measuredPowerScale-1;
										break;
								case 8:	//pulseDwell
										if (pulseDwell>100) pulseDwell = pulseDwell-10;
										break;

								default:
										itemSelected=1;
										break;
							}
							break;

					case 2:	//auxValue
//							switch (itemSelected)
//							{
//								case 1:	//speed-trim DAC
//										if (dac_1B_value>0) dac_1B_value = dac_1B_value-0x10;
//										break;
//								case 2:	//voltage-trim DAC
//										if (dac_2B_value>0) dac_2B_value = dac_2B_value-0x10;
//										break;
//								case 3:	//spare DAC
//										if (dac_3B_value>0) dac_3B_value = dac_3B_value-0x10;
//										break;
//								default:
//										itemSelected=1;
//										break;
//							}
//							break;

					default:
							edit_field=0;
							break;
				}
				key_press=NONE;
				break;
*/
/* 
		case F3:
//				if (edit_field<2)
				if (edit_field<1)
				{
					edit_field++;	//next edit field.
				}
				else
				{
					edit_field = 0;	//wrap edit field.
				}
				delay(250);
				fresh_screen = TRUE;
				key_press=NONE;
				break;

		case F4:
//
//AUTO-CALIBRATE PARALLELING DAC NULL POINT
//
				if (MY_PARALLEL && (!gmm_state.generator1_contactor_state) && itemSelected==1)
				{
					calibrateMyParallel();
				}
				else
				{
					display_type = TEST_PCB; //bail to other test display.
				}
				key_press=NONE;
				break;

		default:
				break;
	}
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
//
//BESECKE TRANSFER CONTROL
//
//  Front-end for MULTIGEN_BESECKE systems.  Option selection got us here.
//
//			1.	Orignal MULTIGEN_BESECKE for Lurssen Yachts.  Uses Tie-Breaker,
//				 Gen Input to Conv., all generators on other side of tie.
//				To/From MIXED-MODE transfers (split conv/gen bus) allowed.
//
//			MODES of Operation (based on switch input to 604278 TB14-3,-4.
//					AUTOMATIC => Full transfer capibility allowed.
//					MANUAL => No Automatic transfers (inhibit for maintenance)
//
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
void besecke_transfer_info_display()
{
  int bus=get_busState2();
  unsigned u2Q1;			//=((optcon_status_2 & 0x1000) == 0x1000);	//EXTERNAL_CB_CLOSED()
  unsigned u2Q2=((optcon_status_2 & 4) == 4);			//tieBreaker_CLOSED()
//  int genReady=process_MULTIGEN_REM_XFR_REQ();		//used to auto-select genset.

	if (OPTCON_PCB)
		u2Q1=((optcon_status_2 & 0x1000) == 0x1000);	//EXTERNAL_CB Confirmation signal.
	else
		u2Q1=(_getbit (P5, 10));						//Converter Output C/B Confirmation


////////////////////////////////////////////////////////////////
//	lcd_display(" CONVERTER TRANSFER INFORMATION DISPLAY \0",1,1);
////////////////////////////////////////////////////////////////
//				 1234567 1234567 12345678 1234567 1234567
////////////////////////////////////////////////////////////////
	lcd_display("MAIN Bus:            LIGHT Bus:         \0",1,1);
	lcd_display("2Q1:      , 2Q2:      ,                 \0",2,1);
	lcd_display("CONVERTER Key for:                      \0",3,1);
	lcd_display("GENERATOR Key for:                      \0",4,1);
	
	lcd_display(cbState[u2Q1],2,5);		//EXTERNAL_CB_CLOSED()
	lcd_display(cbState[u2Q2],2,17);	//tieBreaker_CLOSED()

	lcd_display("                \0",2,25);

	if (flash)
		lcd_display("Last Transfer:  \0",2,25);
	else
		lcd_display(getXfrErrCodeDescriptor(last_transfer_status),2,25);

	switch (bus)
	{
		case 0:
				lcd_display("BLACK-OUT\0",1,10);		//MAIN Bus
				lcd_display("BLACK-OUT\0",1,32);		//LIGHT Bus
				lcd_display("Converter Both Buses \0",3,20);		//CONVERTER Key
				lcd_display("Selected GEN Mode    \0",4,20);		//GENERATOR Key
				break;
		case 2:
				lcd_display("GENERATOR\0",1,10);		//MAIN Bus
				lcd_display("GENERATOR\0",1,32);		//LIGHT Bus
				lcd_display("Converter Both Buses \0",3,20);		//CONVERTER Key
			
				if (!u2Q1)
					lcd_display(" 2Q1 MUST BE CLOSED! \0",4,20);
				else 
					lcd_display("Mixed-Mode, Gen+Conv \0",4,20);		//GENERATOR Key
				break;
		case 3:
				lcd_display("CONVERTER\0",1,10);		//MAIN Bus
				lcd_display("CONVERTER\0",1,32);		//LIGHT Bus
				lcd_display("Converter Already On \0",3,20);		//CONVERTER Key
				lcd_display("Selected GEN Mode    \0",4,20);		//GENERATOR Key
				break;
		case 6:
				lcd_display("GENERATOR\0",1,10);		//MAIN Bus
				lcd_display("BLACK-OUT\0",1,32);		//LIGHT Bus
				lcd_display("Mixed-Mode, Gen+Conv \0",3,20);		//CONVERTER Key
				lcd_display("Selected GEN Mode    \0",4,20);		//GENERATOR Key
				break;
		case 7:
				lcd_display("BLACK-OUT\0",1,10);		//MAIN Bus
				lcd_display("CONVERTER\0",1,32);		//LIGHT Bus
				lcd_display("Converter Both Buses \0",3,20);		//CONVERTER Key
				lcd_display("Selected GEN Mode    \0",4,20);		//GENERATOR Key
				break;
		case 10:
				lcd_display("GENERATOR\0",1,10);		//MAIN Bus
				lcd_display("CONVERTER\0",1,32);		//LIGHT Bus
				lcd_display("Converter Already On \0",3,20);		//CONVERTER Key
				lcd_display("Selected GEN Mode    \0",4,20);		//GENERATOR Key
				break;
		case 11:
				lcd_display("BLACK-OUT\0",1,10);		//MAIN Bus
				lcd_display("BLACK-OUT\0",1,32);		//LIGHT Bus
				lcd_display("Converter LIGHT Bus  \0",3,20);		//CONVERTER Key
				lcd_display("Selected GEN MAIN Bus\0",4,20);		//GENERATOR Key
				break;
		case 15:
				lcd_display("GENERATOR\0",1,10);		//MAIN Bus
				lcd_display("CONVERTER\0",1,32);		//LIGHT Bus
				if (transfer_in_progress)
				{
					lcd_display("Transfer In Progress \0",3,20);		//CONVERTER Key	
					lcd_display("Transfer In Progress \0",4,20);		//GENERATOR Key
				}
				else
				{
					lcd_display("Gen&Conv.Head-To-Head\0",3,20);		//CONVERTER Key	
					lcd_display("Gen&Conv.Head-To-Head\0",4,20);		//GENERATOR Key
				}
				break;
		default:
				lcd_display(" Unknown \0",1,10);		//MAIN Bus
				lcd_display(" Unknown \0",1,32);		//LIGHT Bus
				lcd_display("                     \0",3,20);		//CONVERTER Key
				lcd_display("BUS:                 \0",4,20);		//GENERATOR Key
				lcd_display(itoa(bus),4,25);
				break;
	}

	if (!u2Q1)  					lcd_display(" 2Q1 MUST BE CLOSED! \0",3,20);
	if (gp_state.inpstat1!=ONLINE)	lcd_display("Shore Power is NOT ON\0",3,20);

	if (manual_mode())
	{
		lcd_display("System in MANUAL MODE\0",3,20);		//CONVERTER Key	
		lcd_display("System in MANUAL MODE\0",4,20);		//GENERATOR Key
	}

	if (new_key==18)
	{
		display_type = SYSTEM_STATUS; 
		display_function = F1; 
	}
//	lcd_display(itoa(genReady),4,40);	//show which genset will be the auto-selected genset
}
/*---------------------------------------------------------------------------*/
void auto_shutdown_control_screen()
{
	clear_screen();
	lcd_display("AUTO-SHUTDOWN CONTROL\0",1,8);
	lcd_display("SHUTDOWN \0",2,1);
	lcd_display("Enable  Disable                    Exit \0",4,1);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		if(autoShutdownEnable==ENABLED)
		{
			lcd_display(" ENABLED \0",2,10);
		}
		else
		{
			lcd_display("DISABLED \0",2,10);
		}

		put_cursor(2,10);
		delay(250);
		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
					autoShutdownEnable = ENABLED;		//inhibit if overload.
					break;

			case 20:	//F2:
					autoShutdownEnable = DISABLED;	//do not inhibit.
					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(500);	
						break;

			default:
					break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void auto_transfer_control_screen()	//automatic transfer to CONV on autorestart.
{
  int edit_field=0;
  int row[2]=	{ 2, 2};
  int column[2]={ 11,37};

	clear_screen();
	lcd_display("AUTO-TRANSFER ON AUTO-RESTART CONTROLS\0",1,2);
	lcd_display("Feature:         ,   Ignore Slave:\0",2,2);
	lcd_display("Enable  Disable  Forward   Back    Exit\0",4,1);


	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		if(auto_transfer_enable)
		{
			lcd_display(" ENABLED \0",2,11);
		}
		else
		{
			lcd_display("DISABLED \0",2,11);
		}

		if(AUTO_TRANSFER)
		{
			lcd_display("YES\0",2,37);
		}
		else
		{
			lcd_display(" NO\0",2,37);
		}

		if (edit_field)	//if (edit_field==1)
		{
			lcd_display(" Yes      No     Forward   Back    Exit \0",4,1);
		}
		else	//else (edit_field==0)
		{
			lcd_display("Enable  Disable  Forward   Back    Exit \0",4,1);
		}

		put_cursor(row[edit_field],column[edit_field]);
		pause(250);
		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
					switch (edit_field)
					{
						case 0:	//
								auto_transfer_enable = ENABLED;
								break;
						case 1:	//
								AUTO_TRANSFER = ENABLED;
								break;
						default: 
								edit_field=0;
								break;
					}
					break;

			case 20:	//F2:
					switch (edit_field)
					{
						case 0:	//
								auto_transfer_enable = DISABLED;
								break;
						case 1:	//
								AUTO_TRANSFER = DISABLED;
								break;
						default: 
								edit_field=0;
								break;
					}
					break;

			case 8:		//F3	//advance cursor.
			case 17:		//CONVERTER POWER	//advance cursor.
					{
						if (edit_field<=0)
						{
							edit_field=1;
						}
						else
						{
							edit_field--;	
						}
						delay(250);	
					}
					break;

			case 9:		//F4	//backup cursor.
			case 16:		//SHORE POWER		//backup cursor.
					{
						if (edit_field>=1)
						{
							edit_field=0;
						}
						else
						{
							edit_field++;	
						}
						delay(250);	
					}
					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(250);	
						break;

			default:
					break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void cord_capacity_setup_screen()	//programmable shore cord capacity via Front Panel, v2.00.
{
  int icordRating[10]={30,32,50,60,63,100,125,150,200,250};
  int iCord;
  float capacity;
  float voltageNom;
  float voltageLL;
  
  float rated_power=RATED_POWER;
  float rated_amps;

//
//Validate Vin before falling into loop...
//
	if(voltage_in_range(SP1, SP1A_sync, 170.0, 600.0))
	{
		voltageNom=gp_Vab[SP1];
	}
	else
	{
		clear_screen();
		lcd_display("SHORE POWER VOLTAGE OUT OF RANGE!\0",3,1);
		lcd_display("Any key to continue ...                 \0",4,1);
		wait_for_key();
		new_key=0;
		return;
	}

	switch (shoreCordRating)
	{
		case 30:	iCord = 0;	break;
		case 32:	iCord = 1;	break;
		case 50:	iCord = 2;	break;
		case 60:	iCord = 3;	break;
		case 63:	iCord = 4;	break;
		case 100:	iCord = 5;	break;
		case 125:	iCord = 6;	break;
		case 150:	iCord = 7;	break;
		case 200:	iCord = 8;	break;
		case 250:	iCord = 9;	break;
		default:	iCord = 9;	break;
	}

	clear_screen();

			   //12345678901234567890123456789012345678901234567890
	lcd_display("SHORE CORD SETUP CONV.CAPACITY:      kVA\0",1,1);
	lcd_display("MASTER CORD CAPACITY:    Amps           \0",2,1);
	lcd_display("VOLTAGE:    Vac, FORM:  \01, Freq.:   Hz\0",3,1);
	lcd_display(" More    Less                      Exit \0",4,1);

	if (( 184.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 195.0 )) voltageNom = 190.0;
	else if (( 195.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 204.0 )) voltageNom = 200.0;
	else if (( 204.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 214.0 )) voltageNom = 208.0;
	else if (( 214.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 225.0 )) voltageNom = 220.0;
	else if (( 225.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 235.0 )) voltageNom = 230.0;
	else if (( 235.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 250.0 )) voltageNom = 240.0;
	else if (( 250.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 270.0 )) voltageNom = 260.0;
	else if (( 370.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 390.0 )) voltageNom = 380.0;
	else if (( 390.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 408.0 )) voltageNom = 400.0;
	else if (( 408.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 425.0 )) voltageNom = 415.0;
	else if (( 425.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 455.0 )) voltageNom = 440.0;
	else if (( 470.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 490.0 )) voltageNom = 480.0;
	else voltageNom = gp_Vab[SP1];

	voltageLL = voltageNom;

	if (gp_state.input1_form==THREE_PHASE)
	{
		voltageNom = voltageNom * ONE_SQRT3;
	}

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		if (gp_state.range_mode_id==LOW_RANGE)
		{
			rated_amps = IN_RATED_AMPS_LO_ONE;
		}
		else
		{
		 	rated_amps = IN_RATED_AMPS_HI_ONE;
		}

		if (gp_state.input1_form==THREE_PHASE)
		{
//			rated_power = rated_power/3;
		}
		else	// ONE_PHASE
		{
			rated_amps = rated_amps * 1.5;
//			rated_amps = rated_amps * 1.732;
		}

		capacity = (gp_state.input1_form * icordRating[iCord] * voltageNom) / 1000.0;

		if (capacity > (gp_state.input1_form * rated_amps * voltageNom / 1000.0))
		{
	 		capacity = gp_state.input1_form * rated_amps * voltageNom / 1000.0;
		}

		if (capacity > rated_power)
		{
			capacity = rated_power;
		}

		lcd_display_value(capacity,1,33,"%5.1f\0");
		lcd_display_value(voltageLL,3,10,"%3.0f\0");	//show default Vprog/Vout.
		lcd_display(itoa(gp_state.input1_form),3,24);	//show default Vprog/Vout.
		lcd_display_value(gp_Freq[SP1],3,35,"%2.0f\0");	//show default Vprog/Vout.
		lcd_display_value(shoreCordRating,2,22,"%3.0f\0");		//edit_field==0

		lastMasterCapacity = capacity;
	
		put_cursor(2,24);

		pause(250);
		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
						if (iCord < 9)
						{
							iCord++;
						}

						shoreCordRating = icordRating[iCord];
						break;

			case 20:	//F2:
						if (iCord > 0)
						{
							iCord--;
						}

						shoreCordRating = icordRating[iCord];
						break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(250);	
						break;

			case 8:	//F3:
						break;

			default:
						break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void droop_control_screen()	//Load Management.
{
  int edit_field=0;
  unsigned long droopDampMinutes=droopDampTime/6000;	//convert from eventTime to minutes.

	clear_screen();
	lcd_display("LOAD MANAGEMENT DROOP CONTROL\0",1,7);
	lcd_display(" Droop   % of Vout at Shore Cord Alarm  \0",2,1);
	lcd_display(" Droop:         , Recovery in    minutes\0",3,1);
	lcd_display(" More    Less    Forward   Back    Exit \0",4,1);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display("  \0",2,8);	//clear field
		lcd_display("  \0",3,31);	//clear field

		if (droop < 10)
		{
			lcd_display(itoa(droop),2,9);
		}
		else
		{
			lcd_display(itoa(droop),2,8);
		}
	
		if(droopEnable==ENABLED)
		{
			lcd_display(" ENABLED\0",3,9);
		}
		else
		{
			lcd_display("DISABLED\0",3,9);
		}

		if (droopDampMinutes < 10)
		{
			lcd_display(itoa(droopDampMinutes),3,31);
		}
		else
		{
			lcd_display(itoa(droopDampMinutes),3,31);
		}

		switch (edit_field)
		{
			case 0:
					lcd_display(" More    Less    Forward   Back    Exit \0",4,1);
					put_cursor(2,9);
					break;
			case 1:
					lcd_display("Enable  Disable  Forward   Back    Exit \0",4,1);
					put_cursor(3,9);
					break;
			case 2:
					lcd_display(" More    Less    Forward   Back    Exit \0",4,1);
					put_cursor(3,31);
					break;
		}

		pause(250);
		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
						switch (edit_field)
						{
							case 0:
									if (droop<5) droop++;
									break;
							case 1:
									droopEnable = ENABLED;		//droop if cord alarm.
									break;
							case 2:
									if (droopDampMinutes<60) droopDampMinutes++;
									break;
						}
						break;

			case 20:	//F2:
						switch (edit_field)
						{
							case 0:
									if (droop>1) droop--;
									break;
							case 1:
									droopEnable = DISABLED;		//droop if cord alarm.
									break;
							case 2:
									if (droopDampMinutes>1) droopDampMinutes--;
									break;
						}
						break;

			case 8:		//F3:
						if (edit_field>=2)
						{
							edit_field=0;
						}
						else
						{
							edit_field++;	
						}
						break;

			case 9:	//F4:
						if (edit_field<=0)
						{
							edit_field=2;
						}
						else
						{
							edit_field--;	
						}
						break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(250);	
						break;

			default:
					break;
		}
	}

	if (droopEnable==DISABLED)
	{
		newVoltage = GP_OUTPUT_VOLTAGE;
		slew_output(0.5, GP_OUTPUT_FREQUENCY, newVoltage);
	}

	droopDampTime = droopDampMinutes * 6000;	//convert from minutes to eventTime.

	new_key = 0;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void slave_capacity_setup_screen()	//programmable shore cord capacity via Front Panel, v2.00.
{
  int edit_field=0;
  int i=0;

  int icordRating[10]={30,32,50,60,63,100,125,150,200,250};
  int iCord;
  float capacity;
  float voltageNom;
  float voltageLL;
  float masterCapacity=lastMasterCapacity;
  
  float rated_power=RATED_POWER;
  float rated_amps;

//
//Validate Vin before falling into loop...
//
	if(voltage_in_range(SP1, SP1A_sync, 170.0, 600.0))
	{
		voltageNom=gp_Vab[SP1];
	}
	else
	{
		clear_screen();
		lcd_display("SHORE POWER VOLTAGE OUT OF RANGE!\0",3,1);
		lcd_display("Any key to continue ...                 \0",4,1);
		wait_for_key();
		new_key=0;
		return;
	}

	switch (shoreCordRating)
	{
		case 30:	iCord = 0;	break;
		case 32:	iCord = 1;	break;
		case 50:	iCord = 2;	break;
		case 60:	iCord = 3;	break;
		case 63:	iCord = 4;	break;
		case 100:	iCord = 5;	break;
		case 125:	iCord = 6;	break;
		case 150:	iCord = 7;	break;
		case 200:	iCord = 8;	break;
		case 250:	iCord = 9;	break;
		default:	iCord = 9;	break;
	}

rejected:
	i = 0;

	clear_screen();

	if (CORDMAN_OPTION)
	{
		lcd_display("LOAD SHARING    MASTER CAPACITY:     kVA\0",1,1);
		lcd_display(" More    Less    Forward   Back    Exit \0",4,1);
	}
	else
	{
		lcd_display("SHORE CORD SETUP                        \0",1,1);
		lcd_display(" More    Less                      Exit \0",4,1);
		edit_field=1;
	}

			   //12345678901234567890123456789012345678901234567890
	lcd_display("SLAVE CORD CAPACITY:    Amps ->      kVA\0",2,1);
	lcd_display("VOLTAGE:    Vac, FORM:  \01, Freq.:   Hz\0",3,1);

	if (( 184.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 195.0 )) voltageNom = 190.0;
	else if (( 195.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 204.0 )) voltageNom = 200.0;
	else if (( 204.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 214.0 )) voltageNom = 208.0;
	else if (( 214.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 225.0 )) voltageNom = 220.0;
	else if (( 225.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 235.0 )) voltageNom = 230.0;
	else if (( 235.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 250.0 )) voltageNom = 240.0;
	else if (( 250.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 270.0 )) voltageNom = 260.0;
	else if (( 370.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 390.0 )) voltageNom = 380.0;
	else if (( 390.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 408.0 )) voltageNom = 400.0;
	else if (( 408.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 425.0 )) voltageNom = 415.0;
	else if (( 425.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 455.0 )) voltageNom = 440.0;
	else if (( 470.0 < gp_Vab[SP1] ) && ( gp_Vab[SP1] <= 490.0 )) voltageNom = 480.0;
	else voltageNom = gp_Vab[SP1];

	voltageLL = voltageNom;

	if (gp_state.input1_form==THREE_PHASE)
	{
		voltageNom = voltageNom * ONE_SQRT3;
	}

	pause(250);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		if (gp_state.range_mode_id==LOW_RANGE)
		{
			rated_amps = IN_RATED_AMPS_LO_ONE;
		}
		else
		{
		 	rated_amps = IN_RATED_AMPS_HI_ONE;
		}

		if (gp_state.input1_form==ONE_PHASE)
		{
			rated_amps = rated_amps * 1.5;
		}

		capacity = (gp_state.input1_form * icordRating[iCord] * voltageNom) / 1000.0;

		if (capacity > (gp_state.input1_form * rated_amps * voltageNom / 1000.0))
		{
	 		capacity = gp_state.input1_form * rated_amps * voltageNom / 1000.0;
		}

		if (capacity > rated_power)
		{
			capacity = rated_power;
		}

		if (CORDMAN_OPTION) lcd_display_value(masterCapacity,1,33,"%5.1f\0");

		lcd_display("     ", 2, 33);
		lcd_display_value(capacity,2,33,"%5.1f\0");
		
		lcd_display_value(voltageLL,3,10,"%3.0f\0");	//show default Vprog/Vout.
		lcd_display(itoa(gp_state.input1_form),3,24);	//show default Vprog/Vout.
		lcd_display_value(gp_Freq[SP1],3,35,"%2.0f\0");	//show default Vprog/Vout.
		lcd_display_value(shoreCordRating,2,22,"%3.0f\0");		//edit_field==0
	
		if (edit_field==0)
		{
			put_cursor(1,34);
			pause(10);
		}
		else
		{
			put_cursor(2,22);
			pause(250);
		}

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
						if (edit_field==0)
						{
							if (masterCapacity < RATED_POWER)
								masterCapacity = masterCapacity+.1;
						}
						else
						{
							if (iCord < 9)
							{
								iCord++;
							}
							shoreCordRating = icordRating[iCord];
						}
						break;

			case 20:	//F2:
						if (edit_field==0)
						{
							if (masterCapacity > 5.0) masterCapacity = masterCapacity-.1;
						}
						else
						{
							if (iCord > 0)
							{
								iCord--;
							}
							shoreCordRating = icordRating[iCord];
						}
						break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(250);	
						break;

			case 8:		//F3:
			case 9:		//F4:
						if (CORDMAN_OPTION)
						{
							if (edit_field==0)
							{
								edit_field = 1;	//toggle edit fieldt.
								pause(250);
							}
							else
							{
								edit_field = 0;	//toggle edit fieldt.
								pause(250);
							}
						}
						break;
			default:
						break;
		}

	}

 	lastMasterCapacity = masterCapacity;
	
	slaveLoadShare = (int) (1000.0 * (capacity / masterCapacity));

	while (loadShareTable[i][gp_state.vout_range] > slaveLoadShare)
	{
		i++;
		if (i>99) break;
	}

	if (i<2 || i>98)
	{
		lcd_display("LOAD BALANCE DYNAMIC RANGE EXCEEDED!    \0",3,1);
		lcd_display("Maximum Reenter                    Exit \0",4,1);
		pause(250);
		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
						mach.pwm2_dutyCycle = (unsigned) loadShareTable[98][0];	//max. imbalance.
 						break;

			case 20:	//F2:
						new_key=0;
						goto rejected;

//			case 10:	//F5:
//						new_key = 18;	//force SYSTEM STATUS key press response.
//						display_type = SYSTEM_STATUS; //bail to default display.
//						delay(250);	
//						break;
			default:	break;
		}
	}
	else
	{
		mach.pwm2_dutyCycle = (unsigned) loadShareTable[i][0];
	}

	init_PWM2(mach.pwm2_bits,mach.pwm2_align,mach.pwm2_dutyCycle);		//unsigned(bits,align,duty)

	new_key = 0;

///////////temp code/////////////	
//	clear_screen();
// 	lcd_display("SLAVE DUTY CYCLE = ",1,1);
//	lcd_display_value(mach.pwm2_dutyCycle,1,20,"%3.0f");	//show new duty.
//	lcd_display("loadShareTable INDEX = ",2,1);
//	lcd_display_value(i,2,24,"%3.0f");						//show index.
//	lcd_display("slaveLoadShare = ",3,1);
//	lcd_display_value(slaveLoadShare,3,18,"%3.0f");						//show slaveLoadShare.
//	lcd_display("Any key to continue ...",4,1);
//	wait_for_key();
//	new_key = 0;
///////////temp code/////////////	
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#define MAX_CORD_RATING 250
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void shorepoweralarm_setup_screen()	//extern struct gp_state_struct gp_state;
{
  int edit_field=0;
  int row[3]=	{ 2,  3, 3};
  int column[3]={22, 11,33};
//  int row[3]=	{ 2,  3, 4};
//  int column[3]={22, 11, 8};
  int icordRating[10]={30,32,50,60,63,100,125,150,200,250};
  int iCord;

  char *status[2]={	"DISABLED\0",
			   		"ENABLED \0"};

	switch (shoreCordRating)
	{
		case 30:	iCord = 0;	break;
		case 32:	iCord = 1;	break;
		case 50:	iCord = 2;	break;
		case 60:	iCord = 3;	break;
		case 63:	iCord = 4;	break;
		case 100:	iCord = 5;	break;
		case 125:	iCord = 6;	break;
		case 150:	iCord = 7;	break;
		case 200:	iCord = 8;	break;
		case 250:	iCord = 9;	break;
		default:	iCord = 9;	break;
	}

refresh:

	clear_screen();

//	if (CORDMAN_OPTION)
//	{
		lcd_display("      LOAD MANAGEMENT CONFIGURATION     \0",1,1);
////		lcd_display("LOAD MANAGEMENT\0",1,12);
//	}
//	else
//	{
//		lcd_display("            SHORE CORD SETUP            \0",1,1);
//	}
	lcd_display(" SHORE CORD RATING =     AMPS           \0",2,1);
	lcd_display(" ALARM AT    % OF RATING, ALARM         \0",3,1);
	lcd_display(" Cord    Droop  Transfer  Cursor   Exit \0",4,1);

	lcd_display_value(shoreCordRating,2,22,"%3.0f\0");	//edit_field==0
	lcd_display_value(alarmLevel,3,11,"%3.0f\0");		//edit_field==1
	lcd_display(status[alarmState],3,33);				//edit_field==2

	delay(500); 		//wait a 1/2 sec. for operator to see screen.

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display_value(shoreCordRating,2,22,"%3.0f\0");		//edit_field==0
		lcd_display_value(alarmLevel,3,11,"%3.0f\0");		//edit_field==1
		lcd_display(status[alarmState],3,33);				//edit_field==2

		if (edit_field==2)
		{
			lcd_display("Enable  Disable  Forward   Back    Exit \0",4,1);
		}
//		else if (CORDMAN_OPTION && edit_field==0)
		else if (edit_field==0)
		{
			if (gp_state.local_master==SLAVE || EXT_OSC_OPTION)
				lcd_display(" Cord                     Cursor   Exit \0",4,1);
			else
				lcd_display(" Cord    Droop  Transfer  Cursor   Exit \0",4,1);
		}
		else 
		{
			lcd_display(" More    Less    Forward   Back    Exit \0",4,1);
		}

		put_cursor(row[edit_field],column[edit_field]);

		delay(250);

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
					switch (edit_field)
					{
						case 0:	//cordRating field
//								if (CORDMAN_OPTION)
//								{
									if (gp_state.local_master==SLAVE)
									{
										slave_capacity_setup_screen();
									}
									else
									{
										cord_capacity_setup_screen();
									}
									delay(250);
									new_key = 0;
									goto refresh;
//								}
//								else
//								{
//									if (iCord < 9)
//									{
//										iCord++;
//									}
//									shoreCordRating = icordRating[iCord];
//								} 	
// 								break;

						case 1:	//alarmLevel field
								if (alarmLevel < LmMAX_LEVEL)
								{
									alarmLevel = alarmLevel + 10;
								}
								break;

						case 2:	//gp_state.dsc_alarmState field
								alarmState = ENABLED;
								break;

						default: 
								edit_field=2;
								break;
					}
					break;

			case 20:	//F2:
					switch (edit_field)
					{
						case 0:	//cordRating field
//								if (CORDMAN_OPTION && (gp_state.local_master!=SLAVE))
								if (gp_state.local_master!=SLAVE && !EXT_OSC_OPTION)
								{
									droop_control_screen();
									delay(250);	
									new_key = 0;
									goto refresh;
								}
//								else
//								{
//									if (iCord > 0)
//									{
//										iCord--;
//									}	
//									shoreCordRating = icordRating[iCord];
//								}
								break;

						case 1:	//alarmLevel field
								if (alarmLevel > 50)
								{
									alarmLevel = alarmLevel - 10;
								}
								break;

						case 2:	//gp_state.dsc_alarmState field
								alarmState = DISABLED;
								clear_CORD_ALARM();

								if (newVoltage != GP_OUTPUT_VOLTAGE)	//recover from droop.
								{
									newVoltage = GP_OUTPUT_VOLTAGE;
									slew_output(0.5, GP_OUTPUT_FREQUENCY, newVoltage);
								}
								break;
						default: 
								edit_field=2;
								break;
					}
					break;

			case 8:	//F3:
			case 16:		//SHORE POWER		//backup cursor.
//					if (CORDMAN_OPTION && (edit_field==0) && (gp_state.local_master!=SLAVE))
					if ((edit_field==0) && (gp_state.local_master!=SLAVE) && !EXT_OSC_OPTION)
					{
						auto_transfer_on_overload();
						delay(250);
						new_key = 0;
						goto refresh;
					}
					else if (edit_field>=2)
					{
						edit_field=0;
					}
					else
					{
						edit_field++;	
					}
					break;

			case 9:	//F4:
			case 17:		//CONVERTER POWER	//advance cursor.
					if (edit_field<=0)
					{
						edit_field=2;
					}
					else
					{
						edit_field--;	
					}
					break;

			case 10:	//F5:
					new_key = 18;	//force SYSTEM STATUS key press response.
					display_type = SYSTEM_STATUS; //bail to default display.
					delay(500);	
					break;

			default:
					break;
		}
	}

	new_key = 0;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void auto_transfer_on_overload()	//automatic transfer to CONV on overload.
{
  int edit_field=0;
  int row[2]=	{ 2, 2};
  int column[2]={ 11,36};
  int maxGenset=2;

	if (MULTIGEN_OPTION) maxGenset=4;

	clear_screen();
	lcd_display("      TRANSFER ON OVERLOAD CONTROL      \0",1,1);
	lcd_display(" Feature:         , Select Genset:      \0",2,1);
	lcd_display("Enable  Disable  Forward   Back    Exit \0",4,1);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		if(autoTransferOnOverload)
		{
			lcd_display(" ENABLED\0",2,11);
		}
		else
		{
			lcd_display("DISABLED\0",2,11);
		}

		switch (autoTransferGenset)
		{
			case GEN1:
						lcd_display("GEN#1\0",2,36);
						break;
			case GEN2:
						lcd_display("GEN#2\0",2,36);
						break;
			case GEN3:
						lcd_display("GEN#3\0",2,36);
						break;
			case GEN4:
						lcd_display("GEN#4\0",2,36);
						break;
			default:
						lcd_display("GEN#1\0",2,36);
						break;

		}

		if (edit_field)	//if (edit_field==1)
		{
			lcd_display(" More    Less    Forward   Back    Exit \0",4,1);
		}
		else	//else (edit_field==0)
		{
			lcd_display("Enable  Disable  Forward   Back    Exit \0",4,1);
		}

		put_cursor(row[edit_field],column[edit_field]);
		pause(250);

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
					switch (edit_field)
					{
						case 0:	//
								autoTransferOnOverload = ENABLED;
								break;
						case 1:	//
								if (autoTransferGenset < maxGenset)
								{
									autoTransferGenset++;
								}
								break;
						default: 
								edit_field=0;
								break;
					}
					break;

			case 20:	//F2:
					switch (edit_field)
					{
						case 0:	//
								autoTransferOnOverload = DISABLED;
								break;
						case 1:	//
								if (autoTransferGenset > 1)
								{
									autoTransferGenset--;
								}
								break;
						default: 
								edit_field=0;
								break;
					}
					break;

			case 8:		//F3	//advance cursor.
			case 17:		//CONVERTER POWER	//advance cursor.
					{
						if (edit_field<=0)
						{
							edit_field=1;
						}
						else
						{
							edit_field--;	
						}
						put_cursor(row[edit_field],column[edit_field]);
						delay(250);	
					}
					break;

			case 9:		//F4	//backup cursor.
			case 16:		//SHORE POWER		//backup cursor.
					{
						if (edit_field>=1)
						{
							edit_field=0;
						}
						else
						{
							edit_field++;	
						}
						put_cursor(row[edit_field],column[edit_field]);
						delay(250);	
					}
					break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(250);	
						break;

			default:
					break;
		}
	}
	new_key = 0;
}
/*---------------------------------------------------------------------------*/
void energy_management_setup_screen()
{
	clear_screen();
	lcd_display(" SHORE CORD and LOAD MANAGEMENT CONTROL \0",1,1);
	lcd_display("F1: Shore Cord Alarm                    \0",2,1);
	lcd_display("F2: Load Shed Control                   \0",3,1);
	lcd_display(" Cord    Shed                      Exit \0",4,1);

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
						shorepoweralarm_setup_screen();
						break;

			case 20:	//F2:
						load_management_setup_screen();
						break;
			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(250);	
						break;

			default:
						break;
		}
	new_key = 0;
}
/*--------------------------------------------------------------------------*/
void load_management_setup_screen()
{
  unsigned delayMax=(10000-100);	//10sec maximum hysterysis
  int edit_field=0;
  int row[8]=	{ 2, 2, 3, 3, 2, 2, 3, 3};
  int column[8]={ 7,20, 7,20,12,25,12,25};

	clear_screen();
	lcd_display("LOAD   ON   Ton     OFF  Toff  \0",1,1);
	lcd_display("SHED     %    0ms     %    0ms \0",2,1);
	lcd_display("ADD      %    0ms     %    0ms \0",3,1);
	lcd_display(" More    Less    Cursor   Default  Exit \0",4,1);

	while (new_key!=18)	//exit test when SYSTEM_STATUS is pressed.
	{
		lcd_display_value(LmShedOnLevel,2,7,"%3.0f\0");		//edit_field==1
		lcd_display_value(LmShedOffLevel,2,20,"%3.0f\0");	//edit_field==1
		lcd_display_value(LmAddOnLevel,3,7,"%3.0f\0");		//edit_field==1
		lcd_display_value(LmAddOffLevel,3,20,"%3.0f\0");	//edit_field==1

		lcd_display_value(LmShedOnDelay,2,12,"%4.0f\0");		//edit_field==1
		lcd_display_value(LmShedOffDelay,2,25,"%4.0f\0");	//edit_field==1
		lcd_display_value(LmAddOnDelay,3,12,"%4.0f\0");		//edit_field==1
		lcd_display_value(LmAddOffDelay,3,25,"%4.0f\0");	//edit_field==1


		put_cursor(row[edit_field],column[edit_field]);
		pause(100);

		wait_for_key();

		switch (new_key)
		{
			case 19:	//F1:
					switch (edit_field)
					{
						case 0:	//
								if (LmShedOnLevel < LmMAX_LEVEL)
								{
									LmShedOnLevel++;
								}
								break;
						case 1:	//
								if (LmShedOffLevel < LmShedOnLevel)
								{
									LmShedOffLevel++;
								}
								break;
						case 2:	//
								if (LmAddOnLevel < LmAddOffLevel)
								{
									LmAddOnLevel++;
								}
								break;
						case 3:	//
								if (LmAddOffLevel < LmShedOnLevel)
								{
									LmAddOffLevel++;
								}
								break;
						case 4:	//
								if (LmShedOnDelay < delayMax)
								{
									LmShedOnDelay+=100;
								}
								break;
						case 5:	//
								if (LmShedOffDelay < delayMax)
								{
									LmShedOffDelay+=100;
								}
								break;
						case 6:	//
								if (LmAddOnDelay < delayMax)
								{
									LmAddOnDelay+=100;
								}
								break;
						case 7:	//
								if (LmAddOffDelay < delayMax)
								{
									LmAddOffDelay+=100;
								}
								break;

						default: 
								edit_field=0;
								break;
					}
					break;

			case 20:	//F2:
					switch (edit_field)
					{
						case 0:	//
								if (LmShedOnLevel > (LmShedOffLevel+1.0))
								{
									LmShedOnLevel--;
								}
								break;
						case 1:	//
								if (LmShedOffLevel > (LmAddOnLevel+1.0))
								{
									LmShedOffLevel--;
								}
								break;
						case 2:	//
								if (LmAddOnLevel > 25.0)
								{
									LmAddOnLevel--;
								}
								break;
						case 3:	//
								if (LmAddOffLevel > (LmAddOnLevel+1.0))
								{
									LmAddOffLevel--;
								}
								break;
						case 4:	//
								if (LmShedOnDelay >= 100)
								{
									LmShedOnDelay-=100;
								}
								break;
						case 5:	//
								if (LmShedOffDelay >= 100)
								{
									LmShedOffDelay-=100;
								}
								break;
						case 6:	//
								if (LmAddOnDelay >= 100)
								{
									LmAddOnDelay-=100;
								}
								break;
						case 7:	//
								if (LmAddOffDelay >= 100)
								{
									LmAddOffDelay-=100;
								}
								break;
						default: 
								edit_field=0;
								break;
					}
					break;

			case 8:		//F3	//advance cursor.
			case 17:		//CONVERTER POWER	//advance cursor.
						if (edit_field>=7)
						{
							edit_field=0;
						}
						else
						{
							edit_field++;	
						}
						put_cursor(row[edit_field],column[edit_field]);
						delay(250);	
					break;

			case 9:		//F4	//backup cursor.
			case 16:		//SHORE POWER		//backup cursor.
						LmShedOnLevel	=95.0;	//Load Mgmt defaults
						LmShedOffLevel	=85.0;
						LmAddOffLevel	=80.0;
						LmAddOnLevel	=60.0;
						LmShedOnDelay	=200;
						LmShedOffDelay	=200;
						LmAddOnDelay	=200;
						LmAddOffDelay	=200;
						break;

			case 10:	//F5:
						new_key = 18;	//force SYSTEM STATUS key press response.
						display_type = SYSTEM_STATUS; //bail to default display.
						delay(250);	
						break;

			default:
					break;
		}
	}
	new_key = 0;
}
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

