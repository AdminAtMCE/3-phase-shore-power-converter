/* @(#)COMM.C  */
#include "ac.h"

//EVENT LOG extern
#include "event.h"
extern int LogEvent( unsigned char, unsigned char );
extern volatile unsigned long eventTime;	//unsigned long eventTime in 10ms units.

#define IE_BUF_SIZE 8192
#define IE_PARM_MAXSIZE 32
#define IE_KEYWORD_MAXSIZE 130

#define QUERY (strstr(IE_keyword,"?\0"))

/*=========================================================================*/
//				FUNCTION PROTOTYPES
/*=========================================================================*/

//#ifdef MODBUS
void remoteManager( void );
void KWH_clear( void );
void aux_clear( void );
//void shorKWH_clear( void );
void forceDefaultDisplayMode( void );	//force local display out of interactive mode
//#endif //MODBUS

int parse_CAL_XREF(void);	//return=1=successful, return=0=failure=command_error.
int parse_CAL(void);		//return=1=successful, return=0=failure=command_error.
int process_ext_ref_input(float*,float*,float*);	//return=1=successful, return=0=failure=command_error.
//extern int process_ext_ref_input(float *, float *, float *, float *, float , int);

int raw(void);				//COMM.c iec function
int parse_ROUT(void);

void float2outbufr(float);
void crfloat2outbufr(float);

void set_REMOTE_mode(void);

#ifdef MY_DFT
void myDFT(int, int, float*, float);
int parse_HARMONICS(int);
//int parse_THD(int);
#endif // MY_DFT

/*=========================================================================*/
//				GLOBAL COMM DATA STRUCTURES
/*=========================================================================*/
/* ========================================================================================================= */
/* ======================================================================================= PERSISTENT MEMORY */
#pragma noclear
/* ========================================================================================================= */
#pragma align hb=c 								// ALIGNMENT: PEC ADDRESSABLE
/* ========================================================================================================= */
/* ========================================================================================================= */

//#pragma combine fb=A0x7F000
volatile unsigned far int IE_stat0;	/* tms9914 Interrupt status 0 */
#define IE_mask0 IE_stat0 		/* tms9914 Interrupt mask 0 */
//#pragma combine fb=A0x7F002
volatile unsigned far int IE_stat1;	/* tms9914 Interrupt status 1 */
#define	IE_mask1 IE_stat1 		/* tms9914 Interrupt mask 1 */
//#pragma combine fb=A0x7F004
volatile unsigned far int IE_adstat;	/* tms9914 address status 0 */
//#pragma combine fb=A0x7F006
volatile unsigned far int IE_bustat;	/* tms9914 bus status 0 */
#define	IE_auxcmd IE_bustat 		/* tms9914 aux cmd reg */
//#pragma combine fb=A0x7F008
volatile unsigned far int IE_addreg;	/* address reg */
//#pragma combine fb=A0x7F00A
volatile unsigned far int IE_serpol;	/* serial poll reg */
//#pragma combine fb=A0x7F00E
volatile unsigned far int IE_datain;	/* data in reg */
#define	IE_dataout IE_datain		/* data out reg */

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#pragma default_attributes
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

volatile unsigned immediate;
unsigned remote_genset;
unsigned remoteEvLogMode;

float value;						/* initial field value */

unsigned char IE_addstatus;		/* holds address status from IEEE port. */
int IFC_flag;				/* Flag indicates IFC rcvd by GPIB. Application must reset it. */
volatile unsigned char IE_byte_in;		/* holds rcvd data byte from IEEE port. */
volatile unsigned int IE_inptr;			/* pointer to next empty byte in buffer, */
struct IE_STATUS STB;			/* Status Byte structure */
struct IE_ESR ESR;			/* ERROR & STATUS register define Status Byte structure */
unsigned char ESE;			/* std Event Status Enable reg. */
int LLO;				/* Local Lockout mode flag. */
volatile unsigned int eom_flag;			/* indicates eom or eoi rcvd. */
unsigned int IE_outptr;			/* point to next byte to send out. */
unsigned int IE_out_count;		/* tracks # of bytes to send. */
char IE_keyword[IE_KEYWORD_MAXSIZE];	/* holds extracted IEEE-488.2 or SCPI cmd or query pgm header. */

unsigned char auxcmd_bustat;
unsigned char address_register;
unsigned char serial_poll;
unsigned char IEEE_dataout;

unsigned char IE_status0;		/* holds status0 byte from IEEE port. */
unsigned char IE_status1;		/* holds status1 byte from IEEE port. */
unsigned char IE_status0_iec;		/* holds status0 byte from IEEE port before iec_main() corrupts register. */
unsigned char IE_status1_iec;		/* holds status1 byte from IEEE port before iec_main() corrupts register. */
unsigned int REM;			/* flag indicates status of REM bit. */
volatile char far IE_inbufr[IE_BUF_SIZE];	/* buffer for rcving data from IEEE port */
char far IE_outbufr[IE_BUF_SIZE];	/* buffer for sending data out IEEE port */
char far SYSTEM_ERROR[256];		/* holds SCPI SYST:ERR error/event # and desc. */
char far ERROR_DESC[160];		/* holds SCPI SYST:ERR detail */
unsigned char SRE;			/* Service Request Enable reg. */


unsigned int STAT_OPER_EVENT;		/* scpi :STATus:OPER:EVENt? reg (old) */
unsigned int STAT_OPER_ENABLE;		/* scpi :STATus:OPER:ENABle reg */

unsigned int sobg, sotbuf;		/* for debugging */

unsigned int STAT_QUES_EVENT;		/* scpi :STATus:QUEStionable:EVENt? reg */
unsigned int STAT_QUES_ENABLE;		/* scpi :STATus:QUEStionable:ENABle reg */


//char far IE_parm_name[IE_PARM_MAXSIZE];	// holds extracted SCPI param name. (char pgm data).
//char far IE_parm_data[IE_PARM_MAXSIZE];	// holds extracted SCPI param data (char or numeric pgm data).
char IE_parm_name[IE_PARM_MAXSIZE];	// holds extracted SCPI param name. (char pgm data).
char IE_parm_data[IE_PARM_MAXSIZE];	// holds extracted SCPI param data (char or numeric pgm data).


					/* small quantities only (in ascii form). */
float IE_parm_value;			/* holds extracted SCPI param value(s) (char or numeric pgm data), */
					/* small quantities only (converted to numeric). */
char far IE_cvt_bufr[IE_PARM_MAXSIZE];	/* buffer for converting values to strings. */
int OCAS;				/* Oper complete Command active state, inverse of OCIS. */
int OQAS;				/* Oper complete Query Command active state, inverse of OQIS. */
unsigned far char DDT[256];		/* holds defined trigger cmds. */
int TRG_mode;				/* indicates trigger mode. */
struct stat_oper_event_temp STAT_OPER_EVENT_temp;
struct scpi_status_oper_cond STAT_OPER_COND;
struct scpi_status_ques STAT_QUES_COND;	/* scpi :STATus:QUEStionable:CONDition? reg */
int scpi_parser_reset;			/* says if parser was reset for this pgm msg. */
unsigned char IE_byte_out;		/* holds data byte to be sent to IEEE port. */

int (*SCPI_hdr_path)(void);		/* pointer to header path (root function) set function to default) */

int IE_wf_selected;			/* IE WF operation flag. */

char far parm_buf[15];                      //char buffer

char far parm_name_buf[15];                      //char buffer

char huge *pm=parm_name_buf;


/*=========================================================================*/
//				EXTERN DATA STRUCTURES AND FUNCTIONS
/*=========================================================================*/
#include "log.h"		//log types.
extern void marshalEvent( eventType * );
extern void initializeEventLog( void );
extern void startEventLog( void );
extern unsigned eventLogCount( void );
extern char *getXfrErrCodeDescriptor( unsigned char );
extern char *getErrOnCodeDescriptor( unsigned char );
//EventView
extern eventType *viewFirstEvent( void );
extern eventType *viewLastEvent( void );
extern eventType *viewNextEvent( eventType * );
extern eventType *viewPreviousEvent( eventType * );
extern eventType *remoteEventLogView;
extern unsigned remoteActive;
extern unsigned maintenanceMode;
//TECHNEL_OPTION
extern unsigned generator_on_delay;
extern unsigned gen_start_cmd_enable;
extern unsigned inhibit_transfer;
//END TECHNEL_OPTION
//NEW_G2G_OPTION
extern unsigned generator_off_delay;
extern unsigned gen_stop_cmd_enable;
//END NEW_G2G_OPTION
extern unsigned GP_CONTROL_WORD;	//image in RAM
extern volatile unsigned far int gp_control_word;			//CS3B(low) GP CONTROL WORD.
extern volatile unsigned far int gp_status_word;		// GP STATUS WORD1.
extern volatile unsigned far int gp_status_word2;		// GP STATUS WORD2.
extern volatile unsigned far int gp_status_word3;		// GP STATUS WORD3.
extern volatile unsigned far int gp_configuration_word;	// GP CONFIGURATION WORD.
extern volatile unsigned far int gp_configuration_word2;	// GP CONFIGURATION WORD.
extern unsigned VIRTUALconfiguration_word;	// VIRTUAL CONFIGURATION WORD.
extern unsigned VIRTUALconfiguration_word2;	// VIRTUAL CONFIGURATION WORD.
extern void makeBit( unsigned * ,unsigned, unsigned );	//word bit to value.	v2.47
extern int check_gp_status(void);
extern int set_sysstat(unsigned);
extern volatile unsigned abort_power_on;
extern void init_outputs(void); 			// set outputs to nominal.
extern unsigned DUAL_INPUT;
extern unsigned PARALLELING;
extern unsigned MULTIGEN_OPTION;	//S3-5
extern unsigned SEAMLESS_TRANSFER;
extern float IN_RATED_AMPS_LO_ONE;
extern float IN_RATED_AMPS_HI_ONE;
extern float RATED_POWER;
extern unsigned CORDMAN_OPTION;	//S3-7
extern unsigned EXT_CB_OPTION;
extern void set_EXT_CB_OPEN(void);			//toggle "set_EXT_CB_OPEN" signal high then low.

extern unsigned transferMode;		
extern unsigned NEW_G2G_OPTION;	//S3-8
extern unsigned SWITCHGEAR_INTERFACE_OPTION;	//S2-8
extern unsigned MULTIGEN_BESECKE;
extern unsigned GEN_AMPS_PRESENT;	//S4-6  v2.32, or better
extern int set_meter_mux(unsigned, unsigned);
extern volatile unsigned transfer_in_progress;
extern unsigned g2c;
extern unsigned c2g;
extern volatile unsigned remote_off_flag;
// from GP_LCD module
extern void lcd_display(char *, int, int);
extern void clear_screen(void);
//extern void meas_all(void);			/* calc new meter values. */

extern float gp_Van_DC[8], gp_Vbn_DC[8], gp_Vcn_DC[8];	/* RMS metered output value */
extern float gp_Van[8], gp_Vbn[8], gp_Vcn[8];			/* RMS metered output value */
extern float gp_Vab[8], gp_Vbc[8], gp_Vca[8];			/* RMS metered output value */
extern float gp_Ia_rms[8], gp_Ib_rms[8], gp_Ic_rms[8];	/* RMS metered output value */
extern float gp_Ia_pk[8], gp_Ib_pk[8], gp_Ic_pk[8];		/* peak currents */
extern float gp_KWa[8], gp_KWb[8], gp_KWc[8];			/* metered output value */
extern float gp_KVAa[8], gp_KVAb[8], gp_KVAc[8];	/* calculated output value */
extern float gp_PFa[8], gp_PFb[8], gp_PFc[8];			/* calculated output value */
extern float gp_ICFa[8], gp_ICFb[8], gp_ICFc[8];	/* calculated output value */
extern float gp_Freq[8];							  	/* calculated frequencye */
extern float gp_level_a[8], gp_level_b[8], gp_level_c[8]; /* RMS metered output value */
extern float system_level;						//highest level of input/output A/B/C.
extern float system_kva;						//highest load of input/output A/B/C.
extern float system_kw;								//highest power of input/output A/B/C.
extern float wattHours;
extern volatile unsigned long lastWattTime;	//unsigned long wattTime in 10ms units.
extern volatile unsigned long wattTime;	//unsigned long wattTime in 10ms units.
/////////////////////////////////
// SIX - new global data
extern int gp_OverRange[8][8];			/* Metering overrange indicators. */
extern float system_temperature;
extern float positive_hvdc;
extern float negative_hvdc;
extern char *VER;
extern char *ship_name;	
extern int gp_mode;
///////////////////////////////

////////////////////////////////////////////
extern int huge meter_data[METER_ARRAY];			/* raw ADC metering data */
extern float Vint_volts, Vext_volts;				/* volts to volts ratio */
extern int AGC_state;						/* 0=disabled, 1=Enabled */

extern struct gp_state_struct gp_state;
extern struct mach_struct mach;
//extern struct queueType command,*command_queue;
//extern struct queueType exception,*exception_queue;

#ifdef CALIBRATION_RANGE_CHECK
extern float MIN_KFACTOR;
extern float MAX_KFACTOR;
#endif // CALIBRATION_RANGE_CHECK

extern int shoreCordRating;						// {30 to 250 amps}.
extern int alarmLevel;								// {50to100% of dsc_cordRating}.
extern unsigned alarmState;						// {DISABLED,ENABLED}.
extern int droop;
extern int droopEnable;
extern unsigned long droopDampTime;

extern int slaveLoadShare;
extern int loadShareTable[100][3];	//dutyCycle,percentOfLoad(LOW_RANGE),percentOfLoad(HIGH_RANGE)
extern float lastMasterCapacity;
extern unsigned autoTransferOnOverload;
extern unsigned autoTransferGenset;

extern char *gp_cid;
extern unsigned DELTA_OUTPUT;

/*=========================================================================*/
//				EXTERN DATA STRUCTURES
/*=========================================================================*/

extern void scale_volts(void); 				// scale volts to load DACs.
extern void load_osc_regs(void);
extern int slew_output(float, float, float);		//create and execute transient for output transistion.
extern float GP_OUTPUT_VOLTAGE;
extern float GP_OUTPUT_FREQUENCY;
extern float newSlewRate,newFrequency,newVoltage;	//used with slew_output() for load management.
extern float tempSlewRate,tempFrequency,tempVoltage;	//used with slew_output() for load management.
extern unsigned GP_OUTPUT_FORM;


#ifdef IEEE_ERROR
extern int IEEE_DEBUG;
#endif // IEEE_ERROR

extern unsigned char IE_dev_address;	/* 0-30, device address in BB RAM. */
extern int INTERFACE;			/* 0=IEEE-488, 1=RS-232, 2=NONE */
extern long int ser_baud_rate;		/* Baud rate */
extern int ser_parity;			/* Parity */
extern int eos_mode;			/* XMTD eos: 1=LF, 0=CR/LF, 2=CR. */

extern int Form,Coupling;
extern int no_PAs;						/* number of power amps in parallel in 1 phase mode */
extern int bc_osc;			/* flag states if BC osc PCB assy installed */
//extern int display_no;			/* meter display #, neg value == err display. */
extern char *str;			/* string to be output */
//extern char MOD[8];	// MOD # APPENDED TO *IDN? query reply.
//extern char mod_name[8];				//RESERVE 7 CHARS
extern char *MOD;
extern int trans_mode;
//extern int REMOTE_mode;			/* IEEE-488 bus mode */
extern int LLO_state;			//flag to bypass logic in main() loop when in LLO.
extern char* IDN;
extern int pgm_edit_no;
extern int orig_pgm_no;
//extern int ledA;
//extern int ledB;
//extern int ledC;
//extern int ledF;
extern int ALC_mode;						/* ALC off, */
//extern int Remote_sense;					/* Int sense. */
extern struct user_pgm_structure far pgm_edit;
extern struct user_pgm_structure far def_pgm;
extern struct user_pgm_structure far user_pgm[MAX_PGM+5];			/* declare user programs */
extern struct transient_seg_structure Trans_seg[TOTAL_SEGS+1];	/* segment in edit. */
extern float F_min;						/* s/be 15 hz.	DO NOT set to ZERO! Must be => 10 for make_step_data(). */
extern float F_max;						/* s/be 1200 hz. DO NOT set > 1200 */
//extern int F_span;
extern float F_span;
extern float V_max;
extern float V_min;
extern int active_pgm_no;					/* set manual mode true. */
extern int exec_flag;						/* flag says pgm executed if true. */
extern int trans_changed_flag;					/* says current trans pgm was edited. */
extern struct transient_seg_structure seg_edit[MAX_SEGS+1];	/* segment in edit. */
//extern unsigned char pgm_seg_table[MAX_SEGS+1];			/* temp list of i_segments for THIS pgm. */
extern unsigned int pgm_seg_table[MAX_SEGS+1];			/* temp list of i_segments for THIS pgm. */
extern int last_seg;
extern int seg_no;						/* user pgm segment #, 1 of no_segs. */
extern iram char cvtbufr[161];					/* buffer for sprintf conversions (== 1 line) */
extern unsigned int huge user_wf[WF_ARRAY];			/* user waveforms */
extern unsigned int WF_edit[MAX_WF_STEPS];			/* declare WF edit area */
extern int wf_no;						/* default for WF edit */
//extern float value;						/* initial field value */
extern system unsigned int wf_steps;				/* number of steps in one waveform */
//extern int Output_ONOFF;					/* on/off enable flag */
//extern char key, ikey;						/* keystroke buffer */
//extern int key_count;			/* # of keys entered for numeric input. */
extern float ILMr;						/* IL range */

extern void meas_all(void);			/* calc new meter values. */

extern unsigned daq_mux_id;

extern float Va_out, Vb_out, Vc_out;		/* dynamic output voltage (modified by wfK, XFMR_ratio, AUTO-CAL) */
extern float amps_volts;			/* sets ammeter scaling & ILM range */
extern float amps_volts_display;		// cts_on_primary fix 12-04-95,dave

//#ifndef MOD5278
extern float Kmeter_I_A2, Kmeter_I_A3;
//#endif //~MOD5278
extern float meter_Vint_A, meter_Vint_B, meter_Vint_C;		// Externally referenced voltage entries.
extern float meter_Vext_A, meter_Vext_B, meter_Vext_C;
extern float meter_I_A, meter_I_B, meter_I_C;

extern float Ka_wf, Kb_wf, Kc_wf;		//holds SINErms/WFrms ratio of active waveform.
extern float Kout_A, Kout_B, Kout_C;	// correct Vout using calibration Kfactor.
extern float Kout_AB;				//split phase metering calibration bug fix, 3-4-96, dave
extern float Kout_1;				//single phase metering calibration bug fix, 3-4-96, dave
extern float Kmeter_Vint_A, Kmeter_Vint_B, Kmeter_Vint_C;	// Correct Vmetering based on EXT ref cal.
extern float Kmeter_Vext_A, Kmeter_Vext_B, Kmeter_Vext_C;
extern float Kmeter_I_A, Kmeter_I_B, Kmeter_I_C;
extern float Kmeter_I_A2;
extern float Kmeter_I_A3;

extern float Ia_pk;		/* metered output value */
extern float Ib_pk;		/* metered output value */
extern float Ic_pk;		/* metered output value */
extern float Van;		/* RMS metered output value also 1/2 phase Vrms. */
extern float Vbn;		/* RMS metered output value */
extern float Vcn;		/* RMS metered output value */
extern float Vab;		/* RMS metered output value // v A-B, L-L */
extern float Vbc;		/* RMS metered output value // v B-C, L-L */
extern float Vca;		/* RMS metered output value // v C-A, L-L */
extern float Ia_rms;		/* metered output value */
extern float Ib_rms;		/* metered output value */
extern float Ic_rms;		/* metered output value */
extern float PFa;		/* calculated output value */
extern float PFb;		/* calculated output value */
extern float PFc;		/* calculated output value */
extern float ICFa;		/* calculated output value (Iload Crest Factor) */
extern float ICFb;		/* calculated output value (Ipk/Irms) */
extern float ICFc;		/* calculated output value */
extern float KWa;		/* metered output value */
extern float KWb;		/* metered output value */
extern float KWc;		/* metered output value */
extern float KVAa;		/* calculated output value //KVA & PF calc'd later. */
extern float KVAb;		/* calculated output value */
extern float KVAc;		/* calculated output value */

extern float huge waveform[8][6][METER_DATA_BUF_SIZE];	//6sources,9channels,<1024samples
extern int waveform_sample_count[8][6];
extern unsigned adcRate;	//ADCON ADCTC & ADSTC

//float fx[FFT_BUFR_SIZE];		/* imaginary data array declaration */

extern system unsigned int adc_samples;	/* number of samples in metering one waveform */
extern float XFMR_ratio;		/* XFMR ratio of XFMR installed. Holds value of dip switch */
extern char bat_ok[];			/* battery backed up ram test string */

extern volatile unsigned special_function;
extern volatile int combo_key;

extern int Setup_mode;		/* memory for menu selection */
extern int fsamples;		/* # of samples for FFT work. */
extern int OverRange[8];	/* Metering overrange indicators. */
extern int RAM_reset;           //flag to indicate RAM has been reset.
extern volatile int display_type;
extern volatile unsigned new_key;
extern volatile unsigned Lock;
extern volatile unsigned input_dropped;
extern unsigned autorestart;

#ifdef NO_OPTCON_PCB
extern volatile unsigned far int optcon_control;		// OPCON CONTROL WORD--DUMMY.
extern volatile unsigned far int optcon_control_2;		// OPCON CONTROL WORD 2.
#else												//CS3B(low)
extern volatile system unsigned int optcon_control;		//CS2*Y7*(low) OPCON CONTROL WORD.
extern volatile system unsigned int optcon_control_2;		//CS2*Y7*(low) OPCON CONTROL WORD 2.
#endif

extern unsigned char last_transfer_status;
extern int active_status_code;
extern int last_failure_code;

extern float maxSystemLevel;
extern float maxSystemPower;
extern float averSystemLevel;
extern float averSystemPower;

extern float maxLevelIn1;
extern float maxPowerIn1;

//CONVERTER INPUT
extern float minShorVolts;
extern float maxShorVolts;
extern float minShorAmps;
extern float maxShorAmps;
extern float minShorFreq;
extern float maxShorFreq;
//CONVERTER OUTPUT
extern float minConvVolts;
extern float maxConvVolts;
extern float minConvAmps;
extern float maxConvAmps;
extern float minConvFreq;
extern float maxConvFreq;
extern float maxConvLevel;
extern float maxConvPower;
//GENERATOR #1
extern float minGen1Volts;
extern float maxGen1Volts;
extern float minGen1Freq;
extern float maxGen1Freq;
//GENERATOR #2
extern float minGen2Volts;
extern float maxGen2Volts;
extern float minGen2Freq;
extern float maxGen2Freq;

extern volatile unsigned ts;	//transfer via serial request.
extern volatile unsigned shutoffId;
extern char *shutoffStr[15];
extern unsigned monitorTime;

//#ifdef MODBUS
extern void modbus_init ( void );
extern void idle_modbus( void );
extern void isr_Modbus_RxD( void );			// if receive interrupt request flag then RxD a byte.
extern void isr_Modbus_Error( void );
extern unsigned modbusActive;
extern unsigned char modbusNodeId;
extern unsigned mbCommand;
extern void refreshModbusData( void );
extern unsigned getPowerFactor(void);

//#endif //MODBUS

extern unsigned TIE_CONF_PRESENT;		//S3-7
extern unsigned autoShutdownEnable;		//M/Y BOUNTY HUNTER feature

extern float Debug_Var[];


/*=========================================================================*/
//						COMM.C INLINE FUNCTION DEFINITIONS
/*=========================================================================*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
_inline unsigned valueInRange(float val, float vmin, float vmax)
{
	return (vmin<=val && val<=vmax);
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
_inline unsigned eGen1_CLOSED()
{
	return  ((optcon_status_2 & 0x0100) == 0x0100);	//GENERATOR N1 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned eGen2_CLOSED()
{
	return  ((optcon_status_2 & 0x0200) == 0x0200);	//GENERATOR N2 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned eGen3_CLOSED()
{
#ifdef TEST_FIXTURE
	return  ((gp_configuration_word & 0x4000)!=0);	//FOR DEBUG ONLY!!
#else
	return  ((optcon_status_2 & 0x0400) == 0x0400);	//GENERATOR FORE Confirmation signal.
#endif //TEST_FIXTURE
}
/*---------------------------------------------------------------------------*/
_inline unsigned eGen4_CLOSED()
{
	return  ((optcon_status_2 & 0x0800) == 0x0800);	//GENERATOR SPARE Confirmation signal.
}
/*---------------------------------------------------------------------------*/
_inline unsigned tieBreaker_CLOSED()
{
	if (TIE_CONF_PRESENT)
		return (_getbit(P5,9));					//TIE AUX Confirmation signal.
	else
		return  ((optcon_status_2 & 4) == 4);	//CB3 Confirmation signal.
}
/*---------------------------------------------------------------------------*/
/*=========================================================================*/
//						COMM.C INTERRUPTS
/*=========================================================================*/
/* ------------------------------------------------------------------------- */
/* S0RIR Receive Interrupt Request it's been set high */
interrupt(0x2B) using(SER_RB) void SERIAL()
{
	if (modbusActive)
	{
		isr_Modbus_RxD();
	}
	else
	{
		isr_SERIAL_1();
	}
}
/* -------------------------------------------------------------------------- */
/* S0EIR Error Interrupt Request it's been set high */
interrupt(0x2C) using(SER_RB) void SERIAL_ERROR()
{
	if (modbusActive)
	{
		isr_Modbus_Error();
	}
	else
	{
		isr_SERIAL_0();
	}
}
/* -------------------------------------------------------------------------- */


/*=========================================================================*/
//						COMM.C FUNCTIONS
/*=========================================================================*/
/* -------------------------------------------------------------------------------------------------------*/
void isr_SERIAL_0()			/* if S0EIR error interrupt request flag */
{
//int i;

	ESR.cme=0;			/* Clr cmd error flag. */
	ERROR_DESC[0]='\0';

	if (_getbit(S0CON,8)==1)	/* if PARITY err, */
	{
		ESR.cme=1;		/* flag cmd error: */
		strcat(ERROR_DESC,";RS-232 PARITY ERROR;\0");	/* point to error msg. */
		_putbit(0,S0CON,8);	/* clr parity error flag */
	}

	if (_getbit(S0CON,9)==1)	/* if FRAMING err, */
	{
		ESR.cme=1;		/* flag cmd error: */
		strcat(ERROR_DESC,";RS-232 FRAMING ERROR;\0");	/* point to error msg. */
		_putbit(0,S0CON,9);	/* clr frame error flag */
	}

	if (_getbit(S0CON,10)==1)	/* if OVERRUN err, */
	{
		ESR.cme=1;		/* flag cmd error: */
		strcat(ERROR_DESC,";RS-232 OVERRUN ERROR;\0");	/* point to error msg. */
		_putbit(0,S0CON,10);	/* clr overrun error flag */
	}

	if (ESR.cme)
	{
		IE_esr_report();	/* report Event Status. */
		IE_inptr=0;				/* reset input bufr. */
		IE_out_count=0;		/* clr output bufr counter and pointer. */
		IE_outptr=0;
		IE_status0=0;		/* Also clr status & mav to stop XMTR. */
		STB.mav=0;
	}
	_putbit(0,S0EIC,7);		/* clr error interrupt request flag */
}

/* ------------------------------------------------------------------------- */
void isr_SERIAL_1()				// if receive intrrupt request flag
{
	if (IE_inptr == sizeof(IE_inbufr)-1)	// see if inbufr is full.
	{
		IE_inptr=0;			// bufr full, reset bufr ptr so caller can see how many bytes rcvd (0 if [full] error)
		ESR.cme=1;			// flag cmd error: DEADLOCK!
		strcpy(ERROR_DESC,"Input buffer full\0");	// point to error msg.
		IE_esr_report();							// report Event Status.
	}

	IE_byte_in=S0RBUF;			// get the contents of the serial receive buffer S0RBUF

	if (IE_byte_in==0x0D) IE_byte_in=0x0A;	// make CR (ENTER) into NL
	IE_inbufr[IE_inptr]=IE_byte_in;			// add to buffer
	IE_inptr++;

	if (IE_byte_in==0x0A) 			// if NL rcvd,
	{
		IE_inbufr[IE_inptr]=NULL;	// terminate string w/null (after NL).
		IE_inptr++;					// inc bufr pointer
		eom_flag=1;					// indicate eom rcvd.
									// Don't Allow more data to be rcvd till parsed.
	}

	if (IE_byte_in==(unsigned char)modbusNodeId) 		// if Modbus Node Id rcvd,
	{
		if (IE_byte_in!=0x0A)
		{
			modbusActive=1;
			immediate=0;
		}
	}

	if (immediate) 			// if immediate processing flagged ('!').
	{
		if (IE_byte_in==126)	//sp OFF.
		{
			IE_inptr--;					//remove '~' from inbufr.
			input_dropped=FALSE;			//v1.97.
			autorestart=DISABLED;
			mbCommand=1;			//use command interpreter
			LogEvent(Ev_COMM_EPO,0);
			shutoffId = 3;				//reason input is off, EPO.
		}

		immediate=0;
		_putbit(1,S0RIC,6);			// enable serial interrupt for immediate byte.
	}

	if (IE_byte_in==0x21) 			// if '!' rcvd.
	{
		IE_inptr--;					//remove '!' from inbufr.
		immediate=1;
		_putbit(1,S0RIC,6);			// enable serial interrupt for immediate byte.
	}

}
/* ------------------------------------------------------------------------- */
//////////////////////////////////
//	Set baud rate, parity, etc. 
void	set_serial_parms()			
{
unsigned int ctrl;

	ctrl=0;
											/* Build Control Register S0CON byte. */
	switch (ser_parity)
	{
		case 0: 	ctrl =0x80D1;	break;	/* NO parity */
		case 1:		ctrl =0x90F7;	break;	/* ODD parity */
		case 2:		ctrl =0x80F7;	break;	/* EVEN parity */
		default: 	ctrl =0x80D1;	break;	/* NO parity */
	}

#ifdef ASC0
	S0CON = ctrl;	/* loads the serial channel control register S0CON */
#endif
					/* with the selected parity */
					/* Use only 8 bits, 1 stop. */
	switch (ser_baud_rate)
	{                                     /* calculation of the reload value done */
		case 38400:	ctrl = 0x000F;	break;	/* by the formula */
		case 19200: ctrl = 0x001F;	break;	/* S0BRL=(CPU clock/(32*Baud Rate))-1 */
		case 9600: 	ctrl = 0x0040;	break;	/* @ CPU clock of 20 MHz. */
		case 4800: 	ctrl = 0x0081;	break;
		case 2400: 	ctrl = 0x0103;	break;
		case 1200: 	ctrl = 0x0207;	break;
		case 300: 	ctrl = 0x0822;	break;
		default: 		ctrl = 0x001F;	break;	/* 19200 */
	}
	sobg = ctrl;			/* for debugging */
#ifdef ASC0
	S0BG = sobg;			/* Baud Rate register S0BG */
#endif							/* of the Serial Channel ASC0 in Asynchronous op. */
	if(ctrl==0x001F) ser_baud_rate=19200;		// the default baud rate is shown on the display 

}


/* ------------------------------------------------------------------------- */
/* 11 */
void IE_init()
{

unsigned char a;

	if (gp_state.sysstat!=OK)
		set_sysstat(OK);	//clear alarm, forces SUMMARY DISPLAY

#ifdef ASC0
	if(INTERFACE==1)			/* if serial init */
	{

#ifdef STARTUP_MSGs
//	clear_screen();
	lcd_display("INITIALIZING SERIAL COMMUNICATIONS...\0",1,1);
	delay(100);
#endif //STARTUP_MSGs

		_bfld(DP3,0X0C00,0X0400);	/* direction bits */
		_putbit(1,P3,10);		/* enable TXD0 output */
		S0TIC=0;			/* clear Transmit Interrupt Control register */
		S0RIC=0;			/* clear Receive Interrupt Control register */
		S0EIC=0;			/* clear Error Interrupt Control Register */
		set_serial_parms();		/* set Baud Rate, Parity check, etc. */
//		S0EIC=0x006B;			/* S0EIE=1,ILVL=1010b,GLVL=0011b */
//		S0RIC=0x006A;			/* S0RIE=1,ILVL=1010b,GLVL=0010b */
		immediate=0;	//clear immediate command flag.

		IE_inptr=0;				/* reset input bufr. */
		IE_out_count=0;		/* clr output bufr counter and pointer. */
		IE_outptr=0;
		IE_status0=0;		/* Also clr status & mav to stop XMTR. */
		STB.mav=0;
		IE_inbufr[IE_inptr]=NULL;		/* terminate string w/null (after NL). */
		IE_outbufr[IE_outptr]=NULL;		/* terminate string w/null (after NL). */
//#ifdef MODBUS
		modbus_init();
//#endif //MODBUS
		S0EIC=0x006B;			/* S0EIE=1,ILVL=1010b,GLVL=0011b */
		S0RIC=0x006A;			/* S0RIE=1,ILVL=1010b,GLVL=0010b */
	}
#endif
	if(INTERFACE==0)			/* Handle 9914. */
	{
#ifdef STARTUP_MSGs
//	clear_screen();
	lcd_display("INITIALIZING GPIB COMMUNICATIONS...\0",1,1);
	delay(100);
#endif //STARTUP_MSGs

		IE_auxcmd=0x80;			/* do SOFTWARE RESET */
		delay(1);
		a=IE_dev_address;		/* get Device address */
		a &= 0x1F;			/* MASK UPPER THREE BITS */
		if (a>=31) a=30;		/* limit device address to 0-30 per IEEE-488 std. */
		IE_addreg=a;			/* WRITE TO ADDRESS REGISTER */
		IE_mask0=0;			/* mask all interrupt bits since RESET does not clr'em */
		IE_mask1=0;			/* do both mask regs. */

#if (GPIB_CLK_FREQ==20)
// ######################################################################################################################################
// ########################### NAT9914APD 20MHz operation setup #################### start ##############################################
		IE_auxcmd=0x99;			/* switch to uPD7210 mode to setup MICR register: GPIB configuration for 20MHz clock. */
		delay(1);
		IE_serpol=0x50;			/* AUXMR page-in register auxiliary command.  7210 mode, offset 5 AUXMR. */
		delay(1);
		IE_auxcmd=0x81;			/* set MICR in ICR2.  7210 mode, page-in state, offset 3 ICR2. */
		delay(1);
		IE_serpol=0x2A;			/* write to AUXMR (actually ICR because of first 4 bits) to specify 20MHz clock. */
		delay(1);
		IE_serpol=0x15;			/* switch out of uPD7210 mode and back to 9914 mode. */
		delay(1);
//		IE_auxcmd=0x95;			// std1		short T1 delay
		IE_auxcmd=0x97;			// vstd1	very short T1 delay
		delay(1);
// ######################################################################################################################################
// ########################### NAT9914APD 20MHz operation setup #################### end ################################################
#endif //GPIB_CLK_FREQ == 20 MHz

		IE_auxcmd=0x93;			/* Disable All Interrupts */
		delay(1);
						/* 9914 registers still indicate real status */
						/* RESET SRQ byte */
		IE_serpol=0;			/* so no more errors are seen by host. */
						/* set NBAF, New Byte Available False */
		IE_auxcmd=0x5;			/* so trash in Dataout reg won't be sent */
		delay(1);
		IE_auxcmd=0x0;			/* undo SOFTWARE RESET */
		delay(1);
	}
}
/* ------------------------------------------------------------------------- */
/* 12 */
			/* void IE_ifc() */

/* ------------------------------------------------------------------------------- */
/* 13 */
void set_REMOTE_mode()
{					/* since controlling cmd rcvd, */
//	REMOTE_mode=1;			/* set REMOTE mode on */
//	ledA=0;				/* init these LEDs OFF for REMOTE CONTROL mode */
//	ledB=0;
//	ledC=0;
//	ledF=0;
//	update_LEDs();
}

/* ------------------------------------------------------------------------------------- */
/* 14 */
void IE_load_serpoll()			/* Load status in to serial poll reg. */
{					/* set SRQ if mss true. */
					/* Last calc MSS bit of STB. */
unsigned char *addr = &STB.tag-1;	/* get address of STB bit struct */
char stb = *addr;			/* get 8 bit byte of condition. */
char mss = (stb&0xBF) & (SRE&0xBF);	/* AND status w/enable bits */

	if (mss)
		STB.mss =1;		/* set mss bit of STB if status not 0 */
	else
		STB.mss =0;
	stb = *addr;			/* get updated Status Byte. */

	if (INTERFACE==0)		/* if not serial, */
		IE_serpol=stb & 0xBF;	/* outp(IE_serpol,stb & 0xBF);*/
					/* load Serial Poll reg w/status byte, */
					/* but first strip bit 6 so rsv1 not generated. */
}

/* -------------------------------------------------------------------------------------- */
/* 15 */
void IE_Service_Request()		/* Set status byte and do RQS if MSS true. */
{
	IE_load_serpoll();		/* Load Serial Poll reg */
					/* Do IEEE-488.2 RQS function if mss true. */
	if (STB.mss)			/* if service requested, */
	{
		if (INTERFACE==0)	/* if not serial, */
			IE_auxcmd=0x98;	/* set rsv2 (RQS) function. */
	}
}

/* ------------------------------------------------------------------------------- */
/* 16 */
void	scpi_STAT_OPER_EVENT_report()		/* report :STATus:OPERation:EVENt? reg */
{
int summary,cond;
unsigned int *addr = &STAT_OPER_COND.tag-1;	/* get address of bit struct */

	cond = *addr;				/* get 16 bit word of condition. */
	STAT_OPER_EVENT=cond;			/* store cond as event word since no transition filtering is used */
	summary = cond & STAT_OPER_ENABLE;	/* AND event w/enable bits */
	if (summary)				/* if EVENt is enabled, */
	{
		STB.oper_stat=1;		/* set summary bit if event not 0. */
		IE_Service_Request();		/* Set status byte and do RQS if MSS true. */
	}
}
/* --------------------------------------------------------------------------------- */
/* 17 */
void	scpi_STAT_QUES_EVENT_report()		/* report :STATus:QUES:EVENt? reg */
{

int summary,cond;
unsigned int *addr = &STAT_QUES_COND.tag-1;	/* get address of bit struct */
	cond = *addr;						/* get 16 bit word of condition. */

	STAT_QUES_EVENT=cond;				/* store cond as event word since no transition filtering is used */

	summary = cond & STAT_QUES_ENABLE;	/* AND event w/enable bits */
	if (summary)						/* if EVENt is enabled, */
	{	STB.ques_stat=1;			/* set summary bit if event not 0. */
		IE_Service_Request();			/* Set status byte and do RQS if MSS true. */
	}
}

/* ------------------------------------------------------------------------------- */
/* 18 */
void	IE_esr_report()
{
 unsigned char *addr;
 char esb,esr;
 int n;

	SYSTEM_ERROR[0]=NULL;

								/* set sys_error msg. */

	if (ESR.qye)
	{	strcpy(SYSTEM_ERROR,"-400, \"Query error\"\0");	/* load bufr w/msg. */
		STB.eeq=1;						/* set scpi error/event que summary bit. */
	}

	if (ESR.dde)
	{	strcpy(SYSTEM_ERROR,"-300, \"Device-specific error\"\0");	/* load bufr w/msg. */
		STB.eeq=1;							/* set scpi error/event que summary bit. */
	}

	if (ESR.exe)
	{	strcpy(SYSTEM_ERROR,"-200, \"Execution error\"\0");	/* load bufr w/msg. */
		STB.eeq=1;							/* set scpi error/event que summary bit. */
	}

	if (ESR.cme)
	{	strcpy(SYSTEM_ERROR,"-100, \"Command error\"\0");	/* load bufr w/msg. */
		STB.eeq=1;				/* set scpi error/event que summary bit. */
	}


	strcat(SYSTEM_ERROR,";\0");		/* terminate w/; */
	strcat(SYSTEM_ERROR,ERROR_DESC);	/* build syst:err string */

#ifdef IEEE_ERROR
    if (IEEE_DEBUG)
    {
	lcd_display("          \0",1,30);
	lcd_display("                                        \0",2,1);
	lcd_display("                                        \0",3,1);
	lcd_display("                                        \0",4,1);
	lcd_display("IE_esr_report(): SCPI_hdr_path=\0",1,1);
	lcd_display(ltoX((long)SCPI_hdr_path),1,32);

	lcd_display("IE_keyword=\0",2,1);
	lcd_display(IE_keyword,2,12);

	lcd_display("ESR.cme=\0",2,30);
	lcd_display(itoa(ESR.cme),2,38);

	lcd_display("SYSTEM_ERROR=\0",3,1);
	lcd_display(SYSTEM_ERROR,3,14);

	lcd_display("ERROR_DESC=\0",4,1);
	lcd_display(ERROR_DESC,4,12);

	delay(3000);

	lcd_display("          \0",1,30);
	lcd_display("                                        \0",2,1);
	lcd_display("                                        \0",3,1);
	lcd_display("                                        \0",4,1);

        lcd_display(">>>>\0",1,1);
	lcd_display(&IE_inbufr[IE_inptr],1,4);

	delay(3000);

    }
#endif //IEEE_ERROR

	for ( n=0; n<strlen(SYSTEM_ERROR); n++)
	{
		if (SYSTEM_ERROR[n]<0x20)			/* if any ctrl chars found in err string, */
			SYSTEM_ERROR[n]='_';			/* replace it. */
	}

				/* report Event Status Bit. */
				/* First calc ESB bit of STB. */
	addr = &ESR.tag-1;	/* get address of ESR bit struct */
	esr = *addr;		/* get 8 bit byte of condition. */
	esb = esr & ESE;	/* AND condition w/enable bits */

	if (esb)
	{	STB.esb =1;					/* set esb bit of STB if ESB not 0 */
		IE_Service_Request();		/* Set status byte and do RQS if MSS true. */
	}
	else
	{	STB.esb =0;					/* clr esb bit of STB if ESR not enabled. */
		IE_load_serpoll();			/* Load Serial Poll reg */
	}

//	for(i=0;i<160;i++)
//		ERROR_DESC[i]='\0';			/* clr err desc. */

	ERROR_DESC[0]=NULL;			/* clr err desc. */
}

/* ---------------------------------------------------------------------------------------- */
/* 19 */
int IE_load_outbufr_char(char c)	/* add byte to buffer if not full. */
{

	if (1 > (sizeof(IE_outbufr)-IE_outptr)) /* see if msg too big to fit in space left in bufr. */
	{
		IE_outptr=0;				/* msg too big. */
		IE_out_count=0;				/* clr # of bytes to send. */
		STB.mav=0;					/* clr msg available flag. */
		ESR.qye=1;					/* flag query error: DEADLOCK! */
		strcpy(ERROR_DESC,"Output buffer full\0");	/* point to err msg. */
		IE_esr_report();				/* signal host of error */
		return 0;					/* not OK. */
	}

	IE_outbufr[IE_outptr]=c;	/* copy byte. */

	IE_outptr ++;			/* adjust bufr ptr. */
	IE_out_count ++;			/* adjust 'bytes to send' count. */

	STB.mav =1;				/* flag msg available. */

	return 1;				/* OK */
}
/* ---------------------------------------------------------------------------------------- */

void	IE_load_outbufr_str(char *s)		/* add string to buffer if not full. */
{
int n;
unsigned msgsize;						/* Response Formatter (parser sets NL). */


	msgsize=strlen(s);					/* get length of string to copy. */

	for ( n=0; n<msgsize; n++)
	{	if (!(IE_load_outbufr_char(*s) ))
			return;			/* add byte to buffer if not full. */
						/* if full, ret. */
		s++;				/* point to next byte to load. */
	}

}

/* ---------------------------------------------------------------------------------------------------------------------- */
/* 20 */
void IE_sep()	/* terminate query response data with Response msg seperator (;). */
{
	if (IE_outbufr[IE_outptr-1] == ',')
	{
		IE_outptr--;
        IE_out_count--;
	}
	IE_outbufr[IE_outptr]=';';
	IE_outptr++;
  IE_out_count++;
}

/* --------------------------------------------------------------------------------- */
/* 21 */
void	scpi_SYST_ERROR_query() /* query :SYSTem:ERRor? queue */
{
char *ptr={"0, \"No error\"\0"};

	if (STB.eeq) IE_load_outbufr_str(SYSTEM_ERROR);			/* if queue not empty, */
	else IE_load_outbufr_str(ptr);			/* else */

	IE_sep();					/* terminate query response data with seperator (;). */


	ESR.qye=0;		/* clr ESR flag bits that correspond to SYST:ERR msgs. */
	ESR.dde=0;		/* (special 'Automatic' ESR_flags clearing) */
	ESR.exe=0;
	ESR.cme=0;

	STB.eeq=0;		/* clr scpi error/event que summary bit. */
	IE_load_serpoll();	/* Update Serial Poll reg. */

}
/* ------------------------------------------------------------------------------------- */
/* 22 */
void	scpi_SYST_VERS_query()			/* query :SYSTem:VERSion? */
{

	IE_load_outbufr_str("1992.0,\0");	/* add data to buffer if not full. */
}
/* -------------------------------------------------------------------------- */
/* 23 */
void	scpi_STAT_OPER_EVENT_query()			/* query :STATus:OPERation:EVENt? reg */
{
	sprintf(IE_cvt_bufr,"%1d\0",STAT_OPER_EVENT);	/* load ascii rep of value in to bufr. */

	IE_load_outbufr_str(IE_cvt_bufr);	/* add data to buffer if not full. */
	IE_sep();							/* terminate query response data with seperator (;). */

											/* clr reg due to read. */
	STAT_OPER_EVENT=0;				/* clr STATus:OPER:EVENt reg. */
	STB.oper_stat=0;					/* clr summary bit. */
	IE_load_serpoll();				/* Update Serial Poll reg. */
}
/* -------------------------------------------------------------------------- */
/* 24 */
void	scpi_STAT_OPER_COND_query()		/* query :STATus:OPERation:COND? reg */
{
unsigned int *addr =&STAT_OPER_COND.tag-1;	/* get address of struct */

	sprintf(IE_cvt_bufr,"%1d\0",*addr);	/* load ascii rep of value in to bufr. */

	IE_load_outbufr_str(IE_cvt_bufr);	/* add data to buffer if not full. */
	IE_sep();							/* terminate query response data with seperator (;). */
}
/* -------------------------------------------------------------------------- */
/* 25 */
void	scpi_STAT_OPER_ENABLE()			/* set :STATus:OPER:ENABle reg */
{
	IE_get_parm_value();					/* get numeric from rcvd string. */
	STAT_OPER_ENABLE=IE_parm_value;		/* cvt float to WORD. */
}
/* -------------------------------------------------------------------------- */
/* 26 */
void	scpi_STAT_OPER_ENABLE_query()			/* query :STATus:OPERation:ENABle? reg */
{
	sprintf(IE_cvt_bufr,"%1d\0",STAT_OPER_ENABLE);/* load ascii rep of value in to bufr. */
	IE_load_outbufr_str(IE_cvt_bufr);	/* add data to buffer if not full. */
	IE_sep();							/* terminate query response data with seperator (;). */
}
/* -------------------------------------------------------------------------- */
/* 27 */
void	scpi_STAT_QUES_EVENT_query()			/* query :STATus:QUES:EVENt? reg */
{

	sprintf(IE_cvt_bufr,"%1d\0",STAT_QUES_EVENT);/* load ascii rep of value in to bufr. */
	IE_load_outbufr_str(IE_cvt_bufr);	/* add data to buffer if not full. */
	IE_sep();							/* terminate query response data with seperator (;). */

											/* clr reg due to read. */
	STAT_QUES_EVENT=0;				/* clr STATus:QUEStionable:EVENt reg */
	STB.ques_stat=0;					/* clr summary bit. */
	IE_load_serpoll();				/* Update Serial Poll reg. */
}
/* -------------------------------------------------------------------------- */
/* 28 */
void	scpi_STAT_QUES_COND_query()		/* query :STATus:QUES:COND? reg */
{
unsigned int *addr =&STAT_QUES_COND.tag-1;	/* get address of struct */

	sprintf(IE_cvt_bufr,"%1d\0",*addr);	/* load ascii rep of value in to bufr. */
	IE_load_outbufr_str(IE_cvt_bufr);	/* add data to buffer if not full. */
	IE_sep();						/* terminate query response data with seperator (;). */
}
/* -------------------------------------------------------------------------- */
/* 29 */
void	scpi_STAT_QUES_ENABLE()			/* set :STATus:QUES:ENABle reg */
{
	IE_get_parm_value();				/* get numeric from rcvd string. */
	STAT_QUES_ENABLE=IE_parm_value;	/* cvt float to WORD. */
}
/* -------------------------------------------------------------------------- */
/* 30 */
void	scpi_STAT_QUES_ENABLE_query()			/* query :STATus:QUES:ENABle? reg */
{
	sprintf(IE_cvt_bufr,"%1d\0",STAT_QUES_ENABLE);	/* load ascii rep of value in to bufr. */
	IE_load_outbufr_str(IE_cvt_bufr);	/* add data to buffer if not full. */
	IE_sep();							/* terminate query response data with seperator (;). */
}
/* -------------------------------------------------------------------------- */
/* 31 */
void	scpi_STAT_PRESET()				/* set :STATus:PRESet reg */
{
	STAT_QUES_ENABLE=0;	/* scpi :clr STATus:QUEStionable:ENABle reg */
	STAT_OPER_ENABLE=0;	/* scpi :clr STATus:QUEStionable:ENABle reg */
}


/* -------------------------------------------------------------------------- */
/* 32 */
void	IE_ESE()				/* set Event Status reg Enable. */
{
	IE_get_parm_value();	/* get numeric from rcvd string. */
	ESE=IE_parm_value;	/* cvt float to byte. */
}
/* -------------------------------------------------------------------------- */
/* 33 */
void	IE_ESE_query()		/* Event Status reg Enable query. */
{
float ese=ESE;				/* cvt char to float */

	sprintf(IE_cvt_bufr,"%1.0f\0",ese);	/* load ascii rep of value in to bufr. */

	IE_load_outbufr_str(IE_cvt_bufr);	/* add data to buffer if not full. */
	IE_sep();						/* terminate query response data with seperator (;). */
}

/* -------------------------------------------------------------------------- */
/* 34 */
void	IE_ESR_query()		/* query Event Status Reg. */
{
unsigned char *addr = &ESR.tag-1;	/* get address of ESR bit struct */
float esr = *addr;			/* get 8 bit Status Byte. */

	sprintf(IE_cvt_bufr,"%1.0f\0",esr);	/* load ascii rep of value in to bufr. */

	IE_load_outbufr_str(IE_cvt_bufr);	/* add data to buffer if not full. */
	IE_sep();						/* terminate query response data with seperator (;). */

	*addr=0;						/* clr ESR due to read. */
	STB.esb=0;						/* clr summary bit. */
	IE_load_serpoll();				/* Update Serial Poll reg. */

}
/* -------------------------------------------------------------------------- */
/* 35 */
void	IE_SRE()				/* set Service Request Enable. */
{
	IE_get_parm_value();		/* get numeric from rcvd string. */
	SRE=IE_parm_value;		/* cvt float to byte. */
}
/* -------------------------------------------------------------------------- */
/* 36 */
void	IE_SRE_query()			/* Service Request Enable query. */
{
float sre=SRE;					/* cvt char to float */

	sprintf(IE_cvt_bufr,"%1.0f\0",sre);	/* load ascii rep of value in to bufr. */
	IE_load_outbufr_str(IE_cvt_bufr);	/* add data to buffer if not full. */
	IE_sep();				/* terminate query response data with seperator (;). */

}
/* -------------------------------------------------------------------------- */
/* 37 */
void	IE_STB_query()			/* Status Byte query. */
{
unsigned char *addr = &STB.tag-1;	/* get address of STB bit struct */
float stb = *addr;				/* get 8 bit Status Byte. */

	sprintf(IE_cvt_bufr,"%1.0f\0",stb);	/* replaces gcvt() */
	IE_load_outbufr_str(IE_cvt_bufr);	/* add data to buffer if not full. */
	IE_sep();				/* terminate query response data with seperator (;). */

}
/* ------------------------------------------------------------------------- */
/* 38 */
void IE_RST()				/* set OCIS, OQIS, clr DDT, TRG, reset outputs. */
{

	DDT[0]=0;			/* clr defined trigger. */
	TRG_mode=0;		/* set trigger mode OFF. */
	OCAS=0;			/* set OCIS. */
	OQAS=0;			/* set OQIS. */
	IE_CLS();						/* Clr bus errs and rsv & serial poll byte. */
	gp_reset();
//	Remote_sense=0;		/* Int sense. */
//	output_OFF();		/* disable output cont, and Remote sense. */
//#ifdef IEC_4_11
//	trans_mode_OFF();	//shut-down transient interrupt
//#endif //IEC_4_11
//	ALC_mode=0;		/* ALC off, */
//	auto_psm=1;				/* auto */
//	zop_mode=0;				/* Off */
//	Trans_seg[0].T=0;	/* Transition time default is 0. */
//	pgm_edit_no=0;	/* */
//	orig_pgm_no=1;	/* if no pgm was edited, edit pgm1. (always non-zero). */
//	pgm_edit=def_pgm;		/* load defaults for new pgm. */
//	user_pgm[0]=pgm_edit;	/* set up manual mode values. */
//	exec_osc_pgm();		/* since int actually is not passed */
//	display_no=0;					/* set display to V/I meter */
//	ledA=0;				/* init LEDs */
//	ledB=0;
//	ledC=0;
//	ledF=0;
//	REMOTE_mode=0;
//	LLO=0;					//added @ v4.00
//	update_LEDs();				/* turn OFF REMOTE_MODE LED */
}
/* -------------------------------------------------------------------------- */
/* 39 */
void	IE_TST_query()			// *TST? query cmd.
{
        if (RAM_reset)
        {
	        IE_load_outbufr_str("1\0");
        }
        else
        {
	        IE_load_outbufr_str("0\0");
        }
		IE_sep();			// terminate query response data with seperator (;).
}

/* -------------------------------------------------------------------------- */
/* 40 */
void	IE_CLS()							/* Clr Status Byte and all Event regs */
{
unsigned char *addr = &STB.tag-1;		/* get address of STB bit struct */
unsigned char stb=*addr;					/* get stb */

	*addr=stb & 0x10;				/* clr 8 bit STB Status Byte but not MAV bit per ieee-488.2 11.2.4. */
	IE_load_serpoll();				/* Load status in to serial poll reg. */

	if (STB.mss==0)					/* If mss is false, clr rsv. */
	{	if (INTERFACE==0)		/* if not serial, */
		IE_auxcmd = 0x18;		/* clr rsv2 (RQS) function. */
	}

	addr = &ESR.tag-1;			/* get address of ESR bit struct */
	*addr=0;				/* clr 8 bit ESR Event Status Byte. */

	STAT_OPER_EVENT=0;				/* clr scpi :STATus:QUEStionable:EVENt? reg */
	STAT_QUES_EVENT=0;				/* clr scpi :STATus:QUEStionable:EVENt? reg */

	OCAS=0;							/* set OCIS. */
	OQAS=0;							/* set OQIS. */

#ifdef APU
	apu_fault_register = 0;
#endif //APU

	ERROR_DESC[0] = NULL;
	SYSTEM_ERROR[0] = NULL;
}
/* -------------------------------------------------------------------------- */
void IE_IDN_query()				/* Identify device */
{
extern int bc_osc;				/* flag states if BC osc PCB assys installed. */
	
	strcpy(cvtbufr,gp_state.gp_cid);		/* put text string in buffer */
//	strcpy(cvtbufr,gp_cid);		/* put text string in buffer */
	strcat(cvtbufr," \0");		// append blank.
	

  // str=ASSY;
	strcat(cvtbufr,ASSY);		// append ASSY number.
	strcat(cvtbufr," \0");		// append blank.
	strcat(cvtbufr,VER);		// append Version number (if exists).
	IE_load_outbufr_str(cvtbufr);		/* copy string data */
	IE_sep();					/* terminate query response data with seperator (;). */
}

/* -------------------------------------------------------------------------- */
void	IE_OPC()					/* Operation Complete cmd. */
{
	OCAS=1;
	ESR.opc=0;
	IE_esr_report();
}
/* -------------------------------------------------------------------------- */
void	IE_OPC_query()				/* Operation Complete query. */
{
  	ESR.opc=0;
  	OQAS=1;

	if (trans_mode)					//indicate transient status.
	{
		IE_load_outbufr_str("0\0");  	//transient in progress.
	}
	else
	{
		IE_load_outbufr_str("1\0");  	//transient NOT in progress.
	}
	IE_sep();				 	//terminate query response with seperator.
}
/* -------------------------------------------------------------------------- */
/* 44 */
void	IE_WAI()				/* WAIT cmd. */
{								/* indicate WAIt mode in effect. */
								/* most functions do not need WAIt since */
								/* All cmds are sequential */
								/* operation by design except EXEC:TRANS. */

//char c;
	if (trans_mode==0) return;

//wait:

	if (INTERFACE==0)						/* if not serial, */
        {
		if (IE_status1_iec)
		{
			IE_status1=IE_status1_iec;
                        IE_status1_iec=0;
		}
		else
                {
			IE_status1=IE_stat1;	/* store status0 byte from IEEE port. */
		}
	}
	else
        {
		IE_status1=0;					/* else CLR status. */
	}

	if ((IE_status1 & 0x8)== 8)			/* if DCL is set, */
	{
		init_outputs();			/* set all outputs to nominal */
#ifndef IECTEST
		_int166(0);				/* reset device */
#endif
	}

//	scan_kybd();				/* do partial scan of kybd */
//	switch(ikey)
//	{
//		case 'R': c='R'; break;	/* abort LOCAL key been hit */
//		case 'T': c='T'; break;	/* abort TRANS key been hit */
//		case 'O': c='O'; break;	/* abort OUTPUT ENABLE key been hit */
//	}

//	trans_mode_OFF();						/* abort trans if TRANS or OUTPUT keys hit. */
//	if (c=='O') 
//		toggle_Output_ONOFF();	/* toggle output if 'O' key seen. */
//	if (trans_mode) goto wait;			/* wait till trans done. */
}

/* ------------------------------------------------------------------------- */
void IE_oper_complete()		/* indicate operation complete if requested. */
{							/* for EXEC:TRANS;*OPC(?). */
	if (OCAS)				/* if *OPC cmd set, */
	{
		OCAS=0;				/* indicate Oper complete Query Command state done. */
		ESR.opc=1;			/* set opc bit in std event reg. */
		IE_esr_report();
	}
}
/* ------------------------------------------------------------------------- */
/* 46 */
///////////////////////////////////////////////
// function to implement the "SYST" CMDs/SUB_CMD
 int parse_SYST()		
{

	if (scpi_parser_reset) IE_get_keyword();	// get next keyword if parser reset. 
	/////////////////////////
	// ship name
	if (strstr(IE_keyword,"NAME\0"))				// does part of string match? 
	{	
		if(strstr(IE_keyword,":\0"))
		{
			IE_get_keyword();

			if(strstr(IE_keyword,"SHIP\0"))
			{
				sprintf(cvtbufr,"%s\0", ship_name);	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}

			if(strstr(IE_keyword,"ENGINEER\0"))
			{
				sprintf(cvtbufr,"%s\0","DAVE NEWBERRY\0");	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
		}
		else
		{
			if (QUERY) 
			{//:NAME?
				sprintf(cvtbufr,"%s\0", ship_name);	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
			}
			else	
			{// :NAME s[32]
//				ship_name = IE_get_parm_name();		//get pointer to SCPI string parameter.
				IE_get_parm_name();		//get pointer to SCPI string parameter.
				strcpy(ship_name,IE_parm_name);		//copy string to :SYST:NAME.
			}
			return 1;
		}
	}
	/////////////////////////
	// System Mode 
	if (strstr(IE_keyword,"MODE\0"))				// does part of string match? 
	{	
		sprintf(cvtbufr,"%d\0", gp_mode);	
		IE_load_outbufr_str(cvtbufr);	// add data to buffer 
		IE_sep();					// terminate query response data with seperator.
		return 1;
	}

	if (strstr(IE_keyword,"XCONF\0"))				// does part of string match? 
	{	
//			sprintf(cvtbufr,"%u",gp_configuration_word2);	
			sprintf(cvtbufr,"%u\0",VIRTUALconfiguration_word2);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}

	if (strstr(IE_keyword,"CONF\0"))				// does part of string match? 
	{	
//			sprintf(cvtbufr,"%u",gp_configuration_word);	
			sprintf(cvtbufr,"%u\0",VIRTUALconfiguration_word);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}

	////////////////////////
	// System ERR 
	if (strstr(IE_keyword,"ERR\0"))				// does part of string match? 
	{	
		scpi_SYST_ERROR_query();
		return 1;
	}
	/////////////////////////
	// option - DUAL INPUT 
	if (strstr(IE_keyword,"OPT\0"))				// does part of string match? 
	{	
		if(strstr(IE_keyword,":\0")) {
			IE_get_keyword();
			if(strstr(IE_keyword,"INPUT\0")) {
				sprintf(cvtbufr,"%d\0",DUAL_INPUT);
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
			if(strstr(IE_keyword,"XFER\0")) {
				sprintf(cvtbufr,"%d\0",SEAMLESS_TRANSFER);
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
			if(strstr(IE_keyword,"PARA\0")) {
				sprintf(cvtbufr,"%d\0",PARALLELING);
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
			if(strstr(IE_keyword,"MODE\0")) {
				sprintf(cvtbufr,"%d\0",gp_state.local_master);
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
			if(strstr(IE_keyword,"RANG\0")) {
				sprintf(cvtbufr,"%f\0",ILMr);
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
#ifdef MM3			
			if(strstr(IE_keyword,"FORM\0")) {
				sprintf(cvtbufr,"%d\0",DELTA_OUTPUT);
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
			
			if(strstr(IE_keyword,"RATED\0")) {
				sprintf(cvtbufr,"%d\0",(unsigned int)(RATED_POWER));
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
			
			if(strstr(IE_keyword,"PF\0")) {
				if (getPowerFactor() == 80)
					sprintf(cvtbufr,"0\0");
				else
					sprintf(cvtbufr,"1\0");
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
#endif
		}
	}
	return 0;								// parse failed. 

}

/* ------------------------------------------------------------------------- */
/* 47 */
int parse_STAT_OPER()							/* parse further. */
{
	if (scpi_parser_reset) IE_get_keyword();	/* get next keyword if parser reset. */
								/* define EVENt mnenonic */
	if (strstr(IE_keyword,"EVEN\0"))				/* does part of string match? */
	{
		scpi_STAT_OPER_EVENT_query();	/* Do query function. */
		return 1;
	}
								/* define CONDition mnenonic */
	if (strstr(IE_keyword,"COND\0"))				/* does part of string match? */
	{
		scpi_STAT_OPER_COND_query();	/* Do query function. */
		return 1;
	}
								/* define ENABle mnenonic */
	if (strstr(IE_keyword,"ENAB\0"))				/* does part of string match? */
	{
		if (QUERY)		/* See if this is a query */
		{									/* if so, */
			scpi_STAT_OPER_ENABLE_query();	/* Do query function. */
			return 1;
		}
		else							/* else */
			scpi_STAT_OPER_ENABLE();	/* Set ENABle bits. */
		return 1;
	}

	return 0;								/* parse failed. */
}
/* ---------------------------------------------------------------------------- */
/* 48 */
int parse_STAT_QUES()							/* parse further. */
{
	if (scpi_parser_reset) IE_get_keyword();	/* get next keyword if parser reset. */
								/* define EVENt mnenonic */
	if (strstr(IE_keyword,"EVEN\0"))				/* does part of string match? */
	{	scpi_STAT_QUES_EVENT_query();	/* Do query function. */
		return 1;
	}
								/* define CONDition mnenonic */
	if (strstr(IE_keyword,"COND\0"))				/* does part of string match? */
	{	scpi_STAT_QUES_COND_query();	/* Do query function. */
		return 1;
	}
								/* define ENABle mnenonic */
	if (strstr(IE_keyword,"ENAB\0"))				/* does part of string match? */
	{
		if (QUERY)		/* See if this is a query */
		{									/* if so, */
			scpi_STAT_QUES_ENABLE_query();	/* Do query function. */
			return 1;
		}
		else							/* else */
			scpi_STAT_QUES_ENABLE();	/* Set ENABle bits. */
		return 1;
	}

	return 0;								/* parse failed. */
}
/* ---------------------------------------------------------------------------- */
/* 49 */
int parse_STAT()			/* parse further. */
{
// int t;
  unsigned statusWord0=0;

	if (scpi_parser_reset) IE_get_keyword();	// get next keyword if parser reset.

	if (strstr(IE_keyword,"SW0\0"))				// does part of string match? 
	{	
			//construct statusWord0
			if (gp_status_word & 0x0040) statusWord0 = statusWord0 | 0x0001;	//OUTPUT_ENABLE
			if (gp_status_word & 0x0080) statusWord0 = statusWord0 | 0x0002;	//INPUT_ENABLE
			if (gp_status_word3 & 0x0010) statusWord0 = statusWord0 | 0x0004;	//K4_CLOSE_CMD
			if (gp_status_word3 & 0x0080) statusWord0 = statusWord0 | 0x0008;	//K2_CLOSE_CMD
			if (gp_status_word2 & 0x0008) statusWord0 = statusWord0 | 0x0010;	//V_OUT_OK

			if (remote_genset==GEN1) statusWord0 = statusWord0 | 0x0400;	//G1_MASTER
			if (gp_state.genstat1) statusWord0 = statusWord0 | 0x0800;		//G1_ONLINE
			if (remote_genset==GEN2) statusWord0 = statusWord0 | 0x2000;	//G2_MASTER
			if (gp_state.genstat2) statusWord0 = statusWord0 | 0x4000;		//G2_ONLINE
			
#ifdef TEST_FIXTURE
	if (gp_state.inpstat1)	// FOR DEBUG ONLY!!;
	{
		makeBit(&statusWord0,3,1);	//set K2_CLOSE_CMD SW0 virtual status bit.
	}
	else
	{
		makeBit(&statusWord0,3,0);	//clear K2_CLOSE_CMD SW0 virtual status bit.
	}
	if (gp_state.invstat)	// FOR DEBUG ONLY!!;
	{
		makeBit(&statusWord0,2,1);	//set K4_CLOSE_CMD SW0 virtual status bit.
		makeBit(&statusWord0,0,1);	//set OUTPUT_ENABLE SW0 virtual status bit.
	}
	else
	{
		makeBit(&statusWord0,2,0);	//clear K4_CLOSE_CMD SW0 virtual status bit.
		makeBit(&statusWord0,0,0);	//clear OUTPUT_ENABLE SW0 virtual status bit.
	}
	if (gp_state.converter_power_state)	// FOR DEBUG ONLY!!;
	{
		makeBit(&statusWord0,0,1);	//set OUTPUT_ENABLE SW0 virtual status bit.
	}
	else
	{
		makeBit(&statusWord0,0,0);	//clear OUTPUT_ENABLE SW0 virtual status bit.
	}
#endif //TEST_FIXTURE

			sprintf(cvtbufr,"%u\0",statusWord0);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}

	if (strstr(IE_keyword,"COPS\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%u\0",optcon_status_2);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}

	if (strstr(IE_keyword,"TIE\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%u\0",tieBreaker_CLOSED());	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}

	if (strstr(IE_keyword,"SW1\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%u\0",gp_status_word);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}

	if (strstr(IE_keyword,"SW2\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%u\0",gp_status_word2);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}

	if (strstr(IE_keyword,"SW3\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%u\0",gp_status_word3);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}

	if (strstr(IE_keyword,"G1\0"))				// does part of string match? 
	{	
		if (MULTIGEN_OPTION)
			sprintf(cvtbufr,"%d\0",eGen1_CLOSED());	
		else
			sprintf(cvtbufr,"%d\0",gp_state.genstat1);	
		IE_load_outbufr_str(cvtbufr);	// add data to buffer 
		IE_sep();					// terminate query response data with seperator.
		return 1;
	}

	if (strstr(IE_keyword,"G2\0"))				// does part of string match? 
	{	
		if (MULTIGEN_OPTION)
			sprintf(cvtbufr,"%d\0",eGen2_CLOSED());	
		else
			sprintf(cvtbufr,"%d\0",gp_state.genstat2);	
		IE_load_outbufr_str(cvtbufr);	// add data to buffer 
		IE_sep();					// terminate query response data with seperator.
		return 1;
	}

	if (strstr(IE_keyword,"G3\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%d\0",eGen3_CLOSED());	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}

	if (strstr(IE_keyword,"G4\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%d\0",eGen4_CLOSED());	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}

	if (strstr(IE_keyword,"SP1\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%d\0",gp_state.inpstat1);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}
	if (strstr(IE_keyword,"SP2\0"))				// does part of string match? 
	{	

			sprintf(cvtbufr,"%d\0",gp_state.inpstat2);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}
	if (strstr(IE_keyword,"GEN1\0"))				// does part of string match? 
	{	
		if (MULTIGEN_OPTION)
			sprintf(cvtbufr,"%d\0",eGen1_CLOSED());	
		else
			sprintf(cvtbufr,"%d\0",gp_state.genstat1);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}
	if (strstr(IE_keyword,"GEN2\0"))				// does part of string match? 
	{	
		if (MULTIGEN_OPTION)
			sprintf(cvtbufr,"%d\0",eGen2_CLOSED());	
		else
			sprintf(cvtbufr,"%d\0",gp_state.genstat2);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}
	if (strstr(IE_keyword,"GEN3\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%d\0",eGen3_CLOSED());	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}
	if (strstr(IE_keyword,"GEN4\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%d\0",eGen4_CLOSED());	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}
	if (strstr(IE_keyword,"CONV\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%d\0",gp_state.invstat);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}
	if (strstr(IE_keyword,"SYST\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%d\0",gp_state.sysstat);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}
	if (strstr(IE_keyword,"VIEWPOW\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%d\0",gp_state.shore_power_viewed);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}
//////////////////////////////////////////////
	//
	if (strstr(IE_keyword,"FAULT\0"))				// does part of string match? 
	{	
		if(strstr(IE_keyword,":\0"))
		{
			IE_get_keyword();
			if (strstr(IE_keyword,"ILIM\0"))				// does part of string match? 
			{	
				sprintf(cvtbufr,"%d\0",gp_state.hardware_current_limit);	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
			if (strstr(IE_keyword,"PFC\0"))				// does part of string match? 
			{	
				sprintf(cvtbufr,"%d\0",gp_state.pfc_fault);	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
			if (strstr(IE_keyword,"LVDC\0"))				// does part of string match? 
			{	
				sprintf(cvtbufr,"%d\0",gp_state.lvdc_fault);	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
			if (strstr(IE_keyword,"CONV\0"))				// does part of string match? 
			{	
				sprintf(cvtbufr,"%d\0",gp_state.inverter_fault);	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
			if (strstr(IE_keyword,"PHVDC\0"))				// does part of string match? 
			{	
				sprintf(cvtbufr,"%d\0",gp_state.pos_hvdc_overvoltage);	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
			if (strstr(IE_keyword,"NHVDC\0"))				// does part of string match? 
			{	
				sprintf(cvtbufr,"%d\0",gp_state.neg_hvdc_overvoltage);	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
			if (strstr(IE_keyword,"OVERTEMP\0"))				// does part of string match? 
			{	
				sprintf(cvtbufr,"%d\0",gp_state.system_overtemperature);	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
			if (strstr(IE_keyword,"DOWN\0"))				// does part of string match? 
			{	
				sprintf(cvtbufr,"%d\0",gp_state.shutdown);	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
			if (strstr(IE_keyword,"IINP\0"))				// does part of string match? 
			{	
				sprintf(cvtbufr,"%d\0",gp_state.input_overcurrent);	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
		}
	}

	if (strstr(IE_keyword,"CPON\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%d\0",gp_state.converter_power_state);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}
	if (strstr(IE_keyword,"TSGEN1\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%d\0",gp_state.generator1_contactor_state);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}
	if (strstr(IE_keyword,"TSGEN2\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%d\0",gp_state.generator2_contactor_state);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}
	if (strstr(IE_keyword,"TSCON\0"))				// does part of string match? 
	{	
			sprintf(cvtbufr,"%d\0",gp_state.output_contactor_state);	
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}

	if (strstr(IE_keyword,"LFAIL\0"))				// does part of string match? 
	{	
			IE_load_outbufr_str(getErrOnCodeDescriptor(last_failure_code));	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}

	if (strstr(IE_keyword,"CODE\0"))				// does part of string match? 
	{	
			IE_load_outbufr_str(getErrOnCodeDescriptor(active_status_code));	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
	}

	if (strstr(IE_keyword,"INFO\0"))				// does part of string match? 
	{	
		if (gp_state.inpstat1==OFFLINE && !shutoffId) shutoffId=4;

		sprintf(cvtbufr,"%u\0",shutoffId);	
		IE_load_outbufr_str(cvtbufr);	// add data to buffer 
		IE_sep();						// terminate query response data with seperator.
		return 1;
	}

	if (strstr(IE_keyword,"SHUTDOWN\0"))				// does part of string match? 
	{	
		if (gp_state.inpstat1==OFFLINE && !shutoffId) shutoffId=4;

		IE_load_outbufr_str(shutoffStr[shutoffId]);	// add data to buffer 
		IE_sep();						// terminate query response data with seperator.
		return 1;
	}

	return 0;								// parse failed.
}
/* ------------------------------------------------------------------------ */
///////////////////////////////////////////////////
// main SCPI parse 
int SCPI_parse()
{
int t;

	////////////////////////////
	// handler for MEAS CMDs
	if (strstr(IE_keyword,"MEAS\0"))		// does part of string match? 
	{
		SCPI_hdr_path=parse_MEAS;		// store root function address. 
		t=parse_MEAS();							// parse further. 
		return t;
	}

	////////////////////////////
	// handler for SYST ?????
	if (strstr(IE_keyword,"SYST\0"))		// does part of string match? 
	{	SCPI_hdr_path=parse_SYST;				// store root function address. 
		t=parse_SYST();									// parse further. 
		return t;
	}
	/////////////////////////////
	// handler for ROUT
	if (strstr(IE_keyword,"ROUT\0"))		/* does part of string match? */
	{
		SCPI_hdr_path=parse_ROUT;	/* store root function address. */
		t=parse_ROUT();			/* parse further. */
		return t;
	}

	/////////////////////////////
	// handler for SOUR
	//if (strstr(IE_keyword,"SOUR\0"))		/* does part of string match? */
	//{
	//	SCPI_hdr_path=parse_SOUR;	/* store root function address. */
	//	t=parse_SOUR();			/* parse further. */
	//	sprintf(cvtbufr,"%d\0",t);	
	//	IE_load_outbufr_str(cvtbufr);	// add data to buffer 
	//	IE_sep();					// terminate query response data with seperator.
	//	return t;
	//}
	//////////////////////////////////
	// handler STATus pgm mnemonic 

	if (strstr(IE_keyword,"STAT\0"))		
	{	SCPI_hdr_path=parse_STAT;	
		t=parse_STAT();		
		return t;
	}
	///////////////////////////////////
	// comment out for first release -SIX

	
	if (strstr(IE_keyword,"CAL\0"))		
	{	
		SCPI_hdr_path=parse_CAL;	
		t=parse_CAL();				
		return 1;
	}

	/* define SOURce pgm mnemonic */
	if (!(strstr(IE_keyword,"SOUR\0")))	scpi_parser_reset=0; // clr flag since SOURce keyword non-existent. */
	/* This allows current keyword to be parsed. */
	SCPI_hdr_path=parse_SOUR;		/* store root function address. */
	t=parse_SOUR();					/* parse SOURce keyword. */
	// sprintf(cvtbufr,"%d\0",t);	
	// IE_load_outbufr_str(cvtbufr);	// add data to buffer 
	// IE_sep();					// terminate query response data with seperator.
	return t;
	// return 0;
}

/* --------------------------------------------------------------------------- */
/* 51 */
///////////////////////////////////////////////////////////////
// new parse for GP3k
void IE_parse()		/* Parser and Execution Control unit. */
{
int first_parse,i;

	remoteActive=YES;

	if (STB.mav !=0)				/* Since data rcvd while output bufr not empty, */
	{
		STB.mav=0;						/* clr msg available flag. */
		IE_outptr=0;					/* reset output bufr ptr. */
		IE_out_count=0;				/* clr # of bytes to send. */
		ESR.qye=1;						/* flag query error since response trashed. */
		IE_esr_report();			/* report Event Status. */
	}
	IE_inptr=0;							/* reset input bufr ptr to start parsing at beginning of inbufr */
	first_parse=1;					/* set this flag for parsing first cmd of pgm msg */

	for(i=0 ; i< IE_BUF_SIZE; i++)				/* cvt ENTIRE string to uppercase for comparison. */
	{
		if ((IE_inbufr[i] >= 0x61) && (IE_inbufr[i] <= 0x7A))
			IE_inbufr[i] = IE_inbufr[i] & 0xDF; // to upper case
	}

parse:

	if (IE_inptr!=0)					/* if past start of input string, */
		IE_scan_to_end();				/* scan to end of cmd. */

	if( (IE_inbufr[IE_inptr] == '\n') || (IE_inbufr[IE_inptr]==0))
		goto endparse;					/* ret when terminator found. */

	IE_get_keyword();					/* get cmd/query pgm header. */
										/* Analyze cmds now. */
	////////////////////////////////
	// disable commands - SIX
	// if (strstr(IE_keyword,"*OPC?\0"))		// does part of string match? 
	// {
	// 	IE_OPC_query();				//set OQAS. */
	// 	goto parse;						//loop till terminator found. */
	// }
	// if (strstr(IE_keyword,"*OPC\0"))		/* does part of string match? */
	// {
	// 	IE_OPC();						/* set OCAS. */
	// 	goto parse;						/* loop till terminator found. */
	// }
	///////////////////////////////////
	// end - SIX
	///////////////////////////////////
	if (strstr(IE_keyword,"*IDN?\0"))		//  does part of string match? 
	{	IE_IDN_query();					// Identify device if so 
		goto parse;							// loop till terminator found. 
	}
	//////////////////////////////////////////
	// SIX - disable commends
	// if (strstr(IE_keyword,"*STB\0"))		/* does part of string match? */
	// {	IE_STB_query();					/* Report status byte if so */
	//	goto parse;						/* loop till terminator found. */
	//}

		if (strstr(IE_keyword,"*CLS\0"))		/* does part of string match? */
		{
	// 	IE_CLS();						/* clr all Event STATUS bits */
			IE_init();
	   		goto parse;						/* loop till terminator found. */
	   	}

	// if (strstr(IE_keyword,"*SRE?\0"))		/* does part of string match? */
	// {	IE_SRE_query();					/* report Service Request Enable. */
	// 	goto parse;						/* loop till terminator found. */
	// }
	// if (strstr(IE_keyword,"*SRE\0"))		/* does part of string match? */
	// {	IE_SRE();						/* set Service Request Enable. */
	// 	goto parse;						/* loop till terminator found. */
	// }
	// if (strstr(IE_keyword,"*ESR?\0"))		/* does part of string match? */
	// {	IE_ESR_query();					/* Report Event Status Reg byte if so */
	//	goto parse;						/* loop till terminator found. */
	// }
	// if (strstr(IE_keyword,"*ESE?\0"))		/* does part of string match? */
	// {	IE_ESE_query();					/* Report Event Status Enable byte if so */
	//	goto parse;						/* loop till terminator found. */
	// }
	// if (strstr(IE_keyword,"*ESE\0"))		/* does part of string match? */
	// {	IE_ESE();						/* set Event Status Enable byte if so */
	// 	goto parse;						/* loop till terminator found. */
	// }
	///////////////////////////////////////
	// SIX - end 
	if (strstr(IE_keyword,"*RST\0"))		/* does part of string match? */
	{
//		IE_RST();						/* set OCIS, OQIS, clr DDT, TRG, reset outputs. */
		IE_init();
		goto parse;					/* loop till terminator found. */
	}
	if (strstr(IE_keyword,"*RESET\0"))		/* does part of string match? */
	{
		bat_ok[0] = ' ';
		gp_reset();
		goto parse;					/* loop till terminator found. */
	}
	///////////////////////////////////////////////
	// SIX - comment out 
	// if (strstr(IE_keyword,"*WAI\0"))		/* does part of string match? */
	// {	IE_WAI();						/* null function. */
	// 	goto parse;						/* loop till terminator found. */
	// }
	///if (strstr(IE_keyword,"*TST?\0"))		/* does part of string match? */
	// {	IE_TST_query();					/* test ram. */
	// 	goto parse;						/* loop till terminator found. */
	// }
	// if (strstr(IE_keyword,"*TRG\0"))		/* does part of string match? */
	//{
	// 	goto parse;						/* loop till terminator found. */
	// }
	///* SERIAL.2 cmds (c) 1993 BC */
	if (strstr(IE_keyword,"*LLO\0"))		/* does part of string match? */
	{
			LLO=1;							
	// 	set_REMOTE_mode();		/* Force REMOTE mode for Serial LLO. */
			goto parse;						/* loop till terminator found. */
	}
	if (strstr(IE_keyword,"*GTL\0"))		/* does part of string match? */
	{	
		LLO=0;
		IE_out_count=0;					/* clr output bufr counter and pointer. */
		IE_outptr=0;
	//		display_no=0;					/* set display to V/I meter */
	//	IE_CLS();						/* Clr bus errs and rsv & serial poll byte. */
	//		REMOTE_mode=0;					/* clr Remote_mode */
	//		update_LEDs();					/* turn OFF REMOTE_MODE LED */
		goto parse;						/* loop till terminator found. */
	}
	////////////////////////////
	//if (strstr(IE_keyword,"*DCL\0"))		/* does part of string match? */
	// {
	//	init_outputs();					/* set all outputs to nominal */
	// #ifndef IECTEST
	//	_int166(0);						/* reset device */
	// #endif
	// }
	///////////////////////////////////////
	//  Parse SCPI cmd. 
	if (IE_keyword[0]==':')				// Does first keyword in msg unit start with a colon? 
	{	
		IE_keyword[0]=' ';				// replace colon w/space so only trailing colons can exist (so compound commands can be detected). */
		SCPI_hdr_path=SCPI_parse;	// if so, reset SCPI function memory, 
		scpi_parser_reset=1;			//  set flag.
	}
	else
		scpi_parser_reset=0;	   	// clr flag since parsing already started. 
	// previous hdr path to be kept and used. 
	// current keyword will be parsed at 2nd parse level. */

	if (first_parse)					/* is this the 1st byte? */
	{	
		scpi_parser_reset=1;		/* if so, set flag to parse 1st byte regardless of leading colon. */
		first_parse=0;					/* clr this flag also */
	}

	if (scpi_parser_reset==1)	/* if new cmd, */
	{	
		if (SCPI_parse())				/* parse SCPI cmd */
		goto parse;							/* Loop if parse was successfull. */
	}
	else								/* else if not new cmd, */
	{	if (SCPI_hdr_path())			/* parse SCPI cmd using previous hdr path. */
		goto parse;						/* Loop if parse was successfull. */
	}

	/* no command match found */
	ESR.cme=1;							/* flag cmd. err */
	strcpy(ERROR_DESC,IE_keyword);		/* point to bad keyword */
	IE_esr_report();					/* report Event status bit. */

endparse:

	if (STB.mav)			/* if Msg AVailable to send, */
	{	
		IE_outptr--;		/* adjust bufr ptr so NL replaces last Response msg seperator (;). */
										/* IE_out_count is OK. */
		if (INTERFACE==0) goto send_NL;	/* can't send CR if GPIB. */
										/* XMTD eos: 1=LF, 0=CR/LF, 2=CR. */
		if (eos_mode==1) goto send_NL;	/* Send LF only. */

		IE_load_outbufr_char(*"\r\0");	/* load CR to send out. */
		if (eos_mode==2) goto send;		/* Send CR only */

send_NL:
		IE_load_outbufr_char(*"\n\0");	/* load LF (NL) to send out. */

send:
		IE_out_count--;					/* Correct output bufr byte count due to auto-inc in IE_load_outbufr_char(). */
		IE_Service_Request();			/* Set status byte and do RQS if MSS true. */
	}

	IE_inptr=0;				/* bufr all parsed, so reset input bufr ptr. */
	IE_outptr=0;			/* reset output bufr ptr. */
										/* IE_out_count might be set. */
//		IE_out_count=0;		/* clr output bufr counter and pointer. */
//		IE_status0=0;		/* Also clr status & mav to stop XMTR. */
//#ifdef IEC
//		IE_status0_iec=0;	/* Also clr status & mav to stop XMTR. */
//#endif //IEC
//		STB.mav=0;
}

/* ------------------------------------------------------------------------- */

/* 52 */

void IE_rcv_byte()	/* get byte from REMOTE port, add it to buffer if not full. */
{
	if (IE_inptr == sizeof(IE_inbufr)-1)	/* see if inbufr is full. */
	{
		IE_inptr=0;					/* bufr full, reset bufr ptr so caller can see how many bytes rcvd (0 if [full] error) */
		ESR.cme=1;					/* flag cmd error: DEADLOCK! */
		strcpy(ERROR_DESC,"Input buffer full\0");	/* point to error msg. */
		IE_esr_report();				/* report Event Status. */
	}

	IE_byte_in=IE_datain;					/* get the byte from 9914 */

	if (IE_byte_in==0x0D) IE_byte_in=0x0A;	/* make CR into NL */
	IE_inbufr[IE_inptr]=IE_byte_in;		/* add to buffer */
	IE_inptr++;							/* inc bufr pointer */

}

/* ------------------------------------------------------------------------ */
/* 53 */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
void IE_send_byte()						/* send byte out REMOTE port */
{
	IE_byte_out=IE_outbufr[IE_outptr];	/* get byte to send out. */

	if (INTERFACE==0)					/* if not serial, */
	{	if (IE_byte_out==0xA)			/* if NL,RCV */
		IE_auxcmd=8;
		IE_dataout=IE_byte_out;
	}
	else								/* If SERIAL, */
	{
		_putbit(0,S0TIC,7);			/* set ready to transmit */
		sotbuf = IE_byte_out;		/* for debugging */
#ifdef ASC0
		S0TBUF = sotbuf;			/* put byte in transmit data buffer */
#endif
		while(!_getbit(S0TIC,7));	/* wait until transmit buffer empty */
	}
	IE_outptr++;					/* point to the next byte to send out */
}
/* --------------------------------------------------------------------------- */
void remoteManager()
{
	if (modbusActive)
	{
		refreshModbusData();	//got fresh data, so, refresh mb data
		idle_modbus();
	}
	else
	{
		IE_main();
	}
}
/* --------------------------------------------------------------------------- */
void IE_main()	/* REMOTE port executive routine. Msg Exchange ctrl & I/O ctrl. */
{
unsigned int timer;

	if (modbusActive) return;

check:
	timer=10000;		/* allow buffer to be filled */

re_check:

	timer--;						/* track timeout here */
	if (timer == 0) return;			/* ret on timout */
	/* Check status of 9914 (REM, IFC, DCL). */
	/* See if we s/RCV bytes from REMOTE port, */
	/* if one rcvd, loop for another unless eom or timout occurs. */

	if (eom_flag==1)	/* if eom (eoi) rcvd, parse string. */
	{
		if (INTERFACE==1)		/* if serial, */
		{
//			if (trans_mode) trans_mode_OFF();	// Kill TRANS if in trans mode. 
#ifdef ASC0
			_putbit(0,S0RIC,6);			// disable serial interrupt 
#endif
		}
		if (IE_inbufr[0]==0x0A)	IE_inptr=0;	// If only NL rcvd, (ie; CR,LF) Restart IE_inptr 				
		else IE_parse();	  // Extract & perform cmds from rcvd string. 
							// Also build output string for queries if req. */
		eom_flag=0;		// reset eom or (eoi) flag since parsing done, */
		_nop();
#ifdef ASC0
		if(INTERFACE==1) _putbit(1,S0RIC,6);// enable serial interrupt 
#endif
	}
	// STAT_update();	/* update SHUTDOWN, :STAT:OPER & STAT:QUES regs. */
	if (STB.mav)		/* if Msg AVailable to send, */
	{								/* try to send it. */
		if (INTERFACE==1)	goto send; // if serial
		if ((IE_status0 & 0x10)== 0x10) /* if BO is set, */
send:	{
			IE_out_count--;		/* dec bytes to send. */
			IE_send_byte();		/* send byte. */
			if (INTERFACE==1)	IE_status0=0; // if serial, clr status so isr_SERIAL_0() can set it 
			/* upon TC. */
			if (IE_out_count==0)		/* if 0 bytes left to send, */
			{
				IE_outptr=0;			/* reset ptr. */
				STB.mav=0;				/* clr msg available flag. */
				IE_load_serpoll();		/* Update Serial Poll reg */
				return;					/* msg sent. */
			}
			goto check;			//* loop to chk status, restart timer. 
		}
		goto re_check;		//* loop to chk status, w/countdown. 
	}
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
int setLoadShare(float newMasterCapacity)
{
  int i=0;
  float masterCapacity=newMasterCapacity;
  float capacity=getCapacity();

//
//make sure hardware and option configured.
//
	if (!CORDMAN_OPTION) 
	{
		return NOT_OK;	//"SLAVE NOT CONFIGURED FOR LOAD SHARING\0"
	}
	
	if (gp_state.local_master!=SLAVE)
	{
		return NOT_OK;
	}

//
//validate and lookup load sharing setpoint, then set PWM2 duty cycle.
//
	slaveLoadShare = (int) (1000.0f * (capacity / masterCapacity));

	while (loadShareTable[i][gp_state.vout_range] > slaveLoadShare)
	{
		i++;
		if (i>99) break;
	}

	if (i<2 || i>98)
	{
		return NOT_OK;	//"LOAD BALANCE DYNAMIC RANGE EXCEEDED!\0";
	}
	else
	{
		mach.pwm2_dutyCycle = (unsigned) loadShareTable[i][0];
	 	lastMasterCapacity = masterCapacity;
	}

	init_PWM2(mach.pwm2_bits,mach.pwm2_align,mach.pwm2_dutyCycle);		//unsigned(bits,align,duty)

	return OK;
}
/*---------------------------------------------------------------------------*/
float getCapacity()				//usage example:  float capacity=getCapacity();
{
  int icordRating[10]={30,32,50,60,63,100,125,150,200,250};
  int iCord;
  float capacity;
  float voltageNom;
  float rated_amps;

	switch (shoreCordRating)	//Slave's Cord.
	{
		case 30:	iCord = 0;	break;
		case 32:	iCord = 1;	break;
		case 50:	iCord = 2;	break;
		case 60:	iCord = 3;	break;
		case 63:	iCord = 4;	break;
		case 100:	iCord = 5;	break;
		case 125:	iCord = 6;	break;
		case 150:	iCord = 7;	break;
		case 200:	iCord = 8;	break;
		case 250:	iCord = 9;	break;
		default:	iCord = 9;	break;
	}

//
//calculate Capacity.
//
	voltageNom = gp_Vab[SP1];

	if (gp_state.input1_form==THREE_PHASE)
	{
		voltageNom = voltageNom * ONE_SQRT3;
	}

	if (gp_state.range_mode_id==LOW_RANGE)
	{
		rated_amps = IN_RATED_AMPS_LO_ONE;
	}
	else
	{
	 	rated_amps = IN_RATED_AMPS_HI_ONE;
	}
	

	if (gp_state.input1_form==ONE_PHASE)
	{
		rated_amps = rated_amps * 1.5f;
	}

	capacity = ((float) gp_state.input1_form * icordRating[iCord] * voltageNom) / 1000.0f;

	if (capacity > ((float) gp_state.input1_form * rated_amps * voltageNom / 1000.0f))
	{
 		capacity = (float) gp_state.input1_form * rated_amps * voltageNom / 1000.0f;
	}

	if (capacity > RATED_POWER)
	{
		capacity = RATED_POWER;
	}

//	if (daq_mux_id == OSC) {
//		Debug_Var[3] = gp_state.input1_form;
//		Debug_Var[4] = rated_amps;
//		Debug_Var[5] = capacity;
//	}
		

	return capacity;
}
/*--------------------------------------------------------------------------*/
/* -------------------------------------------------------------------------- */
/* 61 */
int parse_SOUR()						/* signal Source functions */
{
  unsigned i,count;
  float vtemp;
  int maxGenset=2;
  unsigned long droopDampMinutes=droopDampTime/6000;	//convert from eventTime to minutes.
  unsigned udroopDampMinutes=(unsigned)droopDampMinutes;

//	int key_code;

	if (scpi_parser_reset) IE_get_keyword();	/* get next keyword if parser reset. */

	// set_REMOTE_mode();			/* since controlling cmd rcvd, set REMote mode */

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Hello ASEA POWER SYSTEMS"	SERIAL COMMUNICATIONS TEST
//////////////////////////////////////////////////////////////////////////////////////////////////////
	if (strstr(IE_keyword,"HELLO\0"))			/* does part of string match? */
	{
		if (strstr(IE_keyword,":\0"))			/* is there another keyword?, if so, */
		{
			IE_get_keyword();				/* get next keyword */
		}

		clear_screen();
		lcd_display("HELLO, ASEA POWER SYSTEMS!\0", 2, 7);
		delay(2000);
		return 1;
	}
	if (strstr(IE_keyword,"TALKTOME\0"))			/* does part of string match? */
	{
		if (strstr(IE_keyword,":\0"))			/* is there another keyword?, if so, */
		{
			IE_get_keyword();				/* get next keyword */
		}

		sprintf(cvtbufr,"%s\0","HELLO DAVE\0");	
		IE_load_outbufr_str(cvtbufr);	// add data to buffer 
		IE_sep();					// terminate query response data with seperator.

		clear_screen();
		lcd_display("HELLO, Dave Newberry!  You are my god. \0", 2, 1);
		lcd_display("I am listening and will do as you wish!\0", 3, 1);
		delay(2000);
		return 1;
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef TEST_FW
#define vMIN  (GP_OUTPUT_VOLTAGE - 1.00 * GP_OUTPUT_VOLTAGE)
#define vMAX  (GP_OUTPUT_VOLTAGE + 0.20 * GP_OUTPUT_VOLTAGE)
#define fMIN  (20)
#define fMAX  (600)
#else
#define vMIN  (GP_OUTPUT_VOLTAGE - 0.2 * GP_OUTPUT_VOLTAGE)
#define vMAX  (GP_OUTPUT_VOLTAGE + 0.2 * GP_OUTPUT_VOLTAGE)
#define fMIN  (20)
#define fMAX  (120)
#endif //TEST_FW


#define dMIN  (0.001)	//0.001s=1ms.
#define dMAX  (30.000)	//30s.
//#define vMIN  (GP_OUTPUT_VOLTAGE - 1 * GP_OUTPUT_VOLTAGE)	//100% down, 0 volts
//#define vMAX  (GP_OUTPUT_VOLTAGE + 1 * GP_OUTPUT_VOLTAGE)	//100% up, 2*volts
//#define fMIN  (20)
//#define fMAX  (440)
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//PROG
	if (strstr(IE_keyword,"PROG\0"))				// does part of string match? 
	{	
		if(strstr(IE_keyword,":\0")) 
		{	
			IE_get_keyword();

			if(strstr(IE_keyword,"EXEC\0")) 
			{
				if (strstr(IE_keyword,"DEF\0")) 
				{//:PROG:EXECecuteDEFault
//					restore_output();	//RESTORE OUTPUT FREQUENCY AND VOLTAGE, etc. UNCONDITIONALLY!
										//slew_output(0.5, GP_OUTPUT_FREQUENCY, GP_OUTPUT_VOLTAGE).
					newSlewRate = 0.5;
					newFrequency = GP_OUTPUT_FREQUENCY;
					newVoltage = GP_OUTPUT_VOLTAGE;

					slew_output(newSlewRate, newFrequency, newVoltage);

					//tempSlewRate = newSlewRate;
					//tempFrequency = newFrequency;
					//tempVoltage = newVoltage;
				}
				else	
				{// :PROG:EXEC
					newSlewRate = tempSlewRate;
					newFrequency = tempFrequency;
					newVoltage = tempVoltage;

					slew_output(newSlewRate, newFrequency, newVoltage);
				}
				return 1;
			}			

			if(strstr(IE_keyword,"VTEMP\0")) 
			{
				if (QUERY) 
				{//:PROG:VTEMP?
					sprintf(cvtbufr,"%3.1f\0",tempVoltage);
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();					// terminate query response data with seperator.
				}
				else	
				{// :PROG:VTEMP x
					value = IE_get_parm_value();
					if ( (vMIN<value) && (value<=vMAX) )  tempVoltage = value;
				}
				return 1;
			}

			if(strstr(IE_keyword,"VOUT\0")) 
			{
				if (QUERY) 
				{//:PROG:VOUT?
					sprintf(cvtbufr,"%3.1f\0",newVoltage);
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();					// terminate query response data with seperator.
				}
				else	
				{// :PROG:VOUT x
					value = IE_get_parm_value();
					if ( (vMIN<value) && (value<=vMAX) )  tempVoltage = value;
				}
				return 1;
			}

			if(strstr(IE_keyword,"FTEMP\0")) 
			{
				if (QUERY) 
				{//:PROG:FTEMP?
					sprintf(cvtbufr,"%3.1f\0",tempFrequency);
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();					// terminate query response data with seperator.
				}
				else	
				{// :PROG:FTEMP x
					value = IE_get_parm_value();
					if ( (fMIN<value) && (value<=fMAX) )  tempFrequency = value;
				}
				return 1;
			}

			if(strstr(IE_keyword,"FREQ\0")) 
			{
				if (QUERY) 
				{//:PROG:FREQ?
					sprintf(cvtbufr,"%3.1f\0",newFrequency);
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();					// terminate query response data with seperator.
				}
				else	
				{// :PROG:FREQ x
					value = IE_get_parm_value();
					if ( (fMIN<value) && (value<=fMAX) )  tempFrequency = value;
				}
				return 1;
			}

			if(strstr(IE_keyword,"RTEMP\0")) 
			{
				if (QUERY) 
				{//:PROG:RTEMP?
					sprintf(cvtbufr,"%5.3f\0",tempSlewRate);
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();					// terminate query response data with seperator.
				}
				else	
				{// :PROG:RTEMP x
					value = IE_get_parm_value();
					if ( (dMIN<value) && (value<=dMAX) )  tempSlewRate = value;
				}
				return 1;
			}

			if(strstr(IE_keyword,"RAMP\0")) 
			{
				if (QUERY) 
				{//:PROG:RAMP?
					sprintf(cvtbufr,"%5.3f\0",newSlewRate);
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();					// terminate query response data with seperator.
				}
				else	
				{// :PROG:RAMP x
					value = IE_get_parm_value();
					if ( (dMIN<value) && (value<=dMAX) )  tempSlewRate = value;
				}
				return 1;
			}
		}
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
	if (strstr(IE_keyword,"IMPED\0")) /* does part of string match? */
	{
        	return parse_SOUR_IMPED();
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
//DROOP
	if (strstr(IE_keyword,"DROOP\0"))				// does part of string match? 
	{	
		if(strstr(IE_keyword,":\0")) 
		{	
			IE_get_keyword();

			if(strstr(IE_keyword,"RATE\0")) 
			{
				if (QUERY) 
				{//:DROOP:RATE?
					sprintf(cvtbufr,"%d\0",droop);
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();					// terminate query response data with seperator.
				}
				else	
				{// :DROOP:RATE x
					value = IE_get_parm_value();
					if ( (0<value) && (value<=5) )  droop = (int) value;
				}
				return 1;
			}
			if(strstr(IE_keyword,"STAT\0")) 
			{
				if (QUERY) 
				{//:DROOP:STATe?
					sprintf(cvtbufr,"%d\0",droopEnable);	
					IE_load_outbufr_str(cvtbufr);// add data to buffer 
					IE_sep();// terminate query response data with seperator.
				}
				else	
				{// :DROOP:STATe x
					droopEnable = (unsigned) IE_get_parm_value();

					if (droopEnable==DISABLED)
					{
						newVoltage = GP_OUTPUT_VOLTAGE;
						slew_output(0.5, GP_OUTPUT_FREQUENCY, newVoltage);
					}
				}
				return 1;
			}
			if(strstr(IE_keyword,"DELAY\0")) 
			{
//	droopDampTime = droopDampMinutes * 6000;	//convert from minutes to eventTime.

				if (QUERY) 
				{//:DROOP:DELAY?
					sprintf(cvtbufr,"%u\0",udroopDampMinutes);
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();					// terminate query response data with seperator.
				}
				else	
				{// :DROOP:DELAY x
					value = IE_get_parm_value();
					if ( (0.0<value) && (value<=60.0) )  droopDampTime = (unsigned long) value*6000.0;
				}
				return 1;
			}
		}
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
//LOAD
	if (strstr(IE_keyword,"LOAD\0"))				// does part of string match? 
	{	
		if(strstr(IE_keyword,":\0")) 
		{	
			IE_get_keyword();

			if(strstr(IE_keyword,"GEN\0")) 
			{
				if (QUERY) 
				{//:LOAD:GEN?
					sprintf(cvtbufr,"%u\0",autoTransferGenset);
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();					// terminate query response data with seperator.
				}
				else	
				{// :LOAD:GEN x
					value = IE_get_parm_value();
					if (MULTIGEN_OPTION) maxGenset=4;
					if ( (0<value) && (value<=maxGenset) )  autoTransferGenset = (unsigned) value;
					else return 0;						//failed request
				}
				return 1;
			}
			if(strstr(IE_keyword,"XFER\0")) 
			{
				if (QUERY) 
				{//:LOAD:XFER?
					sprintf(cvtbufr,"%d\0",autoTransferOnOverload);	
					IE_load_outbufr_str(cvtbufr);// add data to buffer 
					IE_sep();// terminate query response data with seperator.
				}
				else	
				{// :LOAD:XFER x
					autoTransferOnOverload = (unsigned) IE_get_parm_value();
				}
				return 1;
			}
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////
//MASTCAP
	if (strstr(IE_keyword,"MASTCAP\0"))				// does part of string match? 
	{	
		if (QUERY) 
		{//:MASTCAP?
			value = lastMasterCapacity;
			sprintf(cvtbufr,"%3.1f\0",value);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
		}
		else	
		{// :MASTCAP x
			if (gp_state.local_master==SLAVE)
			{
				value = IE_get_parm_value();
				if ( (4.9<value) && (value<=125.0) )
				{
					setLoadShare(value);
				}
			}
			else
			{
				return 0;	//must be a SLAVE cabinet to accept command
			}
		}
		return 1;
	}
//CAP
	if (strstr(IE_keyword,"CAP\0"))				// does part of string match? 
	{	
		if (QUERY) 
		{//:CAPACITY?
			value = getCapacity();
//			value = lastMasterCapacity;
			sprintf(cvtbufr,"%3.1f\0",value);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
		}
		else	
		{// :CAPACITY x
			if (gp_state.local_master==SLAVE)
			{
				value = IE_get_parm_value();
				if ( (4.9<value) && (value<=125.0) )
				{
					setLoadShare(value);
				}
			}
			else
			{
				return 0;	//must be a SLAVE cabinet to accept command
			}
		}
		return 1;
	}
///////////////////////////////////////////////////////////////////////////////////////////////
//CORD
	if (strstr(IE_keyword,"CORD\0"))				// :CORD
	{	
		if(strstr(IE_keyword,":\0")) 
		{	
			IE_get_keyword();
		
			if(strstr(IE_keyword,"TEST\0")) 
			{	
				if (QUERY) 
				{//:CORDalarm:TEST?
					sprintf(cvtbufr,"%d\0",gp_state.cord_fault);	
					IE_load_outbufr_str(cvtbufr);// add data to buffer 
					IE_sep();// terminate query response data with seperator.
				}
				else	
				{// :CORDalarm:TEST x	// {OFF=0.ON=1}.
					if ((unsigned) IE_get_parm_value())
					{
						set_CORD_ALARM();
					}
					else
					{
						clear_CORD_ALARM();
					}
				}
			}

			if(strstr(IE_keyword,"STAT\0")) 	//cord alarm ENABLED/DISABLED state.
			{
				if (QUERY) 
				{//:CORDalarm:STATe?
					sprintf(cvtbufr,"%d\0",alarmState);	
					IE_load_outbufr_str(cvtbufr);// add data to buffer 
					IE_sep();// terminate query response data with seperator.
				}
				else	
				{// :CORDalarm:STATe x
					alarmState = (unsigned) IE_get_parm_value();
					
					if (alarmState==DISABLED)
					{
						clear_CORD_ALARM();

						if (newVoltage != GP_OUTPUT_VOLTAGE)	//recover from droop.
						{
							newVoltage = GP_OUTPUT_VOLTAGE;
							slew_output(0.5, GP_OUTPUT_FREQUENCY, newVoltage);
						}
					}
				}
			}			
			if(strstr(IE_keyword,"RAT\0")) 
			{
				if (QUERY) 
				{//:CORDalarm:RAT?
					sprintf(cvtbufr,"%d\0",shoreCordRating);
					IE_load_outbufr_str(cvtbufr);// add data to buffer 
					IE_sep();// terminate query response data with seperator.
				}
				else	
				{// :CORDalarm:RAT n
					value = IE_get_parm_value();
					if ( (30.0<=value) && (value<=250.0) )
					{
						shoreCordRating = (int) value;
						
						if (gp_state.local_master==SLAVE)
						{
			 				setLoadShare(lastMasterCapacity);
						}
					}
				}
			}
			if(strstr(IE_keyword,"LEV\0")) 	//:LEVel
			{
				if (QUERY) 
				{//:CORDalarm:LEVel?
					sprintf(cvtbufr,"%d\0",alarmLevel);
					IE_load_outbufr_str(cvtbufr);// add data to buffer 
					IE_sep();// terminate query response data with seperator.
				}
				else	
				{// :CORDalarm:LEVel n
					value = IE_get_parm_value();
					if ( (50.0<=value) && (value<=100.0) ) alarmLevel = (int) value;
				}
			}
			return 1;
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////
//MAINTENANCE
	if (strstr(IE_keyword,"MAINTENANCE\0")) 	//:MAINTENANCE?
	{
		if (NEW_G2G_OPTION)
		{
			sprintf(cvtbufr,"%d\0",maintenanceMode);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();						// terminate query response data with seperator.
		}
		return 1;
	}
//BUS
	if (strstr(IE_keyword,"BUS\0")) 	//:BUS?
	{
		if (NEW_G2G_OPTION || MULTIGEN_BESECKE)
			sprintf(cvtbufr,"%d\0",get_busState2());	//range:  -2 <= busState <= 10
		else
			sprintf(cvtbufr,"%d\0",get_busState());
		IE_load_outbufr_str(cvtbufr);	// add data to buffer 
		IE_sep();						// terminate query response data with seperator.
		return 1;
	}
//GENSTAR
	if (strstr(IE_keyword,"GENSTAR\0"))				// does part of string match? 
	{	
		if(strstr(IE_keyword,":\0")) 
		{	
			IE_get_keyword();
		
			if(strstr(IE_keyword,"2ON\0")) 
			{	
				LogEvent(Ev_COMM_GENSTART_ON,0);
				set_GEN2_START_CMD();	//toggle "GEN_START_CMD" signal high then low.
				return 1;
			}
			else if(strstr(IE_keyword,"ON\0")) 	//Legacy 1 genset.
			{	
				LogEvent(Ev_COMM_GENSTART_ON,0);
				set_GEN_START_CMD();
				return 1;
			}

			if(strstr(IE_keyword,"STAT\0")) 
			{
				if (QUERY) 
				{//:GENSTAR:STATe?
					sprintf(cvtbufr,"%d\0",gen_start_cmd_enable);	
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();						// terminate query response data with seperator.
				}
				else	
				{// :GENSTAR:STATe x
					gen_start_cmd_enable = (unsigned) IE_get_parm_value();
				}
				return 1;
			}			
			if(strstr(IE_keyword,"DELAY\0")) 
			{
				if (QUERY) 
				{//:GENSTAR:DELAY?
					sprintf(cvtbufr,"%d\0",generator_on_delay);
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();						// terminate query response data with seperator.
				}
				else	
				{// :GENSTAR:DELAY x
					generator_on_delay = (unsigned) IE_get_parm_value();	/* Get requested zop state. */
				}
				return 1;
			}
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////
//GENSTOP
	if (strstr(IE_keyword,"GENSTOP\0"))				// does part of string match? 
	{	
		if(strstr(IE_keyword,":\0")) 
		{	
			IE_get_keyword();
		
			if(strstr(IE_keyword,"2OFF\0")) 
			{	
//				LogEvent(Ev_COMM_GENSTOP,0);
				set_GEN2_STOP_CMD();	//toggle "GEN2_STOP_CMD" signal high then low.
				return 1;
			}
			else if(strstr(IE_keyword,"OFF\0")) 	//Legacy 1 genset.
			{	
//				LogEvent(Ev_COMM_GENSTOP,0);
				set_GEN_STOP_CMD();		//toggle "GEN_STOP_CMD" signal high then low.
				return 1;
			}
		
			if(strstr(IE_keyword,"STAT\0")) 
			{
				if (QUERY) 
				{//:GENSTOP:STATe?
					sprintf(cvtbufr,"%d\0",gen_stop_cmd_enable);	
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();						// terminate query response data with seperator.
				}
				else	
				{// :GENSTOP:STATe x
					gen_stop_cmd_enable = (unsigned) IE_get_parm_value();
				}
				return 1;
			}			
			if(strstr(IE_keyword,"DELAY\0")) 
			{
				if (QUERY) 
				{//:GENSTOP:DELAY?
					sprintf(cvtbufr,"%d\0",generator_off_delay);
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();						// terminate query response data with seperator.
				}
				else	
				{// :GENSTOP:DELAY x
					generator_off_delay = (unsigned) IE_get_parm_value();	/* Get requested zop state. */
				}
				return 1;
			}
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////
//SHOR
	if (strstr(IE_keyword,"SHOR\0"))		//Converter
	{										
		if (strstr(IE_keyword,":\0")) 
		{
			IE_get_keyword();		
			if(strstr(IE_keyword,"OFF\0")) 
			{
				input_dropped=FALSE;			//v1.97.
				autorestart=DISABLED;
				LogEvent(Ev_COMM_SHORE_OFF,0);
#ifdef TEST_FIXTURE
				_putbit(0,P3,3);
#else
				set_REM_IN_OFF();
#endif //TEST_FIXTURE
				shutoffId = 2;				//reason input is off, remote sp off.
				return 1;
			}
			if(strstr(IE_keyword,"ON\0")) 
			{	
				gp_state.shore_power_viewed = TRUE; // touch SP
				LogEvent(Ev_COMM_SHORE_ON,0);
#ifdef TEST_FIXTURE
				_putbit(1,P3,3);
#else
				set_REM_IN_ON();
#endif //TEST_FIXTURE
				return 1;
			}
		}
	}
//CONV
	if (strstr(IE_keyword,"CONV\0"))		//Converter
	{										
		if (strstr(IE_keyword,":\0")) 
		{
			IE_get_keyword();		

			if(strstr(IE_keyword,"ON\0")) 
			{	
				gp_state.shore_power_viewed = TRUE; // touch SP
				
				LogEvent(Ev_COMM_CONV_ON,0);

				if(gp_state.sysstat!=FAILURE)
				{
					if (NEW_G2G_OPTION && !maintenanceMode && !tieBreaker_CLOSED() && !transfer_in_progress)	//& OPERATOR PRESSED CONVERTER POWER 'ON' BUTTON.
					{
						if (get_busState2()==BsG1AG2B)
							abort_power_on=0;
						else
							abort_power_on = 37;
					}
					else
					{
						if (EXT_CB_OPTION)
						{
							remote_off_flag=NO;
						}
						set_REM_OUT_ON();		//toggle "REM_OUT_ON" signal high then low.
						enable_output();	//level control "REM_OUT_OFF" high=enable output low=disable output.
					}
				}
				return 1;
			}

			if(strstr(IE_keyword,"OFF\0")) 
			{
//				remote_off_flag=YES;

				LogEvent(Ev_COMM_CONV_OFF,0);
#ifdef TEST_FIXTURE
					_putbit(0,P3,2);
#else
					if (EXT_CB_OPTION)
					{			
						if ((GP_CONTROL_WORD & 2) == 2)
						{
							LogEvent(Ev_REM_OUT_OFF,2);
						}

						disable_output();	//level control "REM_OUT_OFF" high=enable output low=disable output.

//						set_EXT_CB_OPEN();		//600ms strobe out of PD13.
//						GP_CONTROL_WORD = GP_CONTROL_WORD & 0xFFFD;		//clear REM_OUT_OFF in image.
//						gp_control_word = GP_CONTROL_WORD;	   		//write GP_CONTROL_WORD.
					}
					else
					{
						disable_output();	// open output contactor.
//				 		output_OFF();
					}

					remote_off_flag=YES;
#endif //~TEST_FIXTURE

				return 1;
			}
		}
	}
//AUTOSTART
	if (strstr(IE_keyword,"AUTOSTART\0"))		//Converter
	{										
		if (strstr(IE_keyword,":\0")) 
		{
			IE_get_keyword();		

			if(strstr(IE_keyword,"ON\0")) 
			{	
				if (gp_state.invstat==ONLINE) autorestart=ON;
//				autorestart=ON;
				return 1;
			}

			if(strstr(IE_keyword,"OFF\0")) 
			{
				autorestart=OFF;
				return 1;
			}

			if(strstr(IE_keyword,"STAT\0")) 
			{
				if (QUERY) 
				{//:AUTOSTART:STATe?
					sprintf(cvtbufr,"%d\0",autorestart);	
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();					// terminate query response data with seperator.
				}
				else	
				{// :AUTOSTART:STATe x
					autorestart = (unsigned) IE_get_parm_value();	/* Get requested zop state. */
				}
				return 1;
			}			
		}
	}
//AUTOSTOP
	if (strstr(IE_keyword,"AUTOSTOP\0"))		//Converter
	{										
		if (strstr(IE_keyword,":\0")) 
		{
			IE_get_keyword();		

			if(strstr(IE_keyword,"ON\0")) 
			{	
				autoShutdownEnable=ON;
				return 1;
			}

			if(strstr(IE_keyword,"OFF\0")) 
			{
				autoShutdownEnable=OFF;
				return 1;
			}

			if(strstr(IE_keyword,"STAT\0")) 
			{
				if (QUERY) 
				{//:AUTOSTOP:STATe?
					sprintf(cvtbufr,"%d\0",autoShutdownEnable);	
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();					// terminate query response data with seperator.
				}
				else	
				{// :AUTOSTOP:STATe x
					autoShutdownEnable = (unsigned) IE_get_parm_value();
				}
				return 1;
			}			
		}
	}
//TS
	if (strstr(IE_keyword,"TS\0"))		//TRANSFER SWITCH
	{										
		forceDefaultDisplayMode();	//force local display out of interactive mode

		if (strstr(IE_keyword,":\0"))
		{
			IE_get_keyword();		

			if (strstr(IE_keyword,"STAT\0")) 
			{	
				IE_load_outbufr_str(getXfrErrCodeDescriptor(last_transfer_status));	// add data to buffer 
				IE_sep();						// terminate query response data with seperator.
				last_transfer_status = (unsigned char) 86;
				return 1;
			}

			if (strstr(IE_keyword,"LXFR\0")) 
			{	
				IE_load_outbufr_str(getXfrErrCodeDescriptor(last_transfer_status));	// add data to buffer 
				IE_sep();						// terminate query response data with seperator.
				return 1;
			}

			if (strstr(IE_keyword,"BUS\0")) 
			{	
				LogEvent(Ev_COMM_XFR_REQUEST,0);
				c2g = (int) IE_get_parm_value();
				return 1;
			}

			if (strstr(IE_keyword,"MODE\0")) 
			{	
				sprintf(cvtbufr,"%u\0",transferMode);	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}

			if (strstr(IE_keyword,"CONV\0")) 
			{	
				if (strstr(IE_keyword,":\0")) IE_get_keyword();	
				if (strstr(IE_keyword,"ON\0")) 
				{									// TRANSFER TO CONVERTER
					LogEvent(Ev_COMM_TS_CONV,0);
#ifdef TEST_FIXTURE
				if (_getbit(P3,3))		//mock input on
				{
					_putbit(1,P3,9);	//MOCK TIP High
					_putbit(1,P3,2);	//MOCK OUTPUT ON
					delay(3000);
					_putbit(0,P3,9);	//MOCK TIP Low
				}
#else
					g2c=1;
#endif //TEST_FIXTURE
					return 1;
				}
			}

			if (strstr(IE_keyword,"GEN\0")) 
			{	
				if (strstr(IE_keyword,":\0")) IE_get_keyword();	
				if(strstr(IE_keyword,"ON\0")) 
				{									// TRANSFER TO GENERATOR
					LogEvent(Ev_COMM_TS_GEN,0);
#ifdef TEST_FIXTURE
					_putbit(0,P3,2);	//MOCK OUTPUT OFF
#else
					c2g=1;
#endif //TEST_FIXTURE
					return 1;
				}			
			}

			if (strstr(IE_keyword,"G1\0")) 
			{	
				if (strstr(IE_keyword,":\0")) IE_get_keyword();	//":MASTER" assumed.

				if (QUERY)	// See if this is a query.
				{								   
					sprintf(cvtbufr,"%d\0",(remote_genset==GEN1));	
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();						// terminate query response data with seperator.
					return 1;
				}
				else							
				{
					LogEvent(Ev_COMM_TS_G1_MAST,0);
					remote_genset = GEN1;
					if (MULTIGEN_OPTION) set_gen_mux(remote_genset);

					if (GEN_AMPS_PRESENT)
					{
						set_Igen_mux(GEN1);
//						set_meter_mux(GEN, GEN_sync);
//						meas_all();
					}
					return 1;
				}
			}

			if (strstr(IE_keyword,"G2\0")) 
			{	
				if (strstr(IE_keyword,":\0")) IE_get_keyword();	//":MASTER" assumed.

				if (QUERY)	// See if this is a query.
				{								   
					sprintf(cvtbufr,"%d\0",(remote_genset==GEN2));	
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();						// terminate query response data with seperator.
					return 1;
				}
				else							
				{
					LogEvent(Ev_COMM_TS_G2_MAST,0);
					remote_genset = GEN2;
					if (MULTIGEN_OPTION)
					{
						set_gen_mux(remote_genset);
					}
					else
					{
						if (GEN_AMPS_PRESENT)
						{
							set_Igen_mux(GEN2);
//							set_meter_mux(SYS, SYS_sync);
//							meas_all();
						}
					}
					return 1;
				}
			}

			if (strstr(IE_keyword,"G3\0")) 
			{	
				if (strstr(IE_keyword,":\0")) IE_get_keyword();	//":MASTER" assumed.

				if (QUERY)	// See if this is a query.
				{								   
					sprintf(cvtbufr,"%d\0",(remote_genset==3));	
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();						// terminate query response data with seperator.
					return 1;
				}
				else							
				{
					remote_genset = 3;
					if (MULTIGEN_OPTION) set_gen_mux(remote_genset);
					return 1;
				}
			}

			if (strstr(IE_keyword,"G4\0")) 
			{	
				if (strstr(IE_keyword,":\0")) IE_get_keyword();	//":MASTER" assumed.

				if (QUERY)	// See if this is a query.
				{								   
					sprintf(cvtbufr,"%d\0",(remote_genset==4));	
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();						// terminate query response data with seperator.
					return 1;
				}
				else							
				{
					remote_genset = 4;
					if (MULTIGEN_OPTION) set_gen_mux(remote_genset);
					return 1;
				}
			}
			return 0;
		}
		return 1;	//return good status for ":TS" which acts like pressing SYSTEM_STATUS key.
	}
//KWH
	if (strstr(IE_keyword,"KWH\0"))		//Event Log
	{										
		if (strstr(IE_keyword,":\0")) 
		{
			IE_get_keyword();
		
			if(strstr(IE_keyword,"CLEAR\0")) 
			{	
				KWH_clear();
				return 1;
			}
		}
	}
//AUX
	if (strstr(IE_keyword,"AUX\0"))		//Auxilliary Min & Max Measurements.
	{										
		if (strstr(IE_keyword,":\0")) 
		{
			IE_get_keyword();
		
			if(strstr(IE_keyword,"CLEAR\0")) 
			{	
				aux_clear();
				return 1;
			}
		}
	}
//LOG
	if (strstr(IE_keyword,"LOG\0"))		//Event Log
	{										
			IE_get_keyword();
		
			if(strstr(IE_keyword,"CLEAR\0")) 
			{	
				initializeEventLog();	//Event Log initialization--call once each full memory reset.
				startEventLog();
				return 1;
			}

			if(strstr(IE_keyword,"COUNT\0")) 
			{									//:LOG:COUNT?
				sprintf(cvtbufr,"%d\0",eventLogCount());	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();						// terminate query response data with seperator.
				return 1;
			}			

			if(strstr(IE_keyword,"MODE\0")) 
			{
				if (QUERY) 
				{//:LOG:MODE?
					sprintf(cvtbufr,"%d\0",remoteEvLogMode);	
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();						// terminate query response data with seperator.
				}
				else	
				{// :LOG:MODE x
					remoteEvLogMode = (unsigned) IE_get_parm_value();	// Get requested zop state.
				}
				return 1;
			}			

			if(strstr(IE_keyword,"DUMP\0")) 
			{	//:LOG:DUMP
				remoteEventLogView = viewFirstEvent();
				marshalEvent(remoteEventLogView);

				count = (unsigned) eventLogCount();

				if (count>100) count = 100;
				else count--;

				for (i=0;i<count;i++)
				{
					sprintf(cvtbufr,"\n\rEvent#%d:\t\0",i+1);	//cvt to ascii & format.
					IE_load_outbufr_str(cvtbufr);				//add to buffer.
					remoteEventLogView = viewNextEvent(remoteEventLogView);
					marshalEvent(remoteEventLogView);
				}
				return 1;
			}			

			if(strstr(IE_keyword,"TOP\0")) 
			{	//:LOG:TOP
				remoteEventLogView = viewFirstEvent();	
				marshalEvent(remoteEventLogView);
				return 1;
			}			

			if(strstr(IE_keyword,"NEXT\0")) 
			{	//:LOG:NEXT
				remoteEventLogView = viewNextEvent(remoteEventLogView);
				marshalEvent(remoteEventLogView);
				return 1;
			}			

			if(strstr(IE_keyword,"PREV\0")) 
			{	//:LOG:PREV
				remoteEventLogView = viewPreviousEvent(remoteEventLogView);
				marshalEvent(remoteEventLogView);
				return 1;
			}			
	}
//AGC
	if (strstr(IE_keyword,"AGC\0"))		//Converter
	{										
		if (strstr(IE_keyword,":\0")) 
		{
			IE_get_keyword();
					
#ifndef MM3			
			if(strstr(IE_keyword,"OFF\0")) 
			{
				AGC_state = DISABLED;
				ALC_mode = AGC_state;
				return 1;
			}

			if(strstr(IE_keyword,"ON\0")) 
			{	
				AGC_state = ENABLED;
				ALC_mode = AGC_state;
				return 1;
			}
#endif

			if(strstr(IE_keyword,"CLEAR\0")) 
			{	
				Va_out=def_pgm.Va * Ka_wf * Kout_A;	/* else restore orig pgm value */
				Vb_out=def_pgm.Vb * Kb_wf * Kout_B;	/* else restore orig pgm value */
				Vc_out=def_pgm.Vc * Kc_wf * Kout_C;			/* else restore orig pgm value */
				scale_volts();
				load_osc_regs();
				return 1;
			}

			if(strstr(IE_keyword,"STAT\0")) 
			{
				if (QUERY) 
				{//:AGC:STATe?
					sprintf(cvtbufr,"%d\0",AGC_state);	
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();					// terminate query response data with seperator.
				}
				return 1;
			}			
		}
	}
//PWM3
	if (strstr(IE_keyword,"PWM3\0"))				// does part of string match? 
	{	
		if(strstr(IE_keyword,":\0")) 
		{	
			IE_get_keyword();

			if(strstr(IE_keyword,"STAT\0")) 
			{
				if (QUERY) 
				{//:IMPED:PWM3:STATe?
					sprintf(cvtbufr,"%d\0",mach.pwm3_state);	
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();						// terminate query response data with seperator.
				}
				else	
				{// :IMPED:PWM3:STATe x
					vtemp = IE_get_parm_value();

					if (valueInRange(vtemp,0,1))
					{
						mach.pwm3_state = (unsigned) vtemp;
					}
					else return 0;
				}
			}			
			if(strstr(IE_keyword,"DUTY\0")) 
			{
				if (QUERY) 
				{//:IMPED:PWM3:DUTY?
					sprintf(cvtbufr,"%d\0",mach.pwm3_dutyCycle);
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();						// terminate query response data with seperator.
				}
				else	
				{// :IMPED:PWM3:DUTY x
					vtemp = IE_get_parm_value();

					if (valueInRange(vtemp,0,100))
					{
						mach.pwm3_dutyCycle = (unsigned) vtemp;
					}
					else return 0;
				}
			}
			if(strstr(IE_keyword,"BITS\0")) 
			{
				if (QUERY) 
				{//:IMPED:PWM3:BITS?
					sprintf(cvtbufr,"%d\0",mach.pwm3_bits);	
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();						// terminate query response data with seperator.
				}
				else	
				{// :IMPED:PWM3:BITS x
					vtemp = IE_get_parm_value();

					if (valueInRange(vtemp,8,16))
					{
						mach.pwm3_bits = (unsigned) vtemp;
					}
					else return 0;
				}
			}
			if(strstr(IE_keyword,"ALIGN\0")) 
			{
				if (QUERY) 
				{//:IMPED:PWM3:ALIGN?
					sprintf(cvtbufr,"%d\0",mach.pwm3_align);	
					IE_load_outbufr_str(cvtbufr);	// add data to buffer 
					IE_sep();						// terminate query response data with seperator.
				}
				else	
				{// :IMPED:PWM3:ALIGN x
					vtemp = IE_get_parm_value();

					if (valueInRange(vtemp,0,1))
					{
						mach.pwm3_align = (unsigned) vtemp;
					}
					else return 0;
				}
			}
		}
  
  		if (mach.pwm3_state)
  			init_PWM3(mach.pwm3_bits,mach.pwm3_align,mach.pwm3_dutyCycle);		//unsigned(bits,align,duty)
  		else
  			init_PWM3(9,0,50);		//unsigned(bits,align,duty)

		return 1;
	}//endif 'PWM3'

//ADCRAT
	if(strstr(IE_keyword,"ADCRAT\0")) 
	{
		if (QUERY) 
		{//:ADCRAT?
			sprintf(cvtbufr,"%u\0",adcRate);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();						// terminate query response data with seperator.
		}
		else	
		{// :ADCRAT x
			vtemp = IE_get_parm_value();

			if (valueInRange(vtemp,1,3))
			{
				adcRate = (unsigned) vtemp;
			}
			else if (valueInRange(vtemp,8,15))
			{
				adcRate = (unsigned) vtemp;
			}
			else return 0;
		}
	}

//TMON
	if(strstr(IE_keyword,"TMON\0")) //Get Monitor Time
	{	
		sprintf(cvtbufr,"%u\0",monitorTime*10);	
		IE_load_outbufr_str(cvtbufr);	// add data to buffer 
		IE_sep();						// terminate query response data with seperator.
		return 1;
	}//endif :TMON

//RCRQST
	if(strstr(IE_keyword,"RCRQST\0")) //RemoteControlReQueST
	{	
		sprintf(cvtbufr,"%u\0",1);	
		IE_load_outbufr_str(cvtbufr);	// add data to buffer 
		IE_sep();						// terminate query response data with seperator.
		return 1;
	}//endif :RCRQST

//BAUD
	if(strstr(IE_keyword,"BAUD\0")) 
	{
		if (QUERY) 
		{//:BAUD?
			sprintf(cvtbufr,"%u\0",ser_baud_rate);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();						// terminate query response data with seperator.
		}
		else	
		{// :BAUD x
			vtemp = IE_get_parm_value();

			if (valueInRange(vtemp,38400,38400))
			{
				ser_baud_rate = (unsigned) vtemp;
			}
			else if (valueInRange(vtemp,19200,19200))
			{
				ser_baud_rate = (unsigned) vtemp;
			}
			else if (valueInRange(vtemp,9600,9600))
			{
				ser_baud_rate = (unsigned) vtemp;
			}
			else if (valueInRange(vtemp,4800,4800))
			{
				ser_baud_rate = (unsigned) vtemp;
			}
			else if (valueInRange(vtemp,2400,2400))
			{
				ser_baud_rate = (unsigned) vtemp;
			}
			else if (valueInRange(vtemp,1200,1200))
			{
				ser_baud_rate = (unsigned) vtemp;
			}
			else if (valueInRange(vtemp,300,300))
			{
				ser_baud_rate = (unsigned) vtemp;
			}
			else return 0;
		}
		return 1;
	}//:BAUD
	
	return 0;				/* parse failed. */

}
///
/* -------------------------------------------------------------------------- */
void forceDefaultDisplayMode()	//force local display out of interactive mode
{
	new_key=18;
	combo_key = 5;
	special_function=0;
//	ALC_mode=AGC_state;	//v1.99
	gp_mode=NORMAL_MODE;	//allow use of HELP key.
	display_type = SYSTEM_STATUS; 
//	ts = TRUE;	//transfer via serial flag.
}
/* -------------------------------------------------------------------------- */
void KWH_clear()
{
	wattTime = 0;
	lastWattTime = 1000;
	wattHours = 0.0f;
	maxLevelIn1 = 0.0f;
	maxPowerIn1 = 0.0f;
	maxSystemLevel = 0.0f;
	maxSystemPower = 0.0f;
}
/* -------------------------------------------------------------------------- */
void aux_clear()
{
//CONVERTER INPUT
	minShorVolts 	= 	600.0;
	maxShorVolts	= 	0.0;
	minShorAmps		=	600.0;
	maxShorAmps		=	0.0;
	minShorFreq		=	100.0;
	maxShorFreq		=	0.0;
//CONVERTER OUTPUT
	minConvVolts 	= 	600.0;
	maxConvVolts	= 	0.0;
	minConvAmps		=	600.0;
	maxConvAmps		=	0.0;
	minConvFreq		=	100.0;
	maxConvFreq		=	0.0;
	maxConvLevel	=	0.0;
	maxConvPower	=	0.0;
//GENERATOR #1
	minGen1Volts 	= 	100.0;
	maxGen1Volts	= 	0.0;
	minGen1Freq		=	100.0;
	maxGen1Freq		=	0.0;
//GENERATOR #2
	minGen2Volts 	= 	600.0;
	maxGen2Volts	= 	0.0;
	minGen2Freq		=	100.0;
	maxGen2Freq		=	0.0;
}
/* -------------------------------------------------------------------------- */
///
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
void float2outbufr(float fnum)
{
	sprintf(cvtbufr,"%3.2f\0",fnum);	// cvt value to ascii
	IE_load_outbufr_str(cvtbufr);	// add data to buffer 
	IE_load_outbufr_str(",\0");		// add data seperator to bufr.
}
/* -------------------------------------------------------------------------- */

/* 65 */
////////////////////////////////////////////////////////////
// function to parse the MEAS CMD
int parse_MEAS()			/* Report stored measured values */
{
int i;
int status=0;
int ovr=0;
char *raw_parm_buf=parm_buf;
char *channel_name=parm_buf;
int gp_channel=-1;		//initialize to invalid channel for parse test.

	if (scpi_parser_reset) IE_get_keyword();		/* get next keyword if parser reset. */

	if (strstr(IE_keyword,"AC\0"))	IE_get_keyword(); // skip AC: to get next keyword. 

	if (strstr(IE_keyword,"MAXRATE\0")) 	//must parse MAXRATE before RATE.
	{	
		value=maxSystemLevel;					
		goto report;		 
	}
	if (strstr(IE_keyword,"AVERRATE\0")) 	//must parse MAXRATE before RATE.
	{	
		value=averSystemLevel;					
		goto report;		 
	}
	if (strstr(IE_keyword,"RATE\0")) 
	{	
		value=system_level;					
		goto report;		 
	}
	if (strstr(IE_keyword,"MAXPOW\0")) 	//must parse MAXPOW before POW.
	{	
		value=maxSystemPower;					
		goto report;		 
	}
	if (strstr(IE_keyword,"AVERPOW\0")) 	//must parse MAXPOW before POW.
	{	
		value=averSystemPower;					
		goto report;		 
	}
	if (strstr(IE_keyword,"POW\0")) 
	{	
		value=system_kw;					
		goto report;		 
	}
	if (strstr(IE_keyword,"KVA\0")) 
	{	
		value=system_kva;					
		goto report;		 
	}
	if (strstr(IE_keyword,"KWH\0")) 
	{	
		value=wattHours;					
		goto report;		 
	}

	if (strstr(IE_keyword,"SP1\0")) gp_channel = SP1;
	else if (strstr(IE_keyword,"SP2\0")) 	gp_channel = SP2;
	else if (strstr(IE_keyword,"SP\0")) 	gp_channel = SP1;
	else if (strstr(IE_keyword,"GEN1\0"))
	{
		gp_channel = GEN;
		if (GEN_AMPS_PRESENT)
		{
			set_Igen_mux(GEN1);
//			set_meter_mux(GEN, GEN_sync);

//			meas_all();
		}
	}
	else if (strstr(IE_keyword,"GEN2\0"))
	{
		gp_channel = SYS;
		if (GEN_AMPS_PRESENT && !MULTIGEN_OPTION)
		{
			set_Igen_mux(GEN2);
//			set_meter_mux(SYS, SYS_sync);
//			meas_all();
		}
	}
	else if (strstr(IE_keyword,"GEN\0"))
	{
		if (SWITCHGEAR_INTERFACE_OPTION || MULTIGEN_BESECKE)
		{
			set_gen_mux(remote_genset);
			meas_all();

			switch (remote_genset)
			{
				case GEN1:
							gp_channel = GEN;
							break;
				case GEN2:
							gp_channel = SYS;
							break;
				case GEN3:
							gp_channel = GEN;
							break;
				case GEN4:
							gp_channel = SYS;
							break;
				default:
							return 0;
			}
		}
		else
		{
			gp_channel = GEN;
			if (GEN_AMPS_PRESENT)
			{
				set_Igen_mux(GEN1);
			}
		}
	}
	else if (strstr(IE_keyword,"CONV\0")) gp_channel = OSC;
	else if (strstr(IE_keyword,"EXT\0")) 	gp_channel = EXT;
	else if (strstr(IE_keyword,"AUX\0"))
///////////////////////
// process AUX
//////////////////////
	{
		gp_channel = AUX;
		if (strstr(IE_keyword,":\0")) 
		{	
			IE_get_keyword();
			if (strstr(IE_keyword,"TEMP\0")) 
			{	
				value=system_temperature;					
				goto report;		
			}
			if (strstr(IE_keyword,"PHVDC\0")) 
			{	
				value=positive_hvdc;					
				goto report;		
			}
			if (strstr(IE_keyword,"NHVDC\0")) 
			{	
				value=negative_hvdc;					
				goto report;		
			}
		}
	}
///////////////////////
// end of AUX
//////////////////////
	else gp_channel = -1;


	if (gp_channel > -1 && gp_channel < AUX)		//THEN PROCESS VALID gp_channel
	{
		if (strstr(IE_keyword,":\0")) IE_get_keyword();	

		if (strstr(IE_keyword,"WF\0")) 
		{
			if (strstr(IE_keyword,":\0")) IE_get_keyword();	
			if (strstr(IE_keyword,"VOLT\0")) 
			{
				if (strstr(IE_keyword,"1\0")) 
				{	
					for (i=0;i<waveform_sample_count[gp_channel][A_VOLTS];i++)
					{
						float2outbufr(waveform[gp_channel][A_VOLTS][i]);
					}
					IE_sep();					/* terminate query response data with seperator. */
					return 1;
				}
				if (strstr(IE_keyword,"2\0")) 
				{	
					for (i=0;i<waveform_sample_count[gp_channel][B_VOLTS];i++)
					{
						float2outbufr(waveform[gp_channel][B_VOLTS][i]);
					}
					IE_sep();					/* terminate query response data with seperator. */
					return 1;
				}
				if (strstr(IE_keyword,"3\0")) 
				{	
					for (i=0;i<waveform_sample_count[gp_channel][C_VOLTS];i++)
					{
						float2outbufr(waveform[gp_channel][C_VOLTS][i]);
					}
					IE_sep();					/* terminate query response data with seperator. */
					return 1;
				}
			}
			if (strstr(IE_keyword,"CURR\0")) 
			{
				if (strstr(IE_keyword,"1\0")) 
				{	
					for (i=0;i<waveform_sample_count[gp_channel][A_AMPS];i++)
					{
						float2outbufr(waveform[gp_channel][A_AMPS][i]);
					}
					IE_sep();					/* terminate query response data with seperator. */
					return 1;
				}
				if (strstr(IE_keyword,"2\0")) 
				{	
					for (i=0;i<waveform_sample_count[gp_channel][B_AMPS];i++)
					{
						float2outbufr(waveform[gp_channel][B_AMPS][i]);
					}
					IE_sep();					/* terminate query response data with seperator. */
					return 1;
				}
				if (strstr(IE_keyword,"3\0")) 
				{	
					for (i=0;i<waveform_sample_count[gp_channel][C_AMPS];i++)
					{
						float2outbufr(waveform[gp_channel][C_AMPS][i]);
					}
					IE_sep();					/* terminate query response data with seperator. */
					return 1;
				}
			}

			if(strstr(IE_keyword,"SPC\0"))
			{
				sprintf(cvtbufr,"%u\0",waveform_sample_count[gp_channel][A_VOLTS]);
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();						// terminate query response data with seperator.
				return 1;
			}
		}
//
//:MEAS:channel:HARMONICS
//
#ifdef MY_DFT
		if (strstr(IE_keyword,"HARMONICS\0"))
		{
			return parse_HARMONICS(gp_channel);
		}
#endif // MY_DFT
		
		if (strstr(IE_keyword,"FREQ\0")) 
		{	
			value=gp_Freq[gp_channel];		
			goto report;		
		}

		if (strstr(IE_keyword,"VOLT\0")) 
		{
			if (strstr(IE_keyword,"1\0")) 
			{	
				value=gp_Van[gp_channel];			
//				if(gp_OverRange[gp_channel][A_VOLTS]) ovr = 1;
				goto report;		
			}
			if (strstr(IE_keyword,"2\0")) 
			{	
				value=gp_Vbn[gp_channel];			
//				if(gp_OverRange[gp_channel][B_VOLTS]) ovr = 1;
				goto report;		
			}
			if (strstr(IE_keyword,"3\0")) 
			{	
				value=gp_Vcn[gp_channel];
//				if(gp_OverRange[gp_channel][C_VOLTS]) ovr = 1;			
				goto report;		
			}
		}

		if (strstr(IE_keyword,"KVAR1\0")) 
		{	
			value=sqrt((gp_KVAa[gp_channel]*gp_KVAa[gp_channel])-(gp_KWa[gp_channel]*gp_KWa[gp_channel]));	
			sprintf(cvtbufr,"%3.2f\0",value);	/* replaces gcvt() */
			IE_load_outbufr_str(cvtbufr);	/* add data to buffer */
			IE_sep();					/* terminate query response data with seperator. */
			return 1;
		}
		if (strstr(IE_keyword,"KVAR2\0")) 
		{	
			value=sqrt((gp_KVAb[gp_channel]*gp_KVAb[gp_channel])-(gp_KWb[gp_channel]*gp_KWb[gp_channel]));	
			sprintf(cvtbufr,"%3.2f\0",value);	/* replaces gcvt() */
			IE_load_outbufr_str(cvtbufr);	/* add data to buffer */
			IE_sep();					/* terminate query response data with seperator. */
			return 1;
		}
		if (strstr(IE_keyword,"KVAR3\0")) 
		{	
			value=sqrt((gp_KVAc[gp_channel]*gp_KVAc[gp_channel])-(gp_KWc[gp_channel]*gp_KWc[gp_channel]));
			sprintf(cvtbufr,"%3.2f\0",value);	/* replaces gcvt() */
			IE_load_outbufr_str(cvtbufr);	/* add data to buffer */
			IE_sep();					/* terminate query response data with seperator. */
			return 1;
		}

		if (strstr(IE_keyword,"VLL1\0")) 
		{	
			value=gp_Vab[gp_channel];	
			goto report;		
		}
		if (strstr(IE_keyword,"VLL2\0")) 
		{	
			value=gp_Vbc[gp_channel];	
			goto report;		
		}
		if (strstr(IE_keyword,"VLL3\0")) 
		{	
			value=gp_Vca[gp_channel];
			goto report;		
		}

		if (strstr(IE_keyword,"CURR\0")) { 
			if (strstr(IE_keyword,":\0")) {
				IE_get_keyword();

				if (strstr(IE_keyword,"PEAK1\0")) 
				{	value=gp_Ia_pk[gp_channel];				
					goto report;		
				}
				if (strstr(IE_keyword,"PEAK2\0")) 
				{	value=gp_Ib_pk[gp_channel];				
					goto report;		
				}
				if (strstr(IE_keyword,"PEAK3\0")) 
				{	value=gp_Ic_pk[gp_channel];				
					goto report;		
				}
				if (strstr(IE_keyword,"CREST1\0")) 
				{	value=gp_ICFa[gp_channel];				
					goto report;		
				}
				if (strstr(IE_keyword,"CREST2\0")) 
				{	value=gp_ICFb[gp_channel];				
					goto report;		
				}
				if (strstr(IE_keyword,"CREST3\0")) 
				{	value=gp_ICFc[gp_channel];				
					goto report;		
				}
			} else {
				if (strstr(IE_keyword,"1\0")) 
				{	value=gp_Ia_rms[gp_channel];	
//					if(gp_OverRange[gp_channel][A_AMPS]) ovr = 1;
					goto report;		
				}
				if(strstr(IE_keyword,"2\0")) 
				{	value=gp_Ib_rms[gp_channel];				
//					if(gp_OverRange[gp_channel][B_AMPS]) ovr = 1;
					goto report;		
				}
				if (strstr(IE_keyword,"3\0")) 
				{	value=gp_Ic_rms[gp_channel];					
//					if(gp_OverRange[gp_channel][C_AMPS]) ovr = 1;
					goto report;		
				}
			}
		}
		if (strstr(IE_keyword,"KVA1\0")) 
		{	
			value=gp_KVAa[gp_channel];					
			goto report;		
		}
		if (strstr(IE_keyword,"KVA2\0")) 
		{	
			value=gp_KVAb[gp_channel];					
			goto report;		
		}
		if (strstr(IE_keyword,"KVA3\0")) 
		{	
			value=gp_KVAc[gp_channel];					
			goto report;		
		}
		if (strstr(IE_keyword,"POW\0")) 
		{	
			if (strstr(IE_keyword,"1\0")) 
			{	value=gp_KWa[gp_channel];				
					goto report;		
			} 
			if (strstr(IE_keyword,"2\0")) 
			{	value=gp_KWb[gp_channel];				
					goto report;		
			} 
			if (strstr(IE_keyword,"3\0")) 
			{	value=gp_KWc[gp_channel];				
					goto report;		
			} 
		}
		if (strstr(IE_keyword,"PF1\0")) 
		{	
			value=gp_PFa[gp_channel];					
			goto report;		
		}
		if (strstr(IE_keyword,"PF2\0")) 
		{	
			value=gp_PFb[gp_channel];					
			goto report;		
		}		
		if (strstr(IE_keyword,"PF3\0")) 
		{	
			value=gp_PFc[gp_channel];					
			goto report;		
		}
		if (strstr(IE_keyword,"FORM\0")) 
		{	
			if (gp_channel==SP1)
				value=(float)gp_state.input1_form;
			else
				value=(float)GP_OUTPUT_FORM;
			goto report;		
		}
		if (strstr(IE_keyword,"RATE1\0")) 
		{	
			value=(float)gp_level_a[gp_channel];					
			goto report;		 
		}
		if (strstr(IE_keyword,"RATE2\0")) 
		{	
			value=(float)gp_level_b[gp_channel];					
			goto report;		 
		}
		if (strstr(IE_keyword,"RATE3\0")) 
		{	
			value=(float)gp_level_c[gp_channel];					
			goto report;		 
		}

		if (strstr(IE_keyword,"ALL\0")) 
		{	
			if (gp_channel==SP1)
			{
				float2outbufr(gp_Vab[gp_channel]);
				float2outbufr(gp_Vbc[gp_channel]);
				float2outbufr(gp_Vca[gp_channel]);
				float2outbufr(gp_Ia_rms[gp_channel]);
				float2outbufr(gp_Ib_rms[gp_channel]);
				float2outbufr(gp_Ic_rms[gp_channel]);
				float2outbufr(gp_Freq[gp_channel]);
			}
			else if (gp_channel==OSC)
			{
				float2outbufr(gp_Van[gp_channel]);
				float2outbufr(gp_Vbn[gp_channel]);
				float2outbufr(gp_Vcn[gp_channel]);
				float2outbufr(gp_Ia_rms[gp_channel]);
				float2outbufr(gp_Ib_rms[gp_channel]);
				float2outbufr(gp_Ic_rms[gp_channel]);
				float2outbufr(gp_Freq[gp_channel]);
			}
			else if (gp_channel==GEN)
			{
				float2outbufr(gp_Van[gp_channel]);
				float2outbufr(gp_Vbn[gp_channel]);
				float2outbufr(gp_Vcn[gp_channel]);
				float2outbufr(gp_Freq[gp_channel]);
				float2outbufr(gp_Van[SYS]);
				float2outbufr(gp_Vbn[SYS]);
				float2outbufr(gp_Vcn[SYS]);
				float2outbufr(gp_Freq[SYS]);
			}

			IE_sep();					/* terminate query response data with seperator. */
			return 1;
		}

		if (strstr(IE_keyword,"CR1\0")) 
		{	
			if (gp_channel==SP1)
			{
				crfloat2outbufr(gp_Vab[gp_channel]);
				crfloat2outbufr(gp_Vbc[gp_channel]);
				crfloat2outbufr(gp_Vca[gp_channel]);
				crfloat2outbufr(gp_Ia_rms[gp_channel]);
				crfloat2outbufr(gp_Ib_rms[gp_channel]);
				crfloat2outbufr(gp_Ic_rms[gp_channel]);
				crfloat2outbufr(gp_KWa[gp_channel]);
				crfloat2outbufr(gp_KWb[gp_channel]);
				crfloat2outbufr(gp_KWc[gp_channel]);
				crfloat2outbufr(gp_Freq[gp_channel]*10.0);
			}
			else if (gp_channel==OSC)
			{
				crfloat2outbufr(gp_Van[gp_channel]);
				crfloat2outbufr(gp_Vbn[gp_channel]);
				crfloat2outbufr(gp_Vcn[gp_channel]);
				crfloat2outbufr(gp_Ia_rms[gp_channel]);
				crfloat2outbufr(gp_Ib_rms[gp_channel]);
				crfloat2outbufr(gp_Ic_rms[gp_channel]);
				crfloat2outbufr(gp_KWa[gp_channel]);
				crfloat2outbufr(gp_KWb[gp_channel]);
				crfloat2outbufr(gp_KWc[gp_channel]);
				crfloat2outbufr(gp_Freq[gp_channel]*10.0);
			}
			else if (gp_channel==GEN)
			{
				crfloat2outbufr(gp_Van[gp_channel]);
				crfloat2outbufr(gp_Vbn[gp_channel]);
				crfloat2outbufr(gp_Vcn[gp_channel]);
				crfloat2outbufr(gp_Freq[gp_channel]*10.0);
				crfloat2outbufr(gp_Van[SYS]);
				crfloat2outbufr(gp_Vbn[SYS]);
				crfloat2outbufr(gp_Vcn[SYS]);
				crfloat2outbufr(gp_Freq[SYS]*10.0);
			}

			IE_sep();					/* terminate query response data with seperator. */
			return 1;
		}


	}
////////////////////////////////////////////////////
// end of MEAS parse for GP
////////////////////////////////////////////////////

		return 0;						/* parse failed. */

report:
		sprintf(cvtbufr,"%3.1f\0",value);	/* replaces gcvt() */
		IE_load_outbufr_str(cvtbufr);	/* add data to buffer */
		IE_sep();					/* terminate query response data with seperator. */

		return 1;

}
/* -------------------------------------------------------------------------- */
void crfloat2outbufr(float fnum)
{
  unsigned unum=(unsigned)fnum+0.5;	//round and convert float fnum to unsigned unum.

	sprintf(cvtbufr,"%03u\0",unum);	// cvt value to ascii
	IE_load_outbufr_str(cvtbufr);	// add data to buffer 
}
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* 67 */
////////////////////////////////////////////////////
//
void STAT_update()		/* update :STATUS:OPER:CONDition reg. */
{
#ifdef STB_output_state
unsigned contactor_state;
#endif //STB_output_state
unsigned int *addr = &STAT_OPER_EVENT_temp.tag-1;	/* get address of bit struct */
	
	*addr=STAT_OPER_EVENT;		/* store 16 bit int into struct. */

#ifndef PAC
	if (_getbit(P5,10))					/* test SHUTDOWN term */
	{
		if (STB.shutdown==0)			/* skip if STB bit already set. */
		{
			STB.shutdown=1;			/* set bit if shutdown */
//			IE_Service_Request();		/* Set status byte and do RQS if MSS true. */
			ESR.dde = 1;
			strncpy(ERROR_DESC,"POWER SOURCE OUTPUT IS IN SHUTDOWN MODE.\0",40);
			IE_esr_report();
		}
	}
	else
#endif //~PAC								/* if pgm operate mode, set pgm running bit. */
	{

//		if (display_no<0 && STB.shutdown) display_no=0;

		STB.shutdown=0;	/* clr if not since STB bits are not latched events. */
		IE_load_serpoll();	/* Load status in to serial poll reg. */
	}

#ifndef PAC
#ifdef STB_output_state
	contactor_state = 1 ^ _getbit(P5,10);					// SHUTDOWN NOT
//	contactor_state = contactor_state & Output_ONOFF; 		// AND OUTPUT_ON

	if (contactor_state)		                // test OUTPUT CONTACTOR term.
	{
		if (STB.output_state==0)	        // skip if STB bit already set.
		{
			STB.output_state=1;	        // set bit if shutdown.
			IE_Service_Request();	        // Set status byte and do RQS if MSS true.
		}
	}
	else
	{
   		STB.output_state=0;	                // clr if not since STB bits are not latched events.
		IE_load_serpoll();	                // Load OUTPUT CONTACTOR status in to serial poll reg.
	}
#endif //STB_output_state
#endif //~PAC								/* if pgm operate mode, set pgm running bit. */

	if (exec_flag)
	{
		if (STAT_OPER_EVENT_temp.program_running==0)	/* skip if EVENT bit already set. */
		{
			STAT_OPER_COND.program_running=1;
			scpi_STAT_OPER_EVENT_report();	/* report :STATus:OPERation:EVENt? reg */
		}
	}
	else STAT_OPER_COND.program_running=0;		/* else clr CONDition. */
}

/* -------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------*/
////////////////////////////////////////////////////////////
// handler for ROUT commends 
int parse_ROUT()		//Report stored measured values */
{
	if (scpi_parser_reset) IE_get_keyword();	// get next keyword if parser reset. 
	/////////////////////////////
	/////////////////////////
	// SP2
	if (strstr(IE_keyword,"SP2\0"))				// does part of string match? 
	{	
		if(strstr(IE_keyword,":\0")) 
		{	
			IE_get_keyword();
			if(strstr(IE_keyword,"XPT\0")) {
				sprintf(cvtbufr,"%d\0",gp_state.scr_mode_id);	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
		}
	}

	if (strstr(IE_keyword,"BUS\0")) 	//:BUSSTAT?
	{//:BUSSTAT?
		if (NEW_G2G_OPTION || MULTIGEN_BESECKE)
			sprintf(cvtbufr,"%d\0",get_busState2());
		else
			sprintf(cvtbufr,"%d\0",get_busState());
		IE_load_outbufr_str(cvtbufr);	// add data to buffer 
		IE_sep();						// terminate query response data with seperator.
		return 1;
	}

	if (strstr(IE_keyword,"RANG\0"))				// does part of string match? 
	{	
		if(strstr(IE_keyword,":\0")) 
		{	
			IE_get_keyword();
			if(strstr(IE_keyword,"VAL\0")) {
				sprintf(cvtbufr,"%d\0",gp_state.range_mode_id);	
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				IE_sep();					// terminate query response data with seperator.
				return 1;
			}
		}
	}
	return 0;								/* parse failed. */

}
//==========================================================================
//==========================================================================
// code below modified or added for v1.06 and UPC-1T/IEC jan-feb 1996, dave
//==========================================================================
//==========================================================================
/* -------------------------------------------------------------------------- */
char *IE_get_keyword()		//get pointer to SCPI keyword.
{
//	IE_keyword=NULL;
	IE_get(IE_keyword,IE_KEYWORD_MAXSIZE);
	return IE_keyword;
}
/* ----------------------------------------------------------------------------------------------------- */
char *IE_get_parm_name()	//get pointer to SCPI string parameter.
{
	IE_get(IE_parm_name,IE_PARM_MAXSIZE);
	return IE_parm_name;
}
/* ----------------------------------------------------------------------------------------------------- */
float IE_get_parm_value()	//destringify a SCPI numeric parameter.
{
  int i;

	IE_get(IE_parm_data,IE_PARM_MAXSIZE);

	if (strstr(IE_parm_data,"OFF\0")) 	IE_parm_value=0.0;		/* set value to 0 if so. */
	else if (strstr(IE_parm_data,"ON\0")) IE_parm_value=1.0;		/* set value to 1 if so. */
	else
	{
		strcpy(IE_cvt_bufr,IE_parm_data);		/* copy string to working bufr. */
		for (i=0; i<sizeof(IE_parm_data); i++)
		{
			if ((IE_cvt_bufr[i] == 'W') || (IE_cvt_bufr[i] == 'F'))	/* replace W or F w/space. */
			{
				IE_cvt_bufr[i]=' ';
			}
		}
		IE_parm_value = atof(IE_cvt_bufr);	/* cvt to float value. */
	}

 	value = IE_parm_value;  //added for use with external_reference MOD6109

	return IE_parm_value;
}
/*--------------------------------------------------------------------------*/
															 //make this inline or a macro!
int white_space(char c) {return ( (c < 33) && (c != 10) );}	/* see if white space. */
//This function added during IEC redesign to simplify code, dave 1-5-95.
/*--------------------------------------------------------------------------*/

void IE_get(char IE_buf[], int buf_size)			//upciepar.c function added during IEC redesign to simplify code, dave 1-5-95.
{
int n=0;
unsigned char more=1;

	while (white_space(IE_inbufr[IE_inptr])) IE_inptr++;	//ignore white spaces

	while ( (n < (buf_size-1)) && more)		//while room in buffer and more info
	{
		switch (IE_buf[n] = IE_inbufr[IE_inptr++])	//check for other sentinals
		{
			case ':' :	if (n>1)	// keyword seperator
						more=0;
                        		break;
			case ',' :	more=0;		// data seperator
					break;
			case ';' :	more=0;		// pmu seperator
					break;
			case '\n':	IE_inptr--;	// pgm msg terminator
					more=0;
					break;
                        default  :	break;
		}
		if (white_space(IE_buf[n++]))	//increment pointer and look for a white space sentinal
			more=0;
	}
	IE_buf[n] = NULL;	// terminate buffer with NULL
}
/*--------------------------------------------------------------------------*/
void IE_scan_to_end()
{							/* Inc IE_inptr till ; or /n found. */
unsigned char c;

	if ( IE_inbufr[IE_inptr-1] == ';') return;	/* if inptr already past end of cmd, ret. */

scan:
	c=IE_inbufr[IE_inptr];	/* get byte. */
	if (c == *";\0")
	{	c=0;						/* ret if pmu seperator. */
		IE_inptr++;			/* point to next byte */
	}
	if (c == *"\n\0") c=0;	/* ret if pgm msg terminator. */
	if (c==0)		return;		/* if terminator or seperator found, */
	IE_inptr++;				/* point to next byte to test. */
	goto scan;
}

/*--------------------------------------------------------------------------*/
/* -------------------------------------------------------------------------- */
int parse_SOUR_IMPED()
{
//float temp_float;

	if (scpi_parser_reset) IE_get_keyword();	/* get next keyword if parser reset. */

	{//not :SOURce:IMPEDance?
		if (strstr(IE_keyword,":\0")) /* is there another keyword?, if so, */
		{//:SOURce:IMPEDance:[CAL || STATe || RESet || LIM:]
			IE_get_keyword();	/* get next keyword(:CALibrate, :STATe or nn.nnn) */

			if (strstr(IE_keyword,"PWM2\0"))				// does part of string match? 
			{	
				if(strstr(IE_keyword,":\0")) 
				{	
					IE_get_keyword();

					if(strstr(IE_keyword,"STAT\0")) 
					{
						if (QUERY) 
						{//:IMPED:PWM2:STATe?
							sprintf(cvtbufr,"%d\0",mach.pwm2_state);	
							IE_load_outbufr_str(cvtbufr);	// add data to buffer 
							IE_sep();					// terminate query response data with seperator.
						}
						else	
						{// :IMPED:PWM2:STATe x
							mach.pwm2_state = (unsigned) IE_get_parm_value();	/* Get requested zop state. */
						}
					}			
					if(strstr(IE_keyword,"DUTY\0")) 
					{
						if (QUERY) 
						{//:IMPED:PWM2:DUTY?
							sprintf(cvtbufr,"%d\0",mach.pwm2_dutyCycle);
							IE_load_outbufr_str(cvtbufr);	// add data to buffer 
							IE_sep();					// terminate query response data with seperator.
						}
						else	
						{// :IMPED:PWM2:DUTY x
							mach.pwm2_dutyCycle = (unsigned) IE_get_parm_value();	/* Get requested zop state. */
						}
					}
					if(strstr(IE_keyword,"BITS\0")) 
					{
						if (QUERY) 
						{//:IMPED:PWM2:BITS?
							sprintf(cvtbufr,"%d\0",mach.pwm2_bits);	
							IE_load_outbufr_str(cvtbufr);	// add data to buffer 
							IE_sep();					// terminate query response data with seperator.
						}
						else	
						{// :IMPED:PWM2:BITS x
							mach.pwm2_bits = (unsigned) IE_get_parm_value();	/* Get requested zop state. */
						}
					}
					if(strstr(IE_keyword,"ALIGN\0")) 
					{
						if (QUERY) 
						{//:IMPED:PWM2:ALIGN?
							sprintf(cvtbufr,"%d\0",mach.pwm2_align);	
							IE_load_outbufr_str(cvtbufr);	// add data to buffer 
							IE_sep();					// terminate query response data with seperator.
						}
						else	
						{// :IMPED:PWM2:ALIGN x
							mach.pwm2_align = (unsigned) IE_get_parm_value();	/* Get requested zop state. */
						}
					}
				}
		  
		  		if (mach.pwm2_state)
		  			init_PWM2(mach.pwm2_bits,mach.pwm2_align,mach.pwm2_dutyCycle);		//unsigned(bits,align,duty)
		  		else
		  			init_PWM2(9,0,50);		//unsigned(bits,align,duty)

				return 1;
			}

		}//end  :SOUR:IMPED:keyword

		return 1;						/* ret OK even if range err as error is flagged in calc_zop(). */
	} //end :IMPED XX (not a query)
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
//GMM v1.0 code for Remote Calibration modified for MM3
//////////////////////////////////////////////////////////////////////////////////////////////////////
/* -------------------------------------------------------------------------- */
//////////////////////////////////////////////////////////////////////////////////////////////////////
//	int parse_CAL()		//return=1=successful, return=0=failure=command_error.
// 						//Set or Query Kfactor (:CAL:xxxx).
//						//Use to Upload & Download Kfactors during firmware upgrade procedure.
// Note: calculated Kfactor must be within 20% of nominal otherwise a command error is flagged.
//////////////////////////////////////////////////////////////////////////////////////////////////////
int parse_CAL()						// CALIBRATION control functions
{
  char *kformat="%5.3f";

	if (scpi_parser_reset) IE_get_keyword();	// get next keyword if parser reset.

	if(strstr(IE_keyword,"XREF")) 
	{
		return parse_CAL_XREF();
	}
/*
	if(strstr(IE_keyword,"VSP1")) 
	{
		if (QUERY) 
		{//:CAL:VSP1?
			sprintf(cvtbufr,kformat,Kmeter_Vsp1_A);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		else	
		{// :CAL:VSP1 x
			value = IE_get_parm_value();
			if ((MIN_KFACTOR<value) && (value<=MAX_KFACTOR))  Kmeter_Vsp1_A = value;
			else return 0;
		}
		return 1;
	}

	if(strstr(IE_keyword,"ISP1")) 
	{
		if (QUERY) 
		{//:CAL:ISP1?
			sprintf(cvtbufr,kformat,Kmeter_Isp1_A);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		else	
		{// :CAL:ISP1 x
			value = IE_get_parm_value();
			if ((MIN_KFACTOR<value) && (value<=MAX_KFACTOR))  Kmeter_Isp1_A = value;
			else return 0;
		}
		return 1;
	}

	if(strstr(IE_keyword,"VSP2")) 
	{
		if (QUERY) 
		{//:CAL:VSP2?
			sprintf(cvtbufr,kformat,Kmeter_Vsp1_B);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		else	
		{// :CAL:VSP2 x
			value = IE_get_parm_value();
			if ((MIN_KFACTOR<value) && (value<=MAX_KFACTOR))  Kmeter_Vsp1_B = value;
			else return 0;
		}
		return 1;
	}

	if(strstr(IE_keyword,"ISP2")) 
	{
		if (QUERY) 
		{//:CAL:ISP2?
			sprintf(cvtbufr,kformat,Kmeter_Isp1_B);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		else	
		{// :CAL:ISP2 x
			value = IE_get_parm_value();
			if ((MIN_KFACTOR<value) && (value<=MAX_KFACTOR))  Kmeter_Isp1_B = value;
			else return 0;
		}
		return 1;
	}

	if(strstr(IE_keyword,"VSP3")) 
	{
		if (QUERY) 
		{//:CAL:VSP3?
			sprintf(cvtbufr,kformat,Kmeter_Vsp1_C);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		else	
		{// :CAL:VSP3 x
			value = IE_get_parm_value();
			if ((MIN_KFACTOR<value) && (value<=MAX_KFACTOR))  Kmeter_Vsp1_C = value;
			else return 0;
		}
		return 1;
	}

	if(strstr(IE_keyword,"ISP3")) 
	{
		if (QUERY) 
		{//:CAL:ISP3?
			sprintf(cvtbufr,kformat,Kmeter_Isp1_C);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		else	
		{// :CAL:ISP3 x
			value = IE_get_parm_value();
			if ((MIN_KFACTOR<value) && (value<=MAX_KFACTOR))  Kmeter_Isp1_C = value;
			else return 0;
		}
		return 1;
	}
*/

	if(strstr(IE_keyword,"VOLT1")) 
	{
		if (QUERY) 
		{//:CAL:VOLT1?
			sprintf(cvtbufr,kformat,Kmeter_Vint_A);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		else	
		{// :CAL:VOLT1 x
			value = IE_get_parm_value();
			if ((MIN_KFACTOR<value) && (value<=MAX_KFACTOR))  Kmeter_Vint_A = value;
			else return 0;
		}
		return 1;
	}

	if(strstr(IE_keyword,"AMP1")) 
	{
		if (QUERY) 
		{//:CAL:AMP1?
			sprintf(cvtbufr,kformat,Kmeter_I_A);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		else	
		{// :CAL:AMP1 x
			value = IE_get_parm_value();
			if ((MIN_KFACTOR<value) && (value<=MAX_KFACTOR))  Kmeter_I_A = value;
			else return 0;
		}
		return 1;
	}

	if(strstr(IE_keyword,"VOLT2")) 
	{
		if (QUERY) 
		{//:CAL:VOLT2?
			sprintf(cvtbufr,kformat,Kmeter_Vint_B);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		else	
		{// :CAL:VOLT2 x
			value = IE_get_parm_value();
			if ((MIN_KFACTOR<value) && (value<=MAX_KFACTOR))  Kmeter_Vint_B = value;
			else return 0;
		}
		return 1;
	}

	if(strstr(IE_keyword,"AMP2")) 
	{
		if (QUERY) 
		{//:CAL:AMP2?
			sprintf(cvtbufr,kformat,Kmeter_I_B);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		else	
		{// :CAL:AMP2 x
			value = IE_get_parm_value();
			if ((MIN_KFACTOR<value) && (value<=MAX_KFACTOR))  Kmeter_I_B = value;
			else return 0;
		}
		return 1;
	}

	if(strstr(IE_keyword,"VOLT3")) 
	{
		if (QUERY) 
		{//:CAL:VOLT3?
			sprintf(cvtbufr,kformat,Kmeter_Vint_C);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		else	
		{// :CAL:VOLT3 x
			value = IE_get_parm_value();
			if ((MIN_KFACTOR<value) && (value<=MAX_KFACTOR))  Kmeter_Vint_C = value;
			else return 0;
		}
		return 1;
	}

	if(strstr(IE_keyword,"AMP3")) 
	{
		if (QUERY) 
		{//:CAL:AMP3?
			sprintf(cvtbufr,kformat,Kmeter_I_C);
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 
			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		else	
		{// :CAL:AMP3 x
			value = IE_get_parm_value();
			if ((MIN_KFACTOR<value) && (value<=MAX_KFACTOR))  Kmeter_I_C = value;
			else return 0;
		}
		return 1;
	}

	return 0;
}
/*--------------------------------------------------------------------------*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
//	int parse_CAL_XREF()	//return=1=successful, return=0=failure=command_error.
// 							//Set a Kfactors by EXTERNALLY REFERENCED VALUE (:CAL:XREF:xxxx).
//		Note: Input must be online to calibrate Shore Power Current and
//			  Output must be online to calibrate Converter Current.
//////////////////////////////////////////////////////////////////////////////////////////////////////
int parse_CAL_XREF()        //return=1=successful, return=0=failure=command_error.
{
    int parse_status=0;         //parse_CAL_VAL status to return to calling function. //return=1=successful, return=0=failure=command_error.
    float external_reference=0.0;
    float dummy=0.0;

//	check_gp_status();
//	if (gp_state.sysstat) return (parse_status=0);	//bad status, so, error.

	if (strstr(IE_keyword,":"))	/* is there another keyword?, if so, */
	{
		IE_get_keyword();		/* get next keyword */

		if (strstr(IE_keyword,"VOLT"))  //IS THE KEYWORD A VALID "TERM" FOR KFACTOR CALIBRATION...
		{
  			parse_status = 1;               //YES, SUCCESSFULLY PARSED A VALID "TERM"
	  		external_reference = IE_get_parm_value();	//value in unadjusted AMPS to VOLTS.

         	if (external_reference==0.0) external_reference=1.0;

			if (strstr(IE_keyword,"3"))						// Generator Phase C
			{
				parse_status = process_ext_ref_input(
										&external_reference,
					        	  		&gp_Vcn[OSC],
			        					&Kmeter_Vint_C);
			}
			else if (strstr(IE_keyword,"2"))				// Generator Phase B
			{
				parse_status = process_ext_ref_input(
										&external_reference,
					        	  		&gp_Vbn[OSC],
			        					&Kmeter_Vint_B);
			}
			else if (strstr(IE_keyword,"1"))				// Generator Phase A
			{
				parse_status = process_ext_ref_input(
										&external_reference,
					        	  		&gp_Van[OSC],
			        					&Kmeter_Vint_A);
			}
			else
			{
				parse_status = 0;
			}
		}
		else if (strstr(IE_keyword,"AMP"))  //IS THE KEYWORD A VALID "TERM" FOR KFACTOR CALIBRATION...
		{
			if (gp_state.generator1_contactor_state==OFFLINE) return (parse_status=0);

       	 	parse_status = 1;               //YES, SUCCESSFULLY PARSED A VALID "TERM"
	   		external_reference = IE_get_parm_value();	//value in unadjusted AMPS to VOLTS.

        	if (external_reference==0.0) external_reference=1.0;

			if (strstr(IE_keyword,"3"))						// Generator Phase C
			{
				parse_status = process_ext_ref_input(
										&external_reference,
					        			&gp_Ic_rms[OSC],
				    	    			&Kmeter_I_C);
			}
			else if (strstr(IE_keyword,"2"))				// Generator Phase B
			{
				parse_status = process_ext_ref_input(
										&external_reference,
					        			&gp_Ib_rms[OSC],
				    	    			&Kmeter_I_B);
			}
			else if (strstr(IE_keyword,"1"))				// Generator Phase A
			{
				parse_status = process_ext_ref_input(
										&external_reference,
					        			&gp_Ia_rms[OSC],
				    	    			&Kmeter_I_A);
			}
			else
			{
				parse_status = 0;
			}
     	}
/*
		else if (strstr(IE_keyword,"VSP"))  //IS THE KEYWORD A VALID "TERM" FOR KFACTOR CALIBRATION...
		{
  			parse_status = 1;               //YES, SUCCESSFULLY PARSED A VALID "TERM"
	  		external_reference = IE_get_parm_value();	//value in unadjusted AMPS to VOLTS.

         	if (external_reference==0.0) external_reference=1.0;

			if (strstr(IE_keyword,"3"))						// Shore Phase C
			{
				parse_status = process_ext_ref_input(
										&external_reference,
					        	  		&gp_Vcn[SP1],
			        					&Kmeter_Vsp1_C);
			}
			else if (strstr(IE_keyword,"2"))				// Shore Phase B
			{
				parse_status = process_ext_ref_input(
										&external_reference,
					        	  		&gp_Vbn[SP1],
			        					&Kmeter_Vsp1_B);
			}
			else if (strstr(IE_keyword,"1"))				// Shore Phase A
			{
				parse_status = process_ext_ref_input(
										&external_reference,
					        	  		&gp_Van[SP1],
			        					&Kmeter_Vsp1_A);
			}
			else
			{
				parse_status = 0;
			}
		}
		else if (strstr(IE_keyword,"ISP"))  //IS THE KEYWORD A VALID "TERM" FOR KFACTOR CALIBRATION...
		{
			if (gp_state.shore_cb_state==OFFLINE) return (parse_status=0);

       	 	parse_status = 1;               //YES, SUCCESSFULLY PARSED A VALID "TERM"
	   		external_reference = IE_get_parm_value();	//value in unadjusted AMPS to VOLTS.

        	if (external_reference==0.0) external_reference=1.0;

			if (strstr(IE_keyword,"3"))						// Shore Phase C
			{
				parse_status = process_ext_ref_input(
										&external_reference,
					        			&gp_Ic_rms[SP1],
				    	    			&Kmeter_Isp1_C);
			}
			else if (strstr(IE_keyword,"2"))				// Shore Phase B
			{
				parse_status = process_ext_ref_input(
										&external_reference,
					        			&gp_Ib_rms[SP1],
				    	    			&Kmeter_Isp1_B);
			}
			else if (strstr(IE_keyword,"1"))				// Shore Phase A
			{
				parse_status = process_ext_ref_input(
										&external_reference,
					        			&gp_Ia_rms[SP1],
				    	    			&Kmeter_Isp1_A);
			}
			else
			{
				parse_status = 0;
			}
     	}
*/
     	else
  		{//NO matching "TERM" -- set status and will drop through ("query" must be false).
        	parse_status = 0;       //command error, invalid "TERM" for :CAL:KFACTOR:"TERM" command.
       	}
 	}

   	if (parse_status)
	{
		RAM_reset = 0;                  // clear RAM reset flag on successful assertion.
	}

	return parse_status;
}
/*-------------------------------------------------------------------------- */
///////////////////////////////////////////////////////////////////////////////
//	int process_ext_ref_input(float*, float*, float*)	
//
//	Purpose: Calibrate meter term given external reference.
//				if given an external reference==0
//				then Kfactor is reset to 1.0 (ie cleared).
//	Returns:	1=successful, 0=failure/command_error.
//
// 	Note:	Calculated Kfactor must be within 20% of nominal 
//			otherwise a command error is flagged and calibration is ignored.
///////////////////////////////////////////////////////////////////////////////
int process_ext_ref_input(float *ext_ref, float *meas, float *Kmeter)
{
  float temp_Kfactor;

//	*Kmeter=1.0;					// Clr kfactor so meas_all() produces an uncompensated measurement.

	if (*meas>0.0)							// if GMM measurement is not 0 Calc kfactors.
	{
		temp_Kfactor = ((*Kmeter * *ext_ref) / *meas);

		if ((temp_Kfactor<MIN_KFACTOR || temp_Kfactor>MAX_KFACTOR) && *ext_ref!=0.0)
		{
			*ext_ref = 0.0;					//clear external reference entry
//			raise_exception(-18); 			//calibration range error.
			return 0;
		}

		*Kmeter = temp_Kfactor;
	}

	if (*Kmeter==0.0)
	{
		*Kmeter=1.0;						// If 0.0 or no entry, force k=1.
	}

	if (*ext_ref==0.0)
	{
		*Kmeter=1.0;						// If entry=0, force k=1.
	}

	return 1;
}
/* -------------------------------------------------------------------------- */
#ifdef MY_DFT
/////////////////////////////////////////////////////////////////////////////////
// myDFT computes Forward Transform, from time-domain to frequency-domain. x(t)->X(f)
//
// int m:		Number of discrete elements in x1
// float *Xin:	Pointer to array of REAL elements to perform transform upon.
//
/////////////////////////////////////////////////////////////////////////////////
void myDFT(int channel, int m, float *Xin, float rms)
{
   int i,k;
   float arg;
   float cosarg,sinarg;
   float Re,Im,Mag;
   float perFund=(100.0/rms);
   
   for (i=0;i<m;i++) 
   {
      waveform[OSC][channel][i] = 0;	//Re{Hn} stored in waveform[OSC][channel][i]
      waveform[EXT][channel][i] = 0;	//Im{Hn} stored in waveform[EXT][channel][i]
      arg = two_PI * (float)i / (float)m;
      
	  for (k=0;k<m;k++) 
	  {
         cosarg = (float) cos((double)(k * arg));
         sinarg = (float) sin((double)(k * arg));
         waveform[OSC][channel][i] += Xin[k] * cosarg;	//captured sinewave is inverted
         waveform[EXT][channel][i] += Xin[k] * sinarg;	//captured sinewave is inverted
      }
      Re = waveform[OSC][channel][i] = waveform[OSC][channel][i] / (float) m;
      Im = waveform[EXT][channel][i] = waveform[EXT][channel][i] / (float) m;

      Mag = waveform[XXX][channel][i] = (float) sqrt(Re*Re+Im*Im) * SQRT_OF_2;		//MAGNITUDE
//      Mag = waveform[XXX][channel][i] = (float) sqrt(Re*Re+Im*Im);						//MAGNITUDE
      waveform[EXT][channel][i] = ((float) Radians_to_Degrees * (float) atan2(Im,Re));	//PHASE
      waveform[AUX][channel][i] = Mag * perFund; 												//PERCENT OF FUNDAMENTAL
   }
 }
/////////////////////////////////////////////////////////////////////////////////
// for	x[i] := Real component of X[i]
// and	y[i] := Imaginary component of X[i]
//
// Magnitude(X[i]) = sqrt((x[i]*x[i])+(y[i]*y[i]))
//
// Phase(X[i]) = tan-1(y[i]/x[i])		Note: atan2{-pi,pi}; atan{-pi/2,pi/2};
//
/////////////////////////////////////////////////////////////////////////////////
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
int parse_HARMONICS(int gp_channel)
{
  int i;
  unsigned long dftTime=eventTime;
  float thd=0.0;

	if (strstr(IE_keyword,":\0")) IE_get_keyword();

	if (strstr(IE_keyword,"VOLT\0")) 
	{
		if (strstr(IE_keyword,"1\0")) 
		{
			myDFT(A_VOLTS,waveform_sample_count[gp_channel][A_VOLTS],waveform[gp_channel][A_VOLTS],gp_Van[gp_channel]);

//			for (i=0;i<waveform_sample_count[gp_channel][A_VOLTS];i++)
//			{
//				float2outbufr(waveform[OSC][A_VOLTS][i]);	//Re{Hn}
//				float2outbufr(waveform[EXT][A_VOLTS][i]);	//Im{Hn}
//			}

			sprintf(cvtbufr,"\n\rDFT Time: %u ms\n\rHarmonic: \tMagnitude \tPhase \tPercent_of_Fundamental\0",(eventTime-dftTime));	// cvt value to ascii
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 

			for (i=0;i<((waveform_sample_count[gp_channel][A_VOLTS]+1)/4);i++)
			{
				sprintf(cvtbufr,"\n\r H[%d]\t\t\0",i);			// cvt value to ascii
				IE_load_outbufr_str(cvtbufr);					// add data to buffer 
				float2outbufr(waveform[XXX][A_VOLTS][i]);		//MAGNITUDE
				IE_load_outbufr_str("\t\t\0");					// add tab to buffer 
				float2outbufr(waveform[EXT][A_VOLTS][i]);		//PHASE
				IE_load_outbufr_str("\t\0");						// add tab to buffer 
				if (waveform[EXT][A_VOLTS][i] > (-100.0)) 
				{
					IE_load_outbufr_str("\t\0");					// add tab to buffer 
				}
				float2outbufr(waveform[AUX][A_VOLTS][i]);		//PERCENT OF FUNDAMENTAL

				if (i>1)	//sum THD data
				{
					thd += (waveform[XXX][A_VOLTS][i]*waveform[XXX][A_VOLTS][i]);
				}
			}
			thd = sqrt(thd);
			thd = thd/gp_Van[gp_channel];
			thd = thd*100.0;
			sprintf(cvtbufr,"\n\r THD =\t%4.2f\0",thd);	// cvt value to ascii
			IE_load_outbufr_str(cvtbufr);					// add data to buffer 

			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		if (strstr(IE_keyword,"2\0")) 
		{	
			myDFT(B_VOLTS,waveform_sample_count[gp_channel][B_VOLTS],waveform[gp_channel][B_VOLTS],gp_Vbn[gp_channel]);

			sprintf(cvtbufr,"\n\rDFT Time: %u ms\n\rHarmonic: \tMagnitude \tPhase \tPercent_of_Fundamental\0",(eventTime-dftTime));	// cvt value to ascii
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 

			for (i=0;i<((waveform_sample_count[gp_channel][B_VOLTS]+1)/4);i++)
			{
				sprintf(cvtbufr,"\n\r H[%d]\t\t\0",i);			// cvt value to ascii
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				float2outbufr(waveform[XXX][B_VOLTS][i]);	//MAGNITUDE
				IE_load_outbufr_str("\t\t\0");					// add tab to buffer 
				float2outbufr(waveform[EXT][B_VOLTS][i]);	//PHASE
				IE_load_outbufr_str("\t\0");						// add tab to buffer 
				if (waveform[EXT][B_VOLTS][i] > (-100.0)) 
				{
					IE_load_outbufr_str("\t\0");					// add tab to buffer 
				}
				float2outbufr(waveform[AUX][B_VOLTS][i]);	//PERCENT OF FUNDAMENTAL

				if (i>1)	//sum THD data
				{
					thd += (waveform[XXX][B_VOLTS][i]*waveform[XXX][B_VOLTS][i]);
				}
			}
			thd = sqrt(thd);
			thd = thd/gp_Vbn[gp_channel];
			thd = thd*100.0;
			sprintf(cvtbufr,"\n\r THD =\t%4.2f\0",thd);	// cvt value to ascii
			IE_load_outbufr_str(cvtbufr);					// add data to buffer 

			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		if (strstr(IE_keyword,"3\0")) 
		{	
			myDFT(C_VOLTS,waveform_sample_count[gp_channel][C_VOLTS],waveform[gp_channel][C_VOLTS],gp_Vcn[gp_channel]);

			sprintf(cvtbufr,"\n\rDFT Time: %u ms\n\rHarmonic: \tMagnitude \tPhase \tPercent_of_Fundamental\0",(eventTime-dftTime));	// cvt value to ascii
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 

			for (i=0;i<((waveform_sample_count[gp_channel][C_VOLTS]+1)/4);i++)
			{
				sprintf(cvtbufr,"\n\r H[%d]\t\t\0",i);			// cvt value to ascii
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				float2outbufr(waveform[XXX][C_VOLTS][i]);	//MAGNITUDE
				IE_load_outbufr_str("\t\t\0");					// add tab to buffer 
				float2outbufr(waveform[EXT][C_VOLTS][i]);	//PHASE
				IE_load_outbufr_str("\t\0");						// add tab to buffer 
				if (waveform[EXT][C_VOLTS][i] > (-100.0)) 
				{
					IE_load_outbufr_str("\t\0");					// add tab to buffer 
				}
				float2outbufr(waveform[AUX][C_VOLTS][i]);	//PERCENT OF FUNDAMENTAL

				if (i>1)	//sum THD data
				{
					thd += (waveform[XXX][C_VOLTS][i]*waveform[XXX][C_VOLTS][i]);
				}
			}
			thd = sqrt(thd);
			thd = thd/gp_Vcn[gp_channel];
			thd = thd*100.0;
			sprintf(cvtbufr,"\n\r THD =\t%4.2f\0",thd);	// cvt value to ascii
			IE_load_outbufr_str(cvtbufr);					// add data to buffer 

			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
	}
	if (strstr(IE_keyword,"CURR\0")) 
	{
		if (strstr(IE_keyword,"1\0")) 
		{	
			myDFT(A_AMPS,waveform_sample_count[gp_channel][A_AMPS],waveform[gp_channel][A_AMPS],gp_Ia_rms[gp_channel]);

			sprintf(cvtbufr,"\n\rDFT Time: %u ms\n\rHarmonic: \tMagnitude \tPhase \tPercent_of_Fundamental\0",(eventTime-dftTime));	// cvt value to ascii
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 

			for (i=0;i<((waveform_sample_count[gp_channel][A_AMPS]+1)/4);i++)
			{
				sprintf(cvtbufr,"\n\r H[%d]\t\t\0",i);			// cvt value to ascii
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				float2outbufr(waveform[XXX][A_AMPS][i]);	//MAGNITUDE
				IE_load_outbufr_str("\t\t\0");					// add tab to buffer 
				float2outbufr(waveform[EXT][A_AMPS][i]);	//PHASE
				IE_load_outbufr_str("\t\0");						// add tab to buffer 
				if (waveform[EXT][A_AMPS][i] > (-100.0)) 
				{
					IE_load_outbufr_str("\t\0");					// add tab to buffer 
				}
				float2outbufr(waveform[AUX][A_AMPS][i]);	//PERCENT OF FUNDAMENTAL

				if (i>1)	//sum THD data
				{
					thd += (waveform[XXX][A_AMPS][i]*waveform[XXX][A_AMPS][i]);
				}
			}
			thd = sqrt(thd);
			thd = thd/gp_Ia_rms[gp_channel];
			thd = thd*100.0;
			sprintf(cvtbufr,"\n\r THD =\t%4.2f\0",thd);	// cvt value to ascii
			IE_load_outbufr_str(cvtbufr);					// add data to buffer 

			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		if (strstr(IE_keyword,"2\0")) 
		{	
			myDFT(B_AMPS,waveform_sample_count[gp_channel][B_AMPS],waveform[gp_channel][B_AMPS],gp_Ib_rms[gp_channel]);

			sprintf(cvtbufr,"\n\rDFT Time: %u ms\n\rHarmonic: \tMagnitude \tPhase \tPercent_of_Fundamental\0",(eventTime-dftTime));	// cvt value to ascii
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 

			for (i=0;i<((waveform_sample_count[gp_channel][B_AMPS]+1)/4);i++)
			{
				sprintf(cvtbufr,"\n\r H[%d]\t\t\0",i);			// cvt value to ascii
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				float2outbufr(waveform[XXX][B_AMPS][i]);	//MAGNITUDE
				IE_load_outbufr_str("\t\t\0");					// add tab to buffer 
				float2outbufr(waveform[EXT][B_AMPS][i]);	//PHASE
				IE_load_outbufr_str("\t\0");						// add tab to buffer 
				if (waveform[EXT][B_AMPS][i] > (-100.0)) 
				{
					IE_load_outbufr_str("\t\0");					// add tab to buffer 
				}
				float2outbufr(waveform[AUX][B_AMPS][i]);	//PERCENT OF FUNDAMENTAL

				if (i>1)	//sum THD data
				{
					thd += (waveform[XXX][B_AMPS][i]*waveform[XXX][B_AMPS][i]);
				}
			}
			thd = sqrt(thd);
			thd = thd/gp_Ib_rms[gp_channel];
			thd = thd*100.0;
			sprintf(cvtbufr,"\n\r THD =\t%4.2f\0",thd);	// cvt value to ascii
			IE_load_outbufr_str(cvtbufr);					// add data to buffer 

			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
		if (strstr(IE_keyword,"3\0")) 
		{	
			myDFT(C_AMPS,waveform_sample_count[gp_channel][C_AMPS],waveform[gp_channel][C_AMPS],gp_Ic_rms[gp_channel]);

			sprintf(cvtbufr,"\n\rDFT Time: %u ms\n\rHarmonic: \tMagnitude \tPhase \tPercent_of_Fundamental\0",(eventTime-dftTime));	// cvt value to ascii
			IE_load_outbufr_str(cvtbufr);	// add data to buffer 

			for (i=0;i<((waveform_sample_count[gp_channel][C_AMPS]+1)/4);i++)
			{
				sprintf(cvtbufr,"\n\r H[%d]\t\t\0",i);			// cvt value to ascii
				IE_load_outbufr_str(cvtbufr);	// add data to buffer 
				float2outbufr(waveform[XXX][C_AMPS][i]);	//MAGNITUDE
				IE_load_outbufr_str("\t\t\0");					// add tab to buffer 
				float2outbufr(waveform[EXT][C_AMPS][i]);	//PHASE
				IE_load_outbufr_str("\t\0");						// add tab to buffer 
				if (waveform[EXT][C_AMPS][i] > (-100.0)) 
				{
					IE_load_outbufr_str("\t\0");					// add tab to buffer 
				}
				float2outbufr(waveform[AUX][C_AMPS][i]);	//PERCENT OF FUNDAMENTAL

				if (i>1)	//sum THD data
				{
					thd += (waveform[XXX][C_AMPS][i]*waveform[XXX][C_AMPS][i]);
				}
			}
			thd = sqrt(thd);
			thd = thd/gp_Ic_rms[gp_channel];
			thd = thd*100.0;
			sprintf(cvtbufr,"\n\r THD =\t%4.2f\0",thd);	// cvt value to ascii
			IE_load_outbufr_str(cvtbufr);					// add data to buffer 

			IE_sep();					// terminate query response data with seperator.
			return 1;
		}
	}
	return 0;
}
//END parse_HARMONICS();
/* -------------------------------------------------------------------------- */
#endif // MY_DFT
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
