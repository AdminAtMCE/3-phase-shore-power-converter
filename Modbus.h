/*******************************************************************************
modbus.h

	Written by:	Dave Newberry for ASEA Power Systems

	Copyright(c)2006, ASEA Power Systems
*******************************************************************************/
/*============================================================================*/
///DATA TYPES START
/*============================================================================*/
//only need these data types for modbus
#define	BYTE	unsigned char
#define	CHAR    char
#define	UINT	unsigned int
#define	UINT16	unsigned
#define	UINT32	unsigned long
#define	VOID    void
/*============================================================================*/
///DATA TYPES END
/*============================================================================*/

/*============================================================================*/
//	START OF MODBUS SERVER HEADER FILE
/*============================================================================*/
/******************************************************************/
/*       MESSAGE STATES (WHILE RECEIVING)                         */
/******************************************************************/
#define CMPLT_COMPLETE  0x01
#define CMPLT_HOPE      0x02
#define CMPLT_NO_HOPE   0x03
#define CMPLT_NO_HOPE_1	0x04
#define CMPLT_NO_HOPE_2 0x05
/******************************************************************/
/*       MODBUS PARAMETERS                                        */
/******************************************************************/
#define MODBUS_NODE_ID  0x03	//default Node Id--Modbus Address

#ifdef MM3					
	#define ASEA_PRODUCT_ID 192	//Assy# 601192 Dual-Master Controller
#else
	#define ASEA_PRODUCT_ID 200	//EPROM Assy# reference 601___
#endif //~MM3

/******************************************************************/
/*       USEFUL DEFINITIONS                                       */
/******************************************************************/

/******************************************************************/
/*      MODBUS FUNCTION CODES                                     */
/******************************************************************/
#define MODBUS_READ_COILS           0x01
#define MODBUS_READ_DISCRETEINPUTS	0x02
#define MODBUS_READ_REGISTER        0x03
#define MODBUS_READ_INPUTREGISTERS  0x04
#define MODBUS_WRITE_COIL           0x05
#define MODBUS_WRITE_REGISTER       0x06
#define MODBUS_WRITE_MULT_REG       0x10
#define MODBUS_WRITE_MULT_COIL      0x0F

/******************************************************************/
/*      OTHER MODBUS #DEFINES                                     */
/******************************************************************/
#define MX_RX_TX_BUFFERSIZE         1024	//can be smaller, like 255
#define	ILLEGAL_FUNCTION			0x01
#define ILLEGAL_DATA_ADDRESS        0x02
#define ILLEGAL_DATA_VALUE          0x03	//Note: This is NOT a data range error...
#define BYTE_SIZE                   8
#define COIL_ON                     0xFF00
#define COIL_OFF                    0x0000
#define WRITE_COIL_RESPONSE_LENGTH  6
#define READ                        0x01
#define WRITE                       0x02

#define MX_COILS	96	//40
#define MX_DI		144	//64
#define MX_IN		192	//154//162
#define MX_REG		144	//106//130

/******************************************************************/
/*       MAP DESCRIPTOR STRUCTURE                                 */
/******************************************************************/
typedef struct
   {
	   UINT16  	address;
	   UINT16  	data;
	   UINT16*	offset;
   } MAP_DESC_TYP ;

typedef struct
   {
	   BYTE    function;
	   BYTE    node_id;
	   UINT16  address;
	   UINT16  length;
	   UINT16  data;
	   UINT16  checksum;
   } IN_BUF_TYP ;
/******************************************************************/

/******************************************************************/
/*       MODBUS PROTOTYPES           MODBUS.c                     */
/******************************************************************/
VOID 	initializeModbusRAM( VOID );
VOID    modbus_init( VOID );
VOID    idle_modbus( VOID );
VOID	isr_Modbus_Error( VOID );
VOID 	isr_Modbus_RxD( VOID );			// if receive interrupt request flag then RxD a byte.

UINT16  modrtuChecksum( BYTE *, UINT16 );
BYTE    modbusCheckAddress( VOID );
VOID    modbusGiveUpCharacter( VOID );
UINT16 	modbusPollReceiveComplete( VOID );
VOID    modbusRespond( MAP_DESC_TYP *);
VOID    get_UART_data( VOID );
VOID    clr_rx( VOID );
BYTE	ASEA_getch( VOID );
UINT16 	ASEA_getRxBufDataCnt( VOID );
VOID 	ASEA_putString( BYTE *, UINT16 );
VOID 	ASEA_sendByte( BYTE );			// send byte out SERIAL port
VOID 	ASEA_TxD( UINT16 );				// send msg out REMOTE port
BYTE 	Get_Modbus_Node_ID( VOID );
VOID 	modbusSendUARTPacket( BYTE );
BYTE 	modbusByteFromBitLength( UINT16 );
UINT16	modbusParseMessage( VOID );
VOID 	modbusReturnException (BYTE );
MAP_DESC_TYP *modbusFetchData( VOID );
MAP_DESC_TYP *modbusStoreData( VOID );
MAP_DESC_TYP *store_data_to_data_array( BYTE, UINT16, UINT16 );
MAP_DESC_TYP *find_map_desc_with_data( BYTE, UINT16 );
//UINT16	ASEA_getTxBufDataCnt( VOID );
//BYTE 		ASEA_putByte( BYTE );

/******************************************************************/
/*       DATA ACCESS API             MBX.c                        */
/******************************************************************/
VOID    da_put_UINT16( MAP_DESC_TYP *, UINT16 );
VOID    da_put_BIT( MAP_DESC_TYP *, UINT16 );
VOID    da_get_BIT( MAP_DESC_TYP * );
VOID    da_get_UINT16( MAP_DESC_TYP * );
VOID    init_da( VOID );
VOID	convertStaticRegisters( VOID );		//one-time conversion only (on init_da()).
VOID	convertCoils( VOID );
VOID	convertDiscreteInputs( VOID );
VOID	convertInputRegisters( VOID );
VOID	convertHoldingRegisters( VOID );
UINT16	ASEA_storeData(UINT16, MAP_DESC_TYP *);	//scale,validate,store data OR exec f().
VOID	refreshModbusData( VOID );

//VOID	storeCoil( UINT16, UINT16 );
//VOID	storeHoldingRegister( UINT16, UINT16 );

//===============================================================
//Circular mbQueue Header File		(queue contained in modbus.c)
//===============================================================
//----------------------------------------------------------------
//Maximum queue size must be defined in number of 16-bit words.
//----------------------------------------------------------------
#define MB_QSIZE MX_RX_TX_BUFFERSIZE	//sizeof queue = #of mbDataType items.
//----------------------------------------------------------------
//Queue data type
//----------------------------------------------------------------
typedef BYTE mbDataType;	//queue holds BYTE data types.
//----------------------------------------------------------------
//Queue Structure
//----------------------------------------------------------------
struct  mbQueueType
{
	UINT16 size;
	UINT16 count;
	mbDataType queue[MB_QSIZE];
	mbDataType *head;
	mbDataType *tail;
};
//----------------------------------------------------------------
// mbQueue Function Prototypes
//----------------------------------------------------------------
struct mbQueueType *mbInitializeQueue(struct mbQueueType *);
VOID mbEnqueue(struct mbQueueType *, mbDataType *);	//put data item in queue.
mbDataType mbDequeue(struct mbQueueType *);			//get data item from queue.
UINT16 mbQueueEmpty(struct mbQueueType *);			//boolean check if queue empty.
UINT16 mbQueueFull(struct mbQueueType *);		   	//boolean check if queue full.
VOID mbFlush_queue(struct mbQueueType *);	   		//clear all queue entries.
/*============================================================================*/
//	END OF MODBUS SERVER HEADER FILE
/*============================================================================*/

