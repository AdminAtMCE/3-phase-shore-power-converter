/* @(#)EH.C */
/* ASEA SOURCE CODE */
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	FILE:			eh.c
//	PURPOSE:		Source Module for Asea Controller	EXCEPTION_HANDLER
//
//	Proprietary Software:	ASEA POWER SYSTEMS
//
//				COPYRIGHT @ ASEA JANUARY 1999
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "ac.h"

//EVENT LOG extern
#include "event.h"
extern int LogEvent( unsigned char, unsigned char );

extern volatile unsigned far int gp_status_word;		// GP STATUS WORD1.
extern volatile unsigned far int gp_status_word2;		// GP STATUS WORD2.
extern volatile unsigned far int gp_status_word3;		// GP STATUS WORD3.
extern unsigned REMOTE_RUN;					//S2-4 RUN(low) input M1041
					   
extern void clear_screen(void);
extern void lcd_display(char *, int, int);
extern void set_Vr_ILMr(void);
extern struct user_pgm_structure pgm_edit;
extern char *str;				/* string to be output */
extern int REMOTE_mode;
extern float ILMr;
extern float Vr;				/* Voltage range */
extern float XFMR_ratio;			/* XFMR ratio of XFMR installed */
extern float F_min;				/* EXECuted Prog. FREQ Limits. */
extern float F_max;
extern float I_ILMr;				/* Current programming and metering range. */
extern float V_min;				/* EXECuted Prog. voltage Limits. */
extern float V_max;
extern float F_span;						/* upper frequency hardware limit */
extern int zop_mode;				/* zop on/off flag. */
extern float WFk_wf[(WF_ARRAY/MAX_WF_STEPS)];	/* get SINErms/WFrms ratio of active waveform */
extern float Ka_wf;				/* holds SINErms/WFrms ratio of active waveform... */
extern float Kb_wf;				/* passed from WFedit to calc_pgm_values(). */
extern float Kc_wf;
extern float amps_volts;			/* sets ammeter scaling & ILM range */
extern int display_type;
extern int display_function;
extern char *key_name[];
extern char *sumstat[5];
extern char *system_status[4];
extern char *master_slave[2];
extern char *ship_name;									 
extern char *gp_cid;
extern int undetected_sync_signal_source;
extern struct gp_state_struct gp_state;

extern struct IE_ESR ESR;			/* std event status reg. structure */
extern char ERROR_DESC[160];			/* holds SCPI SYST:ERR detail */
extern void IE_esr_report(void);
extern int wait_for_key(void);

extern int active_status_code;
extern int last_failure_code;
extern int last_failure_code_logged;
extern unsigned MODEL_LC;			//S4-8
//extern unsigned MOD1030_OPTION;	//S4-1

extern unsigned char ten_ms_IRQ;
extern unsigned int debug, debug2, debug3;

/* ------------------------------------------------------------------ */
/* ------------------------------------------------------------------ */

/*-------------------------------------------------------------------------- */
#ifdef EXCEPTION_HANDLER
void raise_exception(int exception_id)
{
char *exception[]={
"                                        ",	//   0
"                                        ",	//  -1
"                                        ",	//  -2
"                                        ",	//  -3
"DEVICE UNRECOGNIZED.                    ",	//  -4
"WRITE ATTEMPTED TO A READ-ONLY DEVICE.  ",	//  -5
"UNRECOGNIZED EXCEPTION IDENTIFIER.      ",	//  -6
"                                        ",	//  -7
"                                        ",	//  -8
"                                        ",	//  -9
"                                        ",	// -10
"ILLEGAL POWER SOURCE FORM - RESET UPC!! ",	// -11
"                                        ",	// -12
"                                        ",	// -13
"                                        ",	// -14
"                                        ",	// -15
"INVALID XFMR RATIO = 0.0, USE ATTEMPTED!",	// -16
"                                        ",	// -17
"CALIBRATION RANGE ERROR:                ", // -18
"INPUT POWER CORRUPT, SYSTEM WILL RESET. ", // -19
"High-Voltage DC Phase-A greater than 210",	// -20
"High-Voltage DC Phase-B greater than 210",	// -21
"High-Voltage DC Phase-C greater than 210",	// -22
"NO EXTERNAL CB OPEN AUX CONFIRM SIGNAL. ",	// -23
"NO EXTERNAL CB CLOSE AUX CONFIRM SIGNAL.",	// -24
"TRANSIENT STEP CALCULATION- FATAL ERROR.",	// -25
"WARNING:OUTPUT       CONVERTER OVERTEMP.",	// -26
"WARNING:INVALID DISPLAY FUNCTION.       ",	// -27
"WARNING:SYNC SIGNAL NOT DETECTED.       ",	// -28
"WARNING:SYNC SIGNAL NOT DETECTED.       "};	// -29

	exception_id = 0 - exception_id;

	if (exception_id>250 || exception_id==26)
	{
		ESR.exe = 1;
	 	lcd_display("\
DIAGNOSTIC:OT        * *         * *    \n\
OVER-TEMPERATURE CONDITION.             \n\
CHECK AIR INTAKE AND AMBIENT TEMPERATURE\n\
SYSTEM SHUTDOWN.                        ",1,1);
	}

	if (exception_id > 250) active_status_code = exception_id - 200;
	else active_status_code = exception_id;
		
//	while (ten_ms_IRQ)		//wait for ten_ms_IRQ
//		;				

	switch (exception_id)
	{
		case 20://HVDC>210	
		case 21://HVDC>210	
		case 22://HVDC>210	
				lcd_display("DIAGNOSTIC:HVDC>210 ",1,1);
				lcd_display(exception[exception_id],2,1);
				break;

		case 23://NO EXTERNAL CB CLOSE AUX CONFIRM SIGNAL.	
	 	lcd_display("\
DIAGNOSTIC:EXT_CB    * *         * *    \n\
NO EXTERNAL CB OPEN AUX CONFIRM SIGNAL  \n\
CIRCUIT BREAKER MAY BE JAMMED.          \n\
                                        ",1,1);
                                        
//				if (active_status_code != last_failure_code) {
//					while (ten_ms_IRQ)		//wait for ten_ms_IRQ
//						;
//						
//					LogEvent(Ev_ERR_ON,active_status_code);
//				}
				break;

		case 24://NO EXTERNAL CB CLOSE AUX CONFIRM SIGNAL.	
	 	lcd_display("\
DIAGNOSTIC:EXT_CB    * *         * *    \n\
NO EXTERNAL CB CLOSE AUX CONFIRM SIGNAL \n\
CONVERTER POWER OUTPUT TURNED OFF.      \n\
                                        ",1,1);
                                        
//				if (active_status_code != last_failure_code) {
//	                debug = active_status_code;
//	                debug2 = last_failure_code;
//	                debug3++;
//                                        
//					while (ten_ms_IRQ)		//wait for ten_ms_IRQ
//						;
//						
//					LogEvent(Ev_ERR_ON,active_status_code);
//				}
				break;

		case 241://NO EXTERNAL CB CLOSE AUX CONFIRM SIGNAL.	
	 	lcd_display("\
DIAGNOSTIC:EXT_CB    * *         * *    \n\
NO EXTERNAL CB CLOSE AUX CONFIRM SIGNAL \n\
                                        \n\
                                        ",1,1);
                                        
//				if (active_status_code != last_failure_code) {
//					while (ten_ms_IRQ)		//wait for ten_ms_IRQ
//						;
//						
//					LogEvent(Ev_ERR_ON,active_status_code);
//				}
				break;

		case 25:// TRANSIENT STEP CALCULATION- FATAL ERROR.
				ESR.exe = 1;
				break;
 
		case 18://calibration range error.
				ESR.exe = 1;
				break;

		case 19://POWER CORRUPT error.
				ESR.dde = 1;
				break;

		case 30://INPUT_OVLD error.
				ESR.exe = 1;
				lcd_display("\
DIAGNOSTIC:OVERLOAD  * *         * *    \n\
INPUT OVERLOAD DETECTED.                \n\
                                        \n\
                                        ",1,1);
				break;

		case 31://OUTPUT_OVLD error.
				ESR.exe = 1;
				lcd_display("\
DIAGNOSTIC:OVERLOAD  * *         * *    \n\
OUTPUT OVERLOAD DETECTED.               \n\
SHUTDOWN IMMINENT.                      \n\
                                        ",1,1);
				break;

//DOCK POWER BROWNOUT v2.21
		case 32://OUTPUT_OVLD error.
				ESR.exe = 1;
				lcd_display("\
DIAGNOSTIC:BROWNOUT  * *         * *    \n\
DOCK POWER BROWNOUT DETECTED.           \n\
                                        \n\
                                        ",1,1);
				break;

//OIL LEVEL LOW or EPO v3.00
		case 33://OIL LEVEL LOW error / EPO
				ESR.exe = 1;
//				lcd_display("\
//WARNING: OIL LEVEL LOW  * *      * *    \n\
//LIQUID COOLED CONVERTER: OIL LEVEL LOW. \n\
//       * * INVERTER SHUTDOWN * *        ",1,1);
			if (!MODEL_LC) {
				lcd_display("\
DIAGNOSTIC:EPO       * *         * *    \n\
EMERGENCY POWER OFF                     \n\
       * * INVERTER SHUTDOWN * *        \n\
                                        ",1,1);
			}
			else {
				lcd_display("\
DIAGNOSTIC:SPARE     * *         * *    \n\
COOLANT OIL LOW                         \n\
Please Contact ASEA Factory Service     \n\
Representative For Assistance.          ",1,1);

				if (REMOTE_RUN)	lcd_display("or EPO * * ",2,17);
			}
				break;

		case 249://GROUND_FAULT	
			if (!MODEL_LC) {
	 	lcd_display("\
DIAGNOSTIC:GND_FAULT * *         * *    \n\
SYSTEM  _GROUND_FAULT_  DETECTED.       \n\
       * * INVERTER SHUTDOWN * *        \n\
                                        ",1,1);
	       	}
	       	else {
	 	lcd_display("\
DIAGNOSTIC:SPARE_1_OT * *         * *   \n\
MOISTURE IN COOLANT OIL                 \n\
Please Contact ASEA Factory Service     \n\
Representative For Assistance.          ",1,1);
	       	}
				break;

		case 261://OVERTEMPERATURE	
				lcd_display("T1_OT",1,12);
				break;
		case 262://OVERTEMPERATURE	
				lcd_display("HVDC_PS_OT",1,12);
				break;
		case 263://OVERTEMPERATURE	
				lcd_display("SHED LOAD.",2,30);
				lcd_display("SHUTDOWN IMMINENT.",4,1);
				lcd_display("PRE-ALARM OT_A",1,12);
//				lcd_display("OT_A",1,12);
				break;
		case 264://OVERTEMPERATURE	
				lcd_display("INV_A_OT",1,12);
				break;
		case 266://OVERTEMPERATURE	
				lcd_display("SHED LOAD.",2,30);
				lcd_display("SHUTDOWN IMMINENT.",4,1);
				lcd_display("PRE-ALARM OT_B",1,12);
//				lcd_display("OT_B",1,12);
				break;
		case 267://OVERTEMPERATURE	
				lcd_display("INV_B_OT",1,12);
				break;
		case 268://OVERTEMPERATURE	
				lcd_display("PRE-ALARM OT_AMBIENT",1,12);
//				lcd_display("AMBIENT_OT",1,12);
//				break;
		case 269://OVERTEMPERATURE	
				lcd_display("SHED LOAD.",2,30);
				lcd_display("SHUTDOWN IMMINENT.",4,1);
//				if (MOD1030_OPTION)
					lcd_display("PRE-ALARM OT_C",1,12);
//				else
//					lcd_display("OT_C",1,12);
				break;
		case 270://OVERTEMPERATURE	
				lcd_display("INV_C_OT",1,12);
				break;
		case 26://OVERTEMPERATURE	
				break;

		case 27://INVALID DISPLAY FUNCTION	
				ESR.exe = 1;
				lcd_display("\
WARNING: INVALID DISPLAY FUNCTION:      \n\
PLEASE REPORT THIS ERROR TO THE FACTORY.\n\
                                        \n\
                                        ",1,1);
				lcd_display(itoa(display_type),1,36);
				lcd_display(itoa(display_function),1,38);
				break;

		case 28://NO SYNC SIGNAL	
				ESR.exe = 1;
				clear_screen();

				switch (undetected_sync_signal_source)		 // daq_mux_id of offending signal source.
				{

					case AUX:
								lcd_display("\
DIAGNOSTIC:Daux      * *         * *    \n\
SYNC SIGNAL NOT FOUND.  METERING ERROR. \n\
                                        \n\
                                        ",1,1);
								break;
					case XXX:

					case -1:
												
					case EXT:
											lcd_display("\
DIAGNOSTIC:EXT       * *         * *    \n\
CONVERTER SYNC SIGNAL NOT DETECTED.     \n\
                                        \n\
                                        ",1,1);
											break;

					case SP2:
											lcd_display("\
DIAGNOSTIC:SP2       * *         * *    \n\
INPUT SYNC SIGNAL NOT DETECTED.         \n\
                                        \n\
SHORE POWER METERING ERROR.             ",1,1);
											break;

					case SYS:
											lcd_display("\
DIAGNOSTIC:GEN2      * *         * *    \n\
GENERATOR SYNC SIGNAL NOT DETECTED.     \n\
                                        \n\
                                        ",1,1);
											break;

					case GEN:
											lcd_display("\
DIAGNOSTIC:GEN1      * *         * *    \n\
GENERATOR SYNC SIGNAL NOT DETECTED.     \n\
TRANSFERS DISABLED.                     \n\
CHECK GENERATOR METERING INTERFACE.     ",1,1);
											break;

					case SP1:
											lcd_display("\
DIAGNOSTIC:SP1       * *         * *    \n\
INPUT SYNC SIGNAL NOT DETECTED.         \n\
                                        \n\
SHORE POWER METERING ERROR.             ",1,1);
											break;

					case OSC://
											lcd_display("\
DIAGNOSTIC:OUTPUT    * *         * *    \n\
METERING ERROR                          \n\
CONVERTER SYNC SIGNAL NOT DETECTED.     ",1,1);
											break;

					default:
								lcd_display("\
DIAGNOSTIC:NONE                        \n\
THIS IS AN INVALID FAULT.              \n\
PRESS 'F1' & 'F2' TO CLEAR.            \n\
                                       ",1,1);
								break;
				}
				break;

		case 29://INVERTER SHUTDOWN	
				ESR.exe = 1;
				lcd_display("\
DIAGNOSTIC:D86       * *         * *    \n\
                                        \n\
                                        \n\
       * * INVERTER SHUTDOWN * *        ",1,1);

				if (gp_state.input_overcurrent)					// {OK,NOT_OK}.	//NOT_OK==INPUT_OVERCURRENT.
				{
					lcd_display("INPUT OVERCURRENT DETECTED.             ",2,1);
					active_status_code = 40;
//					if (active_status_code != last_failure_code) 
//						LogEvent(Ev_ERR_ON,active_status_code);
				}

				if (gp_state.hardware_current_limit)			// {OK,NOT_OK}.	//NOT_OK==CURRENT_LIMITING.
				{
					lcd_display("HARDWARE CURRENT LIMIT > THAN 10sec.    ",2,1);
					active_status_code = 41;
//					if (active_status_code != last_failure_code) 
//						LogEvent(Ev_ERR_ON,active_status_code);
				}
				if (gp_state.pfc_fault)							// {OK,NOT_OK}.	//NOT_OK==PFC_FAULT.
				{
					lcd_display("PFC FAULT DETECTED.                     ",2,1);
					active_status_code = 42;
//					if (active_status_code != last_failure_code) 
//						LogEvent(Ev_ERR_ON,active_status_code);
				}
				if (gp_state.lvdc_fault)						// {OK,NOT_OK}.	//NOT_OK==LVDC_FAULT.
				{
					lcd_display("LOW VOLTAGE DC FAULT DETECTED.          ",2,1);
					active_status_code = 43;
//					if (active_status_code != last_failure_code) 
//						LogEvent(Ev_ERR_ON,active_status_code);
				}
				if (gp_state.inverter_fault)					// {OK,NOT_OK}.	//NOT_OK==INVERTER_FAULT.
				{
					lcd_display("INVERTER FAULT DETECTED.                ",2,1);
					active_status_code = 44;
//					if (active_status_code != last_failure_code) 
//						LogEvent(Ev_ERR_ON,active_status_code);
				}
				if (gp_state.system_overtemperature)			// {OK,NOT_OK}.	//NOT_OK==SYSTEM_OVERTEMPERATURE.
				{
					lcd_display("SYSTEM OVERTEMPERATURE DETECTED.        ",2,1);
					active_status_code = 45;
//					if (active_status_code != last_failure_code) 
//						LogEvent(Ev_ERR_ON,active_status_code);
				}
				break;

		default:		//daq_mux_id BAD (TO THE BONE!)
				ESR.dde = 1;
				lcd_display("UNRECOGNIZED EXCEPTION IDENTIFIER.      ",1,1);	//  -6
				break;
	}

	if (active_status_code != last_failure_code_logged)
	{
//        debug = active_status_code;
//        debug2 = last_failure_code;
//        debug3++;
                            
		while (ten_ms_IRQ)		//wait for ten_ms_IRQ
			;

		if ((active_status_code != 33) || (MODEL_LC))
			LogEvent(Ev_ERR_ON,active_status_code);
		
		last_failure_code_logged = active_status_code;
	}
	
	last_failure_code = active_status_code;
	
	if (exception_id<30)
	{
		strncpy(ERROR_DESC,exception[exception_id],39);
		IE_esr_report();
	}
}
#endif //EXCEPTION_HANDLER

/*--------------------------------------------------------------------------*/
