/* @(#)MEAS.C */
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	FILE:			meas.c
//	PURPOSE:		METER Source Module for the ASEA 3-phase models.
//
//	Proprietary Software:	ASEA POWER SYSTEMS
//
//				COPYRIGHT @ ASEA JANUARY 1999
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

/* ======================================================== INCLUDE FILES */
#include "ac.h"
//EVENT LOG extern
#include "event.h"
extern int LogEvent( unsigned char, unsigned char );

#ifdef TRACE_ON
	#include "queue.h"
	extern struct queueType *trace_queue;
#endif //TRACE_ON

#define ADC_10_MIN 	-511	// 10-bit ADC adjusted lower counts limit.
#define ADC_10_MAX	510		// 10-bit ADC adjusted upper counts limit.

//==========================================================================
// Typedefs
//==========================================================================
typedef unsigned long	timeType;	//This type exists in log.h
typedef unsigned		timerId;	//Timer Id/Handle is an index

/*=========================================================================*/
/*=========================PUBLIC==========================================*/
//extern void meas_all(void);			/* calc new meter values. */
//extern float get_positive_hvdc(void);
//extern float get_negative_hvdc(void);
//extern float meas_freq(int mux_id);
//extern void meas_avg_system_temperature(void);
//extern void clear_meter_data(unsigned);  // set measured meter data values to zero given daq channel.

/*=========================================================================*/
/*=========================PRIVATE=========================================*/
interrupt(0x36) using (GEN2_SYNC_RB) void gen2_sync(void);	// CC22INT, line sync. counter.		
interrupt(0x44) using (GEN1_SYNC_RB) void gen1_sync(void);	// CC29INT, line sync. counter.		
interrupt(0x37) using (SYNC_RB) void output_sync(void);	// CC23, line sync. counter.		
interrupt(0x35) using (SYNC_RB) void sp1_sync(void);	// CC21, line sync. counter.		
interrupt(0x28) using (ADC_RB) void adc_read(void);		// ADCINT, acquire meter data.
//interrupt(0x29) using (ADC_RB) void adc_overrun(void);	// 10-BIT ADC overrun exception interrupt.	

void updateLoadManagementSignals( float );	//v3.00 LoadManagementPlcInterfaceOption

float calculate_level(float,float,float,float,float);

void meas_all(void);			/* calc new meter values. */
void init_meas(void);			/* enable metering interrupts */
void meas_start(void);			/* enable metering interrupts */
void meas_3phase(void);
float meas_freq(int mux_id);
float get_system_temperature(void);
float get_positive_hvdc(void);
float get_negative_hvdc(void);
void meas_avg_system_temperature(void);
void clear_meter_data(unsigned);		// set measured meter data values to zero for given daq channel.
void initialize_metering_ram(void);			// initializes battery-backed-up RAM variables for osc calib.,etc..

void dummy_meter_data(unsigned);
void generateWaveform(int, float, float, float *);
/*=========================================================================*/
/*=========================================================================*/

/* ===================================================== PERSISTANT MEMORY */
#pragma noclear
#pragma default_attributes
/* ======================================================================================= PERSISTANT MEMORY */
system int meter_data[METER_ARRAY];			/* raw ADC metering data */
//int huge meter_data[METER_ARRAY];			/* raw ADC metering data */

float huge waveform[8][6][METER_DATA_BUF_SIZE];	//6sources,9channels,<1024samples
int waveform_sample_count[8][6];

unsigned adcRate;	//ADCON ADCTC & ADSTC
system volatile unsigned int adc_samples;			/* number of samples in metering one waveform */
float Kmeter_Vint_A, Kmeter_Vint_B, Kmeter_Vint_C;	/* Correct Vmetering based on EXT ref cal. */
float Kmeter_Vext_A, Kmeter_Vext_B, Kmeter_Vext_C;
float Kmeter_I_A, Kmeter_I_B, Kmeter_I_C;
float amps_volts;					/* sets ammeter scaling & ILM range */
float amps2volts;					// sets ammeter scaling.
float volts2volts;					// sets voltmeter scaling.
float Vint_volts, Vext_volts;				/* volts to volts ratio */

float Kmeter_Vgen_A, Kmeter_Vgen_B, Kmeter_Vgen_C;	//Generator Voltage Kfactors.
float Kmeter_Igen_A, Kmeter_Igen_B, Kmeter_Igen_C;	//Generator Current Kfactors.

float Kmeter_Vsp1_1, Kmeter_Vsp1_A, Kmeter_Vsp1_B, Kmeter_Vsp1_C;	//Shore Power #1 Voltage Kfactors.
float Kmeter_Isp1_1, Kmeter_Isp1_A, Kmeter_Isp1_B, Kmeter_Isp1_C;	//Shore Power #1 Current Kfactors.

float Kmeter_Vsp2_1, Kmeter_Vsp2_A, Kmeter_Vsp2_B, Kmeter_Vsp2_C;	//Shore Power #2 Voltage Kfactors.
float Kmeter_Isp2_1, Kmeter_Isp2_A, Kmeter_Isp2_B, Kmeter_Isp2_C;	//Shore Power #2 Current Kfactors.

float Kmeter_Vsys_A, Kmeter_Vsys_B, Kmeter_Vsys_C;	//System Voltage Kfactors.
float Kmeter_Isys_A, Kmeter_Isys_B, Kmeter_Isys_C;	//System Current Kfactors.

float Kmeter_Vaux_A, Kmeter_Vaux_B, Kmeter_Vaux_C;	//Aux In Voltage Kfactors.
float Kmeter_Iaux_A, Kmeter_Iaux_B, Kmeter_Iaux_C;	//Aux In Current Kfactors.

/* ======================================================================================= VOLITILE MEMORY */
#pragma clear	// fill iram with cleared data so noclear data is put in batt ram.
/* ======================================================================================= VOLITILE MEMORY */
volatile unsigned tempADCON;

float Van_csc, Vbn_csc, Vcn_csc, Vab_csc;	/* one cycle RMS metered output value for csc*/
int OverRange[8];							/* Metering overrange indicators. */
int gp_OverRange[8][8];						/* Metering overrange indicators. */
volatile unsigned osc_gen_sync;
volatile unsigned sync_count;		// global temp.
volatile unsigned sync_1_count;		// global temp.

//unsigned T5cnt;
volatile unsigned last_deltaT5;	// Last relative Tick count for one cycle, used for Lock guard in gen_sync().
volatile unsigned oldT5;		// global temp to determine deltaT5.
volatile unsigned newT5;		// global temp.
volatile unsigned deltaT5;	// Relative Tick count for one cycle, used for frequency calc.
volatile unsigned ff;		// Flip-Flop used to alternately start and stop a meter cycle.
volatile unsigned oldT5gen2;		// global temp to determine deltaT5.
volatile unsigned newT5gen2;		// global temp.
volatile unsigned deltaT5gen2;	// Relative Tick count for one cycle, used for frequency calc.
volatile unsigned last_deltaT5gen2;	// Last relative Tick count for one cycle, used for Lock guard in gen_sync().
volatile unsigned oldT5gen1;		// global temp to determine deltaT5.
volatile unsigned newT5gen1;		// global temp.
volatile unsigned deltaT5gen1;	// Relative Tick count for one cycle, used for frequency calc.
volatile unsigned last_deltaT5gen1;	// Last relative Tick count for one cycle, used for Lock guard in gen_sync().

float system_temperature;
float positive_hvdc;
float negative_hvdc;

extern float Debug_Var[];
/*=========================================================================*/
//				EXTERN
/*=========================================================================*/

extern volatile int display_type;
extern volatile int display_function;

//from upc_sid.c	SYSTEM INTERFACE DRIVER
extern int set_meter_mux(unsigned, unsigned);
extern int set_sysstat(unsigned);
//from upc_osc.c	OSCILLATOR & TRANSIENT
extern void CSC(void);							// Continuously self calibrate.

extern unsigned int huge user_wf[WF_ARRAY];			/* user waveforms */

extern int huge *a_volts;		// meter_data pointer.
extern int huge *a_amps;		// meter_data pointer.
extern int huge *b_volts;		// meter_data pointer.
extern int huge *b_amps;		// meter_data pointer.
extern int huge *c_volts;		// meter_data pointer.
extern int huge *c_amps;		// meter_data pointer.

extern int sp1a_raw[8];
extern int sp1b_raw[8];
extern int sp1c_raw[8];
extern int sp2a_raw[8];
extern int sp2b_raw[8];
extern int sp2c_raw[8];
extern int sp2ba_raw[8];
extern int sp2cb_raw[8];
extern int sp2ac_raw[8];


extern float IN_RATED_AMPS_LO_ONE;
extern float IN_RATED_AMPS_HI_ONE;
extern float IN_RATED_AMPS_LO_TWO;
extern float IN_RATED_AMPS_HI_TWO;
extern float OUTPUT_RATED_AMPS;
extern unsigned GP_OUTPUT_FORM;
extern unsigned AC75_CONTROL_PCB;	//S4-7	Used in Model 'L' Series & AC75, or bigger, units.
extern unsigned LOW_RANGE_INPUT;	//S3-8  Used for HIGH_RANGE output with LOW_RANGE input units
extern unsigned GEN_AMPS_PRESENT;	//S4-6  v2.32, or better

extern int shoreCordRating;						// {30 to 250 amps}.

extern volatile unsigned long lastWattTime;	//unsigned long wattTime in 10ms units.
extern volatile unsigned long wattTime;	//unsigned long wattTime in 10ms units.
extern float wattHours;
extern unsigned wattHourMeterON;

extern unsigned EXT_XFMR_OPTION;
extern float RATED_POWER;
extern float GP_OUTPUT_FREQUENCY;
extern float GP_OUTPUT_VOLTAGE;
extern float Set_Output_Voltage;
extern unsigned DUAL_INPUT;
extern float GP_XFMR_RATIO;
//extern unsigned DUAL_GENSET;
extern unsigned MULTIGEN_OPTION;	//S3-5
extern unsigned MULTIGEN_BESECKE;		//gp_status_word3 & 0x0004 (high) to configure.
extern unsigned OPTCON_PCB;		//opcon_status bit#15

extern unsigned NV_MODEL;		//S4-1

#ifdef NO_OPTCON_PCB
extern volatile unsigned far int optcon_control;		// OPCON CONTROL WORD--DUMMY.
extern volatile unsigned far int optcon_control_2;		// OPCON CONTROL WORD 2.
#else												//CS3B(low)
extern volatile system unsigned int optcon_control;		//CS2*Y7*(low) OPCON CONTROL WORD.
extern volatile system unsigned int optcon_control_2;		//CS2*Y7*(low) OPCON CONTROL WORD 2.
#endif

extern unsigned OPTCON_CONTROL_2;	//image in RAM

extern struct gp_state_struct gp_state;
///extern volatile unsigned new_key;
extern volatile int display_function;
extern volatile int gp_diagnostic_code;
extern unsigned daq_mux_id;
extern unsigned sync_mux_id;
extern int undetected_sync_signal_source;

extern float XFMR_ratio;					/* XFMR_ratio in use */
extern volatile iram unsigned long wait_10ms;		/* 10ms interrupt increments */
extern system unsigned int wf_steps;		/* number of steps in one waveform */
extern volatile int meter_calc;				/* flag==1 allows metering calculations */
extern int ALC_mode;				/* 0=disabled, 1=Enabled */

extern float gp_Van_DC[8], gp_Vbn_DC[8], gp_Vcn_DC[8];	/* RMS metered output value */
extern float gp_Van[8], gp_Vbn[8], gp_Vcn[8];			/* RMS metered output value */
extern float gp_Vab[8], gp_Vbc[8], gp_Vca[8];			/* RMS metered output value */
extern float gp_Ia_rms[8], gp_Ib_rms[8], gp_Ic_rms[8];	/* RMS metered output value */
extern float gp_Ia_pk[8], gp_Ib_pk[8], gp_Ic_pk[8];		/* peak currents */
extern float gp_KWa[8], gp_KWb[8], gp_KWc[8];			/* metered output value */
extern float gp_KVAa[8], gp_KVAb[8], gp_KVAc[8];	   	/* calculated output value */
extern float gp_PFa[8], gp_PFb[8], gp_PFc[8];			/* calculated output value */
extern float gp_ICFa[8], gp_ICFb[8], gp_ICFc[8];	  	/* calculated output value */
extern float gp_level_a[8], gp_level_b[8], gp_level_c[8];	  	/* calculated output Load Level (% rating) */
extern float system_level;								//highest level of input/output A/B/C.
extern float system_kva;						//highest load of input/output A/B/C.
extern float system_kw;								//highest power of input/output A/B/C.
extern float gp_Freq[8];							  	/* calculated frequencye */

extern float maxSystemLevel;
extern float maxSystemPower;

extern float maxLevelIn1;
extern float maxPowerIn1;

extern float maxGenFreq;
extern float minGenFreq;

//CONVERTER INPUT
extern float minShorVolts;
extern float maxShorVolts;
extern float minShorAmps;
extern float maxShorAmps;
extern float minShorFreq;
extern float maxShorFreq;
//CONVERTER OUTPUT
extern float minConvVolts;
extern float maxConvVolts;
extern float minConvAmps;
extern float maxConvAmps;
extern float minConvFreq;
extern float maxConvFreq;
extern float maxConvLevel;
extern float maxConvPower;
//GENERATOR #1
extern float minGen1Volts;
extern float maxGen1Volts;
extern float minGen1Freq;
extern float maxGen1Freq;
//GENERATOR #2
extern float minGen2Volts;
extern float maxGen2Volts;
extern float minGen2Freq;
extern float maxGen2Freq;

extern float averSystemLevel;
extern float averSystemPower;

extern iram float Van_DC, Vbn_DC, Vcn_DC;
extern iram float Van, Vbn, Vcn, Vab, Vbc, Vca;		/* RMS metered output value */
extern iram float Ia_rms, Ib_rms, Ic_rms;				/* RMS metered output value */
extern iram float Ia_pk, Ib_pk, Ic_pk;					/* peak currents */
extern iram float KWa, KWb, KWc;							/* metered output value */
extern iram float KVAa, KVAb, KVAc;						/* calculated output value */
extern float PFa, PFb, PFc;							/* calculated output value */
extern float ICFa, ICFb, ICFc;						/* calculated output value */
extern float Freq;									  	/* calculated frequencye */
extern int Form;				/* Form in use */

extern int active_pgm_no;			/* pgm being executed */
extern struct user_pgm_structure far user_pgm[MAX_PGM+5];	/* declare pgm0 + pgm1-99 + (pgm100-103 mil 704d) */
extern int trans_mode;				/* indicates trans in progress if ==1 */
extern int exec_flag;				/* flag says pgm executed if true. */

extern volatile unsigned Lock;
extern unsigned generator_transfer_rolloff;

//#ifdef MODBUS
extern void remoteManager( void );
extern unsigned modbusActive;
//#endif //MODBUS

extern float LmShedOffLevel;
extern float LmShedOnLevel;
extern float LmAddOnLevel;
extern float LmAddOffLevel;
extern unsigned LmShedOnDelay;
extern unsigned LmShedOffDelay;
extern unsigned LmAddOnDelay;
extern unsigned LmAddOffDelay;

extern timerId LmAddOffTimer;		//declare & allocate timer
extern timerId LmShedOffTimer;	//declare & allocate timer
extern timerId LmShedOnTimer;		//declare & allocate timer
extern timerId LmAddOnTimer;		//declare & allocate timer

extern unsigned timerRunning( timerId );	//Boolean indicating if timer started.
extern timerId		getTimer( void );			//allocate a new timer.
extern void		startTimer( timerId );		//set this timer to NOW and run.
extern timeType	elapsedTime( timerId );		//get elapsed time for this timer.
extern unsigned	timerRunning( timerId );	//Boolean indicating if timer RUNNING.
extern void		stopTimer( timerId );		//set this timer to 0 aka stopped.
extern timeType	TimMilliseconds( unsigned );	//convert ms to 10ms timeType ticks.

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//@METER		METER		METER		METER		METER		METER		METER		METER		   @
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
/* ======================================================================================= FUNCTION DEFINITIONS */
/* ======================================================================================= INTERRUPTS/TASKS */
/*---------------------------------------------------------------------------*/
interrupt(0x35) using (SYNC_RB) void	// CC21INT.
sp1_sync()
{
//  register unsigned osc_reset=0xFF07;

#ifdef TIME_TICKS_OUT
//_putbit(1,P3,15);	//P3.15 => Port P3A-7 => output high.	//sp1_sync
#endif //TIME_TICKS_OUT

	CC21IC = 0;					// disable sync interrupt.

	oldT5 = newT5;
	newT5 = T5;

	if (newT5 < oldT5)
	{
		deltaT5 = (0x10000 - oldT5)  + newT5;	//calculate delta for wrapped timer.
	}
	else
	{
		deltaT5 = newT5 - oldT5;				//calculate delta.
	}

	if (!meter_calc)							//do not interrupt meas_all().
	{
		if(ff)
		{
			ADCON=0;
			ADCIC=0;
			CC21IC=0;

//			if (adc_samples)
//			{
//				adc_samples = adc_samples-1;	//remove last sample...or not.
//			}
			meter_calc = 1; 				// enable meas_all() calculations.
		}
		else
		{
			ADCIC = 0x0076;						// enable metering interrupt
//			ADCON = 0xF0B7;						// start adc read
//			ADCON = 0x10B7;						// start adc read
///			ADCON = 0x30B7;						// start adc read
///
			tempADCON	= 0x30B5 & 0x0FFF;			// clear ADCTC & ADSTC
			ADCON 	= (adcRate<<12) | tempADCON;	//ADCON ADCTC & ADSTC// start adc read
///		
			CC21IC = 0x0077;					// enable sync interrupt.
		}
		ff = ff^1;								//toggle ff
	}

#ifdef TIME_TICKS_OUT
//_putbit(0,P3,15);	//P3.15 => Port P3A-7 => output low.	//sp1_sync
#endif //TIME_TICKS_OUT
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
interrupt(0x44) using (GEN1_SYNC_RB) void	// CC29INT.
gen1_sync()
{
//  register unsigned osc_reset=0xFF02;	//SYNC DEAD-ON.
  register unsigned osc_reset=0xFF07;	//LEAD BY 7 degrees TO COMPENSATE FOR PHASE DELAY IN GEN_SYNC.

	CC29IC = 0;					// disable GEN sync interrupt.

	oldT5gen1 = newT5gen1;
	newT5gen1 = T5;
	last_deltaT5gen1=deltaT5gen1;
	sync_count = T0;

	if (newT5gen1 < oldT5gen1)
	{
		deltaT5gen1 = (0x10000 - oldT5gen1)  + newT5gen1;	//calculate delta for wrapped timer.
	}
	else
	{
		deltaT5gen1 = newT5gen1 - oldT5gen1;				//calculate delta.
	}

	if(Lock && daq_mux_id==GEN)
	{
//changeT0:
//		if (T0<0xFF02)
//		if (osc_gen_sync || T0>0xFFFD)
		if (osc_gen_sync || T0<0xFF02)	//has wrapped and comming back towards osc_reset.
		{
			if (deltaT5gen1>(last_deltaT5gen1-generator_transfer_rolloff))
			{
				if (deltaT5gen1<(last_deltaT5gen1+generator_transfer_rolloff))
				{
		//			CC6IR = 1;		// request transient interrupt at cycle_reset.
					T0 = osc_reset;	//SYNC.
					osc_gen_sync=YES;
				}
				else
				{
//					deltaT5gen1 = (last_deltaT5gen1+deltaT5gen1) >> 1;	//try 1/2 way there...
//					goto changeT0;
					Lock = 0;
				}
			}
			else
			{
//				deltaT5gen1 = (last_deltaT5gen1+deltaT5gen1) >> 1;	//try 1/2 way there...
//				goto changeT0;
				Lock = 0;
			}
		}
	}

	if (!meter_calc)							//do not interrupt meas_all().
	{
		if(ff)
		{
			ADCON=0;
			ADCIC=0;
			CC29IC=0;

//			if (adc_samples)
//			{
//				adc_samples = adc_samples-1;	//remove last sample...or not.
//			}
			meter_calc = 1; 				// enable meas_all() calculations.
		}
		else
		{
			ADCIC = 0x0076;						// enable metering interrupt
//			ADCON = 0xF0B7;						// start adc read
//			ADCON = 0x10B7;						// start adc read
///			ADCON = 0x30B7;						// start adc read
///
			tempADCON	= 0x30B5 & 0x0FFF;			// clear ADCTC & ADSTC
			ADCON 	= (adcRate<<12) | tempADCON;		//ADCON ADCTC & ADSTC// start adc read
///		

//			CC29IC = 0x0077;					// enable sync interrupt.
		}
		ff = ff^1;								//toggle ff
	}

//	if (display_type==GENERATOR_POWER && display_function==F4)
	{
		CC29IC = 0x0077;	//re-enable local generator sync interrupt.
	}
}
/*---------------------------------------------------------------------------*/
interrupt(0x36) using (GEN2_SYNC_RB) void	// CC22INT.
gen2_sync()
{
//  register unsigned osc_reset=0xFF02;	//SYNC DEAD-ON.
  register unsigned osc_reset=0xFF08;	//LEAD BY 8 degrees TO COMPENSATE FOR PHASE DELAY IN GEN_SYNC.

	CC22IC = 0;					// disable GEN sync interrupt.

	oldT5gen2 = newT5gen2;
	newT5gen2 = T5;
	last_deltaT5gen2=deltaT5gen2;
	sync_1_count=T0;

	if (newT5gen2 < oldT5gen2)
	{
		deltaT5gen2 = (0x10000 - oldT5gen2)  + newT5gen2;	//calculate delta for wrapped timer.
	}
	else
	{
		deltaT5gen2 = newT5gen2 - oldT5gen2;				//calculate delta.
	}

	if(Lock && daq_mux_id==SYS)
	{
		if (osc_gen_sync || T0<0xFF02)
		{
			if (deltaT5gen2>(last_deltaT5gen2-generator_transfer_rolloff))
			{
				if (deltaT5gen2<(last_deltaT5gen2+generator_transfer_rolloff))
				{
					osc_gen_sync=YES;
					CC6IR = 1;		// request transient interrupt at cycle_reset.
					T0 = osc_reset;	//SYNC.
				}
				else
				{
					Lock = 0;
				}
			}
			else
			{
				Lock = 0;
			}
		}
	}

	if (!meter_calc)							//do not interrupt meas_all().
	{
		if(ff)
		{
			ADCON=0;
			ADCIC=0;
			CC22IC=0;

//			if (adc_samples)
//			{
//				adc_samples = adc_samples-1;	//remove last sample...or not.
//			}
			meter_calc = 1; 				// enable meas_all() calculations.
		}
		else
		{
			ADCIC = 0x0076;						// enable metering interrupt
//			ADCON = 0xF0B7;						// start adc read
//			ADCON = 0x10B7;						// start adc read
///			ADCON = 0x30B7;						// start adc read
///
			tempADCON	= 0x30B5 & 0x0FFF;			// clear ADCTC & ADSTC
			ADCON 	= (adcRate<<12) | tempADCON;		//ADCON ADCTC & ADSTC// start adc read
///		

//			CC22IC = 0x0073;					// enable sync interrupt.
		}
		ff = ff^1;								//toggle ff
	}

//	if (display_type==GENERATOR_POWER && display_function==F4)
	{
		CC22IC = 0x0073;	// enable sync interrupt.
	}
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
interrupt(0x37) using (SYNC_RB) void	// CC23INT.
output_sync()
{
//  register unsigned osc_reset=0xFF07;

#ifdef TIME_TICKS_OUT
_putbit(1,P3,5);	//P3.5 => Port P3A-6 => output high.	//out_sync
#endif //TIME_TICKS_OUT

	CC23IC = 0;					// disable sync interrupt.

	oldT5 = newT5;
	newT5 = T5;

	if (newT5 < oldT5)
	{
		deltaT5 = (0x10000 - oldT5)  + newT5;	//calculate delta for wrapped timer.
	}
	else
	{
		deltaT5 = newT5 - oldT5;				//calculate delta.
	}

	if (!meter_calc)							//do not interrupt meas_all().
	{
		if(ff)
		{
			ADCON=0;
			ADCIC=0;
			CC23IC=0;

//			if (adc_samples)
//			{
//				adc_samples = adc_samples-1;	//remove last sample...or not.
//			}
			meter_calc = 1; 				// enable meas_all() calculations.
		}
		else
		{
			ADCIC = 0x0076;						// enable metering interrupt
//			ADCON = 0xF0B7;						// start adc read
//			ADCON = 0x10B3;						// start adc read
//			ADCON = 0x10B7;						// start adc read
///			ADCON = 0x30B7;						// start adc read
///
			tempADCON	= 0x30B5 & 0x0FFF;			// clear ADCTC & ADSTC
			ADCON 	= (adcRate<<12) | tempADCON;		//ADCON ADCTC & ADSTC// start adc read
///		

			CC23IC = 0x0077;					// enable sync interrupt.
		}
		ff = ff^1;								//toggle ff
	}

#ifdef TIME_TICKS_OUT
_putbit(0,P3,5);	//P3.5 => Port P3A-6 => output low.		//out_sync
#endif //TIME_TICKS_OUT
}
/*---------------------------------------------------------------------------*/
interrupt(0x28) using (ADC_RB) void	// ADCINT.
adc_read()
{
register int data;
register unsigned channel;

#ifdef TIME_TICKS_OUT
_putbit(1,P3,8);	//P3.8 => Port P3A-8 => output high.	//adc_read
#endif //TIME_TICKS_OUT

//	while (_getbit(ADCON,8));		//stall if busy.

	data = ADDAT;						// get data from previous conversion
	channel = data & 0xF000;			//get channel number now (could get it from ADDAT before stripped off: channel=data&0xF000;)
	channel = channel>>12;

	switch (channel)					// look only at channel #
	{
		case 7: //SPARE
				break;

		case 6: //SPARE   
				break;

		case 5: //C_AMPS	
  				*c_amps = data;		//store data
  				c_amps++;
//				ci++;
				break;

		case 4: //B_AMPS	
				*b_amps = data;		//store data
				b_amps++;
//				bi++;
				break;

		case 3: //A_AMPS  
  				*a_amps = data;		//store data
  				a_amps++;
//				ai++;
				break;

		case 2: //C_VOLTS	
				*c_volts = data;		//store data
				c_volts++;
//				cv++;
				break;

		case 1: //B_VOLTS  
				*b_volts = data;		//store data
				b_volts++;
//				bv++;
				break;

		case 0: //A_VOLTS	
				*a_volts = data;		//store data
				a_volts++;
//				av++;

				if (adc_samples < METER_DATA_BUF_SIZE)
				{
					adc_samples++;
				}
				
				if (adc_samples >= METER_DATA_BUF_SIZE)
				{
					ADCIC=0;
					ADCON=0;
				}
				break;

		default: //error cond.
//					adc_samples=0;
//					lcd_display("ADC CHANNEL INVALID--INTERNAL ERROR.",1,1);
//					while((P8&0xF)==0xF)	_srvwdt();	// wait for any key press
				ADCIC=0;
				ADCON=0;
				break;
 	}
#ifdef TIME_TICKS_OUT
 _putbit(0,P3,8);	//P3.8 => Port P3A-8 => output low.		//adc_read
#endif //TIME_TICKS_OUT
 }
/*---------------------------------------------------------------------------*/
/*interrupt(0x29) using (ADC_RB) void	// ADEIR
adc_overrun()
{
	clear_screen();
	lcd_display("CPU EXCEPTION ENCOUNTERED: \nADC OVERRUN ERROR:  \nADCON =      ",1,1);
	lcd_display(ltoX((long)ADCON),3,10);
	while((P8&0xF)==0xF)
		_srvwdt();		   		// wait for any key press
}
/*---------------------------------------------------------------------------*/
/*interrupt(0x) using (ilim_RB) void	// INT.
dclowline()	// pfc diode protection interrupt.	//monitor DC rails for lowline fault.
{
}
/*---------------------------------------------------------------------------*/

/* ============================================================================================================ */
/* ======================================================================================= FUNCTION DEFINITIONS */
/*---------------------------------------------------------------------------*/
void initialize_metering_ram()			// initializes battery-backed-up RAM variables for osc calib.,etc..
{
	adc_samples = ADC_SAMPLES;		/* default number of samples in metering one waveform */

	Kmeter_Vgen_A = Kmeter_Vgen_B = Kmeter_Vgen_C = 1.00;	//Generator Voltage Kfactors.
	Kmeter_Igen_A = Kmeter_Igen_B = Kmeter_Igen_C = 1.00;	//Generator Current Kfactors.
	Kmeter_Vsp1_1 = Kmeter_Vsp1_A = Kmeter_Vsp1_B = Kmeter_Vsp1_C = 1.00;	//Shore Power #1 Voltage Kfactors.
	Kmeter_Isp1_1 = Kmeter_Isp1_A = Kmeter_Isp1_B = Kmeter_Isp1_C = 1.00;	//Shore Power #1 Current Kfactors.
	Kmeter_Vsp2_1 = Kmeter_Vsp2_A = Kmeter_Vsp2_B = Kmeter_Vsp2_C = 1.00;	//Shore Power #2 Voltage Kfactors.
	Kmeter_Isp2_1 = Kmeter_Isp2_A = Kmeter_Isp2_B = Kmeter_Isp2_C = 1.00;	//Shore Power #2 Current Kfactors.
	Kmeter_Vsys_A = Kmeter_Vsys_B = Kmeter_Vsys_C = 1.00;	//System Voltage Kfactors.
	Kmeter_Isys_A = Kmeter_Isys_B = Kmeter_Isys_C = 1.00;	//System Current Kfactors.
	Kmeter_Vaux_A = Kmeter_Vaux_B = Kmeter_Vaux_C = 1.00;	//Aux In Voltage Kfactors.
	Kmeter_Iaux_A = Kmeter_Iaux_B = Kmeter_Iaux_C = 1.00;	//Aux In Current Kfactors.

	Kmeter_Vint_A = 1;			// Correct Vmetering based on EXT ref cal.
	Kmeter_Vint_B = 1;
	Kmeter_Vint_C = 1;
	Kmeter_Vext_A = 1;
	Kmeter_Vext_B = 1;
	Kmeter_Vext_C = 1;
	Kmeter_I_A = 1;
	Kmeter_I_B = 1;
	Kmeter_I_C = 1;
	
//	adcRate = 3;	//DEFAULT
	adcRate = 9;	//DEFAULT	64spc@60Hz, 76spc@50Hz

//	XFMR_ratio=GP_XFMR_RATIO;
//	amps_volts=amps2volts;
//	Vint_volts=volts2volts;				// VINT_VOLTS volts = 1 volt standard.
//	Vext_volts=VEXT_VOLTS;				// GP EXTERNAL CONVERTER VOLTAGE SCALING.
}
/*---------------------------------------------------------------------------*/
void meas_all()		// meas and calc values now, return with meter_data[] formated.
{
#ifdef TEST_FIXTURE
	dummy_meter_data(daq_mux_id);
	if (modbusActive) remoteManager();
#else

  int i;								// general purpose index variable.
  unsigned long t0;
  float rated_amps;
  float shoreCord=(float)shoreCordRating;
  float rated_power=RATED_POWER;

//	while (wait_10ms>0xFFF0)
//		lcd_display("waiting . . .",1,1);	//wait for 10ms counter to wrap.
//		;	//wait for 10ms counter to wrap.

	init_meas();						// start data acquistion interrupts.

#ifdef	TEST_FW
	t0=wait_10ms+10;
#else
	t0=wait_10ms+5;
#endif	//TEST_FW

	while(!meter_calc)					// wait for data acquistion to finish.
	{
		if (t0<wait_10ms)			// timeout after 50 milliseconds.
		{																	 			
			clear_meter_data(daq_mux_id);
			
			ADCIC=0;
			ADCON=0;
			
			if(!Lock)
			{
				CC21IC = 0;			// disable SP1_sync interrupt.
				CC23IC = 0;			// disable OSC_sync interrupt.
				if (!(!MULTIGEN_OPTION && display_type==GENERATOR_POWER && display_function==F4))
				{
					CC22IC = 0;			// disable GEN2_sync interrupt.
					CC29IC = 0;			// disable GEN1_sync interrupt.
				}
			}
			
			if (daq_mux_id==SP1 && gp_state.inpstat1!=ONLINE) return;
			if (daq_mux_id==SP2 && gp_state.inpstat2!=ONLINE) return;
			if (daq_mux_id==OSC && gp_state.invstat!=ONLINE) return;
			if (daq_mux_id==SYS && gp_state.genstat2!=ONLINE) return;
			if (daq_mux_id==GEN && gp_state.genstat1!=ONLINE) return;

			if (!MULTIGEN_OPTION)
			{
				gp_diagnostic_code=-28;
				undetected_sync_signal_source=(int)daq_mux_id;
				set_sysstat(WARNING);
			}

//			if(!Lock)
//			{
//				CC21IC = 0;			// disable SP1_sync interrupt.
//				CC23IC = 0;			// disable OSC_sync interrupt.
//				if (!(!MULTIGEN_OPTION && display_type==GENERATOR_POWER && display_function==F4))
//				{
//					CC22IC = 0;			// disable GEN2_sync interrupt.
//					CC29IC = 0;			// disable GEN1_sync interrupt.
//				}
//			}
			return;
		}
//#ifdef MODBUS
	if (modbusActive) remoteManager();
//#endif //MODBUS
	}

	meas_3phase();	//this function presently requires approx. 420ms to execute.			

	gp_Van[daq_mux_id]		=	Van;
	gp_Vbn[daq_mux_id]		=	Vbn;
	gp_Vcn[daq_mux_id]		=	Vcn;

	gp_Vab[daq_mux_id]		=	Vab;
	gp_Vbc[daq_mux_id]		=	Vbc;
	gp_Vca[daq_mux_id]		=	Vca;

	gp_Ia_rms[daq_mux_id]	=	Ia_rms;
	gp_Ib_rms[daq_mux_id]	=	Ib_rms;
	gp_Ic_rms[daq_mux_id]	=	Ic_rms;

	gp_Ia_pk[daq_mux_id]	=	Ia_pk;
	gp_Ib_pk[daq_mux_id]	=	Ib_pk;
	gp_Ic_pk[daq_mux_id]	=	Ic_pk;

	gp_KWa[daq_mux_id]		=	KWa;
	gp_KWb[daq_mux_id]		=	KWb;
	gp_KWc[daq_mux_id]		=	KWc;

	gp_KVAa[daq_mux_id]		=	KVAa;
	gp_KVAb[daq_mux_id]		=	KVAb;
	gp_KVAc[daq_mux_id]		=	KVAc;

	gp_PFa[daq_mux_id]		=	PFa;
	gp_PFb[daq_mux_id]		=	PFb;
	gp_PFc[daq_mux_id]		=	PFc;

	gp_ICFa[daq_mux_id]		=	ICFa;
	gp_ICFb[daq_mux_id]		=	ICFb;
	gp_ICFc[daq_mux_id]		=	ICFc;

	gp_Van_DC[daq_mux_id]	=	Van_DC;
	gp_Vbn_DC[daq_mux_id]	=	Vbn_DC;
	gp_Vcn_DC[daq_mux_id]	=	Vcn_DC;

///
//  float KVAR_a=sqrt((gp_KVAa[mux_id]*gp_KVAa[mux_id])-(gp_KWa[mux_id]*gp_KWa[mux_id]));
//  float KVAR_b=sqrt((gp_KVAb[mux_id]*gp_KVAb[mux_id])-(gp_KWb[mux_id]*gp_KWb[mux_id]));
//  float KVAR_c=sqrt((gp_KVAc[mux_id]*gp_KVAc[mux_id])-(gp_KWc[mux_id]*gp_KWc[mux_id]));
///

	if (daq_mux_id==GEN && (deltaT5gen1 > 0))
	{
		gp_Freq[daq_mux_id] 	= 	TICK_COUNT/(float)deltaT5gen1;	//calculate frequency from tick count.
	}
	else if (daq_mux_id==SYS && (deltaT5gen2 > 0))				
	{					 
		gp_Freq[daq_mux_id] 	= 	TICK_COUNT/(float)deltaT5gen2;	//calculate frequency from tick count.
	}
	else if (deltaT5 > 0)
	{					 
		gp_Freq[daq_mux_id] 	= 	TICK_COUNT/(float)deltaT5;	//calculate frequency from tick count.
	}

	if (daq_mux_id==GEN || daq_mux_id==SYS)
	{
		if (gp_Freq[daq_mux_id]>maxGenFreq) maxGenFreq=gp_Freq[daq_mux_id];
		if (gp_Freq[daq_mux_id]<minGenFreq) minGenFreq=gp_Freq[daq_mux_id];
	}
//	gp_Freq[daq_mux_id]		=	meas_freq(daq_mux_id); 		//measure frequency

// ================================================================================================
	switch (daq_mux_id)
	{
		case OSC:										//INTERNAL CONVERTER POWER
					if (GP_OUTPUT_FORM==ONE_PHASE)
					{
						gp_KWa[daq_mux_id]		=	KWa + KWb;
						gp_KVAa[daq_mux_id]		=	KVAa + KVAb;
					}

					rated_power = getCapacity();

					if (shoreCord < OUTPUT_RATED_AMPS)
						rated_amps = rated_power / gp_Van[daq_mux_id];

		case EXT:										//EXTERNAL CONVERTER POWER
		case SYS:										//EXTERNAL CONVERTER POWER
		case GEN:										//GENERATOR POWER POWER
					if(GP_OUTPUT_FORM==THREE_PHASE)
					{
						rated_power = rated_power/3;
					}

					if(GP_OUTPUT_FORM==TWO_PHASE)
					{
						rated_power = rated_power * 0.5;
					}

					rated_amps = OUTPUT_RATED_AMPS;
					break;
		case SP1:										//SHORE POWER #1 POWER
		case SP2:										//SHORE POWER #2 POWER
					gp_Vab[daq_mux_id] = gp_Van[daq_mux_id];
					gp_Vbc[daq_mux_id] = gp_Vbn[daq_mux_id];
					gp_Vca[daq_mux_id] = gp_Vcn[daq_mux_id];

					if (gp_state.range_mode_id==LOW_RANGE)
					{
						rated_amps = IN_RATED_AMPS_LO_ONE;
					}
					else
					{
					 	rated_amps = IN_RATED_AMPS_HI_ONE;
					}

					if (gp_state.input1_form==THREE_PHASE)
					{
						rated_power = rated_power/3;

						gp_KWa[daq_mux_id]		=	KWa * ONE_SQRT3;
						gp_KWb[daq_mux_id]		=	KWb * ONE_SQRT3;
						gp_KWc[daq_mux_id]		=	KWc * ONE_SQRT3;

						gp_KVAa[daq_mux_id]		=	KVAa * ONE_SQRT3;
						gp_KVAb[daq_mux_id]		=	KVAb * ONE_SQRT3;
						gp_KVAc[daq_mux_id]		=	KVAc * ONE_SQRT3;
					}
					else	// ONE_PHASE
					{
						if (gp_state.inpstat2==ONLINE)
						{
							rated_amps = rated_amps * 0.75;
						}
						else
						{
							rated_amps = rated_amps * 1.5;
						}
					}

					if (shoreCord < rated_amps) rated_amps = shoreCord;

					break;
		default:										//other POWER
					rated_amps = OUTPUT_RATED_AMPS;
					break;

	}


//	if (daq_mux_id == OSC) {
//		Debug_Var[0] = rated_power;
//		Debug_Var[1] = rated_amps;
//		Debug_Var[2] = gp_Ia_rms[daq_mux_id];
//	}

	gp_level_a[daq_mux_id] = calculate_level(	rated_power, 
												rated_amps, 
												gp_KWa[daq_mux_id], 
												gp_KVAa[daq_mux_id], 
												gp_Ia_rms[daq_mux_id]);

	gp_level_b[daq_mux_id] = calculate_level(	rated_power, 
												rated_amps, 
												gp_KWb[daq_mux_id], 
												gp_KVAb[daq_mux_id], 
												gp_Ib_rms[daq_mux_id]);

	gp_level_c[daq_mux_id] = calculate_level(	rated_power, 
												rated_amps, 
												gp_KWc[daq_mux_id], 
												gp_KVAc[daq_mux_id], 
												gp_Ic_rms[daq_mux_id]);

// ================================================================================================

 	for (i=0; i<8; i++) gp_OverRange[daq_mux_id][i] = OverRange[i]; //set Overrange flags.

#endif //~TEST_FIXTURE

	if (daq_mux_id==OSC || daq_mux_id==SP1)
	{										//determine SYSTEM LEVEL
		system_level=gp_level_a[OSC];

		if (system_level < gp_level_b[OSC])
		{
			system_level = gp_level_b[OSC];
		}

		if (system_level < gp_level_c[OSC])
		{
			system_level = gp_level_c[OSC];
		}

		if (system_level < gp_level_a[SP1])
		{
			system_level = gp_level_a[SP1];
		}

		if (system_level < gp_level_b[SP1])
		{
			system_level = gp_level_b[SP1];
		}

		if (system_level < gp_level_c[SP1])
		{
			system_level = gp_level_c[SP1];
		}

		updateLoadManagementSignals(system_level);	//v3.00 LoadManagementPlcInterfaceOption

	//system_kva
		system_kva=gp_KVAa[OSC] + gp_KVAb[OSC] + gp_KVAc[OSC];

		if (system_kva < (gp_KVAa[SP1] + gp_KVAb[SP1] + gp_KVAc[SP1]))
			system_kva = gp_KVAa[SP1] + gp_KVAb[SP1] + gp_KVAc[SP1];

	//system_kw
		system_kw=gp_KWa[OSC] + gp_KWb[OSC] + gp_KWc[OSC];

		if (system_kw < (gp_KWa[SP1] + gp_KWb[SP1] + gp_KWc[SP1]))
			system_kw = gp_KWa[SP1] + gp_KWb[SP1] + gp_KWc[SP1];

	}

//
// set maximum input levels & power
//
	if (maxLevelIn1 < gp_level_a[SP1]) 	maxLevelIn1 = gp_level_a[SP1];
	if (maxLevelIn1 < gp_level_b[SP1]) 	maxLevelIn1 = gp_level_b[SP1];
	if (maxLevelIn1 < gp_level_c[SP1]) 	maxLevelIn1 = gp_level_c[SP1];
	if (maxPowerIn1 < (gp_KWa[SP1] + gp_KWb[SP1] + gp_KWc[SP1])) maxPowerIn1 = (gp_KWa[SP1] + gp_KWb[SP1] + gp_KWc[SP1]);

//
// set maximum system levels & power
//
	if (maxSystemLevel < system_level) maxSystemLevel = system_level;
	if (maxSystemPower < system_kw) maxSystemPower = system_kw;

	averSystemLevel = (averSystemLevel + system_level) / 2.0;
	averSystemPower = (averSystemPower + system_kw) / 2.0;

//
//Watt-Hour Meter
//
//	if (wattHourMeterON && daq_mux_id==SP1)
	if (wattHourMeterON)
	{
		if (wattTime >= lastWattTime)	//accumulate every 10sec.s
		{
			lastWattTime = lastWattTime+1000;
			wattHours = wattHours + ((gp_KWa[SP1] + gp_KWb[SP1] + gp_KWc[SP1]) / 360.0);
		}
	}

//	refreshModbusData();	//got fresh data, so, refresh mb data
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void init_meas()	// enable metering interrupts
{
		a_volts = A_VOLTS * METER_DATA_BUF_SIZE + meter_data;   // calc meter_data pointer for this channel.
		a_amps =  A_AMPS  * METER_DATA_BUF_SIZE + meter_data;   // calc meter_data pointer for this channel.
		b_volts = B_VOLTS * METER_DATA_BUF_SIZE + meter_data;   // calc meter_data pointer for this channel.
		b_amps =  B_AMPS  * METER_DATA_BUF_SIZE + meter_data;   // calc meter_data pointer for this channel.
		c_volts = C_VOLTS * METER_DATA_BUF_SIZE + meter_data;   // calc meter_data pointer for this channel.
		c_amps =  C_AMPS  * METER_DATA_BUF_SIZE + meter_data;   // calc meter_data pointer for this channel.

		adc_samples = 0;
		meter_calc = 0;						// disable calculations

//		ai=av=bi=bv=ci=cv=0;

//		for (i=0;i<METER_ARRAY;i++)
//		{
//			meter_data[i] = 0;
//		}

//		clear_meter_data(daq_mux_id);
#ifndef MM3
		if (EXT_XFMR_OPTION && (daq_mux_id==OSC))
		{
			volts2volts = VEXT_VOLTS * XFMR_ratio;
			amps2volts = AMPS_VOLTS / XFMR_ratio;

			Vext_volts=VEXT_VOLTS;				// GP EXTERNAL CONVERTER VOLTAGE SCALING.
			Vint_volts=volts2volts;				// VINT_VOLTS volts = 1 volt standard.
			amps_volts=amps2volts;
		}
		else if (EXT_XFMR_OPTION && (daq_mux_id==GEN))
		{
			volts2volts = VEXT_VOLTS;
			amps2volts = AMPS_VOLTS;

			Vext_volts=VEXT_VOLTS;				// GP EXTERNAL CONVERTER VOLTAGE SCALING.
			Vint_volts=volts2volts;				// VINT_VOLTS volts = 1 volt standard.
			amps_volts=amps2volts;
		}
		else if (EXT_XFMR_OPTION && (daq_mux_id==SYS))
		{
			volts2volts = VEXT_VOLTS;
			amps2volts = AMPS_VOLTS;

			Vext_volts=VEXT_VOLTS;				// GP EXTERNAL CONVERTER VOLTAGE SCALING.
			Vint_volts=volts2volts;				// VINT_VOLTS volts = 1 volt standard.
			amps_volts=amps2volts;
		}
//		else if (GP_OUTPUT_VOLTAGE>270.0)		//06162015 DHK Commented Out
		else if (Set_Output_Voltage>270.0)		//06162015 DHK
		{
			Vext_volts=VEXT_VOLTS;			// GP EXTERNAL CONVERTER VOLTAGE SCALING.
			Vint_volts=VINT_VOLTS*2.0;				// VINT_VOLTS volts = 1 volt standard.
			amps_volts=AMPS_VOLTS;
		}
		else
#endif //~MM3
		{
			Vext_volts=VEXT_VOLTS;				// GP EXTERNAL CONVERTER VOLTAGE SCALING.
			Vint_volts=VINT_VOLTS;				// VINT_VOLTS volts = 1 volt standard.
			amps_volts=AMPS_VOLTS;
		}

		ff = 0;

		meas_start();
}
/*---------------------------------------------------------------------------*/
void meas_start()	// enable metering interrupts
{
	if (MULTIGEN_BESECKE) MULTIGEN_OPTION=FALSE;

		if (sync_mux_id==OSC_sync)
		{
//#ifdef TEST_FIXTURE
//	if (!(!MULTIGEN_OPTION && display_type==GENERATOR_POWER && display_function==F4))
//	{
//		CC22IC = 0;			// disable GEN2_sync interrupt.
//	}
//	CC21IC = 0;			// disable SP1_sync interrupt.
//	CC23IC = 0;			// disable OUTPUT_sync interrupt.
//	CC29IC = 0x0077;	// enable GEN1_sync interrupt.
//#else
			if (!(!MULTIGEN_OPTION && display_type==GENERATOR_POWER && display_function==F4))
			{
				CC22IC = 0;			// disable GEN2_sync interrupt.
				CC29IC = 0;			// disable GEN1_sync interrupt.
			}
			CC21IC = 0;			// disable SP1_sync interrupt.
			CC23IC = 0x0077;	// enable OUTPUT_sync interrupt.
//#endif //TEST_FIXTURE
		}

		if (sync_mux_id==SP1A_sync) 
		{
			if (!(!MULTIGEN_OPTION && display_type==GENERATOR_POWER && display_function==F4))
			{
				CC22IC = 0;			// disable GEN2_sync interrupt.
				CC29IC = 0;			// disable GEN1_sync interrupt.
			}
			CC23IC = 0;			// disable OUTPUT_sync interrupt.
			CC21IC = 0x0077;	// enable SP1_sync interrupt.
		}

		if (sync_mux_id==GEN_sync) 
		{
			if (!(!MULTIGEN_OPTION && display_type==GENERATOR_POWER && display_function==F4))
			{
				CC22IC = 0;			// disable GEN2_sync interrupt.
			}
			CC21IC = 0;			// disable SP1_sync interrupt.
			CC23IC = 0;			// disable OUTPUT_sync interrupt.
			CC29IC = 0x0077;	// enable GEN1_sync interrupt.
		}

		if (sync_mux_id==SYS_sync) //USING SYS FOR GENSET #2 METERING.
		{
			if (!(!MULTIGEN_OPTION && display_type==GENERATOR_POWER && display_function==F4))
			{
				CC29IC = 0;			// disable GEN1_sync interrupt.
			}
			CC21IC = 0;			// disable SP1_sync interrupt.
			CC23IC = 0;			// disable OUTPUT_sync interrupt.
			CC22IC = 0x0073;	// enable GEN2_sync interrupt.
		}

	if (MULTIGEN_BESECKE) MULTIGEN_OPTION=TRUE;
}
/*---------------------------------------------------------------------------*/
float meas_freq(int mux_id)		//calculate external frequency, (32/CLK=period)/deltaT5
{														
  float temp_deltaT5;
  float frequency=0.0;

	temp_deltaT5 = (float) deltaT5;	//get count calculated in _sync() interrupt.

	if (temp_deltaT5)				
	{					 
		frequency = TICK_COUNT/temp_deltaT5;	//calculate frequency from tick count.
	}
	return gp_Freq[mux_id] = frequency;
}
/*---------------------------------------------------------------------------*/
/*
float meas_new_freq(int mux_id)		//calculate external frequency, (32/CLK=period)/deltaT5
{														
  unsigned temp_daq_mux_id=daq_mux_id;
  unsigned temp_sync_mux_id=sync_mux_id;
  float temp_deltaT5;
  float frequency=0.0;

	set_meter_mux(mux_id,get_sync_mux_id(mux_id));

	meas_start();

	delay(100);

	temp_deltaT5 = (float) deltaT5;	//get count calculated in adc_sync() interrupt.

	if (temp_deltaT5)				
	{					 
		frequency = TICK_COUNT/temp_deltaT5;	//calculate frequency from tick count.
	}

	set_meter_mux(temp_daq_mux_id, temp_sync_mux_id);	//restore daq & sync mux settings.

	return gp_Freq[mux_id] = frequency;
}
/*---------------------------------------------------------------------------*/
void meas_avg_system_temperature()
{
  static float temps[10];
  static float temp=0.0;
  static int i=0;

	temps[i++] = get_system_temperature();
	if (i==10)
	{
		temp=0.0;
		for (i=0;i<10;i++)	temp+=temps[i];
		temp = temp/10;
		system_temperature = (system_temperature + temp)/2.0;
		i=0;
	}
//		system_temperature = get_system_temperature();
}
/*---------------------------------------------------------------------------*/
float get_system_temperature()
{
	return 0.0;
}
/*
  unsigned temp_daq_mux_id=daq_mux_id;
  unsigned temp_sync_mux_id=sync_mux_id;
  int data=0;
  float temperature;					// 8.5Vdc=100% max. rated temperature

	set_meter_mux(AUX,AUX_sync);

	ADCON = 0x1080;						// start adc read.

	while (_getbit(ADCON,8));			//stall if busy.

	data = ADDAT;						// get data from previous conversion.
	data = (data & 0x03FF) - 512;	//strip-off channel number to get data.

	temperature = data * V_ADC * 10.0;			//scale raw data.

	ADCON = 0;							// disable adc read.

	set_meter_mux(temp_daq_mux_id, temp_sync_mux_id);	//restore daq & sync mux settings.

	return temperature;
}
/*---------------------------------------------------------------------------*/
float get_positive_hvdc()
{
	return 0.0;
}
/*
  unsigned temp_daq_mux_id=daq_mux_id;
  unsigned temp_sync_mux_id=sync_mux_id;
  int data=0;

	set_meter_mux(AUX,AUX_sync);

	ADCON = 0x1081;						// start adc read.

	while (_getbit(ADCON,8));			//stall if busy.

	data = ADDAT;						// get data from previous conversion.
	data = (data & 0x03FF) - 512;	//strip-off channel number to get data.
//	data = data & 0x03FF;			//strip-off channel number to get data.

	positive_hvdc = data * V_ADC * 50.0;			//scale raw data.

	ADCON = 0;							// disable adc read.

	set_meter_mux(temp_daq_mux_id, temp_sync_mux_id);	//restore daq & sync mux settings.

	return positive_hvdc;
}
/*---------------------------------------------------------------------------*/
float get_negative_hvdc()
{
	return 0.0;
}
/*
  unsigned temp_daq_mux_id=daq_mux_id;
  unsigned temp_sync_mux_id=sync_mux_id;
  int data=0;

	set_meter_mux(AUX,AUX_sync);

	ADCON = 0x1082;						// start adc read.

	while (_getbit(ADCON,8));			//stall if busy.

	data = ADDAT;						// get data from previous conversion.
	data = (data & 0x03FF) - 512;		//strip-off channel number to get data.

	negative_hvdc = data * V_ADC * -50.0;			//scale raw data.

	ADCON = 0;							// disable adc read.

	set_meter_mux(temp_daq_mux_id, temp_sync_mux_id);	//restore daq & sync mux settings.

	return negative_hvdc;
}
/*---------------------------------------------------------------------------*/
void clear_meter_data(unsigned mux_id)
{// set measured meter data values to zero for given daq channel.
  int i;

	gp_Freq[mux_id]		=	0;

	gp_Van[mux_id]		=	0;
	gp_Vbn[mux_id]		=	0;
	gp_Vcn[mux_id]		=	0;
	gp_Vab[mux_id]		=	0;
	gp_Vbc[mux_id]		=	0;
	gp_Vca[mux_id]		=	0;
	gp_Ia_rms[mux_id]	=	0;
	gp_Ib_rms[mux_id]	=	0;
	gp_Ic_rms[mux_id]	=	0;
	gp_Ia_pk[mux_id]	=	0;
	gp_Ib_pk[mux_id]	=	0;
	gp_Ic_pk[mux_id]	=	0;
	gp_KWa[mux_id]		=	0;
	gp_KWb[mux_id]		=	0;
	gp_KWc[mux_id]		=	0;
	gp_KVAa[mux_id]		=	0;
	gp_KVAb[mux_id]		=	0;
	gp_KVAc[mux_id]		=	0;
	gp_PFa[mux_id]		=	0;
	gp_PFb[mux_id]		=	0;
	gp_PFc[mux_id]		=	0;
	gp_ICFa[mux_id]		=	0;
	gp_ICFb[mux_id]		=	0;
	gp_ICFc[mux_id]		=	0;

	gp_level_a[mux_id]	=	0;
	gp_level_b[mux_id]	=	0;
	gp_level_c[mux_id]	=	0; 

//	if (mux_id==OSC || mux_id==SP1)
//	{
//		system_level		=	0;			//highest level of input/output A/B/C.
//		system_kva			=	0;			//highest load of input/output A/B/C.
//		system_kw			=	0;			//highest power of input/output A/B/C.
//	}

//	if (mux_id!=OSC)
//	{
//		gp_Freq[mux_id]		=	0;
//	}

	gp_Van_DC[mux_id]	=	0;
	gp_Vbn_DC[mux_id]	=	0;
	gp_Vcn_DC[mux_id]	=	0;
	
	for (i=0;i<METER_DATA_BUF_SIZE;i++)
	{
		waveform[mux_id][A_VOLTS][i] = 0.0;
		waveform[mux_id][B_VOLTS][i] = 0.0;
		waveform[mux_id][C_VOLTS][i] = 0.0;
		waveform[mux_id][A_AMPS][i] = 0.0;
		waveform[mux_id][B_AMPS][i] = 0.0;
		waveform[mux_id][C_AMPS][i] = 0.0;
//		waveform[mux_id][A_KW][i] = 0.0;
//		waveform[mux_id][B_KW][i] = 0.0;
//		waveform[mux_id][C_KW][i] = 0.0;
	}
	waveform_sample_count[mux_id][A_VOLTS] = 0;
	waveform_sample_count[mux_id][B_VOLTS] = 0;
	waveform_sample_count[mux_id][C_VOLTS] = 0;
	waveform_sample_count[mux_id][A_AMPS] = 0;
	waveform_sample_count[mux_id][B_AMPS] = 0;
	waveform_sample_count[mux_id][C_AMPS] = 0;
//	waveform_sample_count[mux_id][A_KW] = 0;
//	waveform_sample_count[mux_id][B_KW] = 0;
//	waveform_sample_count[mux_id][C_KW] = 0;
}
/*---------------------------------------------------------------------------*/
void meas_3phase()
{
	register unsigned N	=adc_samples;  		// N  = number of samples (unsigned int).
 	float n				=(float)N;			// n  = number of samples (float)
	float nn			=n*n;			 	// nn = number of samples SQUARED (float).
	register int i;							// index variable.
//	register int j;							// index variable.
//	int thirty_degrees;						// index variable for current w/30-degree phase shift.
 	float rms;								// working temp var for AC RMS calculations.
	float temp_float=0;

	register int va;						// raw data point, PHASE-A VOLTS.
	register int vb;						// raw data point, PHASE-B VOLTS.
	register int vc;						// raw data point, PHASE-C VOLTS.

	register int ia;						// raw data point, PHASE-A AMPS.
	register int ib;						// raw data point, PHASE-B AMPS.
	register int ic;						// raw data point, PHASE-C AMPS.

  	int van_dc;								// dc on PHASE-A VOLTS channel.
  	int vbn_dc;								// dc on PHASE-B VOLTS channel.
  	int vcn_dc;								// dc on PHASE-C VOLTS channel.

  	int ia_dc;								// dc on PHASE-A AMPS channel.
  	int ib_dc;								// dc on PHASE-B AMPS channel.
  	int ic_dc;								// dc on PHASE-C AMPS channel.

 	int ia_peak=0;							// peak current, PHASE-A AMPS.
 	int ib_peak=0;							// peak current, PHASE-B AMPS.
 	int ic_peak=0;							// peak current, PHASE-C AMPS.

 	float kwa=0.0;				   			// kilo-watts, PHASE-A.
 	float kwb=0.0;				   			// kilo-watts, PHASE-B.
 	float kwc=0.0;				   			// kilo-watts, PHASE-C.

  	float wa;
  	float wb;
  	float wc;

  	float sa;
  	float sb;
  	float sc;

  	float sab;
  	float sbc;
  	float sca;

	float vabX=0.0;
	float vbcX=0.0;
	float vcaX=0.0;

	float vabXX=0.0;
	float vbcXX=0.0;
	float vcaXX=0.0;

 	long vaX=0;								// raw sum, PHASE-A VOLTS.
 	long vbX=0;								// raw sum, PHASE-B VOLTS.
 	long vcX=0;								// raw sum, PHASE-C VOLTS.

   	long iaX=0;								// raw sum, PHASE-A AMPS.
   	long ibX=0;								// raw sum, PHASE-B AMPS.
   	long icX=0;								// raw sum, PHASE-C AMPS.

  	float vaXX=0.0;							// raw sum of squares, PHASE-A VOLTS.
  	float vbXX=0.0;							// raw sum of squares, PHASE-B VOLTS.
  	float vcXX=0.0;							// raw sum of squares, PHASE-C VOLTS.

   	float iaXX=0.0;							// raw sum of squares, PHASE-A AMPS.
   	float ibXX=0.0;							// raw sum of squares, PHASE-B AMPS.
   	float icXX=0.0;							// raw sum of squares, PHASE-C AMPS.

	int *a_volts_meter_data	=A_VOLTS * METER_DATA_BUF_SIZE + meter_data;	// calc meter_data pointer, PHASE-A VOLTS channel.
	int *b_volts_meter_data	=B_VOLTS * METER_DATA_BUF_SIZE + meter_data;	// calc meter_data pointer, PHASE-B VOLTS channel.
	int *c_volts_meter_data	=C_VOLTS * METER_DATA_BUF_SIZE + meter_data;	// calc meter_data pointer, PHASE-C VOLTS channel.

 	int *a_amps_meter_data	=A_AMPS  * METER_DATA_BUF_SIZE + meter_data;	// calc meter_data pointer, PHASE-A AMPS channel.
 	int *b_amps_meter_data	=B_AMPS  * METER_DATA_BUF_SIZE + meter_data;	// calc meter_data pointer, PHASE-B AMPS channel.
 	int *c_amps_meter_data	=C_AMPS  * METER_DATA_BUF_SIZE + meter_data;	// calc meter_data pointer, PHASE-C AMPS channel.

	float a_amps_scale;		// scale factor, PHASE-A AMPS.
	float b_amps_scale;		// scale factor, PHASE-B AMPS.
	float c_amps_scale;		// scale factor, PHASE-C AMPS.

   	float a_volts_scale; 	// scale factor, PHASE-A VOLTS.
   	float b_volts_scale;  	// scale factor, PHASE-B VOLTS.
   	float c_volts_scale;  	// scale factor, PHASE-C VOLTS.

	float i_temp_scale;
	float v_temp_scale;

//	float wa_scale;
//	float wb_scale;
//	float wc_scale;

#ifdef EXCEPTION_HANDLER
        if (adc_samples<=0)
		{
Van=Vbn=Vcn=Vab=Vca=Ia_rms=Ib_rms=Ic_rms=Ia_pk=Ib_pk=Ic_pk=KWa=KWb=KWc=KVAa=KVAb=KVAc=PFa=PFb=PFc=ICFa=ICFb=ICFc=0;
//  			raise_exception(-28);						// adc_samples CORRUPT! Tell user to reset F_span.
			return;
        }
#endif //EXCEPTION_HANDLER

		for (i=0; i<8; i++) OverRange[i]=0;	  			// Clr all Overrange flags.

		switch (daq_mux_id)
		{
			case OSC://INTERNAL CONVERTER POWER
					i_temp_scale	=	I_ADC * amps_volts;	//partial scale calculation.
					i_temp_scale	=	i_temp_scale * 1.08;	//output INCREASE amps:volts by 8%, v1.70

					if((AC75_CONTROL_PCB && RATED_POWER>=50.0) || (RATED_POWER>=75.0) || (NV_MODEL))
//					if((AC75_CONTROL_PCB && RATED_POWER>=55.0) || RATED_POWER>=75.0)
//					if(RATED_POWER>=75.0)
					{
						i_temp_scale = 2 * i_temp_scale;
					}

					v_temp_scale	=	V_ADC * Vint_volts;				//partial scale calculation.

#ifdef TEST_FIXTURE						//TEST_FIXTURE debug mode
					if (gp_state.vout_range==HIGH_RANGE) v_temp_scale = v_temp_scale * 2;
#endif //TEST_FIXTURE

					a_amps_scale	=	Kmeter_I_A * i_temp_scale; 		// scale factor, PHASE-A AMPS.
					b_amps_scale	=	Kmeter_I_B * i_temp_scale; 		// scale factor, PHASE-B AMPS.
					c_amps_scale	=	Kmeter_I_C * i_temp_scale; 		// scale factor, PHASE-C AMPS.
					a_volts_scale	=	Kmeter_Vint_A  * v_temp_scale;	// scale factor, Vint.
					b_volts_scale	=	Kmeter_Vint_B  * v_temp_scale;	// scale factor, Vint.
					c_volts_scale	=	Kmeter_Vint_C  * v_temp_scale;	// scale factor, Vint.
					break;

			case EXT://EXTERNAL CONVERTER POWER
					i_temp_scale	=	I_ADC * amps_volts;				//partial scale calculation.
					i_temp_scale	=	i_temp_scale * 1.08;	//output INCREASE amps:volts by 8%, v1.70

					if((AC75_CONTROL_PCB && RATED_POWER>=50.0) || (RATED_POWER>=75.0) || (NV_MODEL))
					{
						i_temp_scale = 2 * i_temp_scale;
					}

					v_temp_scale	=	V_ADC * Vext_volts;				//partial scale calculation.

					a_amps_scale	=	Kmeter_I_A * i_temp_scale; 		// scale factor, PHASE-A AMPS.
					b_amps_scale	=	Kmeter_I_B * i_temp_scale; 		// scale factor, PHASE-B AMPS.
					c_amps_scale	=	Kmeter_I_C * i_temp_scale; 		// scale factor, PHASE-C AMPS.
					a_volts_scale	=	Kmeter_Vext_A  * v_temp_scale;	// scale factor, Vext.
					b_volts_scale	=	Kmeter_Vext_B  * v_temp_scale;	// scale factor, Vext.
					c_volts_scale	=	Kmeter_Vext_C  * v_temp_scale;	// scale factor, Vext.
					break;

			case SP1://SHORE POWER #1
					if (gp_state.input1_form==ONE_PHASE)
					{
#ifdef DUAL_SHORE_CORD
						if (gp_state.inpstat2==ONLINE)
						{
							i_temp_scale	=	I_ADC * amps_volts;		//partial scale calculation.
						}
						else
						{
							i_temp_scale 	= 	3.0 * I_ADC * amps_volts;
						}

						if(AC75_CONTROL_PCB && RATED_POWER < 50.0)
						{
							i_temp_scale = 0.5 * i_temp_scale;
						}

						i_temp_scale	=	i_temp_scale * 0.87;	//input DECREASE amps:volts by 8%, v1.70

						if (gp_state.inpstat2==ONLINE)
						{
							b_amps_scale	=	Kmeter_Isp1_B * i_temp_scale;	// scale factor, PHASE-B AMPS.
						}
						else
						{
							b_amps_scale	=	0.0;	// scale factor, PHASE-B AMPS.
						}
#else
						i_temp_scale 	= 	3.0 * I_ADC * amps_volts;

						if(AC75_CONTROL_PCB && RATED_POWER < 50.0)
						{
							i_temp_scale = 0.5 * i_temp_scale;
						}
						i_temp_scale	=	i_temp_scale * 0.87;	//input DECREASE amps:volts by 8%, v1.70
						b_amps_scale	=	0.0;	// scale factor, PHASE-B AMPS.
#endif //DUAL_SHORE_CORD

						v_temp_scale	=	V_ADC * Vext_volts;		//partial scale calculation.
						a_amps_scale	=	Kmeter_Isp1_1 * i_temp_scale;	// scale factor, PHASE-A AMPS.
						c_amps_scale	=	0.0;	// scale factor, PHASE-C AMPS.
						a_volts_scale	=	Kmeter_Vsp1_1 * v_temp_scale;	// scale factor, Vext.
						b_volts_scale	=	Kmeter_Vsp1_B * v_temp_scale;	// scale factor, Vext.
						c_volts_scale	= 	v_temp_scale;	// scale factor, Vext.
					}
					else	//assume THREE-PHASE.
					{
#ifdef DUAL_SHORE_CORD
						i_temp_scale	=	I_ADC * amps_volts;		//partial scale calculation.
#else
						i_temp_scale	=	1.73 * I_ADC * amps_volts;		//partial scale calculation.
#endif //DUAL_SHORE_CORD

						if(AC75_CONTROL_PCB && RATED_POWER < 50.0)
						{
							i_temp_scale = 0.5 * i_temp_scale;
						}

						i_temp_scale	=	i_temp_scale * 0.87;	//input DECREASE amps:volts by 8%, v1.70

//						if(gp_state.range_mode_id==LOW_RANGE && (AC75_CONTROL_PCB || RATED_POWER>=70.0))
//						{
//							i_temp_scale = 2 * i_temp_scale;
//						}
						
						if ((gp_state.range_mode_id==LOW_RANGE) && (!LOW_RANGE_INPUT)) {
							if ((AC75_CONTROL_PCB) || (RATED_POWER>=70.0))
							{
								i_temp_scale = 2 * i_temp_scale;
							}
						}

						v_temp_scale	=	V_ADC * Vext_volts;	//partial scale calculation.
						a_amps_scale	=	Kmeter_Isp1_A * i_temp_scale;	// scale factor, PHASE-A AMPS.
						b_amps_scale	=	Kmeter_Isp1_B * i_temp_scale;	// scale factor, PHASE-B AMPS.
						c_amps_scale	=	Kmeter_Isp1_C * i_temp_scale;	// scale factor, PHASE-C AMPS.
						a_volts_scale	=	Kmeter_Vsp1_A  * v_temp_scale;	// scale factor, Vext.
						b_volts_scale	=	Kmeter_Vsp1_B  * v_temp_scale;	// scale factor, Vext.
						c_volts_scale	= 	Kmeter_Vsp1_C  * v_temp_scale;	// scale factor, Vext.
					}
					break;

			case SP2://SHORE POWER #2
					if (gp_state.input2_form==ONE_PHASE)
					{
						i_temp_scale 	= 	3.0 * I_ADC * amps_volts;

						if(AC75_CONTROL_PCB && RATED_POWER < 50.0)
//						if(AC75_CONTROL_PCB && RATED_POWER < 55.0)
//						if(AC75_CONTROL_PCB)
						{
							i_temp_scale = 0.5 * i_temp_scale;
						}

						i_temp_scale	=	i_temp_scale * 0.87;	//input DECREASE amps:volts by 8%, v1.70
						v_temp_scale	=	V_ADC * Vext_volts;		//partial scale calculation.
						a_amps_scale	=	Kmeter_Isp2_1 * i_temp_scale;	// scale factor, PHASE-A AMPS.
						b_amps_scale	=	0.0;	// scale factor, PHASE-B AMPS.
						c_amps_scale	=	0.0;	// scale factor, PHASE-C AMPS.
						a_volts_scale	=	Kmeter_Vsp2_1  * v_temp_scale;	// scale factor, Vext.
						b_volts_scale	=	v_temp_scale;	// scale factor, Vext.
						c_volts_scale	= 	v_temp_scale;	// scale factor, Vext.
					}
					else	//assume THREE-PHASE.
					{
						i_temp_scale	=	1.73 * I_ADC * amps_volts;		//partial scale calculation.

						if(AC75_CONTROL_PCB && RATED_POWER < 50.0)
//						if(AC75_CONTROL_PCB && RATED_POWER < 55.0)
//						if(AC75_CONTROL_PCB)
						{
							i_temp_scale = 0.5 * i_temp_scale;
						}

						i_temp_scale	=	i_temp_scale * 0.87;	//input DECREASE amps:volts by 8%, v1.70

						if(gp_state.range_mode_id==LOW_RANGE && ((AC75_CONTROL_PCB) || (RATED_POWER>=70.0)))
						{
							i_temp_scale = 2 * i_temp_scale;
						}

						v_temp_scale	=	V_ADC * Vext_volts;	//partial scale calculation.
						a_amps_scale	=	Kmeter_Isp2_A * i_temp_scale;	// scale factor, PHASE-A AMPS.
						b_amps_scale	=	Kmeter_Isp2_B * i_temp_scale;	// scale factor, PHASE-B AMPS.
						c_amps_scale	=	Kmeter_Isp2_C * i_temp_scale;	// scale factor, PHASE-C AMPS.
						a_volts_scale	=	Kmeter_Vsp2_A  * v_temp_scale;	// scale factor, Vext.
						b_volts_scale	=	Kmeter_Vsp2_B  * v_temp_scale;	// scale factor, Vext.
						c_volts_scale	= 	Kmeter_Vsp2_C  * v_temp_scale;	// scale factor, Vext.
					}
					break;

			case GEN://GENERATOR POWER
					i_temp_scale	=	I_ADC * amps_volts;				//partial scale calculation.
					i_temp_scale	=	i_temp_scale * 0.87;	//input DECREASE amps:volts by 8%, v1.70

					if (GEN_AMPS_PRESENT)
					{
						i_temp_scale	=	i_temp_scale * 2.0;	//amps:volts 2x greater for Gen's
					}

					v_temp_scale	=	V_ADC * Vint_volts;				//partial scale calculation.

					a_amps_scale	=	Kmeter_Igen_A * i_temp_scale;	// scale factor, PHASE-A AMPS.
					b_amps_scale	=	Kmeter_Igen_B * i_temp_scale;	// scale factor, PHASE-B AMPS.
					c_amps_scale	=	Kmeter_Igen_C * i_temp_scale;	// scale factor, PHASE-C AMPS.
					a_volts_scale	=	Kmeter_Vgen_A  * v_temp_scale;	// scale factor, Vext.
					b_volts_scale	=	Kmeter_Vgen_B  * v_temp_scale;	// scale factor, Vext.
					c_volts_scale	=	Kmeter_Vgen_C  * v_temp_scale;	// scale factor, Vext.
					break;

			case SYS://SYSTEM POWER
					i_temp_scale	=	I_ADC * amps_volts;				//partial scale calculation.
					i_temp_scale	=	i_temp_scale * 0.87;	//input DECREASE amps:volts by 8%, v1.70

					if (GEN_AMPS_PRESENT)
					{
						i_temp_scale	=	i_temp_scale * 2.0;	//amps:volts 2x greater for Gen's
					}

					v_temp_scale	=	V_ADC * Vint_volts;				//partial scale calculation.
#ifdef TEST_FIXTURE
					v_temp_scale	=	2 * v_temp_scale;	//compensate for test fixture.
#endif //TEST_FIXTURE
					a_amps_scale	=	Kmeter_Isys_A * i_temp_scale;	// scale factor, PHASE-A AMPS.
					b_amps_scale	=	Kmeter_Isys_B * i_temp_scale;	// scale factor, PHASE-B AMPS.
					c_amps_scale	=	Kmeter_Isys_C * i_temp_scale;	// scale factor, PHASE-C AMPS.
					a_volts_scale	=	Kmeter_Vsys_A  * v_temp_scale;	// scale factor, Vext.
					b_volts_scale	=  	Kmeter_Vsys_B  * v_temp_scale;	// scale factor, Vext.
					c_volts_scale	=	Kmeter_Vsys_C  * v_temp_scale;	// scale factor, Vext.
					break;

			case AUX://AUX IN
					i_temp_scale	=	I_ADC * amps_volts;				//partial scale calculation.
					v_temp_scale	=	V_ADC * Vint_volts;				//partial scale calculation.

					a_amps_scale	=	Kmeter_Iaux_A * i_temp_scale;	// scale factor, PHASE-A AMPS.
					b_amps_scale	=	Kmeter_Iaux_B * i_temp_scale;	// scale factor, PHASE-B AMPS.
					c_amps_scale	=	Kmeter_Iaux_C * i_temp_scale;	// scale factor, PHASE-C AMPS.
					a_volts_scale	=	Kmeter_Vaux_A  * v_temp_scale;	// scale factor, Vext.
					b_volts_scale	=	Kmeter_Vaux_B  * v_temp_scale;	// scale factor, Vext.
					c_volts_scale	=	Kmeter_Vaux_C  * v_temp_scale;	// scale factor, Vext.
					break;

			case XXX://spare

			default://invalid daq_mux_id
					raise_exception(-33);
					break;
		}

        for (i=0;i<N;i++)
        {
        	va	=	(*(a_volts_meter_data+i) & 0x3FF)-512;		// get raw data and strip off ch #.
        	ia	=	(*(a_amps_meter_data+i)  & 0x3FF)-512;		// get raw data and strip off ch #.
        	*(a_volts_meter_data+i)	=	va;						// restore raw data without ch #.
        	*(a_amps_meter_data+i)	=	ia;						// restore raw data without ch #.

        	vb	=	(*(b_volts_meter_data+i) & 0x3FF)-512;		// get raw data and strip off ch #.
        	ib	=	(*(b_amps_meter_data+i)  & 0x3FF)-512;		// get raw data and strip off ch #.
        	*(b_volts_meter_data+i)	=	vb;						// restore raw data without ch #.
        	*(b_amps_meter_data+i)	=	ib;						// restore raw data without ch #.

        	vc	=	(*(c_volts_meter_data+i) & 0x3FF)-512;		// get raw data and strip off ch #.
        	ic	=	(*(c_amps_meter_data+i)  & 0x3FF)-512;		// get raw data and strip off ch #.
        	*(c_volts_meter_data+i)	=	vc;						// restore raw data without ch #.
        	*(c_amps_meter_data+i)	=	ic;						// restore raw data without ch #.

			if(va < ADC_10_MIN || va > ADC_10_MAX)				// look for Overrange condition.
				OverRange[A_VOLTS] = 1;							// Set flag if overrange.
																
			if(vb < ADC_10_MIN || vb > ADC_10_MAX)				// look for Overrange condition.
				OverRange[B_VOLTS] = 1;							// Set flag if overrange.

			if(vc < ADC_10_MIN || vc > ADC_10_MAX)				// look for Overrange condition.
				OverRange[C_VOLTS] = 1;							// Set flag if overrange.

			if(ia < ADC_10_MIN || ia > ADC_10_MAX)				// look for Overrange condition.
				OverRange[A_AMPS] = 1;							// Set flag if overrange.

			if(ib < ADC_10_MIN || ib > ADC_10_MAX)				// look for Overrange condition.
				OverRange[B_AMPS] = 1;							// Set flag if overrange.

			if(ic < ADC_10_MIN || ic > ADC_10_MAX)				// look for Overrange condition.
				OverRange[C_AMPS] = 1;							// Set flag if overrange.

         	vaX	+=	(long) va;						 			// sum raw data.
         	vbX	+=	(long) vb;						 			// sum raw data.
         	vcX	+=	(long) vc;						 			// sum raw data.

				sa = (float) va * a_volts_scale;					// scale raw data.
				sb = (float) vb * b_volts_scale;					// scale raw data.
				sc = (float) vc * c_volts_scale;					// scale raw data.

	         	sab	=	sa - sb;									// calculate scaled line-line vector sum.
	         	sbc	=	sb - sc;									// calculate scaled line-line vector sum.
	         	sca	=	sc - sa;									// calculate scaled line-line vector sum.

	         	vabX	+=	sab;									// sum scaled raw vector data.
	         	vbcX	+=	sbc;									// sum scaled raw vector data.
	         	vcaX	+=	sca;									// sum scaled raw vector data.

	         	vabXX	+=	(float) sab * sab;						// sum squares.
	         	vbcXX	+=	(float) sbc * sbc;						// sum squares.
	         	vcaXX	+=	(float) sca * sca;						// sum squares.

         	iaX	+=	(long) ia; 									// sum raw data.
         	ibX	+=	(long) ib; 									// sum raw data.
         	icX	+=	(long) ic; 									// sum raw data.

         	vaXX	+=	(float) va * va;						// sum squares.
         	vbXX	+=	(float) vb * vb;						// sum squares.
         	vcXX	+=	(float) vc * vc;						// sum squares.

           	iaXX	+=	(float) ia * ia;						// sum squares.
           	ibXX	+=	(float) ib * ib;						// sum squares.
           	icXX	+=	(float) ic * ic;						// sum squares.

		  	if (ia > ia_peak)
		  	{
		   		ia_peak	=	ia;								   // capture raw peak amps for this cycle.
          	}

		  	if (ib > ib_peak)
		  	{
		   		ib_peak	=	ib;									// capture raw peak amps for this cycle.
          	}

		  	if (ic > ic_peak)
		  	{
		   		ic_peak	=	ic;									// capture raw peak amps for this cycle.
          	}

///			if (new_key) return;

        }

		if (modbusActive) remoteManager();					//05072018 DHK Added

        van_dc	=	(int) vaX / n;
		temp_float = vaX;
		Van_DC	=	(float) temp_float / n * a_volts_scale;
		rms		=	sqrt(vaXX/n-(temp_float*temp_float/nn));
        Van		=	rms * a_volts_scale;
		if (Van < 0.1) Van = 0.0;

        vbn_dc	=	(int) vbX / n;
		temp_float = vbX;
		Vbn_DC	=	(float) temp_float / n * b_volts_scale;
		rms		=	sqrt(vbXX/n-(temp_float*temp_float/nn));
        Vbn		=	rms * b_volts_scale;
		if (Vbn < 0.1) Vbn = 0.0;

        vcn_dc	=	(int) vcX / n;
		temp_float = vcX;
		Vcn_DC	=	(float) temp_float / n * c_volts_scale;
		rms		=	sqrt(vcXX/n-(temp_float*temp_float/nn));
        Vcn		=	rms * c_volts_scale;
		if (Vcn < 0.1) Vcn = 0.0;

        Vab		=	sqrt(vabXX/n-(vabX*vabX/nn));
		if (Vab < 0.1) Vab = 0.0;

        Vbc		=	sqrt(vbcXX/n-(vbcX*vbcX/nn));
		if (Vbc < 0.1) Vbc = 0.0;

        Vca		=	sqrt(vcaXX/n-(vcaX*vcaX/nn));
		if (Vca < 0.1) Vca = 0.0;

        ia_dc	=	(int) iaX / n;								
		temp_float = iaX;
		rms		=	sqrt(iaXX/n-(temp_float*temp_float/nn));
        Ia_rms	=	rms * a_amps_scale;
		if (Ia_rms < 0.01) Ia_rms	=	0.0;

        ib_dc	=	(int) ibX / n;								
		temp_float = ibX;
		rms		=	sqrt(ibXX/n-(temp_float*temp_float/nn));
        Ib_rms	=	rms * b_amps_scale;
		if (Ib_rms < 0.01) Ib_rms	=	0.0;

        ic_dc	=	(int) icX / n;								
		temp_float = icX;
		rms		=	sqrt(icXX/n-(temp_float*temp_float/nn));
        Ic_rms	=	rms * c_amps_scale;
		if (Ic_rms < 0.01) Ic_rms	=	0.0;

		Ia_pk	=	(float) ia_peak;					// remove average DC, or not ...
		Ia_pk	=	Ia_pk - (float) ia_dc;					// remove average DC, or not ...
		Ia_pk	=	Ia_pk * a_amps_scale;					// scale peak amps.
		if (Ia_pk < 0.01) Ia_pk	=	0.0;

		Ib_pk	=	(float) ib_peak;					// remove average DC, or not ...
		Ib_pk	=	Ib_pk - (float) ib_dc;					// remove average DC, or not ...
		Ib_pk	=	Ib_pk * b_amps_scale;					// scale peak amps.
		if (Ib_pk < 0.01) Ib_pk	=	0.0;

		Ic_pk	=	(float) ic_peak;					// remove average DC, or not ...
		Ic_pk	=	Ic_pk - (float) ic_dc;					// remove average DC, or not ...
		Ic_pk	=	Ic_pk * c_amps_scale;					// scale peak amps.
		if (Ic_pk < 0.01) Ic_pk	=	0.0;

        KVAa	=	Van * Ia_rms * 0.001;					// calc kva for 1-phase.
        KVAb	=	Vbn * Ib_rms * 0.001;					// calc kva for 1-phase.
        KVAc	=	Vcn * Ic_rms * 0.001;					// calc kva for 1-phase.

//		IE_main();			//poll remote to speed up comm.

		waveform_sample_count[daq_mux_id][A_VOLTS] = N;
		waveform_sample_count[daq_mux_id][B_VOLTS] = N;
		waveform_sample_count[daq_mux_id][C_VOLTS] = N;
		waveform_sample_count[daq_mux_id][A_AMPS] = N;
		waveform_sample_count[daq_mux_id][B_AMPS] = N;
		waveform_sample_count[daq_mux_id][C_AMPS] = N;
//	waveform_sample_count[daq_mux_id][A_KW] = N;
//	waveform_sample_count[daq_mux_id][B_KW] = N;
//	waveform_sample_count[daq_mux_id][C_KW] = N;

// 		wa_scale = 0.001 * a_volts_scale * a_amps_scale;
// 		wb_scale = 0.001 * b_volts_scale * b_amps_scale;
// 		wc_scale = 0.001 * c_volts_scale * c_amps_scale;

        for (i=0;i<N;i++)
        {
	       	va	=	*(a_volts_meter_data+i) - van_dc;		// get raw data and take out average dc component.
        	ia	=	*(a_amps_meter_data+i)  - ia_dc;		// get raw data and take out average dc component.
			waveform[daq_mux_id][A_VOLTS][i] = (float) va * a_volts_scale;
			waveform[daq_mux_id][A_AMPS][i] = (float) ia * a_amps_scale;
//	       	*(a_volts_waveform_data+i)	=	va;					// store modified (DC compensated) raw data.
//        	*(a_amps_waveform_data+i)	=	ia;					// store modified (DC compensated) raw data.
          	wa	=	(float) va * ia;

//			waveform[daq_mux_id][A_KW][i] =  (float) wa * wa_scale;

       		kwa	+=	wa;

	       	vb	=	*(b_volts_meter_data+i) - vbn_dc;		// get raw data and take out average dc component.
        	ib	=	*(b_amps_meter_data+i)  - ib_dc;		// get raw data and take out average dc component.
			waveform[daq_mux_id][B_VOLTS][i] = (float) vb * b_volts_scale;
			waveform[daq_mux_id][B_AMPS][i] = (float) ib * b_amps_scale;
//	       	*(b_volts_waveform_data+i)	=	vb;					// store modified (DC compensated) raw data.
//        	*(b_amps_waveform_data+i)	=	ib;					// store modified (DC compensated) raw data.
          	wb	=	(float) vb * ib;

//			waveform[daq_mux_id][B_KW][i] =  (float) wb * wb_scale;

       		kwb	+=	wb;

	       	vc	=	*(c_volts_meter_data+i) - vcn_dc;		// get raw data and take out average dc component.
        	ic	=	*(c_amps_meter_data+i)  - ic_dc;		// get raw data and take out average dc component.
			waveform[daq_mux_id][C_VOLTS][i] = (float) vc * c_volts_scale;
			waveform[daq_mux_id][C_AMPS][i] = (float) ic * c_amps_scale;
//	       	*(c_volts_waveform_data+i)	=	vc;					// store modified (DC compensated) raw data.
//        	*(c_amps_waveform_data+i)	=	ic;					// store modified (DC compensated) raw data.
          	wc	=	(float) vc * ic;

//			waveform[daq_mux_id][C_KW][i] =  (float) wc * wc_scale;

       		kwc	+=	wc;

///			if (new_key) return;
        }

//		if (daq_mux_id == SP1) {
//			Debug_Var[0] = van_dc;
//			Debug_Var[1] = ia_dc;
//			Debug_Var[2] = kwa;
//		}
		
		kwa	=	kwa / n;								// average by number of samples per cycle.
		kwa	=	kwa * a_volts_scale * a_amps_scale;		// scale.
		KWa	=	kwa * 0.001;							// scale watts to kilo-watts and save AVERAGE INSTAINOUS POWER.

		kwb	=	kwb / n;								// average by number of samples per cycle.
		kwb	=	kwb * b_volts_scale * b_amps_scale;		// scale.
		KWb	=	kwb * 0.001;							// scale watts to kilo-watts and save AVERAGE INSTAINOUS POWER.

		kwc	=	kwc / n;								// average by number of samples per cycle.
		kwc	=	kwc * c_volts_scale * c_amps_scale;		// scale.
		KWc	=	kwc * 0.001;							// scale watts to kilo-watts and save AVERAGE INSTAINOUS POWER.

		if (KVAa!=0)
        {
			PFa = KWa / KVAa;		   					// calc Pf if KVA !=0, else PF=1.
	        if (PFa > 1.0) PFa = 1.0;
	        if (PFa < -1.0) PFa = -1.0;
	        if (PFa > (-0.01) && PFa<0.01) PFa = 0.0;
        }
        else
		{
			PFa = 1.0;
        }

		if (KVAb!=0)
        {
			PFb = KWb / KVAb;		   					// calc Pf if KVA !=0, else PF=1.
	        if (PFb > 1.0) PFb = 1.0;
	        if (PFb < -1.0) PFb = -1.0;
	        if (PFb > (-0.01) && PFb<0.01) PFb = 0.0;
        }
        else
		{
			PFb = 1.0;
        }

		if (KVAc!=0)
        {
			PFc = KWc / KVAc;		   					// calc Pf if KVA !=0, else PF=1.
	        if (PFc > 1.0) PFc = 1.0;
	        if (PFc < -1.0) PFc = -1.0;
	        if (PFc > (-0.01) && PFc<0.01) PFc = 0.0;
        }
        else
		{
			PFc = 1.0;
        }

		if (KWa < 0.0)
		{
			KWa = -KWa;
		} 

		if (KWb < 0.0)
		{
			KWb = -KWb;
		} 

		if (KWc < 0.0)
		{
			KWc = -KWc;
		} 

		if (Ia_rms!=0)
		{
			ICFa = Ia_pk / Ia_rms;						// calc ICF if Irms !=0, 
		}                                               // else ICF=1.
		else
		{
			ICFa = 1.0;
        }

		if (Ib_rms!=0)
		{
			ICFb = Ib_pk / Ib_rms;						// calc ICF if Irms !=0, 
		}                                               // else ICF=1.
		else
		{
			ICFb = 1.0;
        }

		if (Ic_rms!=0)
		{
			ICFc = Ic_pk / Ic_rms;						// calc ICF if Irms !=0, 
		}                                               // else ICF=1.
		else
		{
			ICFc = 1.0;
        }

		Van_csc = Van;									// one cycle RMS metered output value for csc.
		Vbn_csc = Vbn;									// one cycle RMS metered output value for csc.
		Vcn_csc = Vcn;									// one cycle RMS metered output value for csc.

		if (gp_state.vout_range==HIGH_RANGE) 		//AGC High Range fix added v2.00a
		{
				Van_csc = Van_csc * 0.5;
				Vbn_csc = Vbn_csc * 0.5;
				Vcn_csc = Vcn_csc * 0.5;
		}

		if (ALC_mode && daq_mux_id==OSC)
		{
			CSC();										// Continuously self calibrate, but only if not Trans mode.
		}

		if (modbusActive) remoteManager();					//05072018 DHK Added
//		meter_calc = 0;									// disable calculations.
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// function: 	float calculate_level(float,float,float,float,float);
//
// arguments:	TYPE	NAME			DESCRIPTION
//				float	rated_power		rated Volt-Amperes established for particular system.
//				float	rated_amps		rated Amperes established for particular system.
//				float	watts			metered watage (Note: NOT kW).
//				float	va				metered volt-amperes (Note: NOT kVA).
//				float	amps			metered current.
//
//				<float	derated_power	rated Wattage established for particular system.>
//
// purpose: Calculate percentage of power rating based on greatest of rated VA, rated Wattage, rated Amps.
//
// returns:	Power Level (as a percentage of rating).
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float calculate_level(float rated_power, float rated_amps, float watts, float va, float amps)
{	
  float level_amps, level_watts, level=0.0;
  float power_factor=1.00;

	if (daq_mux_id == OSC)
	{
#ifdef MM3
	if (RATED_POWER==108.0 || RATED_POWER==80.0 || RATED_POWER==150.0)
#else
	if (RATED_POWER==54.0 || RATED_POWER==40.0 || RATED_POWER==125.0)
#endif //~MM3
		{
			power_factor = 0.80;
		}
		else
		{
			power_factor = 0.85;
		}
	}



	if (rated_power > 0.0)
	{
		level_watts  = 	watts / (rated_power * power_factor) * 100.0;	// calculated output Load Level and convert KW to watts.
		level  = 	va / rated_power * 100.0;			// calculated output Load Level and convert KW to watt.
	}

	if (level_watts > level)
	{
		level = level_watts;
	}

	if (rated_amps > 0.0)
	{
		level_amps = amps / rated_amps * 100.0;
	}

	if (level_amps > level)
	{
		level = level_amps;
	}

	if ((level > 999.0) || (level < 0.1))
	{
		level = 0.0;
	}

 	return level;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void dummy_meter_data(unsigned mux_id)
{// set measured meter data values to dummy values for given daq channel.
//  int i,j;
//  float ftemp;
  float rated_amps=OUTPUT_RATED_AMPS;
//  float shoreCord=(float)shoreCordRating;
  float rated_power=RATED_POWER;
  float amps=0.0;
  float tempAmps=0.0;
  int waveSamples;

	GP_OUTPUT_FORM=THREE_PHASE;
	gp_state.input1_form=THREE_PHASE;

	if (mux_id==SP1)
	{
		GP_OUTPUT_VOLTAGE=480.0;
		gp_Freq[mux_id]	=60.0;
		tempAmps=20.0;
	}
	else
	{
//		if (_getbit(P3,3))		//mock input on
		{
			if (_getbit(P8,0))		//v switch
			{
				GP_OUTPUT_VOLTAGE=230.0;
				gp_Freq[mux_id]	=GP_OUTPUT_FREQUENCY=50.0;
				tempAmps=41.7;
			}
			else
			{
				GP_OUTPUT_VOLTAGE=120.0;
				gp_Freq[mux_id]	=GP_OUTPUT_FREQUENCY=60.0;
				tempAmps=60.0;
			}
		}
//		else
//		{
//			GP_OUTPUT_VOLTAGE=0.0;
//			if (_getbit(P3,2))		//mock output on
//			{
//				_putbit(0,P3,2);	//no input so output off
//			}
//		}
	}

//	if (_getbit(P3,2))		//mock output on
	{
		amps=tempAmps;
	}

	gp_Van[mux_id]		=	GP_OUTPUT_VOLTAGE;
	gp_Vbn[mux_id]		=	GP_OUTPUT_VOLTAGE;
	gp_Vcn[mux_id]		=	GP_OUTPUT_VOLTAGE;
	gp_Vab[mux_id]		=	GP_OUTPUT_VOLTAGE*SQRT_OF_3;
	gp_Vbc[mux_id]		=	GP_OUTPUT_VOLTAGE*SQRT_OF_3;
	gp_Vca[mux_id]		=	GP_OUTPUT_VOLTAGE*SQRT_OF_3;
	gp_Ia_rms[mux_id]	=	amps;
	gp_Ib_rms[mux_id]	=	amps;
	gp_Ic_rms[mux_id]	=	amps;
	gp_Ia_pk[mux_id]	=	amps*1.41;
	gp_Ib_pk[mux_id]	=	amps*1.41;
	gp_Ic_pk[mux_id]	=	amps*1.41;
	gp_KWa[mux_id]		=	GP_OUTPUT_VOLTAGE*gp_Ia_rms[mux_id]/1000.0;
	gp_KWb[mux_id]		=	GP_OUTPUT_VOLTAGE*gp_Ib_rms[mux_id]/1000.0;
	gp_KWc[mux_id]		=	GP_OUTPUT_VOLTAGE*gp_Ic_rms[mux_id]/1000.0;
	gp_KVAa[mux_id]		=	gp_KWa[mux_id];
	gp_KVAb[mux_id]		=	gp_KWb[mux_id];
	gp_KVAc[mux_id]		=	gp_KWc[mux_id];
	gp_PFa[mux_id]		=	1.0;
	gp_PFb[mux_id]		=	1.0;
	gp_PFc[mux_id]		=	1.0;
	gp_ICFa[mux_id]		=	1.41;
	gp_ICFb[mux_id]		=	1.41;
	gp_ICFc[mux_id]		=	1.41;

	switch (mux_id)
	{
		case OSC:										//INTERNAL CONVERTER POWER
//					if (GP_OUTPUT_FORM==ONE_PHASE)
//					{
//						gp_KWa[mux_id]		=	gp_KWa[mux_id] + gp_KWb[mux_id];
//						gp_KVAa[mux_id]		=	gp_KVAa[mux_id] + gp_KVAb[mux_id];
//					}

//					rated_power = getCapacity();

//					if (shoreCord < OUTPUT_RATED_AMPS) rated_amps = rated_power / gp_Van[mux_id];

		case EXT:										//EXTERNAL CONVERTER POWER
		case SYS:										//EXTERNAL CONVERTER POWER
		case GEN:										//GENERATOR POWER POWER
					if(GP_OUTPUT_FORM==THREE_PHASE)
					{
						rated_power = rated_power/3;
					}

//					if(GP_OUTPUT_FORM==TWO_PHASE)
//					{
//						rated_power = rated_power * 0.5;
//					}

					rated_amps = OUTPUT_RATED_AMPS;
					break;
		case SP1:										//SHORE POWER #1 POWER
		case SP2:										//SHORE POWER #2 POWER
					gp_Vab[mux_id] = gp_Van[mux_id];
					gp_Vbc[mux_id] = gp_Vbn[mux_id];
					gp_Vca[mux_id] = gp_Vcn[mux_id];

					if (gp_state.range_mode_id==LOW_RANGE)
					{
						rated_amps = IN_RATED_AMPS_LO_ONE;
					}
					else
					{
					 	rated_amps = IN_RATED_AMPS_HI_ONE;
					}

					if (gp_state.input1_form==THREE_PHASE)
					{
						rated_power = rated_power/3;

						gp_KWa[mux_id]		=	gp_KWa[mux_id] * ONE_SQRT3;
						gp_KWb[mux_id]		=	gp_KWb[mux_id] * ONE_SQRT3;
						gp_KWc[mux_id]		=	gp_KWc[mux_id] * ONE_SQRT3;

						gp_KVAa[mux_id]		=	gp_KVAa[mux_id] * ONE_SQRT3;
						gp_KVAb[mux_id]		=	gp_KVAb[mux_id] * ONE_SQRT3;
						gp_KVAc[mux_id]		=	gp_KVAc[mux_id] * ONE_SQRT3;
					}
					else	// ONE_PHASE
					{
						rated_amps = rated_amps * 1.5;
					}

//					if (shoreCord < rated_amps) rated_amps = shoreCord;

					break;
		default:										//other POWER
					rated_amps = OUTPUT_RATED_AMPS;
					break;
	}

	gp_level_a[mux_id] = calculate_level(	rated_power, 
											rated_amps, 
											gp_KWa[mux_id], 
											gp_KVAa[mux_id], 
											gp_Ia_rms[mux_id]);

	gp_level_b[mux_id] = calculate_level(	RATED_POWER, 
											rated_amps, 
											gp_KWb[mux_id], 
											gp_KVAb[mux_id], 
											gp_Ib_rms[mux_id]);

	gp_level_c[mux_id] = calculate_level(	rated_power, 
											rated_amps, 
											gp_KWc[mux_id], 
											gp_KVAc[mux_id], 
											gp_Ic_rms[mux_id]);

////Note: Max waveSamples is METER_DATA_BUF_SIZE = 512
	waveSamples=64;
}
/*

	generateWaveform(waveSamples, gp_Van[mux_id]*SQRT_OF_2, 0.0, &waveform[mux_id][A_VOLTS]);
	waveform_sample_count[mux_id][A_VOLTS] = waveSamples;
	generateWaveform(waveSamples, gp_Vbn[mux_id]*SQRT_OF_2, 120.0, &waveform[mux_id][B_VOLTS]);
	waveform_sample_count[mux_id][B_VOLTS] = waveSamples;
	generateWaveform(waveSamples, gp_Vcn[mux_id]*SQRT_OF_2, 240.0, &waveform[mux_id][C_VOLTS]);
	waveform_sample_count[mux_id][C_VOLTS] = waveSamples;
	generateWaveform(waveSamples, gp_Ia_pk[mux_id], 0.0, &waveform[mux_id][A_AMPS]);
	waveform_sample_count[mux_id][A_AMPS] = waveSamples;
	generateWaveform(waveSamples, gp_Ib_pk[mux_id], 120.0, &waveform[mux_id][B_AMPS]);
	waveform_sample_count[mux_id][B_AMPS] = waveSamples;
	generateWaveform(waveSamples, gp_Ic_pk[mux_id], 240.0, &waveform[mux_id][C_AMPS]);
	waveform_sample_count[mux_id][C_AMPS] = waveSamples;
}
/*---------------------------------------------------------------------------*/
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//	void generateWaveform(int steps, float scale, float phase, float *array)
//
//	purpose:	Generates a floating-point sinewave array.
//	where:		steps is number of waveform steps
//				scale is peak value to scale waveform
//				phase is degrees of phase-shift in waveform
//				array is the address of a floating point array to place wave.
//
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/*---------------------------------------------------------------------------*/
void generateWaveform(int steps, float scale, float phase, float *array)
{
	int i;
	float sine, ftemp, w;
	float fsteps=(float)steps;

		phase = phase*two_PI/360.0;	//convert phase from degrees to radians.

		w = two_PI/fsteps;

		for(i=0; i<steps; i++)
		{
			ftemp = (float) i;
			ftemp = (ftemp * w) + phase;
			if (ftemp>two_PI) ftemp = ftemp - two_PI;
			sine = sin(ftemp);
			*(array+i) = sine * scale;	//scale & place in array.
		}
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

//=====================================================================
//	Load Management Signals
//=====================================================================
void set_LmAdd()
{
	if (!_getbit(P3,5))
	{
	_putbit(1,P3,5);
	LogEvent(Ev_LOAD_MANAGEMENT,3);
	}
}
//=====================================================================
void clear_LmAdd()
{
	if (_getbit(P3,5))
	{
	_putbit(0,P3,5);
	LogEvent(Ev_LOAD_MANAGEMENT,4);
	}
}
//=====================================================================
void clear_LmShed()
{
	if (_getbit(P3,9))
	{
		_putbit(0,P3,9);
		LogEvent(Ev_LOAD_MANAGEMENT,2);
		
		if (OPTCON_PCB)
		{
			OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xFFFD;; 	//clear LOAD SHED in image.
			optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.					
		}
	}
}
//=====================================================================
void set_LmShed()
{
	if (!_getbit(P3,9))
	{
		_putbit(1,P3,9);
		LogEvent(Ev_LOAD_MANAGEMENT,1);
		
		if (OPTCON_PCB)
		{
			OPTCON_CONTROL_2 = OPTCON_CONTROL_2 | 0x0002; 	//set LOAD SHED in image.
			optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.					
		}
	}
}
//=====================================================================

/*---------------------------------------------------------------------------*/
//done in init_ram()
	//timerId LmAddOffTimer = getTimer();		//declare & allocate timer
	//timerId LmShedOffTimer = getTimer();	//declare & allocate timer
	//timerId LmShedOnTimer = getTimer();		//declare & allocate timer
	//timerId LmAddOnTimer = getTimer();		//declare & allocate timer

// updateLoadManagementSignals(p) updates two signals, "S" and "A" where:
//			S is Shed Load output to a PLC or Switchgear system
//			A is Add Load output to a PLC or Switchgear system
//
// p is percentage of rated system power utilized
//
void updateLoadManagementSignals(float p)	//v3.00 LoadManagementPlcInterfaceOption
{
	if (gp_state.invstat==OFFLINE)
	{
		_putbit(0,P3,9);	//initialize Shed OFF
		_putbit(0,P3,5);	//initialize Add OFF

		if (OPTCON_PCB)
		{
			OPTCON_CONTROL_2 = OPTCON_CONTROL_2 & 0xFFDF;; 	//clear LOAD SHED in image.
			optcon_control_2 = OPTCON_CONTROL_2;	   		//write OPTCON_CONTROL.					
		}
	}
	else
	{
	//ADD OFF
		if (p >= LmAddOffLevel)
		{
			if (timerRunning(LmAddOffTimer))
			{
				if (elapsedTime(LmAddOffTimer) > TimMilliseconds(LmAddOffDelay))
				{
					clear_LmAdd();		// A = 0
					stopTimer(LmAddOffTimer);
				}
			}
			else
			{
				startTimer(LmAddOffTimer);
			}
		}
		else
		{
			stopTimer(LmAddOffTimer);
		}

	//SHED OFF
		if (p <= LmShedOffLevel)
		{
			if (timerRunning(LmShedOffTimer))
			{
				if (elapsedTime(LmShedOffTimer) > TimMilliseconds(LmShedOffDelay))
				{
					clear_LmShed();								// S = 0
					stopTimer(LmShedOffTimer);
				}
			}
			else
			{
				startTimer(LmShedOffTimer);
			}
		}
		else
		{
			stopTimer(LmShedOffTimer);
		}

	//SHED ON
		if (p >= LmShedOnLevel)
		{
			if (timerRunning(LmShedOnTimer))
			{
				if (elapsedTime(LmShedOnTimer) > TimMilliseconds(LmShedOnDelay))
				{
					set_LmShed();								// S = 1
					stopTimer(LmShedOnTimer);
				}
			}
			else
			{
				startTimer(LmShedOnTimer);
			}
		}
		else
		{
			stopTimer(LmShedOnTimer);
		}

	//ADD ON
		if (p <= LmAddOnLevel)
		{
			if (timerRunning(LmAddOnTimer))
			{
				if (elapsedTime(LmAddOnTimer) > TimMilliseconds(LmAddOnDelay))
				{
					set_LmAdd();								// A = 1
					stopTimer(LmAddOnTimer);
				}
			}
			else
			{
				startTimer(LmAddOnTimer);
			}
		}
		else
		{
			stopTimer(LmAddOnTimer);
		}
	}
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
