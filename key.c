/* @(#)KEY.C */
/* ASEA SOURCE CODE */
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	FILE:			key.c
//	PURPOSE:		KEYPAD Source Module for the Asea Controller.
//
//	Proprietary Software:	ASEA POWER SYSTEMS
//
//				COPYRIGHT @ ASEA JANUARY 1999
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

/* ======================================================================================= INCLUDE FILES */
#include "ac.h"
//EVENT LOG extern

#include "event.h"
extern int LogEvent( unsigned char, unsigned char );

//==========================================================================
// Typedefs
//==========================================================================
typedef unsigned long	timeType;	//This type exists in log.h
typedef unsigned		timerId;	//Timer Id/Handle is an index

/*=========================================================================*/
/*=========================PUBLIC==========================================*/
//extern void strobe_kybd_for_slew(void);
//extern int wait_for_key(void);

/*=========================================================================*/
/*=========================PRIVATE=========================================*/
interrupt(0x30) using (KYBD_RB) void shore_keyin(void);		// CC16INT, key has been pressed.
interrupt(0x31) using (KYBD_RB) void converter_keyin(void);	// CC17INT, key has been pressed.
interrupt(0x32) using (KYBD_RB) void status_keyin(void);	// CC18INT, key has been pressed.
interrupt(0x33) using (KYBD_RB) void f1_keyin(void);		// CC19INT, key has been pressed.
interrupt(0x34) using (KYBD_RB) void f2_keyin(void);		// CC20INT, key has been pressed.

interrupt(0x1B) using (KYBD_RB) void shore_power_off(void);	// CC11INT, key has been pressed.

void scan_kybd(void);
void strobe_kybd_for_slew(void);
int wait_for_key(void);

/*=========================================================================*/
/*=========================================================================*/

/* ======================================================================================= PERSISTANT MEMORY */
#pragma default_attributes
/* ======================================================================================= PERSISTANT MEMORY */

/* ======================================================================================= VOLITILE MEMORY */
#pragma clear	// fill iram with cleared data so noclear data is put in batt ram.
/* ======================================================================================= VOLITILE MEMORY */
volatile unsigned new_key;
volatile unsigned last_key;
volatile unsigned key_hold_time;
extern volatile unsigned shore_power_off_key; //v1.84

/*=========================================================================*/
//				EXTERN DATA STRUCTURES
/*=========================================================================*/
extern void meas_all(void);			/* calc new meter values. */
extern struct queueType command,*command_queue;
extern volatile int display_type;
extern volatile int display_function;
extern volatile unsigned special_function;
extern volatile unsigned abort_power_on;
extern char bat_ok[];					// battery backed up ram test string.
extern volatile int combo_key;
extern int check_gp_status(void);
extern unsigned EXT_CB_OPTION;


extern void init_lcd(void);
extern int set_sysstat(unsigned);
extern struct gp_state_struct gp_state;
extern volatile int gp_mode;
extern unsigned autorestart;
extern volatile int first_time_through;
extern volatile unsigned far int gp_status_word3;	//CS3C(low) GP STATUS WORD3.
extern volatile unsigned input_dropped;
extern unsigned MULTIGEN_OPTION;		//S3-5
extern unsigned NEW_G2G_OPTION;			//S3-8
extern volatile unsigned ts;			//transfer via serial request.
extern volatile unsigned shutoffId;
extern volatile unsigned retryCount;
extern unsigned MODEL_LC;				//S4-8

//extern void allowInputOn( void );	//REM_IN_OFF deasserted.
//extern void	stopTimer( timerId );	//set this timer to 0 aka stopped.
//extern unsigned timerRunning( timerId );	//Boolean indicating if timer started.
//extern timerId inhibitOnTimer;		//declare inhibitOnTimer.
extern void softStartGuard( void );	//v3.00

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//@KEYPAD		KEYPAD		KEYPAD		KEYPAD		KEYPAD		KEYPAD		KEYPAD		KEYPAD		KEYPAD
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
/* ======================================================================================= FUNCTION DEFINITIONS */
/* ======================================================================================= INTERRUPTS/TASKS */
/*---------------------------------------------------------------------------*/
interrupt(0x30) using (KYBD_RB) void shore_keyin()		// CC16INT, key has been pressed.
{														// column 5 SHORE POWER || F1
//	if (!MODEL_LC)	//Model 'L' sees 3 keys only.
//	{
		if (gp_state.gp_mode==NORMAL_MODE)
		{
			scan_kybd();
		}
//	}
}	
/*---------------------------------------------------------------------------*/
interrupt(0x31) using (KYBD_RB) void converter_keyin()	// CC17INT, key has been pressed.
{														// column 4 GENERATOR POWER || F2
//	if (!MODEL_LC)	//Model 'L' sees 3 keys only.
//	{
		if (gp_state.gp_mode==NORMAL_MODE)
		{
			scan_kybd();
		}
//	}
}	
/*---------------------------------------------------------------------------*/
interrupt(0x32) using (KYBD_RB) void status_keyin()		// CC18INT, key has been pressed.
{														// column 3 CONVERTER POWER || F3 
//	if (!MODEL_LC)	//Model 'L' sees 3 keys only.
//	{
		if (gp_state.gp_mode==NORMAL_MODE)
		{
			scan_kybd();
		}
//	}
}	
/*---------------------------------------------------------------------------*/
interrupt(0x33) using (KYBD_RB) void f1_keyin()		// CC19INT, key has been pressed.
{													// column 2 SYSTEM STATUS || F4 
	if (gp_state.gp_mode==NORMAL_MODE)				//CLEAR_FAULT on MODLE_LC
	{
		scan_kybd();
	}
}	
/*---------------------------------------------------------------------------*/
interrupt(0x34) using (KYBD_RB) void f2_keyin()		// CC20INT, key has been pressed.
{													// column 1 CALIBRATE || F5 
	if (gp_state.gp_mode==NORMAL_MODE)				//ENABLE_RESTART on MODEL_LC
	{
		scan_kybd();
	}
}	
/*---------------------------------------------------------------------------*/
interrupt(0x1B) using (KYBD_RB) void shore_power_off()	// CC11INT, key has been pressed.
{														// SHORE POWER OFF command key. 
	CC11IC = 0;		//disable interrupt when being serviced.

	if (shore_power_off_key)
	{
		input_dropped=FALSE;	// Operator pressed SP OFF, so, disable autorestart.
		autorestart=DISABLED;
	}
	
	LogEvent(Ev_KEY_PRESS,253);
	shutoffId = 1;				//reason input is off, sp off key.
	retryCount=0;

	CC11IC = 0x0067;		//enable interrupt when done.
}	
/*---------------------------------------------------------------------------*/

/* ============================================================================================================ */
/* ======================================================================================= FUNCTION DEFINITIONS */
/*---------------------------------------------------------------------------*/
void strobe_kybd_for_slew()
{
	scan_kybd();
}
/*---------------------------------------------------------------------------*/
void scan_kybd()
{    
//  register unsigned keyin_enable=0x66;	// to avoid EXTR instruction.
  register unsigned p8,f1,f2,f3,f4,f5,sp,gp,cp,ss,c;

	new_key = 0;

	CC16IC = 0; 			//disable kybd interrupt.
	CC17IC = 0; 			//disable kybd interrupt.
	CC18IC = 0; 			//disable kybd interrupt.
	CC19IC = 0; 			//disable kybd interrupt.
	CC20IC = 0; 			//disable kybd interrupt.

	p8=f1=f2=f3=f4=f5=sp=gp=cp=ss=c=0;

//#ifdef TEST_FIXTURE		//FORCE CONFIGURATION
//	return;
//#else

//	if (!MODEL_LC)	//Model 'L' LOOK AT ROW#1 cc19 & cc20 only.
//	{
		_putbit(0,P3,7);	//ROW #2 low
		_nop();
		delay(1);
		_putbit(1,P3,2);	//ROW #1 high for read.
		delay(1);
//	}

	p8=P8;

	if (!(p8&16))								//SP
	{
//		if (MODEL_LC)	//Model 'L' ENABLE_RESTART.
//		{
//			if (autorestart)
//			{
//				cp=TRUE;
//				f2=TRUE;		//autorestart OFF
//			}
//			else
//			{
//				cp=TRUE;
//				f1=TRUE;		//autorestart ON
//			}
//		}
//		else
//		{
			sp=1;
			if (special_function)
			{
				combo_key=2;					// move cursor to next calibration field.
			}
			else
			{
				display_type = SHORE_POWER_1; 
			}	
//		}
		new_key=16;
	}

	if (!(p8&8))								//GENERATOR_POWER
	{
//		if (MODEL_LC)	//Model 'L' CLEAR_FAULT.
//		{
//			f1=TRUE;
//			f2=TRUE;
//		}
//		else
//		{
			gp=1;
			if (special_function)
			{
				combo_key=2;					// move cursor to next calibration field.
			}
			else
			{
				display_type = GENERATOR_POWER; 
			}	
//		}
		new_key=11;
	}

//	if (!MODEL_LC)	//Model 'L' LOOK AT ROW#1 cc19 & cc20 only.
//	{
		if (!(p8&4))					   			//CP
		{
			cp=1;			

			if (special_function)
			{
				combo_key=2;					// move cursor to next calibration field.
			}
			else
			{
				display_type = CONVERTER_POWER; 
			}	
			new_key=17;
		}

		if (!(p8&2))								//SS
		{
			ss=1; 
			if (special_function)
			{
				combo_key=86;					// reset combo_key value.
			}
			else
			{
				display_type = SYSTEM_STATUS; 
				display_function = F1; 
	//			key_hold_time = 0;
			}	
			key_hold_time = 0;
			new_key=18; 
		}

		if (!(p8&1))								//C
		{
			c=1; 
			if (special_function && cp)
			{
				combo_key=3;	// calibrate oscilator after slew up|down.
			}
			new_key=99;
//#ifdef TEST_FIXTURE
//			abort_power_on=1;
//#endif
		}

		_putbit(0,P3,2);	//ROW #1 LOW
		_nop();
		_putbit(1,P3,7);	//ROW #2 HIGH read row 2.
		_nop();
		delay(1);

		p8=P8;

		if (!(p8&16))								//F1
		{
			f1=1;
			if (special_function)
			{
					combo_key=0;	// slew up (increase calibration tern's kfactor).
			}
			else
			{
				display_function = F1; 
	//			key_hold_time = 0;
			}
			new_key=19; 
			key_hold_time = 0;
		}

		if (!(p8&8))								//F2
		{
			f2=1;
			if (special_function)
			{
				combo_key=1;		// slew down (decrease calibration tern's kfactor).
			}
			else
			{
				display_function = F2; 
	//			key_hold_time = 0;
			}
			new_key=20; 
			key_hold_time = 0;
		}

		if (!(p8&4))					   			//F3
		{
			f3=1;
			if (!special_function) display_function = F7; 
			new_key=8; 
			key_hold_time = 0;
		}

		if (!(p8&2))								//F4
		{
			f4=1;
			if (!special_function) display_function = F4; 
			new_key=9; 
			key_hold_time = 0;
		}

		if (!(p8&1))								//F5
		{
			f5=1;
			if (special_function)
			{
				combo_key=5;	// exit.
			}
			else
			{
				display_type = HELP;
			}
			new_key=10; 
			key_hold_time = 0;
		}
//	}//endif (!MODEL_LC)	//Model 'L' LOOK AT ROW#1 ONLY.
 
//SINGLE KEY PRESS EVENTS
//	if (sp) 	new_key=16;	//DISPLAY_TYPE=SHORE_POWER
//	if (gp) 	new_key=11;	//DISPLAY_TYPE=GENERATOR_POWER
//	if (cp) 	new_key=17;	//DISPLAY_TYPE=CONVERTER_POWER
//	if (ss) 	new_key=18;	//DISPLAY_TYPE=SYSTEM_STATUS, gp_mode=NORMAL;
//	if (c) 		new_key=13;	//DISPLAY_TYPE=SYSTEM_STATUS, gp_mode=NORMAL;
//	if (f1)		new_key=19;		//DISPLAY_FUNCTION=F1 : 
//	if (f2) 	new_key=20;		//DISPLAY_FUNCTION=F2 : 
//	if (f3) 	new_key=8;		//DISPLAY_FUNCTION=F4 : GP_STATUS_WORD,system_temperature,+HVDC,-HVDC;
//	if (f4) 	new_key=9;		//DISPLAY_TYPE=HELP : show_systat(); 
//	if (f5) 	new_key=10;		//DISPLAY_TYPE=HELP : show_systat();

//COMBINATION KEY PRESS EVENTS

	if(ss && f1 && f2)
	{
		bat_ok[0] =' ';
		gp_reset();						// ss && f1 && f2
	}

	if(c && f4)
	{
		abort_power_on=4;
	}

	if (!special_function) 
	{
		if(f1 && cp && (gp_state.invstat==ONLINE))
		{
			if (!EXT_CB_OPTION || shore_power_off_key)
				autorestart=1;
			display_type = SYSTEM_STATUS; 
			gp_mode=NORMAL_MODE;
			new_key=0;
			first_time_through=2;
		}
//		if(f1 && cp && ss && (gp_state.invstat==ONLINE))
//		{
//			autorestart=1;
//			display_type = SYSTEM_STATUS; 
//			gp_mode=NORMAL_MODE;
//			new_key=0;
//			first_time_through=2;
//		}

//		if(f2 && cp && sp)
		if(f2 && cp)
		{
			autorestart=0;
			display_type = SYSTEM_STATUS; 
			gp_mode=NORMAL_MODE;
			new_key=0;
			first_time_through=2;
		}

		if(ss && f1)
		{
			display_type = HELP;			// ss && f1
			gp_mode=NORMAL_MODE;
		//clear SHUTDOWN status.
			set_sysstat(OK);
			init_lcd();
			new_key=0;
		}

		if(ss && f2)
		{
			display_type = STATUS2;			// ss && f2
			new_key=0;
		}

		if(ss && f3)
		{
			display_type = EDIT_SERIAL_COMM_PARAMS;			// ss && f3
			new_key=0;
		}

		if(ss && f5)
		{
//			if (timerRunning(inhibitOnTimer))
//			{
//				stopTimer(inhibitOnTimer);					//stop inhibitOnTimer.
//				allowInputOn();								//ALLOW INPUT ON.
//			}
			display_type = SYSTEM_STATUS; 
			gp_mode=NORMAL_MODE;
			new_key=111;						//ABORT SOFT-START GUARD INHIBIT
		}

		if(f4 && f5 && !MULTIGEN_OPTION)
		{
			gp_mode=TEST_MODE;
			display_type = GENERATOR_POWER;	
			new_key=0;
		}

		if(ss && c)
		{
			new_key=400;
		}

		if(f1 && f2)						// f1 && f2 
		{
			gp_mode=NORMAL_MODE;			
			set_sysstat(OK);			    //clear SHUTDOWN status.
		
//			if (!MODEL_LC)	//Model 'L' LOOK AT ROW#1 cc19 & cc20 only.
//			{
				init_lcd();						
				new_key=0;
//			}
		}

		if(cp && sp)						//Extreme Measurements
		{
			display_function = F11; 
		}

		if(sp && cp && c)
		{
			display_type = CONVERTER_POWER; 
			display_function = F6; 			// sp && ss && f2
			new_key=0;
		}

//		if(display_function==F6)
//		{
//			if(f1 || f2 || f3 || f4 || f5 || gp || ss)	display_function=F1;
//		}

		if(display_type==HELP)
		{
			if(f1 || f2 || f3)	display_type=CONVERTER_POWER;
		}

		if(f2 && f3)						// f1 && f2 = f9
		{
			display_function = F9;
		}

		if(f3 && f4)						// f3 && f4 = f8
		{
			display_function = F8;
		}

		if(gp && f4)						//TRANSFER INHIBIT--TECHNEL_OPTION
		{
			new_key = 100;
		}

		if(gp && f5)						//GENERATOR AUTOSTART CONTROL--TECHNEL_OPTION
		{
			new_key = 101;
		}

		if(gp && f3)						//
		{
			new_key = 103;
		}

		if(gp && f1)						//
		{
			new_key = 104;
		}

		if(gp && sp)						//GENERATOR TIMEOUT CONTROL
		{
			new_key = 105;
		}

		if(gp && ss)						//GENERATOR min & max freq's
		{
			display_function = F10; 
		}

		if(sp && f2)						//shore cord alarm setup screen.
		{
			new_key = 201;
		}

		if(sp && f3)						//shore cord alarm setup screen.
		{
			new_key = 203;
		}

		if(cp && f4)						//external output circuit-breaker control screen.
		{
			new_key = 304;
		}

		if(cp && f5)						//Vout control screen.
		{
			new_key = 305;
		}

		if(cp && ss)						//CB reset control screen.
		{
			new_key = 306;
		}

		if(sp && f4)						//AGC control screen.
		{
			new_key = 204;
		}

		if(sp && f5)						//AUTO_SHUTDOWN CONTROL SCREEN
		{
			new_key = 205;
		}
#ifdef GNOP_ON
		if(cp && sp && f5)						//gnop
		{
			new_key = 206;
		}
#endif // GNOP_ON
		if(f1 && c)
		{
			display_type = CONVERTER_POWER; 
			new_key=99;
		}

		if(f2 && c)
		{
			display_type = CONVERTER_POWER; 
			if (!special_function) display_function = F5; 
			new_key=0;
		}

		if(gp && cp)						//AUTO-TRANSFER on autorestart.
		{
			new_key = 202;
		}

	}//endif !special_function.

	// all ROWS HIGH, allow (not enable) keypad interrupt(s).
	_putbit(1,P3,7);	//ROW #2 HIGH
	_nop();
	_putbit(1,P3,2);	//ROW #1 HIGH

	if (display_type==GENERATOR_POWER && display_function==F4)
	{
		if (!NEW_G2G_OPTION && !MULTIGEN_OPTION) display_function=F1;	//then no Synchroscope, so, don't confuse meas.c
	}

//	if (0<new_key&&new_key<255) LogEvent(Ev_KEY_PRESS,(unsigned char)new_key);
//#endif
}
/*---------------------------------------------------------------------------*/
int wait_for_key()		//command code = 	
{
//	if (MODEL_LC) return (new_key=18);	//Model 'L' return SYSTEM_STATUS key press.

	new_key=0;

	while (!new_key)
	{
		new_key=0;

		meas_all();	//remoteManager() call done during meas_all(); NOTE: may be too much overhead.

		check_gp_status();	//respond to CONVERTER POWER button events.

		softStartGuard();

		if (gp_mode==NORMAL_MODE) meter_mux_iterator(); 	// iterate meter & sync mux controls.

		strobe_kybd_for_slew();

		if (ts)
		{
			new_key=18;	//system_status key injection.
			ts = FALSE;
		}
	}
	return new_key;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*					OLD
void scan_kybd()
{    
  int key_code;
  register unsigned p8,cp_on,ts_gen,ts_off,ts_con,cp_off,f1,f2,f3,help,sp,gp,cp,ss;

	CC20IC = 0;

	new_key=0;

	p8=cp_on=ts_gen=ts_off=ts_con=cp_off=f1=f2=f3=help=sp=gp=cp=ss=0;

	_bfld(P7,0xf0,0xe0);					// strobe kybd0.
	p8=P8;
	if (!(p8&8))								//cp ON
	{
		cp_on=1;
	}

	_bfld(P7,0xf0,0xd0);					// strobe kybd1.
	p8=P8;
	if (!(p8&1))								//ts GENERATOR
	{
		ts_gen=1;
	}
	if (!(p8&2))								//ts OFF
	{
		ts_off=1;
	}
	if (!(p8&4))					   			//ts CONVERTER
	{
		ts_con=1;			
	}
	if (!(p8&8))								//cp OFF
	{
		cp_off=1; 
		disable_input_power();			//disable all input power.
		flush_queue(command_queue);	//clear all queue entries.
		abort_power_on=1;
	}

	_bfld(P7,0xf0,0xb0);					// strobe kybd2.
	p8=P8;
	if (!(p8&1))								//F1
	{
		f1=1;
		if (!special_function)
		{
			_putbit(1,P7,3); 	//latch display_function
			_putbit(0,P7,3);
		}
	}
	if (!(p8&2))								//F2
	{
		f2=1;
		if (!special_function)
		{
			_putbit(1,P7,3); 	//latch display_function
			_putbit(0,P7,3);
		}
	}
	if (!(p8&4))					   			//F3
	{
		f3=1;
		if (!special_function)
		{
			_putbit(1,P7,3); 	//latch display_function
			_putbit(0,P7,3);
		}
	}
	if (!(p8&8))								//HELP
	{
		help=1;
	}

	_bfld(P7,0xf0,0x70);					// strobe kybd3.
	p8=P8;
	if (!(p8&1))								// SHORE POWER
	{
		sp=1;
		if (!special_function)
		{
			_putbit(1,P8,7);	//latch display_type
			_putbit(0,P8,7);
		}
	}
	if (!(p8&2))								// GENERATOR POWER
	{
		gp=1;
		if (!special_function)
		{
			_putbit(1,P8,7);	//latch display_type
			_putbit(0,P8,7);
		}
	}
	if (!(p8&4))								// CONVERTER POWER
	{
		cp=1;
		if (!special_function)
		{
			_putbit(1,P8,7);	//latch display_type
			_putbit(0,P8,7);
		}
	}
	if (!(p8&8))								// SYSTEM STATUS
	{
		ss=1;
		if (!special_function)
		{
			_putbit(1,P8,7);	//latch display_type
			_putbit(0,P8,7);
		}
	}

//SINGLE KEY PRESS EVENTS
	key_code=0;
	if (cp_on) 	key_code=1;		//analyze_input_power();
	if (ts_gen) key_code=2;		//transfer2generator();
	if (ts_off) key_code=3;		//output_OFF(), generator_OFF();
	if (ts_con) key_code=4;		//transfer2converter();
	if (cp_off) key_code=5;		//disable_input_power();
	if (f1)		key_code=6;		//DISPLAY_FUNCTION=F1 : 
	if (f2) 	key_code=7;		//DISPLAY_FUNCTION=F2 : 
	if (f3) 	key_code=8;		//DISPLAY_FUNCTION=F4 : GP_STATUS_WORD,system_temperature,+HVDC,-HVDC;
	if (help) 	key_code=9;		//DISPLAY_TYPE=HELP : show_systat(); 
	if (sp) 	key_code=10;	//DISPLAY_TYPE=SHORE_POWER
	if (gp) 	key_code=11;	//DISPLAY_TYPE=GENERATOR_POWER
	if (cp) 	key_code=12;	//DISPLAY_TYPE=CONVERTER_POWER
	if (ss) 	key_code=13;	//DISPLAY_TYPE=SYSTEM_STATUS, gp_mode=NORMAL;

//COMBINATION KEY PRESS EVENTS
	if (cp_off && sp)		key_code=14;//DISPLAY_TYPE=SHORE_POWER, gp_mode=CALIBRATION_MODE;
	if (cp_on && sp)		key_code=14;//DISPLAY_TYPE=SHORE_POWER, gp_mode=CALIBRATION_MODE;
	if (cp_off && gp)		key_code=15;//DISPLAY_TYPE=GENERATOR_POWER, gp_mode=CALIBRATION_MODE;
	if (cp_on  && gp)		key_code=16;//DISPLAY_TYPE=GENERATOR_POWER, gp_mode=CALIBRATION_MODE;
	if (cp_on  && cp)		key_code=17;//DISPLAY_TYPE=CONVERTER_POWER, gp_mode=CALIBRATION_MODE;
	if (ss && f1)			key_code=18;//gp_mode=NORMAL_MODE; clear shutdown hardware status register;
	if (ss && f3)			key_code=19;//DISPLAY_TYPE=EDIT_SERIAL_COMM_PARAMS;	   
	if (f2 && f3)			key_code=21;//DISPLAY_FUNCTION=F5 : dump crosspoint input buffers to lcd;	   
	if (ss && sp)			key_code=22;//reevaluate shore power status;	   
	if (ss && gp)			key_code=23;//reevaluate generator status;
	if (f2 && cp && help)	key_code=24;//DISPLAY_FUNCTION=F6 : designer_id;
	if (ss && f2)			key_code=25;//reinitialize LCD;
	if (f1 && f2)			key_code=26;//DISPLAY_FUNCTION=F7 : Ipeak, Icrest, Irms;
	if (f1 && f3)			key_code=27;//DISPLAY_FUNCTION=F8 : V_kfactor, I_kfactor, OSC_Kout;
	if (f1 && help)			key_code=28;//DISPLAY_FUNCTION=F9 : ZOP_state,min,max,now, ALC_state,Va_out,Vb_out,Vc_out;

	if (cp_off && f1 && f3 && ts_off) {bat_ok[0] =' '; gp_reset();}//reset all system memory & reboot controller.
	if (cp_off && f1 && f3)	{gp_reset();}	//reset system.	key_code=-1;//combo_key=-1;		// reboot controller.

	_bfld(P7,0xf0,0x00);	// all ROWS LOW, allow (not enable) interrupt.

	if (!queueFull(command_queue) && key_code)
	{
		enqueue(command_queue, &key_code); 		//if queueFull() does not enqueue new entry.
	}
}
/*---------------------------------------------------------------------------*/
